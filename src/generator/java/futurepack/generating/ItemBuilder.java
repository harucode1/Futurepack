package futurepack.generating;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

public class ItemBuilder extends BuilderBase<ItemDef>
{

	protected ItemBuilder(File srcOut, File resOut) 
	{
		super(srcOut, resOut);
	}

	public static ItemBuilder create(File srcOut, File resOut)
	{
		return new ItemBuilder(srcOut, resOut);
	}
	
	
	public void addItem(String className, String registry_name, String texture, String args)
	{
		ItemDef def = new ItemDef(className, args, registry_name);
		def.inventory = new Model("item/generated", Collections.singletonMap("layer0", texture));

		this.defs.add(def);
	}	
	
	@Override
	public void buildExtraBody(ClassBuilder build, String packageName, String className, String group)
	{
		ItemDef[] def = defs.toArray(new ItemDef[defs.size()]);
		ItemDef.writeItemDefs(build, def);
			
		try 
		{
			ItemDef.writeItemModels(def, this.resOut);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}
