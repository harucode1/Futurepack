package futurepack.extensions.jei.crafting;

import javax.annotation.Nullable;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.common.recipes.crafting.ShapedRecipeWithResearch;
import futurepack.extensions.jei.BaseRecipeCategory;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.extensions.vanilla.crafting.ICraftingCategoryExtension;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Size2i;

public class ShapedResearchCraftingExtension<T extends ShapedRecipeWithResearch> implements ICraftingCategoryExtension
{
	protected final T recipe;

	private float scale = 21F/256F;
	private int x,y;
	
	public ShapedResearchCraftingExtension(T recipe) 
	{
		this.recipe = recipe;
		x = 61;
		y = 16;
	}
	
	@Override
	public void setIngredients(IIngredients ingredients) 
	{
		ingredients.setInputIngredients(recipe.getIngredients());
		ingredients.setOutput(VanillaTypes.ITEM, recipe.getResultItem());
	}

	@Nullable
	@Override
	public ResourceLocation getRegistryName() 
	{
		return recipe.getId();
	}

	@Nullable
	@Override
	public Size2i getSize() 
	{
		return new Size2i(recipe.getRecipeWidth(), recipe.getRecipeHeight());
	}
	
	@Override
	public void drawInfo(int recipeWidth, int recipeHeight, MatrixStack matrixStack, double mouseX, double mouseY) 
	{
		if(!isResearched())
		{
			matrixStack.pushPose();
			matrixStack.scale(scale, scale, 1);
			GlStateManager._color4f(1f, 1f, 1f, 1f);
			BaseRecipeCategory.blockedIcon.draw(matrixStack, (int)(x/scale), (int)(y/scale));
			matrixStack.popPose();
		}
	}
	
	public boolean isResearched()
	{
		return recipe.isLocalResearched();
	}

}
