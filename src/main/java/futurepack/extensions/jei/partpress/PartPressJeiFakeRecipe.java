package futurepack.extensions.jei.partpress;

import net.minecraft.item.ItemStack;

public class PartPressJeiFakeRecipe 
{
	public PartPressJeiFakeRecipe(String in, ItemStack out)
	{
		input = in;
		output = out;
	}
	
	public String input;
	public ItemStack output;

}
