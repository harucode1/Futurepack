package futurepack.extensions.jei;

import futurepack.api.Constants;
import net.minecraft.util.ResourceLocation;

public class FuturepackUids
{
	public static final ResourceLocation CRAFTING = new ResourceLocation(Constants.MOD_ID, "crafting");
	
	public static final ResourceLocation ASSEMBLY = new ResourceLocation(Constants.MOD_ID,  "assembly");
	
	public static final ResourceLocation INDUTRIALFURNACE = new ResourceLocation(Constants.MOD_ID,  "furnace.industrial");
	
	public static final ResourceLocation INDUSTRIALNEONFURNACE = new ResourceLocation(Constants.MOD_ID,  "furnace.industrial.neon");
	
	public static final ResourceLocation ZENTRIFUGE = new ResourceLocation(Constants.MOD_ID,  "zentrifuge");
	
	public static final ResourceLocation CRUSHER = new ResourceLocation(Constants.MOD_ID,  "crusher");
	
	public static final ResourceLocation AIRBRUSH = new ResourceLocation(Constants.MOD_ID,  "airbrush");
	
	public static final ResourceLocation RECYCLER = new ResourceLocation(Constants.MOD_ID,  "recycler");
	
	public static final ResourceLocation WATERCOOLER = new ResourceLocation(Constants.MOD_ID,  "watercooler");
	
	public static final ResourceLocation PARTPRESS = new ResourceLocation(Constants.MOD_ID,  "partpress");
}
