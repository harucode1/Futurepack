package futurepack.extensions.jei;

import java.util.Collections;
import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.depend.api.helper.HelperComponent;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public abstract class BaseRecipeCategory<T> implements IRecipeCategory<T> 
{
	protected IDrawable bg;
	protected IDrawable icon;
	protected ResourceLocation uid;
	public static IDrawable blockedIcon;
	private float scale = 21F/256F;
	private int x,y;
	
	public BaseRecipeCategory(IGuiHelper gui, IItemProvider icon, ResourceLocation uid, int x, int y) 
	{
		this.icon = gui.createDrawableIngredient(new ItemStack(icon));
		this.uid = uid;
		this.bg = createBackground(gui);
		ResourceLocation location = new ResourceLocation(Constants.MOD_ID, "textures/gui/worning.png");
		blockedIcon = gui.createDrawable(location, 0, 0, 256, 256);
		this.x = x;
		this.y = y;
	}
	
	protected abstract IDrawable createBackground(IGuiHelper gui);
	
	@Override
	public ResourceLocation getUid() 
	{
		return uid;
	}

	@Override
	public String getTitle() 
	{
		ResourceLocation uid = getUid();
		return I18n.get(String.format("%s.%s.name", uid.getNamespace(), uid.getPath()));
	}

	@Override
	public IDrawable getBackground() 
	{
		return bg;
	}

	@Override
	public IDrawable getIcon() 
	{
		return icon;
	}
	
	@Override
	public void draw(T recipe, MatrixStack matrixStack, double mouseX, double mouseY) 
	{
		if(!isResearched(recipe))
		{
			matrixStack.pushPose();
			matrixStack.scale(scale, scale, 1);
			blockedIcon.draw(matrixStack, (int)(x/scale), (int)(y/scale));
			matrixStack.popPose();
		}
	}
	
	@Override
	public List<ITextComponent> getTooltipStrings(T recipe, double mouseX, double mouseY) 
	{
		if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+21, y+21) && !isResearched(recipe))
		{
			return Collections.singletonList(new TranslationTextComponent("futurepack.jei.notresearched"));
		}
		return Collections.emptyList();
	}
	
	public abstract boolean isResearched(T rec);
}
