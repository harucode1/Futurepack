package futurepack.extensions.jei.airbrush;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import futurepack.api.Constants;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.recipes.airbrush.AirbrushRecipeJEI;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class AirbrushCategory extends BaseRecipeCategory<AirbrushRecipeJEI>
{

	public AirbrushCategory(IGuiHelper gui)
	{
		super(gui, ToolItems.airBrush, FuturepackUids.AIRBRUSH, 0, 0);
	}

	@Override
	public Class<? extends AirbrushRecipeJEI> getRecipeClass() 
	{
		return AirbrushRecipeJEI.class;
	}

	@Override
	public void setIngredients(AirbrushRecipeJEI rec, IIngredients ing) 
	{
		List<List<ItemStack>> inputs = new ArrayList<List<ItemStack>>();
		inputs.add(Collections.singletonList(rec.getInput()));
		
		List<ItemStack> color = new ArrayList<ItemStack>();
		color.add(rec.getColorItem());
		if(color.get(0)!=null && color.get(0).getItem()==Blocks.SAND.asItem())
		{
			color.add(new ItemStack(Blocks.GRAVEL));
		}
		inputs.add(color);
		
		ing.setInputLists(VanillaTypes.ITEM, inputs);
		
		ing.setOutput(VanillaTypes.ITEM, rec.getOutput());
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, AirbrushRecipeJEI recipe, IIngredients ingredients) 
	{
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();

		guiItemStacks.init(0, true, 99, 24-7);
		guiItemStacks.init(1, true, 5, 48-7);
		
		guiItemStacks.init(2, false, 159, 24-7);
		
		guiItemStacks.set(ingredients);
	}

	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/lackier_rezept.png");
		return gui.createDrawable(res, 0, 7, 185, 63);
	}

	@Override
	public boolean isResearched(AirbrushRecipeJEI rec) 
	{
		return true;
	}

}
