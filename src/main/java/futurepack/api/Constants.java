package futurepack.api;

import net.minecraft.util.ResourceLocation;

public class Constants
{
	public static final String MOD_ID = "futurepack";
	
	public static final String ADD_RESEARCH_JSON = "research.add.json.reader";
	public static final String DUNGEON_WHITELIST = "dungeon.whitelist";

	public static final int RECURSIVE_BLOCKUPDATES = 127;
	
	public static ResourceLocation unicode_font = new ResourceLocation("futurepack:only_unicode");
	
	public static final String TINKERS_CONSTRUCT = "tconstruct";
}
