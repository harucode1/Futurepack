package futurepack.client.research;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import futurepack.api.event.ResearchFinishEvent;
import futurepack.common.FPLog;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import futurepack.common.sync.NetworkHandler;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;

public class LocalPlayerResearchHelper
{
	public static ArrayList<String> researches = null;//new ArrayList();
	private static LinkedList<WeakReference<Runnable>> able = new LinkedList<WeakReference<Runnable>>();
	
	public static ArrayList<String> notReaded = new ArrayList<String>();
	public static ArrayList<String> researching = new ArrayList<String>();
	
	public static void setupData(int[] local)
	{
		FPLog.logger.info("Get Research Data from Server");
		
		ArrayList<String> old = researches;		
		researches = new ArrayList<String>();
			
		for(int i : local)
		{
			String res = ResearchManager.getById(i).getName();
					
			if(old==null || !old.contains(res))
			{
				Research r = ResearchManager.getById(i);
				if(old!=null)
				{	
					MinecraftForge.EVENT_BUS.post(new ResearchFinishEvent.Client(Minecraft.getInstance().player, r));
					notReaded.add(res);
					Collections.sort(notReaded);
				}			
			}
			researches.add(res);
		}
		
		
		while(!able.isEmpty())
		{
			WeakReference<Runnable> run = able.removeFirst();
			if(run!=null && run.get()!=null)
			{
				run.get().run();
			}
		}			
	}
	
	public static void setupResearching(Integer[] local)
	{
		researching.clear();
		for(int i : local)
		{
			String res = ResearchManager.getById(i).getName();
			
			if(!researching.contains(res))
			{
				researching.add(res);
			}		
		}
	}
		
	public static void requestServerData(Runnable eventhandler)
	{
		able.addLast(new WeakReference<Runnable>(eventhandler));
		FPLog.logger.info("Send Research Request to Server");
		NetworkHandler.requestResearchDataFromServer();
	}
	
	public static CustomPlayerData getLocalPlayerData()
	{
		return CustomPlayerData.createForLocalPlayer();
	}
}
