package futurepack.client;

import java.util.ArrayList;
import java.util.function.Function;

import futurepack.client.render.RenderGleiter;
import futurepack.client.render.RenderRoomAnalyzer;
import futurepack.client.render.block.RenderLogistic;
import futurepack.common.FPConfig;
import futurepack.common.ManagerGleiter;
import futurepack.common.entity.living.EntityAlphaJawaul;
import futurepack.common.gui.GuiFPWorldLoading;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.player.FakePlayerSP;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageRequestJawaulInventory;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.screen.ConfirmBackupScreen;
import net.minecraft.client.gui.screen.DirtMessageScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.inventory.InventoryScreen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;


public class ClientEvents 
{
	public static final ClientEvents INSTANCE = new ClientEvents();   
	
	private ClientEvents()
	{
		
	}
	
	private static ArrayList<Function<RenderWorldLastEvent, IRenderTypeBuffer>> renderers = new ArrayList<>();
	
	public static Function<RenderWorldLastEvent, IRenderTypeBuffer> addDeferedRendering(Function<RenderWorldLastEvent, IRenderTypeBuffer> callback)
	{
		renderers.add(callback);
		return callback;
	}
	
	private static void finishDrawing(IRenderTypeBuffer buffer)
	{
		if(buffer instanceof IRenderTypeBuffer.Impl)
		{
			((IRenderTypeBuffer.Impl) buffer).endBatch();
		}
	}
	
	@SubscribeEvent
	public void onRenderWorld(RenderWorldLastEvent event)
	{
		renderers.stream().map(f -> f.apply(event)).forEach(ClientEvents::finishDrawing);
		renderers.clear();
		
		ItemStack rooma = ItemStack.EMPTY;
		PlayerEntity pl = Minecraft.getInstance().player;
		
		if(pl.getMainHandItem().getItem() == ToolItems.roomanalyzer)
		{
			rooma = pl.getMainHandItem();
		}
		else if(pl.getOffhandItem().getItem() == ToolItems.roomanalyzer)
		{
			rooma = pl.getOffhandItem();
		}
		if(!rooma.isEmpty())
		{
			pl.level.getProfiler().push("futurepack_air_overlay");
			RenderRoomAnalyzer.render(event.getPartialTicks(), event.getMatrixStack());
			pl.level.getProfiler().pop();
		}
		
		boolean mainH = !pl.getItemInHand(Hand.MAIN_HAND).isEmpty() && pl.getItemInHand(Hand.MAIN_HAND).getItem() == ToolItems.logisticEditor;
		boolean offH = !pl.getItemInHand(Hand.OFF_HAND).isEmpty() && pl.getItemInHand(Hand.OFF_HAND).getItem() == ToolItems.logisticEditor;
		
		if(mainH || offH)
		{
			RenderLogistic.onItemHeld(pl.getItemInHand(mainH ? Hand.MAIN_HAND : Hand.OFF_HAND), event.getPartialTicks(), event.getMatrixStack());
		}
		
	}
	
	@SubscribeEvent
	public void onItemPulling(FOVUpdateEvent event)
	{
		ItemStack it = event.getEntity().getUseItem();
		if(!it.isEmpty())
		{
			int j = it.getUseDuration() - event.getEntity().getUseItemRemainingTicks();
			float f;
			if(it.getItem() == ToolItems.laser_bow)
			{
				f = j / ( 20F / (1 + EnchantmentHelper.getItemEnchantmentLevel(Enchantments.POWER_ARROWS, it)) );
			}
			else if(it.getItem() == ToolItems.grenade_launcher)
			{
				f = j /20F;
			}
			else
			{
				return;
			}
			
			f = (f * f + f * 2.0F) / 3.0F;
			if (f < 0.1D || event.getEntity().getUseItemRemainingTicks()==0)
			{
				return;
			}
			if (f > 1.0F)
			{
				f = 1.0F;
			}
			event.setNewfov(event.getFov()-(f*0.5F));
		}
	}
	
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
//		try
//		{
//			FPBlocks.setupPreRendering();
//		}
//		catch(NoSuchMethodError e){}
//		FPBlocks.setupRendering();
//		FPItems.setupRendering();  
		
	}
	
	@SubscribeEvent
	public void airOverlayPre(RenderGameOverlayEvent.Pre event)
	{
		if(event.getType() == ElementType.AIR)
		{
			Minecraft mc = Minecraft.getInstance();
			if(!AtmosphereManager.hasWorldOxygen(mc.level))
			{
				Entity e = mc.getCameraEntity();
				if(mc.player == e)
				{
					Minecraft.getInstance().setCameraEntity(new FakePlayerSP(mc.player));
				}
			}			
		}
	}
	
	@SubscribeEvent
	public void airOverlayPost(RenderGameOverlayEvent.Post event)
	{
		if(event.getType() == ElementType.AIR)
		{
			Minecraft mc = Minecraft.getInstance();
			if(!AtmosphereManager.hasWorldOxygen(mc.level))
			{
				if(mc.getCameraEntity().getClass()==FakePlayerSP.class)
				{
					Minecraft.getInstance().setCameraEntity(mc.player);
				}
			}
			if(System.currentTimeMillis() - lastUpdate < 2000)
			{
				int guiX=0, guiY=0;
				guiX = Minecraft.getInstance().getWindow().getGuiScaledWidth()/2;
				guiY = Minecraft.getInstance().getWindow().getGuiScaledHeight()-59;
				int tank = Math.round(20 * tankFill);
				for(int i=0;i<10;i++)
				{
					int id = tank >= 2 ? 30 : tank >= 1 ? 31 : 32;
					HelperComponent.renderSymbol(event.getMatrixStack(), guiX + 83 -8 * (i), guiY, 0, id);//id 30-32
					tank -= 2;
				}
				
			}
		}
	}
	
	private static long lastUpdate = 0;
	private static float tankFill = 0F;
	
	public static void setAirTanks(float full)
	{
		lastUpdate = System.currentTimeMillis();
		tankFill = full;
	}
	
	@SubscribeEvent
	public void onRenderPlayerModel(RenderPlayerEvent.Post render)
	{
		if(ManagerGleiter.isGleiterOpen(render.getPlayer()))
		{
			RenderGleiter.onRender(render);
		}
	}
		
	@SubscribeEvent
	public void onRenderPlayerFirstPerson(RenderHandEvent event)
	{
		if(event.getHand()== Hand.MAIN_HAND)
		{
			if(ManagerGleiter.isGleiterOpen(Minecraft.getInstance().player))
			{
				RenderGleiter.onRender(event);
			}
		}
	}
	
	@SubscribeEvent
	public void onGuiOpen(GuiOpenEvent event)
	{
		Screen gui = event.getGui();
		if (gui instanceof ConfirmBackupScreen && FPConfig.CLIENT.futurepackSkipBackupScreen.get())
		{
			Minecraft th = Minecraft.getInstance();
			//gui.init(th, th.getMainWindow().getScaledWidth(), th.getMainWindow().getScaledHeight());
			
			ConfirmBackupScreen cbs = (ConfirmBackupScreen)gui;
			
			if(cbs.getTitle() instanceof TranslationTextComponent)
			{
				TranslationTextComponent ttc = (TranslationTextComponent) cbs.getTitle();
				if("selectWorld.backupQuestion.experimental".equals(ttc.getKey()))
				{
					Thread t = new Thread(() -> {
						try
						{
							Thread.sleep(247);
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						
						if(th.screen == cbs)
						{
							/**
							 * btn0 = Create Backup and load world
							 * btn1 = Just load world without backup
							 */
							int btnNumber = 1;
							
							if(FPConfig.CLIENT.futurepackCreateBackupOnSkip.get()) {
								btnNumber = 0;
							}
							
							Button IknowWhatIamDoing = (Button) cbs.children().get(btnNumber); //lets create ALOT of backups

							TranslationTextComponent displayText = new TranslationTextComponent("menu.loadingLevel");
							
							if(FPConfig.CLIENT.futurepackShowCustomLoadingScreen.get()) {
								Minecraft.getInstance().setScreen(new GuiFPWorldLoading(displayText));
							}
							else {
								Minecraft.getInstance().setScreen(new DirtMessageScreen(displayText));
							}
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							
							th.executeBlocking(IknowWhatIamDoing::onPress);
						}
					});
					t.start();
				}
			}
		}
		else if(gui instanceof InventoryScreen)
		{
			ClientPlayerEntity cp = Minecraft.getInstance().player;
			if(cp.isPassenger() && cp.getVehicle() instanceof EntityAlphaJawaul && ((EntityAlphaJawaul)cp.getVehicle()).isTame() && ((EntityAlphaJawaul)cp.getVehicle()).hasChest())
			{

				FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageRequestJawaulInventory());
				event.setCanceled(true);
			}
		}
	}
	
}
