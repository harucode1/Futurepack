package futurepack.client.color;

import java.awt.Color;
import java.util.Random;

import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.item.IItemColorable;
import futurepack.common.item.misc.ItemAncientEnergy;
import futurepack.common.item.misc.ItemResearchBlueprint;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.research.Research;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.item.BlockItem;
import net.minecraft.item.IDyeableArmorItem;
import net.minecraft.world.GrassColors;
import net.minecraft.world.biome.BiomeColors;

public class ItemBlockColoring 
{
	public static void setupColoring()
	{
		ItemColors itemC = Minecraft.getInstance().getItemColors();
		BlockColors blockC = Minecraft.getInstance().getBlockColors();
		
		itemC.register((stack, tint) ->  {
			 BlockState state = ((BlockItem)stack.getItem()).getBlock().defaultBlockState();
			return blockC.getColor(state, null, null, tint);
		},  TerrainBlocks.grass_t);
		
		blockC.register((state, world, pos, tintIndex) -> {
			return world != null && pos != null ? BiomeColors.getAverageGrassColor(world, pos) : GrassColors.get(0.5D, 1.0D);
		}, TerrainBlocks.grass_t);
		
		itemC.register((stack,  tint) -> {
			return tint==1 ? ((IItemColorable)stack.getItem()).getColor(stack) : 0xFFFFFF;
		}, ToolItems.modul_paraglider);
		itemC.register((stack,  tint) -> {
			return tint==1 ? ((IDyeableArmorItem)stack.getItem()).getColor(stack) : 0xFFFFFF;
		}, ToolItems.gleiter);
		
		itemC.register((stack, tind) -> {
			
			if(tind==0)
				return 0xffffff;
			
			Research r = ItemResearchBlueprint.getResearchFromItem(stack);
			if(r!=null)
			{
				Random rand = new Random(r.id*10 + tind);
				int color = 0;			
				color |= rand.nextInt(256) << 16;
				color |= (rand.nextInt(256)) << 8;
				color |= (rand.nextInt(256)) << 0;
				return color;		
			}
			return 0xffffff;
		}, MiscItems.RESEARCH_BLUEPRINT);
		
		
		itemC.register((stack, tind) -> {
			if(tind==0)
			{
				float f = ItemAncientEnergy.getFillState(stack);
				return Color.HSBtoRGB((240F-120F*f)/360F, 0.8F, Math.min(1F, 0.5F * f + 0.3F));
			}
			return 0xffffff;
		}, MiscItems.ancient_energy);
	}
}
