package futurepack.client.sos;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.IGuiEventListener;

public abstract class AnimationBase implements IGuiEventListener
{
	protected int xPos,yPos,width,height;
	
	public void init(int xPos, int yPos, int width, int height)
	{
		this.xPos=xPos;
		this.yPos=yPos;
		this.width=width;
		this.height=height;
	}
	
	public abstract void render(MatrixStack matrixStack, int mouseX, int mouseY);
	
	public abstract boolean isFinished();
}
