package futurepack.client.sos;

import java.util.Random;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.util.ResourceLocation;

public class AnimationPusle extends AnimationPicture
{
	private final int parts;
	private final ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/os/fragez.png");
	private Random r;
	public AnimationPusle(int time, TileEntityScannerBlock data)
	{
		super(new ResourceLocation(Constants.MOD_ID, "textures/os/asp_fehlt.png"), time);
		parts = data.getProperty(4);
		r = new Random();
		//ticks = System.currentTimeMillis() + 10 * 1000;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		r.setSeed(hashCode());
		int w = (int) (width * 0.8);
		int h = (int) (height * 0.8);
		int x = xPos + (width)/2;
		int y = yPos + (height)/2;
		
		Minecraft.getInstance().getTextureManager().bind(res);
		for(int i=0;i<parts;i++)
		{
			GlStateManager._pushMatrix();
			int ws = w / 4;
			int hs = h / 4;
			GlStateManager._translated(x, y, 0);
			GlStateManager._rotatef(r.nextFloat()*360-90F, 0, 0, 1);
			AbstractGui.blit(matrixStack, -ws*2, -hs*2, ws, hs, 0, 0, 1, 1, 1, 1);
			GlStateManager._popMatrix();
		}
		super.render(matrixStack, mouseX, mouseY);
	}
}
