package futurepack.client.sos;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.util.ResourceLocation;

public class AnimationPicture extends AnimationBase
{

	private int time;
	private ResourceLocation res;
	protected long ticks = -1;
	
	public AnimationPicture(ResourceLocation loc, int time)
	{
		this.time = time;
		res = loc;
	}
		
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		if(ticks==-1)
		{
			ticks = System.currentTimeMillis();
		}
		
		int w = 48;
		int h = 48;
		int x = xPos + (width - w)/2;
		int y = yPos + (height - h)/2;
		
		Minecraft.getInstance().getTextureManager().bind(res);
		
		AbstractGui.blit(matrixStack, x, y, w, h, 0, 0, 1, 1, 1, 1);
	}

	@Override
	public boolean isFinished()
	{
		return ticks!=-1 && System.currentTimeMillis() - ticks >= time;
	}

}
