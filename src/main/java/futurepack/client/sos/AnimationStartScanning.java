package futurepack.client.sos;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.resources.I18n;

public class AnimationStartScanning extends AnimationBase
{
	private boolean finished = false;
	private Runnable func;
	
	public AnimationStartScanning(Runnable func)
	{
		this.func = func;
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		String s = I18n.get("gui.scanner.start.name");
		FontRenderer font = Minecraft.getInstance().font;
		int w = font.width(s)+4;
		int h = 6 + font.lineHeight;
		int x = xPos + (width -w)/2;
		int y = yPos + width/2 - h/2;
				
		HelperComponent.drawRoundButton(matrixStack, mouseX, mouseY, x, y, w, h);
		font.draw(matrixStack, s, x+2, y+3, 0xffffff);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button)
	{
		if(button == 0)
		{
			String s = I18n.get("gui.scanner.start.name");
			FontRenderer font = Minecraft.getInstance().font;
			int w = font.width(s)+4;
			int h = 6 + font.lineHeight;
			int x = xPos + (width -w)/2;
			int y = yPos + width/2 - h/2;
			if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+w, y+h))
			{
				finished=true;
				if(func!=null)
				{
					func.run();
					return true;
				}
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}
	
	@Override
	public boolean isFinished()
	{
		return finished;
	}

}
