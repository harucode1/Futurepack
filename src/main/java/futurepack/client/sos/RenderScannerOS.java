package futurepack.client.sos;

import java.util.LinkedList;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import futurepack.depend.api.EnumScannerState;
import net.minecraft.client.gui.IGuiEventListener;
import net.minecraft.util.ResourceLocation;

//hehe: render SOS
public class RenderScannerOS implements IGuiEventListener
{
	private EnumScannerState state;
	private int xPos,yPos;
	private int width, height;
	
	
	private LinkedList<AnimationBase> buffer = new LinkedList<AnimationBase>();
	private AnimationBase currentAnimation;
	
	
	public RenderScannerOS(int width, int height)
	{
		this.width = width;
		this.height = height;
		buffer.add(new AnimationStartOS());
	}
	
	public void setPosition(int x, int y)
	{
		xPos = x;
		yPos = y;
	}
	
	public void render(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		setupAnimation();
		
		if(currentAnimation!=null)
		{
			currentAnimation.render(matrixStack, mouseX, mouseY);
		}
	}
	
	private void setupAnimation()
	{
		if(currentAnimation!=null)
		{
			if(currentAnimation.isFinished() && !buffer.isEmpty())
			{
				currentAnimation = null;
			}
		}
		if(currentAnimation==null)
		{
			currentAnimation = buffer.removeFirst();
			currentAnimation.init(xPos, yPos, width, height);
		}
	}

	public void addAnimation(AnimationBase animationScanning)
	{
		buffer.add(animationScanning);
	}
	
	public void setupScanning(Runnable r)
	{
		addAnimation(new AnimationStartScanning(r));
		addAnimation(new AnimationScanning());
	}
	
	
	private ResourceLocation hekchen = new ResourceLocation(Constants.MOD_ID, "textures/os/hak_grun.png");
	private ResourceLocation sterne = new ResourceLocation(Constants.MOD_ID, "textures/os/stern.png");
	private ResourceLocation xgelb = new ResourceLocation(Constants.MOD_ID, "textures/os/x_gelb.png");
	private ResourceLocation xrot = new ResourceLocation(Constants.MOD_ID, "textures/os/x_rot.png");
	private ResourceLocation frage = new ResourceLocation(Constants.MOD_ID, "textures/os/fragez.png");
//	private ResourceLocation part = new ResourceLocation(Constants.MOD_ID, "textures/os/asp_fehlt.png");
	
	public void setState(EnumScannerState state, TileEntityScannerBlock data)
	{
		switch (state)
		{
		case MissingBasses://fragezeichen
			addAnimation(new AnimationPicture(frage, 2000));
			break;
		case Error: //blue screen
			addAnimation(new AnimationError());
			break;
		case ResearchedEverything://grünes heckschen
			addAnimation(new AnimationPicture(hekchen, 2000));
			break;
		case Succses: //stern
			addAnimation(new AnimationPicture(sterne, 2000));
			break;
		case WrongAspects: //gelbes kreuz
			addAnimation(new AnimationPicture(xgelb, 2000));
			break;
		case WrongItem: //rotes kreuz
			addAnimation(new AnimationPicture(xrot, 2000));
			break;
		case Partwise:
			addAnimation(new AnimationPusle(2000, data));
			break;
			
		default:
			break;
		}
		
		this.state = state;
	}

	@Override
	public void mouseMoved(double p_212927_1_, double p_212927_3_) 
	{
		if(currentAnimation!=null)
			currentAnimation.mouseMoved(p_212927_1_, p_212927_3_);
	}

	@Override
	public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) 
	{
		if(currentAnimation!=null)
			return currentAnimation.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
		else
			return false;
	}

	@Override
	public boolean mouseReleased(double p_mouseReleased_1_, double p_mouseReleased_3_, int p_mouseReleased_5_) 
	{
		if(currentAnimation!=null)
			return currentAnimation.mouseReleased(p_mouseReleased_1_, p_mouseReleased_3_, p_mouseReleased_5_);
		else
			return false;
	}

	@Override
	public boolean mouseDragged(double p_mouseDragged_1_, double p_mouseDragged_3_, int p_mouseDragged_5_, double p_mouseDragged_6_, double p_mouseDragged_8_) 
	{
		if(currentAnimation!=null)
			return currentAnimation.mouseDragged(p_mouseDragged_1_, p_mouseDragged_3_, p_mouseDragged_5_, p_mouseDragged_6_, p_mouseDragged_8_);
		else
			return false;
	}

	@Override
	public boolean mouseScrolled(double p_mouseScrolled_1_, double p_mouseScrolled_3_, double p_mouseScrolled_5_) 
	{
		if(currentAnimation!=null)
			return currentAnimation.mouseScrolled(p_mouseScrolled_1_, p_mouseScrolled_3_, p_mouseScrolled_5_);
		else
			return false;
	}

	@Override
	public boolean keyPressed(int p_keyPressed_1_, int p_keyPressed_2_, int p_keyPressed_3_) 
	{
		if(currentAnimation!=null)
			return currentAnimation.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_);
		else
			return false;
	}

	@Override
	public boolean keyReleased(int p_223281_1_, int p_223281_2_, int p_223281_3_) 
	{
		if(currentAnimation!=null)
			return currentAnimation.keyReleased(p_223281_1_, p_223281_2_, p_223281_3_);
		else
			return false;
	}

	@Override
	public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) 
	{
		if(currentAnimation!=null)
			return currentAnimation.charTyped(p_charTyped_1_, p_charTyped_2_);
		else
			return false;
	}

	@Override
	public boolean changeFocus(boolean p_changeFocus_1_) 
	{
		if(currentAnimation!=null)
			return currentAnimation.changeFocus(p_changeFocus_1_);
		else
			return false;
	}

	@Override
	public boolean isMouseOver(double p_isMouseOver_1_, double p_isMouseOver_3_) 
	{
		if(currentAnimation!=null)
		return currentAnimation.isMouseOver(p_isMouseOver_1_, p_isMouseOver_3_);
		else
			return false;
	}
}
