package futurepack.client.sos;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.util.ResourceLocation;

public class AnimationScanning extends AnimationBase
{

	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/os/lupe64.png");
	
	private long ticks = -1;
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		if(ticks==-1)
		{
			ticks = System.currentTimeMillis();
		}
		GL11.glPushMatrix();
		
		Minecraft.getInstance().getTextureManager().bind(res);
		
		int x= xPos + (83-48)/2;
		int y= yPos + (80-48)/2;
		double pi = 2*Math.PI * (System.currentTimeMillis()-ticks)/1000D;
		GL11.glTranslated((Math.sin(pi) *10), (Math.cos(pi) *10), 0);
		
		AbstractGui.blit(matrixStack, x, y, 48, 48, 0, 0, 48, 48, 48, 48);
		GL11.glPopMatrix();
	}

	@Override
	public boolean isFinished()
	{
		return ticks!=-1 && System.currentTimeMillis()-ticks >= 1200;
	}

}
