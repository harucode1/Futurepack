package futurepack.client.sos;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.util.ResourceLocation;

public class AnimationStartOS extends AnimationBase
{
	private ResourceLocation scrench = new ResourceLocation(Constants.MOD_ID, "textures/os/s_os.png");
	
	private long ticks = -1;
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		AbstractGui.fill(matrixStack, xPos, yPos, xPos+width, yPos+height, 0xff000000);
		
		if(ticks==-1)
		{
			ticks = System.currentTimeMillis();
		}
		Minecraft.getInstance().getTextureManager().bind(scrench);
		
		double a = Math.sin(Math.PI * (System.currentTimeMillis()-ticks) / 2500.0 );
//		a+=1;
//		a *= 0.5;
		GL11.glColor4d(a, a, a, 1);
		int x = (int) (xPos + (width -width*0.75)/2 );
		int y = (int) (yPos + (height -height*0.75*3/4)/2 );
		AbstractGui.blit(matrixStack, x, y,(int)(width * 0.75),  (int)(height*0.75 * 3/4),  0,0, 1,1, 1, 1);
//		Gui.blit(matrixStack, x, y, 64, 48, width/2, height/2, 64, 48);	
	}

	@Override
	public boolean isFinished()
	{
		return ticks!=-1 && System.currentTimeMillis()-ticks >= 2500;
	}

}
