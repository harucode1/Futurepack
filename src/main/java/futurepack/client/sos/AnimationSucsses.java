
package futurepack.client.sos;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.util.ResourceLocation;

public class AnimationSucsses extends AnimationBase
{

	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/os/hak_grun.png");
	
	private long ticks = -1;
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		if(ticks==-1)
		{
			ticks = System.currentTimeMillis();
		}

		Minecraft.getInstance().getTextureManager().bind(res);
		
		int x= xPos + (83-48)/2;
		int y= yPos + (80-48)/2;

		AbstractGui.blit(matrixStack, x, y, 0, 0,48,48, 48, 48, 48, 48);

	}

	@Override
	public boolean isFinished()
	{
		return ticks!=-1 && System.currentTimeMillis()-ticks >= 1000;
	}

}

