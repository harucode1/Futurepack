package futurepack.client.particle;

import java.util.ArrayList;

import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import net.minecraft.client.particle.IParticleRenderType;
import net.minecraft.client.particle.TexturedParticle;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3d;

public class ParticlePathSearch extends TexturedParticle
{
	private Vector3d[] path;
	private int pointer = 0;
	
	protected ParticlePathSearch(ClientWorld worldIn, double posXIn, double posYIn, double posZIn, Vector3d[] coords)
	{
		super(worldIn, posXIn, posYIn, posZIn);		
		path = coords;
		pointer=path.length-1;
		setPos(path[pointer].x, path[pointer].y, path[pointer].z);
		pointer--;
		lifetime=20*60*2;
	}
	
	public ParticlePathSearch(ClientWorld worldIn, ParentCoords coords)
	{
		this(worldIn, coords.getX(), coords.getY(), coords.getZ(), compressPath(coords));		
	}

	
	private static Vector3d[] compressPath(ParentCoords coords)
	{
		ArrayList<Vector3d> posList = new ArrayList<Vector3d>();
		Direction face = null;
		ParentCoords pos = coords;
		while(pos!=null)
		{
			ParentCoords next = pos.getParent();
			if(next!=null)
			{
				Direction newF = FacingUtil.getSide(pos, next);
				if(newF != face)
				{
					posList.add(new Vector3d(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5));
					face = newF;
				}
			}
			else
			{
				posList.add(new Vector3d(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5));
			}
			pos = next;
		}		
		return posList.toArray(new Vector3d[posList.size()]);
	}


	
	@Override
	public void tick()
	{
		if(pointer < 0)
		{
			remove();
		}
		else
		{
			Vector3d v1 = path[pointer];
			
			if(new Vector3d(x, y, z).subtract(v1).length() < 0.09 )
			{
				pointer--;
			}
			Vector3d v2 = path[pointer+1];
			
			Vector3d mot = v1.subtract(v2).normalize();
			xd = mot.x * 0.06;
			yd = mot.y * 0.06;
			zd = mot.z * 0.06;			
		}
		
		super.tick();
	}

	@Override
	public IParticleRenderType getRenderType() 
	{
		return IParticleRenderType.PARTICLE_SHEET_OPAQUE;
	}

	@Override
	protected float getU0() 
	{
		return 0;
	}

	@Override
	protected float getU1() 
	{
		return 0;
	}

	@Override
	protected float getV0() 
	{
		return 0;
	}

	@Override
	protected float getV1() 
	{
		return 0;
	}
}
