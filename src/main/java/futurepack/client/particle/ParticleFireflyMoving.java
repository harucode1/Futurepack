package futurepack.client.particle;

import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.extensions.albedo.LightList;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.math.vector.Vector3d;

public class ParticleFireflyMoving extends ParticleFireflyStill
{
	
	//int[] colors = new int[]{0x00ff00, 0xffff00};
	//int pos;
	///int ticks, maxticks;
	
	double fx, fy, fz;
	
	
	public ParticleFireflyMoving(ClientWorld worldIn, double posXIn, double posYIn, double posZIn)
	{
		this(worldIn, posXIn, posYIn, posZIn, 1, 1, 1);
	}
	
	public ParticleFireflyMoving(ClientWorld worldIn, double posXIn, double posYIn, double posZIn, double xSpeedIn, double ySpeedIn, double zSpeedIn)
	{
		super(worldIn, posXIn, posYIn, posZIn, xSpeedIn, ySpeedIn, zSpeedIn);
		fx=posXIn; fy=posYIn; fz=posZIn;
		noMove=false;
	}

	@Override
	public void tick()
	{
		Vector3d vec3 = new Vector3d(fx-this.x, fy-this.y, fz-this.z);
		Vector3d mot = new Vector3d(this.xd, this.yd, this.zd);
		if(vec3.lengthSqr() > 2)
		{		
			Vector3d rel = mot.yRot(0.5F * gCol);
			double val = getDegree(vec3, mot);
			if(val > Math.toRadians(30))
			{
				if(getDegree(vec3, rel)<val)
				{
					rel = mot.xRot(0.1F);
				}
				xd=rel.x;
				yd=rel.y;
				zd=rel.z;
			}
			
		}
		
		super.tick();
	}

	@Override
	public void render(IVertexBuilder buffer, ActiveRenderInfo renderInfo, float partialTicks) 
	{
	    float age = (this.age+partialTicks)/this.lifetime*7F;
        setAlpha(sin(age));
	    super.render(buffer, renderInfo, partialTicks);
	    LightList.addLight((float)x, (float)y, (float)z, rCol, gCol, bCol, alpha, 2.5F*alpha);
	}
	
	private double getDegree(Vector3d vec1, Vector3d vec2)
	{
		return Math.acos(vec1.dot(vec2)/(vec1.length()*vec2.length()));
	}
}
