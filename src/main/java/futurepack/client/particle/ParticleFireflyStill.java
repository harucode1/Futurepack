package futurepack.client.particle;

import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.client.ClientEventsModBus;
import net.minecraft.client.particle.IParticleRenderType;
import net.minecraft.client.particle.TexturedParticle;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;

//@ TODO: OnlyIn(Dist.CLIENT)
public class ParticleFireflyStill extends TexturedParticle
{
	int[] colors = new int[]{0x00ff00, 0xffff00};
	int pos;
	int ticks, maxticks;
	boolean noMove;
	
	private TextureAtlasSprite sprite;
	
	public ParticleFireflyStill(ClientWorld w, double xCoordIn, double yCoordIn, double zCoordIn, double xSpeedIn, double ySpeedIn, double zSpeedIn)
	{
		super(w, xCoordIn, yCoordIn, zCoordIn, xSpeedIn, ySpeedIn, zSpeedIn);
		noMove=true;
		ticks=0;
		maxticks=20;
		pos=0;
		lifetime = 200;
		scale(1.5F);
		gravity=0;
		
		int color1 = colors[pos];
		int color2 = colors[(pos+1)%colors.length];
		initRandomColor(color1, color2);
		
		sprite = ClientEventsModBus.particles.getSprite(new ResourceLocation("minecraft", "particle/generic_0"));
	}
	
	public void initRandomColor(int color1, int color2)
	{
		double progress = random.nextDouble();
		
		color1 &= getWhite(progress);
		color2 &= getWhite(1F - progress);
		
		int end = color1 | color2;
		
		setColor(end);
	}
	
	public ParticleFireflyStill(ClientWorld w, double x, double y, double z)
	{
		this(w,x,y,z,0,0,0);
	}
	
	private static int getWhite(double brightness)
	{
		int col = (int) (255 * brightness);
		int color = col | (col<<8) | (col<<16);		
		return Math.abs(color);		
	}
	
	@Override
	public int getLightColor(float f)
	{
		int sky = 15;
		int block = 15;
		return sky<<20 | block<<4;
	}
	
	private void setColor(int color)
	{
		float r = ((color>>16) & 255) /255F;
		float g = ((color>>8) & 255) /255F;
		float b = ((color) & 255) /255F;
		
		setColor(r,g,b) ;
	}
	
	@Override
	public void tick()
	{
		if(noMove)
		{
			xd=0;
			yd=0;
			zd=0;
		}
		super.tick();
	}
	
	@Override
	public void render(IVertexBuilder buffer, ActiveRenderInfo renderInfo, float partialTicks) 
	{
	    float age = (this.age+partialTicks)/this.lifetime*2.5F;
        setAlpha(sin(age));
	    
	    super.render(buffer, renderInfo, partialTicks);
	}
	
	protected float sin(float f)
	{
		float sinn = (float) (Math.sin((f-0.1F)*Math.PI) +0.5);
		sinn = Math.min(1F, sinn);
		sinn = Math.max(0F, sinn);
		return sinn;
	}

	@Override
	public IParticleRenderType getRenderType() 
	{
		return IParticleRenderType.PARTICLE_SHEET_LIT;//test if fireflies work correctly
	}

	@Override
	protected float getU0() 
	{
		return sprite.getU0();
	}

	@Override
	protected float getU1() 
	{
		return sprite.getU1();
	}

	@Override
	protected float getV0() 
	{
		return sprite.getV0();
	}

	@Override
	protected float getV1() 
	{
		return sprite.getV1();
	}

}
