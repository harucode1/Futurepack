package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityCrawler;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;

public class RenderCrawler extends net.minecraft.client.renderer.entity.MobRenderer<EntityCrawler, ModelCrawler>
{
	private static final ResourceLocation TEXTURES = new ResourceLocation(Constants.MOD_ID,"textures/entity/crawler.png");
	private static ModelCrawler childModel = new ModelCrawler();
	private static ModelCrawler normalModel = new ModelCrawler();
	
	public RenderCrawler(EntityRendererManager re)
	{
		super(re, normalModel, 1.0F);
	}
	
	@Override
	protected float getFlipDegrees(EntityCrawler entityLivingBaseIn)
    {
        return 180.0F;
    }
	
	@Override
	public void render(EntityCrawler entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
	{
		matrixStackIn.pushPose();
		if(entity.isBaby())
		{
//			this.entityModel = childModel;
//			matrixStackIn.translate(0, -1.3, 0);
			this.shadowRadius = 0.5F;
		}
		else
		{
//			this.entityModel = normalModel;
			this.shadowRadius = 1F;
		}
		
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		matrixStackIn.popPose();
	}
	
    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	public ResourceLocation getTextureLocation(EntityCrawler entity)
    {
        return TEXTURES;
    }
}
