package futurepack.client.render.entity;

import futurepack.api.Constants;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.ZombieRenderer;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.util.ResourceLocation;

public class RenderCyberZombie extends ZombieRenderer
{
	private static final ResourceLocation ZOMBIE_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/cyberzombie.png");
	
	public RenderCyberZombie(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
	}

	@Override
	public ResourceLocation getTextureLocation(ZombieEntity entity)
    {
        return ZOMBIE_TEXTURES;
    }
}
