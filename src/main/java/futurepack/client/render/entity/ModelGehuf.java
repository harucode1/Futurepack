package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import futurepack.common.entity.living.EntityGehuf;
import net.minecraft.client.renderer.entity.model.AgeableModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModelGehuf extends AgeableModel<EntityGehuf>
{
	//fields
    ModelRenderer body1;
    ModelRenderer body2;
    ModelRenderer legfrontleft;
    ModelRenderer legfrontright;
    ModelRenderer legbackright;
    ModelRenderer legbacktleft;
    ModelRenderer schwanz;
    ModelRenderer halts;
    ModelRenderer head;
    ModelRenderer nose;
  
    ImmutableList<ModelRenderer> parts;
    
    protected float field_78145_g = 8.0F;
    protected float field_78151_h = 4.0F;
    
    private float headRotationAngleX;

    public ModelGehuf()
    {
    	texWidth = 64;
    	texHeight = 32;
    
    	body1 = new ModelRenderer(this, 0, 2);
        body1.addBox(-3F, 0F, -3F, 6, 6, 6);
        body1.setPos(0F, 0F, 0F);
        body1.setTexSize(64, 32);
        body1.mirror = true;
        setRotation(body1, 0F, 0F, 0F);
        body2 = new ModelRenderer(this, 24, 3);
        body2.addBox(-3F, 0F, -3F, 6, 5, 6);
        body2.setPos(6F, 0.5F, 0F);
        body2.setTexSize(64, 32);
        body2.mirror = true;
        setRotation(body2, 0F, 0F, 0F);
        legfrontleft = new ModelRenderer(this, 0, 20);
        legfrontleft.addBox(-1F, 0F, -1F, 2, 10, 2);
        legfrontleft.setPos(-1F, 6F, -2F);
        legfrontleft.setTexSize(64, 32);
        legfrontleft.mirror = true;
        setRotation(legfrontleft, 0F, 0F, 0F);
        legfrontright = new ModelRenderer(this, 8, 20);
        legfrontright.addBox(-1F, 0F, -1F, 2, 10, 2);
        legfrontright.setPos(-1F, 6F, 2F);
        legfrontright.setTexSize(64, 32);
        legfrontright.mirror = true;
        setRotation(legfrontright, 0F, 0F, 0F);
        legbackright = new ModelRenderer(this, 8, 20);
        legbackright.addBox(-1F, 0F, -1F, 2, 10, 2);
        legbackright.setPos(7F, 5.5F, 2F);
        legbackright.setTexSize(64, 32);
        legbackright.mirror = true;
        setRotation(legbackright, 0F, 0F, 0F);
        legbacktleft = new ModelRenderer(this, 0, 20);
        legbacktleft.addBox(-1F, 0F, -1F, 2, 10, 2);
        legbacktleft.setPos(7F, 5.5F, -2F);
        legbacktleft.setTexSize(64, 32);
        legbacktleft.mirror = true;
        setRotation(legbacktleft, 0F, 0F, 0F);
        schwanz = new ModelRenderer(this, 16, 20);
        schwanz.addBox(0F, 0F, -0.5F, 1, 6, 1);
        schwanz.setPos(9F, 2F, 0F);
        schwanz.setTexSize(64, 32);
        schwanz.mirror = true;
        setRotation(schwanz, 0F, 0F, 0F);
        halts = new ModelRenderer(this, 0, 14);
        halts.addBox(-6F, -1.5F, -1.5F, 6, 3, 3);
        halts.setPos(-2F, 4.5F, 0F);
        halts.setTexSize(64, 32);
        halts.mirror = true;
        setRotation(halts, 0F, 0F, 0F);
        head = new ModelRenderer(this, 26, 14);
        head.addBox(-5F, -2F, -2.5F, 5, 5, 5);
        head.setPos(-8F, 4F, 0F);
        head.setTexSize(64, 32);
        head.mirror = true;
        setRotation(head, 0F, 0F, 0F);
        nose = new ModelRenderer(this, 18, 14);
        nose.addBox(-1F, -1F, -1.5F, 1, 2, 3);
        nose.setPos(-13F, 5F, 0F);
        nose.setTexSize(64, 32);
        nose.mirror = true;
        setRotation(nose, 0F, 0F, 0F);
        
        addChild(halts, head);
        nose.x -= halts.x;
        nose.y -= halts.y;
        nose.z -= halts.z;
        addChild(head, nose);
        
        Builder<ModelRenderer> builder = ImmutableList.builder();
		builder.add(body1);
		builder.add(body2);
		builder.add(legfrontleft);
		builder.add(legfrontright);
		builder.add(legbackright);
		builder.add(legbacktleft);
		builder.add(schwanz);
//		builder.add(halts);
		
		this.parts = builder.build();
    }
  
    private void addChild(ModelRenderer base, ModelRenderer child)
    {
    	child.x -= base.x;
    	child.y -= base.y;
    	child.z -= base.z;
    	base.addChild(child);
    }
    
    @Override
	public void setupAnim(EntityGehuf eGehuf, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
    {
        float f6 = (180F / (float)Math.PI);
        this.halts.zRot = -headPitch / f6;
        this.head.yRot = netHeadYaw / f6;
        this.halts.zRot= -this.headRotationAngleX;
        
        this.legfrontleft.zRot = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.legfrontright.zRot = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.legbackright.zRot = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.legbacktleft.zRot = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        
        this.schwanz.zRot = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI / 2) * 0.3F * limbSwingAmount;
    }
    
    @Override
    public void prepareMobModel(EntityGehuf entityGehuf, float limbSwing, float limbSwingAmount, float partialTickTime)
    {
        super.prepareMobModel(entityGehuf, limbSwing, limbSwingAmount, partialTickTime);
        this.headRotationAngleX = entityGehuf.getHeadRotationAngleX(partialTickTime);
    }
  
    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
    	model.xRot = x;
    	model.yRot = y;
    	model.zRot = z;
    }


	@Override
	protected Iterable<ModelRenderer> headParts() 
	{
		return ImmutableList.of(halts);
	}

	@Override
	protected Iterable<ModelRenderer> bodyParts() 
	{
		return parts;
	}
}
