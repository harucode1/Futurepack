package futurepack.client.render.entity;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityHeuler;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;

public class RenderHeuler extends net.minecraft.client.renderer.entity.MobRenderer<EntityHeuler, ModelHeuler>
{	
	private static final ResourceLocation RES = new ResourceLocation(Constants.MOD_ID, "textures/entity/heuler.png");

    public RenderHeuler(EntityRendererManager renderManagerIn)
    {
        super(renderManagerIn, new ModelHeuler(), 0.5F);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
    public ResourceLocation getTextureLocation(EntityHeuler entity)
    {
        return RES;
    }
}
