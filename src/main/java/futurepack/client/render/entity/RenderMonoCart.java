package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.api.Constants;
import futurepack.common.entity.monocart.EntityMonocart;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;

public class RenderMonoCart extends EntityRenderer<EntityMonocart>
{
	private ModelMonoCart model = new ModelMonoCart();
	
	public RenderMonoCart(EntityRendererManager m)
	{
		super(m);
	}
	
	@Override
	public void render(EntityMonocart entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
	{
		entity.getCommandSenderWorld().getProfiler().push("renderMonocart");
		matrixStackIn.pushPose();
		
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		
		IVertexBuilder buf = bufferIn.getBuffer(model.renderType(getTextureLocation(entity)));
		
		float y = 0F;
		if(entity.xRot!=0)
			y+=0.1;
		if(!entity.isRolling() && entity.getPower()<1F)
			y+=0.2;
		matrixStackIn.translate(0, y + 0.65 -0.4, 0);
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(entity.yRot+90));
		matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(entity.xRot));
		matrixStackIn.scale(0.99F, 0.99F, 0.99F);
		model.renderToBuffer(matrixStackIn, buf, packedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
		matrixStackIn.popPose();
		
		entity.getCommandSenderWorld().getProfiler().pop();
	}
	
	
	@Override
	public ResourceLocation getTextureLocation(EntityMonocart entity)
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/monocart.png");
	}

}
