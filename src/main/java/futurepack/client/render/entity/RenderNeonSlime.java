package futurepack.client.render.entity;

import futurepack.api.Constants;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.SlimeRenderer;
import net.minecraft.entity.monster.SlimeEntity;
import net.minecraft.util.ResourceLocation;

public class RenderNeonSlime extends SlimeRenderer 
{
	private static final ResourceLocation SLIME_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/neon_slime.png");


	public RenderNeonSlime(EntityRendererManager renderManagerIn) 
	{
		super(renderManagerIn);
	}

	public ResourceLocation getTextureLocation(SlimeEntity entity) 
	{
		return SLIME_TEXTURES;
	}
}
