package futurepack.client.render.entity;

import futurepack.common.entity.living.EntityEvilRobot;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Hand;
import net.minecraft.util.HandSide;

public class ModelEvilRobot extends BipedModel<EntityEvilRobot>
{
	public ModelEvilRobot()
	{
		this(0.0F, false);
	}

	public ModelEvilRobot(float modelSize, boolean fl)
	{
		super(modelSize, 0.0F, 64, 32);

		if (!fl)
		{
			this.rightArm = new ModelRenderer(this, 40, 16);
			this.rightArm.addBox(-1.0F, -2.0F, -1.0F, 2, 12, 2, modelSize);
			this.rightArm.setPos(-5.0F, 2.0F, 0.0F);
			this.leftArm = new ModelRenderer(this, 40, 16);
			this.leftArm.mirror = true;
			this.leftArm.addBox(-1.0F, -2.0F, -1.0F, 2, 12, 2, modelSize);
			this.leftArm.setPos(5.0F, 2.0F, 0.0F);
			this.rightLeg = new ModelRenderer(this, 0, 16);
			this.rightLeg.addBox(-1.0F, 0.0F, -1.0F, 2, 12, 2, modelSize);
			this.rightLeg.setPos(-2.0F, 12.0F, 0.0F);
			this.leftLeg = new ModelRenderer(this, 0, 16);
			this.leftLeg.mirror = true;
			this.leftLeg.addBox(-1.0F, 0.0F, -1.0F, 2, 12, 2, modelSize);
			this.leftLeg.setPos(2.0F, 12.0F, 0.0F);
		}
	}

	@Override
	public void prepareMobModel(EntityEvilRobot entitylivingbaseIn, float f1, float f2, float partialTickTime)
	{
		this.rightArmPose = BipedModel.ArmPose.EMPTY;
		this.leftArmPose = BipedModel.ArmPose.EMPTY;
		ItemStack itemstack = entitylivingbaseIn.getItemInHand(Hand.MAIN_HAND);
		
		if (itemstack != null && itemstack.getItem() == Items.BOW && (entitylivingbaseIn).isAggressive())
		{
			if (entitylivingbaseIn.getMainArm() == HandSide.RIGHT)
			{
				this.rightArmPose = BipedModel.ArmPose.BOW_AND_ARROW;
			}
			else
			{
				this.leftArmPose = BipedModel.ArmPose.BOW_AND_ARROW;
			}
		}
		
		super.prepareMobModel(entitylivingbaseIn, f1, f2, partialTickTime);
	}

// FIXME: maybe evil robot no longer renders items in hand
//	@Override
//	public void postRenderArm(float scale, HandSide side)
//	{
//		float f = side == HandSide.RIGHT ? 1.0F : -1.0F;
//		ModelRenderer modelrenderer = this.getArmForSide(side);
//		modelrenderer.rotationPointX += f;
//		modelrenderer.postRender(scale);
//		modelrenderer.rotationPointX -= f;
//	}
}
