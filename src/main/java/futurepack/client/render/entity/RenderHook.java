package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.common.entity.throwable.EntityHook;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.culling.ClippingHelper;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.SpriteRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3d;

public class RenderHook extends SpriteRenderer<EntityHook>
{
	public RenderHook(EntityRendererManager renderManagerIn, ItemRenderer itemRendererIn)
	{
		super(renderManagerIn, itemRendererIn);
	}

	@Override
	public boolean shouldRender(EntityHook livingEntity, ClippingHelper camera, double camX, double camY, double camZ)
	{
		EntityHook hok = livingEntity;
		if(hok.getClientThrower()!=null)
			return true;
		return super.shouldRender(livingEntity, camera, camX, camY, camZ);
	}
	
	@Override
	public void render(EntityHook hook, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) {
		
		super.render(hook, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);

		LivingEntity liv = (LivingEntity) hook.getOwner();
//		
//		GL11.glDisable(GL11.GL_TEXTURE_2D);
//		GL11.glDisable(GL11.GL_LIGHTING);
//		GL11.glColor4f(0,1,0, 1);
//		GL11.glLineWidth(5F);
//		
//		GL11.glBegin(GL11.GL_LINES);
//		if(liv!=null)
//			GL11.glVertex3d(liv.posX,liv.posY,liv.posZ);
//		else
//			GL11.glVertex3d(0,0,0);
//		GL11.glVertex3d(x,y,z);
//		GL11.glEnd();
//		
//		GL11.glLineWidth(1F);
//		GL11.glEnable(GL11.GL_TEXTURE_2D);
//		GL11.glEnable(GL11.GL_LIGHTING);
		
		if (liv != null)
		{
			//THIS is a copy of the FishingBobber vanilla renderer
			
			
			
			matrixStackIn.pushPose();
			int i = liv.getMainArm() == HandSide.RIGHT ? 1 : -1;
			ItemStack itemstack = liv.getMainHandItem();
			if (!(itemstack.getItem() instanceof net.minecraft.item.FishingRodItem)) {
				i = -i;
			}

			float f_swing = liv.getAttackAnim(partialTicks);
			float f1_swong_degrees = MathHelper.sin(MathHelper.sqrt(f_swing) * (float)Math.PI);
			float f2_yaw = MathHelper.lerp(partialTicks, liv.yBodyRotO, liv.yBodyRot) * ((float)Math.PI / 180F);
			double d0 = (double)MathHelper.sin(f2_yaw);
			double d1 = (double)MathHelper.cos(f2_yaw);
			double d2 = (double)i * 0.35D;
			double d3 = 0.8D;
			double d4_x;
			double d5_y;
			double d6_z;
			float f3_y;
			if ((this.entityRenderDispatcher.options == null || this.entityRenderDispatcher.options.getCameraType().isFirstPerson() ) && liv == Minecraft.getInstance().player) 
			{
				double d7 = this.entityRenderDispatcher.options.fov;
				d7 = d7 / 100.0D;
				Vector3d Vector3d = new Vector3d((double)i * -0.36D * d7, -0.045D * d7, 0.4D);
				Vector3d = Vector3d.xRot(-MathHelper.lerp(partialTicks, liv.xRotO, liv.xRot) * ((float)Math.PI / 180F));
				Vector3d = Vector3d.yRot(-MathHelper.lerp(partialTicks, liv.yRotO, liv.yRot) * ((float)Math.PI / 180F));
				Vector3d = Vector3d.yRot(f1_swong_degrees * 0.5F);
				Vector3d = Vector3d.xRot(-f1_swong_degrees * 0.7F);
				d4_x = MathHelper.lerp((double)partialTicks, liv.xo, liv.getX()) + Vector3d.x;
				d5_y = MathHelper.lerp((double)partialTicks, liv.yo, liv.getY()) + Vector3d.y;
				d6_z = MathHelper.lerp((double)partialTicks, liv.zo, liv.getZ()) + Vector3d.z;
				f3_y = liv.getEyeHeight();
			}
			else
			{
				d4_x = MathHelper.lerp((double)partialTicks, liv.xo, liv.getX()) - d1 * d2 - d0 * d3;
				d5_y = liv.yo + (double)liv.getEyeHeight() + (liv.getY() - liv.yo) * (double)partialTicks - 0.45D;
				d6_z = MathHelper.lerp((double)partialTicks, liv.zo, liv.getZ()) - d0 * d2 + d1 * d3;
				f3_y = liv.isCrouching() ? -0.1875F : 0.0F;
			}

			double d9_x = MathHelper.lerp((double)partialTicks, hook.xo, hook.getX());
			double d10_y = MathHelper.lerp((double)partialTicks, hook.yo, hook.getY()) + 0.25D;
			double d8_z = MathHelper.lerp((double)partialTicks, hook.zo, hook.getZ());
			float f4_x = (float)(d4_x - d9_x);
			float f5_y = (float)(d5_y - d10_y) + f3_y;
			float f6_z = (float)(d6_z - d8_z);
			IVertexBuilder ivertexbuilder1 = bufferIn.getBuffer(RenderType.lines());
			Matrix4f matrix4f1 = matrixStackIn.last().pose();
			int j = 16;

			for(int k = 0; k < j; ++k) 
			{
				stringVertex(f4_x, f5_y, f6_z, ivertexbuilder1, matrix4f1, fraction(k, j));
				stringVertex(f4_x, f5_y, f6_z, ivertexbuilder1, matrix4f1, fraction(k + 1, j));
			}

			matrixStackIn.popPose();
		}
	}
	
	private static float fraction(int p_229105_0_, int p_229105_1_) {
		return (float)p_229105_0_ / (float)p_229105_1_;
	}

	//changed the color here from black to green
	private static void stringVertex(float p_229104_0_, float p_229104_1_, float p_229104_2_, IVertexBuilder p_229104_3_, Matrix4f p_229104_4_, float p_229104_5_) {
		p_229104_3_.vertex(p_229104_4_, p_229104_0_ * p_229104_5_, p_229104_1_ * (p_229104_5_ * p_229104_5_ + p_229104_5_) * 0.5F + 0.25F, p_229104_2_ * p_229104_5_).color(0, 190, 0, 255).endVertex();
	}

}
