package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import futurepack.common.entity.EntityForstmaster;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;

/**
 * Forstmaster - Mantes
 * Created using Tabula 5.1.0
 */
public class ModelForstmaster extends SegmentedModel<EntityForstmaster>
{
    public ModelRenderer Ramen1;
    public ModelRenderer Ramen2;
    public ModelRenderer Ramen3;
    public ModelRenderer Ramen6;
    public ModelRenderer Sagearm2;
    public ModelRenderer Sagearm1;
    public ModelRenderer Triebwerk2;
    public ModelRenderer Triebwerk1;
    public ModelRenderer Ramen52;
    public ModelRenderer Ramen5;
    public ModelRenderer Ramen4;
    public ModelRenderer Ramen42;
    public ModelRenderer Sagekasten134;
    public ModelRenderer Sagekasten212;
    public ModelRenderer Kopf;
    public ModelRenderer Sezlingsbox;
    public ModelRenderer Luke1;
    public ModelRenderer Luke2;
    public ModelRenderer Sageblatt1;
    public ModelRenderer Sageblatt2;
    public ModelRenderer Sageblatt3;
    public ModelRenderer Sageblatt4;

	private ImmutableList<ModelRenderer> parts;
	
    public ModelForstmaster()
    {
        this.texWidth = 128;
        this.texHeight = 128;
        this.Sagekasten134 = new ModelRenderer(this, 0, 28);
        this.Sagekasten134.setPos(-2.0F, 8.0F, -6.0F);
        this.Sagekasten134.addBox(-5.0F, -1.0F, -3.0F, 5, 3, 3, 0.0F);
        this.setRotateAngle(Sagekasten134, 0.0F, -0.7853981852531433F, 0.0F);
        this.Ramen2 = new ModelRenderer(this, 22, 10);
        this.Ramen2.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen2.addBox(4.0F, 5.0F, -5.0F, 1, 2, 8, 0.0F);
        this.setRotateAngle(Ramen2, 5.890622339648657E-16F, 3.4103603158333183E-16F, -4.650491460089887E-16F);
        this.Ramen4 = new ModelRenderer(this, 41, 3);
        this.Ramen4.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen4.addBox(1.0F, 4.5F, 3.0F, 1, 3, 3, 0.0F);
        this.Sageblatt3 = new ModelRenderer(this, 47, 17);
        this.Sageblatt3.setPos(-3.5F, 9.0F, -11.0F);
        this.Sageblatt3.addBox(-1.5F, -0.6000000238418579F, -1.5F, 3, 1, 3, 0.0F);
        this.Sagearm2 = new ModelRenderer(this, 29, 21);
        this.Sagearm2.setPos(3.0F, 6.0F, -4.0F);
        this.Sagearm2.addBox(0.0F, 0.0F, -1.0F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Sagearm2, -1.082827091217041F, 2.480262023815341E-16F, 0.0F);
        this.Ramen5 = new ModelRenderer(this, 41, 3);
        this.Ramen5.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen5.addBox(-2.0F, 4.5F, 3.0F, 1, 3, 3, 0.0F);
        this.Luke1 = new ModelRenderer(this, 0, 17);
        this.Luke1.setPos(-3.0F, 7.5F, 0.0F);
        this.Luke1.addBox(0.0F, 0.0F, -3.0F, 3, 1, 6, 0.0F);
        this.Ramen6 = new ModelRenderer(this, 52, 4);
        this.Ramen6.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen6.addBox(-1.0F, 5.0F, 4.0F, 2, 2, 1, 0.0F);
        this.Ramen42 = new ModelRenderer(this, 41, 3);
        this.Ramen42.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen42.addBox(4.0F, 4.5F, 3.0F, 1, 3, 3, 0.0F);
        this.Sageblatt2 = new ModelRenderer(this, 47, 17);
        this.Sageblatt2.setPos(3.5F, 8.0F, -11.0F);
        this.Sageblatt2.addBox(-1.5F, 0.0F, -1.5F, 3, 1, 3, 0.0F);
        this.setRotateAngle(Sageblatt2, 0.0F, 0.7853981852531433F, 0.0F);
        this.Ramen52 = new ModelRenderer(this, 41, 3);
        this.Ramen52.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen52.addBox(-5.0F, 4.5F, 3.0F, 1, 3, 3, 0.0F);
        this.Sagekasten212 = new ModelRenderer(this, 0, 28);
        this.Sagekasten212.setPos(2.0F, 8.0F, -6.0F);
        this.Sagekasten212.addBox(0.0F, -1.0F, -3.0F, 5, 3, 3, 0.0F);
        this.setRotateAngle(Sagekasten212, 0.0F, 0.7853981852531433F, 0.0F);
        this.Sagearm1 = new ModelRenderer(this, 29, 21);
        this.Sagearm1.setPos(-3.0F, 6.0F, -4.0F);
        this.Sagearm1.addBox(-1.0F, 0.0F, -1.0F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Sagearm1, -1.082827091217041F, 2.480262023815341E-16F, 0.0F);
        this.Kopf = new ModelRenderer(this, 25, 0);
        this.Kopf.setPos(0.0F, 0.0F, 0.0F);
        this.Kopf.addBox(-2.0F, 4.0F, -8.0F, 4, 4, 4, 0.0F);
        this.Ramen1 = new ModelRenderer(this, 22, 10);
        this.Ramen1.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen1.addBox(-5.0F, 5.0F, -5.0F, 1, 2, 8, 0.0F);
        this.Luke2 = new ModelRenderer(this, 0, 17);
        this.Luke2.setPos(3.0F, 7.5F, -3.0F);
        this.Luke2.addBox(-3.0F, 0.0F, 0.0F, 3, 1, 6, 0.0F);
        this.Triebwerk2 = new ModelRenderer(this, 51, 11);
        this.Triebwerk2.setPos(-3.0F, 6.0F, 4.0F);
        this.Triebwerk2.addBox(-1.0F, -1.0F, -1.0F, 2, 2, 2, 0.0F);
        this.setRotateAngle(Triebwerk2, 0.7853981852531433F, 4.960524047630682E-16F, -3.72039316807191E-16F);
        this.Triebwerk1 = new ModelRenderer(this, 51, 11);
        this.Triebwerk1.setPos(3.0F, 6.0F, 4.0F);
        this.Triebwerk1.addBox(-1.0F, -1.0F, -1.0F, 2, 2, 2, 0.0F);
        this.setRotateAngle(Triebwerk1, 0.7853981852531433F, 4.960524047630682E-16F, -3.72039316807191E-16F);
        this.Sezlingsbox = new ModelRenderer(this, 0, 0);
        this.Sezlingsbox.setPos(0.0F, 2.0F, 0.0F);
        this.Sezlingsbox.addBox(-4.0F, 0.0F, -4.0F, 8, 6, 8, 0.0F);
        this.Ramen3 = new ModelRenderer(this, 41, 0);
        this.Ramen3.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen3.addBox(-4.0F, 5.0F, -5.0F, 8, 2, 1, 0.0F);
        this.Sageblatt4 = new ModelRenderer(this, 47, 17);
        this.Sageblatt4.setPos(-3.5F, 8.0F, -11.0F);
        this.Sageblatt4.addBox(-1.5F, 0.0F, -1.5F, 3, 1, 3, 0.0F);
        this.setRotateAngle(Sageblatt4, 0.0F, 0.7853981852531433F, 0.0F);
        this.Sageblatt1 = new ModelRenderer(this, 47, 17);
        this.Sageblatt1.setPos(3.5F, 9.0F, -11.0F);
        this.Sageblatt1.addBox(-1.5F, -0.6000000238418579F, -1.5F, 3, 1, 3, 0.0F);
        
        Builder<ModelRenderer> builder = ImmutableList.builder();
        builder.add(Ramen1);
        builder.add(Ramen2);
        builder.add(Ramen3);
        builder.add(Ramen6);
        builder.add(Sagearm2);
        builder.add(Sagearm1);
        builder.add(Triebwerk2);
        builder.add(Triebwerk1);
        builder.add(Ramen52);
        builder.add(Ramen5);
        builder.add(Ramen4);
        builder.add(Ramen42);
        builder.add(Sagekasten134);
        builder.add(Sagekasten212);
        builder.add(Kopf);
        builder.add(Sezlingsbox);
        builder.add(Luke1);
        builder.add(Luke2);
        builder.add(Sageblatt1);
        builder.add(Sageblatt2);
        builder.add(Sageblatt3);
        builder.add(Sageblatt4);
        this.parts = builder.build();
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z)
    {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }


	@Override
	public Iterable<ModelRenderer> parts() {
		return this.parts;
	}


	@Override
	public void setupAnim(EntityForstmaster entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {		
	}
}
