package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.api.Constants;
import futurepack.common.entity.EntityForceField;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.vector.Matrix3f;
import net.minecraft.util.math.vector.Matrix4f;

public class RenderForceField extends EntityRenderer<EntityForceField>
{
	public static ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/entity/force_field.png");

	public RenderForceField(EntityRendererManager renderManager)
	{
		super(renderManager);
	}
	
	@Override
	public void render(EntityForceField entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
	{
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		
		AxisAlignedBB bb = entity.getBoundingBox().move(-entity.getX(), -entity.getY(), -entity.getZ());
		
		renderBox(bb, matrixStackIn, bufferIn, packedLightIn, OverlayTexture.NO_OVERLAY, 0, 153, 255);
		
	}
	
	@Override
	public ResourceLocation getTextureLocation(EntityForceField entity)
	{		
		return res;
	}
	
	public static void renderBox(AxisAlignedBB boundingBox, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, int overlay, int red, int green, int blue)
    {
		
        float w = (float) (boundingBox.maxX - (float)boundingBox.minX);
        float h = (float) (boundingBox.maxY - (float)boundingBox.minY);
        
        float minU= -w/2;
        float minV= -h/2;
        float maxU= w/2;
        float maxV= h/2;
        
        double offset = (System.currentTimeMillis() % 2300) / 2300D;
        
        minU +=offset;
        minV +=offset;
        maxU +=offset;
        maxV +=offset;
        
        renderSides(bufferIn, matrixStackIn, boundingBox, minU, minV, maxU, maxV, new boolean[]{true, true, true, true, true, true}, red, green, blue, packedLightIn, overlay);
    }
	
	public static void renderSides(IRenderTypeBuffer bufferIn, MatrixStack matrixStackIn, AxisAlignedBB boundingBox, float minU, float minV, float maxU, float maxV, boolean[] sides,int red, int green, int blue, int lightMap, int overlay)
    {		
		IVertexBuilder vertexbuffer = bufferIn.getBuffer(RenderType.entityTranslucentCull(res));
		
		MatrixStack.Entry last = matrixStackIn.last();
		Matrix4f mat = last.pose();
		Matrix3f normals = last.normal();
		
		if(sides[Direction.NORTH.ordinal()])
		{
			//North
			vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.maxY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(minU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 0.0F, -1.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.maxY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(maxU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 0.0F, -1.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.minY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(maxU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 0.0F, -1.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.minY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(minU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 0.0F, -1.0F).endVertex();
		}
		if(sides[Direction.SOUTH.ordinal()])
		{
	        //South
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.minY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(minU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 0.0F, 1.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.minY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(maxU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 0.0F, 1.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.maxY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(maxU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 0.0F, 1.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.maxY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(minU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 0.0F, 1.0F).endVertex();
		}
        if(sides[Direction.DOWN.ordinal()])
		{
	        //Down    
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.minY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(minU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, -1.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.minY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(maxU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, -1.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.minY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(maxU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, -1.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.minY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(minU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, -1.0F, 0.0F).endVertex();
		}
        if(sides[Direction.UP.ordinal()])
		{
	        //Up
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.maxY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(minU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 1.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.maxY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(maxU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 1.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.maxY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(maxU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 1.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.maxY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(minU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 0.0F, 1.0F, 0.0F).endVertex();
		}
        if(sides[Direction.WEST.ordinal()])
		{
	        //West
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.minY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(maxU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, -1.0F, 0.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.maxY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(maxU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, -1.0F, 0.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.maxY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(minU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, -1.0F, 0.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.minX, (float)boundingBox.minY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(minU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, -1.0F, 0.0F, 0.0F).endVertex();
		}
        if(sides[Direction.EAST.ordinal()])
		{
	        //East
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.minY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(minU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 1.0F, 0.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.maxY, (float)boundingBox.minZ).color(red, green, blue, 255).uv(minU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 1.0F, 0.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.maxY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(maxU, maxV).overlayCoords(overlay).uv2(lightMap).normal(normals, 1.0F, 0.0F, 0.0F).endVertex();
	        vertexbuffer.vertex(mat, (float)boundingBox.maxX, (float)boundingBox.minY, (float)boundingBox.maxZ).color(red, green, blue, 255).uv(maxU, minV).overlayCoords(overlay).uv2(lightMap).normal(normals, 1.0F, 0.0F, 0.0F).endVertex();
		}
    }
}
