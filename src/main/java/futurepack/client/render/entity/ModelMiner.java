package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import futurepack.common.entity.EntityMiner;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ModelMiner extends SegmentedModel<EntityMiner>
{
	//fields
    protected ModelRenderer body;
    protected ModelRenderer laser1;
    protected ModelRenderer laser2;
    protected ModelRenderer triebwerk1;
    protected ModelRenderer triebwerk2;
    protected ModelRenderer head;
    
	private ImmutableList<ModelRenderer> parts;
  
    public ModelMiner()
    {
    	texWidth = 64;
    	texHeight = 64;
    
    	body = new ModelRenderer(this, 0, 5);
    	body.addBox(-2F, -2F, -2F, 4, 4, 4);
    	body.setPos(0F, 0F, 0F);
    	body.setTexSize(64, 64);
    	body.mirror = true;
    	setRotation(body, 0F, 0F, 0F);
    	laser1 = new ModelRenderer(this, 8, 13);
    	laser1.addBox(-1F, -1F, -1F, 2, 2, 2);
    	laser1.setPos(2F, 2F, -2F);
    	laser1.setTexSize(64, 64);
    	laser1.mirror = true;
    	setRotation(laser1, 0F, 0F, 0F);
    	laser2 = new ModelRenderer(this, 8, 13);
    	laser2.addBox(-1F, -1F, -1F, 2, 2, 2);
    	laser2.setPos(-2F, 2F, -2F);
    	laser2.setTexSize(64, 64);
    	laser2.mirror = true;
    	setRotation(laser2, 0F, 0F, 0F);
    	triebwerk1 = new ModelRenderer(this, 0, 0);
    	triebwerk1.addBox(-1F, -1F, -1F, 2, 2, 3);
    	triebwerk1.setPos(3F, -1F, 1F);
    	triebwerk1.setTexSize(64, 64);
    	triebwerk1.mirror = true;
    	setRotation(triebwerk1, 0F, 0F, 0F);
    	triebwerk2 = new ModelRenderer(this, 0, 0);
    	triebwerk2.addBox(-1F, -1F, -1F, 2, 2, 3);
    	triebwerk2.setPos(-3F, -1F, 1F);
    	triebwerk2.setTexSize(64, 64);
    	triebwerk2.mirror = true;
    	setRotation(triebwerk2, 0F, 0F, 0F);
    	head = new ModelRenderer(this, 16, 5);
    	head.addBox(-1F, -1F, -1F, 2, 3, 2);
    	head.setPos(0F, 0F, -3F);
    	head.setTexSize(64, 64);
    	head.mirror = true;
    	setRotation(head, 0F, 0F, 0F);
    	
    	Builder<ModelRenderer> builder = ImmutableList.builder();
    	
    	builder.add(body);
    	builder.add(laser1);
    	builder.add(laser2);
    	builder.add(triebwerk1);
    	builder.add(triebwerk2);
    	builder.add(head);
    	
    	this.parts = builder.build();
    }	
  
    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
    	model.xRot = x;
    	model.yRot = y;
    	model.zRot = z;
    }

	@Override
	public Iterable<ModelRenderer> parts() {
		return this.parts;
	}

	@Override
	public void setupAnim(EntityMiner entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {	
	}

}
