package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import futurepack.common.entity.throwable.EntityRocket;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ModelRocket extends SegmentedModel<EntityRocket>
{
	//fields
	ModelRenderer Finne1;
	ModelRenderer Raketenkorpus;
	ModelRenderer Shape1;
	ModelRenderer Spitze2;
	ModelRenderer Finne4;
	ModelRenderer Finne2;
	ModelRenderer Finne3;

	private ImmutableList<ModelRenderer> parts;
	
	public ModelRocket()
	{
		texWidth = 32;
		texHeight = 32;
		
		Finne1 = new ModelRenderer(this, 0, 1);
		Finne1.addBox(1.5F, -0.5F, 7F, 1, 1, 2);
		Finne1.setPos(0F, 0F, 0F);
		Finne1.setTexSize(32, 32);
		Finne1.mirror = true;
		setRotation(Finne1, 0F, 0F, 0F);
		Raketenkorpus = new ModelRenderer(this, 0, 0);
		Raketenkorpus.addBox(-1.5F, -1.5F, 0F, 3, 3, 10);
		Raketenkorpus.setPos(0F, 0F, 0F);
		Raketenkorpus.setTexSize(32, 32);
		Raketenkorpus.mirror = true;
		setRotation(Raketenkorpus, 0F, 0F, 0F);
		Shape1 = new ModelRenderer(this, 17, 1);
		Shape1.addBox(-0.5F, -0.5F, -2F, 1, 1, 1);
		Shape1.setPos(0F, 0F, 0F);
		Shape1.setTexSize(32, 32);
		Shape1.mirror = true;
		setRotation(Shape1, 0F, 0F, 0F);
		Spitze2 = new ModelRenderer(this, 0, 5);
		Spitze2.addBox(-1F, -1F, -1F, 2, 2, 1);
		Spitze2.setPos(0F, 0F, 0F);
		Spitze2.setTexSize(32, 32);
		Spitze2.mirror = true;
		setRotation(Spitze2, 0F, 0F, 0F);
		Finne4 = new ModelRenderer(this, 0, 1);
		Finne4.addBox(-0.5F, 1.5F, 7F, 1, 1, 2);
		Finne4.setPos(0F, 0F, 0F);
		Finne4.setTexSize(32, 32);
		Finne4.mirror = true;
		setRotation(Finne4, 0F, 0F, 0F);
		Finne2 = new ModelRenderer(this, 0, 1);
		Finne2.addBox(1.5F, -0.5F, 7F, 1, 1, 2);
		Finne2.setPos(0F, 0F, 0F);
		Finne2.setTexSize(32, 32);
		Finne2.mirror = true;
		setRotation(Finne2, 0F, 0F, -1.570796F);
		Finne3 = new ModelRenderer(this, 0, 1);
		Finne3.addBox(1.5F, -0.5F, 7F, 1, 1, 2);
		Finne3.setPos(0F, 0F, 0F);
		Finne3.setTexSize(32, 32);
		Finne3.mirror = true;
		setRotation(Finne3, 0F, 0F, -3.141593F);
		
		Builder<ModelRenderer> builder = ImmutableList.builder();
		builder.add(Finne1);
		builder.add(Raketenkorpus);
		builder.add(Shape1);
		builder.add(Spitze2);
		builder.add(Finne2);
		builder.add(Finne3);
		builder.add(Finne4);
		this.parts = builder.build();
	}
	
	/*
	 * FIXME
	@Override
	public void render(EntityRocket entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		super.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
		setRotationAngles(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
		Finne1.render(scale);
		Reketenkorpus.render(scale);
		Shape1.render(scale);
		Spitze2.render(scale);
		Finne4.render(scale);
		Finne2.render(scale);
		Finne3.render(scale);
	}*/
	
	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.xRot = x;
		model.yRot = y;
		model.zRot = z;
	}
	
	@Override
	public void setupAnim(EntityRocket e, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		//FIXME super.setRotationAngles(e, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
	}

	@Override
	public Iterable<ModelRenderer> parts() {
		return this.parts;
	}
}
