package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import futurepack.common.entity.monocart.EntityMonocart;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ModelMonoCart extends SegmentedModel<EntityMonocart>
{
  //fields
    ModelRenderer Streifenleiste1;
    ModelRenderer Streifenleiste2;
    ModelRenderer Boden2;
    ModelRenderer Fahrleiste;
    ModelRenderer Boden1;
    ModelRenderer Cargo;
    ModelRenderer Halter1;
    ModelRenderer Halter2;
    ModelRenderer Halter3;
    ModelRenderer Halter4;

	private ImmutableList<ModelRenderer> parts;
  
  public ModelMonoCart()
  {
    texWidth = 64;
    texHeight = 64;
    
      Streifenleiste1 = new ModelRenderer(this, 0, 30);
      Streifenleiste1.addBox(-1F, 0F, -2F, 1, 1, 4);
      Streifenleiste1.setPos(-7F, 7F, 0F);
      Streifenleiste1.setTexSize(64, 64);
      Streifenleiste1.mirror = true;
      setRotation(Streifenleiste1, 0F, 0F, 0F);
      Streifenleiste2 = new ModelRenderer(this, 0, 30);
      Streifenleiste2.addBox(0F, 0F, -2F, 1, 1, 4);
      Streifenleiste2.setPos(7F, 7F, 0F);
      Streifenleiste2.setTexSize(64, 64);
      Streifenleiste2.mirror = true;
      setRotation(Streifenleiste2, 0F, 0F, 0F);
      Boden2 = new ModelRenderer(this, 0, 22);
      Boden2.addBox(-8F, 0F, -6F, 16, 1, 7);
      Boden2.setPos(0F, 8F, -2F);
      Boden2.setTexSize(64, 64);
      Boden2.mirror = true;
      setRotation(Boden2, 0F, 0F, 0F);
      Fahrleiste = new ModelRenderer(this, 10, 30);
      Fahrleiste.addBox(-7F, 0F, -1F, 14, 2, 2);
      Fahrleiste.setPos(0F, 8F, 0F);
      Fahrleiste.setTexSize(64, 64);
      Fahrleiste.mirror = true;
      setRotation(Fahrleiste, 0F, 0F, 0F);
      Boden1 = new ModelRenderer(this, 0, 22);
      Boden1.addBox(-8F, 0F, -6F, 16, 1, 7);
      Boden1.setPos(0F, 8F, 2F);
      Boden1.setTexSize(64, 64);
      Boden1.mirror = true;
      setRotation(Boden1, 0F, 3.141593F, 0F);
      Cargo = new ModelRenderer(this, 0, 0);
      Cargo.addBox(-7F, 0F, -7F, 14, 8, 14);
      Cargo.setPos(0F, 0F, 0F);
      Cargo.setTexSize(64, 64);
      Cargo.mirror = true;
      setRotation(Cargo, 0F, 0F, 0F);
      Halter1 = new ModelRenderer(this, 46, 22);
      Halter1.addBox(0F, -7F, -2F, 1, 7, 4);
      Halter1.setPos(-8F, 8F, 4F);
      Halter1.setTexSize(64, 64);
      Halter1.mirror = true;
      setRotation(Halter1, 0F, 0F, 0F);
      Halter2 = new ModelRenderer(this, 46, 22);
      Halter2.addBox(0F, -7F, -2F, 1, 7, 4);
      Halter2.setPos(-8F, 8F, -4F);
      Halter2.setTexSize(64, 64);
      Halter2.mirror = true;
      setRotation(Halter2, 0F, 0F, 0F);
      Halter3 = new ModelRenderer(this, 46, 22);
      Halter3.addBox(0F, -7F, -2F, 1, 7, 4);
      Halter3.setPos(7F, 8F, -4F);
      Halter3.setTexSize(64, 64);
      Halter3.mirror = true;
      setRotation(Halter3, 0F, 0F, 0F);
      Halter4 = new ModelRenderer(this, 46, 22);
      Halter4.addBox(0F, -7F, -2F, 1, 7, 4);
      Halter4.setPos(7F, 8F, 4F);
      Halter4.setTexSize(64, 64);
      Halter4.mirror = true;
      setRotation(Halter4, 0F, 0F, 0F);
      
      Builder<ModelRenderer> builder = ImmutableList.builder();
      builder.add(Streifenleiste1);
      builder.add(Streifenleiste2);
      builder.add(Boden2);
      builder.add(Fahrleiste);
      builder.add(Boden1);
      builder.add(Cargo);
      builder.add(Halter1);
      builder.add(Halter2);
      builder.add(Halter3);
      builder.add(Halter4);
      this.parts = builder.build();
  }
  
  /*
   * 
  public void render(float f5, boolean invEmtpy)
  {
    Streifenleiste1.render(f5);
    Streifenleiste2.render(f5);
    Boden2.render(f5);
    Fahrleiste.render(f5);
    Boden1.render(f5);
    if(!invEmtpy)
    	Cargo.render(f5);
    Halter1.render(f5);
    Halter2.render(f5);
    Halter3.render(f5);
    Halter4.render(f5);
  }*/
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.xRot = x;
    model.yRot = y;
    model.zRot = z;
  }

@Override
public Iterable<ModelRenderer> parts() {
	return this.parts;
}

@Override
public void setupAnim(EntityMonocart entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {}
  

}
