package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.api.Constants;
import futurepack.common.entity.EntityMiner;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.BeaconTileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;

//TODO: irgendwas Bug rum wenn man den Mine rund das Waser anzeiht, das wasser wird irgendwie heller
public class RenderMiner extends EntityRenderer<EntityMiner>
{
	public RenderMiner(EntityRendererManager m)
	{
		super(m);
	}

	ModelMiner model = new ModelMiner();
	
	@Override
	public void render(EntityMiner miner, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
	{
		miner.getCommandSenderWorld().getProfiler().push("renderMiner");
		super.render( miner, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		
		matrixStackIn.pushPose();
		matrixStackIn.translate(0.0, 0.3, 0.0);
		matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(180F));
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(180F + miner.yRot));
		
		IVertexBuilder builder = bufferIn.getBuffer(model.renderType(this.getTextureLocation( miner)));
		
		model.triebwerk1.xRot = miner.rot;
		model.triebwerk2.xRot = miner.rot;
		
		model.renderToBuffer(matrixStackIn, builder, packedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
		
		float scale = 1F/4F;
		matrixStackIn.scale(scale, scale, scale);
		//GL11.glRotatef(180F, 0, 0, 1);
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-90F));
		
		matrixStackIn.translate(0.0, 0.5, 0);
		renderBeamSegment(matrixStackIn, bufferIn, partialTicks, miner.level.getGameTime()*2, 0, (int) (miner.dis * 8), new float[] {1F, 0.125F, 0.625F});
		matrixStackIn.translate(-1, 0, 0);
		renderBeamSegment(matrixStackIn, bufferIn, partialTicks, miner.level.getGameTime()*-2, 0, (int) (miner.dis * 8), new float[] {1F, 0.125F, 0.125F});
		
		matrixStackIn.popPose();
		miner.getCommandSenderWorld().getProfiler().pop();
	}
	
	public static void renderBeamSegment(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, float partialTicks, long totalWorldTime, int yOffset, int height, float[] colors) 
	{
		BeaconTileEntityRenderer.renderBeaconBeam(matrixStackIn, bufferIn, BeaconTileEntityRenderer.BEAM_LOCATION, partialTicks, 1.0F, totalWorldTime, yOffset, height, colors, 0.2F, 0.25F);
	}
	
	@Override
	public ResourceLocation getTextureLocation(EntityMiner p_110775_1_) 
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/miner.png");
	}
}
