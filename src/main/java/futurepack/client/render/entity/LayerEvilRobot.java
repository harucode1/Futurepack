package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityEvilRobot;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.util.ResourceLocation;

public class LayerEvilRobot extends LayerRenderer<EntityEvilRobot, ModelEvilRobot>
{

	private static final ResourceLocation TEX_OVER = new ResourceLocation(Constants.MOD_ID, "textures/entity/bot_overlay.png");
	private final LivingRenderer<EntityEvilRobot, ModelEvilRobot> renderer;
	private ModelEvilRobot layerModel;
	
	public LayerEvilRobot(LivingRenderer<EntityEvilRobot, ModelEvilRobot> p_i47131_1_)
	{
		super(p_i47131_1_);
		this.renderer = p_i47131_1_;
		this.layerModel = new ModelEvilRobot(0.25F, true);
	}

	@Override
	public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, EntityEvilRobot entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) 
	{
		if(entitylivingbaseIn.getState().hasArmor)
		{
			coloredCutoutModelCopyLayerRender(this.getParentModel(), this.layerModel, TEX_OVER, matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, partialTicks, 1F, 1F, 1F);
		}
	}

}
