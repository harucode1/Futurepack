package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.common.entity.living.EntityHeuler;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ModelHeuler extends EntityModel<EntityHeuler>
{
	public ModelRenderer Kopf;
	public ModelRenderer Auge1;
	public ModelRenderer Auge2;
	public ModelRenderer Hinterteil1;
	public ModelRenderer Seite1;
	public ModelRenderer Seite2;
	public ModelRenderer Fluegel1;
	public ModelRenderer Fluegel11;
	public ModelRenderer Fluegel2;
	public ModelRenderer Fluegel21;
	public ModelRenderer Hinterteil3;
	public ModelRenderer Hinterteil2;
	
	private ImmutableList<ModelRenderer> parts;

	public ModelHeuler()
	{
		super(RenderType::entityTranslucentCull);
		
		this.texWidth = 128;
		this.texHeight = 128;
		this.Kopf = new ModelRenderer(this, 0, 6);
		this.Kopf.setPos(0.0F, 0.0F, 0.0F);
		this.Kopf.addBox(-1.0F, -1.0F, -2.0F, 2, 2, 2, 0.0F);
		this.setRotateAngle(Kopf, 0.37178611755371094F, -0.0F, 0.0F);
		this.Auge2 = new ModelRenderer(this, 10, 6);
		this.Auge2.setPos(0.0F, 0.0F, 0.0F);
		this.Auge2.addBox(-1.2999999523162842F, -0.5F, -2.5F, 1, 1, 1, 0.0F);
		this.setRotateAngle(Auge2, 0.34906584024429316F, -0.0F, 0.0F);
		this.Hinterteil1 = new ModelRenderer(this, 0, 0);
		this.Hinterteil1.setPos(0.0F, 0.0F, 0.0F);
		this.Hinterteil1.addBox(-1.0F, -1.0F, 0.0F, 2, 2, 3, 0.0F);
		this.Hinterteil3 = new ModelRenderer(this, 37, 0);
		this.Hinterteil3.setPos(0.0F, 0.0F, 5.0F);
		this.Hinterteil3.addBox(-0.5F, 0.5F, 0.0F, 1, 1, 3, 0.0F);
		this.setRotateAngle(Hinterteil3, -0.8179294466972353F, -0.0F, 0.0F);
		this.Auge1 = new ModelRenderer(this, 10, 6);
		this.Auge1.setPos(0.0F, 0.0F, 0.0F);
		this.Auge1.addBox(0.30000001192092896F, -0.5F, -2.5F, 1, 1, 1, 0.0F);
		this.setRotateAngle(Auge1, 0.34906584024429316F, -0.0F, 0.0F);
		this.Fluegel11 = new ModelRenderer(this, 18, 6);
		this.Fluegel11.setPos(1.5F, 0.0F, 0.0F);
		this.Fluegel11.addBox(0.0F, 0.0F, 0.0F, 5, 0, 3, 0.0F);
		this.setRotateAngle(Fluegel11, 0.12384802760623698F, -0.3272010074684565F, -0.3695356217171503F);
		this.Hinterteil2 = new ModelRenderer(this, 28, 0);
		this.Hinterteil2.setPos(0.0F, 0.0F, 3.0F);
		this.Hinterteil2.addBox(-1.0F, -0.5F, 0.0F, 2, 1, 2, 0.0F);
		this.setRotateAngle(Hinterteil2, -0.4461433291435241F, -0.0F, 0.0F);
		this.Seite1 = new ModelRenderer(this, 20, 0);
		this.Seite1.setPos(0.0F, 0.0F, 0.0F);
		this.Seite1.addBox(1.0F, -0.5F, 0.0F, 1, 1, 2, 0.0F);
		this.Fluegel21 = new ModelRenderer(this, 35, 6);
		this.Fluegel21.setPos(-1.5F, 0.0F, 0.0F);
		this.Fluegel21.addBox(-5.0F, 0.0F, 0.0F, 5, 0, 3, 0.0F);
		this.setRotateAngle(Fluegel21, 0.12384802760623698F, 0.3272010074684565F, 0.3695356217171503F);
		this.Fluegel2 = new ModelRenderer(this, 35, 6);
		this.Fluegel2.setPos(-1.5F, 0.0F, 0.0F);
		this.Fluegel2.addBox(-5.0F, 0.0F, 0.0F, 5, 0, 3, 0.0F);
		this.setRotateAngle(Fluegel2, -0.12384802760623698F, 0.3272010074684565F, -0.3695356217171503F);
		this.Seite2 = new ModelRenderer(this, 20, 0);
		this.Seite2.setPos(0.0F, 0.0F, 0.0F);
		this.Seite2.addBox(-2.0F, -0.5F, 0.0F, 1, 1, 2, 0.0F);
		this.Fluegel1 = new ModelRenderer(this, 18, 6);
		this.Fluegel1.setPos(1.5F, 0.0F, 0.0F);
		this.Fluegel1.addBox(0.0F, 0.0F, 0.0F, 5, 0, 3, 0.0F);
		this.setRotateAngle(Fluegel1, -0.12384802760623698F, -0.3272010074684565F, 0.3695356217171503F);
		
		Builder<ModelRenderer> builder = ImmutableList.builder();
		builder.add(Kopf);
		builder.add(Auge1);
		builder.add(Auge2);
		builder.add(Hinterteil1);
		builder.add(Seite1);
		builder.add(Seite2);
		builder.add(Fluegel1);
		builder.add(Fluegel11);
		builder.add(Fluegel2);
		builder.add(Fluegel21);
		builder.add(Hinterteil3);
		builder.add(Hinterteil2);
		this.parts = builder.build();
	}

	@Override
	public void setupAnim(EntityHeuler entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		//super.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
		
		double f = ageInTicks % Math.PI;
		
		Fluegel11.zRot = (float) Math.sin(f);
		Fluegel2.zRot = (float) Math.cos(f);
		Fluegel21.zRot = (float) -Math.sin(f);
		Fluegel1.zRot = (float) -Math.cos(f);
	}
	
	/*
	@Override
	public void render(EntityHeuler entity, float f, float f1, float f2, float f3, float f4, float f5) 
	{ 
		this.Kopf.render(f5);
		this.Auge2.render(f5);
		this.Hinterteil1.render(f5);
		this.Hinterteil3.render(f5);
		this.Auge1.render(f5);
		this.Fluegel11.render(f5);
		this.Hinterteil2.render(f5);
		this.Seite1.render(f5);
		this.Fluegel21.render(f5);
		this.Fluegel2.render(f5);
		this.Seite2.render(f5);
		this.Fluegel1.render(f5);	
	}*/

	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.xRot = x;
		modelRenderer.yRot = y;
		modelRenderer.zRot = z;
	}

//	@Override
	public Iterable<ModelRenderer> getParts() {
		return this.parts;
	}

	@Override
	public void renderToBuffer(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) 
	{
		getParts().forEach( p -> p.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha));
	}
}
