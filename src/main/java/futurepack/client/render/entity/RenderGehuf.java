package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.FPConfig;
import futurepack.common.entity.living.EntityGehuf;
import futurepack.extensions.albedo.LightList;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;

public class RenderGehuf extends net.minecraft.client.renderer.entity.MobRenderer<EntityGehuf, ModelGehuf>
{
	private static final ResourceLocation tex = new ResourceLocation(Constants.MOD_ID,"textures/entity/gehuf.png");
    
	
	public RenderGehuf(EntityRendererManager m)
    {
        super(m, new ModelGehuf(), 0.75F);
    }

    
//  	TODO: @Mantes should re-make the gehuf model since its no longer possible to rotate this properly...      

    @Override
    public void render(EntityGehuf entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
    {
    	matrixStackIn.pushPose();
    	matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(90F));
    	matrixStackIn.translate(0, -0.5, 0);
    	if(entityIn.isBaby()) {
    		matrixStackIn.scale(0.9f, 0.9f, 0.9f);
    	}
    	else {
    		matrixStackIn.scale(1.5f, 1.5f, 1.5f);
    		matrixStackIn.translate(0, -0.18, 0);
    	}
    	    	
    	super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
    	matrixStackIn.popPose();
    	
    	if(entityIn.hasChest()) {
    		matrixStackIn.pushPose();
    		
    		float f = MathHelper.rotLerp(partialTicks, entityIn.yBodyRotO, entityIn.yBodyRot);
    		
    		this.setupRotations(entityIn, matrixStackIn, this.getBob(entityIn, partialTicks), f, partialTicks);
    		    		
    		matrixStackIn.scale(1.8f, 1.8f, 1.8f);
    		
    		matrixStackIn.translate(0, 0.74f, 0.32f);
    		
    		//Maybe we could add support for custom blocks on gehuf like techtable etc. would be renderable pretty easy
    		
    		ItemStack it = new ItemStack(Blocks.CHEST);
    		
    		if(FPConfig.HALOWEEN.getAsBoolean()) {
    			it = new ItemStack(Blocks.CARVED_PUMPKIN);
    			matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(180));
    			LightList.addLight((float)entityIn.getX(), (float)entityIn.getY(), (float)entityIn.getZ(), 0.8f, 0.65f, 0.5f, 0.9F, 10f);
    		}
    		    		
    		Minecraft.getInstance().getItemRenderer().renderStatic(it, ItemCameraTransforms.TransformType.GROUND, packedLightIn, 655360, matrixStackIn, bufferIn);
    		
    		matrixStackIn.popPose();
    	}
    	
    	
    }
    
    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	public ResourceLocation getTextureLocation(EntityGehuf p_110775_1_)
    {
        return tex;
    }
}
