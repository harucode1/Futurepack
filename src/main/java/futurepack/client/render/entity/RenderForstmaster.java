package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.api.Constants;
import futurepack.common.entity.EntityForstmaster;
import futurepack.common.entity.EntityForstmaster.EnumState;
import futurepack.extensions.albedo.LightList;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix3f;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3f;

public class RenderForstmaster extends EntityRenderer<EntityForstmaster>
{
	private ModelForstmaster model = new ModelForstmaster();

	public RenderForstmaster(EntityRendererManager renderManager)
	{
		super(renderManager);
	}

	@Override
	public void render(EntityForstmaster entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
	{
		entity.getCommandSenderWorld().getProfiler().push("renderForstmaster");
		matrixStackIn.pushPose();
		
		matrixStackIn.translate(0, 0.5, 0);
		matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(180F));
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(entity.yRot + 180F));
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(entity.xRot));
		
		IVertexBuilder builder = bufferIn.getBuffer(model.renderType(getTextureLocation(entity)));
		
		if(entity.getState()==EnumState.FELLING)//Spinn
		{
			float rot = (float) (Math.PI * (System.currentTimeMillis()%1000)/150);
			this.model.Sageblatt1.yRot=rot;
			this.model.Sageblatt2.yRot=rot+0.7853981852531433F;
			this.model.Sageblatt3.yRot=rot;
			this.model.Sageblatt4.yRot=rot+0.7853981852531433F;
		}
		model.renderToBuffer(matrixStackIn, builder, packedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
		
//		if(partialTicks < 0.25)
//			entity.scanningProgress++; //TODO this is only for debug remove this
		matrixStackIn.popPose();
		
		if(entity.getState()==EnumState.STANDBY && entity.getPower()>0.1) //entity layer check of 1
		{
			float pi = (float) ((entity.scanningProgress+partialTicks) / 60F * 2F*Math.PI);
			float tx = (float) (Math.sin(pi)*10D);
			float tz = (float) (Math.cos(pi)*10D);
			
			drawGradientTriange(entity, 5F, tx, tz, bufferIn, matrixStackIn, 0xF00F0, OverlayTexture.NO_OVERLAY);//0xF00F0 = 15728880 is from Beacon beam
			LightList.addLight((float)entity.getX(), (float)entity.getY(), (float)entity.getZ(), 0F, 1F, 1F, 1F, 7F);
		}
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		
		entity.getCommandSenderWorld().getProfiler().pop();
	}
	

	private void drawGradientTriange(EntityForstmaster entity, float height, float x, float z, IRenderTypeBuffer bufferIn, MatrixStack stack, int lightMap, int overlay)
	{
		IVertexBuilder builder = bufferIn.getBuffer(RenderType.entityTranslucent(getTextureLocation(entity)));
		//.add(POSITION_3F).add(COLOR_4UB).add(TEX_2F).add(TEX_2S).add(TEX_2SB).add(NORMAL_3B).add(PADDING_1B).build()
		//pos color UV lightmap overlay normals
		
		MatrixStack.Entry last = stack.last();
		Matrix4f matrix = last.pose();
		Matrix3f nomrals = last.normal();
		
		float u1 = 0F;
		float v1 = 48F/128F;
		float u2 = 64F/128F;
		float v2 = 112F/128F;
		
//		builder.pos(matrix, x, +height, z).color(0F, 1F, 1F, 0.01F).tex(u1, v1).overlay(overlay).lightmap(lightMap).normal(nomrals, 1, 0, 0).endVertex();
//		builder.pos(matrix, 0F, 0.0F, 0F) .color(0, 1F, 1F, 0.9F)  .tex(u2, v1).overlay(overlay).lightmap(lightMap).normal(nomrals, 1, 0, 0).endVertex();
//		builder.pos(matrix, 0F, -0.1F, 0F).color(0, 1F, 1F, 0.9F)  .tex(u2, v2).overlay(overlay).lightmap(lightMap).normal(nomrals, 1, 0, 0).endVertex();
//		builder.pos(matrix, x, -height, z).color(0F, 1F, 1F, 0.01F).tex(u1, v2).overlay(overlay).lightmap(lightMap).normal(nomrals, 1, 0, 0).endVertex();
		
		builder.vertex(matrix, 0F, 0.2F, 0F) .color(0, 1F, 1F, 0.9F)  .uv(u2, v1).overlayCoords(overlay).uv2(lightMap).normal(nomrals, -1, 0, 0).endVertex();
		builder.vertex(matrix, x, +height, z).color(0F, 1F, 1F, 0.01F).uv(u1, v1).overlayCoords(overlay).uv2(lightMap).normal(nomrals, -1, 0, 0).endVertex();
		builder.vertex(matrix, x, -height, z).color(0F, 1F, 1F, 0.01F).uv(u1, v2).overlayCoords(overlay).uv2(lightMap).normal(nomrals, -1, 0, 0).endVertex();
        builder.vertex(matrix, 0F, -0.0F, 0F).color(0, 1F, 1F, 0.9F)  .uv(u2, v2).overlayCoords(overlay).uv2(lightMap).normal(nomrals, -1, 0, 0).endVertex();

	}
	
	@Override
	public ResourceLocation getTextureLocation(EntityForstmaster entity)
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/forstmaster.png");
	}

}
