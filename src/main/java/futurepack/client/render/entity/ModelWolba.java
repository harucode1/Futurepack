package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.common.entity.living.EntityWolba;
import net.minecraft.client.renderer.entity.model.AgeableModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.util.math.MathHelper;

public class ModelWolba extends AgeableModel<EntityWolba>
{
	//fields
	ModelRenderer korpus_h;
	ModelRenderer korpus_v;
	ModelRenderer korpus_m;
	ModelRenderer hinterteil;
	
	ModelRenderer hals;
	
	ModelRenderer kopf;
	
	ModelRenderer schnautze;
	ModelRenderer kiefer;
	//ModelRenderer schnautze;
	

    ModelRenderer bein_loh;
    ModelRenderer bein_luh;
 
    ModelRenderer bein_roh;
    ModelRenderer bein_ruh;
    
    ModelRenderer bein_lov;
    ModelRenderer bein_luv;
    
    ModelRenderer bein_rov;
    ModelRenderer bein_ruv;
    
    
    ModelRenderer wolle1;
    ModelRenderer wolle2;
    
    
    ModelRenderer gewei_r1;
    ModelRenderer gewei_r2;
    ModelRenderer gewei_r3;
    
    ModelRenderer gewei_l1;
    ModelRenderer gewei_l2;
    ModelRenderer gewei_l3;
    
    ModelRenderer schwanz;

    private float headRotationAngleX;
    private float[] fur_color;
    private boolean wool_on;
    
    private ImmutableList<ModelRenderer> parts;
    
    public ModelWolba()
    {
    	texWidth = 128;
    	texHeight = 128;
    
    	
    	korpus_h = new ModelRenderer(this, 46, 0);
    	korpus_h.addBox(-2F, 0F, 0F, 6, 5, 3);
    	korpus_h.setPos(0F, 0F + 4F, 11F-8F);
    	korpus_h.setTexSize(128, 128);
    	korpus_h.mirror = true;
        setRotation(korpus_h, 0F, 0F, 0F);
    	
        korpus_m = new ModelRenderer(this, 18, 8);
        korpus_m.addBox(-1.5F, 0.5F, 0F, 5, 4, 3);
        korpus_m.setPos(0F, 0F + 4F, 8F-8F);
        korpus_m.setTexSize(128, 128);
        korpus_m.mirror = true;
        setRotation(korpus_m, 0F, 0F, 0F);
        
        hinterteil = new ModelRenderer(this, 0, 0);
        hinterteil.addBox(-1.5F, 0.5F, 0F, 5, 4, 1);
        hinterteil.setPos(0F, 0F + 4F, 14F-8F);
        hinterteil.setTexSize(128, 128);
        hinterteil.mirror = true;
        setRotation(hinterteil, 0F, 0F, 0F);
        
        korpus_v = new ModelRenderer(this, 46, 0);
        korpus_v.addBox(-3F, -1F, 0F, 6, 5, 3);
        korpus_v.setPos(1F, 1F + 4F, 5F-8F);
        korpus_v.setTexSize(128, 128);
        korpus_v.mirror = true;
        setRotation(korpus_v, 0F, 0F, 0F);
        
        hals = new ModelRenderer(this, 20, 0);
        hals.addBox(-1.5F, -1.5F, -4F, 3, 3, 4);
        hals.setPos(1F, 2.3F + 4F, 6F-8F);
        hals.setTexSize(128, 128);
        hals.mirror = true;
        setRotation(hals, -0.4461433F, 0F, 0F);
        
        kopf = new ModelRenderer(this, 16, 33);
        kopf.addBox(-2F, -2F, -4F, 4, 4, 4);
        kopf.setPos(1F, 1.5F + 1F + 4F, 3F-8F);
        kopf.setTexSize(128, 128);
        kopf.mirror = true;
        setRotation(kopf, 0.4461433F, 0F, 0F);
        
        schnautze = new ModelRenderer(this, 0, 17);
        schnautze.addBox(-1.5F, -1F, -1F, 3, 2, 1);
        schnautze.setPos(0F, -0.8F + 2F, 2F + -1F - 8F);
        schnautze.setTexSize(128, 128);
        schnautze.mirror = true;
        setRotation(schnautze, 0F, 0F, 0F);
        
        kiefer = new ModelRenderer(this, 0, 34);
        kiefer.addBox(-1.5F, 0F, -2F, 3, 1, 4);
        kiefer.setPos(0F, -0.8F + 3F, 2F + 0F - 8F);
        kiefer.setTexSize(128, 128);
        kiefer.mirror = true;
        setRotation(kiefer, 0.1487144F, 0F, 0F);
        
        
        bein_loh = new ModelRenderer(this, 36, 0);
        bein_loh.addBox(-1F, -1F, -1F, 2, 7, 2);
        bein_loh.setPos(3.3F, 2F + 4F, 12.5F - 8F);
        bein_loh.setTexSize(128, 128);
        bein_loh.mirror = true;
        setRotation(bein_loh, 0F, 0F, 0F);
        
        bein_luh = new ModelRenderer(this, 36, 9);
        bein_luh.addBox(-1F, 0F, -1F, 2, 4, 2);
        bein_luh.setPos(3.3F, 8F + 4F, 12.5F - 8F);
        bein_luh.setTexSize(128, 128);
        bein_luh.mirror = true;
        setRotation(bein_luh, 0F, 0F, 0F);
        
        addChild(bein_loh, bein_luh);
        
        
        bein_roh = new ModelRenderer(this, 36, 0);
        bein_roh.addBox(-1F, -1F, -1F, 2, 7, 2);
        bein_roh.setPos(-1.3F, 2F + 4F, 12.5F - 8F);
        bein_roh.setTexSize(128, 128);
        bein_roh.mirror = true;
        setRotation(bein_roh, 0F, 0F, 0F);
        
        bein_ruh = new ModelRenderer(this, 36, 9);
        bein_ruh.addBox(-1F, 0F, -1F, 2, 4, 2);
        bein_ruh.setPos(-1.3F, 8F + 4F, 12.5F - 8F);
        bein_ruh.setTexSize(128, 128);
        bein_ruh.mirror = true;
        setRotation(bein_ruh, 0F, 0F, 0F);
        
        addChild(bein_roh, bein_ruh);
        
        
        bein_lov = new ModelRenderer(this, 36, 0);
        bein_lov.addBox(-1F, -1F, -1F, 2, 7, 2);
        bein_lov.setPos(3.3F, 2F + 4F, 6.5F - 8F);
        bein_lov.setTexSize(128, 128);
        bein_lov.mirror = true;
        setRotation(bein_lov, 0F, 0F, 0F);
        
        bein_luv = new ModelRenderer(this, 36, 9);
        bein_luv.addBox(-1F, 0F, -1F, 2, 4, 2);
        bein_luv.setPos(3.3F, 8F + 4F, 6.5F - 8F);
        bein_luv.setTexSize(128, 128);
        bein_luv.mirror = true;
        setRotation(bein_luv, 0F, 0F, 0F);
        
        addChild(bein_lov, bein_luv);
        
        bein_rov = new ModelRenderer(this, 36, 0);
        bein_rov.addBox(-1F, -1F, -1F, 2, 7, 2);
        bein_rov.setPos(-1.3F, 2F + 4F, 6.5F - 8F);
        bein_rov.setTexSize(128, 128);
        bein_rov.mirror = true;
        setRotation(bein_rov, 0F, 0F, 0F);
        
        bein_ruv = new ModelRenderer(this, 36, 9);
        bein_ruv.addBox(-1F, 0F, -1F, 2, 4, 2);
        bein_ruv.setPos(-1.3F, 8F + 4F, 6.5F - 8F);
        bein_ruv.setTexSize(128, 128);
        bein_ruv.mirror = true;
        setRotation(bein_ruv, 0F, 0F, 0F);
        
        addChild(bein_rov, bein_ruv);
        
        
        schwanz = new ModelRenderer(this, 66, 0);
        schwanz.addBox(-1F, -1F, -2F, 2, 8, 2);
        schwanz.setPos(1F, 2F+4F, 15.5F-8F);
        schwanz.setTexSize(128, 128);
        schwanz.mirror = true;
        setRotation(schwanz, 0.1487144F, 0F, 0F);

        wolle1 = new ModelRenderer(this, 0, 16);
        wolle1.addBox(-3.5F, -0.49F, -1F, 7, 5, 11.1f);
        wolle1.setPos(1F, 0F + 4F, 5F-8F);
        wolle1.setTexSize(128, 128);
        wolle1.mirror = true;
        setRotation(wolle1, 0F, 0F, 0F);
        
        wolle2 = new ModelRenderer(this, 37, 21);
        wolle2.addBox(-2.5F, -1.5F, -0.5F, 5, 2, 10);
        wolle2.setPos(1F, 0F + 4F, 5F-8F);
        wolle2.setTexSize(128, 128);
        wolle2.mirror = true;
        setRotation(wolle2, 0F, 0F, 0F);
        
        
        //0F, -0.8F + 2F, 2F + -1F - 8F
        
        gewei_r1 = new ModelRenderer(this, 0, 12);
        gewei_r1.addBox(-0.5F, -1F, -0.5F, 1, 2, 1);
        gewei_r1.setPos(-1F, -0.8F + -1F + 6F, 0F + -1F - 7F);
        gewei_r1.setTexSize(128, 128);
        gewei_r1.mirror = true;
        setRotation(gewei_r1, -0.1396263F, 0F, 0F);
        
        gewei_r2 = new ModelRenderer(this, 46, 11);
        gewei_r2.addBox(-0.55F, -1F, 0F, 1, 1, 4);
        gewei_r2.setPos(-1F, -0.8F + 0F + 6F, 0F + -1F - 7F);
        gewei_r2.setTexSize(128, 128);
        gewei_r2.mirror = true;
        setRotation(gewei_r2, 0.3346075F, 0F, 0F);
        
        gewei_r3 = new ModelRenderer(this, 0, 12);
        gewei_r3.addBox(-0.60F, 0F, -0.5F, 1, 2, 1);
        gewei_r3.setPos(-1F, -0.8F + -1F + 6F, 1F + -1F - 7F);
        gewei_r3.setTexSize(128, 128);
        gewei_r3.mirror = true;
        setRotation(gewei_r3, 1.012911F, 0F, 0F);
        
        gewei_l1 = new ModelRenderer(this, 0, 12);
        gewei_l1.addBox(-0.5F, -1F, -0.5F, 1, 2, 1);
        gewei_l1.setPos(3F, -0.8F + -1F + 6F, 0F + -1F - 7F);
        gewei_l1.setTexSize(128, 128);
        gewei_l1.mirror = true;
        setRotation(gewei_l1, -0.1396263F, 0F, 0F);
        
        gewei_l2 = new ModelRenderer(this, 46, 11);
        gewei_l2.addBox(-0.45F, -1F, 0F, 1, 1, 4);
        gewei_l2.setPos(3F, -0.8F + 0F + 6F, 0F + -1F - 7F);
        gewei_l2.setTexSize(128, 128);
        gewei_l2.mirror = true;
        setRotation(gewei_l2, 0.3346075F, 0F, 0F);
        
        gewei_l3 = new ModelRenderer(this, 0, 12);
        gewei_l3.addBox(-0.40F, 0F, -0.5F, 1, 2, 1);
        gewei_l3.setPos(3F, -0.8F + -1F + 6F, 1F + -1F - 7F);
        gewei_l3.setTexSize(128, 128);
        gewei_l3.mirror = true;
        setRotation(gewei_l3, 1.012911F, 0F, 0F);
        
        addChild(kopf, gewei_r1);
        addChild(kopf, gewei_r2);
        addChild(kopf, gewei_r3);
        
        addChild(kopf, gewei_l1);
        addChild(kopf, gewei_l2);
        addChild(kopf, gewei_l3);
        

        addChild(hals, kopf);
        addChild(kopf, schnautze);
        addChild(kopf, kiefer);

        Builder<ModelRenderer> builder = ImmutableList.builder();
        builder.add(korpus_h);
        builder.add(korpus_v);
        builder.add(korpus_m);
        builder.add(hinterteil);
//        builder.add(hals);
        builder.add(bein_loh);
        builder.add(bein_roh);
        builder.add(bein_lov);
        builder.add(bein_rov);
        builder.add(schwanz);
        this.parts = builder.build();
    }
  
    private void addChild(ModelRenderer base, ModelRenderer child)
    {
    	child.x -= base.x;
    	child.y -= base.y;
    	child.z -= base.z;
    	base.addChild(child);
    }
    
    @Override
    public void setupAnim(EntityWolba entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
    {
        this.hals.xRot = -0.4461433F + headPitch * 0.017453292F;
        this.hals.yRot = netHeadYaw * 0.017453292F;
       
        this.bein_roh.xRot = MathHelper.cos(limbSwing * 0.662F*3F) * (1.4F/3F) * limbSwingAmount;
        this.bein_loh.xRot = MathHelper.cos(limbSwing * 0.662F*3F + (float)Math.PI) * (1.4F/3F) * limbSwingAmount;
        this.bein_rov.xRot = MathHelper.cos(limbSwing * 0.662F*3F + (float)Math.PI) * (1.4F/3F) * limbSwingAmount;
        this.bein_lov.xRot = MathHelper.cos(limbSwing * 0.662F*3F) * (1.4F/3F) * limbSwingAmount;
    }
    
    @Override
    public void prepareMobModel(EntityWolba entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTickTime)
    {
    	super.prepareMobModel(entitylivingbaseIn, limbSwing, limbSwingAmount, partialTickTime);
    	
    	EntityWolba wolba = (entitylivingbaseIn);
    	
    	this.headRotationAngleX = wolba.getHeadEatAngleScale(partialTickTime);
        
        fur_color = SheepEntity.getColorArray(wolba.getColor());        
        wool_on = !wolba.isSheared();
    }
  
    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
    	model.xRot = x;
    	model.yRot = y;
    	model.zRot = z;
    }
    
    @Override
    public void renderToBuffer(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {
    	super.renderToBuffer(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
    	
    	if(wool_on) {
	    	matrixStackIn.pushPose();
	    	
	    	this.wolle1.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, fur_color[0], fur_color[1], fur_color[2], 1);
	    	this.wolle2.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, fur_color[0], fur_color[1], fur_color[2], 1);
	    	
	    	matrixStackIn.popPose();
    	}
    	
    }
	@Override
	protected Iterable<ModelRenderer> headParts() 
	{
		return ImmutableList.of(hals);
	}

	@Override
	protected Iterable<ModelRenderer> bodyParts() 
	{
		return parts;
	}
}
