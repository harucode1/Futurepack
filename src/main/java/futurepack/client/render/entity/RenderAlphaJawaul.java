package futurepack.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityAlphaJawaul;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class RenderAlphaJawaul extends MobRenderer<EntityAlphaJawaul, ModelAlphaJawaul> 
{
   private static final ResourceLocation TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/alpha_jawaul.png");
   private static final ResourceLocation TEXTURES_BERNER = new ResourceLocation(Constants.MOD_ID, "textures/entity/alpha_jawaul_berner.png");

   
   public RenderAlphaJawaul(EntityRendererManager renderManagerIn) 
   {
      super(renderManagerIn, new ModelAlphaJawaul(), 0.5F);
   }

   @Override
   protected float getBob(EntityAlphaJawaul livingBase, float partialTicks) 
   {
      return livingBase.getTailRotation();
   }

   @Override
   public void render(EntityAlphaJawaul entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
   {
      super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);

   }

   @Override
   public ResourceLocation getTextureLocation(EntityAlphaJawaul entity) 
   {
	   String name = entity.getName().getString();
	   boolean berner = "berner".equalsIgnoreCase(name) || "dvorack".equalsIgnoreCase(name);
	   return berner ? TEXTURES_BERNER : TEXTURES;
   }
}