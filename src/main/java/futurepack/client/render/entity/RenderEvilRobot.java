package futurepack.client.render.entity;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityEvilRobot;
import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.util.ResourceLocation;

public class RenderEvilRobot extends BipedRenderer<EntityEvilRobot, ModelEvilRobot>
{
	private static final ResourceLocation TEX = new ResourceLocation(Constants.MOD_ID, "textures/entity/bot.png");

	public RenderEvilRobot(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, new ModelEvilRobot(), 0.5F);
		this.addLayer(new HeldItemLayer<EntityEvilRobot, ModelEvilRobot>(this));
        this.addLayer(new BipedArmorLayer<EntityEvilRobot, ModelEvilRobot, ModelEvilRobot>(this, new ModelEvilRobot(0.5F, true), new ModelEvilRobot(1.0F, true)));		
		this.addLayer(new LayerEvilRobot(this));
	}
	
	
//	@Override
//	public void transformHeldFull3DItemLayer()
//    {
//	       GlStateManager.translatef(0.09375F, 0.1875F, 0.0F);
//    }
	
	
	
	@Override
	public ResourceLocation getTextureLocation(EntityEvilRobot entity)
	{
		return TEX;
	}

}
