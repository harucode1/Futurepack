package futurepack.client.render.entity;

import futurepack.api.Constants;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.WolfRenderer;
import net.minecraft.entity.passive.WolfEntity;
import net.minecraft.util.ResourceLocation;

public class RenderJawaul extends WolfRenderer 
{

	public RenderJawaul(EntityRendererManager renderManagerIn) 
	{
		super(renderManagerIn);
	}

	private static final ResourceLocation WOLF_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/jawaul/jawaul.png");
	private static final ResourceLocation TAMED_WOLF_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/jawaul/jawaul_tame.png");
	private static final ResourceLocation ANGRY_WOLF_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/jawaul/jawaul_angry.png");

	@Override
	public ResourceLocation getTextureLocation(WolfEntity entity) 
	{
		if (entity.isTame()) 
		{
			return TAMED_WOLF_TEXTURES;
		}
		else 
		{
			return entity.isAngry() ? ANGRY_WOLF_TEXTURES : WOLF_TEXTURES;
		}
	}
}
