package futurepack.client.render;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.renderer.RenderState;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;

public abstract class FuturepackRenderTypes extends RenderType 
{
	public static final RenderState.TransparencyState HOLOGRAM_TRANSPARENCY = new RenderState.TransparencyState("hologram_transparency", () -> {
		RenderSystem.enableBlend();
		RenderSystem.blendFunc(GlStateManager.SourceFactor.ONE_MINUS_DST_COLOR, GlStateManager.DestFactor.ONE);
	}, () -> {
		RenderSystem.disableBlend();
		RenderSystem.defaultBlendFunc();
	});
	//262144 is same as used in translucent
	public static final RenderType HOLOGRAM = create("hologram", DefaultVertexFormats.BLOCK, GL11.GL_QUADS, 262144, true, true, RenderType.State.builder().setShadeModelState(RenderState.SMOOTH_SHADE).setLightmapState(LIGHTMAP).setTextureState(BLOCK_SHEET_MIPPED).setTransparencyState(HOLOGRAM_TRANSPARENCY).createCompositeState(true));
	

	public FuturepackRenderTypes(String p_i225992_1_, VertexFormat p_i225992_2_, int p_i225992_3_, int p_i225992_4_,
			boolean p_i225992_5_, boolean p_i225992_6_, Runnable p_i225992_7_, Runnable p_i225992_8_) {
		super(p_i225992_1_, p_i225992_2_, p_i225992_3_, p_i225992_4_, p_i225992_5_, p_i225992_6_, p_i225992_7_, p_i225992_8_);
		// TODO Auto-generated constructor stub
	}
	
}
