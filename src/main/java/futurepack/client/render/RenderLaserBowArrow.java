package futurepack.client.render;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.common.entity.throwable.EntityLaser;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector4f;

public class RenderLaserBowArrow extends EntityRenderer<EntityLaser>
{

	public RenderLaserBowArrow(EntityRendererManager man)
	{
		super(man);
	}
	
	@Override
	public void render(EntityLaser e, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
	{
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		
		GL11.glLineWidth(3F);
		
		GL11.glColor4f(1, 0, 0, 1);
		GL11.glBegin(GL11.GL_LINES);
		
		Vector3d mot = e.getDeltaMovement();
		
		Matrix4f matrix = matrixStackIn.last().pose();
		Vector4f pos4f = new Vector4f(0F, 0F, 0F, 1.0F);
		pos4f.transform(matrix);
		
		GL11.glVertex3d(pos4f.x() + mot.x, pos4f.y() + mot.y, pos4f.z()+mot.z);
		GL11.glVertex3d(pos4f.x(), pos4f.y() , pos4f.z());
		
		GL11.glEnd();
		
		GL11.glLineWidth(1F);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glColor4f(1, 1, 1, 1);
	}

	@Override
	public ResourceLocation getTextureLocation(EntityLaser p_110775_1_) 
	{
		return null;
	}

}
