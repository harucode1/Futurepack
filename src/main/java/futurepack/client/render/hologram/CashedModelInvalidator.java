package futurepack.client.render.hologram;

import java.util.ArrayList;
import java.util.Map;

import futurepack.common.FPLog;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class CashedModelInvalidator implements Runnable
{
	private Thread running;
	private ArrayList<Box> updateBoxes = new ArrayList<Box>();
	private World w;
	private Map<TileEntity, CashedModel> modelCash;
	
	public CashedModelInvalidator(World w, Map<TileEntity, CashedModel> modelCash)
	{
		this.w = w;
		this.modelCash = modelCash;
		//1st thread creation, then event register
		running = new Thread(this, "FP-CashInvalidator");
		running.setDaemon(true);
		running.start();
		
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@SubscribeEvent
	public void onBlockUpdate(BlockEvent.NeighborNotifyEvent event)
	{
		if(!running.isAlive())
		{
			running.start(); //this will result in a crash but this is okay, since something realy bad has happend.
		}
		updateBoxes.add(new Box(event.getPos()));
	}
	
	@SubscribeEvent
	public void onBlockBreak(BlockEvent.BreakEvent event)
	{
		updateBoxes.add(new Box(event.getPos()));
	}
	
	@Override
	public void run() //TODO: this whole block screams inefficient...
	{
		while(w!=null)
		{
			try
			{	
				while(!updateBoxes.isEmpty())
				{
					ArrayList<Box> todo = updateBoxes;
					updateBoxes = new ArrayList<Box>(todo.size());
					for(int i=todo.size() -1; i>=0;i--)
					{
						Box box = todo.get(i);
						
						for(int x = box.getX1(); x <= box.getX2(); x++)
						{
							for(int y = box.getY1(); y <= box.getY2(); y++)
							{
								for(int z = box.getZ1(); z <= box.getZ2(); z++)
								{
									try
									{
										TileEntity tile = w.getBlockEntity(new BlockPos(x,y,z));
										if(tile!=null)
										{
											CashedModel model = modelCash.get(tile);
											if(model!=null)
											{
												model.markDirty();
											}
										}
									}
									catch (NullPointerException e)
									{
										e.printStackTrace();
//										boolean b = false;
//										//System.out.println("Analyzing Error Source (only works if error was in getPendingTileEntitys)");
//										List<TileEntity> l = ReflectionHelper.getPrivateValue(World.class, w, "pendingBlockEntities", "addedTileEntityList");
//										for(TileEntity t : l)
//										{
//											if(t==null)
//											{
//												System.out.println("Null value in pending Tile list!");
//												b = true;
//											}
//											else if(t.getPos()==null)
//											{
//												System.out.println("tile with null pos " + t);
//												b=true;
//											}
//												
//										};
//										if(!b)
//											e.printStackTrace();
									}
								}
							}
						}
					}
					todo.clear();
				}
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}	
			catch(Throwable e)
			{
				e.printStackTrace();
				FPLog.logger.catching(e);
				for(StackTraceElement elm : e.getStackTrace())
				{
					FPLog.logger.debug(elm.toString());
				}
			}
		}
	}
	
	
	
	public void clear()
	{
		MinecraftForge.EVENT_BUS.unregister(this);
		w = null;
		updateBoxes.clear();
		updateBoxes = null;
	}
	
	private static class Box
	{
		private final int x1,y1,z1;
		private final int x2,y2,z2;
		
		public Box(int x1, int y1, int z1, int x2, int y2, int z2)
		{
			super();
			this.x1 = Math.min(x1, x2);
			this.y1 = Math.min(y1, y2);
			this.z1 = Math.min(z1, z2);
			this.x2 = Math.max(x1, x2);
			this.y2 = Math.max(y1, y2);
			this.z2 = Math.max(z1, z2);
		}
		
		public Box(Vector3i pos)
		{
			this(pos.getX()-1, pos.getY()-1, pos.getZ()-1, pos.getX()+1, pos.getY()+1, pos.getZ()+1);
		}
		
		public int getX1()
		{
			return x1;
		}
		
		public int getY1()
		{
			return y1;
		}
		
		public int getZ1()
		{
			return z1;
		}
		
		public int getX2()
		{
			return x2;
		}
		
		public int getY2()
		{
			return y2;
		}
		
		public int getZ2()
		{
			return z2;
		}
		
		
	}

	public World getWorld()
	{
		return this.w;
	}
}
