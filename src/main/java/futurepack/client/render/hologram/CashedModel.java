package futurepack.client.render.hologram;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CashedModel
{
	private static final BufferBuilder buffer = new BufferBuilder(4*7*40);
	
	private final long UPDATE_TIME = 2000L;
	
	private int light;
	
	private World world;
	private BlockPos pos;
	private BlockState state;
	
	private ByteBuffer data;
	private int elements;
	
	private long creationTime = 0L;
	
	private boolean old = true;
	
	public CashedModel(BlockPos pos, BlockState state, World world)
	{
		this.world = world;
		this.pos = pos;
		this.state = state;
		
		//FIXME create();
	}
	
	private void create(MatrixStack matrixStack, IRenderTypeBuffer bufferTypeIn)
	{				
		old = false;
		
		light = world.getRawBrightness(pos, 0);
		//Tessellator tes = Tessellator.getInstance();
		
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK); ////Blocks: 3F-pos, 4UB-color, 2F-UV, 2S-UV 
		
		//FIXME buffer.setTranslation(-pos.getX(), -pos.getY(), -pos.getZ());
		TemporaryWorld temp = new TemporaryWorld(world, pos, state);
//		HelperRenderBlocks.renderBlockTESR(state, pos, temp, buffer);
		
		//FIXME HelperRenderBlocks.renderBlockTESR(state, matrixStackIn, bufferTypeIn, light, combinedOverlayIn);
		
		//FIXME elements = buffer.getVertexCount();
		buffer.end();
		byte[] bytes = new byte[elements * DefaultVertexFormats.BLOCK.getVertexSize()];
		//FIXME buffer.getByteBuffer().get(bytes);
		buffer.clear(); //so this wont get rendered...
		
		data = ByteBuffer.wrap(bytes).asReadOnlyBuffer();
		data.order(ByteOrder.LITTLE_ENDIAN);
		creationTime = System.currentTimeMillis();
	}
	
	public void build(float x, float y, float z, BufferBuilder buf)
	{
		if(old)//some random so no big lag
		{
			//FIXME create();
		}
		if(buf.getVertexFormat() == DefaultVertexFormats.BLOCK && elements > 0)
		{		
			ByteBuffer vertex = translate(x, y, z);
			vertex.order(ByteOrder.BIG_ENDIAN);
//			HelperRenderBlocks.insertRenderData(buf, vertex, elements);
		}
	}

	private ByteBuffer translate(float x, float y, float z)//data buffer wird sehr oft kopiert, optimierungs bedarf...
	{
		int size = DefaultVertexFormats.BLOCK.getVertexSize();
		int length = size * elements;
		byte[] bytes = new byte[length];
		this.data.position(0);
		this.data.get(bytes); 
		ByteBuffer data = ByteBuffer.wrap(bytes);
		data.order(ByteOrder.LITTLE_ENDIAN);
		for(int i=0;i<elements;i++)
		{
			data.position(i * size);
			float ox, oy, oz;
			ox = data.getFloat();
			oy = data.getFloat();
			oz = data.getFloat();
				
			data.position(i * size);

			data.putFloat(ox+x);
			data.putFloat(oy+y);
			data.putFloat(oz+z);
		}	
		data.position(0);
		return data;
	}

	public BlockPos getPos()
	{
		return pos;
	}

	public BlockState getBlockState()
	{
		return state;
	}
	
	public int getLight()
	{
		return light;
	}

	public void clear()
	{
		pos=null;
		state = null;
		world = null;
		data.clear();
		data = null;
		light = -1;
		elements = -1;
	}

	public void markDirty()
	{
		old = true;
	}
}
