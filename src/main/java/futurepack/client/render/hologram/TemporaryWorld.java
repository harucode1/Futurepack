package futurepack.client.render.hologram;

import java.util.HashMap;

import futurepack.api.interfaces.tilentity.ITileHologramAble;
import net.minecraft.block.BlockState;
import net.minecraft.profiler.IProfiler;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

public class TemporaryWorld extends DelegatedWorldReader
{
	IProfiler prof;
	
	public final HashMap<BlockPos, BlockState> overwriteState;
	public final HashMap<BlockPos, TileEntity> overwriteTiles;
	
	IWorldReader original;
	boolean debug = true;
	
	long update;
	
	public TemporaryWorld(IWorldReader ori, BlockPos p, BlockState f, TileEntity tile)
	{
		super(ori);
		original = ori;
		overwriteState = new HashMap<>();
		overwriteTiles = new HashMap<>();
		overwriteState.put(p, f);
		overwriteTiles.put(p, tile);
		debug = false;
		update = System.currentTimeMillis();
		
		if(ori instanceof World)
		{
			prof = ((World) ori).getProfiler();
		}
	}
	
	public TemporaryWorld(World ori, BlockPos p, BlockState f)
	{
		this(ori, p, f, f.getBlock().createTileEntity(f, ori));
		debug = true;
	}
	
	@Override
	public TileEntity getBlockEntity(BlockPos pos)
	{
		startSection("w.getTileEntity");
		TileEntity tile;
		if(overwriteTiles.containsKey(pos))
		{
			tile =  overwriteTiles.get(pos);
		}
		else
		{
			tile = original.getBlockEntity(pos);
		}
		endSection();
		return tile;
	}

	@Override
	public BlockState getBlockState(BlockPos pos)
	{
		startSection("w.getBlockState");
		BlockState state = overwriteState.getOrDefault(pos, null);
		if(state==null)
		{
			if(getBlockEntity(pos) !=null)
			{
				TileEntity tile = getBlockEntity(pos);
				if(tile instanceof ITileHologramAble)
				{
					ITileHologramAble holo = (ITileHologramAble) getBlockEntity(pos);
					if(holo.hasHologram())
					{
						state =  holo.getHologram();
					}
				}		
			}
			if(state==null)
			{
				state = original.getBlockState(pos);
			}
		}
		endSection();
		return state;
	}


	public void checkOld()
	{
		if(System.currentTimeMillis() - update > 1000)
		{
			update = System.currentTimeMillis();
			overwriteState.clear();
			overwriteTiles.clear();
		}
	}
	
	private void startSection(String name)
	{
		if(prof!=null)
			prof.push(name);
	}
	
	private void endSection()
	{
		if(prof!=null)
			prof.pop();
	}
}
