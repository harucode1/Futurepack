package futurepack.client.render;

import java.util.Vector;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldVertexBufferUploader;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3f;

public class RenderSkyTyros extends RenderSkyBase
{
	private static final ResourceLocation PLANET_SHAPE_TEXTURE = new ResourceLocation(Constants.MOD_ID, "textures/tyros_gas_shape.png");
    private static final ResourceLocation PLANET_COLOR_TEXTURE = new ResourceLocation(Constants.MOD_ID, "textures/tyros_gas_color.png");
	
    private float f = 0.0f;
    
	@Override
	protected void renderAfterStars(float partialTicks, ClientWorld world, Minecraft mc, BufferBuilder bufferbuilder, MatrixStack matrixStackIn)
	{
//		GlStateManager._pushMatrix();
		
		matrixStackIn.pushPose();
		
		GlStateManager._enableTexture();
		GlStateManager._disableLighting();
		GlStateManager._color4f(1F, 1F, 1F, 1F);
//		GlStateManager._rotatef(90, 1, 1, 0);	
		matrixStackIn.mulPose(new Quaternion(new Vector3f(1.0F, 1.0F, 0.0F), 90F, true));
		
		
		this.bindTexture(PLANET_SHAPE_TEXTURE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

		GlStateManager._enableBlend();
		GlStateManager._blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA.value, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA.value, GlStateManager.SourceFactor.ONE.value, GlStateManager.DestFactor.ZERO.value);

		{
			bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
			renderFacesShape(bufferbuilder, matrixStackIn.last().pose());
			bufferbuilder.end();
	        WorldVertexBufferUploader.end(bufferbuilder);
		}
		
		f += 0.0001;
		if(f > 1.0f)
			f -= 1.0f;
		
		{
			this.bindTexture(PLANET_COLOR_TEXTURE);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			
			
			/* Only needed if PLANET_COLOR_TEXTURE contains alpha chanel
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.ZERO, GlStateManager.DestFactor.ONE, GlStateManager.SourceFactor.DST_ALPHA, GlStateManager.DestFactor.ZERO);			
			{
				Tessellator tessellator = Tessellator.getInstance();
				BufferBuilder bufferbuilder = tessellator.getBuffer();
				bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
				renderFaces2(bufferbuilder,f);
				tessellator.draw();	
			}
			*/
			
			GlStateManager._blendFunc(GlStateManager.SourceFactor.DST_ALPHA.value, GlStateManager.DestFactor.ONE_MINUS_DST_ALPHA.value);
			
			{
				bufferbuilder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
				renderFacesOverlay(bufferbuilder,f, matrixStackIn.last().pose());
				bufferbuilder.end();
		        WorldVertexBufferUploader.end(bufferbuilder);
			}
		
		}
		
//		GlStateManager._popMatrix();
		matrixStackIn.popPose();
	}
	

	private void renderFacesShape(BufferBuilder buffer, Matrix4f matrix4f1)
	{
		float size = 50F;
		
		float f22 = 0F;
		float f23 = 0F;
		float f24 = 1F;
		float f14 = 1F;	
		
		buffer.vertex(matrix4f1, (-size), -100.0F, size).uv(f22, f23).color(100, 0, 100, 255).endVertex();
		buffer.vertex(matrix4f1, size, -100.0F, size).uv(f22, f14).color(100, 0, 100, 255).endVertex();
		buffer.vertex(matrix4f1, size, -100.0F, (-size)).uv(f24, f14).color(100, 0, 100, 255).endVertex();
		buffer.vertex(matrix4f1, (-size), -100.0F, (-size)).uv(f24, f23).color(50, 155, 50, 255).endVertex();
	}
	
	private void renderFacesOverlay(BufferBuilder buffer, float detla, Matrix4f matrix4f1)
	{
		float size = 50F;
		
		float f22 = (0F + detla) / 1.50f;
		float f23 = 0F;
		float f24 = (1F + detla) / 1.50f;
		float f14 = 1F;	
		
		buffer.vertex(matrix4f1, (-size), -100.0F, size).uv(f22, f23).color(100, 0, 100, 255).endVertex();
		buffer.vertex(matrix4f1, size, -100.0F, size).uv(f22, f14).color(100, 0, 100, 255).endVertex();
		buffer.vertex(matrix4f1, size, -100.0F, (-size)).uv(f24, f14).color(100, 0, 100, 255).endVertex();
		buffer.vertex(matrix4f1, (-size), -100.0F, (-size)).uv(f24, f23).color(150, 255, 150, 255).endVertex();
	}

	@Override
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1) 
	{
		//no moon on tyros
	}
}
