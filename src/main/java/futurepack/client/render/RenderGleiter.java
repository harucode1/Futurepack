package futurepack.client.render;

import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.lwjgl.opengl.GL11;

import com.mojang.authlib.GameProfile;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.common.ManagerGleiter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;

public class RenderGleiter
{
	private static RenderGleiter instance = new RenderGleiter();
	
	public static void onRender(RenderPlayerEvent event)
	{
		PlayerEntity pl = event.getPlayer();
		ItemStack gleiter = ManagerGleiter.getPlayerGleiter(pl);
		float rot = pl.yBodyRotO + (pl.yBodyRot-pl.yBodyRotO)*event.getPartialRenderTick();
		
		GL11.glPushMatrix();
		
		GlStateManager._enableDepthTest();
		GlStateManager._enableCull();
		
		FloatBuffer matF = GLAllocation.createFloatBuffer(16);
		event.getMatrixStack().last().pose().store(matF);
		
		GL11.glLoadMatrixf(matF);
		
		
		
		instance.render(gleiter, 0, 0, 0, rot, pl);
	
	
		GL11.glPopMatrix();
		GlStateManager._disableDepthTest();
		GlStateManager._disableCull();
	}
	
	public static void onRender(RenderHandEvent event)
	{
		ClientPlayerEntity pl = Minecraft.getInstance().player;
		ItemStack gleiter = ManagerGleiter.getPlayerGleiter(pl);
		float rot = pl.yBodyRotO + (pl.yBodyRot-pl.yBodyRotO)*event.getPartialTicks();
		
		GL11.glPushMatrix();

		GlStateManager._enableCull();
		
		GL11.glRotatef(pl.xRot +90,1,0,0);
		
		GL11.glRotatef(-90,1,0,0);
		
		GL11.glRotatef((pl.yHeadRotO + (pl.yHeadRot-pl.yHeadRotO)*event.getPartialTicks()), 0, 1, 0);
		instance.render(gleiter, 0, -1.5, 0, rot, pl);
		GL11.glPopMatrix();
	}
	
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/model/gleiter.png"); 
	
	public void render(ItemStack gleiter, double x, double y, double z, float rotation, PlayerEntity pl)
	{
//		Random r = new Random(1);
	
		
		
		GL11.glPushMatrix();
		
		
		
		
		GL11.glTranslated(x, y+4, z);		
		GL11.glRotatef(-rotation+180F, 0, 1, 0);
		
		
		
		Minecraft.getInstance().getTextureManager().bind(res);
		
		int color = 0xe15b5b;
		if(gleiter!=null)
		{
			color = Minecraft.getInstance().getItemColors().getColor(gleiter, 1);
		}
		renderGleiter(color);
		renderLines();
		
		
		ResourceLocation res = getTexture(pl.getGameProfile());
		if(res!=null)
		{
			Minecraft.getInstance().getTextureManager().bind(res);
			renderGleiter(0xFFFFFFFF);
		}
		
		GL11.glPopMatrix();
	}
	
	private static void renderGleiter(int color)
	{
		GL11.glBegin(GL11.GL_QUADS);
		//down
		// ----
		// X---
		addVertex(-2.0, -0.6, -1.0,     0, 1.0);
		addVertex(-1.0, -0.2, -1.0,  0.25, 1.0);
		addVertex(-1.0, -0.1,  0.0 , 0.25, 0.75);
		addVertex(-2.0, -0.5,  0.0 ,    0, 0.75);
		// ----
		// -X--
		addVertex(-1.0, -0.2, -1.0, 0.25, 1.0);
		addVertex( 0.0, -0.1, -1.0, 0.5 , 1.0);
		addVertex( 0.0, -0.0,  0.0, 0.5 , 0.75);
		addVertex(-1.0, -0.1,  0.0, 0.25, 0.75);
		// ----
		// --X-
		addVertex(0.0, -0.1, -1.0, 0.5 , 1.0);
		addVertex(1.0, -0.2, -1.0, 0.75, 1.0);
		addVertex(1.0, -0.1,  0.0, 0.75, 0.75);
		addVertex(0.0, -0.0,  0.0, 0.5 , 0.75);
		// ----
		// ---X
		addVertex(1.0, -0.2, -1.0, 0.75, 1.0);
		addVertex(2.0, -0.6, -1.0, 1   , 1.0);
		addVertex(2.0, -0.5,  0.0, 1   , 0.75);
		addVertex(1.0, -0.1,  0.0, 0.75, 0.75);
		// ---X
		// ----
		addVertex(1.0, -0.1, 0.0, 0.75, 0.75);
		addVertex(2.0, -0.5, 0.0, 1   , 0.75);
		addVertex(2.0, -0.6, 1.0, 1   , 0.5);
		addVertex(1.0, -0.2, 1.0, 0.75, 0.5);
		// --X-
		// ----
		addVertex(0.0, 0.0 , 0.0, 0.5 , 0.75);
		addVertex(1.0, -0.1, 0.0, 0.75, 0.75);
		addVertex(1.0, -0.2, 1.0, 0.75, 0.5);
		addVertex(0.0, -0.1, 1.0, 0.5 , 0.5);
		// -X--
		// ----
		addVertex(-1.0, -0.1, 0.0, 0.25, 0.75);
		addVertex( 0.0,  0.0, 0.0, 0.5 , 0.75);
		addVertex( 0.0, -0.1, 1.0, 0.5 , 0.5);
		addVertex(-1.0, -0.2, 1.0, 0.25, 0.5);
		// X---
		// ----
		addVertex(-2.0, -0.5, 0.0, 0   , 0.75);
		addVertex(-1.0, -0.1, 0.0, 0.25, 0.75);
		addVertex(-1.0, -0.2, 1.0, 0.25, 0.5);
		addVertex(-2.0, -0.6, 1.0, 0   , 0.5);
		
		int r = (color>>16) & 0xFF;
		int g = (color>>8) & 0xFF;
		int b = (color) &  0xFF;
		
		GL11.glColor4f(r / 255F, g/255F, b/255F, 1F);
		
		double d = 0;
		
		//up
		// ----
		// X---
		addVertex(-2.0, -0.5 +d,  0.0, 0   , 0.25);
		addVertex(-1.0, -0.1 +d,  0.0, 0.25, 0.25);
		addVertex(-1.0, -0.2 +d, -1.0, 0.25, 0.0);
		addVertex(-2.0, -0.6 +d, -1.0, 0   , 0.0);
		// ----
		// -X--
		addVertex(-1.0, -0.1 +d,  0.0, 0.25, 0.25);
		addVertex( 0.0, -0.0 +d,  0.0, 0.5 , 0.25);
		addVertex( 0.0, -0.1 +d, -1.0, 0.5 , 0.0);
		addVertex(-1.0, -0.2 +d, -1.0, 0.25, 0.0);
		// ----
		// --X-
		addVertex(0.0, -0.0 +d,  0.0, 0.5 , 0.25);
		addVertex(1.0, -0.1 +d,  0.0, 0.75, 0.25);
		addVertex(1.0, -0.2 +d, -1.0, 0.75, 0.0);
		addVertex(0.0, -0.1 +d, -1.0, 0.5 , 0.0);
		// ----
		// ---X
		addVertex(1.0, -0.1 +d,  0.0, 0.75, 0.25);
		addVertex(2.0, -0.5 +d,  0.0, 1   , 0.25);
		addVertex(2.0, -0.6 +d, -1.0, 1   , 0.0);
		addVertex(1.0, -0.2 +d, -1.0, 0.75, 0.0);
		// ---X
		// ----
		addVertex(1.0, -0.2 +d, 1.0, 0.75, 0.5);
		addVertex(2.0, -0.6 +d, 1.0, 1   , 0.5);
		addVertex(2.0, -0.5 +d, 0.0, 1   , 0.25);
		addVertex(1.0, -0.1 +d, 0.0, 0.75, 0.25);
		// --X-
		// ----
		addVertex(0.0, -0.1 +d, 1.0, 0.5 , 0.5);
		addVertex(1.0, -0.2 +d, 1.0, 0.75, 0.5);
		addVertex(1.0, -0.1 +d, 0.0, 0.75, 0.25);
		addVertex(0.0, -0.0 +d, 0.0, 0.5 , 0.25);
		// -X--
		// ----
		addVertex(-1.0, -0.2 +d, 1.0, 0.25, 0.5);
		addVertex( 0.0, -0.1 +d, 1.0, 0.5 , 0.5);
		addVertex( 0.0, -0.0 +d, 0.0, 0.5 , 0.25);
		addVertex(-1.0, -0.1 +d, 0.0, 0.25, 0.25);
		// X---
		// ----
		addVertex(-2.0, -0.6 +d, 1.0, 0   , 0.5);
		addVertex(-1.0, -0.2 +d, 1.0, 0.25, 0.5);
		addVertex(-1.0, -0.1 +d, 0.0, 0.25, 0.25);
		addVertex(-2.0, -0.5 +d, 0.0, 0   , 0.25);
		
		GL11.glEnd();
		
		GL11.glColor4f(1F, 1F, 1F, 1F);
	}
	
	private static void renderLines()
	{
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		
		GL11.glBegin(GL11.GL_LINES);
		
		double x=-0.35,y=-2.55,z=0;
		
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(-2.0, -0.6, -1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(-1.0, -0.2, -1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(0.0, -0.1, -1.0);	
//		GL11.glVertex3d(x,y,z);
//		GL11.glVertex3d(1.0, -0.2, -1.0);	
//		GL11.glVertex3d(x,y,z);
//		GL11.glVertex3d(2.0, -0.6, -1.0);
		
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(-2.0, -0.6, 1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(-1.0, -0.2, 1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(0.0, -0.1, 1.0);	
//		GL11.glVertex3d(x,y,z);
//		GL11.glVertex3d(1.0, -0.2, 1.0);	
//		GL11.glVertex3d(x,y,z);
//		GL11.glVertex3d(2.0, -0.6, 1.0);
		
		x=0.35;
		
//		GL11.glVertex3d(x,y,z);
//		GL11.glVertex3d(-2.0, -0.6, -1.0);	
//		GL11.glVertex3d(x,y,z);
//		GL11.glVertex3d(-1.0, -0.2, -1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(0.0, -0.1, -1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(1.0, -0.2, -1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(2.0, -0.6, -1.0);
		
//		GL11.glVertex3d(x,y,z);
//		GL11.glVertex3d(-2.0, -0.6, 1.0);	
//		GL11.glVertex3d(x,y,z);
//		GL11.glVertex3d(-1.0, -0.2, 1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(0.0, -0.1, 1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(1.0, -0.2, 1.0);	
		GL11.glVertex3d(x,y,z);
		GL11.glVertex3d(2.0, -0.6, 1.0);
		
		GL11.glEnd();
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}
	
	private static void addVertex(double x, double y, double z, double u, double v)
	{
		GL11.glTexCoord2d(u, v);
		GL11.glVertex3d(x, y, z);
	}
	
	private ResourceLocation getTexture(GameProfile prof)
	{
		String name = prof.getName().toLowerCase();
		
		List<String> rs_name = Arrays.asList("mcenderdragon", "mantes", "wugand", "f_roepert");
		List<UUID> rs_uuid = Arrays.asList(UUID.fromString("3cf92882-db51-4816-948f-78a81087f886"), UUID.fromString("62280c0e-9f1d-4f18-b04c-d086a0060b5b"), UUID.fromString("e72a9a48-a7a5-4070-98fd-eabac76bc9cf"), UUID.fromString("ac6d3aad-bba2-412d-bcb9-c850068d6d3b"));
		
		if(rs_uuid.contains(prof.getId()))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/rs_schirm.png");
		}
		if(rs_name.contains(name))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/rs_schirm.png");
		}
		
		//krypto
		if(prof.getId().equals(UUID.fromString("c1b7353e-5e74-4d88-b507-524330b03210")) || prof.getId().equals(UUID.fromString("ab28e20a-b30f-4e6b-ba1f-2b658c8cd270")))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/kry_schirm.png");
		}
		else if(name.equals("masterwolf164") || name.equals("JajaSteele"))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/kry_schirm.png");
		}
		
		//patreon
		if(prof.getId().equals(UUID.fromString("5e2fbfc2-7874-4081-854b-5fbf0d1cf46f")))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/realm_schirm.png");
		}
		else if(name.equals("therealm18"))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/realm_schirm.png");
		}
		
		return null;
	}
}
