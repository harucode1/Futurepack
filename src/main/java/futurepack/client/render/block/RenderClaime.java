package futurepack.client.render.block;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.common.block.misc.TileEntityClaime;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.vector.Matrix4f;

public class RenderClaime extends TileEntityRenderer<TileEntityClaime>
{
	
	public RenderClaime(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}

	@Override
	public void render(TileEntityClaime tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLight, int combinedOverlayIn) 
	{
		tile.getLevel().getProfiler().push("renderClaime");
		matrixStackIn.pushPose();
		
//		GlStateManager.disableTexture();
//		//GL11.glDisable(GL11.GL_ALPHA_TEST);
//		GlStateManager.disableLighting();
		//GL11.glDisable(GL11.GL_CULL_FACE);
		//GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
		
		IVertexBuilder builder = bufferIn.getBuffer(RenderType.leash());
		
		GlStateManager._color4f(1F, 1F, 1F, 1F);
		
		float x=tile.mx, y=tile.my+1, z=tile.mz;
		
		float d = 0.03125F;
		drawQube(builder, x-tile.x-d+1, y-tile.y, z-tile.z-d+1, x-tile.x+d+1, y, z-tile.z+d+1,		1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x-tile.x-d+1, y-tile.y, z+tile.z-d,   x-tile.x+d+1, y, z+tile.z+d,		1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x+tile.x-d,   y-tile.y, z-tile.z-d+1, x+tile.x+d,   y, z-tile.z+d+1,		1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x+tile.x-d,   y-tile.y, z+tile.z-d,   x+tile.x+d,   y, z+tile.z+d,		1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		
		drawQube(builder, x-tile.x-d+1, y-d,        z-tile.z+1, x-tile.x+d+1, y+d,        z+tile.z,	1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x+tile.x-d,   y-d,        z-tile.z+1, x+tile.x+d,   y+d,        z+tile.z, 1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x-tile.x-d+1, y-tile.y-d, z-tile.z+1, x-tile.x+d+1, y-tile.y+d, z+tile.z, 1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x+tile.x-d,   y-tile.y-d, z-tile.z+1, x+tile.x+d,   y-tile.y+d, z+tile.z, 1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		
		drawQube(builder, x-tile.x+1, y-d,        z-tile.z-d+1, x+tile.x, y+d,        z-tile.z+d+1,	1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x-tile.x+1, y-d,        z+tile.z-d,   x+tile.x, y+d,        z+tile.z+d,	1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x-tile.x+1, y-tile.y-d, z-tile.z-d+1, x+tile.x, y-tile.y+d, z-tile.z+d+1,	1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		drawQube(builder, x-tile.x+1, y-tile.y-d, z+tile.z-d,   x+tile.x, y-tile.y+d, z+tile.z+d,	1F,1F, 0F, 1F, matrixStackIn, combinedLight);
		float x1,y1,z1,x2,y2,z2;
		x1=x2=x;
		y1=y2=y-1;
		z1=z2=z;
		
		x1+=0.5-0.0625;
		y1+=0.5-0.0625;
		z1+=0.5-0.0625;
		x2+=0.5+0.0625;
		y2+=0.5+0.0625;
		z2+=0.5+0.0625;
		
		drawQube(builder, x1, y1, z1, x2, y2, z2, 1F, 0F, 0F, 1F, matrixStackIn, combinedLight);
		
		if(tile.renderAll)
		{
			GlStateManager._disableTexture();
			GlStateManager._disableDepthTest();
			
			Tessellator tes = Tessellator.getInstance();
			builder = tes.getBuilder();
			((BufferBuilder)builder).begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR_LIGHTMAP);
			float col = (float) (0.5+Math.sin(System.currentTimeMillis()%1000/500F*Math.PI)*0.5);
			d = 0.03125F *0.5F;
						
			drawQube(builder, x-tile.x-d+1, y-tile.y, z-tile.z-d+1, x-tile.x+d+1, y, z-tile.z+d+1,		col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x-tile.x-d+1, y-tile.y, z+tile.z-d,   x-tile.x+d+1, y, z+tile.z+d, 		col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x+tile.x-d,   y-tile.y, z-tile.z-d+1, x+tile.x+d,   y, z-tile.z+d+1, 		col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x+tile.x-d,   y-tile.y, z+tile.z-d,   x+tile.x+d,   y, z+tile.z+d, 		col,col, 0F, 1F, matrixStackIn, combinedLight);
			
			drawQube(builder, x-tile.x-d+1, y-d,        z-tile.z+1, x-tile.x+d+1, y+d,        z+tile.z, col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x+tile.x-d,   y-d,        z-tile.z+1, x+tile.x+d,   y+d,        z+tile.z, col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x-tile.x-d+1, y-tile.y-d, z-tile.z+1, x-tile.x+d+1, y-tile.y+d, z+tile.z, col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x+tile.x-d,   y-tile.y-d, z-tile.z+1, x+tile.x+d,   y-tile.y+d, z+tile.z, col,col, 0F, 1F, matrixStackIn, combinedLight);
			
			drawQube(builder, x-tile.x+1, y-d,        z-tile.z-d+1, x+tile.x, y+d,        z-tile.z+d+1, col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x-tile.x+1, y-d,        z+tile.z-d,   x+tile.x, y+d,        z+tile.z+d, 	col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x-tile.x+1, y-tile.y-d, z-tile.z-d+1, x+tile.x, y-tile.y+d, z-tile.z+d+1, col,col, 0F, 1F, matrixStackIn, combinedLight);
			drawQube(builder, x-tile.x+1, y-tile.y-d, z+tile.z-d,   x+tile.x, y-tile.y+d, z+tile.z+d, 	col,col, 0F, 1F, matrixStackIn, combinedLight);
			
			tes.end();
			
			GlStateManager._enableTexture();
			GlStateManager._enableDepthTest();
		}

		matrixStackIn.popPose();
			
		tile.getLevel().getProfiler().pop();
	}
	
	private void drawQube(IVertexBuilder builder, float x1, float y1, float z1, float x2, float y2, float z2, float red, float green, float blue, float alpha, MatrixStack matrixStack, int lightMap)
	{
		Matrix4f matrix = matrixStack.last().pose();
		
		builder.vertex(matrix, x1, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x2, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x1, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x2, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x1, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x2, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
	}


}
