package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import futurepack.api.Constants;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.modification.TileEntityRadar;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;

public class RenderRadar extends TileEntityRenderer<TileEntityRadar>
{
	public RenderRadar(TileEntityRendererDispatcher rendererDispatcherIn)
	{
		super(rendererDispatcherIn);
	}

	ModelRadar model = new ModelRadar();
	ResourceLocation texture = new ResourceLocation(Constants.MOD_ID, "textures/blocks/misc/radar.png");
	
	@Override
	public void render(TileEntityRadar tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		tile.getLevel().getProfiler().push("renderRadar");
		
		matrixStackIn.pushPose();
		matrixStackIn.translate(0.5, 1.5, 0.5);
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
		
		Direction direction = tile.getBlockState().getValue(BlockRotateableTile.FACING);

		if(direction == Direction.DOWN) {
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-180F));
			matrixStackIn.translate(0,-2,0);
		}
		else if(direction == Direction.NORTH) {
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-90F));
			matrixStackIn.translate(0,-1,1);
		}
		else if(direction == Direction.EAST) {
			matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(90F));
			matrixStackIn.translate(1,-1,0);
		}
		else if(direction == Direction.SOUTH) {
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
			matrixStackIn.translate(0,-1,-1);
		}
		else if(direction == Direction.WEST) {
			matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(-90F));
			matrixStackIn.translate(-1,-1,0);
		}
		
		IVertexBuilder builder = bufferIn.getBuffer(model.renderType(texture));

		tile.updateRotation(partialTicks);

		model.rotateSpiegel(tile.rotation);

		model.renderToBuffer(matrixStackIn, builder, combinedLightIn, combinedOverlayIn, 1F, 1F, 1F, 1F);
		
		matrixStackIn.popPose();
		tile.getLevel().getProfiler().pop();
	}

}
