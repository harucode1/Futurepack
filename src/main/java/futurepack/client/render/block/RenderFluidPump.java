package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.modification.TileEntityFluidPump;
import net.minecraft.block.BlockState;
import net.minecraft.block.DirectionalBlock;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;

public class RenderFluidPump extends TileEntityRenderer<TileEntityFluidPump>
{

	public RenderFluidPump(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}

	@Override
	public void render(TileEntityFluidPump te, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		
		te.getLevel().getProfiler().push("renderPump");
		BlockState state = te.getLevel().getBlockState(te.getBlockPos());
		if(state.getBlock()==ModifiableBlocks.fluid_pump)
		{
			if(te.getFluid()!=null && !te.getFluid().getFluidStack().isEmpty())
			{
				//GlStateManager.enableBlendProfile(Profile.PLAYER_SKIN);
				
				
				Direction face = state.getValue(DirectionalBlock.FACING);
				float p = 0.0625F;
				float x=0F,y=0F,z=0F;
				
				y+=p*2.1F;
				
				float w=3.8F*p, h=p*11, d=3.8F*p;
				
				switch(face)
				{
				case SOUTH:
					z+=p*10.1F;
					x+=p*1.1F;
					break;
				case NORTH:
					z+=p*2.1F;
					x+=p*11.1F;
					break;
				case EAST:
					z+=p*11.1F;
					x+=p*10.1F;
					break;
				case WEST:
					x+=p*2.1F;
					z+=p*1.1F;
					break;
				case UP:
					z+=p*2.1F;
					y+=p*8F;
					x+=p*1.1F;
					
					h=3.8F*p;
					d=11.8F*p;
					break;
				case DOWN:
					x+=p*1.1F;
					z+=p*2.1F;
					h=3.8F*p;
					d=11.8F*p;
					break;
				}
				
				RenderFluidTank.renderTank(x, y, z, w, h, d, te.getFluid(), bufferIn, matrixStackIn, te.getLevel(), te.getBlockPos());		
				
			}
		}
		te.getLevel().getProfiler().pop();
	}
}
