package futurepack.client.render.block;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;

import futurepack.common.block.logistic.BlockWireRedstone;
import futurepack.common.block.logistic.RedstoneSystem;
import futurepack.common.block.logistic.RedstoneSystem.EnumRedstone;
import futurepack.common.block.logistic.TileEntityWireRedstone;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.RedstoneWireBlock;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class RenderWireRedstone extends RenderFastHologram<TileEntityWireRedstone>
{
	public RenderWireRedstone(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	private void renderWire(TileEntityWireRedstone t,double x, double y, double z, float f1, MatrixStack matrixStackIn, final IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		GL11.glPushMatrix();
		GlStateManager._blendFunc(SourceFactor.ONE.value, DestFactor.ZERO.value);
		GL11.glTranslated(x, y, z);		
		
		World w = t.getLevel();
		BlockPos jkl = t.getBlockPos();
		BlockState state = t.getBlockState();
		
		if(state.hasProperty(BlockWireRedstone.REDSTONE) && state.getValue(BlockWireRedstone.REDSTONE).booleanValue()
				&& state.hasProperty(BlockWireRedstone.REDSTONE_DUST) && state.getValue(BlockWireRedstone.REDSTONE_DUST).booleanValue())
		{
			renderRedstone(w, jkl, Direction.SOUTH, state.getValue(RedstoneSystem.STATE)!=EnumRedstone.OFF, matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn);
		}
			
		
		GL11.glPopMatrix();
	}
	
	@Override
	public final void renderDefault(TileEntityWireRedstone tile, float partialTicks, MatrixStack matrixStackIn, final IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		BlockPos pos = tile.getBlockPos();
		
		renderWire(tile, pos.getX(), pos.getY(), pos.getZ(), partialTicks, matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn);
	}	
	
	private void renderRedstone(World w, BlockPos pos, Direction face, boolean on, MatrixStack matrixStackIn, final IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		w.getProfiler().push("renderRedstone");
		BlockState state = Blocks.REDSTONE_WIRE.defaultBlockState().setValue(RedstoneWireBlock.POWER, on ? 15 : 0);
		state = Blocks.REDSTONE_WIRE.updateShape(state, Direction.UP, state, w, pos, pos.above());
		HelperRenderBlocks.renderBlockSlow(state, pos, w, matrixStackIn, bufferIn);
		w.getProfiler().pop();
	}
}
