package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.common.block.logistic.frames.TileEntityMovingBlocks;
import futurepack.common.block.misc.TileEntityFallingTree;
import futurepack.depend.api.MiniWorld;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;

public class RenderMovingBlocksFast extends TileEntityRenderer<TileEntityMovingBlocks>
{

	public RenderMovingBlocksFast(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}
	
	@Override
	public boolean shouldRenderOffScreen(TileEntityMovingBlocks te)
	{
		return true;
	}

	@Override
	public void render(TileEntityMovingBlocks te, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		GlStateManager._enableCull();
		
		MiniWorld world = te.getMiniWorld();
		if(world!=null)
		{
			float progress =  1F-( ( te.ticks-partialTicks ) /te.maxticks);
			
			matrixStackIn.pushPose();
			
			
			matrixStackIn.translate(progress * te.getDirection().getX(), progress* te.getDirection().getY(), progress* te.getDirection().getZ());
			
			HelperRenderBlocks.renderFast(world, partialTicks, matrixStackIn, bufferIn);
			
			
			
			
			matrixStackIn.popPose();
			//HelperRenderBlocks.renderFastBase(world, buf); // <-- geht
		}
	}

}
