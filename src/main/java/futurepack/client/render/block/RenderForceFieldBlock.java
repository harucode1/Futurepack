package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.FacingUtil;
import futurepack.client.render.entity.RenderForceField;
import futurepack.common.block.misc.TileEntityForceField;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class RenderForceFieldBlock extends RenderFastHologram<TileEntityForceField>
{
	
	public RenderForceFieldBlock(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}
	
	@Override
	public void renderDefault(TileEntityForceField te, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		World w = te.getLevel();
		BlockPos pos = te.getBlockPos();
		BlockState state = w.getBlockState(pos);
		
		boolean[] visible;
		visible = new boolean[FacingUtil.VALUES.length];
		
		for(Direction face : FacingUtil.VALUES)
		{
			visible[face.ordinal()] = !state.skipRendering(w.getBlockState(pos.relative(face)), face);//  !w.isSideSolid(pos.offset(face), face.getOpposite(), false);	
		}
		//sides = new boolean[]{true,true,true, true, true, true};
		
		float minU= -0.5F;
		float minV= -0.5F;
		float maxU= 0.5F;
		float maxV= 0.5F;
	        
		float offset = (System.currentTimeMillis() % 2300) / 2300F;
		
		minU +=offset;
		minV +=offset;
		maxU +=offset;
		maxV +=offset;
	        
		AxisAlignedBB bb = new AxisAlignedBB(0, 0, 0, 1, 1, 1);
		
		RenderForceField.renderSides(bufferIn, matrixStackIn, bb, minU, minV, maxU, maxV, visible, 0, 153, 255, combinedLightIn, combinedOverlayIn);
	}
}
