package futurepack.client.render.block;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.model.ModelRenderer.ModelBox;

public class ModelNeonEngine2 extends SegmentedBlockModel
{
	//fields
	ModelRenderer Energieleiter;
	ModelRenderer HalterH;
	ModelRenderer Spulle1;
	ModelRenderer Spulle2;
	ModelRenderer Spulle3;
	ModelRenderer Spulle4;
	ModelRenderer HalterV;
	ModelRenderer Eisenkern1;
	ModelRenderer Eisenkern2;
	ModelRenderer Eisenkern3;
	ModelRenderer Eisenkern4;
	ModelRenderer Fixirung;
	
	ModelBox box1, box2, box3, box4;
	
	public ModelNeonEngine2()
	{
		super(RenderType::entitySolid);
		 
		texWidth = 64;
		texHeight = 64;
		
		Energieleiter = new ModelRenderer(this, 0, 40);
		Energieleiter.addBox(-4F, -4F, -8F, 8, 8, 16);
		Energieleiter.setPos(0F, 0F, 0F);
		Energieleiter.setTexSize(64, 64);
		Energieleiter.mirror = true;
		setRotation(Energieleiter, 0F, 0F, 0F);
		HalterH = new ModelRenderer(this, 0, 21);
		HalterH.addBox(-8F, -8F, 4F, 16, 16, 3);
		HalterH.setPos(0F, 0F, 0F);
		HalterH.setTexSize(64, 64);
		HalterH.mirror = true;
		setRotation(HalterH, 0F, 0F, 0F);
		Spulle1 = new ModelRenderer(this, 0, 0);
		Spulle1.addBox(-2F, -7F, -0.1F, 4, 4, 8);
		Spulle1.setPos(0F, 0F, 0F);
		Spulle1.setTexSize(64, 64);
		Spulle1.mirror = true;
		setRotation(Spulle1, 0F, 0F, 0.7853982F);
		Spulle2 = new ModelRenderer(this, 0, 0);
		Spulle2.addBox(-2F, -7F, -0.1F, 4, 4, 8);
		Spulle2.setPos(0F, 0F, 0F);
		Spulle2.setTexSize(64, 64);
		Spulle2.mirror = true;
		setRotation(Spulle2, 0F, 0F, -0.7853982F);
		Spulle3 = new ModelRenderer(this, 0, 0);
		Spulle3.addBox(-2F, -7F, -0.1F, 4, 4, 8);
		Spulle3.setPos(0F, 0F, 0F);
		Spulle3.setTexSize(64, 64);
		Spulle3.mirror = true;
		setRotation(Spulle3, 0F, 0F, -2.356194F);
		Spulle4 = new ModelRenderer(this, 0, 0);
		Spulle4.addBox(-2F, -7F, -0.1F, 4, 4, 8);
		Spulle4.setPos(0F, 0F, 0F);
		Spulle4.setTexSize(64, 64);
		Spulle4.mirror = true;
		setRotation(Spulle4, 0F, 0F, 2.356194F);
		HalterV = new ModelRenderer(this, 0, 21);
		HalterV.addBox(-8F, -8F, -3F, 16, 16, 3);
		HalterV.setPos(0F, 0F, 0F);
		HalterV.setTexSize(64, 64);
		HalterV.mirror = true;
		setRotation(HalterV, 0F, 0F, 0F);
		Eisenkern1 = new ModelRenderer(this, 17, 0);
		Eisenkern1.addBox(-1F, -10F, 1F, 2, 4, 2);
		Eisenkern1.setPos(0F, 0F, 0F);
		Eisenkern1.setTexSize(64, 64);
		Eisenkern1.mirror = true;
		setRotation(Eisenkern1, 0F, 0F, 0.7853982F);
		Eisenkern2 = new ModelRenderer(this, 17, 0);
		Eisenkern2.addBox(-1F, -10F, 1F, 2, 4, 2);
		Eisenkern2.setPos(0F, 0F, 0F);
		Eisenkern2.setTexSize(64, 64);
		Eisenkern2.mirror = true;
		setRotation(Eisenkern2, 0F, 0F, 2.356194F);
		Eisenkern3 = new ModelRenderer(this, 17, 0);
		Eisenkern3.addBox(-1F, -10F, 1F, 2, 4, 2);
		Eisenkern3.setPos(0F, 0F, 0F);
		Eisenkern3.setTexSize(64, 64);
		Eisenkern3.mirror = true;
		setRotation(Eisenkern3, 0F, 0F, -2.356194F);
		Eisenkern4 = new ModelRenderer(this, 17, 0);
		Eisenkern4.addBox(-1F, -10F, 1F, 2, 4, 2);
		Eisenkern4.setPos(0F, 0F, 0F);
		Eisenkern4.setTexSize(64, 64);
		Eisenkern4.mirror = true;
		setRotation(Eisenkern4, 0F, 0F, -0.7853982F);
		Fixirung = new ModelRenderer(this, 26, 0);
		Fixirung.addBox(-5F, -5F, -4F, 10, 10, 1);
		Fixirung.setPos(0F, 0F, 0F);
		Fixirung.setTexSize(64, 64);
		Fixirung.mirror = true;
		setRotation(Fixirung, 0F, 0F, 0F);

	}
	
	@Override
	public ModelRenderer[] getParts() 
	{
		return new ModelRenderer[] {Energieleiter, HalterH, Spulle1, Spulle2, Spulle3, Spulle4, HalterV, Eisenkern1, Eisenkern2, Eisenkern3, Eisenkern4, Fixirung};
	}
	
	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.xRot = x;
		model.yRot = y;
		model.zRot = z;
	}
	
	public void setOut(double d)
	{
		float[] offset = new float[] {0+0.1F, 0.5F, 1F+0.2F, 1.5F - 0.25F};
		float[][] axes = new float[][] {{-1F, 1F}, {-1,-1}, {1, -1}, {1,1}};
		ModelRenderer[] pistons = new ModelRenderer[] {Eisenkern1, Eisenkern2, Eisenkern3, Eisenkern4};
		
		for(int i=0;i<4;i++)
		{
			float f = 1+ (float) Math.sin(2*Math.PI*d + Math.PI * offset[i]);
			pistons[i].x = f * axes[i][0];
			pistons[i].y = f * axes[i][1];
		}
	}
}
