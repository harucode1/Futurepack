package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ModelRadar extends Model {
    private final ModelRenderer Radar;
    private final ModelRenderer Spiegel;
    private final ModelRenderer boneWest;
    private final ModelRenderer SpiegelWest_r1;
    private final ModelRenderer boneOst;
    private final ModelRenderer SpiegelOst_r1;
    private final ModelRenderer boneParabolEmpf;
    private final ModelRenderer EmpfArm_r1;
    private final ModelRenderer boneLMB;

    public ModelRadar() {
        super(RenderType::entityCutout);
        texWidth = 128;
        texHeight = 128;

        Radar = new ModelRenderer(this);
        Radar.setPos(0.0F, 24.0F, 0.0F);
        Radar.texOffs(0, 0).addBox(-8.0F, -6.0F, -8.0F, 16.0F, 6.0F, 16.0F, 0.0F, false);
        Radar.texOffs(49, 0).addBox(-4.0F, -7.0F, -4.0F, 8.0F, 1.0F, 8.0F, 0.0F, false);

        Spiegel = new ModelRenderer(this);
        Spiegel.setPos(0.0F, -7.0F, 0.0F);
        Radar.addChild(Spiegel);
        Spiegel.texOffs(64, 15).addBox(-2.5F, -2.0F, -2.5F, 5.0F, 2.0F, 5.0F, 0.0F, false);
        Spiegel.texOffs(0, 23).addBox(-1.5F, -3.0F, -4.0F, 3.0F, 2.0F, 10.0F, 0.0F, false);
        Spiegel.texOffs(17, 24).addBox(-4.0F, -8.0F, 5.5F, 8.0F, 6.0F, 1.0F, 0.0F, false);

        boneWest = new ModelRenderer(this);
        boneWest.setPos(4.0F, -5.0F, 6.0F);
        Spiegel.addChild(boneWest);


        SpiegelWest_r1 = new ModelRenderer(this);
        SpiegelWest_r1.setPos(-4.0F, 5.0F, -6.0F);
        boneWest.addChild(SpiegelWest_r1);
        setRotationAngle(SpiegelWest_r1, 0.0F, 0.7854F, 0.0F);
        SpiegelWest_r1.texOffs(36, 25).addBox(-2.0F, -7.5F, 6.5F, 6.0F, 5.0F, 1.0F, 0.0F, false);

        boneOst = new ModelRenderer(this);
        boneOst.setPos(-4.0F, -5.0F, 6.0F);
        Spiegel.addChild(boneOst);


        SpiegelOst_r1 = new ModelRenderer(this);
        SpiegelOst_r1.setPos(4.0F, 5.0F, -6.0F);
        boneOst.addChild(SpiegelOst_r1);
        setRotationAngle(SpiegelOst_r1, 0.0F, -0.7854F, 0.0F);
        SpiegelOst_r1.texOffs(51, 25).addBox(-4.0F, -7.5F, 6.5F, 6.0F, 5.0F, 1.0F, 0.0F, false);

        boneParabolEmpf = new ModelRenderer(this);
        boneParabolEmpf.setPos(0.0F, -2.0F, -4.0F);
        Spiegel.addChild(boneParabolEmpf);


        EmpfArm_r1 = new ModelRenderer(this);
        EmpfArm_r1.setPos(0.0F, -0.4F, 0.5F);
        boneParabolEmpf.addChild(EmpfArm_r1);
        setRotationAngle(EmpfArm_r1, -0.6109F, 0.0F, 0.0F);
        EmpfArm_r1.texOffs(0, 7).addBox(-1.0F, -0.6F, -3.5F, 2.0F, 2.0F, 4.0F, 0.0F, false);

        boneLMB = new ModelRenderer(this);
        boneLMB.setPos(0.0F, -2.4F, -2.5F);
        boneParabolEmpf.addChild(boneLMB);
        boneLMB.texOffs(0, 0).addBox(-1.5F, -2.0F, -1.0F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        boneLMB.texOffs(0, 25).addBox(-1.0F, -1.6F, 2.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
    }

    public void rotateSpiegel(float degree) {
        Spiegel.yRot = degree;
    }

    @Override
    public void renderToBuffer(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        Radar.render(matrixStack, buffer, packedLight, packedOverlay);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }
}
