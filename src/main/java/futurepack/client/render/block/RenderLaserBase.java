package futurepack.client.render.block;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.common.block.modification.BlockEntityLaserBase;
import futurepack.common.block.modification.TileEntityLaserBase;
import futurepack.common.block.modification.TileEntityRocketLauncher;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.math.vector.Vector4f;

public class RenderLaserBase extends TileEntityRenderer<TileEntityLaserBase>
{

	private ModelLaserBase eater = new ModelEater();
	private ModelLaserBase rocket = new ModelRocketLauncher();

	public RenderLaserBase(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}

	
	@Override
	public void render(TileEntityLaserBase tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		tile.getLevel().getProfiler().push("renderLaserBase");
		
		ModelLaserBase eater = this.eater;
		if(tile.getClass()==TileEntityRocketLauncher.class)
			eater = rocket;
		
		matrixStackIn.pushPose();
		Vector3d src = tile.blockPos;
		matrixStackIn.translate(0.5, 0.375, 0.5);

		
		float rotX = 0;
		float rotY = 0;
		if(src!=null)
		{
			Vector3d base = tile.getVecPos();
			float dx = (float) -(src.x - base.x);
			float dy = (float) -(src.y - base.y);
			float dz = (float) (src.z - base.z);
			float dis = (float) Math.sqrt(dx*dx + dz*dz);
			
			rotY = (float) Math.atan(dx / dz);
			rotX = (float) Math.atan(dy / dis);
			
			if(dz<0)
				rotY-= Math.PI;		
		}
		
		if(tile.pass == 0)
		{	
			matrixStackIn.pushPose();

			if(tile.getBlockState().getValue(BlockEntityLaserBase.ROTATION_VERTICAL) == Direction.DOWN)
			{
				matrixStackIn.translate(0, 0.25, 0);
				eater.rotateXUnit((float) (rotX+Math.PI));
				eater.rotateYUnit(-rotY);
			}
			else
			{
				matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
				eater.rotateXUnit(rotX);
				eater.rotateYUnit(rotY);
			}
			IVertexBuilder buf = bufferIn.getBuffer(eater.renderType(tile.getTexture()));
			eater.renderToBuffer(matrixStackIn, buf, combinedLightIn, combinedOverlayIn, 1F, 1F, 1F, 1F);

			matrixStackIn.popPose();
		}
//		if(true || tile.pass == 1)//we dont have render passes anymore
		{		
			if(tile.getLaser()!=null)
			{
				src = tile.entityPos;
				if(src!=null && tile.lastMessageTime > System.currentTimeMillis() - 333)
				{
					Vector3d base = tile.getVecPos();
					float dx = (float) -(src.x - base.x);
					float dy = (float) -(src.y - base.y);
					float dz = (float) (src.z - base.z);
					double dis = Math.sqrt(dx*dx+dy*dy+dz*dz);
		
					if(tile.getBlockState().getValue(BlockEntityLaserBase.ROTATION_VERTICAL) == Direction.DOWN)
					{
						matrixStackIn.translate(0, -1, 0);
					}
						
					renderBeam((float) dis, rotX, rotY, tile.getLaserColor(), tile.getLaser(), matrixStackIn, bufferIn);
				}
			}		
		}

		matrixStackIn.popPose();
		tile.getLevel().getProfiler().pop();
	}
	
	
	private void renderBeam(float dis, float rotX, float rotY, int col3i, ResourceLocation laser, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn)
	{
		GL11.glPushMatrix();
		Minecraft.getInstance().getTextureManager().bind(laser);
		
		GL11.glEnable(GL11.GL_BLEND);
		GlStateManager._blendFuncSeparate(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);
		
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(true);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		
		matrixStackIn.translate(0, 0.625+0.0625, 0);
		matrixStackIn.mulPose(Vector3f.YP.rotation(-rotY));
		matrixStackIn.mulPose(Vector3f.XP.rotation(rotX));
		
		float r = ((col3i>>16) & 0xFF) /255F;
		float g = ((col3i>>8) & 0xFF) /255F;
		float b = ((col3i>>0) & 0xFF) /255F;
		GL11.glColor4f(r, g, b, 1F);

		float width = 0.125F;
		float texStart = (float) (dis / (2*width) - (System.currentTimeMillis()%1000)*0.005);
		float texEnd = (float) (- (System.currentTimeMillis()%1000)*0.005);
		
		GL11.glBegin(GL11.GL_QUADS);

		Matrix4f mat = matrixStackIn.last().pose();
		
		addVertexWithUV(-width, 0F, 0.5F, 0F, texStart, mat);
		addVertexWithUV(+width, 0F, 0.5F, 1F, texStart, mat);
		addVertexWithUV(+width, 0F, dis, 1F, texEnd, mat);
		addVertexWithUV(-width, 0F, dis, 0F, texEnd, mat);
		
		addVertexWithUV(+width, 0, 0.5F, 0, texStart, mat);
		addVertexWithUV(-width, 0, 0.5F, 1, texStart, mat);
		addVertexWithUV(-width, 0, dis, 1, texEnd, mat);
		addVertexWithUV(+width, 0, dis, 0, texEnd, mat);

		addVertexWithUV(0, +width, 0.5F, 0, texStart, mat);
		addVertexWithUV(0, -width, 0.5F, 1, texStart, mat);
		addVertexWithUV(0, -width, dis, 1, texEnd, mat);
		addVertexWithUV(0, +width, dis, 0, texEnd, mat);

		addVertexWithUV(0, -width, 0.5F, 0, texStart, mat);
		addVertexWithUV(0, +width, 0.5F, 1, texStart, mat);
		addVertexWithUV(0, +width, dis, 1, texEnd, mat);
		addVertexWithUV(0, -width, dis, 0, texEnd, mat);

		GL11.glEnd();
		
		GL11.glRotated(-Math.toDegrees(rotX), 1, 0, 0);
		GL11.glRotated(Math.toDegrees(rotY), 0, 1, 0);
		GL11.glTranslated(0, -0.625, 0);
		GL11.glDepthMask(true);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		
		GL11.glColor4f(1, 1, 1, 1);//Tessellator.getInstance().getVertexBuffer().addVertexWithUV(p_178985_1_, p_178985_3_, p_178985_5_, p_178985_7_, p_178985_9_);
		GL11.glPopMatrix();
	}
	
	private static void addVertexWithUV(float x, float y, float z, float u, float v, Matrix4f mat)
	{
		Vector4f vector4f = new Vector4f(x, y, z, 1.0F);
		vector4f.transform(mat);
		GL11.glTexCoord2f(u,v);
		GL11.glVertex3f(vector4f.x(), vector4f.y(), vector4f.z());
	}
	
	private void renderBeam(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn)
	{
		IVertexBuilder builder = bufferIn.getBuffer(RenderType.translucentNoCrumbling());
		//pos color tex lightMap normal
	}
}
