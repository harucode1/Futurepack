package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.api.Constants;
import futurepack.common.block.misc.BlockDungeonSpawner;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityDungeonSpawner;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix3f;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3f;

public class RenderDungeonSpawner extends TileEntityRenderer<TileEntityDungeonSpawner>
{
	int color;
	public static ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/model/rauschen.png");
	
	public RenderDungeonSpawner(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}
	
	@Override
	public void render(TileEntityDungeonSpawner te, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		te.getLevel().getProfiler().push("renderDungeonSpawner");

		matrixStackIn.pushPose();
		//GL11.glDisable(GL11.GL_TEXTURE_2D);
		
		matrixStackIn.translate(0.5, 0.5, 0.5);
		BlockState state = te.getBlockState();
//		state = state.getActualState(te.getWorld(), te.getPos());
		if(state.getBlock()== MiscBlocks.dungeon_spawner)
		{
			matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(state.getValue(BlockDungeonSpawner.rotation) * -90F));
			
			Direction face = state.getValue(BlockDungeonSpawner.FACING);
			if(face== Direction.DOWN)
			{
				matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180));
			}
			else if(face.getAxis()!=Axis.Y)
			{
				matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90));
			}	
		}
		
		matrixStackIn.translate(0, -0.5+0.0325, 0);
		
		IVertexBuilder buf = bufferIn.getBuffer(RenderType.entityTranslucentCull(res));
		Matrix4f mat = matrixStackIn.last().pose();
		Matrix3f normals = matrixStackIn.last().normal();
		
		for(int i=0;i<te.getWaveCount();i++)
		{
			renderQuadCicle(buf, te.getWaveColors(i), i, mat, normals, combinedLightIn, combinedOverlayIn);
		}
			
		matrixStackIn.popPose();
		te.getLevel().getProfiler().pop();
	}
	
	
	// jeder ring 6 pixel breit und 2 pixel abstandt
	// 1px = 0.0625
	static double p = 0.0625;
	
	public void renderQuadCicle(IVertexBuilder buf, int[] colors, int radius, Matrix4f mat, Matrix3f normals, int lightmapUV, int overlayUV)
	{
		double a1 = 1 + p*8*radius; 						//a = seiten l�nge
		double a2 = a1 + 6*p;
		
//		Tessellator tes = Tessellator.getInstance();
//		BufferBuilder buf = tes.getBuffer();
//		
//		buf.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
		
		double distance = 5 / a1;
		
		double degrees = (360D - distance*colors.length) / colors.length; 
		double degree = 360D / colors.length; 
		
		for(int i=0;i<colors.length;i++)
		{
			this.color = colors[i];
			
			double from = 45+degree * i;			
			double to = from + degrees;
			
			renderPartSafe(buf, from, to, a1, a2, mat, normals, lightmapUV, overlayUV);
		}
//		tes.draw();
	}
	
	public void renderPartSafe(IVertexBuilder buf, double from, double to, double a1, double a2, Matrix4f mat, Matrix3f normals, int lightmapUV, int overlayUV)
	{	
		if(from < 45 && to > 45)
		{
			renderPart(buf, from, 45, a1, a2, mat, normals, lightmapUV, overlayUV);
			from = 45;
		}
		
		if(from < 135 && to > 135)
		{
			renderPart(buf, from, 135, a1, a2, mat, normals, lightmapUV, overlayUV);
			from = 135;
		}
		if(from < 225 && to > 225)
		{
			renderPart(buf, from, 225, a1, a2, mat, normals, lightmapUV, overlayUV);
			from = 225;
		}
		if(from < 315 && to > 315)
		{
			renderPart(buf, from, 315, a1, a2, mat, normals, lightmapUV, overlayUV);
			from = 315;
		}		
		
		renderPart(buf, from, to, a1, a2, mat, normals, lightmapUV, overlayUV);
	}
	
	public void renderPart(IVertexBuilder buf, double from, double to, double a1, double a2, Matrix4f mat, Matrix3f normals, int lightmapUV, int overlayUV)
	{
		addQuadCoord(buf, from, a2, mat, normals, lightmapUV, overlayUV);
		addQuadCoord(buf, to, a2, mat, normals, lightmapUV, overlayUV);
		addQuadCoord(buf, to, a1, mat, normals, lightmapUV, overlayUV);
		addQuadCoord(buf, from, a1, mat, normals, lightmapUV, overlayUV);
	}
	
	private void addQuadCoord(IVertexBuilder buf, double degrees, double r, Matrix4f mat, Matrix3f normals, int lightmapUV, int overlayUV)
	{
		degrees = Math.toRadians(degrees);		
		double max = Math.sqrt(2) * r * 0.5;
		
		float y=0;
		float x = (float) (Math.sin(degrees) *r);
		float z = (float) (Math.cos(degrees) *r);
		
		x = (float) Math.min(max, x);
		x = (float) Math.max(-max, x);
		z = (float) Math.min(max, z);
		z = (float) Math.max(-max, z);
		
		buf.vertex(mat, x, y, z).color(((color>>16) & 255), ((color>>8) & 255), (color & 255), ((color>>24) & 255));
		z += System.currentTimeMillis() % 100000 * 0.0001;
		x*=0.3;
		z*=0.3;
		buf.uv(x+0.5F, z+0.5F).overlayCoords(overlayUV).uv2(lightmapUV).normal(0, 1, 0).endVertex();
	}
}
