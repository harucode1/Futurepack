package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.misc.TileEntityNeonEngine;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3f;

public class RenderNeonEngine extends TileEntityRenderer<TileEntityNeonEngine>
{
	public RenderNeonEngine(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}

	ModelNeonEngine2 model = new ModelNeonEngine2();

	
	@Override
	public void render(TileEntityNeonEngine tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		tile.getLevel().getProfiler().push("renderNeonEngine");
		
		matrixStackIn.pushPose();
		matrixStackIn.translate(0.5, 0.5, 0.5);
		
		float d = 1F/16F;
		
		Direction meta = tile.getBlockState().getValue(BlockRotateableTile.FACING);
		
		
		if(meta== Direction.DOWN)
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-90));
		else if(meta== Direction.UP)
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
		else
		{
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));

//			GL11.glRotatef(90F*meta.getHorizontalIndex(), 0, 1, 0);
			matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(90F*meta.get2DDataValue()));
		}
		
		IVertexBuilder builder = bufferIn.getBuffer(model.renderType(tile.heatState.getTexture()));
		
		model.setOut( (System.currentTimeMillis()+tile.hashCode())%1000 * tile.heatState.getSpeed() );
		model.renderToBuffer(matrixStackIn, builder, combinedLightIn, combinedOverlayIn, 1F, 1F, 1F, 1F);
		
		matrixStackIn.popPose();
		tile.getLevel().getProfiler().pop();
	}

}
