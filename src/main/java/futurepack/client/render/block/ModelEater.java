package futurepack.client.render.block;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ModelEater extends ModelLaserBase
{
	//fields
    ModelRenderer Sockel;
    ModelRenderer RotationsfussSockel;
    ModelRenderer Stutze1;
    ModelRenderer Stutze2;
    ModelRenderer RotationsAchshalter1;
    ModelRenderer RotationsAchshalter2;
    ModelRenderer Laserkorper;
    ModelRenderer Laserschaft;
    ModelRenderer Laserfront1;
    ModelRenderer Laserfront2;
    ModelRenderer Lasermitte1;
    ModelRenderer Lasermitte2;
    ModelRenderer Lasertop;
    ModelRenderer LaserkorperHinten;
  
    public ModelEater()
    {
    	super(RenderType::entitySolid);
    	
    	texWidth = 64;
    	texHeight = 64;
    
    	Sockel = new ModelRenderer(this, 0, 42);
        Sockel.addBox(-8F, 0F, -8F, 16, 6, 16);
        Sockel.setPos(0F, 0F, 0F);
        Sockel.setTexSize(64, 64);
        Sockel.mirror = true;
        setRotation(Sockel, 0F, 0F, 0F);
        RotationsfussSockel = new ModelRenderer(this, 0, 28);
        RotationsfussSockel.addBox(-6F, 0F, -6F, 12, 2, 12);
        RotationsfussSockel.setPos(0F, -2F, 0F);
        RotationsfussSockel.setTexSize(64, 64);
        RotationsfussSockel.mirror = true;
        setRotation(RotationsfussSockel, 0F, 0F, 0F);
        Stutze1 = new ModelRenderer(this, 48, 32);
        Stutze1.addBox(3F, -6F, -2F, 1, 6, 4);
        Stutze1.setPos(0F, -2F, 0F);
        Stutze1.setTexSize(64, 64);
        Stutze1.mirror = true;
        setRotation(Stutze1, 0F, 0F, 0F);
        Stutze2 = new ModelRenderer(this, 48, 32);
        Stutze2.addBox(-4F, -6F, -2F, 1, 6, 4);
        Stutze2.setPos(0F, -2F, 0F);
        Stutze2.setTexSize(64, 64);
        Stutze2.mirror = true;
        setRotation(Stutze2, 0F, 0F, 0F);
        RotationsAchshalter1 = new ModelRenderer(this, 34, 11);
        RotationsAchshalter1.addBox(2F, -2F, -2F, 3, 4, 4);
        RotationsAchshalter1.setPos(0F, -10F, 0F);
        RotationsAchshalter1.setTexSize(64, 64);
        RotationsAchshalter1.mirror = true;
        setRotation(RotationsAchshalter1, 0F, 0F, 0F);
        RotationsAchshalter2 = new ModelRenderer(this, 34, 11);
        RotationsAchshalter2.addBox(-5F, -2F, -2F, 3, 4, 4);
        RotationsAchshalter2.setPos(0F, -10F, 0F);
        RotationsAchshalter2.setTexSize(64, 64);
        RotationsAchshalter2.mirror = true;
        setRotation(RotationsAchshalter2, 0F, 0F, 0F);
        Laserkorper = new ModelRenderer(this, 0, 14);
        Laserkorper.addBox(-2F, -3F, -4F, 4, 6, 8);
        Laserkorper.setPos(0F, -10F, 0F);
        Laserkorper.setTexSize(64, 64);
        Laserkorper.mirror = true;
        setRotation(Laserkorper, 0F, 0F, 0F);
        Laserschaft = new ModelRenderer(this, 30, 19);
        Laserschaft.addBox(-1.5F, -2.5F, -10F, 3, 3, 6);
        Laserschaft.setPos(0F, -10F, 0F);
        Laserschaft.setTexSize(64, 64);
        Laserschaft.mirror = true;
        setRotation(Laserschaft, 0F, 0F, 0F);
        Laserfront1 = new ModelRenderer(this, 48, 28);
        Laserfront1.addBox(1.5F, 0F, -11F, 1, 1, 3);
        Laserfront1.setPos(0F, -10F, 0F);
        Laserfront1.setTexSize(64, 64);
        Laserfront1.mirror = true;
        setRotation(Laserfront1, 0F, 0F, 0F);
        Laserfront2 = new ModelRenderer(this, 48, 28);
        Laserfront2.addBox(-2.5F, 0F, -11F, 1, 1, 3);
        Laserfront2.setPos(0F, -10F, 0F);
        Laserfront2.setTexSize(64, 64);
        Laserfront2.mirror = true;
        setRotation(Laserfront2, 0F, 0F, 0F);
        Lasermitte1 = new ModelRenderer(this, 48, 21);
        Lasermitte1.addBox(1.5F, -3F, -12F, 1, 3, 4);
        Lasermitte1.setPos(0F, -10F, 0F);
        Lasermitte1.setTexSize(64, 64);
        Lasermitte1.mirror = true;
        setRotation(Lasermitte1, 0F, 0F, 0F);
        Lasermitte2 = new ModelRenderer(this, 48, 21);
        Lasermitte2.addBox(-2.5F, -3F, -12F, 1, 3, 4);
        Lasermitte2.setPos(0F, -10F, 0F);
        Lasermitte2.setTexSize(64, 64);
        Lasermitte2.mirror = true;
        setRotation(Lasermitte2, 0F, 0F, 0F);
        Lasertop = new ModelRenderer(this, 48, 16);
        Lasertop.addBox(-1.5F, -3F, -12F, 3, 1, 4);
        Lasertop.setPos(0F, -10F, 0F);
        Lasertop.setTexSize(64, 64);
        Lasertop.mirror = true;
        setRotation(Lasertop, 0F, 0F, 0F);
        LaserkorperHinten = new ModelRenderer(this, 0, 2);
        LaserkorperHinten.addBox(-1F, -4F, -1F, 2, 6, 6);
        LaserkorperHinten.setPos(0F, -10F, 0F);
        LaserkorperHinten.setTexSize(64, 64);
        LaserkorperHinten.mirror = true;
        setRotation(LaserkorperHinten, 0F, 0F, 0F);
    }	
    
    @Override
    public void rotateYUnit(float y)
    {
    	RotationsfussSockel.yRot = y;
	    Stutze1.yRot = y;
	    Stutze2.yRot = y;
	    RotationsAchshalter1.yRot = y;
	    RotationsAchshalter2.yRot = y;
	    Laserkorper.yRot = y;
	    Laserschaft.yRot = y;
	    Laserfront1.yRot = y;
	    Laserfront2.yRot = y;
	    Lasermitte1.yRot = y;
	    Lasermitte2.yRot = y;
	    Lasertop.yRot = y;
	    LaserkorperHinten.yRot = y;
    }
    
    @Override
    public void rotateXUnit(float x)
    {
    	Laserkorper.xRot = x;
	    Laserschaft.xRot = x;
	    Laserfront1.xRot = x;
	    Laserfront2.xRot = x;
	    Lasermitte1.xRot = x;
	    Lasermitte2.xRot = x;
	    Lasertop.xRot = x;
	    LaserkorperHinten.xRot = x;
    }
  
    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
    	model.xRot = x;
    	model.yRot = y;
    	model.zRot = z;
    }

	@Override
	public ModelRenderer[] getParts() 
	{
		return new ModelRenderer[] {Sockel, RotationsfussSockel, Stutze1, Stutze2, RotationsAchshalter1, RotationsAchshalter2, Laserkorper, Laserschaft, Laserfront1, Laserfront2, Lasermitte1, Lasermitte2, Lasertop, LaserkorperHinten};
	}

}
