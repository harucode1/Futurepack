package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;

/**
 * Tiefenkernbohrer.tcn - TechneToTabulaImporter
 * Created using Tabula 7.0.0
 */
public class ModelDeepCoreMiner extends SegmentedBlockModel
{
    public ModelRenderer Arm;
    public ModelRenderer Kopf;
    public ModelRenderer Transformer;
    public ModelRenderer Bodenplatte;
    public ModelRenderer Bohrloch;
    public ModelRenderer Photonengenerator;
    public ModelRenderer Linse, Linse_glow;
    public ModelRenderer Fokusmechanik;
    public ModelRenderer Sicherheitsgitter;
    public ModelRenderer Verbindung4;
    public ModelRenderer Verbindung3;
    public ModelRenderer Verbindung2;
    public ModelRenderer Verbindung1;
    public ModelRenderer Lagereinheit3;
    public ModelRenderer Lagereinheit2;
    public ModelRenderer Lagereinheit1;

    public ModelDeepCoreMiner()
    {
    	super(RenderType::entityCutout);
    	
        this.texWidth = 256;
        this.texHeight = 256;
        this.Lagereinheit1 = new ModelRenderer(this, 0, 67);
        this.Lagereinheit1.setPos(0.0F, 0.0F, 0.0F);
        this.Lagereinheit1.addBox(-7.0F, -18.0F, -23.0F, 14, 14, 14, 0.0F);
        this.Kopf = new ModelRenderer(this, 104, 28);
        this.Kopf.setPos(0.0F, -48.0F, 0.0F);
        this.Kopf.addBox(-8.0F, 0.0F, -8.0F, 16, 16, 16, 0.0F);
        this.Verbindung4 = new ModelRenderer(this, 65, 73);
        this.Verbindung4.setPos(0.0F, 0.0F, 0.0F);
        this.Verbindung4.addBox(-9.0F, -47.0F, 14.0F, 2, 44, 4, 0.0F);
        this.setRotateAngle(Verbindung4, 3.141592653589793F, 8.742278000372485E-8F, 3.141592653589793F);
        this.Lagereinheit2 = new ModelRenderer(this, 0, 67);
        this.Lagereinheit2.setPos(0.0F, 0.0F, 0.0F);
        this.Lagereinheit2.addBox(-7.0F, -33.0F, -23.0F, 14, 14, 14, 0.0F);
        this.Arm = new ModelRenderer(this, 69, 39);
        this.Arm.setPos(0.0F, -22.0F, 17.0F);
        this.Arm.addBox(-3.0F, -19.0F, -4.0F, 6, 22, 8, 0.0F);
        this.setRotateAngle(Arm, 0.7435722351074219F, -0.0F, 0.0F);
        this.Fokusmechanik = new ModelRenderer(this, 172, 28);
        this.Fokusmechanik.setPos(0.0F, -20.0F, 0.0F);
        this.Fokusmechanik.addBox(-3.0F, -18.0F, -3.0F, 6, 16, 6, 0.0F);
        this.Transformer = new ModelRenderer(this, 0, 25);
        this.Transformer.setPos(0.0F, 0.0F, 0.0F);
        this.Transformer.addBox(-8.0F, -24.0F, 8.0F, 16, 24, 16, 0.0F);
        this.Verbindung1 = new ModelRenderer(this, 65, 73);
        this.Verbindung1.setPos(0.0F, 0.0F, 0.0F);
        this.Verbindung1.addBox(-9.0F, -47.0F, -2.0F, 2, 44, 4, 0.0F);
        this.Bodenplatte = new ModelRenderer(this, 106, 67);
        this.Bodenplatte.setPos(0.0F, 0.0F, 0.0F);
        this.Bodenplatte.addBox(-9.0F, -3.0F, -24.0F, 18, 3, 32, 0.0F);
        this.Bohrloch = new ModelRenderer(this, 78, 0);
        this.Bohrloch.setPos(0.0F, 0.0F, 0.0F);
        this.Bohrloch.addBox(-8.0F, -4.0F, -8.0F, 16, 1, 16, 0.0F);
        this.Sicherheitsgitter = new ModelRenderer(this, 86, 106);
        this.Sicherheitsgitter.setPos(0.0F, 0.0F, 0.0F);
        this.Sicherheitsgitter.addBox(-8.0F, -47.0F, -24.0F, 16, 44, 16, 0.0F);
        this.Lagereinheit3 = new ModelRenderer(this, 0, 67);
        this.Lagereinheit3.setPos(0.0F, 0.0F, 0.0F);
        this.Lagereinheit3.addBox(-7.0F, -48.0F, -23.0F, 14, 14, 14, 0.0F);
        this.Linse = new ModelRenderer(this, 67, 25);
        this.Linse.setPos(0.0F, 0.0F, 0.0F);
        this.Linse.addBox(-4.0F, -22.0F, -4.0F, 8, 2, 8, 0.0F);
        this.Linse_glow = new ModelRenderer(this, 188, 0);
        this.Linse_glow.setPos(0.0F, 0.0F, 0.0F);
        this.Linse_glow.addBox(-4.0F, -22.0F, -4.0F, 8, 2, 8, 0.0F);
        this.Verbindung2 = new ModelRenderer(this, 65, 73);
        this.Verbindung2.setPos(0.0F, 0.0F, 0.0F);
        this.Verbindung2.addBox(-9.0F, -47.0F, -1.0F, 2, 44, 4, 0.0F);
        this.setRotateAngle(Verbindung2, 3.141592653589793F, -8.742278000372485E-8F, 3.141592653589793F);
        this.Verbindung3 = new ModelRenderer(this, 65, 73);
        this.Verbindung3.setPos(0.0F, 0.0F, 0.0F);
        this.Verbindung3.addBox(-9.0F, -47.0F, -18.0F, 2, 44, 4, 0.0F);
        this.Photonengenerator = new ModelRenderer(this, 148, 0);
        this.Photonengenerator.setPos(0.0F, 0.0F, 0.0F);
        this.Photonengenerator.addBox(-5.0F, -32.0F, -5.0F, 10, 8, 10, 0.0F);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z)
    {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }
    
    public void setLaserPosition(float y)
    {
    	y*=16F;
    	Linse.y = y;
    	Linse_glow.y = y;
    	Fokusmechanik.y = y - 20F;
    }
    
    @Override
    public ModelRenderer[] getParts() 
    {
    	return new ModelRenderer[] {Lagereinheit1, Kopf, Verbindung4, Lagereinheit2, Arm, Fokusmechanik, Transformer, Verbindung1, Bodenplatte, Bohrloch, Sicherheitsgitter, Lagereinheit3, Linse, Verbindung2, Verbindung3, Photonengenerator};
    }
    
    @Override
    public void renderToBuffer(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) 
    {
    	super.renderToBuffer(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, 1F, 1F, 1F, alpha);
    	Linse_glow.render(matrixStackIn, bufferIn, 0xF000F0, packedOverlayIn, red, green, blue, alpha); //emmissiv rendering
    }
}
