package futurepack.client.render.block;

import java.util.ArrayList;
import java.util.WeakHashMap;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.common.FPConfig;
import futurepack.common.block.logistic.TileEntityPipeBase;
import futurepack.common.block.logistic.TileEntityPipeBase.ItemPath;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3f;

public class RenderPipe extends RenderFastHologram<TileEntityPipeBase>
{	
	public RenderPipe(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}
	
	@Override
	public void renderDefault(TileEntityPipeBase te, float partialTicks, MatrixStack matrix, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		te.getLevel().getProfiler().push("renderPipeItems");
		
		matrix.pushPose();
		
		matrix.translate(0.5, 0.5, 0.5);
		matrix.translate(0, -0.25, 0);
		
		if(Boolean.TRUE.equals(FPConfig.CLIENT.renderPipeItems.get()))
		{
			ArrayList<ItemPath> list = te.getItems();
			
			for(ItemPath ip : list) {
				if(ip.next < 0)
					continue;
				
				matrix.pushPose();
				
				float n = (ip.next - partialTicks)/10F;					
				if(ip.next > 5)
				{
					BlockPos c = ip.from;
					if(c != null)
					{
						int xo = c.getX() - te.getBlockPos().getX();
						int yo = c.getY() - te.getBlockPos().getY();
						int zo = c.getZ() - te.getBlockPos().getZ();
						matrix.translate(xo*-0.5, yo*-0.5, zo*-0.5);
						matrix.translate(xo*n, yo*n, zo*n);
					}
				}
				else if(ip.next < 5)
				{
					BlockPos c = ip.target;
					if(ip.path.size()>0)
					{
						c = ip.path.get(ip.path.size()-1);
					}
					int xo = te.getBlockPos().getX() - c.getX();
					int yo = te.getBlockPos().getY() - c.getY();
					int zo = te.getBlockPos().getZ() - c.getZ();
					matrix.translate(xo*-0.5, yo*-0.5, zo*-0.5);		
					matrix.translate(xo*n, yo*n, zo*n);
				}
				renderItem(te, ip.itemInPipe, matrix, bufferIn, combinedLightIn, combinedOverlayIn);
				
				matrix.popPose();
			}
			if(te.getStackWaiting()!=null)
			{
				renderItem(te, te.getStackWaiting(), matrix, bufferIn, combinedLightIn, combinedOverlayIn);
				for(ItemStack it : te.getRefind())
				{
					renderItem(te, it, matrix, bufferIn, combinedLightIn, combinedOverlayIn);
				}
			}
		}
		
		matrix.popPose();
		te.getLevel().getProfiler().pop();
	}

	private WeakHashMap<ItemStack, ItemEntity> map = new WeakHashMap<>();
	
	private void renderItem(TileEntityPipeBase tile, ItemStack it, MatrixStack matrix, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		tile.getLevel().getProfiler().push("renderItem");
		
		if(it!=null && !it.isEmpty())
		{
			matrix.mulPose(Vector3f.YP.rotationDegrees(45f));
			matrix.scale(0.75f, 0.75f, 0.75f);
			ItemEntity ei = map.computeIfAbsent(it, i -> new ItemEntity(tile.getLevel(), 0, 0, 0, i));
			
			Minecraft.getInstance().getEntityRenderDispatcher().render(ei, 0.0D, 0.0D, 0.0D, 0.0F, 0.0F, matrix, bufferIn, combinedLightIn);
		}
		tile.getLevel().getProfiler().pop();
	}
}
