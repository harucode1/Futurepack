package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.common.block.BlockRotateable;
import futurepack.common.block.misc.TileEntityResearchExchange;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.math.vector.Vector3f;

public class RenderTectern extends TileEntityRenderer<TileEntityResearchExchange> 
{
	
	public RenderTectern(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(TileEntityResearchExchange tileEntityIn, float partialTicks, MatrixStack matrixStackIn,
			IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		
		matrixStackIn.pushPose();
		ItemStack it = new ItemStack(ToolItems.escanner);
		
		Direction dir = tileEntityIn.getBlockState().getValue(BlockRotateable.HORIZONTAL_FACING);
		
		matrixStackIn.translate(0.5d, 0.5d, 0.5d);
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(dir.toYRot() + (dir.getAxis()==Axis.X ? 180 : 0)));
		matrixStackIn.translate(0.0d, 0.43d, 0.3d);
		
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-68F));
		
		Minecraft.getInstance().getItemRenderer().renderStatic(it, ItemCameraTransforms.TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
		
		matrixStackIn.popPose();		
	}
	
}
