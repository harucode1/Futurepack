package futurepack.client.render.block;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ModelRocketLauncher extends ModelLaserBase
{
	//fields
	ModelRenderer Sockel;
	ModelRenderer RotationsfussSockel;
	ModelRenderer Stutze1;
	ModelRenderer Stutze2;
	ModelRenderer RotationsAchshalter1;
	ModelRenderer RotationsAchshalter2;
	ModelRenderer Laserkorper;
	
	public ModelRocketLauncher()
	{
		super(RenderType::entitySolid);
		
		texWidth = 64;
		texHeight = 64;
		
		Sockel = new ModelRenderer(this, 0, 42);
		Sockel.addBox(-8F, 0F, -8F, 16, 6, 16);
		Sockel.setPos(0F, 0F, 0F);
		Sockel.setTexSize(64, 64);
		Sockel.mirror = true;
		setRotation(Sockel, 0F, 0F, 0F);
		RotationsfussSockel = new ModelRenderer(this, 0, 28);
		RotationsfussSockel.addBox(-6F, 0F, -6F, 12, 2, 12);
		RotationsfussSockel.setPos(0F, -2F, 0F);
		RotationsfussSockel.setTexSize(64, 64);
		RotationsfussSockel.mirror = true;
		setRotation(RotationsfussSockel, 0F, 0F, 0F);
		Stutze1 = new ModelRenderer(this, 48, 32);
		Stutze1.addBox(3F, -6F, -2F, 1, 6, 4);
		Stutze1.setPos(0F, -2F, 0F);
		Stutze1.setTexSize(64, 64);
		Stutze1.mirror = true;
		setRotation(Stutze1, 0F, 0F, 0F);
		Stutze2 = new ModelRenderer(this, 48, 32);
		Stutze2.addBox(-4F, -6F, -2F, 1, 6, 4);
		Stutze2.setPos(0F, -2F, 0F);
		Stutze2.setTexSize(64, 64);
		Stutze2.mirror = true;
		setRotation(Stutze2, 0F, 0F, 0F);
		RotationsAchshalter1 = new ModelRenderer(this, 34, 20);
		RotationsAchshalter1.addBox(2F, -2F, -2F, 3, 4, 4);
		RotationsAchshalter1.setPos(0F, -10F, 0F);
		RotationsAchshalter1.setTexSize(64, 64);
		RotationsAchshalter1.mirror = true;
		setRotation(RotationsAchshalter1, 0F, 0F, 0F);
		RotationsAchshalter2 = new ModelRenderer(this, 34, 20);
		RotationsAchshalter2.addBox(-5F, -2F, -2F, 3, 4, 4);
		RotationsAchshalter2.setPos(0F, -10F, 0F);
		RotationsAchshalter2.setTexSize(64, 64);
		RotationsAchshalter2.mirror = true;
		setRotation(RotationsAchshalter2, 0F, 0F, 0F);
		Laserkorper = new ModelRenderer(this, 17, 0);
		Laserkorper.addBox(-2F, -4F, -6F, 4, 8, 12);
		Laserkorper.setPos(0F, -10F, 0F);
		Laserkorper.setTexSize(64, 64);
		Laserkorper.mirror = true;
		setRotation(Laserkorper, 0F, 0F, 0F);
	}
	
	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.xRot = x;
		model.yRot = y;
		model.zRot = z;
	}

	@Override
	public void rotateYUnit(float y)
	{
		RotationsfussSockel.yRot = y;
	    Stutze1.yRot = y;
	    Stutze2.yRot = y;
	    RotationsAchshalter1.yRot = y;
	    RotationsAchshalter2.yRot = y;
	    Laserkorper.yRot = y;
	}

	@Override
	public void rotateXUnit(float x)
	{
		Laserkorper.xRot = x;
	}
	
	@Override
	public ModelRenderer[] getParts() 
	{
		return new ModelRenderer[] {Sockel, RotationsfussSockel, Stutze1, Stutze2, RotationsAchshalter1, RotationsAchshalter2, Laserkorper};
	}
	
	

}
