package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.logistic.plasma.TileEntityPlasmaStorageCore;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;

public class RenderPlasmaTank extends TileEntityRenderer<TileEntityPlasmaStorageCore> 
{
	public RenderPlasmaTank(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}

	public static final ResourceLocation circle = new ResourceLocation(Constants.MOD_ID, "textures/circle_alpha_shade.png");
	

	
	@Override
	public boolean shouldRenderOffScreen(TileEntityPlasmaStorageCore te) 
	{
		return true;
	}


	@Override
	public void render(TileEntityPlasmaStorageCore tileEntityIn, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		// TODO Auto-generated method stub
		
	}
}
