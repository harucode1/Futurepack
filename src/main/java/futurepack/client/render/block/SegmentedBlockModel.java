package futurepack.client.render.block;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;

public abstract class SegmentedBlockModel extends Model 
{
	
	private List<ModelRenderer> parts;

	public SegmentedBlockModel(Function<ResourceLocation, RenderType> renderTypeIn) 
	{
		super(renderTypeIn);
	}

	public abstract ModelRenderer[] getParts();
	
	@Override
	public void renderToBuffer(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) 
	{
		if(parts==null)
		{
			parts = Arrays.asList(getParts());
		}
		parts.forEach(m -> {
			m.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
		});
	}

}
