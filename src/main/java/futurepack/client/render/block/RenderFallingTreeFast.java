package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.common.block.misc.TileEntityFallingTree;
import futurepack.depend.api.MiniWorld;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;

public class RenderFallingTreeFast extends TileEntityRenderer<TileEntityFallingTree>
{

	public RenderFallingTreeFast(TileEntityRendererDispatcher rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}
	
	@Override
	public boolean shouldRenderOffScreen(TileEntityFallingTree te)
	{
		return true;
	}

	@Override
	public void render(TileEntityFallingTree te, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		GlStateManager._enableCull();
		
		MiniWorld world = te.getMiniWorld();
		if(world!=null)
		{
			float progress =  1F-( ( te.ticks-partialTicks ) /te.maxticks);
			world.setRotation(90F * progress );
			HelperRenderBlocks.renderFast(world, partialTicks, matrixStackIn, bufferIn);
			
			//HelperRenderBlocks.renderFastBase(world, buf); // <-- geht
		}
	}

}
