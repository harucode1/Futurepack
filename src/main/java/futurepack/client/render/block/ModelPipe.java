package futurepack.client.render.block;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ModelPipe extends SegmentedBlockModel
{
	//fields
    public ModelRenderer Mitte;
    public ModelRenderer Seite_1;
    public ModelRenderer Seite_2;
    public ModelRenderer Seite_unten;
    public ModelRenderer Seite_obem;
    public ModelRenderer Seite_3;
    public ModelRenderer Seite_4;
    public ModelRenderer Shape1;
    public ModelRenderer Shape2;
    public ModelRenderer Shape3;
    public ModelRenderer Shape4;
    public ModelRenderer Shape_unten;
    public ModelRenderer Shape_oben_;
    
    public ModelPipe()
    {
    	super(RenderType::entitySolid);
    	
    	texWidth = 64;
    	texHeight = 64;
    
    	Mitte = new ModelRenderer(this, 0, 16);
    	Mitte.addBox(-4F, -4F, -4F, 8, 8, 8);
    	Mitte.setPos(0F, 0F, 0F);
    	Mitte.setTexSize(64, 64);
    	Mitte.mirror = true;
    	setRotation(Mitte, 0F, 0F, 0F);
    	Seite_1 = new ModelRenderer(this, 0, 0);
    	Seite_1.addBox(0F, -4F, -4F, 4, 8, 8);
    	Seite_1.setPos(4F, 0F, 0F);
    	Seite_1.setTexSize(64, 64);
    	Seite_1.mirror = true;
    	setRotation(Seite_1, 0F, 0F, 0F);
    	Seite_2 = new ModelRenderer(this, 0, 0);
    	Seite_2.addBox(0F, -4F, -4F, 4, 8, 8);
    	Seite_2.setPos(-4F, 0F, 0F);
    	Seite_2.setTexSize(64, 64);
    	Seite_2.mirror = true;
    	setRotation(Seite_2, 0F, 3.141593F, 0F);
    	Seite_unten = new ModelRenderer(this, 0, 0);
    	Seite_unten.addBox(0F, -4F, -4F, 4, 8, 8);
    	Seite_unten.setPos(0F, 4F, 0F);
    	Seite_unten.setTexSize(64, 64);
    	Seite_unten.mirror = true;
    	setRotation(Seite_unten, 0F, 0F, 1.570796F);
    	Seite_obem = new ModelRenderer(this, 0, 0);
    	Seite_obem.addBox(0F, -4F, -4F, 4, 8, 8);
    	Seite_obem.setPos(0F, -4F, 0F);
    	Seite_obem.setTexSize(64, 64);
    	Seite_obem.mirror = true;
    	setRotation(Seite_obem, 0F, 0F, -1.570796F);
    	Seite_3 = new ModelRenderer(this, 0, 0);
    	Seite_3.addBox(0F, -4F, -4F, 4, 8, 8);
    	Seite_3.setPos(0F, 0F, 4F);
    	Seite_3.setTexSize(64, 64);
    	Seite_3.mirror = true;
    	setRotation(Seite_3, 0F, -1.570796F, 0F);
    	Seite_4 = new ModelRenderer(this, 0, 0);
    	Seite_4.addBox(0F, -4F, -4F, 4, 8, 8);
    	Seite_4.setPos(0F, 0F, -4F);
    	Seite_4.setTexSize(64, 64);
    	Seite_4.mirror = true;
    	setRotation(Seite_4, 0F, 1.570796F, 0F);
    	
    	Shape1 = new ModelRenderer(this, 8, 24);
    	Shape1.addBox(0F, -4F, -4F, 0, 8, 8);
    	Shape1.setPos(4F, 0F, 0F);
    	Shape1.setTexSize(64, 64);
    	Shape1.mirror = true;
    	setRotation(Shape1, 0F, 0F, 0F);
    	Shape2 = new ModelRenderer(this, 8, 24);
    	Shape2.addBox(0F, -4F, -4F, 0, 8, 8);
    	Shape2.setPos(-4F, 0F, 0F);
    	Shape2.setTexSize(64, 64);
    	Shape2.mirror = true;
    	setRotation(Shape2, 0F, 0F, 0F);
    	Shape3 = new ModelRenderer(this, 8, 24);
    	Shape3.addBox(0F, -4F, -4F, 0, 8, 8);
    	Shape3.setPos(0F, 0F, 4F);
    	Shape3.setTexSize(64, 64);
    	Shape3.mirror = true;
    	setRotation(Shape3, 0F, -1.570796F, 0F);
    	Shape4 = new ModelRenderer(this, 8, 24);
    	Shape4.addBox(0F, -4F, -4F, 0, 8, 8);
    	Shape4.setPos(0F, 0F, -4F);
    	Shape4.setTexSize(64, 64);
    	Shape4.mirror = true;
    	setRotation(Shape4, 0F, 1.570796F, 0F);
    	Shape_unten = new ModelRenderer(this, 0, 32);
    	Shape_unten.addBox(-4F, 0F, -4F, 8, 0, 8);
    	Shape_unten.setPos(0F, 4F, 0F);
    	Shape_unten.setTexSize(64, 64);
    	Shape_unten.mirror = true;
    	setRotation(Shape_unten, 0F, 0F, 0F);
    	Shape_oben_ = new ModelRenderer(this, 0, 32);
    	Shape_oben_.addBox(-4F, 0F, -4F, 8, 0, 8);
    	Shape_oben_.setPos(0F, -4F, 0F);
    	Shape_oben_.setTexSize(64, 64);
    	Shape_oben_.mirror = true;
    	setRotation(Shape_oben_, 0F, 0F, 0F);
    }	

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
    	model.xRot = x;
    	model.yRot = y;
    	model.zRot = z;
    }

	@Override
	public ModelRenderer[] getParts() 
	{
		return new ModelRenderer[] {Mitte, Seite_1, Seite_2, Seite_3, Seite_4, Seite_obem, Seite_unten, Shape1, Shape2, Shape3, Shape4, Shape_oben_, Shape_unten};
	}
  


}
