package futurepack.client.render.block;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryUtil;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.datafixers.util.Pair;

import futurepack.api.Constants;
import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.common.FPLog;
import futurepack.common.item.tools.ItemLogisticEditor;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraftforge.common.util.LazyOptional;


public class RenderLogistic
{
	static ResourceLocation logistic = new ResourceLocation(Constants.MOD_ID, "textures/model/overlay_logistic.png");
	
	private static Tessellator tesselator = new Tessellator(20480);
	private static Runnable renderCall = null;
	
	private static void renderLogistics(ILogisticInterface log, double posX, double posY, double posZ, EnumLogisticType mode, Direction face, boolean hovered, TileEntity tile, BufferBuilder tes)
	{			
		if(hovered)
		{
			Minecraft.getInstance().level.getProfiler().push("drawHovered");
			double min = 0.02;
			BufferBuilder buffer = Tessellator.getInstance().getBuilder();
			buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
			renderFace(face, posX-min, posY-min, posZ-min, 1+min*2, 1+min*2, 1+min*2, mode.color, buffer);
			bindTexture(logistic);
			Tessellator.getInstance().end();
			
			GL11.glDepthMask(false);
			EnumLogisticIO io = log.getMode(mode);
			if(io!=EnumLogisticIO.NONE)
			{
				buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
				bindTexture(io.getTexture());
				min = 0.04;
				renderFace(face, posX-min, posY-min, posZ-min, 1+min*2, 1+min*2, 1+min*2, 0xFFFFFFFF, buffer);
				Tessellator.getInstance().end();
			}
			GL11.glDepthMask(true);
			
			Minecraft.getInstance().level.getProfiler().pop();
		}
		else
		{
			VoxelShape sh = tile.getLevel().getBlockState(tile.getBlockPos()).getBlockSupportShape(tile.getLevel(), tile.getBlockPos());
			AxisAlignedBB bb;
			if(sh.isEmpty())
				bb = new AxisAlignedBB(0,0,0, 1,1,1);
			else
				bb = sh.bounds();
//				bb = bb.offset(-tile.getPos().getX(),-tile.getPos().getY(),-tile.getPos().getZ());								
			double min = 0.02;
			renderFace(face, posX-min +bb.minX, posY-min +bb.minY, posZ-min + bb.minZ, (bb.maxX-bb.minX)+min*2, (bb.maxY-bb.minY)+min*2, (bb.maxZ-bb.minZ)+min*2, mode.color, tes);
		}
	}
	
	private static void renderFace(Direction face, double posX, double posY, double posZ, double sizeX, double sizeY, double sizeZ, int color, BufferBuilder tes)
	{
		Minecraft.getInstance().level.getProfiler().push("renderFace");
		
		int x0 = Float.floatToIntBits((float) posX);
		int x1 = Float.floatToIntBits((float)(posX+sizeX));
		int y0 = Float.floatToIntBits((float) posY);
		int y1 = Float.floatToIntBits((float)(posY+sizeY));
		int z0 = Float.floatToIntBits((float) posZ);
		int z1 = Float.floatToIntBits((float)(posZ+sizeZ));
		int u0 = Float.floatToIntBits(0F);
		int u1 = Float.floatToIntBits(1F);
		int v0 = Float.floatToIntBits(0F);
		int v1 = Float.floatToIntBits(1F);
		
		int[] vertexData = null;
		
		if(face== Direction.UP)
		{		
			vertexData = new int[] {
					x0,y1,z1,u0,v0,color,
					x1,y1,z1,u1,v0,color,
					x1,y1,z0,u1,v1,color,
					x0,y1,z0,u0,v1,color
			};
			
		}
		else if(face== Direction.DOWN)
		{
			vertexData = new int[]{
					x0,y0,z0,u1,v0,color,
					x1,y0,z0,u0,v0,color,
					x1,y0,z1,u0,v1,color,
					x0,y0,z1,u1,v1,color
			};
		}
		else if(face== Direction.NORTH)
		{
			vertexData = new int[]{
					x0,y1,z0,u1,v0,color,
					x1,y1,z0,u0,v0,color,
					x1,y0,z0,u0,v1,color,
					x0,y0,z0,u1,v1,color
			};
		}
		else if(face== Direction.SOUTH)
		{
			vertexData = new int[]{
					x1,y1,z1,u1,v0,color,
					x0,y1,z1,u0,v0,color,
					x0,y0,z1,u0,v1,color,
					x1,y0,z1,u1,v1,color,
			};
		}
		else if(face== Direction.WEST)
		{
			vertexData = new int[]{
					x0,y1,z1,u1,v0,color,
					x0,y1,z0,u0,v0,color,
					x0,y0,z0,u0,v1,color,
					x0,y0,z1,u1,v1,color
			};
		}
		else if(face== Direction.EAST)
		{
			vertexData = new int[] {
					x1,y1,z0,u1,v0,color,
					x1,y1,z1,u0,v0,color,
					x1,y0,z1,u0,v1,color,
					x1,y0,z0,u1,v1,color
			};
		}
		else
		{
			FPLog.logger.fatal("Unkown Direction " + (face==null?"null":face.toString()));
		}
		
		if(vertexData!=null)
		{
			ByteBuffer buffer = ByteBuffer.allocate(vertexData.length * 4);
			buffer.order(ByteOrder.nativeOrder());
			buffer.asIntBuffer().put(vertexData);
			tes.putBulkData(buffer);
		}
		Minecraft.getInstance().level.getProfiler().pop();
	}

	public static void onItemHeld(ItemStack heldItem, float partialTicks, MatrixStack matrixStack)
	{
		GL11.glPushMatrix();
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		
		Matrix4f mat = matrixStack.last().pose();
		FloatBuffer matF = GLAllocation.createFloatBuffer(16);
		mat.store(matF);
		GL11.glLoadMatrixf(matF);
//		GlStateManager.disableDepthTest();
		
		PlayerEntity pl = Minecraft.getInstance().player;
		ClientWorld cl = Minecraft.getInstance().level;
		
		float apartialTicks = 1F -partialTicks;
		double pX,pY,pZ;
		pX = (pl.getX() * partialTicks + apartialTicks * pl.xo);
		pY = (pl.getY() * partialTicks + apartialTicks * pl.yo) + pl.getEyeHeight();
		pZ = (pl.getZ() * partialTicks + apartialTicks * pl.zo);
		
		cl.getProfiler().push("renderLogisticsOverlay");
		if(pl != null && Minecraft.getInstance().screen==null)
		{
			Minecraft mc = Minecraft.getInstance();
			boolean mainH = !pl.getItemInHand(Hand.MAIN_HAND).isEmpty() && pl.getItemInHand(Hand.MAIN_HAND).getItem() == ToolItems.logisticEditor;
			boolean offH = !pl.getItemInHand(Hand.OFF_HAND).isEmpty() && pl.getItemInHand(Hand.OFF_HAND).getItem() == ToolItems.logisticEditor;
			
			if(mainH || offH)
			{
				EnumLogisticType mode = ItemLogisticEditor.getModeFromItem(pl.getItemInHand(mainH ? Hand.MAIN_HAND : Hand.OFF_HAND));
				
				BlockRayTraceResult res = null;
				if(mc.hitResult.getType() == Type.BLOCK)
				{
					res = (BlockRayTraceResult) mc.hitResult;
				}
				
				GlStateManager._disableLighting();
//				OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
				setLightmapDisabled(true);
				
				if(renderCall == null)
				{
					BufferBuilder tes = tesselator.getBuilder();
					tes.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
						
					for(TileEntity tile : cl.blockEntityList)
					{
						double posX = tile.getBlockPos().getX();
						double posY = tile.getBlockPos().getY();
						double posZ = tile.getBlockPos().getZ();
							
						render(tile, posX, posY, posZ, partialTicks, mode, res, tes);
					}
					tes.end();
					createStaticRenderCall(tes);
					
				}
				if(renderCall != null)
				{
					bindTexture(logistic);
					GlStateManager._pushMatrix();
					GlStateManager._translated(-pX, -pY, -pZ);
					drawBufferWithoutReset();
					GlStateManager._popMatrix();
					if(System.currentTimeMillis() % 1000 / 10 == 0)
					{
						renderCall = null;
					}
				}
				if(res != null)
				{
					TileEntity tile = cl.getBlockEntity(res.getBlockPos());
					if(tile != null)
					{
						double posX = (tile.getBlockPos().getX() - pX);
						double posY = (tile.getBlockPos().getY() - pY);
						double posZ = (tile.getBlockPos().getZ() - pZ);
						
						render(tile, posX, posY, posZ, partialTicks, mode, res, null);
					}
				}
				GlStateManager._enableLighting();
				setLightmapDisabled(false);
			}
		}
		cl.getProfiler().pop();
		RenderSystem.disableBlend();
		
		GL11.glPopMatrix();
	}

	private static void setLightmapDisabled(boolean b) 
	{
		if(b)
		{
			Minecraft.getInstance().gameRenderer.lightTexture().turnOffLightLayer();
		}
		else
		{
			Minecraft.getInstance().gameRenderer.lightTexture().turnOnLightLayer();
		}
		
	}

	private static void bindTexture(ResourceLocation path) 
	{
		Minecraft.getInstance().getTextureManager().bind(path);
	}

	private static void render(TileEntity tile, double posX, double posY, double posZ, float partialTicks, EnumLogisticType mode, BlockRayTraceResult res, BufferBuilder tes)
	{
		final boolean hover = res==null ? false : tile.getBlockPos().equals(res.getBlockPos());
		
		for(Direction facing : FacingUtil.VALUES)
		{
			LazyOptional<ILogisticInterface> opt = tile.getCapability(CapabilityLogistic.cap_LOGISTIC, facing);
			opt.ifPresent(log -> 
			{
				if(log.isTypeSupported(mode))
				{
					renderLogistics(log, posX, posY, posZ, mode, facing, hover, tile, tes);
				}
			});
		}
	}
	
	private static void createStaticRenderCall(BufferBuilder bufferBuilderIn)//copied from WorldVertexBufferUploader#draw
	{
		Pair<BufferBuilder.DrawState, ByteBuffer> pair = bufferBuilderIn.popNextBuffer();
		BufferBuilder.DrawState bufferbuilder$drawstate = pair.getFirst();
		
		ByteBuffer bufferIn = pair.getSecond();
		int modeIn = bufferbuilder$drawstate.mode();
		VertexFormat vertexFormatIn = bufferbuilder$drawstate.format();
		int countIn = bufferbuilder$drawstate.vertexCount();
		
		RenderSystem.assertThread(RenderSystem::isOnRenderThread);
		bufferIn.clear();
		if (countIn > 0) 
		{
			renderCall = () -> 
			{
				vertexFormatIn.setupBufferState(MemoryUtil.memAddress(bufferIn));
				GlStateManager._drawArrays(modeIn, 0, countIn);
				vertexFormatIn.clearBufferState();
			};
		}
	}
	
	public static void drawBufferWithoutReset() 
	{
		if(renderCall!=null)
		{
			renderCall.run();
		}
	}

	public static void refreshRenderng() 
	{
		renderCall = null;
	}

}
