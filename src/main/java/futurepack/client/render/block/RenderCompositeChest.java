package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityCompositeChest;
import net.minecraft.client.renderer.Atlases;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.RenderMaterial;
import net.minecraft.client.renderer.tileentity.ChestTileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.state.properties.ChestType;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.util.ResourceLocation;

public class RenderCompositeChest<T extends ChestTileEntity> extends ChestTileEntityRenderer<T>
{
	public static final ResourceLocation TEXTURE_NORMAL = new ResourceLocation(Constants.MOD_ID, "model/comp_normal");
	public static final ResourceLocation TEXTURE_NORMAL_LEFT = new ResourceLocation(Constants.MOD_ID, "model/comp_normal_left");
	public static final ResourceLocation TEXTURE_NORMAL_RIGHT = new ResourceLocation(Constants.MOD_ID, "model/comp_normal_right");
	
	public static final RenderMaterial NORMAL_MATERIAL = new RenderMaterial(Atlases.CHEST_SHEET, TEXTURE_NORMAL);
	public static final RenderMaterial NORMAL_MATERIAL_LEFT = new RenderMaterial(Atlases.CHEST_SHEET, TEXTURE_NORMAL_LEFT);
	public static final RenderMaterial NORMAL_MATERIAL_RIGHT = new RenderMaterial(Atlases.CHEST_SHEET, TEXTURE_NORMAL_RIGHT);
	
	private final TileEntityCompositeChest chest = new TileEntityCompositeChest();
	
	public RenderCompositeChest(TileEntityRendererDispatcher renderDispatcher) {
		super(renderDispatcher);
	}
	
	@Override
	public void render(T te, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		if(te==null)
		{
			te = (T) chest;
		}
		super.render(te, partialTicks, matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn);
	}
	
	@Override
	protected RenderMaterial getMaterial(T tileEntity, ChestType chestType) {
		RenderMaterial m = Atlases.chooseMaterial(tileEntity, chestType, false);
		
		
		if(m.texture().getPath().equals("entity/chest/normal")) {
			return NORMAL_MATERIAL;
		}
		else if(m.texture().getPath().equals("entity/chest/normal_left")) {
			return NORMAL_MATERIAL_LEFT;
		}
		else if(m.texture().getPath().equals("entity/chest/normal_right")) {
			return NORMAL_MATERIAL_RIGHT;
		}
		
		return m;
	}
}
