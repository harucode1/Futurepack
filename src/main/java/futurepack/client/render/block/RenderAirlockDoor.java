package futurepack.client.render.block;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.misc.BlockAirlockDoor;
import futurepack.common.block.misc.BlockAirlockDoor.EnumAirlockDirection;
import futurepack.common.block.misc.TileEntityAirlockDoor;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;

public class RenderAirlockDoor extends TileEntityRenderer<TileEntityAirlockDoor>
{
	
	public RenderAirlockDoor(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	private ModelSchleuse model = new ModelSchleuse();
	private ResourceLocation icon = new ResourceLocation(Constants.MOD_ID,"textures/model/schleuse.png");

	@Override
	public void render(TileEntityAirlockDoor te, float partialTicks, MatrixStack matrix,
			IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		te.getLevel().getProfiler().push("renderSpaceDoor");
		
		matrix.pushPose();
		//bindTexture(icon);
		
		matrix.translate(0.5, 0.5, 0.5);
		
		BlockState state = te.getBlockState();
		boolean extended = te.extended;
		EnumAirlockDirection dir = state.getValue(BlockAirlockDoor.DIRECTION);
		
		if( dir == EnumAirlockDirection.UP_WE)//working
		{
			matrix.mulPose(Vector3f.XP.rotationDegrees(180));
		}
		else if(dir == EnumAirlockDirection.DOWN_WE)//working
		{
		}
		else if(dir == EnumAirlockDirection.UP_NS)//working
		{
			matrix.mulPose(Vector3f.XP.rotationDegrees(180));
			matrix.mulPose(Vector3f.YP.rotationDegrees(90));
		}
		else if(dir == EnumAirlockDirection.DOWN_NS)//working
		{
			matrix.mulPose(Vector3f.YP.rotationDegrees(90));
		}
		else//working
		{
			matrix.mulPose(Vector3f.XP.rotationDegrees(90));
			matrix.mulPose(Vector3f.ZP.rotationDegrees(dir.getFacing().toYRot() + 180));
			matrix.mulPose(Vector3f.YP.rotationDegrees(90));
		}
		
		matrix.translate(0, -1, 0);
		
		model.Shape1.render(matrix, bufferIn.getBuffer(model.renderType(icon)), combinedLightIn, combinedOverlayIn);
		
		long last = System.currentTimeMillis() - te.switched;
		boolean flag1 = last<1000;
		
		if(!flag1 && extended)
			model.tor.render(matrix, bufferIn.getBuffer(model.renderType(icon)), combinedLightIn, combinedOverlayIn);
		else if(flag1)
		{
			float offset = last / 1000F;		
			if(extended)
			{
				offset = 1F-offset;			
			}
			
			matrix.scale((float) (1-(offset)*0.01), 1 -(offset)/3F, 1);
			
			offset= offset*  1.5F*1.1875F;
			
			matrix.translate(0, offset, 0);		
			
			model.tor.render(matrix, bufferIn.getBuffer(model.renderType(icon)), combinedLightIn, combinedOverlayIn);
		}
		
		matrix.popPose();
		te.getLevel().getProfiler().pop();
	}
}
