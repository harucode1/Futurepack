package futurepack.client.render;

import futurepack.api.Constants;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.WorldVertexBufferUploader;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraftforge.client.ISkyRenderHandler;

//@ TODO: OnlyIn(Dist.CLIENT)
public class RenderSkyMenelaus extends RenderSkyBase
{
	
	private ResourceLocation locationMoonPhasesPng = new ResourceLocation(Constants.MOD_ID, "textures/mond_menelaus.png");


	public RenderSkyMenelaus()
	{
		super();
	}
	
	@Override
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1) 
	{
		bindTexture(locationMoonPhasesPng);
		int k = phase % 16;
		float u0 = (k%4) / 4.0F;
		float u1 = u0 + 0.25F;
		float v0 = (k/4)  /4F;
		float v1 = v0 +0.25F;
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.vertex(matrix4f1, -size, -100.0F, size).uv(u1, v1).endVertex();
        bufferbuilder.vertex(matrix4f1, size, -100.0F, size).uv(u0, v1).endVertex();
        bufferbuilder.vertex(matrix4f1, size, -100.0F, -size).uv(u0, v0).endVertex();
        bufferbuilder.vertex(matrix4f1, -size, -100.0F, -size).uv(u1, v0).endVertex();
        bufferbuilder.end();
        WorldVertexBufferUploader.end(bufferbuilder);
	}
}
