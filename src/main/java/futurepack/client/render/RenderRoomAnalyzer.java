package futurepack.client.render;

import java.awt.Color;
import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;

import futurepack.client.render.block.RenderLogistic;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3d;

public class RenderRoomAnalyzer 
{
	
	private static ArrayList<SoftReference<AirChunk>> list;
	
	public static void addAirDataReceived(BlockPos pos, byte[] data, int cw, int ch, int cd)
	{
		pos = pos.offset(-16, -16, -16);
		AirChunk air = new AirChunk(cw, cd, ch, pos, data);
		SoftReference<AirChunk> soft = new SoftReference<AirChunk>(air);
		if(list==null)
		{
			list = new ArrayList<SoftReference<AirChunk>>();
		}
		synchronized (list)
		{
			Iterator<SoftReference<AirChunk>> iter = list.iterator();
			while(iter.hasNext())
			{
				SoftReference<AirChunk> ref = iter.next();
				AirChunk aaa = ref.get();
				if(aaa!=null)
				{
					if(aaa.pos.equals(pos))
					{
						iter.remove();
						continue;
					}
				}
				else
				{
					iter.remove();
					continue;
				}
			}
			list.add(soft);
		}
	}
	
	public static class AirChunk
	{
		private final int cw, cd, ch;
		private final BlockPos pos;
		private final byte[] data;
		public final long time;
		
		private BufferBuilder.State bufState = null;
		
		public AirChunk(int cw, int cd, int ch, BlockPos pos, byte[] data)
		{
			super();
			this.cw = cw;
			this.cd = cd;
			this.ch = ch;
			this.pos = pos;
			this.data = data;
			time = System.currentTimeMillis();
		}
		
		public void render(float partialTicks)
		{
			float apartialTicks = 1F - partialTicks;
			if(bufState == null)
			{
				BufferBuilder builder = new BufferBuilder(data.length);
				builder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
				render(builder, data);
//				builder.end();
				bufState = builder.getState();
			}
			ClientPlayerEntity sp = Minecraft.getInstance().player;
			double dx,dy,dz;
			Vector3d p = sp.getEyePosition(partialTicks).scale(-1F).add(pos.getX(), pos.getY(), pos.getZ());
			dx = p.x();
			dy = p.y();
			dz = p.z();
			
//			double dis = dx*dx + dy*dy + dz*dz;
			//if(dis < 50*50)
			{
				GL11.glPushMatrix();
				GlStateManager._translated(dx,dy,dz);
				GlStateManager._disableTexture();
				GlStateManager._enableBlend();
				GlStateManager._depthMask(false);
				GlStateManager._blendFunc(SourceFactor.SRC_ALPHA.value, DestFactor.ONE_MINUS_SRC_ALPHA.value);
				Tessellator tes = Tessellator.getInstance();
				BufferBuilder buffer = tes.getBuilder();
				buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
				
				buffer.restoreState(bufState);
				
				tes.end();
				GlStateManager._enableTexture();
				GlStateManager._disableBlend();
				GlStateManager._depthMask(true);
				GL11.glPopMatrix();
			}
		}
		
		private void render(BufferBuilder buffer, byte[] data)
		{
			for(int x=0;x<16*cw;x++)
			{
				for(int y=0;y<16*ch;y++)
				{
					for(int z=0;z<16*cd;z++)
					{	
						int index = (x)*cd*ch*256 + (y)*cd*16 + (z);
						int air = data[index] + 128;
						
						if(air<=0)
						{
							continue;
						}
						float angle = air / 256F * 0.6666F;
						//Color col = new Color(0xA5000000 | Color.HSBtoRGB(angle, 1F, 1F));
						int rgb = Color.HSBtoRGB(angle, 1F, 1F) & 0xFFFFFF;
						Color col = new Color(0x1a000000 | rgb, true);
						//sides
						
						//sides
						int x0 = Float.floatToIntBits(x);
						int x1 = Float.floatToIntBits(x + 1);
						int y0 = Float.floatToIntBits(y);
						int y1 = Float.floatToIntBits(y + 1);
						int z0 = Float.floatToIntBits(z);
						int z1 = Float.floatToIntBits(z + 1);
						int c = col.getAlpha() <<24 | col.getRed() | col.getGreen()<<8 | col.getBlue()<<16;
						
						int vertexData[] = 
						{
								x0,y0,z0,c,
								x0,y1,z0,c,
								x1,y1,z0,c,
								x1,y0,z0,c,
								
								x0,y0,z1,c,
								x0,y1,z1,c,
								x0,y1,z0,c,
								x0,y0,z0,c,
								
								x1,y0,z1,c,
								x1,y1,z1,c,
								x0,y1,z1,c,
								x0,y0,z1,c,
								
								x1,y0,z0,c,
								x1,y1,z0,c,
								x1,y1,z1,c,
								x1,y0,z1,c,
								
								x0,y1,z0,c,
								x0,y1,z1,c,
								x1,y1,z1,c,
								x1,y1,z0,c,
								
								x1,y0,z0,c,
								x1,y0,z1,c,
								x0,y0,z1,c,
								x0,y0,z0,c
						};
						ByteBuffer bytebuffer = GLAllocation.createByteBuffer(vertexData.length * 4);
						bytebuffer.asIntBuffer().put(vertexData);
						
						buffer.putBulkData(bytebuffer);
					}
				}
			}	
		}
		
	}
	
	
	public static void render(float partialTicks, MatrixStack matrixStack)
	{
		
		long time = System.currentTimeMillis();
		if(list==null)
			return;
		
		GL11.glPushMatrix();
		
		Matrix4f mat = matrixStack.last().pose();
		FloatBuffer matF = GLAllocation.createFloatBuffer(16);
		mat.store(matF);
		GL11.glLoadMatrixf(matF);
		
		
		synchronized (list)
		{
			Iterator<SoftReference<AirChunk>> iter = list.iterator();
			while(iter.hasNext())
			{
				SoftReference<AirChunk> soft = iter.next();
				AirChunk air = soft.get();
				if(air!=null)
				{
					if(time - air.time > 1000 * 60)
					{
						iter.remove();
					}
					else
					{
						air.render(partialTicks);
					}
				}
				else
				{
					iter.remove();
				}
			}
		}
		
		GL11.glPopMatrix();
	}
	
}
