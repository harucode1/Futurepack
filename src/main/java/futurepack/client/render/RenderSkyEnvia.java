package futurepack.client.render;

import futurepack.api.Constants;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;

public class RenderSkyEnvia extends RenderSkyBase
{
	protected ResourceLocation locationSunPng = new ResourceLocation(Constants.MOD_ID, "textures/sun_envia.png");
	
	@Override
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1)
	{
	}
	
	@Override
	public float getSunSize()
	{
		return 3F;
	}
}
