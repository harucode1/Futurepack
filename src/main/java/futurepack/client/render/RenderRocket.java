package futurepack.client.render;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import futurepack.api.Constants;
import futurepack.client.render.entity.ModelRocket;
import futurepack.common.entity.throwable.EntityRocket;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;

public class RenderRocket extends EntityRenderer<EntityRocket>
{
	private ModelRocket model = new ModelRocket();

	public RenderRocket(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
	}

	@Override
	public void render(EntityRocket e, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) 
	{
		matrixStackIn.pushPose();
		
		float yaw = e.yRotO + (e.yRot - e.yRotO) * partialTicks;
		float pitch = e.xRotO + (e.xRot - e.xRotO) * partialTicks;
		
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(yaw + 180));
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(pitch));
		
		IVertexBuilder builder = bufferIn.getBuffer(model.renderType(getTextureLocation(e)));
		model.renderToBuffer(matrixStackIn, builder, packedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
		
		matrixStackIn.popPose();
		super.render(e, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}
	
	@Override
	public ResourceLocation getTextureLocation(EntityRocket p_110775_1_) 
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/rocket.png");
	}
}
