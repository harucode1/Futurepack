package futurepack.client.render;

import java.lang.reflect.Field;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import futurepack.common.DirtyHacks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.FogRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.WorldVertexBufferUploader;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.client.IRenderHandler;
import net.minecraftforge.client.ISkyRenderHandler;

//@ TODO: OnlyIn(Dist.CLIENT)
public class RenderSkyBase implements ISkyRenderHandler
{
//	private int pass;
//	private boolean vboEnabled;
//	private net.minecraft.client.renderer.vertex.VertexBuffer skyVBO, sky2VBO, starVBO;
//	private int glSkyList, glSkyList2, starGLCallList;
	
	private net.minecraft.client.renderer.vertex.VertexBuffer skyVBO, starVBO, sky2VBO;
	private final VertexFormat skyVertexFormat = DefaultVertexFormats.POSITION;
	
	protected ResourceLocation locationMoonPhasesPng = new ResourceLocation("textures/environment/moon_phases.png");
	protected ResourceLocation locationSunPng = new ResourceLocation("textures/environment/sun.png");

	public RenderSkyBase()
	{
//		Class man = WorldRenderer.class;
		WorldRenderer glob = Minecraft.getInstance().levelRenderer;
//		Field[] ff =  man.getDeclaredFields();
//		ff[15].setAccessible(true);
//		ff[16].setAccessible(true);
//		ff[17].setAccessible(true);
//		ff[19].setAccessible(true);
//		ff[20].setAccessible(true);
//		ff[21].setAccessible(true);
		
		
		
		try
		{
			Field f = DirtyHacks.findField(WorldRenderer.class, "skyVBO", "skyBuffer", VertexBuffer.class);
			f.setAccessible(true);
			skyVBO = (VertexBuffer) f.get(glob);
			
			f = DirtyHacks.findField(WorldRenderer.class, "starVBO", "starBuffer", VertexBuffer.class);
			f.setAccessible(true);
			starVBO = (VertexBuffer) f.get(glob); 
			
			f = DirtyHacks.findField(WorldRenderer.class, "sky2VBO", "darkBuffer", VertexBuffer.class);
			f.setAccessible(true);
			sky2VBO = (VertexBuffer) f.get(glob);
			
//			starGLCallList = ff[15].getInt(glob);
//			glSkyList = ff[16].getInt(glob);
//			glSkyList2 = ff[17].getInt(glob);
//			
//			starVBO =  (net.minecraft.client.renderer.vertex.VertexBuffer) ff[19].get(glob);
//			skyVBO = (net.minecraft.client.renderer.vertex.VertexBuffer) ff[20].get(glob);
//			sky2VBO =  (net.minecraft.client.renderer.vertex.VertexBuffer) ff[21].get(glob);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void render(int ticks, float partialTicks, MatrixStack matrixStack, ClientWorld world, Minecraft mc)
	{
		ActiveRenderInfo activerenderinfo = mc.gameRenderer.getMainCamera();
		MatrixStack matrixStackIn = new MatrixStack();
		
		net.minecraftforge.client.event.EntityViewRenderEvent.CameraSetup cameraSetup = net.minecraftforge.client.ForgeHooksClient.onCameraSetup(mc.gameRenderer, activerenderinfo, partialTicks);
		activerenderinfo.setAnglesInternal(cameraSetup.getYaw(), cameraSetup.getPitch());
		matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(cameraSetup.getRoll()));

	    matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(activerenderinfo.getXRot()));
	    matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(activerenderinfo.getYRot() + 180.0F));
		renderSky(matrixStackIn, partialTicks, mc, world);
	}

	protected void renderAfterStars(float partialTicks, ClientWorld world, Minecraft mc, BufferBuilder bufferbuilder, MatrixStack matrixStackIn)
	{
		
	}

	protected void renderBeforeStars(float partialTicks, ClientWorld world, Minecraft mc, BufferBuilder bufferbuilder, MatrixStack matrixStackIn)
	{
		
	}

	protected void bindTexture(ResourceLocation res)
	{
		Minecraft.getInstance().getTextureManager().bind(res);
	}
	
	public float getMoonSize()
	{
		return 20F;
	}
	
	public float getSunSize()
	{
		return 30F;
	}
	
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1)
	{
		bindTexture(locationMoonPhasesPng);
        int l = phase % 4;
        int i1 = phase / 4 % 2;
        float f13 = (float)(l + 0) / 4.0F;
        float f14 = (float)(i1 + 0) / 2.0F;
        float f15 = (float)(l + 1) / 4.0F;
        float f16 = (float)(i1 + 1) / 2.0F;
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.vertex(matrix4f1, -size, -100.0F, size).uv(f15, f16).endVertex();
        bufferbuilder.vertex(matrix4f1, size, -100.0F, size).uv(f13, f16).endVertex();
        bufferbuilder.vertex(matrix4f1, size, -100.0F, -size).uv(f13, f14).endVertex();
        bufferbuilder.vertex(matrix4f1, -size, -100.0F, -size).uv(f15, f14).endVertex();
        bufferbuilder.end();
        WorldVertexBufferUploader.end(bufferbuilder);
	}
	
	protected void renderSun(float size, BufferBuilder bufferbuilder, Matrix4f matrix4f1)
	{
		bindTexture(locationSunPng);
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.vertex(matrix4f1, -size, 100.0F, -size).uv(0.0F, 0.0F).endVertex();
        bufferbuilder.vertex(matrix4f1, size, 100.0F, -size).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(matrix4f1, size, 100.0F, size).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(matrix4f1, -size, 100.0F, size).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.end();
        WorldVertexBufferUploader.end(bufferbuilder);
	}
	
	public void renderSky(MatrixStack matrixStackIn, float partialTicks, Minecraft mc, ClientWorld world) 
	{
		if (mc.level.dimensionType().hasSkyLight()) 
		{
	         RenderSystem.disableTexture();
	         Vector3d Vector3d = world.getSkyColor(mc.gameRenderer.getMainCamera().getBlockPosition(), partialTicks);
	         float f = (float)Vector3d.x;
	         float f1 = (float)Vector3d.y;
	         float f2 = (float)Vector3d.z;
	         FogRenderer.levelFogColor();
	         BufferBuilder bufferbuilder = Tessellator.getInstance().getBuilder();
	         RenderSystem.depthMask(false);
	         RenderSystem.enableFog();
	         RenderSystem.color3f(f, f1, f2);
	         
	         if(this.skyVBO!=null)
	         {
	        	this.skyVBO.bind();
	         	this.skyVertexFormat.setupBufferState(0L);
	         	this.skyVBO.draw(matrixStackIn.last().pose(), 7);
	         	VertexBuffer.unbind();
	         	this.skyVertexFormat.clearBufferState();
	         }
	        
	         RenderSystem.disableFog();
	         RenderSystem.disableAlphaTest();
	         RenderSystem.enableBlend();
	         RenderSystem.defaultBlendFunc();
	         float[] afloat = world.effects().getSunriseColor(world.getTimeOfDay(partialTicks), partialTicks);
	         if (afloat != null) {
	            RenderSystem.disableTexture();
	            RenderSystem.shadeModel(7425);
	            matrixStackIn.pushPose();
	            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90.0F));
	            float f3 = MathHelper.sin(world.getSunAngle(partialTicks)) < 0.0F ? 180.0F : 0.0F;
	            matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(f3));
	            matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(90.0F));
	            float f4 = afloat[0];
	            float f5 = afloat[1];
	            float f6 = afloat[2];
	            Matrix4f matrix4f = matrixStackIn.last().pose();
	            bufferbuilder.begin(6, DefaultVertexFormats.POSITION_COLOR);
	            bufferbuilder.vertex(matrix4f, 0.0F, 100.0F, 0.0F).color(f4, f5, f6, afloat[3]).endVertex();
	            int i = 16;

	            for(int j = 0; j <= 16; ++j) {
	               float f7 = (float)j * ((float)Math.PI * 2F) / 16.0F;
	               float f8 = MathHelper.sin(f7);
	               float f9 = MathHelper.cos(f7);
	               bufferbuilder.vertex(matrix4f, f8 * 120.0F, f9 * 120.0F, -f9 * 40.0F * afloat[3]).color(afloat[0], afloat[1], afloat[2], 0.0F).endVertex();
	            }

	            bufferbuilder.end();
	            WorldVertexBufferUploader.end(bufferbuilder);
	            matrixStackIn.popPose();
	            RenderSystem.shadeModel(7424);
	         }

	         RenderSystem.enableTexture();
	         RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
	         matrixStackIn.pushPose();
	         float f11 = 1.0F - world.getRainLevel(partialTicks);
	         RenderSystem.color4f(1.0F, 1.0F, 1.0F, f11);
	         matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(-90.0F));
	         matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(world.getTimeOfDay(partialTicks) * 360.0F));
	         Matrix4f matrix4f1 = matrixStackIn.last().pose();
	         renderSun(getSunSize(), bufferbuilder, matrix4f1);
	         renderMoon(getMoonSize(), world.getMoonPhase(), bufferbuilder, matrix4f1);
	         
	         renderBeforeStars(partialTicks, world, mc, bufferbuilder, matrixStackIn);
	         
	         RenderSystem.disableTexture();
	         float f10 = getStarBrightness(partialTicks, world, f11);
	         if (f10 > 0.0F && this.starVBO!=null) {
	            RenderSystem.color4f(f10, f10, f10, f10);
	            this.starVBO.bind();
	            this.skyVertexFormat.setupBufferState(0L);
	            this.starVBO.draw(matrixStackIn.last().pose(), 7);
	            VertexBuffer.unbind();
	            this.skyVertexFormat.clearBufferState();
	         }

	         RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
	         
	         renderAfterStars(partialTicks, world, mc, bufferbuilder, matrixStackIn);
	         
	         RenderSystem.disableBlend();
	         RenderSystem.enableAlphaTest();
	         RenderSystem.enableFog();
	         matrixStackIn.popPose();
	         RenderSystem.disableTexture();
	         RenderSystem.color3f(0.0F, 0.0F, 0.0F);
	         double d0 = mc.player.getEyePosition(partialTicks).y - world.getLevelData().getHorizonHeight();
	         if (d0 < 0.0D && this.sky2VBO != null)
	         {
	            matrixStackIn.pushPose();
	            matrixStackIn.translate(0.0D, 12.0D, 0.0D);
	            this.sky2VBO.bind();
	            this.skyVertexFormat.setupBufferState(0L);
	            this.sky2VBO.draw(matrixStackIn.last().pose(), 7);
	            VertexBuffer.unbind();
	            this.skyVertexFormat.clearBufferState();
	            matrixStackIn.popPose();
	         }

	         if (world.effects().hasGround()) {
	            RenderSystem.color3f(f * 0.2F + 0.04F, f1 * 0.2F + 0.04F, f2 * 0.6F + 0.1F);
	         } else {
	            RenderSystem.color3f(f, f1, f2);
	         }

	         RenderSystem.enableTexture();
	         RenderSystem.depthMask(true);
	         RenderSystem.disableFog();
	      }
	   }

	protected float getStarBrightness(float partialTicks, ClientWorld world, float skyBrightness) 
	{
		return world.getStarBrightness(partialTicks) * skyBrightness;
	}
}
