package futurepack.client.render.dimension;

import futurepack.client.render.RenderSkyEnvia;
import net.minecraft.client.Minecraft;
import net.minecraft.client.world.DimensionRenderInfo;
import net.minecraft.util.math.vector.Vector3d;

import net.minecraft.client.world.DimensionRenderInfo.FogType;

public class DimensionRenderTypeEnvia extends DimensionRenderInfo 
{

	private final static float cloud_height = 250;
	
	private final static boolean isNether = false;
	private final static boolean isEnd = false;
	
	public DimensionRenderTypeEnvia()
	{
		super(cloud_height, true, FogType.NORMAL, isEnd, isNether);
		setSkyRenderHandler(new RenderSkyEnvia());
	}

	/**
	 * Calculate sunset color
	 */
	@Override
	public Vector3d getBrightnessDependentFogColor(Vector3d sky_color_body, float celestialAngle_clamped) 
	{
		//From 1.14
		//@Override
    	//public Vec3d getSkyColor(BlockPos cameraEntity, float partialTicks)
    	//{
	  	//Vec3d base = world.getSkyColorBody(cameraEntity, partialTicks);
	  	//float f1 = MathHelper.clamp((float) (base.x + 0.05F), 0.0F, 1.0F);
	  	//float f2 = MathHelper.clamp((float) (base.y + 0.025F), 0.0F, 1.0F);
	  	//return new Vec3d(f1, f2, base.z);

        sky_color_body = sky_color_body.multiply((double)(celestialAngle_clamped * 0.94F + 0.06F), (double)(celestialAngle_clamped * 0.94F + 0.06F), (double)(celestialAngle_clamped * 0.91F + 0.09F));

		return new Vector3d(sky_color_body.z(), sky_color_body.y(), sky_color_body.x());
	}

	/**
	 * show fog at
	 */
	@Override
	public boolean isFoggyAt(int p_230493_1_, int p_230493_2_) 
	{
		return false;
	}

}
