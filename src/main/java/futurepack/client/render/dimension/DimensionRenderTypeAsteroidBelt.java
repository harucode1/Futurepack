package futurepack.client.render.dimension;

import futurepack.client.render.RenderSkyAsteroidBelt;
import net.minecraft.client.world.DimensionRenderInfo;
import net.minecraft.util.math.vector.Vector3d;


public class DimensionRenderTypeAsteroidBelt extends DimensionRenderInfo 
{

	private final static float cloud_height = -256F;
	
	private final static boolean isNether = false;
	private final static boolean isEnd = false;
	
	public DimensionRenderTypeAsteroidBelt()
	{
		super(cloud_height, true, FogType.END, isEnd, isNether);
		setSkyRenderHandler(new RenderSkyAsteroidBelt());
	}

	/**
	 * Calculate sunset color
	 */
	@Override
	public Vector3d getBrightnessDependentFogColor(Vector3d sky_color_body, float celestialAngle_clamped) 
	{

		return sky_color_body.multiply(0.0, 0.0, 0.0); 
	}

	@Override
	public float[] getSunriseColor(float p_230492_1_, float p_230492_2_) 
	{
		return null;
	}
	
	/**
	 * show fog at
	 */
	@Override
	public boolean isFoggyAt(int p_230493_1_, int p_230493_2_) 
	{
		return false;
	}

}
