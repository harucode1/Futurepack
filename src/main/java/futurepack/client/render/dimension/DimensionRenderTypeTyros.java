package futurepack.client.render.dimension;

import futurepack.client.render.RenderSkyTyros;
import net.minecraft.client.Minecraft;
import net.minecraft.client.world.DimensionRenderInfo;
import net.minecraft.util.math.vector.Vector3d;

import net.minecraft.client.world.DimensionRenderInfo.FogType;

public class DimensionRenderTypeTyros extends DimensionRenderInfo 
{

	private final static float cloud_height = 158F;
	
	private final static boolean isNether = false;
	private final static boolean isEnd = false;
	
	public DimensionRenderTypeTyros()
	{
		super(cloud_height, true, FogType.NORMAL, isEnd, isNether);
		setSkyRenderHandler(new RenderSkyTyros());
	}

	/**
	 * Calculate sunset color
	 */
	@Override
	public Vector3d getBrightnessDependentFogColor(Vector3d sky_color_body, float celestialAngle_clamped) 
	{
		return sky_color_body.multiply((double)(celestialAngle_clamped), (double)(celestialAngle_clamped), (double)(celestialAngle_clamped));
	}

	/**
	 * show fog at
	 */
	@Override
	public boolean isFoggyAt(int p_230493_1_, int p_230493_2_) 
	{
		return false;
	}

}
