package futurepack.client.render;

import futurepack.api.Constants;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.WorldVertexBufferUploader;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraftforge.client.ISkyRenderHandler;

//@ TODO: OnlyIn(Dist.CLIENT)
public class RenderSkyAsteroidBelt extends RenderSkyBase
{
	public RenderSkyAsteroidBelt()
	{
		super();
	}
	
	@Override
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1) 
	{
		//no moon in asteroid belt
	}
	
	@Override
	protected float getStarBrightness(float partialTicks, ClientWorld world, float skyBrightness) 
	{
		return 1F;
	}
}
