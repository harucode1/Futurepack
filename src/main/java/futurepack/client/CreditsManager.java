package futurepack.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import net.minecraft.util.text.TextFormatting;

public class CreditsManager
{
	public static ArrayList<CreditsEntry> list;

	static
	{
		init();
	}

	public static void init()
	{
		Gson gson = new Gson();
		Reader r = get("credits");
		JsonReader in = new JsonReader(r);
		JsonArray ja = gson.fromJson(in, JsonArray.class);

		Iterator<JsonElement> iter = ja.iterator();
		list = new ArrayList<>();
		while (iter.hasNext())
		{
			JsonObject ob = iter.next().getAsJsonObject();
			list.add(CreditsEntry.fromJson(ob));
		}
		Collections.sort(list);
		try
		{
			in.close();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static Reader get(String filename)
	{
		InputStream in = ClassLoader.getSystemClassLoader()
				.getResourceAsStream("futurepack/client/" + filename + ".json");
		if (in == null)
		{
			try
			{
				URL url = new URL("modjar://futurepack/futurepack/client/" + filename + ".json");
				in = url.openStream();
			} 
			catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		if (in == null)
		{
			in = CreditsManager.class.getResourceAsStream(filename + ".json");
		}
		return new InputStreamReader(in);
	}

	public static class CreditsEntry implements Comparable<CreditsEntry>
	{
		public final String name;
		public final UUID uuid;
		public final EnumCreditsType type;

		public CreditsEntry(String name, UUID uuid, EnumCreditsType type)
		{
			this.name = name;
			this.uuid = uuid;
			this.type = type;
		}

		public static CreditsEntry fromJson(JsonObject jo)
		{
			String name = jo.get("name").getAsString();
			String suuid = jo.get("uuid").getAsString();
			EnumCreditsType type = EnumCreditsType.getFromJson(jo);
			
			UUID uuid =UUID.fromString(suuid.replaceFirst("(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)", "$1-$2-$3-$4-$5"));
			
			return new CreditsEntry(name, uuid, type);
		}

		public int compareTo(CreditsEntry t)
		{
			return t == null ? 1 : this.type.compareTo(t.type);
		}
	}

	public static enum EnumCreditsType
	{
		RedSnake( TextFormatting.RED + "RedSnake Team"),
		Customizer(TextFormatting.AQUA + "Patreon: Customizer"),
		Updater(TextFormatting.LIGHT_PURPLE + "Patreon: Updater"),
		Supporter(TextFormatting.YELLOW + "Patreon: Supporter"),
		FormerPatreon("Former Patreon"),
		Unknown("Unknow - propbaply some one special");

		public final String headline;
		public String formatting = " ";

		EnumCreditsType(String s)
		{
			this.headline = s;
		}

		public static EnumCreditsType getFromJson(JsonObject jo)
        {
            String type = jo.get("type").getAsString();
            if("redsnaketeam".equals(type))
                return RedSnake;
            else if ("patreon".equals(type))
            {
                String tier = jo.get("tier").getAsString();
                if("updater".equals(tier))
                    return Updater;
                else if("supporter".equals(tier))
                    return Supporter;
                else if("customizer".equals(tier))
                    return Customizer;
                else
                    return FormerPatreon;
            }
            else
            {
                return Unknown;
            }
        }
	}
}
