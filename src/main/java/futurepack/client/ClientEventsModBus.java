package futurepack.client;

import java.util.function.Consumer;

import futurepack.api.Constants;
import futurepack.client.render.block.RenderCompositeChest;
import net.minecraft.client.renderer.Atlases;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Constants.MOD_ID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientEventsModBus {
	
	public static AtlasTexture particles = null;
	
	@SubscribeEvent
	public static void onStitch(TextureStitchEvent.Pre event) {
		
		if(event.getMap().location().equals(AtlasTexture.LOCATION_BLOCKS))
		{
			Consumer<ResourceLocation> register = event::addSprite;
			AdditionalTextures.loadAdditionalBlockTextures(register);
		}
		
		if(event.getMap().location().equals(AtlasTexture.LOCATION_PARTICLES))
		{
			if(particles == null) {
				particles = event.getMap();
			}
		}
		else if (event.getMap().location().equals(Atlases.CHEST_SHEET)) {
			event.addSprite(RenderCompositeChest.TEXTURE_NORMAL);
			event.addSprite(RenderCompositeChest.TEXTURE_NORMAL_LEFT);
			event.addSprite(RenderCompositeChest.TEXTURE_NORMAL_RIGHT);
		}
	}

	
}
