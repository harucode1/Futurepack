package futurepack.client;

import java.util.ArrayList;
import java.util.function.Consumer;

import futurepack.api.Constants;
import net.minecraft.util.ResourceLocation;

public class AdditionalTextures 
{
	public static final ResourceLocation BIOGAS_FLUID = new ResourceLocation(Constants.MOD_ID, "blocks/biogas");
	public static final ResourceLocation BITRIPTENTIUM_FLUID_STILL = new ResourceLocation(Constants.MOD_ID, "blocks/rocketfuel_still");
	public static final ResourceLocation BITRIPTENTIUM_FLUID_FLOW = new ResourceLocation(Constants.MOD_ID, "blocks/rocketfuel_flow");
	public static final ResourceLocation NEON_FLUID_STILL = new ResourceLocation(Constants.MOD_ID, "blocks/neon_still");
	public static final ResourceLocation NEON_FLUID_FLOW = new ResourceLocation(Constants.MOD_ID, "blocks/neon_flow");
	public static final ResourceLocation SALTWATER_FLUID_STILL = new ResourceLocation(Constants.MOD_ID, "blocks/saltwater_still");
	public static final ResourceLocation SALTWATER_FLUID_FLOW = new ResourceLocation(Constants.MOD_ID, "blocks/saltwater_flow");
	public static final ResourceLocation GENERIC_FLUID_STILL = new ResourceLocation(Constants.MOD_ID, "blocks/generic_still");
	public static final ResourceLocation GENERIC_FLUID_FLOW = new ResourceLocation(Constants.MOD_ID, "blocks/generic_flow");
	
	private static final ArrayList<ResourceLocation> TEXTURES = new ArrayList<>(5);
	
	public static void loadAdditionalBlockTextures(Consumer<ResourceLocation> register) 
	{
		register.accept(BIOGAS_FLUID);
		register.accept(BITRIPTENTIUM_FLUID_STILL);
		register.accept(BITRIPTENTIUM_FLUID_FLOW);
		register.accept(NEON_FLUID_STILL);
		register.accept(NEON_FLUID_FLOW);
		register.accept(NEON_FLUID_STILL);
		register.accept(NEON_FLUID_FLOW);
		register.accept(SALTWATER_FLUID_STILL);
		register.accept(SALTWATER_FLUID_FLOW);
		register.accept(GENERIC_FLUID_STILL);
		register.accept(GENERIC_FLUID_FLOW);
	
		TEXTURES.forEach(register);
		TEXTURES.trimToSize();
	}

	public static ResourceLocation get(ResourceLocation fluid) 
	{
		ResourceLocation texture = new ResourceLocation(fluid.getNamespace(), "blocks/" + fluid.getPath());
		TEXTURES.add(texture);
		return texture;
	}
}
