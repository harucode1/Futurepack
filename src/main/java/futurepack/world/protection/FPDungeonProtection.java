package futurepack.world.protection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import futurepack.api.Constants;
import futurepack.common.AsyncTaskManager;
import futurepack.common.FPLog;
import futurepack.depend.api.helper.HelperChunks;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.Items;
import net.minecraft.nbt.ByteArrayNBT;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class FPDungeonProtection 
{
	public static final Set<Block> whitelist;
	
	static
	{
		whitelist = new TreeSet<>(FPDungeonProtection::order);
		addBlockToWhiteList(new ResourceLocation("gravestone:gravestone"));
		addBlockToWhiteList(new ResourceLocation("erebus:block_of_bones"));
	}
	@CapabilityInject(IChunkProtection.class)
	public static final Capability<IChunkProtection> cap_PROTECTION = null;
	
	public static void init()
	{
		CapabilityManager.INSTANCE.register(IChunkProtection.class, new CapabilityChunkProtection.Storage(), CapabilityChunkProtection::new);
		
		
		FPDungeonProtection instance  = new FPDungeonProtection();
		MinecraftForge.EVENT_BUS.register(instance);
	}
	
	private static WeakHashMap<World, DungeonProtection> worlds = new WeakHashMap<World, DungeonProtection>();
	
	@Deprecated
	public static DungeonProtection getInstance(World w)
	{
		if(w.isClientSide)
		{
			FPLog.logger.error("Someone tried to get a DungeonProtecctor for a ClientWorld!");
			return null;
		}
		DungeonProtection pro = worlds.get(w);
		if(pro==null)
		{
			pro = new DungeonProtection();
			worlds.put(w, pro);
		}
		return pro;
	}

	public static void addProtection(World w, MutableBoundingBox box)
	{
		box.x0--;
		box.y0--;
		box.z0--;
		box.x1++;
		box.y1++;
		box.z1++;
		
		int cx1 = box.x0 >>4;
		int cx2 = box.x1 >>4;
		int cz1 = box.z0 >>4;
		int cz2 = box.z1 >>4;
		
		LazyOptional<IChunkProtection>[][] chunks = new LazyOptional[cx2 - cx1 +1][cz2 - cz1 +1];
		for(int cx=cx1;cx<=cx2;cx++)
		{
			for(int cz=cz1;cz<=cz2;cz++)
			{
				chunks[cx-cx1][cz-cz1] = w.getChunk(cx, cz).getCapability(cap_PROTECTION);
			}
		}
		
		for(int x=box.x0;x<=box.x1;x++)
		{
			for(int z=box.z0;z<=box.z1;z++)
			{
				LazyOptional<IChunkProtection> c = chunks[(x>>4)-cx1][(z>>4)-cz1];
				final int x2=x,z2=z;
				c.ifPresent(pro -> {
					for(int y=box.y0;y<=box.y1;y++)
					{
						BlockPos pos = new BlockPos(x2,y,z2);
						pro.setRawProtectionState(pos, (byte) (IChunkProtection.BLOCK_NOT_BREAKABLE | IChunkProtection.BLOCK_NOT_PLACABLE));
					}
				});

			}
		}
	}
	
	public static boolean isUnbreakable(World w, BlockPos pos)
	{
		if(w.isClientSide)
			return false;
		Block bl = w.getBlockState(pos).getBlock();
		
		if(whitelist.contains(bl))
			return false;
		
		Chunk c = (Chunk) w.getChunk(pos);
		LazyOptional<Boolean> bool = c.getCapability(cap_PROTECTION, null).lazyMap(proc -> 
		{
			byte s = proc.getRawProtectionState(pos);
			return IChunkProtection.check(s, IChunkProtection.BLOCK_NOT_BREAKABLE);
		});
		
		return bool.orElse(false);
	}
	
	public static boolean isUnplaceable(World w, BlockPos pos)
	{
		if(w.isClientSide)
			return false;
		
		Chunk c = (Chunk) w.getChunk(pos);
		LazyOptional<Boolean> bool = c.getCapability(cap_PROTECTION, null).lazyMap(proc -> 
		{
			byte s = proc.getRawProtectionState(pos);
			return IChunkProtection.check(s, IChunkProtection.BLOCK_NOT_PLACABLE);
		});
		return bool.orElse(false);
	}
	
	@SubscribeEvent
	public void addChunkCaps(AttachCapabilitiesEvent<Chunk> e)
	{
		Chunk c = e.getObject();
		if(c.getLevel() == null)
		{
			return; // most likely optifine is installed and doing some shit... 
		}
		else if(!c.getLevel().isClientSide)
		{
			e.addCapability(new ResourceLocation(Constants.MOD_ID, "protection"), new ProtectionProvider());
		}
	}


	@SubscribeEvent
	public void onChunkOpen(ChunkDataEvent.Load event)
	{
		if(event.getWorld() == null)
			return; //this is weird that this can happen
		if(event.getWorld().isClientSide())
			return;
		
		DungeonProtection dun = getInstance((World) event.getWorld());		
		
		ChunkPos pcp = event.getChunk().getPos();
		CompoundNBT nbt = event.getData().getCompound(Constants.MOD_ID);
		INBT base = nbt.get("protection");
		
		if(base!=null)
		{
			Chunk c = (Chunk) event.getChunk();
			c.getCapability(cap_PROTECTION, null).ifPresent(p ->
			{
				CapabilityChunkProtection protection = (CapabilityChunkProtection) p;
				protection.deserializeNBT((ByteArrayNBT) base);
				
				Map<Integer, Byte> map = dun.queed.remove(pcp);
				if(map!=null)
				{
					protection.addProtection(map);
					FPLog.logger.info("Applied queed protection to %s (todo:%s)", pcp, dun.queed.size());
				}	
			});
		}				
	}
	
	@SubscribeEvent
	public void onPlayerBreakBlock(BlockEvent.BreakEvent event)
	{
		if(event.getWorld().isClientSide())
			return;
			
		if(isUnbreakable((World) event.getWorld(), event.getPos()))
		{
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void onPlayerPlaceBlock(BlockEvent.EntityPlaceEvent event)
	{
		if(event.getWorld().isClientSide())
			return;

		if(isUnplaceable((World) event.getWorld(), event.getPos()))
		{
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void onExplosion(ExplosionEvent.Detonate event)
	{
		if(event.getWorld().isClientSide)
			return;
		
		Iterator<BlockPos> iter = event.getAffectedBlocks().iterator();
		while(iter.hasNext())
		{
			BlockPos pos = iter.next();
			Chunk c = (Chunk) event.getWorld().getChunk(pos);
			c.getCapability(cap_PROTECTION, null).ifPresent(prot ->
			{
				byte state = prot.getRawProtectionState(pos);
				if(IChunkProtection.check(state, IChunkProtection.BLOCK_NOT_BREAKABLE))
				{
					iter.remove();
				}
			});
		}
	}
	
	@SubscribeEvent
	public void onBucketUsed(FillBucketEvent event)
	{
		if(event.getWorld().isClientSide)
			return;
		
		if(event.getTarget() == null)
			return;
		if(event.getTarget().getType() != Type.BLOCK)
			return;
		
		
		BlockPos pos = ((BlockRayTraceResult)event.getTarget()).getBlockPos();
		Chunk c = (Chunk) event.getWorld().getChunk(pos);
		c.getCapability(cap_PROTECTION, null).ifPresent(prot ->
		{
			byte state = prot.getRawProtectionState(pos);
			
			if(event.getEmptyBucket().getItem() == Items.BUCKET && IChunkProtection.check(state, IChunkProtection.BLOCK_NOT_BREAKABLE))
			{
				event.setCanceled(true);
				return;
			}
			if(event.getEmptyBucket().getItem() != Items.BUCKET && IChunkProtection.check(state, IChunkProtection.BLOCK_NOT_PLACABLE))
			{
				event.setCanceled(true);
				return;
			}
		});
	}
	
	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event)
	{
		if(event.getWorld().isClientSide())
			return;
	
		World w = (World) event.getWorld();
		File dir = HelperChunks.getDimensionDir(w);
		dir.mkdirs();
		File file = new File(dir, "FPDungeons.dat");
		
		try
		{
			FileInputStream in = new FileInputStream(file);
			GZIPInputStream gin = new GZIPInputStream(in);
			
			getInstance((World) event.getWorld()).loadQueed(gin);
			
			gin.close();
		}
		catch (FileNotFoundException e) {
			//this is ok, because it can (new word creation)
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@SubscribeEvent
	public void onWorldSave(WorldEvent.Save event)
	{
		if(event.getWorld().isClientSide())
			return;
		World w = (World) event.getWorld();
		File dir = HelperChunks.getDimensionDir(w);
		dir.mkdirs();
		File file = new File(dir, "FPDungeons.dat");	
		AsyncTaskManager.addTask(AsyncTaskManager.FILE_IO, new Callable<Boolean>()
		{
			@Override
			public Boolean call() throws Exception 
			{
				try
				{
					FileOutputStream out = new FileOutputStream(file);
					GZIPOutputStream gout = new GZIPOutputStream(out);
					
					getInstance((World) event.getWorld()).saveQueed(gout);
					
					gout.close();
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				return true;
			}
		});
	}
	
	private static class DungeonProtection
	{
		private HashMap<ChunkPos, Map<Integer, Byte>> queed = new HashMap<ChunkPos, Map<Integer, Byte>>();
		
//		public final World w;
		
		
//		public DungeonProtection(World w)
//		{
//			this.w = w;
//		}

		private void loadQueed(InputStream s) throws IOException
		{
			DataInputStream in = new DataInputStream(s);
			int c = in.readInt();
			
			for(int i=0;i<c;i++)
			{
				long l = in.readLong();
				int length = in.readInt();
				if(length<0)
					throw new IOException("File is corrupted, tried to read negative amount of bytes");
				byte[] data = new byte[length];
				in.readFully(data);
				
				int x = (int)(  l & 4294967295L);
				int z = (int)( (l>>32) & 4294967295L);
				ChunkPos pos = new ChunkPos(x, z);
				CapabilityChunkProtection protection = new CapabilityChunkProtection();
				ByteArrayNBT arr = new ByteArrayNBT(data);
				protection.deserializeNBT( arr);
				
				if(queed.containsKey(pos))
				{
					Map<Integer, Byte> map = queed.get(pos);
					protection.addProtection(map);
					queed.put(pos, protection.map);
				}
				else
				{
					queed.put(pos, protection.map);
				}	
			}
			in.close();
		}
		
		private void saveQueed(OutputStream o) throws IOException
		{
			Entry<ChunkPos, HashMap<Integer, Byte>>[] set = queed.entrySet().toArray(new Entry[queed.entrySet().size()]);			
			DataOutputStream out = new DataOutputStream(o);
			out.writeInt(set.length);
			for(Entry<ChunkPos, HashMap<Integer, Byte>> e : set)
			{
				CapabilityChunkProtection protection = new CapabilityChunkProtection();
				protection.addProtection(e.getValue());
				ByteArrayNBT arr = protection.serializeNBT();
				
				byte[] bb = arr.getAsByteArray();	
				long l = ChunkPos.asLong(e.getKey().x, e.getKey().z);
				
				out.writeLong(l);
				out.writeInt(bb.length);
				out.write(bb);
			}
			out.flush();
			out.close();
		}
		
	}
	
	public static void addBlockToWhiteList(ResourceLocation block)
	{
		Block bl = ForgeRegistries.BLOCKS.getValue(block);
		if(bl!=null && bl!= Blocks.AIR)
		{
			whitelist.add(bl);
		}
	} 
	
	public static int order(Block b1, Block b2)
	{
		return b1.hashCode() - b2.hashCode();
	}

	private static class ProtectionProvider implements ICapabilityProvider, INBTSerializable<INBT>
	{
		CapabilityChunkProtection cap = new CapabilityChunkProtection();
		LazyOptional<CapabilityChunkProtection> opt = LazyOptional.of(() -> cap);
		
		@Override
		public INBT serializeNBT()
		{
			return cap_PROTECTION.writeNBT(cap, null);
		}

		@Override
		public void deserializeNBT(INBT nbt)
		{
			cap_PROTECTION.readNBT(cap, null, nbt);
		}

		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
		{
			if(capability == cap_PROTECTION)
			{
				return opt.cast();
			}
			return LazyOptional.empty();
		}
		
	}
	
}
