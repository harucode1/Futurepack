package futurepack.world.protection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import it.unimi.dsi.fastutil.ints.Int2ByteOpenHashMap;
import net.minecraft.nbt.ByteArrayNBT;
import net.minecraft.nbt.ByteNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.util.INBTSerializable;

public class CapabilityChunkProtection implements INBTSerializable<ByteArrayNBT>, IChunkProtection
{		
	
	public static class Storage implements IStorage<IChunkProtection>
	{
		private final int LENGTH = 16*16 * 256;
		
		@Override
		public INBT writeNBT(Capability<IChunkProtection> capability, IChunkProtection instance, Direction side)
		{
			if(instance.hasChunkProtection())
			{
				byte[] data = new byte[LENGTH];
				int i = 0;
				for(BlockPos pos : BlockPos.betweenClosed(0, 0, 0, 15, 255, 15))
				{
					i = (pos.getX()<<12) | (pos.getZ()<<8) | (pos.getY());
					
					if(i >= LENGTH)
						System.out.println(pos);
					data[i] = instance.getRawProtectionState(pos);
					i++;
				}
				return new ByteArrayNBT(data);
			}
			else
			{
				return ByteNBT.valueOf((byte)1);
			}
		}

		@Override
		public void readNBT(Capability<IChunkProtection> capability, IChunkProtection instance, Direction side, INBT nbt)
		{
			if(nbt instanceof ByteArrayNBT)
			{
				byte[] data = ((ByteArrayNBT) nbt).getAsByteArray();
				if(data.length!=LENGTH)
					throw new IllegalArgumentException("ArraySizes do not match! Got " + data.length + ", but expected " + LENGTH);
				
				int i = 0;
				for(BlockPos pos : BlockPos.betweenClosed(0, 0, 0, 15, 255, 15))
				{
					i = (pos.getX()<<12) | (pos.getZ()<<8) | (pos.getY());
					instance.setRawProtectionState(pos, data[i]);
				}
			}
		}
	}

	Map<Integer, Byte> map;
	
	
	public CapabilityChunkProtection()
	{
		map = new Int2ByteOpenHashMap();
	}
	
	public void addProtection(Map<Integer, Byte> queed)
	{
		if(queed!=null)
		{	
			map.putAll(queed);
		}	
	}

	@Deprecated
	@Override
	public ByteArrayNBT serializeNBT()
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();		
		try
		{
			GZIPOutputStream zip = new GZIPOutputStream(out);
			DataOutputStream data = new DataOutputStream(zip);
			
			map.entrySet().removeIf(e -> {return e.getValue()==null || e.getValue()==0;}); //removing 'wrong' states
			
			data.writeInt(map.entrySet().size());
			 
			for(Entry<Integer, Byte> e : map.entrySet())
			{
				data.writeInt(e.getKey());
				data.writeByte(e.getValue());
			}
			
			data.close();
		}
		catch (IOException e)
		{		
			e.printStackTrace();
		}		
		return new ByteArrayNBT(out.toByteArray());
	}

	@Deprecated
	@Override
	public void deserializeNBT(ByteArrayNBT nbt)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(nbt.getAsByteArray());
		try
		{
			GZIPInputStream zip = new GZIPInputStream(in);
			DataInputStream data = new DataInputStream(zip);
			
			int s=data.readInt();
			for(int i=0;i<s;i++)
			{
				int pos = data.readInt();
				byte state = data.readByte();
				if(state!=0)
					map.put(pos, state);
			}
			data.close();
		}
		catch (IOException e)
		{		
			e.printStackTrace();
		}	
	}				
	
	public void setState(int pos, byte state)
	{
		if(state==0)
			map.remove(pos);
		else
			map.put(pos, state);
	}
	
	public byte getState(int pos)
	{
		return map.getOrDefault(pos, (byte) 0);
	}
	
	public boolean hasProtection()
	{
		for(Byte b : map.values())
		{
			if(b!=null && b!=0)
				return true;
		};
		return false;
	}

	@Override
	public byte getRawProtectionState(BlockPos pos)
	{
		return getState(IChunkProtection.getPosition(pos));
	}

	@Override
	public void setRawProtectionState(BlockPos pos, byte state)
	{
		setState(IChunkProtection.getPosition(pos), state);
	}

	@Override
	public boolean hasChunkProtection()
	{
		return hasProtection();
	}
}
