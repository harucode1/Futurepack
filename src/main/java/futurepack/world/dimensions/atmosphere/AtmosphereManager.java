package futurepack.world.dimensions.atmosphere;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import futurepack.api.Constants;
import futurepack.api.interfaces.IAirSupply;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.IPlanet;
import futurepack.common.FPLog;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessagePlayerAirTanks;
import futurepack.common.sync.NetworkHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.network.PacketDistributor;

public class AtmosphereManager implements Runnable
{
	public static final AtmosphereManager INSTANCE = new AtmosphereManager();
	
	@CapabilityInject(IAirSupply.class)
	public static final Capability<IAirSupply> cap_AIR = null;
	@CapabilityInject(IChunkAtmosphere.class)
	public static final Capability<IChunkAtmosphere> cap_ATMOSPHERE = null;
	
	private Thread thread;
	
	public static void init()
	{
		CapabilityManager.INSTANCE.register(IAirSupply.class, new CapabilityAirSupply.Storage(), CapabilityAirSupply::new);
		CapabilityManager.INSTANCE.register(IChunkAtmosphere.class, new CapabilityAtmosphere.Storage(), CapabilityAtmosphere::new);
		
		MinecraftForge.EVENT_BUS.register(INSTANCE);
	}
	
	private Set<ServerWorld> quee = new HashSet<ServerWorld>();
	private final Object lock = new Object();
	
	@SubscribeEvent
	public void addAirCapsToEntity(AttachCapabilitiesEvent<Entity> e)
	{
		if(e.getGenericType()==Entity.class)
		{
			if(e.getObject() instanceof LivingEntity)
			{	
				e.addCapability(new ResourceLocation(Constants.MOD_ID, "air"), new AirProvider());
			}
		}
	}
	
	@SubscribeEvent
	public void addAirCapsToChunk(AttachCapabilitiesEvent<Chunk> e)
	{
		if(e.getGenericType()==Chunk.class)
		{
			Chunk c = e.getObject();
			if(c.getLevel() == null)
			{
				return; //most likely optifine in init
			}
			else if(!c.getLevel().isClientSide && !hasWorldOxygen(c.getLevel()))
			{
				e.addCapability(new ResourceLocation(Constants.MOD_ID, "atmosphere"), new AtmosphereProvider());
			}
			
		}
	}
	
	private static class AirProvider implements ICapabilityProvider, INBTSerializable<IntNBT>
	{
		LazyOptional<IAirSupply> air = LazyOptional.of(cap_AIR::getDefaultInstance);
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
		{
			if(capability==cap_AIR)
			{
				return air.cast();
			}
			return LazyOptional.empty();
		}

		@Override
		public IntNBT serializeNBT()
		{
			return IntNBT.valueOf(air.orElse(null).getAir());
		}

		@Override
		public void deserializeNBT(IntNBT nbt)
		{
			int air = nbt.getAsInt();
			IAirSupply iair = this.air.orElse(null);
			iair.addAir(air-iair.getAir());
		}
		
	}
	
	private static class AtmosphereProvider implements ICapabilityProvider, INBTSerializable<INBT>
	{
		LazyOptional<CapabilityAtmosphere> cap = LazyOptional.of(CapabilityAtmosphere::new);
		
		@Override
		public INBT serializeNBT()
		{
			return cap_ATMOSPHERE.writeNBT(cap.orElseThrow(NullPointerException::new), null);
		}

		@Override
		public void deserializeNBT(INBT nbt)
		{
			cap_ATMOSPHERE.readNBT(cap.orElseThrow(NullPointerException::new), null, nbt);
		}

		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
		{
			if(capability == cap_ATMOSPHERE)
			{
				return cap.cast();
			}
			return LazyOptional.empty();
		}
		
	}
	
	@SubscribeEvent
	public void onEntityTick(LivingEvent.LivingUpdateEvent event)
	{
		if(!hasEntityOxygen(event.getEntityLiving()))
		{
			event.getEntity().getCapability(cap_AIR, null).ifPresent(air -> 
			{
				air.reduceAir(1);
				
				World w = event.getEntity().getCommandSenderWorld();
				if(!w.isClientSide)
				{
					double d0 = event.getEntity().getY() + event.getEntity().getEyeHeight();
		            BlockPos blockpos = new BlockPos(event.getEntity().getX(), d0, event.getEntity().getZ());
		            Chunk c = w.getChunkAt(blockpos);
		            c.getCapability(cap_ATMOSPHERE, null).ifPresent( atm -> 
		            {
		            	int d = air.getMaxAir() - air.getAir();
		            	int added = atm.removeAir(blockpos.getX()&15, blockpos.getY()&255, blockpos.getZ()&15, d);
		            	air.addAir(added);
		            });
				}
				
				if(air.getAir()<=-20)
				{
					event.getEntityLiving().hurt(DamageSource.DROWN, 2F);
					air.addAir(20);
				}
				if(air.getAir()%20 == 0 && event.getEntityLiving() instanceof ServerPlayerEntity)
				{
					ServerPlayerEntity mp = (ServerPlayerEntity) event.getEntityLiving();
					if(mp==null)
						throw new NullPointerException("Player is null");
					NetworkHandler.sendPlayerAir(mp);
				}
				event.getEntityLiving().setAirSupply(air.getAir());
			});
		}
	}
	
	public static boolean hasWorldOxygen(World w)
	{
		IPlanet planet = FPPlanetRegistry.instance.getPlanetSafe(w);
		return planet.hasBreathableAtmosphere();
	}
	
	public static boolean hasEntityOxygen(Entity e)
	{
		return hasWorldOxygen(e.getCommandSenderWorld());
	}
	
	public int getAir(Entity e)
	{
		if(hasEntityOxygen(e))
		{
			return e.getAirSupply();
		}
		else
		{
			IAirSupply supp = e.getCapability(cap_AIR, null).orElseThrow(NullPointerException::new);
			return supp.getAir();
		}	
	}
	
	@SubscribeEvent
	public void onWorldTick(TickEvent.WorldTickEvent event)
	{
		if(event.side == LogicalSide.SERVER && event.phase == Phase.END)
		{
			if(!hasWorldOxygen(event.world))
			{
				ServerWorld serv = (ServerWorld) event.world;
				addWorldToUpdateQuee(serv);
			}
		}
	}

	private void addWorldToUpdateQuee(ServerWorld s)
	{
		try
		{
			synchronized (lock)
			{
				quee.add(s);
			}
			
			if(thread==null || !thread.isAlive())
			{
				thread = new Thread(this, "FP-Atmosphere");
				thread.setDaemon(true);
				thread.start();
			}
		}
		catch(NullPointerException e)
		{
			//see github issue #506
			//it can crash inside the HashSet
			//I think this is a synchronized issue, as this also happened with the ArrayList
			FPLog.logger.warn("NPE in AtmoshphereManager#addWorldToUpdateQuee");
			FPLog.logger.catching(e);
		}
	}
	
	@Override
	public void run()
	{
		int empty = 0;
		
		while(empty < 1200)
		{
			if(quee.isEmpty())
			{
				empty++;
				try
				{
					Thread.sleep(50);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				empty=0;
				Collection<ServerWorld> list = null;
				synchronized (lock)
				{
					list = quee;
					quee = new HashSet<>(list.size());
				}
				list.forEach(CapabilityAtmosphere::updateAthmosphere);
				list.clear();
			}
		}
	}
	
	public static IAirSupply getAirSupplyFromEntity(final LivingEntity base)
	{
		if(hasEntityOxygen(base))
		{
			return new IAirSupply()
			{	
				@Override
				public void reduceAir(int amount)
				{
					if (base.isEyeInFluid(FluidTags.WATER))
					{
						addAir(-amount);
					}
				}
				
				@Override
				public int getMaxAir()
				{
					return 300;
				}
				
				@Override
				public int getAir()
				{
					return base.getAirSupply();
				}
				
				@Override
				public void addAir(int amount)
				{
					base.setAirSupply(getAir() + amount);
				}
			};
		}
		else
		{
			final IAirSupply supp = base.getCapability(cap_AIR, null).orElse(null);
			if(supp!=null)
			{
				return new IAirSupply()
				{
					
					@Override
					public void reduceAir(int amount)
					{
						World w = base.getCommandSenderWorld();
						double d0 = base.getY() + base.getEyeHeight();
			            BlockPos pos = new BlockPos(base.getX(), d0, base.getZ());
			            LazyOptional<IChunkAtmosphere> c = w.getChunkAt(pos).getCapability(cap_ATMOSPHERE);
			            if(c.isPresent())
			            {
			            	IChunkAtmosphere atm = c.orElseThrow(NullPointerException::new);
			            	amount -= atm.removeAir(pos.getX()&15, pos.getY(), pos.getZ()&15, amount);
			            }
			            if(amount > 0)
			            {
			            	supp.reduceAir(amount);
			            }
					}
					
					@Override
					public int getMaxAir()
					{
						return supp.getMaxAir();
					}
					
					@Override
					public int getAir()
					{
						return supp.getAir();
					}
					
					@Override
					public void addAir(int amount)
					{
						supp.addAir(amount);
					}
				};
			}
			return null; 
		}
	}

	public static void setAirTanks(float full, PlayerEntity player) 
	{
		if(!player.getCommandSenderWorld().isClientSide())
		{
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)player), new MessagePlayerAirTanks(full));
		}
	}
}
