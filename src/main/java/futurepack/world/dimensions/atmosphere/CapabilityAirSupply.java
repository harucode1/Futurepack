package futurepack.world.dimensions.atmosphere;

import futurepack.api.interfaces.IAirSupply;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class CapabilityAirSupply implements IAirSupply
{
	public static class Storage implements IStorage<IAirSupply>
	{

		@Override
		public INBT writeNBT(Capability<IAirSupply> capability, IAirSupply instance, Direction side)
		{
			return IntNBT.valueOf(instance.getAir());
		}

		@Override
		public void readNBT(Capability<IAirSupply> capability, IAirSupply instance, Direction side, INBT nbt)
		{
			if(nbt instanceof IntNBT)
			{
				int air = ((IntNBT)nbt).getAsInt();
				instance.addAir(air-instance.getAir());
			}
		}
	}
	
	private int air = 300;

	@Override
	public int getAir()
	{
		return air;
	}

	@Override
	public void addAir(int amount)
	{
		air+=amount;
	}

	@Override
	public void reduceAir(int amount)
	{
		air-=amount;
	} 
	
	@Override
	public int getMaxAir()
	{
		return 300;
	}
	
}
