package futurepack.world.dimensions.biomes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.google.common.base.Suppliers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryLookupCodec;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.feature.structure.Structure;

public class RemappedBiomeProvider extends BiomeProvider 
{
	public static final Codec<RemappedBiomeProvider> CODEC = RecordCodecBuilder.create((p) -> 
	{
		return p.group(
				BiomeProvider.CODEC.fieldOf("biomeprovider").forGetter(RemappedBiomeProvider::getBiomeProviderBase),
				Codec.unboundedMap(ResourceLocation.CODEC, ResourceLocation.CODEC).fieldOf("biome_map").forGetter(RemappedBiomeProvider::getBiomeMap),
				ResourceLocation.CODEC.fieldOf("default_biome").forGetter(c -> c.defaultBiome),
				RegistryLookupCodec.create(Registry.BIOME_REGISTRY).forGetter((overworldProvider) -> {
			         return overworldProvider.lookupRegistry;
			      })
		).apply(p, RemappedBiomeProvider::new);
	});
	
	private BiomeProvider base;
	private Supplier<Map<Biome, Biome>> biomeOverrides;
	private Map<ResourceLocation,  ResourceLocation> rawBiomeOverrides;
	private ResourceLocation defaultBiome;
	private Supplier<List<Biome>> list;
	private final Registry<Biome> lookupRegistry;
	
	protected RemappedBiomeProvider(BiomeProvider base, Map<ResourceLocation, ResourceLocation> biomeOverrides, ResourceLocation defaultBiome, Registry<Biome> lookupRegistry)
	{
		super((List<Biome>)null);
		this.biomeOverrides = convert(biomeOverrides, lookupRegistry);
		list = Suppliers.memoize(() -> remap(base.possibleBiomes(), this.biomeOverrides.get(), convert(defaultBiome, lookupRegistry)));
		this.base = base;
		this.rawBiomeOverrides = biomeOverrides;
		this.defaultBiome = defaultBiome;
		this.lookupRegistry = lookupRegistry;
	}
	
	@Override
	public List<Biome> possibleBiomes() 
	{
		return list.get();
	}

	@Override
	public Biome getNoiseBiome(int x, int y, int z) 
	{
		return remap(base.getNoiseBiome(x, y, z), biomeOverrides.get(), convert(defaultBiome, lookupRegistry));
	}

	@Override
	protected Codec<? extends BiomeProvider> codec() 
	{
		return CODEC;
	}

	@Override
	public BiomeProvider withSeed(long seed) 
	{
		return new RemappedBiomeProvider(base.withSeed(seed), rawBiomeOverrides, defaultBiome, lookupRegistry);
	}
	
	public BiomeProvider getBiomeProviderBase() 
	{
		return base;
	}
	
	public Map<ResourceLocation, ResourceLocation> getBiomeMap()
	{
		return rawBiomeOverrides;
	}
	
	@Override
	public boolean canGenerateStructure(Structure<?> structureIn) 
	{
		return this.supportedStructures.computeIfAbsent(structureIn, (structure) -> 
		{
			return this.list.get().stream().anyMatch((biome) -> 
			{
				return biome.getGenerationSettings().isValidStart(structure);
			});
		});
	}

	@Override
	public Set<BlockState> getSurfaceBlocks() 
	{
		if (this.surfaceBlocks.isEmpty()) 
		{
			for(Biome biome : this.list.get()) 
			{
				this.surfaceBlocks.add(biome.getGenerationSettings().getSurfaceBuilderConfig().getTopMaterial());
			}
		}

		return this.surfaceBlocks;
	}
	
	public static Biome remap(Biome b, Map<Biome, Biome> remapping, Supplier<Biome> defaultBiome)
	{
		Biome mapped = remapping.getOrDefault(b, defaultBiome.get());
		if(mapped==null)
			throw new NullPointerException(mapped +" is null");
		return mapped;
	}
	
	public static List<Biome> remap(List<Biome> biomes, Map<Biome, Biome> remapping, Supplier<Biome> defaultBiome)
	{
		return biomes.parallelStream()
				.map(b -> remap(b, remapping, defaultBiome))
				.distinct()
				.collect(Collectors.toList());
	}
	
	public static Supplier<Map<Biome, Biome>> convert(Map<ResourceLocation, ResourceLocation> biomeOverrides2, Registry<Biome> registry)
	{
		return Suppliers.memoize(() -> {
			HashMap<Biome, Biome> map = new HashMap<>();
			biomeOverrides2.entrySet().forEach(e -> {
				Biome key = registry.getOrThrow(RegistryKey.create(Registry.BIOME_REGISTRY, e.getKey()));
				Biome value = registry.getOrThrow(RegistryKey.create(Registry.BIOME_REGISTRY, e.getValue()));
				
				map.put(key, value);
			});
			
			return map;
		});
	}
	
	public static Supplier<Biome> convert(ResourceLocation biome, Registry<Biome> registry)
	{
		return Suppliers.memoize(() -> {
			return registry.getOrThrow(RegistryKey.create(Registry.BIOME_REGISTRY, biome));
		});
	}
}
