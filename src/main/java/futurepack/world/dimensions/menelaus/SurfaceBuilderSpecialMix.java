package futurepack.world.dimensions.menelaus;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;

public class SurfaceBuilderSpecialMix extends SurfaceBuilder<SurfaceBuilderConfig>
{
	private final float specialChance;
	private final SurfaceBuilderConfig specialConfig;
	
	
	public SurfaceBuilderSpecialMix(Codec<SurfaceBuilderConfig> serializer, SurfaceBuilderConfig specialConfig, float specialChance) 
	{
		super(serializer);
		this.specialChance = specialChance;
		this.specialConfig = specialConfig;
	}
	
	@Override
	public void apply(Random random, IChunk chunkIn, Biome biomeIn, int x, int z, int startHeight, double noise, BlockState defaultBlock, BlockState defaultFluid, int seaLevel, long seed, SurfaceBuilderConfig config)
	{
		if (noise > specialChance) {
			SurfaceBuilder.DEFAULT.apply(random, chunkIn, biomeIn, x, z, startHeight, noise, defaultBlock, defaultFluid, seaLevel, seed, specialConfig);
		} else {
			SurfaceBuilder.DEFAULT.apply(random, chunkIn, biomeIn, x, z, startHeight, noise, defaultBlock, defaultFluid, seaLevel, seed, config);
		}
	}

}
