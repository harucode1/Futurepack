package futurepack.world.dimensions;

import java.util.function.Consumer;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Blockreader;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.structure.StructureManager;

public class ChunkGeneratorAsteroidBelt extends ChunkGenerator 
{
	public static final Codec<ChunkGeneratorAsteroidBelt> CODEC = AsteroidBeltSettings.CODEC.fieldOf("settings").xmap(ChunkGeneratorAsteroidBelt::new, ChunkGeneratorAsteroidBelt::settings).codec();

	private AsteroidBeltSettings settings;
	
	public ChunkGeneratorAsteroidBelt(BiomeProvider p_i231887_1_, BiomeProvider p_i231887_2_, AsteroidBeltSettings setting, long seed) 
	{
		super(p_i231887_1_, p_i231887_2_, setting.getSettings(), seed);
		this.settings = setting;
	}
	
	public ChunkGeneratorAsteroidBelt(AsteroidBeltSettings setting) 
	{
		this(new SingleBiomeProvider(setting.getBiomeSupplier()), new SingleBiomeProvider(setting.getBiomeSupplier()), setting, 0L);
	}

	@Override
	protected Codec<ChunkGeneratorAsteroidBelt> codec() 
	{
		return CODEC;
	}

	public AsteroidBeltSettings settings() 
	{
	      return this.settings;
	   }
	
	@Override
	public ChunkGenerator withSeed(long seed) 
	{
		return new ChunkGeneratorAsteroidBelt(biomeSource, runtimeBiomeSource, settings.withSeed(seed), seed);
	}

	@Override
	public void buildSurfaceAndBedrock(WorldGenRegion p_225551_1_, IChunk chunk) 
	{
//		BlockState bedrock = Blocks.BEDROCK.getDefaultState();
//		BlockPos.Mutable mut = new BlockPos.Mutable();
//		for(int x=0;x<16;x++)
//		{
//			for(int z=0;z<16;z++)
//			{
//				mut.setPos(x, 0, z);
//				chunk.setBlockState(mut, bedrock, false);
//			}
//		}
	}

	//set blocks in chunk
	@Override
	public void fillFromNoise(IWorld w, StructureManager p_230352_2_, IChunk chunk) 
	{
		BlockPos.Mutable mut = new BlockPos.Mutable();
		Heightmap heightmap = chunk.getOrCreateHeightmapUnprimed(Heightmap.Type.OCEAN_FLOOR_WG);
		Heightmap heightmap1 = chunk.getOrCreateHeightmapUnprimed(Heightmap.Type.WORLD_SURFACE_WG);
	    BlockState stone = Blocks.STONE.defaultBlockState();
		
		for(int x=0;x<16;x++)
		{
			for(int z=0;z<16;z++)
			{
				
				generatePillarAt(chunk.getPos().x*16 +x, chunk.getPos().z*16 +z, mut, p -> {
					chunk.setBlockState(p, stone, false);
					heightmap .update(p.getX() & 15, p.getY(), p.getZ() & 15, stone);
					heightmap1.update(p.getX() & 15, p.getY(), p.getZ() & 15, stone);
				});
			}
		}
	}

	private void generatePillarAt(int x, int z, BlockPos.Mutable mut, Consumer<BlockPos.Mutable> callback)
	{
		settings.getAsteroidLayers().forEach(l -> l.generatePillarAt(x, z, mut, callback));
	}
	
	
	
	@Override
	public int getBaseHeight(int x, int z, Type heightmapType) 
	{
		BlockState[] states = new BlockState[256];
		generatePillarAt(x, z, new BlockPos.Mutable(), p -> {
			states[p.getY()] = Blocks.STONE.defaultBlockState();
		});
		BlockState air = Blocks.AIR.defaultBlockState();
		for(int i=states.length;i>=0;i--)
		{
			if(heightmapType.isOpaque().test(states[i]==null?air:states[i]))
			{
				return i;
			}
		}
		
		return 0;
	}

	//basicly a horizontal block list ?
	@Override
	public IBlockReader getBaseColumn(int x, int z)
	{
		BlockState[] states = new BlockState[256];
		generatePillarAt(x, z, new BlockPos.Mutable(), p -> {
			states[p.getY()] = Blocks.STONE.defaultBlockState();
		});
		BlockState air = Blocks.AIR.defaultBlockState();
		for(int i=0;i<states.length;i++)
		{
			if(states[i]==null)
			{
				states[i] = air;
			}
		}
		return new Blockreader(states);
	}

}
