package futurepack.world.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import futurepack.common.FPLog;
import futurepack.common.FPLootFunctions;
import futurepack.common.item.ItemRam;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.ILootSerializer;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootFunctionType;
import net.minecraft.loot.RandomValueRange;
import net.minecraft.loot.functions.ILootFunction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.JSONUtils;

public class LootFunctionSetupRam implements ILootFunction
{
	private final Boolean isToasted;
    private final RandomValueRange ram;
    private final String[] ramid;

    public LootFunctionSetupRam(Boolean toastedIn, RandomValueRange ramRangeIn, String...ramidIn)
    {
        isToasted = toastedIn;
        ram = ramRangeIn;
        ramid = ramidIn;
    }

    @Override
	public ItemStack apply(ItemStack stack, LootContext context)
    {
    	CompoundNBT innerNBT = new CompoundNBT();
    	
    	if(ram.getMax() > 10 || ram.getMin() < 0)
    		FPLog.logger.warn("Ram power is out of normal range!");
    		
    	innerNBT.putFloat("ram", ram.getInt(context.getRandom()));
    	
    	if(!isToasted)
    	{
    		if(ItemRam.getRam(stack)!=null)
    		{
    			stack.setTag(innerNBT);
    		}
    		else
    			throw new IllegalStateException("Only rams are working! but got " + stack);
    	}
    	else
    	{
    		CompoundNBT outerNBT = new CompoundNBT();
    		outerNBT.put("tag", innerNBT);
    		
    		if(ramid!=null)
    		{
	    		if(ramid.length >= 2)
	    		{
	        		outerNBT.putString("id", ramid[context.getRandom().nextInt(ramid.length)]);
	    		}
	    		else if(ramid.length == 1)
	    		{
	    			outerNBT.putString("id", ramid[0]);
	    		}
    		}
    		outerNBT.putBoolean("toasted", true);
    		outerNBT.putInt("Count", 1);
    		stack.setTag(outerNBT);
//    		stack.setItemDamage(ItemSpaceship.toasted_ram);
    	}

        return stack;
    }

    public static class Serializer implements ILootSerializer<LootFunctionSetupRam>
    {
    	
            @Override
			public void serialize(JsonObject object, LootFunctionSetupRam functionClazz, JsonSerializationContext serializationContext)
            {
            	object.add("toasted", serializationContext.serialize(functionClazz.isToasted));
            	object.add("ram", serializationContext.serialize(functionClazz.ram));
            	if(functionClazz.isToasted)
            	{
            		object.add("ramid", serializationContext.serialize(functionClazz.ramid));
            	}
            }

            @Override
			public LootFunctionSetupRam deserialize(JsonObject object, JsonDeserializationContext deserializationContext)
            {
            	Boolean toasted = JSONUtils.getAsObject(object, "toasted", deserializationContext, Boolean.class);
            	RandomValueRange ram = JSONUtils.getAsObject(object, "ram", deserializationContext, RandomValueRange.class);
            	String[] ramid = null;
            	if(toasted)
            	{
            		ramid = JSONUtils.getAsObject(object, "ramid", deserializationContext, String[].class);
            	}
            	
                return new LootFunctionSetupRam(toasted, ram, ramid);
            }
    }

	@Override
	public LootFunctionType getType() 
	{
		return FPLootFunctions.SETUP_RAM;
	}
    
}
