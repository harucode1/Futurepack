package futurepack.world.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import futurepack.common.FPLog;
import futurepack.common.FPLootFunctions;
import futurepack.common.item.ItemChip;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.ILootSerializer;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootFunctionType;
import net.minecraft.loot.RandomValueRange;
import net.minecraft.loot.functions.ILootFunction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.JSONUtils;

public class LootFunctionSetupChip implements ILootFunction
{
	private final Boolean isToasted;
    private final RandomValueRange power;
//    private final RandomValueRange type;
    private final String[] chipid;

    public LootFunctionSetupChip(Boolean toastedIn, RandomValueRange powerRangeIn, String...chipidIn)
    {
        isToasted = toastedIn;
//        type = typeRangeIn;
        power = powerRangeIn;
        chipid = chipidIn;
    }

    @Override
	public ItemStack apply(ItemStack stack, LootContext context)
    {
    	CompoundNBT innerNBT = new CompoundNBT();
    	
    	if(power.getMax() > 10 || power.getMin() < 0)
    		FPLog.logger.warn("Chip power is out of normal range!");
    		
    	innerNBT.putFloat("power", (power.getInt(context.getRandom())*0.2F) + 1);
    	
    	if(!isToasted)
    	{
    		if(ItemChip.getChip(stack)!=null)
    		{
    			stack.setTag(innerNBT);
    		}
    		else
    			throw new IllegalStateException("Only chips are working! but got " + stack);
    	}
    	else
    	{
    		CompoundNBT outerNBT = new CompoundNBT();
    		outerNBT.put("tag", innerNBT);
    		
    		if(chipid!=null)
    		{
	    		if(chipid.length >= 2)
	    		{
	        		outerNBT.putString("id", chipid[context.getRandom().nextInt(chipid.length)]);
	    		}
	    		else if(chipid.length == 1)
	    		{
	    			outerNBT.putString("id", chipid[0]);
	    		}
    		}
    		outerNBT.putBoolean("toasted", true);
//    		outerNBT.putInt("Damage", type.generateInt(context.getRandom())); //damage is item meta
    		
    		
    		outerNBT.putInt("Count", 1);
    		stack.setTag(outerNBT);
//    		stack.setItemDamage(ComputerItems.toasted_chip);
    	}

        return stack;
    }

    public static class Serializer implements ILootSerializer<LootFunctionSetupChip>
    {
            @Override
			public void serialize(JsonObject object, LootFunctionSetupChip functionClazz, JsonSerializationContext serializationContext)
            {
            	object.add("toasted", serializationContext.serialize(functionClazz.isToasted));
//            	object.add("type", serializationContext.serialize(functionClazz.type));
            	object.add("power", serializationContext.serialize(functionClazz.power));
            	if(functionClazz.isToasted)
            		object.add("chipid", serializationContext.serialize(functionClazz.chipid));
            }

            @Override
			public LootFunctionSetupChip deserialize(JsonObject object, JsonDeserializationContext deserializationContext)
            {
            	Boolean toasted = JSONUtils.getAsObject(object, "toasted", deserializationContext, Boolean.class);
//            	RandomValueRange type = JSONUtils.deserializeClass(object, "type", deserializationContext, RandomValueRange.class);
            	RandomValueRange power = JSONUtils.getAsObject(object, "power", deserializationContext, RandomValueRange.class);
            	String[] chipid = null;
            	if(toasted)
            	{
            		chipid = JSONUtils.getAsObject(object, "chipid", deserializationContext, String[].class);
            	}
            	
                return new LootFunctionSetupChip(toasted, power, chipid);
            }
    }

	@Override
	public LootFunctionType getType() 
	{
		return FPLootFunctions.SETUP_CHIP;
	}
    
}
