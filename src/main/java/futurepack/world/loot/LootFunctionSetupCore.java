package futurepack.world.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import futurepack.common.FPLog;
import futurepack.common.FPLootFunctions;
import futurepack.common.item.ItemCore;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.ILootSerializer;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootFunctionType;
import net.minecraft.loot.RandomValueRange;
import net.minecraft.loot.functions.ILootFunction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.JSONUtils;

public class LootFunctionSetupCore implements ILootFunction
{
	private final Boolean isToasted;
    private final RandomValueRange core;
    private final String[] coreid;

    public LootFunctionSetupCore(Boolean toastedIn, RandomValueRange coreRangeIn, String...coreidIn)
    {
        isToasted = toastedIn;
        core = coreRangeIn;
        coreid = coreidIn;
    }

    @Override
	public ItemStack apply(ItemStack stack, LootContext context)
    {
    	CompoundNBT innerNBT = new CompoundNBT();
    	
    	if(core.getMax() > 10 || core.getMin() < 0)
    		FPLog.logger.warn("Core power is out of normal range!");
    		
    	innerNBT.putFloat("core", core.getInt(context.getRandom()));
    	
    	if(!isToasted)
    	{
    		if(ItemCore.getCore(stack)!=null)
    		{
    			stack.setTag(innerNBT);
    		}
    		else
    			throw new IllegalStateException("Only cores are working! but got " + stack);
    	}
    	else
    	{
    		CompoundNBT outerNBT = new CompoundNBT();
    		outerNBT.put("tag", innerNBT);

    		if(coreid!=null)
    		{
	    		if(coreid.length >= 2)
	    		{
	        		outerNBT.putString("id", coreid[context.getRandom().nextInt(coreid.length)]);
	    		}
	    		else if(coreid.length == 1)
	    		{
	    			outerNBT.putString("id", coreid[0]);
	    		}
    		}
    		outerNBT.putBoolean("toasted", true);
    		outerNBT.putInt("Count", 1);
    		stack.setTag(outerNBT);
//    		stack.setItemDamage(ItemSpaceship.toasted_core);
    	}

        return stack;
    }

    public static class Serializer implements ILootSerializer<LootFunctionSetupCore>
    {
            @Override
			public void serialize(JsonObject object, LootFunctionSetupCore functionClazz, JsonSerializationContext serializationContext)
            {
            	object.add("toasted", serializationContext.serialize(functionClazz.isToasted));
            	object.add("core", serializationContext.serialize(functionClazz.core));
            	if(functionClazz.isToasted)
            		object.add("coreid", serializationContext.serialize(functionClazz.coreid));
            }

            @Override
			public LootFunctionSetupCore deserialize(JsonObject object, JsonDeserializationContext deserializationContext)
            {
            	Boolean toasted = JSONUtils.getAsObject(object, "toasted", deserializationContext, Boolean.class);
            	RandomValueRange core = JSONUtils.getAsObject(object, "core", deserializationContext, RandomValueRange.class);
            	String[] coreid = null;
            	if(toasted)
            	{
            		coreid = JSONUtils.getAsObject(object, "coreid", deserializationContext, String[].class);
            	}
            	
                return new LootFunctionSetupCore(toasted, core, coreid);
            }
    }

	@Override
	public LootFunctionType getType() 
	{
		return FPLootFunctions.SETUP_CORE;
	}
    
}
