package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.function.Function;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;

public class LargeCanyonWorldCaver extends ExtendedCanyonWorldCaver
{
	protected int LAVA_LEVEL = 22;
	protected float scaleYRange = 1.2F;
	protected float scaleplacementYBound = 3.5F;
	protected float scaleplacementXZBound = 1.75F;
	protected boolean closeTop = true;
	
	@Override
	protected boolean carveSphere(IChunk chunkIn, Function<BlockPos, Biome> p_227208_2_, long seed, int p_222705_4_, int mainChunkX, int mainChunkZ, double xRange, double yRange, double zRange, double placementXZBound, double placementYBound, BitSet mask) 
	{
		yRange *= scaleYRange;
		placementYBound *= scaleplacementYBound;
		placementXZBound *= scaleplacementXZBound;
		
		return super.carveSphere(chunkIn, p_227208_2_, seed, p_222705_4_, mainChunkX, mainChunkZ, xRange, yRange, zRange, placementXZBound, placementYBound, mask);
	}

}
