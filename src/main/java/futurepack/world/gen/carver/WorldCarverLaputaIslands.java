package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.DoubleSupplier;
import java.util.function.Predicate;

import it.unimi.dsi.fastutil.doubles.Double2DoubleFunction;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.SimplexNoiseGenerator;
import net.minecraft.world.gen.carver.EmptyCarverConfig;

public class WorldCarverLaputaIslands extends WorldCaverMultiChunkStructures<EmptyCarverConfig, WorldCarverLaputaIslands.Laputa>
{
	public final int minRadius;
	
	public int minSurface = 60, maxSurface= 100;
	public final Predicate<Biome> acceptedPlaces;
	
	//TODO fix laputa and add to tyros
	public WorldCarverLaputaIslands(int maxRadius, int minRadius, Predicate<Biome> acceptedPlaces) 
	{
		super(EmptyCarverConfig.CODEC, 256);
		chancePerChunk = 1F / (maxRadius);
		this.maxRadius = maxRadius;
		this.minRadius = minRadius;
		this.acceptedPlaces = acceptedPlaces;
		
		minY = 128 + 50;
		maxY = minY+32;
	}

	
	@Override
	public Laputa getStructureBase(IBlockReader w, int chunkX, int chunkZ, long worldSeed, Map<Integer, Laputa> map, Random r) 
	{
//		MinecraftServer
		
		int x = r.nextInt(16);
		int z = r.nextInt(16);
		int y = minY + r.nextInt(maxY-minY);
		BlockPos pos = new BlockPos(chunkX * 16 + x, y, chunkZ*16 + z);
		//problem: getHeight functioniert nich wenn wenn chunk nicht generiert ist.
		
		
		int radius = r.nextInt(maxRadius-minRadius)+minRadius;
		int height = (int) (radius * (0.2 + 0.3*r.nextDouble()));

		
		Laputa l = new Laputa(worldSeed, pos, radius, height, r.nextDouble(), r.nextDouble());
		l.roundnessFactor = 1 + r.nextInt(4);
		l.surfaceHeight = minSurface + r.nextInt(maxSurface-minSurface);
		
		return l;
	}
	
	@Override
	protected boolean recursiveGenerate(IChunk w, int chunkX, int chunkZ, Map<Integer, Laputa> map, BitSet mask, EmptyCarverConfig config)
	{
		int r = maxRadius / 16 + 2;
		List<Laputa> list = gatherStructures(w, chunkX, chunkZ, r, 12L, map);//TODO: somehow get world seed and replace the 12L
		if(list.isEmpty())
			return false;
		
		BlockPos.Mutable posSky = new BlockPos.Mutable();
		BlockPos.Mutable posSurface = new BlockPos.Mutable();
		
		for(int x=0;x<16;x++)
		{
			for(int z=0;z<16;z++)
			{
				Double min = null;
				
				posSky.set(chunkX*16 +x, 0, chunkZ*16 + z);
				posSurface.set(chunkX*16 +x, 0, chunkZ*16 + z);
				
				LaputaPart part = getMergedParts(posSky, list);
				
				if(part==null)
					continue;
				
				int length = (int) (part.getTop() + part.getBottom());
				int skyStart = (int) (part.skyY + part.getTop());
				int surfaceStart = (int) (part.surfaceY + part.getTop());
				
				int bit;
				for(int i=0;i<length;i++)
				{
					posSurface.setY(surfaceStart-i);
					
					if(posSurface.getY() < 10)
						break;
					
					BlockState state = w.getBlockState(posSurface);
					posSky.setY(skyStart-i);
					w.setBlockState(posSky, state, false);
					w.setBlockState(posSurface, AIR, false);
					bit = x | z<<4 | (surfaceStart-i)<<8;
					mask.set(bit);
					bit = x | z<<4 | (skyStart-i)<<8;
					mask.set(bit);
				}
			}
		}
		
		
		return true;
		
	}

	@Override
	public boolean isStartChunk(Random rand, int chunkX, int chunkZ, EmptyCarverConfig config) 
	{
		return true;
	}

	@Override
	protected boolean skip(double p_222708_1_, double p_222708_3_, double p_222708_5_, int p_222708_7_) 
	{
		return false;
	}

	protected static class Laputa
	{
		private BlockPos middle;
		private int surfaceHeight = 60;
		
		private int radius;
		private int height;
		private SimplexNoiseGenerator simplex;
		private Double2DoubleFunction top, bottom;
		
		public int roundnessFactor = 6;
		
		public Laputa(long seed, BlockPos middle, int radius, int height, double m_factor1, double m_factor2)
		{
			super();
			this.middle = middle;
			this.radius = radius;
			this.height = height;
			simplex = new SimplexNoiseGenerator(new Random(seed ^ middle.hashCode()));
			
			double m = Math.max(height, radius) * 0.2 * (1-m_factor1) + m_factor1* Math.min(height, radius)* 0.8;
			CubicSpline[] topA = CubicSpline.fromPoints(new double[][]{{0,height,0},{m,m,-1},{radius,0,-5.6 * (m_factor1+0.01)}});
			top = CubicSpline.asFunction(topA);
			
			int depth = height/2;
			m = Math.max(depth, radius) * 0.2 * (1-m_factor2) + m_factor2* Math.min(depth, radius)* 0.8;
			CubicSpline[] bottomA = CubicSpline.fromPoints(new double[][]{{0,depth,0},{m,m,-1},{radius,0,-5.6 * (m_factor2+0.01)}});
			bottom = CubicSpline.asFunction(bottomA);
		}
		
		public LaputaPart getHeight(BlockPos pos)
		{
			double dis = dis(pos);
			
			if(dis > radius)
				return null;
			
			return new LaputaPart(middle, surfaceHeight, () -> 
			{
				//s1 = s1 * 0.1 * radius;
				//s2 = s2 * 0.05 * height;
				double s1,s2;
				synchronized (this)
				{
					int dx = pos.getX() - middle.getX();
					int dz = pos.getZ() - middle.getZ();
					double angle = Math.atan2(dx, dz);
					
					s1 = simplex.getValue(pos.getX(), pos.getZ());
					s2 = simplex.getValue(pos.getZ(), pos.getX());
					angle += Math.PI / 18D * s2;
					s1 += 0.1 * radius * Math.sin(roundnessFactor * angle) * simplex.getValue(0, angle);
				}
				double  random_dis = dis / (radius+s1) * radius;
				return random_dis;
			}, top, bottom, dis / radius);
		}
		
		private double dis(BlockPos p1)
		{
			int x = p1.getX() - middle.getX();
			int z = p1.getZ() - middle.getZ();
			return Math.sqrt(x*x + z*z);
		}
	}
	
	private static class LaputaPart
	{
		public final BlockPos middle;
		public final int surfaceY;
		public int skyY;
		private Double top;
		private Double bottom;
		private Double distance;
		private final DoubleSupplier sup_dis;
		private final Double2DoubleFunction sup_top, sup_bottom;
		
		public final double rel_distance;
		
		public LaputaPart(BlockPos middle, int surfaceY, DoubleSupplier sup_dis, Double2DoubleFunction sup_top, Double2DoubleFunction sub_bottom, double rel_distance) 
		{
			super();
			this.middle = middle;
			this.surfaceY = surfaceY;
			this.skyY = middle.getY();
			this.sup_dis = sup_dis;
			this.sup_top = sup_top;
			this.sup_bottom = sub_bottom;
			this.rel_distance = rel_distance;
		}

		public double getTop() 
		{
			if(top==null)
				top = sup_top.apply(getDistance());
			
			return top;
		}

		public double getBottom() 
		{
			if(bottom==null)
				bottom = sup_bottom.apply(getDistance());
				
			return bottom;
		}

		public double getDistance() 
		{
			if(distance ==null)
			{
				distance = sup_dis.getAsDouble(); 
			}
			return distance;
		}
	}
	
	public static LaputaPart getMergedParts(BlockPos pos, List<Laputa> islands)
	{
		if(islands.isEmpty())
			return null;
		else
		{
			LaputaPart[] parts = islands.stream().map(l -> l.getHeight(pos)).filter(l -> l!=null).toArray(LaputaPart[]::new);
			if(parts.length == 0)
				return null;
			else if(parts.length == 1)
				return parts[0];
			else
			{
				double[] weight = new double[parts.length];
				double total = 0;
				for(int i=0;i<weight.length;i++)
				{
					weight[i] = parts[i].rel_distance;
					total += parts[i].rel_distance;
				}
				for(int i=0;i<weight.length;i++)
				{
					weight[i] /= total;
				}
				
				int skyY = 0;
				int surfaceY = 0;
				for(int i=0;i<parts.length;i++)
				{
					skyY += weight[i] * parts[i].skyY;
					surfaceY += weight[i] * parts[i].surfaceY;
				}
				BlockPos newMiddle = new BlockPos(pos.getX(), skyY, pos.getZ());
				
				double top=parts[0].getTop(), bottom=parts[0].getBottom();
				for(int i=1;i<parts.length;i++)
				{
					top = Math.max(top, parts[i].getTop());
					bottom = Math.max(bottom, parts[i].getBottom());
				}
				
				final double ttop = top;
				final double bbottom = bottom;
				
				return new LaputaPart(newMiddle, surfaceY, () -> 0, p -> ttop, p -> bbottom, 0D);
				
			}
		}
	}
}
