package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableSet;

import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.carver.WorldCarver;

public class LargeMenelausCavesWorldCaver extends ExtendedCaveWorldCaver
{
	public LargeMenelausCavesWorldCaver(int height) 
	{
		super(height);
	}

	public static final CarveConfig MENEALUS = new CarveConfig(15, WorldCarver.WATER);
	
	protected Set<Block> menelausBlocks = ImmutableSet.of(TerrainBlocks.stone_m, TerrainBlocks.sandstone_m, TerrainBlocks.sand_m);

	//this could be the room
	@Override
	protected void genRoom(IChunk p_222723_1_, Function<BlockPos, Biome> p_227205_2_, long seed, int seaLevel, int x, int z, double xRange, double yRange, double zRange, float roomRadius, double roomHeight, BitSet mask) 
	{
		super.genRoom(p_222723_1_, p_227205_2_, seed, seaLevel, x, z, xRange, yRange, zRange, 2F + roomRadius, roomHeight * 2F, mask);
	}
	
	@Override
	//carveTunnel
	protected void genTunnel(IChunk chunkIn, Function<BlockPos, Biome> p_227205_2_, long seed, int seaLevel, int x, int z, double rangeX, double rangeY, double rangeZ, float radius, float area, float p_222727_15_, int depth, int maxDepth, double dy, BitSet mask) 
	{
		if(rangeY < 45)
		{
			radius *= 2.5F;
//			dy *= 0.5F;
		}
		super.genTunnel(chunkIn, p_227205_2_, seed, seaLevel, x, z, rangeX, rangeY, rangeZ,
				radius, area, p_222727_15_, depth, maxDepth, dy, mask);
	}

	@Override
	protected boolean hasWater(IChunk chunk, int mainChunkX, int mainChunkZ, int minXPos, int maxXPos, int minYPos, int maxYPos, int minZPos, int maxZPos)
	{
		if(minYPos < MENEALUS.FLUID_HEIGHT+1)
		{
			minYPos= MENEALUS.FLUID_HEIGHT+1;
		}
		if(maxYPos < minYPos)
			return false;
		
		return super.hasWater(chunk, mainChunkX, mainChunkZ, minXPos, maxXPos, minYPos, maxYPos, minZPos, maxZPos);
	}

	@Override
	protected boolean canReplaceBlock(BlockState target)
	{
		if(menelausBlocks.contains(target.getBlock()))
			return true;
		
		return super.canReplaceBlock(target);
	}
	
	@Override
	public CarveConfig getConfig() 
	{
		return MENEALUS;
	}
}
