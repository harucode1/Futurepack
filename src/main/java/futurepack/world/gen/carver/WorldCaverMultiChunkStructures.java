package futurepack.world.gen.carver;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

import com.mojang.serialization.Codec;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.carver.ICarverConfig;
import net.minecraft.world.gen.carver.WorldCarver;

public abstract class WorldCaverMultiChunkStructures<C extends ICarverConfig, T> extends WorldCarver<C> 
{
	protected WeakReference<Map<Integer, T>> ref;
	protected int maxRadius;

	public float chancePerChunk;
	public int minY, maxY;
	
	
	public WorldCaverMultiChunkStructures(Codec<C> codec, int height) 
	{
		super(codec, height);
	}
	
	public static Random getRandomWithSeed(long worldSeed, long seed, int x, int z)
    {
        return new Random(worldSeed + x * x * 4987142 + x * 5947611 + z * z * 4392871L + z * 389711 ^ seed);
    }
	
	public static float jitter(Random r, float min, float max)
	{
		float d = max - min;
		return min + d * r.nextFloat();
	}
	
	public List<T> gatherStructures(IBlockReader w, int chunkX, int chunkZ, int radiusChunks, long worldSeed, Map<Integer, T> map)
	{
		ArrayList<T> list = new ArrayList<>(radiusChunks*radiusChunks);
		
		for(int k = chunkX - radiusChunks; k < chunkX+radiusChunks;k++)
		{
			for(int j = chunkZ - radiusChunks; j < chunkZ+radiusChunks;j++)
			{
				T c = getStructure(w, k, j, worldSeed, map);
				if(c!=null)
					list.add(c);
			}
		}
		return list;
	}
	
	public T getStructure(IBlockReader w, int chunkX, int chunkZ, long worldSeed, Map<Integer, T> map)
	{
		Integer i = 10000 * chunkX + chunkZ;
		
		if(map.containsKey(i))
		{
			return map.get(i);
		}
		else
		{
			Random r = getRandomWithSeed(worldSeed, 745679321L, chunkX, chunkZ);
			if(r.nextFloat() < chancePerChunk)
			{
				T t = getStructureBase(w, chunkX, chunkZ, worldSeed, map, r);
				map.put(i, t);
				return t;
			}
			map.put(i, null);
			return null;
		}	
	}
	
	//carve
	@Override
	public boolean carve(IChunk region, Function<BlockPos, Biome> pos_to_biome, Random random, int seaLevel, int chunkX, int chunkZ, int originalX, int originalZ, BitSet mask, C config) 
	{
		if(chunkX!=originalX || chunkZ!=originalZ)
			return false;
		
		int r = maxRadius / 16 + 2;
		
		Map<Integer, T> map;
		synchronized (this)
		{
			if(ref==null || (map=ref.get()) == null)
			{
				map = Collections.synchronizedMap(new HashMap<>(4 * r*r));
				ref = new WeakReference<Map<Integer,T>>(map);
			}
		}
//		map = new Int2ObjectOpenHashMap<>(4 * r*r);
		boolean b = this.recursiveGenerate(region, originalX, originalZ, map, mask, config);
		map = null;
		
		
		return b;
	}

	protected abstract boolean recursiveGenerate(IChunk w, int chunkX, int chunkZ, Map<Integer, T> map, BitSet mask, C config);
	
	public abstract T getStructureBase(IBlockReader w, int chunkX, int chunkZ, long worldSeed, Map<Integer, T> map, Random r);
	
}
