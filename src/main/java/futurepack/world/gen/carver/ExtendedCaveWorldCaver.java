package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.Random;
import java.util.function.BiPredicate;
import java.util.function.Function;

import org.apache.commons.lang3.mutable.MutableBoolean;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.fluid.FluidState;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.Mutable;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.carver.CaveWorldCarver;
import net.minecraft.world.gen.feature.ProbabilityConfig;

public class ExtendedCaveWorldCaver extends CaveWorldCarver 
{

	public static CarveConfig DEFAULT = new CarveConfig(11, LAVA);
	
	public ExtendedCaveWorldCaver(int maxHeight) 
	{
		super(ProbabilityConfig.CODEC, maxHeight);
	}
	
	public static class CarveConfig
	{
		public final int FLUID_HEIGHT;
		public final FluidState FLUID;
		
		public CarveConfig(int fLUID_HEIGHT, FluidState fLUID)
		{
			super();
			FLUID_HEIGHT = fLUID_HEIGHT;
			FLUID = fLUID;
		}
	}
	
	public CarveConfig getConfig()
	{
		return ExtendedCaveWorldCaver.DEFAULT;
	}
	
//	@Override
//	protected boolean carveBlock(IChunk chunkIn, BitSet carvingMask, Random rand, Mutable pos, Mutable pos_up, Mutable pos_down, int p_222703_7_, int p_222703_8_, int p_222703_9_, int x, int z, int chunk_x, int y, int chunk_z, AtomicBoolean is_grass) 
//	{
//		return ExtendedCaveWorldCaver.carveBlock(chunkIn, carvingMask, rand, pos, pos_up, pos_down, p_222703_7_, p_222703_8_, p_222703_9_, x, z, chunk_x, y, chunk_z, is_grass, getConfig(), this::canCarveBlock);
//	}
	
	//carveBlockChunk
	@Override
	protected boolean carveBlock(IChunk chunkIn, Function<BlockPos, Biome> pos2biome, BitSet carvingMask, Random rand, Mutable pos, Mutable pos_up, Mutable pos_down, int somethingA, int somethinB, int somethingC, int x, int z, int chunk_x, int y, int chunk_z, MutableBoolean is_grass) 
	{
		return ExtendedCaveWorldCaver.carveBlock(chunkIn, carvingMask, rand, pos, pos_up, pos_down, somethingA, somethinB, somethingC, x, z, chunk_x, y, chunk_z, is_grass, getConfig(), this::canReplaceBlock, pos2biome);
		
	}	    	   
		
	public static boolean carveBlock(IChunk chunkIn, BitSet carvingMask, Random rand, BlockPos.Mutable position, BlockPos.Mutable position_up, BlockPos.Mutable position_down, int p_222703_7_, int p_222703_8_, int p_222703_9_, int x, int z, int chunk_x, int y, int chunk_z, MutableBoolean is_grass, CarveConfig config, BiPredicate<BlockState, BlockState> canCarve, Function<BlockPos, Biome> pos2biome) 
	{
		int mask_pos = chunk_x | chunk_z << 4 | y << 8;
		if (carvingMask.get(mask_pos)) 
		{
			return false;
		}
		else 
		{
			carvingMask.set(mask_pos);
			position.set(x, y, z);
			BlockState blockstate = chunkIn.getBlockState(position);
			BlockState blockstate1 = chunkIn.getBlockState(position_up.set(position).move(Direction.UP));
			if (blockstate.getBlock() == Blocks.GRASS_BLOCK || blockstate.getBlock() == Blocks.MYCELIUM) 
			{
				is_grass.setTrue();
			}
			
			if (!canCarve.test(blockstate, blockstate1)) 
			{
				return false;
			}
			else 
			{
				if (y < config.FLUID_HEIGHT) 
				{
					chunkIn.setBlockState(position, config.FLUID.createLegacyBlock(), false);
				}
				else 
				{
					chunkIn.setBlockState(position, CAVE_AIR, false);
					if (is_grass.isTrue()) 
					{
						position_down.set(position).move(Direction.DOWN);
						if (chunkIn.getBlockState(position_down).getBlock() == Blocks.DIRT) 
						{
							Biome b = pos2biome.apply(position);
							BlockState top = b.getGenerationSettings().getSurfaceBuilderConfig().getTopMaterial();
							chunkIn.setBlockState(position_down, top, false);
						}
					}
				}

				return true;
			}
		}
	}

}
