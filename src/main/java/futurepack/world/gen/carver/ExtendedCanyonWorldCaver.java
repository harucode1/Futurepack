package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.Random;
import java.util.function.Function;

import org.apache.commons.lang3.mutable.MutableBoolean;

import futurepack.world.gen.carver.ExtendedCaveWorldCaver.CarveConfig;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.Mutable;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.carver.CanyonWorldCarver;
import net.minecraft.world.gen.feature.ProbabilityConfig;

public class ExtendedCanyonWorldCaver extends CanyonWorldCarver 
{
	
	public ExtendedCanyonWorldCaver() 
	{
		super(ProbabilityConfig.CODEC);
	}

	public CarveConfig getConfig()
	{
		return ExtendedCaveWorldCaver.DEFAULT;
	}
	
	@Override
	protected boolean carveBlock(IChunk chunkIn, Function<BlockPos, Biome> p_225556_2_, BitSet carvingMask, Random rand, Mutable pos, Mutable pos_up, Mutable pos_down, int p_222703_7_, int p_222703_8_, int p_222703_9_, int x, int z, int chunk_x, int y, int chunk_z, MutableBoolean is_grass) 
	{
		return ExtendedCaveWorldCaver.carveBlock(chunkIn, carvingMask, rand, pos, pos_up, pos_down, p_222703_7_, p_222703_8_, p_222703_9_, x, z, chunk_x, y, chunk_z, is_grass, getConfig(), this::canReplaceBlock, p_225556_2_);
	}
}
