package futurepack.world.gen.carver;

import net.minecraft.world.gen.carver.WorldCarver;


public class TyrosCaveWorldCarver extends ExtendedCaveWorldCaver
{
	public TyrosCaveWorldCarver(int maxHeight) 
	{
		super(maxHeight);
	}

	public static final CarveConfig TYROS = new CarveConfig(22, WorldCarver.LAVA);
	
    @Override
	public CarveConfig getConfig() 
    {
    	return TYROS;
    }
}
