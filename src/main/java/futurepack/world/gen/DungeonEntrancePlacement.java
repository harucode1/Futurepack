package futurepack.world.gen;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import futurepack.common.FPConfig;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.feature.WorldDecoratingHelper;
import net.minecraft.world.gen.placement.NoPlacementConfig;
import net.minecraft.world.gen.placement.Placement;

public class DungeonEntrancePlacement extends Placement<NoPlacementConfig>
{

	public DungeonEntrancePlacement() 
	{
		super(NoPlacementConfig.CODEC);
	}

	//getPosition
	@Override
	public Stream<BlockPos> getPositions(WorldDecoratingHelper helper, Random rand, NoPlacementConfig configIn, BlockPos pos) 
	{
		int chunkX = pos.getX() >> 4;
		int chunkZ = pos.getZ() >> 4;
		
		ISeedReader world = helper.level;
		
		boolean flag;
		List<? extends String> wl = FPConfig.WORLDGEN.wl_huge_dungeons.get();
		
		flag = wl.contains(world.getLevel().dimension().location().toString());
		if(flag)
		{
			if(FPConfig.WORLDGEN.hugedungeon_mindis.get()*FPConfig.WORLDGEN.hugedungeon_mindis.get() > chunkX*chunkX + chunkZ*chunkZ)
				return Stream.empty();
				
			int x = chunkX*16;
			int z = chunkZ*16;			
			
			rand.nextLong();
			double d0 = rand.nextDouble() - 0.04;
			
			if(0<d0 && d0 < FPConfig.WORLDGEN.hugedungeon_spawnrate.get() && rand.nextInt(80) < Math.max(Math.abs(chunkX), Math.abs(chunkZ)))
			{
				int w = 6;
				int d = 6;
				
				BlockPos start = new BlockPos(x + 8 + rand.nextInt(16-w), 0, z + 8 + rand.nextInt(16-d));
				
				BlockPos p00 = world.getHeightmapPos(Heightmap.Type.WORLD_SURFACE_WG, start).below();
				BlockPos p01 = world.getHeightmapPos(Heightmap.Type.WORLD_SURFACE_WG, start.offset(0,0,d)).below();
				BlockPos p10 = world.getHeightmapPos(Heightmap.Type.WORLD_SURFACE_WG, start.offset(w, 0, 0)).below();
				BlockPos p11 = world.getHeightmapPos(Heightmap.Type.WORLD_SURFACE_WG, start.offset(w, 0, d)).below();
				
				int h = (p00.getY() + p01.getY() + p10.getY() + p11.getY()) / 4;
				
				h-= 1;
				
				BlockPos entrace = new BlockPos(start.getX(), h, start.getZ());
				return Stream.of(entrace);
			}
		}        
		return Stream.empty();
	}

}
