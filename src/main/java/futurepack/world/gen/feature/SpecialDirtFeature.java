package futurepack.world.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.feature.BlockStateFeatureConfig;
import net.minecraft.world.gen.feature.Feature;
/**
 * BlockStateFeatureConfig because I use it as "1 blockstate" config
 */
public class SpecialDirtFeature extends Feature<BlockStateFeatureConfig> 
{

	public SpecialDirtFeature(Codec<BlockStateFeatureConfig> configFactoryIn) 
	{
		super(configFactoryIn);
	}

	@Override
	public boolean place(ISeedReader w, ChunkGenerator p_241855_2_, Random rand, BlockPos pos, BlockStateFeatureConfig config) 
	{
		BlockPos.Mutable mut = new BlockPos.Mutable();
		
		for(int x=-8;x<8;x+=2)
		{
			for(int z=-8;z<8;z+=2)
			{
				mut.set(pos).move(x,0,z);
				BlockPos h = w.getHeightmapPos(Type.WORLD_SURFACE_WG, mut).below();
				if(w.isWaterAt(h))
				{
					genMenelausDirt(w, h, rand, config.state);
					return true;
				}
			}
		}
		return false;
	}
	
	
	public void genMenelausDirt(IWorld w, BlockPos pos, Random r, BlockState fillerblock)
	{
		int radius = 10 + r.nextInt(5);
	
		for(int y=-radius/2;y<1;y++)
		{
			for(int x=-radius;x<radius;x++)
			{
				for(int z=-radius;z<radius;z++)
				{
					if(z*z + x*x - y*y < radius)
					{
						BlockPos xyz = pos.offset(x,y,z);
						BlockState state = w.getBlockState(xyz);
						if(state.getMaterial() != Material.WATER)
						{
							setBlock(w, xyz, fillerblock);
						}
					}
					
				}
			}
		}
	}


	
}
