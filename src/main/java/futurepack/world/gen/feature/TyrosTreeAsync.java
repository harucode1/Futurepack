package futurepack.world.gen.feature;

import java.util.Random;

import futurepack.common.block.misc.MiscBlocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.TickPriority;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class TyrosTreeAsync extends Feature<NoFeatureConfig>
{

	public TyrosTreeAsync() 
	{
		super(NoFeatureConfig.CODEC);
	}

	@Override
	public boolean place(ISeedReader w, ChunkGenerator generator, Random rand, BlockPos pos, NoFeatureConfig config) 
	{
		w.setBlock(pos, MiscBlocks.tyros_tree_gen.defaultBlockState(), 3);
		w.getBlockTicks().scheduleTick(pos,  MiscBlocks.tyros_tree_gen, 20*3, TickPriority.LOW);
		
		return false;
	}

}
