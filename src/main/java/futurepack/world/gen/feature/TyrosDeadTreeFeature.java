package futurepack.world.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.IWorldGenerationReader;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class TyrosDeadTreeFeature extends Feature<NoFeatureConfig>
{
	
	public static BlockState colors[] = {Blocks.LIGHT_GRAY_TERRACOTTA.defaultBlockState(), Blocks.CYAN_TERRACOTTA.defaultBlockState(), Blocks.BLUE_TERRACOTTA.defaultBlockState(), Blocks.LIGHT_BLUE_TERRACOTTA.defaultBlockState()};
	
	public TyrosDeadTreeFeature(Codec<NoFeatureConfig> serialiser)
	{
		super(serialiser);
	}
	
	private boolean canStand(IWorldGenerationReader w, BlockPos pos)
	{
		if(isGround(w, pos.below()))
		{
			if(isGround(w, pos.offset(-2,-1,-2)) && isGround(w, pos.offset(2,-1,2)))
			{
				if(isGround(w, pos.offset(2,-1,-2)) && isGround(w, pos.offset(-2,-1,2)))
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	private boolean isGround(IWorldGenerationReader w, BlockPos pos)
	{
		return w.isStateAtPosition(pos, s -> s.getMaterial() == Material.STONE);
	}
	
	/**
	 * generates the Tree stem
	 * @return the high of the Tree
	 */
	private void genTreeStemm(IWorldGenerationReader w, BlockPos pos, int h, Random rand)
	{	
		BlockPos.Mutable xyz = new BlockPos.Mutable();
		for(int i=0;i<h;i++)
		{
			double r = (1.0 - i/(double)h)*1.5 +1.5;
			for(int x=(int) -r;x<r;x++)
			{
				for(int z=(int) -r;z<r;z++)
				{
					if(x*x + z*z < r*r)
					{
						xyz.set(pos).move(x, i, z);				
						//w.setBlockState(xyz, colors[Math.min(Math.max(Math.abs(x + (rand.nextInt(2) - 1)), Math.abs(z + (rand.nextInt(2) - 1))), colors.length - 1)]);
						//w.setBlockState(xyz, colors[(int) Math.min(Math.max(Math.sqrt(x*x+z*z) + 0.5, 0), colors.length - 1)]);
						BlockState state = colors[(int) Math.min(Math.max(Math.sqrt(x*x+z*z) + rand.nextFloat(), 0), colors.length - 1)];
						this.setBlock(w, xyz, state);
					}
				}
			}
		}
		
		final BlockState ore = Blocks.IRON_ORE.defaultBlockState();
		
		for(int i = -3; i < 0; i++)
		{
			double r = (1.0 - i/(double)3)*1.5 +1.5;
			for(int x=(int) -r;x<r;x++)
			{
				for(int z=(int) -r;z<r;z++)
				{
					if(x*x + z*z < r*r)
					{
						xyz.set(pos).move(x, i, z);		
						if(rand.nextInt(8) <= 4)
							this.setBlock(w, xyz, ore);
						
					}
				}
			}
		} 
	}

	@Override
	public boolean place(ISeedReader w, ChunkGenerator generator, Random rand, BlockPos pos, NoFeatureConfig config) 
	{
		if(!canStand(w, pos))
			return false;
		
		int height = 10  + rand.nextInt(20);
		
		genTreeStemm(w, pos, height, rand);
		
		return true;
	}



}
