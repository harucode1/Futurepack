package futurepack.world.gen.feature;

import java.util.Random;

import futurepack.common.FPConfig;
import futurepack.common.block.misc.MiscBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class BedrockRiftFeature extends Feature<NoFeatureConfig> 
{

	public BedrockRiftFeature() 
	{
		super(NoFeatureConfig.CODEC);
	}

	@Override
	public boolean place(ISeedReader world, ChunkGenerator generator, Random rand, BlockPos pos, NoFeatureConfig config) 
	{
		for (int i = 0; i < FPConfig.WORLDGEN.bedrockRift.get() * 2; i++)
		{
			if(rand.nextFloat() < 0.5f)
				continue;
			
			BlockState state = MiscBlocks.bedrock_rift.defaultBlockState();
			pos = new BlockPos(pos.getX()+2 + rand.nextInt(12), FPConfig.WORLDGEN.bedrockRiftHeight.get(), pos.getZ() + rand.nextInt(12)+2);
				  
			if(!world.isEmptyBlock(pos))
			{
				for(int x = -2; x <= 2; x++)
				{
					for(int z = -2; z <= 2; z++)
					{
						if((Math.abs(x) >= 2 || Math.abs(z) >= 2) && rand.nextFloat() < 0.66f)
							continue;
						
						for(int y = 0; y < 4; y++)
						{
							BlockPos p = pos.offset(x,y,z);
								
							if(y == 0)
							{
							   	if(x==0 && z==0)
							   		world.setBlock(p, state, 2);   
							   	else if(world.getBlockState(p).getBlock() != Blocks.BEDROCK)
							   		world.setBlock(p, Blocks.BEDROCK.defaultBlockState(), 2);		
							}
							else
							{
								if(world.getBlockState(p).getBlock() == Blocks.BEDROCK)
									world.setBlock(p, Blocks.OBSIDIAN.defaultBlockState(), 2);  
							}
						}
					}
				}
			}
		}
		
		return true;
	}

}
