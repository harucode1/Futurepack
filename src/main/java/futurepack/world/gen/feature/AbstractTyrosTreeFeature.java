package futurepack.world.gen.feature;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.mojang.serialization.Codec;

import futurepack.api.FacingUtil;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.Mutable;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.gen.IWorldGenerationReader;
import net.minecraft.world.gen.blockstateprovider.BlockStateProvider;
import net.minecraft.world.gen.blockstateprovider.BlockStateProviderType;
import net.minecraft.world.gen.feature.BaseTreeFeatureConfig;
import net.minecraft.world.gen.feature.Feature;

public abstract class AbstractTyrosTreeFeature<T extends BaseTreeFeatureConfig> extends Feature<T> 
{

//	public final BlockState LOG;
//	public final BlockState LEAVES;
//	public final BaseTreeFeatureConfig config;
    
	public AbstractTyrosTreeFeature(Codec<T> serialiser)
	{
		super(serialiser);
//		this.LOG = log;
//		this.LEAVES = leaves;
//		
//		if(!LOG.has(BlockStateProperties.AXIS))
//		{
//			throw new IllegalArgumentException("Log need to have axis property");
//		}
//		this.config = config;
	}

	protected void genLeaves(IWorldGenerationReader w, BlockPos pos, int leaveGen, MutableBoundingBox box, BaseTreeFeatureConfig config, Random rand, Set<BlockPos> changedLeaves)
	{
		float r = (float) (leaveGen + 0.5);
		BlockPos.Mutable xyz = new BlockPos.Mutable();
		
		for(int x=(int) -r;x<r;x++)
		{
			for(int y=0;y<r;y++)
			{
				for(int z=(int) -r;z<r;z++)
				{
					if(x*x + y*y + z*z <= r*r)
					{
						xyz.set(pos).move(x,y,z);
						if (w.isStateAtPosition(xyz, s -> s.canBeReplacedByLeaves(null, null)))
						{
//							this.setBlockState(w, xyz, LEAVES);
						    setLeaf(w, rand, xyz, changedLeaves, box, config.leavesProvider);
							box.expand(new MutableBoundingBox(xyz, xyz));
						}
					}
				}
			}
		}
	}
	
	protected void connectBough(Set<BlockPos> changedLogs, IWorldGenerationReader w, BlockPos startB, BlockPos end, int leaveGen, Random rand, MutableBoundingBox box, BaseTreeFeatureConfig config)
	{
		int failCounter = 0;
		AtomicInteger offset = new AtomicInteger(0);
		BlockPos.Mutable start = new BlockPos.Mutable().set(startB);
		
		BlockStateProvider wrappedConfig = new BlockStateProvider()
		{			
			@Override
			public BlockState getState(Random r, BlockPos pos) 
			{
				return config.trunkProvider.getState(r, pos).setValue(BlockStateProperties.AXIS, Direction.from3DDataValue(offset.get()).getAxis());
			}

			@Override
			protected BlockStateProviderType<?> type() 
			{
				return null;
			}
        };
		
		while(!start.equals(end))
		{
			if(rand.nextInt(20)==0 || start.distSqr(end) > start.relative(Direction.from3DDataValue(offset.get())).distSqr(end))
			{
				start.move(Direction.from3DDataValue(offset.get()));
				if(w.isStateAtPosition(start, this::canReplace))
				{
//					BlockState state = LOG.with(BlockStateProperties.AXIS, Direction.byIndex(offset).getAxis());
//					setLogState(changedBlocks, w, start, state, box);
					setLog(w, rand, start, changedLogs, box, wrappedConfig);
				}
				else
				{
					failCounter++;
					if(failCounter>600000)
					{
						System.out.println("TyrosTreeGen is failing");
						failCounter = 0;
						break;
					}
				}
			}
			else
			{
				offset.set( (offset.get()+1) % FacingUtil.VALUES.length);
			}
		}
	}
	
	
	public boolean canReplace(BlockState state)
	{
		return state.isAir() || state.is(BlockTags.LEAVES) || state.getMaterial() == Material.WATER || state.is(BlockTags.LOGS);
	}
	
	/**
	 * Generates an tree arm from the start position to the end. The arm is bent
 	 */
	protected void genBough(Set<BlockPos> changedLogs, Set<BlockPos> changedLeaves, IWorldGenerationReader w, BlockPos start, BlockPos end, int leaveGen, Random r, boolean leaves, MutableBoundingBox box, BaseTreeFeatureConfig config)
	{
		
		int sx = end.getX() - start.getX();
		int sy = end.getY() - start.getY();
		int sz = end.getZ() - start.getZ();
		
		int distance = (int) Math.sqrt(sx*sx + sz*sz) + 1;

		float a = (float)-sy / (float)(distance*distance);
		float x_quod = sx / (float)distance;
		float z_quod = sz / (float)distance;
		BlockPos last = start;
		for(int i=0;i<=distance;i++)
		{
			int y = Math.round( a * (i-distance)*(i-distance) + sy);
			int x = Math.round(x_quod * i);
			int z = Math.round(z_quod * i);
			
			BlockPos xyz = start.offset(x,y,z);
			connectBough(changedLogs, w, last, xyz, leaveGen, r, box, config);
			if(leaves)
				genLeaves(w, xyz, leaveGen, box, config, r, changedLeaves);
			last = xyz;
		}
	}
	
	/**
	 * generates the Tree stem
	 * @return the high of the Tree
	 */
	protected void genTreeStemm(Set<BlockPos> changedLog, IWorldGenerationReader worldIn, BlockPos start, int h, MutableBoundingBox box, BaseTreeFeatureConfig config, Random rand)
	{
		BlockPos.Mutable xyz = new BlockPos.Mutable();
		for(int i=0;i<h;i++)
		{
			double r = getRadius(i, h);
			for(int x=(int) -r;x<r;x++)
			{
				for(int z=(int) -r;z<r;z++)
				{
					if(x*x + z*z < r*r)
					{
						xyz.set(start).move(x,i,z);
//						setLogState(changedBlocks, worldIn, xyz, LOG, box);
						setLog(worldIn, rand, xyz, changedLog, box, config.trunkProvider);
					}
				}
			}
		}
	}
	
	protected abstract double getRadius(int i, int h);
	
	protected void setLog(IWorldGenerationReader w, Random rand, Mutable start, Set<BlockPos> changedLogs, MutableBoundingBox box, BlockStateProvider wrappedConfig)
	{
		w.setBlock(start, wrappedConfig.getState(rand, start), 3);
		if(changedLogs!=null)
			changedLogs.add(start.immutable());
	}

	protected void setLeaf(IWorldGenerationReader w, Random rand, Mutable xyz, Set<BlockPos> changedLeaves, MutableBoundingBox box, BlockStateProvider config)
	{
		w.setBlock(xyz, config.getState(rand, xyz), 3);
		if(changedLeaves!=null)
			changedLeaves.add(xyz.immutable());
	}

}
