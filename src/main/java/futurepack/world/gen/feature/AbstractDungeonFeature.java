package futurepack.world.gen.feature;

import java.util.Random;

import futurepack.common.dim.structures.StructureBase;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.server.ServerWorld;

public class AbstractDungeonFeature extends Feature<DungeonFeatureConfig>
{
	public AbstractDungeonFeature() 
	{
		super(DungeonFeatureConfig.CODEC);
	}

	@Override
	public boolean place(ISeedReader w, ChunkGenerator generator, Random rand, BlockPos pos, DungeonFeatureConfig config) 
	{
		pos = new BlockPos(pos.getX() & 0xFFFFFFF0, pos.getY(), pos.getZ() & 0xFFFFFFF0); //chunk  start
		
		int i = rand.nextInt(config.structures.length);
		StructureBase b = config.structures[i].base;
		
		b.generateBase(w, pos);
		if(w.getLevel()!=null && w.getLevel() instanceof ServerWorld)
		{
			CompoundNBT nbt = new CompoundNBT();
			nbt.putBoolean("worlgen", true);
			b.addChestContentBase(w, pos, rand, nbt, (w.getLevel().getServer().getLootTables()));
		}
		return true;
	}

}
