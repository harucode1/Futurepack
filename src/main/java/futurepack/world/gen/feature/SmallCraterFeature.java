package futurepack.world.gen.feature;

import java.util.Random;

import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class SmallCraterFeature extends Feature<NoFeatureConfig> 
{
	//this will be the holes

	public SmallCraterFeature()
	{
		super(NoFeatureConfig.CODEC);
	}

	@Override
	public boolean place(ISeedReader w, ChunkGenerator p_241855_2_, Random rand, BlockPos pos, NoFeatureConfig p_241855_5_) 
	{
		BlockPos blockpos = w.getHeightmapPos(Type.WORLD_SURFACE_WG, pos.offset(rand.nextInt(8) - rand.nextInt(8), -1, rand.nextInt(8) - rand.nextInt(8)));
        int r = 5 + rand.nextInt(5);
		
        boolean water = false;
        
        for(int k=r-1;k>=-r;k--)
		{
        	for(int j=-r;j<+r;j++)
        	{
				for(int l=-r;l<+r;l++)
				{
					if((j*j + k*k + l*l) <= r*r+(rand.nextInt(3))-1)
					{
						BlockPos p = blockpos.offset(j,k,l);
						if(water)
							w.setBlock(p, Blocks.WATER.defaultBlockState(), 3);
						else
						{
							water = w.isWaterAt(p);
							w.removeBlock(p, false);
						}
							
					}
				}
			}
		}
		
		return true;
	}
}
