package futurepack.world.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.world.gen.feature.IFeatureConfig;

public class BigMushroomFeatureConfig implements IFeatureConfig 
{
	public static final Codec<BigMushroomFeatureConfig> CODEC = RecordCodecBuilder.create((builder) -> {
	      return builder.group(BlockState.CODEC.fieldOf("stem").forGetter((c) -> {
	         return c.stem;
	      }), BlockState.CODEC.fieldOf("cap").forGetter((c) -> {
	         return c.cap;
	      }), Codec.INT.fieldOf("min_height").forGetter((c) -> {
	         return c.min_height;
	      }), Codec.INT.fieldOf("max_height").forGetter((c) -> {
		         return c.max_height;
		  })).apply(builder, BigMushroomFeatureConfig::new);
	   });
	
	public final BlockState stem, cap;
	public final int min_height, max_height;
	
	public BigMushroomFeatureConfig(BlockState stem, BlockState cap, int min_height, int max_height) 
	{
		super();
		this.stem = stem;
		this.cap = cap;
		this.min_height = min_height;
		this.max_height = max_height;
	}
	
	public int getHeight(Random r)
	{
		return min_height + r.nextInt(max_height-min_height);
	}
}
