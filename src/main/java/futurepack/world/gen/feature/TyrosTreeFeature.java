package futurepack.world.gen.feature;

import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.IWorldGenerationReader;
import net.minecraft.world.gen.feature.BaseTreeFeatureConfig;

public class TyrosTreeFeature extends AbstractTyrosTreeFeature<BaseTreeFeatureConfig>
{
//	int leaveGen;
	
	public static final TyrosTreeFeature LARGE_TYROS_TREE = new TyrosTreeFeature(BaseTreeFeatureConfig.CODEC);

	public TyrosTreeFeature(Codec<BaseTreeFeatureConfig> codec)
	{
		super(codec);
	}

	public boolean generate(Set<BlockPos> changedLogs, Set<BlockPos> changedLeaves, IWorldGenerationReader world, Random rand, BlockPos pos, MutableBoundingBox box, BaseTreeFeatureConfig config) 
	{
		if(!canStand(world, pos, changedLogs, changedLeaves, rand, box, config))
			return false;
        pos = pos.offset(2, 0, 2);
		
		int height = 20  + rand.nextInt(15);//fixed
		double d = rand.nextDouble() * Math.PI;//
		int parts = 20 + rand.nextInt(10);//fixed
		int leaveGen = 2 + rand.nextInt(2);//fixed

		genTreeStemm(changedLogs, world, pos, height, box, config, rand);
		pos = pos.offset(0, height, 0);	
		
		genTreetop(changedLogs, changedLeaves, world, rand, pos, d, parts, leaveGen, box, config);
		
		return true;
	}

	private void genTreetop(Set<BlockPos> changedLogs, Set<BlockPos> changedLeaves, IWorldGenerationReader world, Random rand, BlockPos pos, double d, int parts, int leaveGen, MutableBoundingBox box, BaseTreeFeatureConfig config) 
	{
		for(int i=0 ; i<parts ; i++)
		{
			double y = (i + 1.0) / parts;    
			double r = Math.sqrt(1.0 - y*y);
			double phi = i * Math.PI * (3 - Math.sqrt(5.0));
		            
		    double x = Math.cos(phi+d) * r;
		    double z = Math.sin(phi+d) * r;
		    
		    int radius = parts +3;
		    
		    Vector3d vec = new Vector3d(x*radius, y*radius, z*radius);
		    genBough(changedLogs, changedLeaves, world, pos, pos.offset( new BlockPos( vec ) ) , leaveGen, rand, true, box, config);
		}
	}
	
	private boolean canStand(IWorldGenerationReader w, BlockPos pos, Set<BlockPos> changedLogs, Set<BlockPos> changedLeaves, Random r, MutableBoundingBox box, BaseTreeFeatureConfig config)
	{
		if(isGround(w, pos.below()))
		{
			if(isGround(w, pos.offset(2,-1,2)) && isGround(w, pos.offset(4,-1,4)))
			{
				if(isGround(w, pos.offset(4,-1,0)) && isGround(w, pos.offset(0,-1,4)))
				{
					return true;
				}
			}
		}
		else if(r.nextInt(5)==0)
		{
			BlockPos deep = pos.below();
			Predicate<BlockState> isWater = s -> s.getMaterial() == Material.WATER;
			if(w.isStateAtPosition(deep, isWater))
			{
				
				while(w.isStateAtPosition(deep, isWater))
				{
					deep = deep.below();
				}
				int absoulute = pos.getY() - deep.getY();
				int arms = 4 + absoulute/2;
				
				for(int i=0;i< arms;i++)
				{
					BlockPos start = pos.offset(r.nextInt(6)-3, 0, r.nextInt(6)-3);
					BlockPos end = deep.offset(r.nextInt(absoulute +5)-r.nextInt(absoulute +5), 0, r.nextInt(absoulute +5)-r.nextInt(absoulute +5));
					genBough(changedLogs, changedLeaves, w, start, end, 0, r, false, box, config);
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected double getRadius(int i, int h)
	{
		return (1.0 - i/(double)h)*1.5 +1.5;
	}

	private boolean isGround(IWorldGenerationReader w, BlockPos pos)
	{
		return w.isStateAtPosition(pos, s -> s.getMaterial() == Material.DIRT || s.getMaterial() == Material.GRASS); //organic becuase grass
	}

	@Override
	public boolean place(ISeedReader reader, ChunkGenerator generator, Random rand, BlockPos pos, BaseTreeFeatureConfig config) 
	{
		return generate(null, null, reader, rand, pos, MutableBoundingBox.getUnknownBox(), config);
	}


}
