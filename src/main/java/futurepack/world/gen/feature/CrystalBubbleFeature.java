package futurepack.world.gen.feature;

import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;

public class CrystalBubbleFeature extends Feature<CrystalBubbleConfig> 
{

	public CrystalBubbleFeature() 
	{
		super(CrystalBubbleConfig.CODEC);
	}

	public static boolean genCristalHole(IWorld w, BlockPos pos, Random r, BlockState fillerblock, BlockState crystalblock)
	{
		if(w.isEmptyBlock(pos) || !w.getBlockState(pos).getFluidState().isEmpty())
		{
			return false;
		}
			
		float width = (0.5F+r.nextFloat());
		float high = 1;//(0.5F+r.nextFloat());
		float depth = (0.5F+r.nextFloat());
		
		int radius = 5 + r.nextInt(5);
		float cristal = 2+r.nextInt(4);
		
		if(w.isEmptyBlock(pos.above((int)(high*radius))) || !w.getBlockState(pos.above((int)(high*radius))).getFluidState().isEmpty())
		{
			return false;
		}		
		
		for(int x=(int) (-radius*width);x<radius*width;x++)
		{
			for(int y=(int) (-radius*high);y<radius*high;y++)
			{
				for(int z=(int) (-radius*depth);z<radius*depth;z++)
				{
					if( (x/width)*(x/width) +  (y/high)*(y/high) +  (z/depth)*(z/depth) <= radius*radius + r.nextInt(5))
					{
						BlockPos xyz = pos.offset(x,y,z);
						BlockState state;
						float h = cristal/(radius*2-2);
						if( x*x+z*z < cristal - (y+radius) *h) 
						{
							state = crystalblock;
						}
						else if(y< radius *-1/3)
						{
							state = fillerblock;
						}
						else
						{
							state = Blocks.AIR.defaultBlockState();
						}
						
						w.setBlock(xyz, state, 2);
					}					
				}
			}
		}
		
		return true;
	}

	@Override
	public boolean place(ISeedReader w, ChunkGenerator generator, Random rand, BlockPos pos, CrystalBubbleConfig config) 
	{
		return genCristalHole(w, pos, rand, config.fillerblock, config.crystalblock);
	}
}
