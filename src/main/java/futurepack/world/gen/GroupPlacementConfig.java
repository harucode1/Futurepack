package futurepack.world.gen;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.world.gen.placement.IPlacementConfig;

public class GroupPlacementConfig implements IPlacementConfig
{
	public final float areaProbability, placeProbability;
	public final int areaSize;
	
	public static final Codec<GroupPlacementConfig> CODEC = RecordCodecBuilder.create((builder) -> {
	      return builder.group(
	    		  Codec.FLOAT.fieldOf("p_area").forGetter(c -> c.areaProbability),
	    		  Codec.INT.fieldOf("area_size").forGetter(c -> c.areaSize),
	    		  Codec.FLOAT.fieldOf("p_place").forGetter(c -> c.placeProbability)
	    		  ).apply(builder, GroupPlacementConfig::new);
		   });
		
	public GroupPlacementConfig(float areaProbability, int areaSize, float placeProbability) 
	{
		super();
		this.areaProbability = areaProbability;
		this.areaSize = areaSize;
		this.placeProbability = placeProbability;
	}
}
