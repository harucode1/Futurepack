package futurepack.world.gen;

import java.util.ArrayList;
import java.util.function.Consumer;

import futurepack.api.Constants;
import futurepack.common.FPConfig;
import futurepack.common.FuturepackTags;
import futurepack.common.WorldGenRegistry;
import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.FeatureSpreadConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.template.RuleTest;
import net.minecraft.world.gen.feature.template.TagMatchRuleTest;
import net.minecraft.world.gen.placement.NoPlacementConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.event.world.BiomeLoadingEvent;

public class WorldGenOres
{
	public static final int SIZE_DIAMOND = 8;
	public static final int SIZE_IRON = 9;
	
	public static final int SIZE_DAS_IST_JA_NUR_EXTRA = 6; //Zitaat: WUgand, 21:23Uhr 23.01.2019. Man muss ja f�r Zitate die genau urhzeit nenenenenenenenenenenenenenen. 
	public static final int SIZE_QUARTZ_KLUMPEN = 12; //ZItat WUgand, zeit sihe oben
	
	public static final RuleTest IS_ROCK_MENELAUS = new TagMatchRuleTest(FuturepackTags.stone_menelaus);
	
	public static ConfiguredFeature<?, ?> tin, copper, zinc, bauxite, magnetite, copper_m, quartz_m, coal_m;
	
	public static void register()
	{
		Registry<ConfiguredFeature<?, ?>> registry = WorldGenRegistries.CONFIGURED_FEATURE;
		
		tin = addOre(TerrainBlocks.ore_tin.defaultBlockState(), SIZE_IRON, FPConfig.WORLDGEN_ORES.tinOre.get(), 0, 64);//
		copper = addOre(TerrainBlocks.ore_copper.defaultBlockState(), SIZE_IRON, FPConfig.WORLDGEN_ORES.copperOre.get(), 0, 64);//
		zinc = addOre(TerrainBlocks.ore_zinc.defaultBlockState(), SIZE_IRON, FPConfig.WORLDGEN_ORES.zincOre.get(), 0, 64);//
	
		bauxite = addOre(TerrainBlocks.ore_bauxite.defaultBlockState(), SIZE_DIAMOND, FPConfig.WORLDGEN_ORES.bauxiteOre.get(), 0, 64);
		magnetite = addOre(TerrainBlocks.ore_magnetite.defaultBlockState(), SIZE_DIAMOND, FPConfig.WORLDGEN_ORES.magnetiteOre.get(), 0, 64);
		
		copper_m = addOreM(TerrainBlocks.ore_copper_m.defaultBlockState(), SIZE_DAS_IST_JA_NUR_EXTRA, FPConfig.WORLDGEN_ORES.copperOreM.get(), 50, 50+64);//
		quartz_m = addOreM(TerrainBlocks.ore_quartz_m.defaultBlockState(), SIZE_QUARTZ_KLUMPEN, FPConfig.WORLDGEN_ORES.quartzOreM.get(), 50, 50+64);//in tags
		coal_m = addOreM(TerrainBlocks.ore_coal_m.defaultBlockState(), SIZE_DIAMOND, FPConfig.WORLDGEN_ORES.coalOreM.get(), 50, 50+64);//
		
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "ore_tin"), tin);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "ore_copper"), copper);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "ore_zinc"), zinc);
		
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "ore_magnetite"), magnetite);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "ore_bauxite"), bauxite);
		
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "ore_copper_m"), copper_m);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "ore_quartz_m"), quartz_m);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "ore_coal_m"), coal_m);
	}
	
	public static void init(ArrayList<Consumer<BiomeLoadingEvent>> list)
	{
		list.add(WorldGenOres::registerOres);
	}
	
	public static void registerOres(BiomeLoadingEvent b)
	{
		WorldGenRegistry.addFeature(b, GenerationStage.Decoration.UNDERGROUND_ORES, () -> tin);
		WorldGenRegistry.addFeature(b, GenerationStage.Decoration.UNDERGROUND_ORES, () -> copper);
		WorldGenRegistry.addFeature(b, GenerationStage.Decoration.UNDERGROUND_ORES, () -> zinc);
		
		WorldGenRegistry.addFeature(b, GenerationStage.Decoration.UNDERGROUND_ORES, () -> bauxite);
		WorldGenRegistry.addFeature(b, GenerationStage.Decoration.UNDERGROUND_ORES, () -> magnetite);
		
		WorldGenRegistry.addFeature(b, GenerationStage.Decoration.UNDERGROUND_ORES, ()-> copper_m);
		WorldGenRegistry.addFeature(b, GenerationStage.Decoration.UNDERGROUND_ORES, ()-> quartz_m);
		WorldGenRegistry.addFeature(b, GenerationStage.Decoration.UNDERGROUND_ORES, ()-> coal_m);
	}

	
	private static ConfiguredFeature<?, ?> addOre(BlockState state, int cluster_size, int count_per_chunk, int min_height, int max_height)
	{
			return Feature.ORE.configured(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, state, cluster_size))
					.decorated(Placement.RANGE.configured(new TopSolidRangeConfig(min_height, 0, max_height)))
					.decorated(Placement.SQUARE.configured(NoPlacementConfig.INSTANCE))
					.decorated(Placement.COUNT.configured(new FeatureSpreadConfig(count_per_chunk)));
		
	}
	
	private static ConfiguredFeature<?, ?> addOreM(BlockState state, int cluster_size, int count_per_chunk, int min_height, int max_height)
	{
			return Feature.ORE.configured(new OreFeatureConfig(IS_ROCK_MENELAUS, state, cluster_size))
					.decorated(Placement.RANGE.configured(new TopSolidRangeConfig(min_height, 0, max_height)))
					.decorated(Placement.SQUARE.configured(NoPlacementConfig.INSTANCE))
					.decorated(Placement.COUNT.configured(new FeatureSpreadConfig(count_per_chunk)));
		
	}
}
