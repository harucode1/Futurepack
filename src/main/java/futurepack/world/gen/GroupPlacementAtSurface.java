package futurepack.world.gen;

import java.util.Random;
import java.util.stream.Stream;

import com.mojang.serialization.Codec;

import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.feature.WorldDecoratingHelper;
import net.minecraft.world.gen.placement.Placement;

public class GroupPlacementAtSurface extends Placement<GroupPlacementConfig>
{

	public GroupPlacementAtSurface(Codec<GroupPlacementConfig> configFactoryIn) 
	{
		super(configFactoryIn);
	}

	@Override
	public Stream<BlockPos> getPositions(WorldDecoratingHelper helper, Random random, GroupPlacementConfig placementConfig, BlockPos pos) 
	{
		int x = pos.getX() / placementConfig.areaSize;
		int z = pos.getZ() / placementConfig.areaSize;
        Random r = SharedSeedRandom.seedSlimeChunk(x, z, helper.level.getSeed(), 324657867L);
        
        if(r.nextFloat() < placementConfig.areaProbability)
        {
        	if(random.nextFloat() < placementConfig.placeProbability)
        	{
	        	int k = random.nextInt(16);
	            int l = random.nextInt(16);
	            return Stream.of(helper.level.getHeightmapPos(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, pos.offset(k, 0, l)));
        	}
        }
        
		return Stream.empty();
	}

}
