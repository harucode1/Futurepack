package futurepack.world.scanning;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemAlreadyExistsException;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import futurepack.common.FPLog;
import futurepack.depend.api.helper.HelperChunks;
import it.unimi.dsi.fastutil.longs.LongRBTreeSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunk;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FPChunkScanner implements Runnable
{
	private static final Object LOCK = new Object();
	
	public static final FPChunkScanner INSTANCE = new FPChunkScanner();
	public static final int WEIGHT = 100;
	
	private static Thread t = null;
	
	private final ArrayList<ChunkData> buffer = new ArrayList<ChunkData>();
	private final Map<String, FileSystem> openJars = new HashMap<String, FileSystem>();
	
	private FPChunkScanner()
	{
		if(INSTANCE!=null)
			throw new IllegalStateException("There should be only 1 instance of this class!");
	}	
	
	private boolean start()
	{
		if(t==null)
		{
			t = new Thread(this, "FP-ChunkScanner");
			t.setDaemon(true);
			t.start();
			return true;
		}	
		return false;
	}

	public ChunkData getData(World w, BlockPos pos)
	{
		ChunkData data = new ChunkData(w, pos, new HashMap<String, Integer>());
		FileSystem fs = null;
		
		try
		{
			fs = retriveFileSystem(data.file());
				
			synchronized (LOCK)
			{
				if(!fs.isOpen())
				{	
					FPLog.logger.error("File " + data.file() +" is no longer accesible from the JVM, a restart is needed to fix this problem.");
				}
				else
				{
					loadChunkData(fs, data);
				}		
			}	
		}
		catch (IOException e)
		{
			e.printStackTrace();			
		}
		
		return data;
	}
	
	private void loadChunkData(FileSystem fs, ChunkData data) throws IOException
	{
		Path p = fs.getPath(data.getFilename());
		BufferedReader reader = Files.newBufferedReader(p, StandardCharsets.UTF_8);
		
		data.load(reader);
		reader.close();
	}
	
	private void saveChunkData(FileSystem fs, ChunkData data) throws IOException
	{		
		Path p = fs.getPath(data.getFilename());
		if(Files.exists(p, LinkOption.NOFOLLOW_LINKS))
		{
			ChunkPos cp = new ChunkPos(data.getCoords());
			addScannedChunk(cp, data.w);
			return;
		}
		
		BufferedWriter writer = Files.newBufferedWriter(p, StandardCharsets.UTF_8, StandardOpenOption.CREATE); 	
			
		data.save(writer);		
		writer.close();
		
		ChunkPos cp = new ChunkPos(data.getCoords());
		addScannedChunk(cp, data.w);
	}
	
	@Override
	public void run()
	{
		try
		{
			int idle = 0;
			while(idle < 10)
			{
				try
				{
					int now = buffer.size();
					
					progressAll();
					saveAllMaps();
					
					if(now != buffer.size())
						idle = 0;
					else
						idle++;
				}
				catch(NoSuchElementException e)
				{
					if(buffer.size() > 0)
					{
//						ArrayList<ChunkData> old = buffer;
//						buffer = new ArrayList<ChunkData>();
//						FPLog.logger.error("ChunkScanner: Lost %s chunks due to broken LinkedList", old.size());
						//tryCopyOldBuffer(old);
					}
					
				}
				Thread.sleep(100);			
			}
			progressAll();	
			saveAllMaps();
			closeAllOpenJars();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	
	private FileSystem retriveFileSystem(URI path) throws IOException
	{
		synchronized (LOCK)
		{
			Map<String,String> map = new HashMap<String, String>();
			map.put("create", "true");
			
			FileSystem fs = null;
			
			try
			{
				fs = FileSystems.getFileSystem(path);
				if(!openJars.containsKey(path.toString()))
				{
					openJars.put(path.toString(), fs);
				}
			}
			catch(FileSystemNotFoundException e)
			{
				String s = path.toString();
				
				for(Entry<String, FileSystem> ent : openJars.entrySet())
				{
					if(s.equals(ent.getKey()))
					{
						fs = ent.getValue();
						break;
					}
				}
				
				if(fs==null)
				{
					fs = FileSystems.newFileSystem(path, map);	
					openJars.put(path.toString(), fs);
				}
			}	
			return fs;
		}
	}
	
	private void closeAllOpenJars()
	{
		synchronized (LOCK)
		{
			Iterator<Entry<String, FileSystem>> iter = openJars.entrySet().iterator();
			while(iter.hasNext())
			{
				try
				{
					Entry<String, FileSystem> ent = iter.next();
					synchronized (ent.getValue())
					{
						ent.getValue().close();
					}
					iter.remove();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	private void progressAll()
	{
		if(buffer.size() > 100)
		{	
			System.out.println("Buffered: " + buffer.size());
		}
		
		int i = buffer.size() -1;
		while(i >= 0)
		{		
			ChunkData data;
			synchronized (buffer)
			{
				data = buffer.get(i);
			}
			Map<String,String> map = new HashMap<String, String>();
			map.put("create", "true");
			FileSystem fs = null;
			if(data!=null)
			{
				try
				{
					data.getMap(); //generates the map if not already done
					
					fs = retriveFileSystem(data.file());
					synchronized (LOCK)
					{
						if(!fs.isOpen())
						{	
							FPLog.logger.error("File " + data.file() +" is no longer accesible from the JVM, a restart is needed to fix this problem.");
						}
						else
						{
							saveChunkData(fs, data);
						}			
					}	
					
				}
				catch (FileAlreadyExistsException e)
				{
					System.out.println(e);
					i--;
					continue; //wait
				}
				catch (FileSystemAlreadyExistsException e)
				{
					
					i--;
					continue; //wait
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			
			synchronized (buffer)
			{
				buffer.remove(i); //only remove if everything worked
				i--;
			}
			
		}
	}
	
	public void scanChunk(IChunk c)
	{		
		if(isChunkScanned(c))
			return;
		
		int x0 = c.getPos().getMinBlockX();
		int z0 = c.getPos().getMinBlockZ();
		BlockState[][][] chunk = new BlockState[16][256][16]; //copy chunk data and analyze it off-thread
		BlockPos.Mutable mut = new BlockPos.Mutable();
		for(int y=0;y<256;y++)
		{
			for(int x=0;x<16;x++)
			{
				for(int z=0;z<16;z++)
				{
					mut.set(x+x0, y, z+z0);
					chunk[x][y][z] = c.getBlockState(mut);
				}
			}
		}
		synchronized (buffer)
		{
			buffer.add(new ChunkData(c.getWorldForge(), c.getPos().getWorldPosition(), chunk));
			if(buffer.size() > 100)
			{
				if(t==null || !t.isAlive())
				{
					t = null;
					start();
				}
			}
		}
	}
	
	@SubscribeEvent
	public void onWorldUnload(WorldEvent.Unload event)
	{
		IWorld world = event.getWorld();
		
		if(world.isClientSide())
			return;
		
		File dir = HelperChunks.getDimensionDir(world);
		dir = new File(dir, "ores");
		dir.mkdirs();
		
		if(buffer.size()>0 && t!=null)
		{
			int buffered;
			do
			{
				buffered = buffer.size();
				try
				{
					t.join(200);
				}
				catch (InterruptedException e) 
				{
					
					e.printStackTrace();
				}
			}
			while(t!=null && t.isAlive() && buffered>buffer.size() && buffer.size()>0); //wait while the thread is still working
		}
		if(buffer.size()>0)
		{			
			File out = new File(dir, "ores_" + System.currentTimeMillis()+".tmp");
			try
			{
				DataOutputStream stream = new DataOutputStream(new FileOutputStream(out));
				buffer.stream().filter(b -> b.w == world).map(FPChunkScanner::saveRaw).forEach(b -> {
					try
					{
						stream.writeInt(b.length);
						stream.write(b);
					}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				});
				stream.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event)
	{
		IWorld world = event.getWorld();
		
		if(world.isClientSide())
			return;
		
		File dir = HelperChunks.getDimensionDir(world);
		dir = new File(dir, "ores");
		dir.mkdirs();
		
		if(dir.exists())
		{
			for(File f : dir.listFiles())
			{
				String s = f.getName();
				if(s.startsWith("ores_") && s.endsWith(".tmp"))
				{
					ArrayList<ChunkData> list = new ArrayList<ChunkData>(200);
					try
					{
						DataInputStream stream = new DataInputStream(new FileInputStream(f));
						while(stream.available() > 0)
						{
							int l = stream.readInt();
							byte[] b = new byte[l];
							stream.read(b);
							ChunkData d = loadRaw(world, b);
							list.add(d);
						}
						stream.close();
						f.delete();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
					synchronized (buffer)
					{
						buffer.addAll(list);
					}
				}
			}
		}
	}
	
	private static byte[] saveRaw(ChunkData data)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try
		{
			DataOutputStream dout = new DataOutputStream(out);
			dout.writeInt(data.getCoords().getX());
			dout.writeInt(data.getCoords().getY());
			dout.writeInt(data.getCoords().getZ());
			BufferedWriter write = new BufferedWriter(new OutputStreamWriter(dout, StandardCharsets.UTF_8));
			data.save(write);
			write.close();
			dout.close();
			out.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return out.toByteArray();
	}
	
	private static ChunkData loadRaw(IWorld w, byte[] data)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		try
		{
			DataInputStream din =  new DataInputStream(in);
			BlockPos pos = new BlockPos(din.readInt(), din.readInt(), din.readInt());
			ChunkData chunkd = new ChunkData(w, pos, new HashMap<>());
			BufferedReader read = new BufferedReader(new InputStreamReader(din, StandardCharsets.UTF_8));
			chunkd.load(read);
			read.close();
			din.close();
			in.close();
			return chunkd;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void join()
	{
		if(t!=null)
		{
			try {
				t.join();
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private ReadWriteLock rwlock =  new ReentrantReadWriteLock();
	
	private boolean isChunkScanned(IChunk pos)
	{
		
		
		boolean b;
		rwlock.readLock().lock();
		b =  getOrLoadScannedChunks(pos.getWorldForge()).contains(pos.getPos().toLong());
		rwlock.readLock().unlock();
		return b;
	}

	private void addScannedChunk(ChunkPos pos, IWorld w)
	{
		rwlock.writeLock().lock();
		getOrLoadScannedChunks(w).add(pos.toLong());
		dirtySets.put(w, getOrLoadScannedChunks(w));
		
		rwlock.writeLock().unlock();
	}
	
	private void saveAllMaps()
	{
		rwlock.readLock().lock();
		
		dirtySets.forEach((world,s) -> 
		{
			File dir = HelperChunks.getDimensionDir(world);
			dir = new File(dir, "ores");
			dir.mkdirs();
			File chunkfile = new File(dir, "chunklist.dat");
			ObjectOutputStream out = null;
			try
			{
				out = new ObjectOutputStream(new FileOutputStream(chunkfile));
				out.writeObject(s);
				out.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		});
		dirtySets.clear();
		
		rwlock.readLock().unlock();
	}
	
	private WeakHashMap<IWorld, LongSet> scannedChunks = new WeakHashMap<IWorld, LongSet>();
	private Map<IWorld, LongSet> dirtySets = Collections.synchronizedMap(new HashMap<IWorld, LongSet>());
	
	private LongSet getOrLoadScannedChunks(IWorld w)
	{
		return scannedChunks.computeIfAbsent(w, this::loadScannedChunks);
	}
	
	private LongSet loadScannedChunks(IWorld world)
	{
		LongSet scannedChunkSet = null;
		File dir = HelperChunks.getDimensionDir(world);
		dir = new File(dir, "ores");
		dir.mkdirs();
		File chunkfile = new File(dir, "chunklist.dat");
		if(!chunkfile.exists())
		{
			scannedChunkSet = new LongRBTreeSet();
		}
		else
		{	
			ObjectInputStream in = null;
			try
			{
				in = new ObjectInputStream(new FileInputStream(chunkfile));
				scannedChunkSet = (LongSet) in.readObject();
			}
			catch (ClassNotFoundException e) 
			{
				e.printStackTrace();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
			try 
			{
				in.close();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		return scannedChunkSet;
	}

	
}
