package futurepack.depend.api.helper;

import java.util.Arrays;

import net.minecraft.block.Block;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IWorldReader;

public class HelperBoundingBoxes 
{
	public static AxisAlignedBB rotate(AxisAlignedBB box, float x, float y)
	{
		Vector3d min = new Vector3d(box.minX, box.minY, box.minZ);
		Vector3d max = new Vector3d(box.maxX, box.maxY, box.maxZ);
		min = rotate(min, x, y);
		max = rotate(max, x, y);
		return new AxisAlignedBB(min, max);
	}
	
	/**
	 * 
	 * @param point
	 * @param x rotation around X axis in degress
	 * @param y rotation around Y axis in degress 
	 * @return
	 */
	public static Vector3d rotate(Vector3d point, float x, float y)
	{
		return point.xRot((float) (x/180.0*Math.PI) ).yRot((float) (y/180.0*Math.PI));
	}
	
	public static AxisAlignedBB[] rotate(float x, float y, AxisAlignedBB...box)
	{
		AxisAlignedBB[] out = new AxisAlignedBB[box.length];
		for(int i=0;i<out.length;i++)
		{
			out[i] = rotate(box[i], x, y);
		}
		return out;
	}
	
	public static VoxelShape createShape(AxisAlignedBB...boxes)
	{
		VoxelShape[] out = new VoxelShape[boxes.length];
		for(int i=0;i<out.length;i++)
		{
			out[i] = VoxelShapes.create(boxes[i]);
		}
		return createShape(out);
	}
	
	public static VoxelShape createShape(VoxelShape...boxes)
	{
		if(boxes.length==0)
			return null;
		else if(boxes.length==1)
		{
			return boxes[0];
		}
		else
		{
			return VoxelShapes.or(boxes[0], boxes);
		}
	}
	
	public static AxisAlignedBB[] fromBoxes(double[][] boxes)
	{
		AxisAlignedBB[] put = new AxisAlignedBB[boxes.length];
		for(int i=0;i<put.length;i++)
		{
			if(boxes[i].length!=6)
				throw new IllegalArgumentException("at pos " + i + ": " + Arrays.toString(boxes[i]));
			else
			{
				put[i] = new AxisAlignedBB(boxes[i][0], boxes[i][1], boxes[i][2], boxes[i][3], boxes[i][4], boxes[i][5]);
			}
		}
		return put;
	}
	
	public static double[][] scale(double[][] boxes, float factor)
	{
		for(int i=0;i<boxes.length;i++)
		{
			for(int j=0;j<boxes[i].length;j++)
			{
				boxes[i][j] *= factor;
			}
				
		}
		return boxes;
	}
	
	public static AxisAlignedBB[] move(AxisAlignedBB[] boxes, double x, double y, double z)
	{
		AxisAlignedBB[] put = new AxisAlignedBB[boxes.length];
		for(int i=0;i<put.length;i++)
		{
			put[i] = boxes[i].move(x, y, z);
		}
		return put;
	}
	
	/**
	 * 
	 * @param boxes
	 * @param x rotation around X axis in degress
	 * @param y rotation around Y axis in degress 
	 * @return
	 */
	public static VoxelShape createBlockShape(double[][] boxes, float x, float y)
	{
		AxisAlignedBB[] bb = fromBoxes(scale(boxes, 1F/16F));
		bb = move(bb, -0.5, -0.5, -0.5);
		bb = rotate(x, y, bb);
		bb = move(bb, 0.5, 0.5, 0.5);
		return createShape(bb);
	}
	
	public static boolean isSideSolid(IWorldReader worldIn, BlockPos pos, Direction directionIn)
	{
		return Block.canSupportCenter(worldIn, pos, directionIn);
	}

	public static BlockPos[][][] splitToBlocks(AxisAlignedBB bb) 
	{
		int w = (int) bb.getXsize();
		int h = (int) bb.getYsize();
		int d = (int) bb.getZsize();
		
		if(w==0 && bb.getXsize() > 0)
		{
			w=1;
		}
		if(h==0 && bb.getYsize() > 0)
		{
			h=1;
		}
		if(d==0 && bb.getZsize() > 0)
		{
			d=1;
		}
		
		BlockPos[][][] pos = new BlockPos[w][h][d];
		BlockPos zero = new BlockPos(bb.minX, bb.minY, bb.minZ);
		for(int x=0;x<w;x++)
		{
			for(int y=0;y<h;y++)
			{
				for(int z=0;z<d;z++)
				{
					pos[x][y][z] = zero.offset(x,y,z);
				}
			}
		}
		
		return pos;
	}
}
