package futurepack.depend.api.helper;

import java.util.function.Function;

import net.minecraft.entity.Entity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.Path;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.ITeleporter;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class HelperEntities
{
	private static HelperEntities Instance = new HelperEntities();
	
	private boolean itemSpawn = true;
	
	public HelperEntities()
	{
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@SubscribeEvent
	public void onEntitySpawn(EntityJoinWorldEvent event)
	{
		if(!itemSpawn)
		{
			if(event.getEntity() instanceof ItemEntity)
			{
				event.setCanceled(true);
			}
		}
	}
	
	public static void disableItemSpawn()
	{
		Instance.itemSpawn = false;
	}
	
	public static void enableItemSpawn()
	{
		Instance.itemSpawn = true;
	}
	
	public static Entity transportToDimension(Entity e, ServerWorld server)
	{
		return e.changeDimension(server, new ITeleporter()
		{
			@Override
			public Entity placeEntity(Entity porting, ServerWorld currentWorld, ServerWorld destWorld, float yaw, Function<Boolean, Entity> repositionEntity) 
			{
				porting.level.getProfiler().popPush("reloading");
				Entity entity = porting.getType().create(server);
				if (entity != null) 
				{
					entity.restoreFrom(porting);
					entity.moveTo(porting.getX(), porting.getY(), porting.getZ(), porting.yRot, porting.xRot);
					entity.setDeltaMovement(porting.getDeltaMovement());
					server.addFromAnotherDimension(entity);
				}
				return entity;
			}
		});
	}
	
	public static ServerPlayerEntity transportPlayerToDimnsion(ServerPlayerEntity pl, ServerWorld w)
	{
		pl.teleportTo(w, pl.getX(), pl.getY(), pl.getZ(), pl.getViewYRot(0), pl.getViewXRot(0));
		return pl;
	}
	
	public static boolean navigateEntity(MobEntity c, BlockPos target, boolean ignorePath)
	{
		if(ignorePath || c.getNavigation().isDone())
		{
			World world = c.getCommandSenderWorld();
			Path p = c.getNavigation().createPath(target, 1);
			if(p!=null)
			{
				c.getNavigation().moveTo(p, 1.0F);
				return true;
			}
			else
			{
				int range = 10;
				int posX = (int) (c.getX() + world.random.nextInt(range) - world.random.nextInt(range));
				int posZ = (int) (c.getZ() + world.random.nextInt(range) - world.random.nextInt(range));
				int posY = (int) c.getY();
				
				while(world.isEmptyBlock(new BlockPos(posX,posY,posZ)))
				{
					posY--;
					if(posY<=0)
						return false;
				}
					
				while(!world.isEmptyBlock(new BlockPos(posX,posY,posZ)) && posY < 256)
				{
					posY++;
					if(posY >= 256)
						return false;
				}
					
				
				double disSqC = target.distSqr(c.position(), true);
				double disSq = (target.getX()-posX)*(target.getX()-posX) + (target.getY()-posY)*(target.getY()-posY) +(target.getZ()-posZ)*(target.getZ()-posZ);
				if(disSq < disSqC || world.random.nextInt(10)==0)
				{
					p = c.getNavigation().createPath(new BlockPos(posX, posY, posZ), 1); //20 is the max away distance
					if(p!=null)
					{
						c.getNavigation().moveTo(p, 0.7F);
						((ServerWorld)world).addParticle(ParticleTypes.FIREWORK, true, c.getX(), c.getY(), c.getZ(), 0, 0.1, 0);
						return true;
					}
				}
			}
		}
		return false;
	}
}
