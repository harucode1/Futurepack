package futurepack.depend.api.helper;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import futurepack.api.Constants;
import futurepack.client.research.LocalPlayerResearchHelper;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.escanner.GuiResearchMainOverviewBase;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchLoader;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class HelperComponent
{
	private static ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/symbole.png");
	
	/**
	 * renders a 18 x 18 item slot
	 * @param matrixStack TODO
	 * @param blitOffset TODO
	 */
	public static void renderSlot(MatrixStack matrixStack, int x, int y, int blitOffset)
	{
		renderSymbol(matrixStack, x, y, blitOffset, 0);
	}
	
	static Method m_toolTip;
	
	static
	{
		for(Method m: Screen.class.getDeclaredMethods())
		{
			Class<?>[] cs = m.getParameterTypes();
			if(cs.length==4)
			{
				if(cs[0] == MatrixStack.class && cs[1] == ItemStack.class && cs[2]==int.class && cs[3]==int.class)
				{
					m_toolTip = m;
					m_toolTip.setAccessible(true);
					break;
				}
			}
		}
	}
	
	public static void renderItemStackWithSlot(MatrixStack matrixStack, ItemStack it, int x, int y, int blitOffset)
	{
		RenderSystem.enableDepthTest();
		RenderHelper.turnOff();
		RenderSystem.color4f(1, 1, 1, 1);
		renderSlot(matrixStack, x, y, blitOffset);
		
		if(it!=null)
		{
			CustomPlayerData data = LocalPlayerResearchHelper.getLocalPlayerData();
			if(!it.getItem().getRegistryName().getNamespace().equals("minecraft") && !data.canProduce(it))
			{
				renderQuestionmark(matrixStack, x, y, blitOffset);
			}
			else
			{	
				RenderHelper.turnBackOn();
				Minecraft.getInstance().getItemRenderer().renderAndDecorateItem(it, x+1, y+1);
				GlStateManager._enableAlphaTest();//this was enableAlpha
					
				if(it.getCount()!=1)
				{		
					int xPosition = x+1;
					int yPosition = y+1;
					FontRenderer fr = Minecraft.getInstance().font;
					String s1 = Integer.toString(it.getCount());
					GL11.glTranslatef(0f, 0f, 160f);
					fr.drawShadow(matrixStack, s1, xPosition + 19 - 2 - fr.width(s1), yPosition + 6 + 3, 16777215);
					GL11.glTranslatef(0f, 0f, -160f);
				}
			}
		}
//		GL11.glColor4f(1, 1, 1, 1);
	}
	
	public static void renderItemStackNormal(MatrixStack matrixStack, ItemStack it, int x, int y, int blitOffset)
	{
		renderItemStackNormal(matrixStack, it, x, y, blitOffset, true);
	}
	
	public static void renderItemStackNormal(MatrixStack matrixStack, ItemStack it, int x,  int y, int blitOffset, boolean slot)
	{
		GlStateManager._disableLighting();
		GL11.glColor4f(1, 1, 1, 1);
		if(slot)
			renderSlot(matrixStack, x, y, blitOffset);
		
		if(it!=null)
		{
			Minecraft.getInstance().getItemRenderer().renderAndDecorateItem(it, x+1, y+1);
			GlStateManager._enableAlphaTest();
				
			if(it.getCount()!=1)
			{		
				int xPosition = x+1;
				int yPosition = y+1;
				FontRenderer fr = Minecraft.getInstance().font;
				String s1 = Integer.toString(it.getCount());
				GL11.glTranslatef(0f, 0f, 160f);
				fr.drawShadow(matrixStack, s1, xPosition + 19 - 2 - fr.width(s1), yPosition + 6 + 3, 16777215);
				GL11.glTranslatef(0f, 0f, -160f);
			}
		}
	}
	
	public static void renderItemText(MatrixStack matrixStack, ItemStack it, int x, int y, int mouseX, int mouseY, Screen gui)
	{
		if(it != null)
		{		
			GL11.glColor4f(1, 1, 1, 1);
			if(isInBox(mouseX, mouseY, x+1, y+1, x+17, y+17))
			{
				try {
					m_toolTip.invoke(gui, matrixStack, it,mouseX,mouseY);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}	
//		GL11.glColor4f(1, 1, 1, 1);
	}
	
	public static boolean isInBox(double mx, double my, double x1, double y1, double x2, double y2)
	{
		return mx>=x1 && my>=y1 && mx<x2 && my<y2;
	}
	
	public static void renderFire(MatrixStack matrixStack, int x, int y, int blitOffset)
	{
		renderSymbol(matrixStack, x, y, blitOffset, 1);	
	}
	
	public static void renderArrow(MatrixStack matrixStack, int x, int y, int blitOffset)
	{
		renderSymbol(matrixStack, x, y, blitOffset, 2);	
	}
	
	public static void renderIndFurn(MatrixStack matrixStack, int x, int y, int blitOffset)
	{
		renderSymbol(matrixStack, x, y, blitOffset, 3);	
	}
	
	public static void renderPartPress(MatrixStack matrixStack, int x, int y, int blitOffset)
	{
		renderSymbol(matrixStack, x, y, blitOffset, 4);
	}
	
	public static void renderSupport(MatrixStack matrixStack, int x, int y, int blitOffset)
	{
		renderSymbol(matrixStack, x, y, blitOffset, 6);
	}
	
	public static void renderQuestionmark(MatrixStack matrixStack, int x, int y, int blitOffset)
	{
		renderSymbol(matrixStack, x, y, blitOffset, 16);
	}
	
	public static void renderIndNeonFurn(MatrixStack matrixStack, int x, int y, int blitOffset)
	{
		renderSymbol(matrixStack, x, y, blitOffset, 17);
	}
	
	public static void renderSymbol(MatrixStack matrixStack, int x, int y, int blitOffset, int id)
	{
		int texW = 144;
		int texH = 144;
		int symbolW = 18;
		int symbolH = 18;
		
		int line = id % (texW/symbolW);
		int collum = id / (texW/symbolW);
		GL11.glColor4f(1F, 1f, 1f, 1f);
		
		RenderSystem.enableDepthTest();
		
		PartRenderer.drawQuadWithTexture(matrixStack, res, x, y, symbolW * line, symbolH * collum, symbolW, symbolH, symbolW, symbolH, texW, texH, blitOffset);
		
		//Minecraft.getInstance().getTextureManager().bindTexture(res);
		
		//AbstractGui.blit(matrixStack, x, y, symbolW, symbolH, symbolW*line, symbolH*collum,symbolW, symbolH, texW, texH);
	}
	
	public static void drawBackground(MatrixStack matrixStack, int x, int y, IGuiComponent com)
	{
		RenderSystem.color4f(1F, 1f, 1f, 1f);
		RenderSystem.enableDepthTest();
		
		AbstractGui.fill(matrixStack, x, y, x+com.getWidth(), y+com.getHeight(), 0xffc6fffa);
		AbstractGui.fill(matrixStack, x+1, y+1, x+com.getWidth(), y+com.getHeight(), 0xff667994);
		AbstractGui.fill(matrixStack,   x+1,   y+1,   x+com.getWidth()-1,   y+com.getHeight()-1, 0xff99d9ea);
	}
	
	public static <T> T getItemTimeBased(int millis, List<T> col)
	{
		if(col==null)
			return null;
			
		if(col.isEmpty())
			return null;
			
		int c = (int) (System.currentTimeMillis() / millis);
		return col.get( c % col.size() );
	}
	
	public static <T> T getStack(List<T> col)
	{
		return getItemTimeBased(800, col);
	}
	
	public static ItemStack[] getStack(List<ItemStack>[] col)
	{
		ItemStack[] its = new ItemStack[col.length];
		for(int i=0;i<col.length;i++)
		{
			its[i] = getItemTimeBased(800, col[i]);
		}
		return its;
	}

	public static void researchItem(ItemStack it, GuiResearchMainOverviewBase gui)
	{
		if(it!=null)
		{	
			CustomPlayerData data = LocalPlayerResearchHelper.getLocalPlayerData();
			Set<Research> set = ResearchLoader.getReqiredResearch(it);
			if(set == null || set.isEmpty())
			{
				return;
			}
			else
			{
				for(Research r : set)
				{
					if(data.hasResearch(r))
					{
						gui.openResearchText(r);
					}
				}
			}
		}
	}
	
	/**
	 * This fills the list so every one has the same size
	 * @param lits this array will get directly edited and has after this method most likely other list instances
	 * @param minSize will be larger if a single list has more elements
	 * 
	 * @return The size of each list
	 */
	public static int fillListToSameSize(List<ItemStack>[] lists, int minSize)
	{
		for(List<ItemStack> list : lists)
		{
			if(list!=null)
				minSize = Math.max(minSize, list.size());
		}
		
		for(int i=0;i<lists.length;i++)
		{
			if(lists[i]!=null && !lists[i].isEmpty() && lists[i].size() < minSize)
			{
				ArrayList<ItemStack> fixed = new ArrayList(lists[i]);
				int j=0;
				while(fixed.size()<minSize)
				{
					fixed.add(lists[i].get(j++));
					j %= lists[i].size();
				}
				lists[i] = fixed;
			}
		}
		
		return minSize;
	}
	
	public static boolean drawRoundButton(MatrixStack matrixStack, int mouseX, int mouseY, int x, int y, int w, int h)
	{
		 w = Math.max(8, w);
		 h = Math.max(8, h);

		 int col1 = 0xffffffff;
		 int col2 = 0xffb1b1b1;
		 int col3 = 0xff373737;
		 int col4 = 0xff9d9d9d;
		 
		 boolean hover = HelperComponent.isInBox(mouseX, mouseY, x, y, x+w, y+h);
		 if(hover)
		 {
			 col1 &= 0xffffe080;
			 col2 &= 0xffffe080;
			 col3 &= 0xffffe080;
			 col4 &= 0xffffe080;
		 }
		 AbstractGui.fill(matrixStack, x+3, y, x+w-3, y+1, col1);
		 AbstractGui.fill(matrixStack, x, y+3, x+1, y+h-3, col1);
		 AbstractGui.fill(matrixStack, x+2, y+1, x+3, y+2, col1);
		 AbstractGui.fill(matrixStack, x+1, y+2, x+2, y+3, col1);
		 
		 AbstractGui.fill(matrixStack, x+3, y+h-1, x+w-3, y+h, col3);
		 AbstractGui.fill(matrixStack, x+w-1, y+3, x+w, y+h-3, col3);
		 AbstractGui.fill(matrixStack, x+w-3, y+h-2, x+w-2, y+h-1, col3);
		 AbstractGui.fill(matrixStack, x+w-2, y+h-3, x+w-1, y+h-2, col3);
		 
		 AbstractGui.fill(matrixStack, x+1, y+h-3, x+2, y+h-2, col4);
		 AbstractGui.fill(matrixStack, x+2, y+h-2, x+3, y+h-1, col4);
		 AbstractGui.fill(matrixStack, x+w-3, y+1, x+w-2, y+2, col4);
		 AbstractGui.fill(matrixStack, x+w-2, y+2, x+w-1, y+3, col4);
		 
		 AbstractGui.fill(matrixStack, x+2, y+2, x+w-2, y+h-2, col2);
		 
		 AbstractGui.fill(matrixStack, x+3, y+1, x+w-3, y+2, col2);
		 AbstractGui.fill(matrixStack, x+3, y+h-2, x+w-3, y+h-1, col2);
		 AbstractGui.fill(matrixStack, x+1, y+3, x+2, y+h-3, col2);
		 AbstractGui.fill(matrixStack, x+w-2, y+3, x+w-1, y+h-3, col2);
		 
		 return hover;
	}
	
	public static ResourceLocation getUnicodeFont()
	{
		return Constants.unicode_font;
	}
	
	public static ResourceLocation getAutokratischFont()
	{
		return new ResourceLocation("futurepack:autokratisch");
	}
	
	public static String toKryptikMessage(String s)
	{
		Pattern letters = Pattern.compile("[\\w]+");
		Matcher matcher = letters.matcher(s);
		
		StringBuilder builder = new StringBuilder(s.length());
		
		int last = 0;
		
		while(matcher.find())
		{
			int start = matcher.start()+1;
			int end = matcher.end()-1;
			
			if(start < end && end-start >= 1)
			{
				builder.append(s.substring(last, start));
				
				String inner = s.substring(start, end);
				builder.append(reverse(inner));
				last = end;
			}
		}
		builder.append(s.substring(last));
		
		return builder.toString();
	}
	
	private static char[] reverse(String s)
	{
		char[] base = s.toCharArray();
		char[] reverse = new char[base.length];
		for(int i=0;i<base.length;i++)
		{
			reverse[i] = base[base.length-i-1];
		}
		return reverse;
	}
}
