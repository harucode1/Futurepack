package futurepack.depend.api.helper;

import java.nio.DoubleBuffer;
import java.util.function.Function;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;

import net.minecraft.client.Minecraft;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;

public class HelperContainerSync
{
//	private ITilePropertyStorage store;
//	private Container holder;
//	
//	private int[] buffer;
//	
//	public HelperContainerSync(Container c, ITilePropertyStorage sto)
//	{
//		this.store = sto;
//		this.holder = c;
//		buffer = new int[sto.getPropertyCount()];
//		Arrays.fill(buffer, Integer.MIN_VALUE);
//	}
//	
//	public void detectAndSendChanges(List<IContainerListener> crafts)
//	{
//		for(int i=0;i<store.getPropertyCount();i++)
//		{
//			if(buffer[i] != store.getProperty(i))
//			{
//				buffer[i] = store.getProperty(i);
//				for(IContainerListener c : crafts)
//					c.sendWindowProperty(holder, i, buffer[i]);			
//			}
//		}
//	}
//	
//	public void onUpdate(int id, int value)
//	{
//		buffer[id] = value;
//		store.setProperty(id, value);
//	}
	
	/**
	 * use lamdas, if this is a container
	 * HelperContainerSync.addInventorySlots(8, 84, inv, this::addSlot);
	 * 
	 */
	public static void addInventorySlots(int x, int y, IInventory inv, Function<Slot, Slot> addSlot)
	{
		for (int l = 0; l < 3; ++l)
        {
            for (int i1 = 0; i1 < 9; ++i1)
            {
                addSlot.apply(new Slot(inv, i1 + l * 9 + 9, x + i1 * 18, y + l * 18));
            }
        }

        for (int l = 0; l < 9; ++l)
        {
            addSlot.apply(new Slot(inv, l, x + l * 18, y+58));
        }
	}
	
	
	static double cursorPosStoreX;
	static double cursorPosStoreY;
	static long time = 0L;
	
	public static void SaveCursorPos()
	{
		DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
		DoubleBuffer y = BufferUtils.createDoubleBuffer(1);
		GLFW.glfwGetCursorPos(Minecraft.getInstance().getWindow().getWindow(), x, y);
		cursorPosStoreX = x.get();
		cursorPosStoreY = y.get();
		time = System.currentTimeMillis();
	}
	
	public static void RestoreCursorPos()
	{
		if(System.currentTimeMillis() - time <= 1000)
		{
			GLFW.glfwSetCursorPos(Minecraft.getInstance().getWindow().getWindow(), cursorPosStoreX, cursorPosStoreY);
		}
	}
	
}
