package futurepack.depend.api.helper;

import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.IItemSupport;
import futurepack.common.block.ItemMoveTicker;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class HelperItems 
{
	public static ITextComponent getTooltip(ItemStack it, IItemNeon neon)
	{
		return new TranslationTextComponent("tooltip.futurepack.item.neon", neon.getNeon(it), neon.getMaxNeon(it));
	}
	
	public static ITextComponent getTooltip(ItemStack it, IItemSupport sp)
	{
		return new TranslationTextComponent("tooltip.futurepack.item.support", sp.getSupport(it), sp.getMaxSupport(it));
	}

	public static void moveItemTo(World world, Vector3d destination, Vector3d itemPos) 
	{
		if(world.isClientSide)
			throw new IllegalStateException("This method is not allowed on Clients!");

		new ItemMoveTicker(world, destination, itemPos);
	}
	
	public static void disableItemSpawn()
	{
		HelperEntities.disableItemSpawn();
	}
	
	public static void enableItemSpawn()
	{
		HelperEntities.enableItemSpawn();
	}
}
