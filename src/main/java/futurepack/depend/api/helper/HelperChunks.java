package futurepack.depend.api.helper;

import java.io.File;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.function.LongConsumer;

import javax.annotation.Nullable;

import futurepack.common.FPConfig;
import futurepack.common.entity.EntityDrone;
import it.unimi.dsi.fastutil.longs.LongArraySet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongRBTreeSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.chunk.ChunkStatus;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.server.ServerChunkProvider;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.server.TicketType;
import net.minecraft.world.storage.WorldSavedData;

public class HelperChunks 
{
	public static final TicketType<ChunkPos> FUTUREPACK = TicketType.create("futurepack", Comparator.comparingLong(ChunkPos::toLong));
	public static final int TICKET_DISTANCE = 2; //same as force chunk
	
	
	private static class TicketKeeper extends WorldSavedData implements Runnable
	{
		private Map<Entity, EntityTicket<Entity>> ticketMap = new WeakHashMap<Entity, HelperChunks.EntityTicket<Entity>>();
		private LongSet chunksToReload = new LongRBTreeSet();
		
		public final ServerWorld world;
		
		private TicketKeeper(ServerWorld world)
		{
			super("futurepack_chunks");
			this.world = world;
		}

		@Override
		public void load(CompoundNBT nbt) 
		{
			long[] chunks = nbt.getLongArray("chunks");
			synchronized (chunksToReload) 
			{
				for(long l : chunks)
				{
					chunksToReload.add(l);
				}
			}
			Thread t = new Thread(this, "FP Reload Chunks");
			t.setDaemon(true);
			t.start();
		}

		@Override
		public CompoundNBT save(CompoundNBT nbt) 
		{
			LongSet chunks;
			synchronized (chunksToReload) 
			{
				chunks = new LongArraySet(chunksToReload.size() + ticketMap.size());
				chunks.addAll(chunksToReload);
			}
			ticketMap.keySet().stream().mapToLong(e -> new ChunkPos(e.blockPosition()).toLong()).forEach(chunks::add);
			nbt.putLongArray("chunks", chunks.toLongArray());
			return nbt;
		}
		
		@Override
		public void run() 
		{
			try  {
				Thread.sleep(50); 
			} catch (InterruptedException e)  {
				e.printStackTrace();
			}
			
			Optional<Long> opt;
			synchronized (chunksToReload)
			{
				opt = chunksToReload.stream().findAny();
			}
			LongSet focedChunksToUnforce = new LongOpenHashSet();
			
			while(opt.isPresent())
			{
				long pos = opt.get();
				
				world.getServer().submitAsync(() -> 
				{
					if(world.setChunkForced(ChunkPos.getX(pos), ChunkPos.getZ(pos), true))
						focedChunksToUnforce.add(pos);
				}).isDone();
				synchronized (chunksToReload) 
				{
					chunksToReload.remove(pos);
				}
				
				try  {
					Thread.sleep(50);
				} catch (InterruptedException e)  {
					e.printStackTrace();
				}
				
				synchronized (chunksToReload)
				{
					opt = chunksToReload.stream().findAny();
				}
			}
			
			try  {
				Thread.sleep(50 * 50);//50 ticks
			} catch (InterruptedException e)  {
				e.printStackTrace();
			}
			
			for(long pos : focedChunksToUnforce)
			{
				world.getServer().submitAsync(() -> 
				{
					IChunk c = world.getChunk(ChunkPos.getX(pos), ChunkPos.getZ(pos), ChunkStatus.FULL, true); //loads the chunk
//					if(c instanceof Chunk)
//					{
//						Chunk ch = (Chunk) c;
//						for(ClassInheritanceMultiMap<Entity> map : ch.getEntityLists())
//						{
//							for(Entity e : map)
//							{
//								world.updateEntity(e);//tickk all entities once
//							}
//						}
//					}
				}).isDone();
				
				try  {
					Thread.sleep(50 * 10); //hopefully do 10 ticks
				} catch (InterruptedException e)  {
					e.printStackTrace();
				}
			}
			
			for(long pos : focedChunksToUnforce)
			{
				world.getServer().submitAsync(() -> 
				{
					world.setChunkForced(ChunkPos.getX(pos), ChunkPos.getZ(pos), false);
				});
			}
			focedChunksToUnforce.clear();
		}
		
	}
	
	public static TicketKeeper getTicketKeeper(ServerWorld world)
	{
		return world.getDataStorage().computeIfAbsent(() -> new TicketKeeper(world), "futurepack_chunks");
	}
	
	
	private static class EntityTicket<T extends Entity> 
	{
		private LongSet loadedChunks = new LongRBTreeSet();
		private final T owner;
		private final ServerWorld world;
		
		public EntityTicket(T owner, ServerWorld world)
		{
			this.owner = owner;
			this.world = world;
		}
		
		public void addChunk(int x, int z)
		{
			addChunk(ChunkPos.asLong(x, z));
		}
		
		public void addChunk(long l)
		{
			if(!loadedChunks.contains(l))
			{
				ChunkPos pos = new ChunkPos(l);
				world.getChunkSource().addRegionTicket(FUTUREPACK, pos, TICKET_DISTANCE, pos); // register
				loadedChunks.add(l);
			}
		}
		
		public void removeChunk(int x, int z)
		{
			removeChunk(ChunkPos.asLong(x, z));
		}
		
		public void removeChunk(long l)
		{
			if(loadedChunks.contains(l))
			{
				ChunkPos pos = new ChunkPos(l);
				world.getChunkSource().removeRegionTicket(FUTUREPACK, pos, TICKET_DISTANCE, pos); // release
				loadedChunks.remove(l);
			}
		}
		
		public void clear()
		{
			for(long l : loadedChunks)
			{
				ChunkPos pos = new ChunkPos(l);
				world.getChunkSource().removeRegionTicket(FUTUREPACK, pos, 3, pos); // release
			}
			loadedChunks.clear();
		}
		
		public void update()
		{
			if(!owner.isAlive())
			{
				removeTicket(world, this);
			}
		}

		public void onlyLoad(long[] add) 
		{
			LongSet rem = new LongArraySet(loadedChunks.size());
			
			rem.addAll(loadedChunks);
			for(long l : add)
			{
				rem.remove(l);
				addChunk(l);
			}
			rem.forEach((LongConsumer) this::removeChunk);
		}
	}
	
	private static void removeTicket(ServerWorld world, EntityTicket<?> ticket)
	{
		ticket.clear();
		TicketKeeper keeper = getTicketKeeper(world);
		if(keeper.ticketMap.remove(ticket.owner)!=null)
		{
			keeper.setDirty(true);
		}
	}
	
	private static EntityTicket<Entity> getTicket(ServerWorld world, Entity entity)
	{
		TicketKeeper keeper = getTicketKeeper(world);
		return keeper.ticketMap.computeIfAbsent(entity, e -> 
		{
			keeper.setDirty(true);
			return new EntityTicket(e, world);
		});
	}
	
	public static void forceloadChunksForEntity(EntityDrone e)
	{
		if(!FPConfig.SERVER.disableMinerChunkloading.get())
		{
			World w = e.getCommandSenderWorld();
			if(!w.isClientSide)
			{
				ServerWorld sw = (ServerWorld) w;
				EntityTicket<EntityDrone> ticket = ((EntityTicket)getTicket(sw, e));
				int i = MathHelper.floor(e.getX())>>4;
				int j = MathHelper.floor(e.getZ())>>4;
				
				int ip = MathHelper.floor(e.getX()+5)>>4;
				int jp = MathHelper.floor(e.getZ()+5)>>4;
				int im = MathHelper.floor(e.getX()-5)>>4;
				int jm = MathHelper.floor(e.getZ()-5)>>4;
				
				//only 1 chunk is loaded when entity is near the middle of a chunk otherwise at best 4 + home position 
				
//				System.out.println(e);
				
				ticket.onlyLoad(new long[]
				{
					ChunkPos.asLong(i, j),
					ChunkPos.asLong(ip,jp),
					ChunkPos.asLong(im, jm),
					ChunkPos.asLong(ip, jm),
					ChunkPos.asLong(im, jp),
					new ChunkPos(e.getInventoryPos()).toLong()
				});
			}
		}
	}
	
	public static void removeTicketIfNeeded(ServerWorld world, Entity entity)
	{
		TicketKeeper keeper = getTicketKeeper(world);
		EntityTicket<Entity> t = keeper.ticketMap.get(entity);
		if(t!=null)
		{
			t.update();
		}
	}

	@Nullable
	public static File getDimensionDir(World w)
	{
		ServerChunkProvider prov = ((ServerWorld)w).getChunkSource();
		File dir = prov.chunkMap.storageFolder;
		return dir;
	}

	@Nullable
	public static File getDimensionDir(IWorld w)
	{
		return getDimensionDir((World)w);
	}
	public static boolean forceChunkIfNeeded(World w, int x, int z) 
	{
		if(!w.isClientSide)
		{
			ServerWorld serv = (ServerWorld) w;
			return serv.setChunkForced(x, z, true);
		}
		return false;
	}
	
	public static void unforceChunks(World w, int x, int z) 
	{
		if(!w.isClientSide)
		{
			ServerWorld serv = (ServerWorld) w;
			serv.setChunkForced(x, z, false);
		}
	}
	
	public static void renderUpdate(World w, BlockPos pos)
	{
//		w.notifyBlockUpdate(pos, Blocks.AIR.getDefaultState(), w.getBlockState(pos), 3);
	}
	
}
