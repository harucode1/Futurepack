package futurepack.depend.api.helper;

import futurepack.common.FPConfig;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Util;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;

public class HelperResearch
{
//	public static boolean isUseable(EntityPlayer pl, Object o)
//	{
//		return true;
//	}
//	
	public static boolean isUseable(PlayerEntity pl, Block o)
	{
		return isUseable(pl, new ItemStack(o));
	}
	
	public static boolean isUseable(PlayerEntity pl, Item o)
	{
		return isUseable(pl, new ItemStack(o));
	}
	
	public static boolean isUseable(PlayerEntity pl, TileEntity o)
	{
		return isUseable(pl, o.getLevel().getBlockState(o.getBlockPos()));
	}
	
	public static boolean isUseable(PlayerEntity pl, BlockState o)
	{
		return isUseable(pl, new ItemStack(o.getBlock()));
	}
	
	public static boolean isUseable(PlayerEntity pl, ItemStack o)
	{
		CustomPlayerData cd = CustomPlayerData.getDataFromPlayer(pl);	
		return cd.canProduce(o);
	}
	
	public static boolean canOpen(PlayerEntity pl, BlockState bl)
	{
		if(pl.level.isClientSide)
		{
			return false;
		}
		else if(isUseable(pl, bl) || FPConfig.SERVER.disableMachineLock.get())
		{
			return true;
		}
		else
		{
			TranslationTextComponent trans = new TranslationTextComponent("research.useblock.missing", "");
			Style style = Style.EMPTY;
			style.withColor(TextFormatting.RED);
			trans.setStyle(style);
			pl.sendMessage(trans, Util.NIL_UUID);
			return false;
		}
	}

	public static boolean canOpen(PlayerEntity pl, Entity entity)
	{
		return true;
	}
}
