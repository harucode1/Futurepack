package futurepack.depend.api;

import java.util.List;

import futurepack.api.ItemPredicateBase;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;

public class NullPredicate extends ItemPredicateBase
{

	@Override
	public boolean apply(ItemStack input)
	{
		return false;
	}

	@Override
	public ItemStack getRepresentItem()
	{
		return null;
	}

	@Override
	public int getMinimalItemCount(ItemStack input)
	{
		return 0;
	}
	
	@Override
	public List<ItemStack> collectAcceptedItems(List<ItemStack> list)
	{
		return list;
	}
	
	@Override
	public String toString()
	{
		return "nullPredicate";
	}
	
	public void write(PacketBuffer buf)
	{
		
	}
	
	public static NullPredicate read(PacketBuffer buf)
	{
		return new NullPredicate();
	}
}
