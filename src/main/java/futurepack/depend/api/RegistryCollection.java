package futurepack.depend.api;

import futurepack.api.EnumScanPosition;
import futurepack.api.interfaces.IGuiRenderable;
import futurepack.api.interfaces.IPlanet;
import futurepack.api.interfaces.IScanPart;
import futurepack.api.interfaces.ISpaceshipUpgrade;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.common.recipes.crushing.FPCrushingManager;
import futurepack.common.recipes.industrialfurnace.FPIndustrialFurnaceManager;
import futurepack.common.recipes.industrialfurnace.FPIndustrialNeonFurnaceManager;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import futurepack.common.research.ResearchPage;
import futurepack.common.research.ScanningManager;
import futurepack.common.spaceships.FPPlanetRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

/**
 * This class contains all the register methods of all classes 
 */
public class RegistryCollection
{
	public static void registerScanPart(EnumScanPosition pos, IScanPart entry)
	{
		ScanningManager.register(pos, entry);
	}
	
	public static Research createResearch(ResourceLocation name, int x, int y, IGuiRenderable icon, ResearchPage base)
	{
		return ResearchManager.createResearch(name, x, y, icon, base);
	}
	
	public static void registerPlanet(IPlanet planet)
	{
		FPPlanetRegistry.instance.registerPlanet(planet);
	}
	
	public static void registerShipUpgrade(ISpaceshipUpgrade upgrade)
	{
		FPPlanetRegistry.instance.registerUpgrade(upgrade);
	}
	
	public static void addAssemblyRecipe(String id, ItemStack[] in, ItemStack out)
	{
		FPAssemblyManager.instance.addRecipe(id, out, in);
	}
	
	public static void addCrushingRecipe(ItemStack in, ItemStack out)
	{
		FPCrushingManager.addCrusherRecipe(in, out);
	}
	
	public static void addIndustrialFurnaceRecipe(String id, ItemStack[] in, ItemStack out)
	{
		FPIndustrialFurnaceManager.addRecipe(id, out, in);
	}
	
	public static void addIndustrialNeonFurnaceRecipe(String id, ItemStack[] in, ItemStack out, int support)
	{
		FPIndustrialNeonFurnaceManager.addRecipe(id, support, out, in);
	}
	
	
}
