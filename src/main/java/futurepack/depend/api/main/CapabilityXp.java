package futurepack.depend.api.main;

import java.util.concurrent.Callable;

import futurepack.api.interfaces.tilentity.ITileXpStorage;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class CapabilityXp implements ITileXpStorage
{
	
	public static class Factory implements Callable<ITileXpStorage>
	{
		@Override
		public ITileXpStorage call() throws Exception 
		{
			return new CapabilityXp();
		}
		
	}
	
	public static class Storage implements IStorage<ITileXpStorage>
	{

		@Override
		public INBT writeNBT(Capability<ITileXpStorage> capability, ITileXpStorage instance, Direction side)
		{
			return IntNBT.valueOf(instance.getXp());
		}

		@Override
		public void readNBT(Capability<ITileXpStorage> capability, ITileXpStorage instance, Direction side, INBT nbt)
		{
			instance.setXp( ((IntNBT)nbt).getAsInt() );
		}
		
	}
	
	private int xp = 0;
	
	@Override
	public int getXp()
	{
		return xp;
	}

	@Override
	public int getMaxXp()
	{
		return 100;
	}

	@Override
	public void setXp(int lvl)
	{
		xp = lvl;
	}

}
