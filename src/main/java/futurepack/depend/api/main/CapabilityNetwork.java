package futurepack.depend.api.main;

import java.util.concurrent.Callable;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class CapabilityNetwork implements ITileNetwork
{
	public static class Factory implements Callable<ITileNetwork>
	{
		@Override
		public ITileNetwork call() throws Exception 
		{
			return new CapabilityNetwork();
		}	
	}
	
	public static class Storage implements IStorage<ITileNetwork>
	{
		@Override
		public INBT writeNBT(Capability<ITileNetwork> capability, ITileNetwork instance, Direction side)
		{
			return null;
		}

		@Override
		public void readNBT(Capability<ITileNetwork> capability, ITileNetwork instance, Direction side, INBT nbt)
		{
			
		}
	}
	
	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public void onFunkPacket(PacketBase pkt) { }

}
