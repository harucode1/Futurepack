//package futurepack.depend.api.main;
//
//import futurepack.api.Constants;
//import futurepack.api.interfaces.tilentity.ITileHologramAble;
//import futurepack.api.interfaces.tilentity.ITileNetwork;
//import futurepack.api.interfaces.tilentity.ITileXpStorage;
//import net.minecraftforge.common.capabilities.Capability;
//import net.minecraftforge.common.capabilities.CapabilityInject;
//import net.minecraftforge.common.capabilities.CapabilityManager;
//import net.minecraftforge.fml.common.FMLCommonHandler;
//import net.minecraftforge.fml.common.Mod;
//import net.minecraftforge.fml.common.Mod.Instance;
//import net.minecraftforge.fml.common.Mod.Metadata;
//import net.minecraftforge.fml.common.ModMetadata;
//import net.minecraftforge.fml.common.event.FMLInitializationEvent;
//import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
//import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
//import net.minecraftforge.fml.loading.FMLCommonLaunchHandler;
//
////@Mod(modid=ApiMain.modID,name="Futurepack API", version="1.1.0")
//public class ApiMain
//{
//	public static final String modID = "fp.api";
//	
//	
//	@Instance(ApiMain.modID)
//	public static ApiMain instance;
//	
//	@Instance(value=Constants.MOD_ID, owner=ApiMain.modID)
//	public static Object parent;
//	
//	@Metadata(value=Constants.MOD_ID, owner=ApiMain.modID)
//	public static ModMetadata parentMeta;
//		
//	@CapabilityInject(ITileNetwork.class)
//	private static final Capability<ITileNetwork> cap_NETWORK = null;
//	@CapabilityInject(ITileXpStorage.class)
//	private static final Capability<ITileXpStorage> cap_EXP = null;
//
//	@CapabilityInject(ITileHologramAble.class)
//	private static final Capability<ITileHologramAble> cap_HOLOGRAM = null;
//	
//	
//	@Mod.EventHandler
//	public void preInit(FMLPreInitializationEvent event)
//	{
//		if(parent!=null)
//			event.getModMetadata().parentMod = FMLCommonLaunchHandler.instance().findContainerFor(parent);
//		
//		
//		parentMeta.childMods.add(FMLCommonHandler.instance().findContainerFor(this));
//		
//		CapabilityManager.INSTANCE.register(ITileNetwork.class, new CapabilityNetwork.Storage(), new CapabilityNetwork.Factory());
//		CapabilityManager.INSTANCE.register(ITileXpStorage.class, new CapabilityXp.Storage(), new CapabilityXp.Factory());
//		CapabilityManager.INSTANCE.register(ITileHologramAble.class, new CapabilityHologram.Storage(), new CapabilityHologram.Factory());
//	}
//	
//	@Mod.EventHandler
//	public void init(FMLInitializationEvent event)
//	{
//		
//	}
//	
//	@Mod.EventHandler
//	public void postInit(FMLPostInitializationEvent event)
//	{
//		
//	}
//	
////	@CapabilityInject(INeonEnergyStorage.class)
////	private static void onRegisteredNeon(Capability<INeonEnergyStorage> neon)
////	{
////		
////	}
////	
////	@CapabilityInject(ISupportStorage.class)
////	private static void onRegisteredSupport(Capability<ISupportStorage> support)
////	{
////		
////	}
////	
////	@CapabilityInject(ITileNetwork.class)
////	private static void onRegisteredNetwork(Capability<ITileNetwork> net)
////	{
////		
////	}
////	
////	@CapabilityInject(ITileXpStorage.class)
////	private static void onRegisteredXP(Capability<ITileXpStorage> xp)
////	{
////		
////	}
////	
////	@CapabilityInject(ILogisticInterface.class)
////	private static void onRegisteredLogisitic(Capability<ILogisticInterface> logistic)
////	{
////		
////	}
////	
////	@CapabilityInject(ITileHologramAble.class)
////	private static void onRegisteredHologram(Capability<ITileHologramAble> holo)
////	{
////		
////	}
//}
