package futurepack.depend.api;

import net.minecraft.client.resources.I18n;

public enum EnumScannerState
{
	WrongItem("wrong.item"),
	ResearchedEverything("research.everything"),
	MissingBasses("research.basses.missing"),
	WrongAspects("wrong.aspetcs"),
	Succses("succses"),
	Partwise("pastwise"),
	Error("error");
	
	private String unlocalized;
	
	private EnumScannerState(String text)
	{
		unlocalized = text;
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public String getLocalizedText()
	{
		return I18n.get(getUnlocalizedText()+ ".name");
	}
	
	public String getUnlocalizedText()
	{
		return "scanner." + unlocalized;
	}
}
