package futurepack.depend.api.interfaces;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.network.PacketBuffer;

public interface IGuiSyncronisedContainer 
{
	public void writeToBuffer(PacketBuffer buf);
	
	public void readFromBuffer(PacketBuffer buf);
	
	public static void writeNBT(PacketBuffer buf, CompoundNBT nbt)
	{
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		try 
		{
			CompressedStreamTools.writeCompressed(nbt, bytes);
			buf.writeByteArray(bytes.toByteArray());
			bytes.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static CompoundNBT readNBT(PacketBuffer buf)
	{
		ByteArrayInputStream bytes = new ByteArrayInputStream(buf.readByteArray());
		try
		{
			CompoundNBT nbt = CompressedStreamTools.readCompressed(bytes);
			return nbt;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
