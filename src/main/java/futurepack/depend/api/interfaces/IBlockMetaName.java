package futurepack.depend.api.interfaces;

import net.minecraft.item.ItemStack;

public interface IBlockMetaName 
{
	public String getMetaNameSub(ItemStack is);
}
