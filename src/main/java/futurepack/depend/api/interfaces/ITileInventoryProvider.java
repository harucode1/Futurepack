package futurepack.depend.api.interfaces;


import futurepack.common.gui.inventory.GuiCompositeChest;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;

public abstract interface ITileInventoryProvider 
{
	public abstract IInventory getInventory();
	
	public default GuiCompositeChest.GenericContainer getInventoryContainer(PlayerInventory inv)
	{
		return new GuiCompositeChest.GenericContainer(inv, this);
	}
	
	public abstract String getGUITitle();
}
