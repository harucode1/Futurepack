package futurepack.depend.api;

import java.util.List;

import futurepack.api.ItemPredicateBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;

public class ItemPredicate extends ItemPredicateBase
{
	private final Item item;
	private final int size;
	
	public ItemPredicate(Item item, int size)
	{
		this.item = item;
		this.size = size;
	}
	
	public ItemPredicate(Item item)
	{
		this(item, 1);
	}
	
	public ItemPredicate(ItemStack item)
	{
		this.item = item.getItem();
		this.size = item.getCount();
	}

	@Override
	public boolean apply(ItemStack input)
	{
		return input!=null && input.getItem() == item;
	}

	@Override
	public ItemStack getRepresentItem()
	{
		return new ItemStack(item, size);
	}

	@Override
	public int getMinimalItemCount(ItemStack input)
	{
		return size;
	}
	
	@Override
	public List<ItemStack> collectAcceptedItems(List<ItemStack> list)
	{
		list.add(new ItemStack(item, size));
		return list;
	}
	
	@Override
	public String toString()
	{
		return item.getRegistryName().toString() + "@" + size;
	}
	
	public void write(PacketBuffer buf)
	{
		buf.writeItem(new ItemStack(item, size));
	}
	
	public static ItemPredicate read(PacketBuffer buf)
	{
		return new ItemPredicate(buf.readItem());
	}
}
