package futurepack.common;

import java.io.PrintStream;
import java.util.Calendar;

import org.apache.logging.log4j.message.AbstractMessageFactory;
import org.apache.logging.log4j.message.Message;

public class SavedMessageFactory extends AbstractMessageFactory 
{
	private final PrintStream out;

	public SavedMessageFactory(PrintStream out)
	{
		this.out = out;
	}
	
	@Override
	public Message newMessage(Object message)
	{
		Message m = super.newMessage(message);
		out.print(TimeAndThread());
		out.println(m.getFormattedMessage());
    	out.flush();
		return m;
	}
	
	@Override
	public Message newMessage(String message)
	{
		Message m = super.newMessage(message);
		out.print(TimeAndThread());
		out.println(m.getFormattedMessage());
    	out.flush();
		return m;
	}
	
    @Override
    public Message newMessage(final String message, final Object... params)
    {
        Message m = super.newMessage(String.format(message, params));
        out.print(TimeAndThread());
		out.println(m.getFormattedMessage());
    	out.flush();
        return m;
    }
    
    private String TimeAndThread()
    {
    	Calendar c = Calendar.getInstance();
    	return String.format("[%s:%s:%s][%s] ", c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND), Thread.currentThread().getName());
    }
}
