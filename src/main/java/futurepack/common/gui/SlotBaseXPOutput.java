package futurepack.common.gui;

import java.util.function.DoubleSupplier;

import futurepack.common.FuturepackTags;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.modification.EnumChipType;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.IRecipeHolder;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;

public class SlotBaseXPOutput extends Slot
{
	private PlayerEntity thePlayer;
    private int sacksize;
	private DoubleSupplier base;
    
	public SlotBaseXPOutput(PlayerEntity player, TileEntityModificationBase base, int slotIndex, int xPosition, int yPosition)
    {
        super((IInventory) base, slotIndex, xPosition, yPosition);
        this.thePlayer = player;
        this.base = () -> base.getChipPower(EnumChipType.ULTIMATE);
    }
	
	public SlotBaseXPOutput(PlayerEntity player, IInventory base, int slotIndex, int xPosition, int yPosition, DoubleSupplier ultimateChipPower)
    {
        super((IInventory) base, slotIndex, xPosition, yPosition);
        this.thePlayer = player;
        this.base = ultimateChipPower;
    }

    @Override
	public boolean mayPlace(ItemStack stack)
    {
    	return container.canPlaceItem(getSlotIndex(), stack);
    }

    @Override
	public ItemStack remove(int amount)
    {
        if (this.hasItem())
        {
        	sacksize += Math.min(amount, this.getItem().getCount());
        }

        return super.remove(amount);
    }

    @Override
	public ItemStack onTake(PlayerEntity playerIn, ItemStack stack)
    {
        this.checkTakeAchievements(stack);
        return super.onTake(playerIn, stack);
    }

    @Override
	protected void onQuickCraft(ItemStack stack, int amount)
    {
        this.sacksize += amount;
        this.checkTakeAchievements(stack);
    }

    @Override
	protected void checkTakeAchievements(ItemStack stack)
    {
        stack.onCraftedBy(this.thePlayer.level, this.thePlayer, this.sacksize);

        if (!this.thePlayer.level.isClientSide)
        {
        	Item crafted = stack.getItem();
            if(crafted.is(FuturepackTags.GEMS) || crafted.is(FuturepackTags.INGOTS))
            {
            	double f = 0.1D * (1D + base.getAsDouble());

                int i = stack.getCount();
                if (f == 0.0F) {
                   i = 0;
                } else if (f < 1.0F) {
                   int j = MathHelper.floor(i * f);
                   if (j < MathHelper.ceil(i * f) && Math.random() < i * f - j) {
                      ++j;
                   }

                   i = j;
                }

                while(i > 0) {
                   int k = ExperienceOrbEntity.getExperienceValue(i);
                   i -= k;
                   this.thePlayer.level.addFreshEntity(new ExperienceOrbEntity(this.thePlayer.level, this.thePlayer.getX(), this.thePlayer.getY() + 0.5D, this.thePlayer.getZ() + 0.5D, k));
                }
             }
            
            if(container instanceof IRecipeHolder)
            	((IRecipeHolder)this.container).awardUsedRecipes(this.thePlayer);
        }

        this.sacksize = 0;
    }
}
