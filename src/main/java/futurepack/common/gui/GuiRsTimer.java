package futurepack.common.gui;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.misc.TileEntityRsTimer;
import futurepack.common.gui.inventory.ActuallyUseableContainerScreen;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

public class GuiRsTimer extends ActuallyUseableContainerScreen<GuiRsTimer.ContainerRsTimer>
{
	private static ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/rs_timer.png");
	
	public GuiRsTimer(PlayerEntity pl, TileEntityRsTimer tile)
	{
		super(new ContainerRsTimer(pl.inventory, tile), pl.inventory, "gui.rs_timer");
	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		this.minecraft.getTextureManager().bind(res);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
		
		
	}
		
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) 
	{
		this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
	}
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		if(mouseButton == 0)
		{
			double mx = mouseX - leftPos;
			double my = mouseY - topPos;
			if(mx >= 124 && mx <= 133)
			{
				long time = tile().getMaxTime();
				boolean changed = false;
				
				if(my > 9 && my < 18)
				{
					time += 1200;
					changed = true;
				}
				else if(my > 18 && my < 27)
				{
					time -= 1200;
					changed = true;
				}
				else if(my > 29 && my < 38)
				{
					time += 20;
					changed = true;
				}
				else if(my > 38 && my < 47)
				{
					time -= 20;
					changed = true;
				}
				else if(my > 49 && my < 58)
				{
					 time += 1;
					 changed = true;
				}
				else if(my > 58 && my < 67)
				{
					time -= 1;
					changed = true;
				}
				
				if(time<1)
				{
					 time = 1;
					 changed = true;
				}
				
				if(changed)
				{
					tile().setMaxTime(time);
					FPPacketHandler.syncWithServer(this.getMenu());
					return true;
				}
			}
		}
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		super.renderLabels(matrixStack, mouseX, mouseY);
		
		long time = tile().getMaxTime();
		
		int ticks = (int) (time % 20);
		int seconds = (int) ((time / 20) % 60);
		long min = (time / 1200);

		font.draw(matrixStack, min  +" min", 49, 15, 0xFF000000);
		font.draw(matrixStack, seconds  +" s", 49, 35, 0xFF000000);
		font.draw(matrixStack, ticks  +" t", 49, 55, 0xFF000000);
	}
	
	private TileEntityRsTimer tile()
	{
		return this.getMenu().tile;
	}

	public static class ContainerRsTimer extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		TileEntityRsTimer tile;
		
		public ContainerRsTimer(PlayerInventory inv, TileEntityRsTimer tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}

		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{			
			return true;
		}

		@Override
		public void writeToBuffer(PacketBuffer nbt) 
		{
			nbt.writeVarLong(tile.getMaxTime());
		}

		@Override
		public void readFromBuffer(PacketBuffer nbt) 
		{
			tile.setMaxTime(nbt.readVarLong());
		}
		
	}
}
