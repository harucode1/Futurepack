package futurepack.common.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import futurepack.api.interfaces.IGuiRenderable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;

public class RenderableItem implements IGuiRenderable
{
	private ItemStack icon;
	
	public RenderableItem(ItemStack item)
	{
		if(item==null)
			throw new NullPointerException("null is not an ItemStack!");
		icon = item;
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, int x, int y, int blitOffset)
	{
		RenderHelper.turnBackOn();
		RenderSystem.enableDepthTest();
		Minecraft.getInstance().getItemRenderer().blitOffset=(float)blitOffset - 100; 	
		RenderSystem.color4f(1F, 1F, 1F, 1F);
		Minecraft.getInstance().getItemRenderer().renderAndDecorateItem(icon, x, y);
		Minecraft.getInstance().getItemRenderer().blitOffset=0F;
	}

	@Override
	public String toString()
	{
		return "Rendering{" + icon +"}";
	}
}
