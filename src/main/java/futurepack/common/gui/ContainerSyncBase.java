package futurepack.common.gui;

import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.gui.inventory.ActuallyUseableContainer;
import net.minecraft.inventory.container.IContainerListener;
import net.minecraft.util.IntReferenceHolder;

public abstract class ContainerSyncBase extends ActuallyUseableContainer
{
//	public final HelperContainerSync localSync;
	
	private boolean inited = false;
	
	public ContainerSyncBase(ITilePropertyStorage store, boolean isRemote)
	{
//		localSync = new HelperContainerSync(this, store);
		for(int i=0;i<store.getPropertyCount();i++)
		{
			this.addDataSlot(new IntRefOfProperty(i, store));
			if(isRemote)
			{
				store.setProperty(i, 0);
			}
		}
	}
	
	@Override
	public void broadcastChanges()
	{
		super.broadcastChanges();
	}
	
	@Override
	public void addSlotListener(IContainerListener listener) 
	{
		super.addSlotListener(listener);
	}
	
	@Override
	public void setData(int id, int data)
	{
		super.setData(id, data);
//		localSync.onUpdate(id, data);
	}
	
	private class IntRefOfProperty extends IntReferenceHolder
	{
		private final int id;
		private final ITilePropertyStorage props;
		
		public IntRefOfProperty(int id, ITilePropertyStorage props) 
		{
			super();
			this.id = id;
			this.props = props;
		}

		@Override
		public int get() 
		{
			return props.getProperty(id);
		}

		@Override
		public void set(int value) 
		{
			props.setProperty(id, value);
		}
		
	}
}
