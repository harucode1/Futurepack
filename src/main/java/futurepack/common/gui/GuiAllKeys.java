package futurepack.common.gui;

import java.util.Arrays;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.common.sync.KeyManager;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import futurepack.common.sync.NetworkHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.text.TranslationTextComponent;

public class GuiAllKeys extends Screen
{

	public GuiAllKeys()
	{
		super(new TranslationTextComponent("futurepack.gui.all_keys"));
	}
	
	@Override
	public void init(Minecraft minecraft, int width, int height)
	{
		super.init(minecraft, width, height);
		
		int i=-1;
		
		EnumKeyTypes[] keys = KeyManager.EnumKeyTypes.getKeys();
		int l = (int) (Math.sqrt(keys.length)) +1;
		int buttonWidth = 20;
		int totalWidth = l * buttonWidth;
		int totalHeight = (1 + keys.length / l)* buttonWidth;
		
		int guiLeft = (width - totalWidth) / 2;
		int guiTop = (height - totalHeight) / 2;
		
		for(EnumKeyTypes key : keys)
		{
			i++;
			int x = buttonWidth * (i%l);
			int y = buttonWidth * (i/l);
			
			ButtonAspect ba = new ButtonAspect(i, guiLeft + x, guiTop + y, new RenderableItem(key.getDisplayItemStack(minecraft.player).get()), null);
			ba.click = () -> {
				onClose();
				NetworkHandler.sendKeyPressedToServer(key);
			};
			ba.setMessage(new TranslationTextComponent(key.translationKey));
			addButton(ba);
		}
	}	

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		for(int i = 0; i < this.buttons.size(); ++i) 
		{
			Widget w = this.buttons.get(i);
			
			if(w.isMouseOver(mouseX, mouseY))
			{
				PartRenderer.renderHoverText(matrixStack, mouseX+9, mouseY+7, 450, Arrays.asList(w.getMessage()), this.width / 2);
			}
		}
		
	}
}
