package futurepack.common.gui;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

public class SlotFakeItem extends Slot
{

	public SlotFakeItem(IInventory inventoryIn, int index, int xPosition, int yPosition) 
	{
		super(inventoryIn, index, xPosition, yPosition);
	}

	
	@Override
	public boolean mayPickup(PlayerEntity playerIn)
	{
		return false;
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		return false;
	}
	
}
