package futurepack.common.gui.escanner;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import futurepack.api.Constants;
import futurepack.api.interfaces.IGuiRenderable;
import futurepack.client.research.LocalPlayerResearchHelper;
import futurepack.common.FPConfig;
import futurepack.common.gui.PartRenderer;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchPage;
import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.advancements.Advancement;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;

public class GuiResearchTreePage extends Screen
{
	public static float scale = 1F;
	
	private GuiResearchMainOverview parent;
	private ResearchPage page;
	private CustomPlayerData custom;
	private static final ResourceLocation border = new ResourceLocation(Constants.MOD_ID, "textures/gui/research_overlay.png");
	private static final ResourceLocation researching = new ResourceLocation(Constants.MOD_ID, "textures/gui/overlay_researching.png");
	private ResourceLocation bg;
	private final int bgw,bgh;
	private Research hovered = null;
	
	public GuiResearchTreePage(ResearchPage page, CustomPlayerData custom, GuiResearchMainOverview parent)
	{
		super(new TranslationTextComponent("gui.research.tree.page"));
		this.page = page;
		this.custom = custom;
		bg = page.getBackground();
		bgw = page.getWidth();
		bgh = page.getHeight();
		this.parent = parent;
		
		posX = 200-(bgw-224)/2;
		posY = -(bgh-224)/2;
		checkBorders();
	}
	
	private Random r = new Random(24634L);	
	
	@Override
	public boolean mouseScrolled(double mx, double my, double dw)
	{
		if(dw>0)
		{
			charTyped('+', -1);
		}
		else if(dw<0)
		{
			charTyped('-', -1);
		}
		return super.mouseScrolled(mx, my, dw);
	}
	
	private boolean BState=false;
	private float posX=200,posY=0;

	@Override
	public boolean isPauseScreen()
	{
		return true;
	}	
	
	@Override
	public void init()
	{
		super.init();
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{

		this.r.setSeed(24634L);
		
		int rx = (width - 224)/2;
		int ry = (height - 224)/2;
		
		setBlitOffset(0);
		
		drawInvisibleCloake(matrixStack);
		
		GL11.glPushMatrix();
		GL11.glScalef(scale, scale, 1F);
		
		drawBackground(matrixStack);
		
		if(custom!=null)
		{
			drawResearches(matrixStack, mouseX, mouseY);
		}
		else
		{
			RenderHelper.turnOff();
			RenderSystem.enableDepthTest();
			this.drawCenteredString(matrixStack, font, "loading data ...", width/2, height/2, 0xff0000);
		}
		
		GL11.glPopMatrix();
		
		drawBorder(matrixStack);
		
		//Buttons
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		rx = (width - 256)/2;
		ry = (height - 256)/2;
		

		RenderSystem.pushMatrix();		
		RenderSystem.translatef(0, 0, 210.0f);
		
		drawString(matrixStack, font, "Scale: " + (int)(scale*100) + "%", rx, ry-15, 0xFFFFFF);
		
		RenderSystem.popMatrix();
		
		setBlitOffset(0);
	}
	
	@Override
	public boolean mouseDragged(double mouseX, double mouseY, int button, double dx, double dy) 
	{
		if(button == 0)
		{
			moved = true;
			
			int rx = (width - 224)/2;
			int ry = (height - 224)/2;
			if(mouseX >= rx && mouseX < rx+224 && mouseY >= ry && mouseY < ry+224 && BState)
			{
				posX += (dx)/scale;
				posY += (dy)/scale;
				
				checkBorders();
			}
			BState=true;
		}
		else
		{
			BState=false;
		}	
		
		return super.mouseDragged(mouseX, mouseY, button, dx, dy);
	}
	
	private void drawInvisibleCloake(MatrixStack matrixStack)
	{
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		RenderSystem.enableDepthTest();
		
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		RenderSystem.colorMask(false, false, false, false);
		this.setBlitOffset(200);
		int rx = (width - 256)/2;
		int ry = (height - 256)/2;
		this.fillGradient(matrixStack, 0, 0, rx, height, 0x00ffffff, 0x00ffffff);
		this.fillGradient(matrixStack, rx+256, 0, width, height, 0x00ffffff, 0x00ffffff);	
		this.fillGradient(matrixStack, rx, 0, rx+256, ry, 0x00ffffff, 0x00ffffff);
		this.fillGradient(matrixStack, rx, ry+256, rx+256, height, 0x00ffffff, 0x00ffffff);
		this.setBlitOffset(0);
		RenderSystem.colorMask(true, true, true, true);
	}
	
	private void drawBackground(MatrixStack matrixStack)
	{	
		if(FPConfig.APRIL_FOOLS.getAsBoolean())
		{
			float h  = (System.currentTimeMillis() % 10000) / 10000F;
			float bb  = (float) Math.sin(System.currentTimeMillis() / 7000D)*0.5F + 0.5F;
			float s  = (float) Math.sin(System.currentTimeMillis() / 3000D)*0.5F + 0.5F;
			java.awt.Color col = Color.getHSBColor(h, s, bb);
			float r = col.getRed() / 255F,g = col.getGreen() / 255F, b = col.getBlue() / 255F;
			GL11.glColor4f(r, g, b, 1F);
		}
		this.minecraft.getTextureManager().bind(bg);
		float rx = (width - 224)/2 /scale;
		float ry = (height - 224)/2 /scale;

		PartRenderer.drawQuadWithTexture(matrixStack, bg, (int) (rx +posX), (int) (ry +posY), 0, 0, bgw, bgh, bgw, bgh, bgw, bgh, PartRenderer.ZLEVEL_BACKGROUND);
	}
	
	private boolean moved = false;
	
	@Override
	public boolean mouseReleased(double x, double y, int button) 
	{
		if(!moved && hovered!=null && button == 0)
		{
			parent.openResearchText(hovered);
			return true;
		}
		moved = false;
		return super.mouseReleased(x, y, button);
	}
	
	private void drawResearches(MatrixStack matrixStack, int mx, int my)
	{
		GL11.glColor4f(1F, 1F, 1F, 1F);
		RenderHelper.turnBackOn(); 
		float rx = ((width - 224)/2/scale);
		float ry = ((height - 224)/2/scale);
		mx/=scale;
		my/=scale;
		
		boolean inWindow = mx>=rx && mx<=rx+224/scale && my>=ry && my<=ry+224/scale;
			
		rx+=(bgw-224)/2;
		ry+=(bgh-224)/2;
		
		this.hovered = null;
		
		
		
		for(Research res : page.getEntries())
		{
			if(res!=null)
			{		
				drawResearch(matrixStack, res, mx, my, inWindow, rx, ry);
			}
		}
		RenderHelper.turnOff();
	}
	
	private void drawResearch(MatrixStack matrixStack, Research res, int mx, int my, boolean inWindow, float rx, float ry)
	{
		GL11.glColor4f(1F, 1F, 1F, 1F);

		boolean hasRes = custom.hasResearch(res);
		boolean canRes = custom.canResearch(res);
		
		if(!res.isVisible(hasRes,canRes))
		{
			return;
		}
		
		float x = rx + posX + res.getX()*24 + 102 - 200;
		float y = ry + posY + res.getY()*24 + 102;
		boolean hovered = false;
		if(inWindow)
		{	
			if(mx>=x && my>=y && mx<x+20 && my<y+20)
			{
				hovered = true;
				this.hovered= res; 
				drawHighlights(matrixStack, mx, my, res);
			}
		}
		RenderSystem.enableAlphaTest();
		RenderSystem.enableDepthTest();
		
		float f = 1F+ (float) Math.sin(Math.PI*((System.currentTimeMillis()%1500) / 750F));
		f*=0.5F;
		
		if(res.getParents()!=null)
		{
			boolean moreParents = false;
			//int d = -(res.getParents().length/2);
			int mi = 0;
			ArrayList<Research> hoverExtra = new ArrayList<Research>(res.getParents().length);
			
			for(Research parent : res.getParents())
			{
				float x2 = rx + posX + parent.getX()*24 + 102 - 200;
				float y2 = ry + posY + parent.getY()*24 + 102;
								
				if(isParentAvailable(parent))
				{
					//d++;
					
					int color = 0xff000000;
	
					if(custom.hasResearch(res) && custom.hasResearch(parent))
					{
						color = 0xffffcc22;
					}
					else if(custom.hasResearch(parent))
					{
						int b = (int) (0x22 * f);
						int g = (int) (0xcc * f) << 8;
						int r = (int) (0xff * f) << 16;
						
						color = 0xff111111 | r | g| b;//0xFF00
						//color = 0xff000000;
					}
					//color |= this.r.nextInt( 0xf ) << 4*2;
	
					RenderSystem.color4f(0f, 0f, 0f, 0f);
					RenderSystem.blendFunc(GL11.GL_SRC_COLOR, GL11.GL_ONE_MINUS_DST_COLOR);
					
					RenderSystem.enableDepthTest();
					drawLine(matrixStack, (int)x+10, (int)y+10, (int)x2+10, (int)y2+10, color);
					
					RenderSystem.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
				}
				else
				{
					if(!custom.hasResearch(parent))
					{
						if(hovered)
						{
							hoverExtra.add(parent);
						}
						connectAround(matrixStack, (int)x+12, (int)y+12, (int)x+mi*8-4, (int)y-30, parent.page);
						mi++;		
					}
					else
					{	
						moreParents = true;
					}
				}
			}
			//if(moreParents)
			//{
				//connectToHole(x, y);
			//}
			
			//TODO: reimplement this
			/*if(!hoverExtra.isEmpty())
			{
				int w = 32 + 8 + 1;
				int h = 4 + hoverExtra.size() * 18;
				int sx = mx +8;
				int sy = my - h - 12;
				
				setBlitOffset(getBlitOffset() + 500);
				PartRenderer.renderGradientBox(sx, sy + h/2, w, h, getBlitOffset());
				for(int i=0;i<hoverExtra.size();i++)
				{
					int tx = sx;
					int ty = sy + i*18 + 2;
					Research r = hoverExtra.get(i);
					r.page.getIcon().render(mx, my, tx, ty, getBlitOffset());
					tx += 18;
					font.drawString(matrixStack, ">", tx, ty+4, 0xFFFFFFFF);
					tx += 5;
					if(custom.canResearch(r))
					{
						r.getIcon().render(mx, my, tx, ty, getBlitOffset());
					}
					else
					{
						HelperComponent.renderQuestionmark(tx, ty, getBlitOffset());
					}
				}
				setBlitOffset(getBlitOffset() - 500);
			}*/
		}
		
		this.setBlitOffset(1);

		//Render Research Background
		RenderSystem.enableBlend();
		
		GL11.glColor4f(0F, 0F, 0F, 1F);
		
		if(hasRes)
		{
			GL11.glColor4f(1F, 1F, 1F, 1F);
		}
		else if(canRes)
		{
			GL11.glColor4f(0.5F + f, 0.5F + f, 0.5F + f, 1f);
		}
		
		setBlitOffset(getBlitOffset() + 1);
		
		RenderHelper.turnOff();
		this.minecraft.getTextureManager().bind(res.getBackground());		
		AbstractGui.blit(matrixStack, (int)x-2, (int)y-2, getBlitOffset(), 0, 0, 24, 24, 24, 24);
		
		setBlitOffset(getBlitOffset() - 1);
		
		
		//Render Blue Border (Researching)
		if(LocalPlayerResearchHelper.researching.contains(res.getName()))
		{
			this.minecraft.getTextureManager().bind(researching);
			setBlitOffset(getBlitOffset() +1);
			AbstractGui.blit(matrixStack, (int)x-2, (int)y-2, getBlitOffset(), 0, 0, 24, 24, 24, 24);
			setBlitOffset(getBlitOffset() -1);
		}
		
		
		if(res.getIcon()!=null && (custom.hasResearch(res) || custom.canResearch(res)))
		{				
			GL11.glColor4f(1F, 1F, 1F, 1F);
			
			//I think this code is bad
			setBlitOffset(getBlitOffset() + 2);
			
			IGuiRenderable ico = res.getIcon();
			
			ico.render(matrixStack, mx, my, (int)x+2, (int)y+2, getBlitOffset());
			

			
			if(Collections.binarySearch(LocalPlayerResearchHelper.notReaded,res.getName())>=0)
			{
				setBlitOffset(getBlitOffset() +2);
							
				int ix = (int) (x +17);
				int iy = (int) (y+1);
				
				int base= 0xff0050;
				int a = (int) ((Math.sin(  (System.currentTimeMillis()/70+ ix*iy)/3.0)) * 64) +128;
				PartRenderer.fill(matrixStack, ix, iy, ix+3, iy+3, (255)<<24 | 0x000000, getBlitOffset());
				
				PartRenderer.fill(matrixStack, ix-2, iy+1, ix+5, iy+2, a<<24 | base, getBlitOffset());
				PartRenderer.fill(matrixStack, ix+1, iy-2, ix+2, iy+5, a<<24 | base, getBlitOffset());
				
				setBlitOffset(getBlitOffset() -2);
			}
			
			setBlitOffset(getBlitOffset() - 2);
		}
		
	}
	
	private boolean isParentAvailable(Research res)
	{
		return page.getEntries().contains(res);
	}
	
	private void drawLine(MatrixStack matrixStack, int x1, int y1, int x2, int y2, int color)
	{
		/*int n =(int) Math.round( Math.sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) ) / 10 );
		n = Math.min(2, n);
		
		int dx = (x2-x1)/n;
		int dy = (y2-y1)/n;
		
		int lx = x1;
		int ly = y1;
		
		for(int i=1;i<n;i++)
		{
			int x3 = x1 + dx*i - r.nextInt(7) + r.nextInt(7);
			int y3 = y1 + dy*i - r.nextInt(7) + r.nextInt(7);
			drawLine2( lx, ly, x3, y3, color);
			
			lx = x3;
			ly = y3;
			
			drawLine2( lx, ly, x2, y2, color);
		}*/
		
		drawLine2( matrixStack, x1, y1, x2, y2, color);
	}
	
	private void drawLine2(MatrixStack matrixStack, int x1, int y1, int x2, int y2, int color)
	{	
		if(x2-x1 > y2-y1)
		{
			this.hLine(matrixStack, x1, x2, y1, color);
			this.vLine(matrixStack, x2, y1, y2, color);	
		}
		else
		{
			this.hLine(matrixStack, x1, x2, y2, color);
			this.vLine(matrixStack, x1, y1, y2, color);
		}
	}
	
	private void drawBorder(MatrixStack matrixStack)
	{
		GL11.glColor4f(1F, 1F, 1F, 1F);
		this.setBlitOffset(210);
		this.minecraft.getTextureManager().bind(border);
		int x = (this.width - 256) / 2;
		int y = (this.height - 256) / 2;
		this.blit(matrixStack, x, y, 0, 0, 256, 256);
		this.setBlitOffset(0);
	}

	private void drawHighlights(MatrixStack matrixStack, int x, int y, Research r)
	{
		ITextComponent[] main = getResearchLines(r);	
		//blit level is 300
		//RenderSystem.disableDepthTest();
		//setBlitOffset(300);
        RenderSystem.enableDepthTest();        
        
        
		RenderHelper.turnOff();
		
		RenderSystem.enableDepthTest();
		
		FontRenderer font = Minecraft.getInstance().font;
		
		int w=0;
		for(int i=0;i<main.length;i++)
		{
			String s = main[i].getString();
			if(s==null)
				s = "null";
			if(s.startsWith(PartRenderer.autokratisch_aurisch))
			{
				s = s.substring(PartRenderer.autokratisch_aurisch.length());
				s = HelperComponent.toKryptikMessage(s);
				StringTextComponent tc = new StringTextComponent(s);
				tc.setStyle(Style.EMPTY.withFont(HelperComponent.getAutokratischFont()));
				main[i] = tc;
			}
		}

		//renderGradientBox(x, y, w, h, blit);
		
		/*
		for(int i=0;i<text.length;i++)
		{
			RenderSystem.pushMatrix();
			
			RenderSystem.translatef(0, 0, blit);
			
			renderers[i].drawString(matrixStack, text[i], x+1, y+1-(h/2)+i*font.FONT_HEIGHT, 0xffffffff);
				
			RenderSystem.popMatrix();
		}*/
        
        PartRenderer.drawHoveringTextFixed(matrixStack, Arrays.asList(main), x - 2, y + 10, -1, font);
        
		//renderTooltip(Arrays.asList(main), x, y);
	}

	private ITextComponent[] getResearchLines(Research r)
	{
		boolean a = this.minecraft.options.advancedItemTooltips;
		ITextComponent title = getResearchText(r);
		ITextComponent[] array = new ITextComponent[r.getGrandparents().length + (a ? 2 : 1)];
		array[0]=title;
		for(int i=0;i<r.getGrandparents().length;i++)
		{
			ResourceLocation res = r.getGrandparents()[i];
			Advancement adv = this.minecraft.player.connection.getAdvancements().getAdvancements().get(res);//ClientAdvancementManager//AdvancementList//ResourceLocation ->Advancement
			if(adv!=null && adv.getChatComponent()!=null)
				array[i+1] = new TranslationTextComponent("research.need.achievement", adv.getChatComponent().getString());
			else
				array[i+1] = new StringTextComponent("You dont know this advancement yet");
		}
		
		if(a)
		{
			array[array.length-1] = new StringTextComponent(TextFormatting.DARK_GRAY + r.getDomain() + ":" + r.getName());
		}
		
		return array;
	}
	
	private ITextComponent getResearchText(Research r)
	{
//		if(custom.hasResearch(r))
//		{
//			return r.getLocalizedName();
//		}
		
		String s = r.getLocalizedName().getString();
		if(!custom.hasResearch(r))
		{
//			StringBuilder build = new StringBuilder(s.length());
//			build.append(PartRenderer.autokratisch);//a7 = §
//			for(char c : s.toCharArray())
//			{
//				build.append(Integer.toHexString(c % 0xf));
//			}
//			s = build.toString();
//			
			s = PartRenderer.autokratisch_aurisch + s;
		}
		
		if(!custom.canResearch(r))
		{
			s = "???";
		}
		return new StringTextComponent(s);
	}
	
	private void connectToHole(int x, int y)
	{
		//if(true)
		//	return;
		/*
		x += 12;
		y += 12;
		
		int rx = (width - 224)/2;
		int ry = (height - 224)/2;
		float bx = rx + posX + blackHoleX;
		float by = ry + posY + blackHoleY;
		
		float dx = -x +bx;
		float dy = -y +by;
		int length = (int) Math.sqrt(dx*dx + dy*dy);
		
		GlStateManager.disableTexture();
        GlStateManager.enableBlend();
        GlStateManager.disableAlphaTest();
        GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA.param, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA.param, GlStateManager.SourceFactor.ONE.param, GlStateManager.DestFactor.ZERO.param);
        GlStateManager.shadeModel(7425);
		
		GL11.glColor4f(0.3F, 0F, 0.5F, 0.5F);
		GlStateManager.disableTexture();
		GL11.glBegin(GL11.GL_LINE_STRIP);
		
		Vector3d dis = new Vector3d(dx, dy, 0);
		Vector3d achse = new Vector3d(0D,0D,1D);
		
		Vector3d rot = dis.crossProduct(achse).normalize();
		
		float db = 0.75F / length;
		
		for(int i=0;i<length;i++)
		{
			float xx = x + dx/length * i;
			float yy = y + dy/length * i;
			
			double pos = (double)i/(double)length * Math.PI * 2D - (System.currentTimeMillis()%10000)/10000D*Math.PI*2 + length;
			double sin  = Math.sin(pos) * 40 * Math.sin( (double)i/(double)length *Math.PI * 0.5) ;
			if(dy<0)
			{
				sin = -sin;
			}
			
			xx += sin * rot.x;
			yy += sin * rot.y;
			
			GL11.glColor4f(0.3F, 0F, 0.5F, 0.5F -db*i);
			
			GL11.glVertex3f(xx, yy, 0F);
		}
		GL11.glColor4f(0F, 0F, 0F, 0F);
		GL11.glVertex3f(bx, by, 0F);
		
		GL11.glEnd();
		
		GlStateManager.shadeModel(7424);
		GlStateManager.disableBlend();
		GlStateManager.enableAlphaTest();
		GlStateManager.enableTexture();*/
	}
	
	
	private void connectAround(MatrixStack matrixStack, int x, int y, int px, int py, ResearchPage page)
	{
		int hx = (x+px)/2;
		int hy = (y+py)/2;
		
		Random r = new Random(page.id);
		int color = 0xFF303030;
		color |= r.nextInt(0xFFFFFF);
		
		drawLine(matrixStack, x, y, hx, hy, color);
	
		fill(matrixStack, hx-1, hy-1, hx+2, hy+2, color);
		fill(matrixStack, hx, hy, hx+1, hy+1, 0xFF000000);
	}
	
	@Override
	public boolean charTyped(char typedChar, int keyCode)
	{
		if(typedChar=='-')
		{
			scale -= 0.25F;
			scale = Math.max(scale, 0.5F);
			checkBorders();
			return true;
		}
		if(typedChar=='+')
		{
			scale += 0.25F;
			scale = Math.min(scale, 1.5F);
			checkBorders();
			return true;
		}
		
		return super.charTyped(typedChar, keyCode);
	}
	
	public void checkBorders()
	{
		int borderW = 0;
		int borderH = 0;
		int borderW2 = (int) (-224/scale +bgw);			
		//int borderH2 = (int)(-224/scale + 380);
		int borderH2 = (int)(-224/scale + bgh);
		
		posX = posX < -borderW2 ? -borderW2 : posX > borderW ? borderW : posX; 
		posY = posY < -borderH2 ? -borderH2 : posY > borderH ? borderH : posY; 
	}
	
	public void updatePlayerData(CustomPlayerData data)
	{
		custom = data;
	}
}
