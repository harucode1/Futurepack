package futurepack.common.gui.escanner;

import java.util.Arrays;
import java.util.function.Supplier;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import futurepack.api.Constants;
import futurepack.common.gui.PartRenderer;
import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.IGuiEventListener;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.AbstractButton;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistries;

public abstract class GuiScannerBase extends Screen
{
	protected ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/escanner.png");
	
	private ResourceLocation info = new ResourceLocation(Constants.MOD_ID, "textures/gui/info.png");
	private ResourceLocation lupe = new ResourceLocation(Constants.MOD_ID, "textures/gui/lupe2.png");
	private ResourceLocation rechner = new ResourceLocation(Constants.MOD_ID, "textures/gui/rechner.png");
	private ResourceLocation paper = new ResourceLocation(Constants.MOD_ID, "textures/gui/paper.png");
	
	//Maximal fl�sche 120 x 142
	// 105 x 140
	
	protected int guiX, guiY;
	
	protected int textureWidth = 187, textureHeight = 228;
	
	double storedNum = 0;
	String Num = "0";
	// 1 +; 2 - ; 3 * ; 4 / ;
	int operation = -1;
	
	private static final float[] colorNormal = new float[]{0F, 0F, 0F};
	private static final float[] colorHover = new float[]{1F, 1F, 0.6F};
	private static final float[] colorSelected = new float[]{1F, 1F, 1F};
	private static final float[] colorDisabled = new float[]{0.5F, 0.5F, 0.5F};
	
	private Screen befor;
	private Screen[] buffer;
	
	protected boolean insideDungeon;
	protected boolean drawButtons = true;
	
	public GuiScannerBase(Screen befor)
	{
		super(new TranslationTextComponent("gui.escanner"));
		this.befor = befor;
		buffer = new Screen[4];
		buffer[this.getIconIndex()] = this;
		filler();
		insideDungeon = false;
	}
	
	
	public void add(GuiScannerBase base)
	{
		buffer[base.getIconIndex()] = base;
	}
	
	public Screen getPreviosGUI()
	{
		return befor;
	}
	
	private void filler()
	{
		if(buffer[0]==null)
		{
			buffer[0] = new GuiResearchMainOverview();
		}
		if(buffer[3]==null)
		{
			buffer[3] = new GuiScannerRechner(this.befor);
		}
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		this.renderBackground(matrixStack);
		
		//Background
		this.minecraft.getTextureManager().bind(res);	
		RenderSystem.color4f(1F, 1F, 1F, 1F);	
		PartRenderer.drawQuadWithTexture(matrixStack, res, guiX, guiY, 0, 0, textureWidth, textureHeight, textureWidth, textureHeight, 256, 256, PartRenderer.ZLEVEL_BACKGROUND);
		
		//Buttons
		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}
	
	@Override
	public boolean charTyped(char typedChar, int keyCode)
	{
		if (keyCode == 1)
		{
			this.minecraft.setScreen(befor);
					
			if(befor instanceof GuiResearchMainOverviewBase)
			{
				((GuiResearchMainOverviewBase)befor).initBuffer(buffer);
			}
			return true;
		}
		return super.charTyped(typedChar, keyCode);
	}
	
	/**
	 * possabylitys: 0=Page, 1=Search, 2=List, 3=rechner;
	 */
	public abstract int getIconIndex();
	
	protected static boolean isHovering(double mx, double my, int x1, int y1, int x2, int y2)
	{
		return HelperComponent.isInBox(mx, my, x1, y1, x2, y2);
	}
	
	public void init(Screen[] buf)
	{
		this.buffer = buf;
		this.buffer[this.getIconIndex()] = this;
		filler();
	}
	
	public GuiResearchMainOverviewBase getResearchGui()
	{
		return (GuiResearchMainOverviewBase) this.buffer[0];
	}
	
	@Override
	public void init()
	{
		super.init();
		guiX = (width - textureWidth) / 2;
		guiY = (height - textureHeight) / 2;	
		insideDungeon = scanDungeon();
//		if(insideDungeon)
//		{
//			Arrays.fill(buffer, null);
//		}
		this.children.add(new IGuiEventListener()//Close button
		{
			@Override
			public boolean mouseReleased(double x, double y, int button)
			{
				if(button == 0)
				{
					if(isHovering(x, y, guiX+10, guiY+53, guiX+10+31, guiY+53+38))
					{
						Minecraft.getInstance().setScreen(befor);
						return true;
					}
				}
				return false;
			}
		});
		
		if(drawButtons)
		{
			int[][] buttons = new int[][]{ {33,107}, {33,127}, {33,146}, {33,164} };
			ResourceLocation[] tex = new ResourceLocation[]{info,lupe,paper,rechner};
			
			for(int j=0;j<buttons.length;j++)
			{
				Buttons b = this.addButton(new Buttons(j, guiX + buttons[j][0], guiY + buttons[j][1], tex[j], "scanner.button."+j+".name"));
				b.selected = (j == getIconIndex());
				final int G = j;
				b.toDisplay = () -> buffer[G];
			}
		}
	}

	@Override
	public boolean isPauseScreen()
	{
		return false;
	}
	
	private final Block QUANTANIUM = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(Constants.MOD_ID, "quantanium"));
	
	private boolean scanDungeon()
	{
		BlockPos pos = this.minecraft.player.blockPosition();
		int r = 15;
		for(int x=-r;x<r;x++)
		{
			for(int y=-r;y<r;y++)
			{
				for(int z=-r;z<r;z++)
				{
					BlockState state = this.minecraft.level.getBlockState(pos.offset(x,y,z));
					if(state.getBlock() == QUANTANIUM)
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public class Buttons extends AbstractButton
	{
		public final ResourceLocation res;
		public Supplier<Screen> toDisplay;
		public boolean selected;
		
		public Buttons(int x, int y, int widthIn, int heightIn, String buttonText, ResourceLocation texture)
		{
			super(x, y, widthIn, heightIn,new TranslationTextComponent(buttonText));
			res = texture;
		}
		
		public Buttons(int buttonId, int x, int y, int widthIn, int heightIn, ResourceLocation texture)
		{
			this(x, y, widthIn, heightIn, null, texture);
		}
		
		public Buttons(int buttonId, int x, int y, ResourceLocation texture, String buttonText)
		{
			this(x, y, 13, 13, buttonText, texture);
		}
		
		@Override
		public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
		{
			if (this.visible)
			{
				this.isHovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
				
				RenderSystem.enableBlend();
				RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA.value, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA.value);
				
				float[] col;
				if(toDisplay.get()==null)
				{
					col=colorDisabled;
				}
				else
				{
					col = colorNormal;
					if(selected)
						col = colorSelected;
					if(isHovered)
						col = colorHover;
				}
				RenderSystem.color4f(col[0], col[1], col[2], 1F);
				
				PartRenderer.drawQuadWithTexture(matrixStack, res, this.x, this.y, 0, 0, width, height, width, height, width, height, PartRenderer.ZLEVEL_BACKGROUND + 1);
				
				RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
				
				if(isHovered)
				{
					PartRenderer.drawHoveringTextFixed(matrixStack, Arrays.asList(this.getMessage()), mouseX, mouseY, -1, font);
				}
			}
		}
		
		@Override
		public void onPress() 
		{
			
		}
		
		@Override
		public void onRelease(double mouseX, double mouseY) 
		{
			if(!selected && toDisplay!=null)
			{
				Screen gui = toDisplay.get();
				GuiScannerBase.this.openGui(gui);
			}
		}
	}
	
	public void openGui(Screen gui)
	{
		if(gui!=null)
		{
			this.minecraft.setScreen(gui);
			
			if(gui instanceof GuiScannerBase)
			{
				((GuiScannerBase)gui).init(buffer);
				((GuiScannerBase)gui).befor = GuiScannerBase.this.befor;
			}
			if(gui instanceof GuiResearchMainOverviewBase)
			{
				((GuiResearchMainOverviewBase)gui).initBuffer(buffer);
			}
		}
	}
}
