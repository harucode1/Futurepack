package futurepack.common.gui.escanner;

import futurepack.common.research.ScannerPageResearch;
import net.minecraft.client.gui.screen.Screen;

public class GuiScannerPageResearch extends GuiScannerPageBase
{

	public GuiScannerPageResearch(String name, Screen bef)
	{
		super(ScannerPageResearch.getComponetsFromName(name), bef, true);
	}

	@Override
	public int getIconIndex()
	{
		return 2;
	}
}
