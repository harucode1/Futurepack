package futurepack.common.gui.escanner;

import java.util.ArrayList;
import java.util.Arrays;

import futurepack.common.research.ScannerPageResearch;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.util.text.event.HoverEvent.Action;

public class GuiScannerPageInfo extends GuiScannerPageBase
{	
	public GuiScannerPageInfo(boolean showInDungeon, ITextComponent... chat)
	{
		super(convert(chat), null, showInDungeon);
	}
	
	protected static IGuiComponent[] convert(ITextComponent[] chat)
	{
		ArrayList<IGuiComponent> list = new ArrayList<IGuiComponent>(chat.length);
		for(ITextComponent co : chat)
		{
			if(co!=null)
			{
				if(co.getStyle()!=null)
				{
					Style s = co.getStyle();
					if(s.getHoverEvent()!=null)
					{
						HoverEvent he = s.getHoverEvent();
						if(he.getAction() == Action.SHOW_TEXT && he.getValue(Action.SHOW_TEXT) != null)
						{
							String value = he.getValue(Action.SHOW_TEXT).getString();
							if(value!=null && value.startsWith("load_research="))
							{
								String research_name = value.split("=")[1];
								list.addAll(Arrays.asList(ScannerPageResearch.getComponetsFromName(research_name)));
								continue;
							}
						}
					}
				}
				
				list.add(new ComponentInteractiveText((IFormattableTextComponent) co));
			}			
		}
		return list.toArray(new IGuiComponent[list.size()]);
	}
	
	@Override
	public int getIconIndex()
	{
		return 1;
	}
}
