package futurepack.common.gui.escanner;

import java.util.List;

import com.google.gson.JsonObject;
import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperJSON;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;

public class ComponentPartpress extends ComponentBase
{
	
	List<ItemStack> in;
	List<ItemStack> out;

	public ComponentPartpress(JsonObject obj)
	{
		super(obj);
		JsonObject jo = obj.get("slots").getAsJsonObject();
		in = HelperJSON.getItemFromJSON(jo.get("in"), true);
		out = HelperJSON.getItemFromJSON(jo.get("out"), true);
		HelperJSON.setupRendering(in);
		HelperJSON.setupRendering(out);
	}

	@Override
	public void init(int maxWidth, Screen gui)
	{
		
	}

	@Override
	public int getWidth()
	{
		return 80;
	}

	@Override
	public int getHeight()
	{
		return 44;
	}

	@Override
	public void render(MatrixStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		RenderHelper.turnOff();
		
		HelperComponent.drawBackground(matrixStack, x, y, this);
		
		ItemStack out = HelperComponent.getStack(this.out);
		ItemStack in = HelperComponent.getStack(this.in);
		
		HelperComponent.renderFire(matrixStack, x+5, y + 23, blitOffset + 1);
		HelperComponent.renderPartPress(matrixStack, x + 31, y + 5, blitOffset + 1);
		
		RenderHelper.turnBackOn();
		
		HelperComponent.renderItemStackWithSlot(matrixStack, in, x+5, y + 5, blitOffset + 1);
		HelperComponent.renderItemStackWithSlot(matrixStack, out, x+57, y + 5, blitOffset + 1);
		
		RenderHelper.turnOff();
	}

	@Override
	public void postRendering(MatrixStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		ItemStack out = HelperComponent.getStack(this.out);
		ItemStack in = HelperComponent.getStack(this.in);
		
		HelperComponent.renderItemText(matrixStack, in, x+5, y + 5, mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, out, x+57, y + 5, mouseX, mouseY, gui);
		
	}

	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		if(mouseButton==0)
		{
			if(HelperComponent.isInBox(mouseX, mouseY, x+6, y+6, x+6+16, y+6+16))
			{
				ItemStack out = HelperComponent.getStack(this.in);
				HelperComponent.researchItem(out, gui.getResearchGui());
			}
			else if(HelperComponent.isInBox(mouseX, mouseY, x+58, y+6, x+58+16, y+6+16))
			{
				ItemStack in = HelperComponent.getStack(this.out);
				HelperComponent.researchItem(in, gui.getResearchGui());
			}
		}
	}
}
