package futurepack.common.gui.escanner;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import futurepack.common.gui.PartRenderer;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.client.gui.screen.Screen;

public abstract class GuiScannerPageBase extends GuiScannerBase
{
	IGuiComponent[] components;
	
	protected int textWidth = 110; //start pos x:56 y:69
	protected int textHeight = 143;
	
	protected int textX = 59, textY = 69;
	
	protected int scrollIndex = 0; //in pixel
	protected int totalHeight = 1;
	
	protected boolean moving = false;
	protected boolean showInDungeon;
	
	public GuiScannerPageBase(IGuiComponent[] comps, Screen bef, boolean showInDungeon)
	{
		super(bef);
		components = comps;
		this.showInDungeon = showInDungeon;
	}
	
	@Override
	public void init()
	{
		super.init();
		
		if(insideDungeon && !showInDungeon)
		{
			components = new IGuiComponent[1];
			components[0] = new ComponentTransmission(ComponentTransmission.Transmission.s, textHeight-10);
		}
		totalHeight = 1;
		for(IGuiComponent com : this.components)
		{
			com.init(textWidth, this);
			com.setAdditionHeight(totalHeight);
			totalHeight += com.getHeight() + 5;
		}
	}
	
	@Override
	public boolean mouseScrolled(double mx, double my, double dw) 
	{
		if(dw>0)
		{
			if(scrollIndex > 0 )
			{
				scrollIndex-= this.font.lineHeight * 2.5F;
			}
		}
		else if(dw<0)
		{
			if(scrollIndex < totalHeight - textHeight)
			{
				scrollIndex+= this.font.lineHeight * 2.5F;
			}
		}
		if(scrollIndex > totalHeight - textHeight)
		{
			scrollIndex = totalHeight - textHeight;
		}
		if(scrollIndex < 0)
		{
			scrollIndex = 0;
		}
		return super.mouseScrolled(mx, my, dw);
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		if(font==null) //sometimes this cause crashes
			return;
		
		// Draws Background and Icons
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		RenderSystem.enableDepthTest();
		
		drawInsisibilityCloak(matrixStack);
		
		drawComponents(matrixStack, mouseX, mouseY);
		
		drawScrollBar(matrixStack, mouseX, mouseY);
		
		drawHover(matrixStack, mouseX, mouseY);
	}
	
	protected void drawComponents(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		for(IGuiComponent com : this.components)
		{
			if(isComponentVisible(com))
			{
				int xpos = guiX + textX;
				int ypos = guiY + textY + com.getAdditionHeight() - this.scrollIndex;
				
				xpos += (this.textWidth - com.getWidth()) / 2;
				
				com.render(matrixStack, xpos, ypos, PartRenderer.ZLEVEL_BACKGROUND + 10, mouseX, mouseY, this);
			}
		}	
	}
	
	protected void drawHover(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		for(IGuiComponent com : this.components)
		{
			if(isComponentVisible(com))
			{
				int xpos = guiX + textX;
				int ypos = guiY + textY + com.getAdditionHeight() - this.scrollIndex;
				
				xpos += (this.textWidth - com.getWidth()) / 2;
				
				boolean hover = isHovering(mouseX, mouseY, xpos, ypos, xpos+com.getWidth(), ypos+com.getHeight());
				com.postRendering(matrixStack, xpos, ypos, getBlitOffset(), mouseX, mouseY, hover, this);
			}
		}	
	}
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		for(IGuiComponent com : this.components)
		{
			if(isComponentVisible(com))
			{
				int xpos = guiX + textX;
				int ypos = guiY + textY + com.getAdditionHeight() - this.scrollIndex;
				
				xpos += (this.textWidth - com.getWidth()) / 2;
				
				boolean hover = isHovering(mouseX, mouseY, xpos, ypos, xpos+com.getWidth(), ypos+com.getHeight());
				if(hover)
				{
					com.onClicked(xpos, ypos, mouseButton, mouseX, mouseY, this);
				}
				
			}
		}	
		
		if(mouseButton == 0)
		{
			if(isHovering(mouseX, mouseY, guiX + textWidth+textX, guiY +textY, guiX + textWidth+textX +4, guiY +textY + 141) || moving)
			{
				moving = true;
				
				double h = mouseY - (guiY +textY);
				double pp = h / 141.0;
				this.scrollIndex = (int) (pp * (totalHeight-textHeight) );
				if(scrollIndex > totalHeight - textHeight)
				{
					scrollIndex = totalHeight - textHeight;
				}
				if(scrollIndex < 0)
				{
					scrollIndex = 0;
				}
			}
		}
		else
		{
			moving = false;
		}
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	protected boolean isComponentVisible(IGuiComponent comp)
	{
		int y = comp.getAdditionHeight() - this.scrollIndex + comp.getHeight();	
		return y > 0 && y < textHeight+ comp.getHeight();
	}
	
	/**
	 * Draws an Inisible Cloack for the Entrys
	 * @param matrixStack TODO
	 */
	protected void drawInsisibilityCloak(MatrixStack matrixStack)
	{
		this.setBlitOffset(200);
		fillGradient(matrixStack, 0, 0, this.guiX + textX, this.height, 0x00ffffff, 0x00ffffff);//complete left
		fillGradient(matrixStack, this.guiX + textWidth+textX, 0, this.width, this.height, 0x00ffffff, 0x00ffffff);//right side
		fillGradient(matrixStack, this.guiX + textX, 0, this.guiX +  textWidth+textX, this.guiY + textY, 0x00ffffff, 0x00ffffff);
		fillGradient(matrixStack, this.guiX + textX, this.guiY +  textHeight+textY, this.guiX +  textWidth+textX, this.height, 0x00ffffff, 0x00ffffff);
		this.setBlitOffset(0);
	}
	
	protected void drawScrollBar(MatrixStack matrixStack, int mx, int my)
	{
		if(totalHeight>textHeight)
		{
			this.minecraft.getTextureManager().bind(res);
			
			int length = Math.min(141,Math.max(4, (textHeight-2) * (textHeight-2)/totalHeight ));
			int pos = (textHeight-2-length) * scrollIndex/(totalHeight-(textHeight));
			
			RenderSystem.color4f(1F,1F,1F,1F);
			
			PartRenderer.drawQuadWithTexture(matrixStack, res, guiX + textWidth + textX, guiY + textY + pos, 187, 1, 4, length-2, 4, length-2, 256, 256, 210);
			PartRenderer.drawQuadWithTexture(matrixStack, res, guiX + textWidth + textX, guiY + textY + pos + length -2, 187, 140, 4, 2, 4, 2, 256, 256, 210);
			
			this.setBlitOffset(0);
		}
	}
}
