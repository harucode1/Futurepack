package futurepack.common.gui.escanner;

import java.util.Iterator;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.text.StringTextComponent;

public class ComponentHeading extends ComponentText
{

	public ComponentHeading(String text)
	{
		super(new StringTextComponent(text));
		
	}

	@Override
	public void init(int maxWidth, Screen gui)
	{
		width = maxWidth;
		
		font = gui.getMinecraft().font;	
//		StringTextComponent tc = new StringTextComponent(rawText);
		rawText.setStyle(rawText.getStyle().withFont(null));
		
		parts = font.split(rawText, width);
		height = font.lineHeight * parts.size();
	}
	
	@Override
	public void render(MatrixStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		Iterator<IReorderingProcessor> it = parts.iterator();
		for(;it.hasNext();y+= font.lineHeight)
		{
			font.draw(matrixStack, it.next() , x, y, 0x547bb1);
		}
	}

}
