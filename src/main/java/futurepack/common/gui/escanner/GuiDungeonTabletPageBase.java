package futurepack.common.gui.escanner;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class GuiDungeonTabletPageBase extends GuiScannerPageBase 
{

	public GuiDungeonTabletPageBase(ITextComponent... chat)
	{
		this(GuiScannerPageInfo.convert(chat), null);
	}
	
	public GuiDungeonTabletPageBase(IGuiComponent[] comps, Screen bef) 
	{
		super(comps, bef, true);
		
		res = new ResourceLocation(Constants.MOD_ID, "textures/gui/dungeon_tablet.png");
		textX = 25;
		textY = 31;
		textWidth = 171;
		textHeight = 176;
		
		textureWidth=223;
		textureHeight=229;
		
		drawButtons = false;
	}

	@Override
	public int getIconIndex() 
	{
		return 2;
	}
	
	protected void drawScrollBar(MatrixStack matrixStack, int mx, int my)
	{
		if(totalHeight>textHeight)
		{
			this.minecraft.getTextureManager().bind(res);
			this.setBlitOffset(200);
			
			int length = Math.min(141,Math.max(4, (textHeight-2) * (textHeight-2)/totalHeight ));
			int pos = (textHeight-2-length) * scrollIndex/(totalHeight-(textHeight));
			
			GL11.glColor4f(1F,1F,1F,1F);
			if(length > 4)
			{
				this.blit(matrixStack, guiX +textWidth+textX, guiY +textY + pos, 241, 0, 4, length-4);
				this.blit(matrixStack, guiX +textWidth+textX, guiY +textY + pos+length -4, 241, 207, 4, 4);//the small dot
			}
			else
			{
				this.blit(matrixStack, guiX +textWidth+textX, guiY +textY + pos, 250, 2, 4, 4);
			}
			this.setBlitOffset(0);
		}
	}

}
