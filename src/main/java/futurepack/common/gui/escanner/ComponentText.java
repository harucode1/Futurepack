package futurepack.common.gui.escanner;

import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import futurepack.common.FPConfig;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.Style;

public class ComponentText implements IGuiComponent
{
	private int ah;
	
	protected int height;

	protected int width;
	
	protected IFormattableTextComponent rawText;
	protected List<IReorderingProcessor> parts;
	
	protected FontRenderer font;
	
	public ComponentText(IFormattableTextComponent text)
	{
		rawText = text;
		if(rawText instanceof IFormattableTextComponent)
		{
			Style s = text.getStyle();
			if(s==null)
			{
				s = Style.EMPTY;
			}
			if(FPConfig.CLIENT.allowUnicodeFont.get())
				s = s.withFont(HelperComponent.getUnicodeFont());
			
			((IFormattableTextComponent) rawText).setStyle(s);
		}
		
	}
	
	@Override
	public void init(int maxWidth, Screen gui)
	{
		width = maxWidth;
		font = gui.getMinecraft().font;
		parts = font.split(rawText, width);
		height = font.lineHeight * parts.size();
	}

	@Override
	public int getAdditionHeight()
	{
		return ah;
	}

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public int getHeight()
	{
		return height;
	}

	@Override
	public void render(MatrixStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		RenderSystem.color4f(1f, 1f, 1f, 1f);

		int start = 0;
		if(y<0)
		{
			start = (-y) / font.lineHeight;
		}
		for(int i=start;i<parts.size();i++)
		{
			int yy = y + i*font.lineHeight;	
			if(yy > gui.height)
				break;
			font.draw(matrixStack, parts.get(i) , x, yy, 0x000000);	
		}	
	}
	
	@Override
	public void setAdditionHeight(int additionHight)
	{
		ah = additionHight;
	}

	@Override
	public void postRendering(MatrixStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		
	}

	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		
	}

}
