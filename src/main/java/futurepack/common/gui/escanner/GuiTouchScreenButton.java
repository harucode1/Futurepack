package futurepack.common.gui.escanner;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;


public class GuiTouchScreenButton extends Button
{

	public GuiTouchScreenButton(int x, int y, int widthIn, int heightIn, ITextComponent buttonText, Button.IPressable onPress)
	{
		super(x, y, widthIn, heightIn, buttonText, onPress);
	}
	
	public GuiTouchScreenButton(int x, int y, int widthIn, int heightIn, String buttonText, Button.IPressable onPress)
	{
		this(x,y,widthIn, heightIn, new StringTextComponent(buttonText), onPress);
	}
	

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		if (this.visible)
		{
			Minecraft mc = Minecraft.getInstance();
			FontRenderer font = mc.font;
			mc.getTextureManager().bind(WIDGETS_LOCATION);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.isHovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
            int k = this.getYImage(this.isHovered);
            RenderSystem.enableBlend();
           // GlStateManager.blendFuncSeparate(770, 771, 1, 0);
            RenderSystem.blendFunc(770, 771);
            
            int c0 = isHovered?0xffa5eeff:0xff99d9ea;
            int c1 = isHovered?0xffe3ffff:0xffcfeef5;
            int c2 = isHovered?0xff779ecf:0xff7092be;
            
            this.setBlitOffset(100);
            RenderSystem.enableDepthTest();
            AbstractGui.fill(matrixStack, this.x, this.y, this.x+this.width, this.y+this.height, c0);
            AbstractGui.fill(matrixStack, this.x, this.y, this.x+this.width-1, this.y+this.height-1, c1);
            AbstractGui.fill(matrixStack, this.x+1, this.y+1, this.x+this.width, this.y+this.height, c2);
            AbstractGui.fill(matrixStack, this.x+1, this.y+1, this.x+this.width-1, this.y+this.height-1, c0);
            
//          this.blit(matrixStack, this.xPosition, this.yPosition, 0, 46 + k * 20, this.width / 2, this.height);
//          this.blit(matrixStack, this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + k * 20, this.width / 2, this.height);
            
//          this.mouseDragged(mc, mouseX, mouseY);
           
            
            int l = 0;

           /* if (packedFGColor != 0)
            {
                l = packedFGColor;
            }
            else*/
            if (!this.active)
            {
                l = 10526880;
            }
            else if (this.isHovered)
            {
                l = 0xff777777;
            }

//            this.drawCenteredString(matrixStack, font, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
            font.draw(matrixStack, this.getMessage().getString(), ((this.x + this.width / 2)-font.width(this.getMessage()) / 2), this.y + (this.height - 8) / 2, l);
        }
	}
}
