package futurepack.common.gui.escanner;

import futurepack.common.research.Research;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.text.TranslationTextComponent;

public abstract class GuiResearchMainOverviewBase extends Screen
{

	protected GuiResearchMainOverviewBase(String titleIn) 
	{
		super(new TranslationTextComponent(titleIn));
	}

	public abstract void initBuffer(Screen[] buffer);

	public abstract void openResearchText(Research r);

}
