package futurepack.common.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import futurepack.api.Constants;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IFluidTankInfo;
import futurepack.api.interfaces.tilentity.ITileXpStorage;
import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.ITextProperties;
import net.minecraft.util.text.LanguageMap;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.event.RenderTooltipEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.gui.GuiUtils;

public class PartRenderer
{
	private static ResourceLocation resEnergyBarTex = new ResourceLocation(Constants.MOD_ID, "textures/gui/energie_bar.png");
	
	public static int ZLEVEL_HOVER = 300;
	public static int ZLEVEL_BACKGROUND = 0;
	public static int ZLEVEL_ITEM = 100;
	public static int ZLEVEL_FP_BARS = 10;
	
	/* Core Helpers */
	
	public static void drawQuadWithTexture(MatrixStack matrixStack, ResourceLocation tex, int x, int y, float u, float v, int width, int height, int widthOnTexture, int heightOnTexture, int textureWidth, int textureHeight, int zLevel) 
	{
		Minecraft.getInstance().getTextureManager().bind(tex);
		AbstractGui.innerBlit(matrixStack, x, x + width, y, y + height, zLevel, widthOnTexture, heightOnTexture, u, v, textureWidth, textureHeight);
	}
	
	public static void drawHoveringTextFixedString(MatrixStack matrixStack, List<String> textLines, int mouseX,  int mouseY, int maxTextWidth, FontRenderer font)
	{
		drawHoveringTextFixed(matrixStack, ItemStack.EMPTY, textLines.stream().map(StringTextComponent::new).collect(Collectors.toList()), mouseX, mouseY, maxTextWidth, font);
	}
	
	public static void drawHoveringTextFixed(MatrixStack matrixStack, List<ITextComponent> textLines, int mouseX,  int mouseY, int maxTextWidth, FontRenderer font)
	{
		drawHoveringTextFixed(matrixStack, ItemStack.EMPTY, textLines, mouseX, mouseY, maxTextWidth, font);
	}
	
	/**
	 * See GuiUtils
	 * Fixed Version to re-enable Depth Test and State Validation
	 * @param matrixStack TODO
	 * @param stack Mainly for Forge Events, ItemStack.Empty is valid
	 * @param textLines	List of Text lines
	 * @param mouseX Current Mouse Pos
	 * @param mouseY Current Mouse Pos
	 * @param maxTextWidth Max Width of Tooltip, can be -1 for infinite
	 * @param font FontRenderer to use
	 * @param screenWidth Size of the Screen (Drawable area relative to Vanilla "Screen" origin)
	 * @param screenHeight Size of the Screen (Drawable area relative to Vanilla "Screen" origin)
	 */
    public static void drawHoveringTextFixed(MatrixStack mStack, @Nonnull final ItemStack stack, List<? extends ITextProperties> textLines, int mouseX,  int mouseY, int maxTextWidth, FontRenderer font)
    {
    	int screenWidth = Minecraft.getInstance().getWindow().getGuiScaledWidth();
    	int screenHeight = Minecraft.getInstance().getWindow().getGuiScaledHeight();
    	
    	int backgroundColor = GuiUtils.DEFAULT_BACKGROUND_COLOR, 
    			borderColorStart = GuiUtils.DEFAULT_BORDER_COLOR_START,
    			borderColorEnd = GuiUtils.DEFAULT_BORDER_COLOR_END;
    	
    	if (!textLines.isEmpty())
        {
            RenderTooltipEvent.Pre event = new RenderTooltipEvent.Pre(stack, textLines, mStack, mouseX, mouseY, screenWidth, screenHeight, maxTextWidth, font);
            if (MinecraftForge.EVENT_BUS.post(event))
                return;
            mouseX = event.getX();
            mouseY = event.getY();
            screenWidth = event.getScreenWidth();
            screenHeight = event.getScreenHeight();
            maxTextWidth = event.getMaxWidth();
            font = event.getFontRenderer();

            RenderHelper.turnOff();		//change
            RenderSystem.disableRescaleNormal();
            RenderSystem.enableDepthTest();					//change
            int tooltipTextWidth = 0;
            
            for (ITextProperties textLine : textLines)
            {
                int textLineWidth = font.width(textLine);
                if (textLineWidth > tooltipTextWidth)
                    tooltipTextWidth = textLineWidth;
            }

            boolean needsWrap = false;

            int titleLinesCount = 1;
            int tooltipX = mouseX + 12;
            if (tooltipX + tooltipTextWidth + 4 > screenWidth)
            {
                tooltipX = mouseX - 16 - tooltipTextWidth;
                if (tooltipX < 4) // if the tooltip doesn't fit on the screen
                {
                    if (mouseX > screenWidth / 2)
                        tooltipTextWidth = mouseX - 12 - 8;
                    else
                        tooltipTextWidth = screenWidth - 16 - mouseX;
                    needsWrap = true;
                }
            }

            if (maxTextWidth > 0 && tooltipTextWidth > maxTextWidth)
            {
                tooltipTextWidth = maxTextWidth;
                needsWrap = true;
            }

            if (needsWrap)
            {
                int wrappedTooltipWidth = 0;
                List<ITextProperties> wrappedTextLines = new ArrayList<>();
                for (int i = 0; i < textLines.size(); i++)
                {
                    ITextProperties textLine = textLines.get(i);
                    List<ITextProperties> wrappedLine = font.getSplitter().splitLines(textLine, tooltipTextWidth, Style.EMPTY);
                    if (i == 0)
                        titleLinesCount = wrappedLine.size();

                    for (ITextProperties line : wrappedLine)
                    {
                        int lineWidth = font.width(line);
                        if (lineWidth > wrappedTooltipWidth)
                            wrappedTooltipWidth = lineWidth;
                        wrappedTextLines.add(line);
                    }
                }
                tooltipTextWidth = wrappedTooltipWidth;
                textLines = wrappedTextLines;

                if (mouseX > screenWidth / 2)
                    tooltipX = mouseX - 16 - tooltipTextWidth;
                else
                    tooltipX = mouseX + 12;
            }

            int tooltipY = mouseY - 12;
            int tooltipHeight = 8;

            if (textLines.size() > 1)
            {
                tooltipHeight += (textLines.size() - 1) * 10;
                if (textLines.size() > titleLinesCount)
                    tooltipHeight += 2; // gap between title lines and next lines
            }

            if (tooltipY < 4)
                tooltipY = 4;
            else if (tooltipY + tooltipHeight + 4 > screenHeight)
                tooltipY = screenHeight - tooltipHeight - 4;

            final int zLevel = 400;
            RenderTooltipEvent.Color colorEvent = new RenderTooltipEvent.Color(stack, textLines, mStack, tooltipX, tooltipY, font, backgroundColor, borderColorStart, borderColorEnd);
            MinecraftForge.EVENT_BUS.post(colorEvent);
            backgroundColor = colorEvent.getBackground();
            borderColorStart = colorEvent.getBorderStart();
            borderColorEnd = colorEvent.getBorderEnd();

            mStack.pushPose();
            Matrix4f mat = mStack.last().pose();
            //TODO, lots of unnessesary GL calls here, we can buffer all these together.
            //done - all 1 GL call
            Tessellator tes = drawGradientRectPre();
            drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY - 4, tooltipX + tooltipTextWidth + 3, tooltipY - 3, backgroundColor, backgroundColor, tes);
            drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY + tooltipHeight + 3, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 4, backgroundColor, backgroundColor, tes);
            drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY - 3, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor, tes);
            drawGradientRect(mat, zLevel, tooltipX - 4, tooltipY - 3, tooltipX - 3, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor, tes);
            drawGradientRect(mat, zLevel, tooltipX + tooltipTextWidth + 3, tooltipY - 3, tooltipX + tooltipTextWidth + 4, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor, tes);
            drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY - 3 + 1, tooltipX - 3 + 1, tooltipY + tooltipHeight + 3 - 1, borderColorStart, borderColorEnd, tes);
            drawGradientRect(mat, zLevel, tooltipX + tooltipTextWidth + 2, tooltipY - 3 + 1, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3 - 1, borderColorStart, borderColorEnd, tes);
            drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY - 3, tooltipX + tooltipTextWidth + 3, tooltipY - 3 + 1, borderColorStart, borderColorStart, tes);
            drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY + tooltipHeight + 2, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3, borderColorEnd, borderColorEnd, tes);
            drawGradientRectPost(tes);
            
            
            MinecraftForge.EVENT_BUS.post(new RenderTooltipEvent.PostBackground(stack, textLines, mStack, tooltipX, tooltipY, font, tooltipTextWidth, tooltipHeight));

            IRenderTypeBuffer.Impl renderType = IRenderTypeBuffer.immediate(Tessellator.getInstance().getBuilder());
            mStack.translate(0.0D, 0.0D, zLevel);

            int tooltipTop = tooltipY;

            for (int lineNumber = 0; lineNumber < textLines.size(); ++lineNumber)
            {
                ITextProperties line = textLines.get(lineNumber);
                if (line != null)
                    font.drawInBatch(LanguageMap.getInstance().getVisualOrder(line), (float)tooltipX, (float)tooltipY, -1, true, mat, renderType, false, 0, 15728880);

                if (lineNumber + 1 == titleLinesCount)
                    tooltipY += 2;

                tooltipY += 10;
            }

            renderType.endBatch();
            mStack.popPose();

            MinecraftForge.EVENT_BUS.post(new RenderTooltipEvent.PostText(stack, textLines, mStack, tooltipX, tooltipTop, font, tooltipTextWidth, tooltipHeight));

            RenderSystem.enableDepthTest();
            RenderSystem.enableRescaleNormal();
        }
    }
	
    public static void drawGradientRect(Matrix4f mat, int zLevel, int left, int top, int right, int bottom, int startColor, int endColor, Tessellator tessellator)
    {
        float startAlpha = (float)(startColor >> 24 & 255) / 255.0F;
        float startRed   = (float)(startColor >> 16 & 255) / 255.0F;
        float startGreen = (float)(startColor >>  8 & 255) / 255.0F;
        float startBlue  = (float)(startColor       & 255) / 255.0F;
        float endAlpha   = (float)(endColor   >> 24 & 255) / 255.0F;
        float endRed     = (float)(endColor   >> 16 & 255) / 255.0F;
        float endGreen   = (float)(endColor   >>  8 & 255) / 255.0F;
        float endBlue    = (float)(endColor         & 255) / 255.0F;

        BufferBuilder buffer = tessellator.getBuilder();
        buffer.vertex(mat, right,    top, zLevel).color(startRed, startGreen, startBlue, startAlpha).endVertex();
        buffer.vertex(mat,  left,    top, zLevel).color(startRed, startGreen, startBlue, startAlpha).endVertex();
        buffer.vertex(mat,  left, bottom, zLevel).color(  endRed,   endGreen,   endBlue,   endAlpha).endVertex();
        buffer.vertex(mat, right, bottom, zLevel).color(  endRed,   endGreen,   endBlue,   endAlpha).endVertex();
    }
    
    @SuppressWarnings("deprecation")
    public static Tessellator drawGradientRectPre()
    {
        RenderSystem.enableDepthTest();
        RenderSystem.disableTexture();
        RenderSystem.enableBlend();
        RenderSystem.defaultBlendFunc();
        RenderSystem.shadeModel(GL11.GL_SMOOTH);

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuilder();
        buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
        return tessellator;
    }
    
    @SuppressWarnings("deprecation")
    public static void drawGradientRectPost(Tessellator tessellator)
    {
        tessellator.end();

        RenderSystem.shadeModel(GL11.GL_FLAT);
        RenderSystem.disableBlend();
        RenderSystem.enableTexture();
    }
	
	/* Feature Renderers */
    
	public static void renderNeon(MatrixStack matrixStack, int x, int y, INeonEnergyStorage engine, int mouseX, int mouseY)
	{
		//Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 0, 0, 11, 72, 11, 72, 256, 256, ZLEVEL_FP_BARS);
		
		int p = (int) ((float)engine.get() / (float)engine.getMax() * 72F);
		
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (72-p), 11, 0+(72-p), 11, p, 11, p, 256, 256, ZLEVEL_FP_BARS + 1);
	}
	
	public static void renderNeonTooltip(MatrixStack matrixStack, int guiLeft, int guiTop, int nx, int ny, INeonEnergyStorage engine, int mouseX, int mouseY)
	{
		if(HelperComponent.isInBox(mouseX, mouseY, guiLeft+nx, guiTop+ny, guiLeft+nx+11, guiTop+ny+72))
		{
			PartRenderer.drawHoveringTextFixedString(matrixStack, Arrays.asList(engine.get() + " / " + engine.getMax() + " NE"), mouseX-guiLeft, mouseY-guiTop, -1, Minecraft.getInstance().font);
		}
	}
	
	public static void renderSupport(MatrixStack matrixStack, int x, int y, ISupportStorage able, int mouseX, int mouseY)
	{
		//Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 0, 0, 11, 72, 11, 72, 256, 256, ZLEVEL_FP_BARS);
		
		int p = (int) ((float)able.get() / (float)able.getMax() * 72F);
		
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (72-p), 22, 0+(72-p), 11, p, 11, p, 256, 256, ZLEVEL_FP_BARS + 1);
	}
	
	public static void renderSupportTooltip(MatrixStack matrixStack, int guiLeft, int guiTop, int nx, int ny, ISupportStorage able, int mouseX, int mouseY)
	{
		if(HelperComponent.isInBox(mouseX, mouseY, guiLeft+nx, guiTop+ny, guiLeft+nx+11, guiTop+ny+72))
		{
			PartRenderer.drawHoveringTextFixedString(matrixStack, Arrays.asList(able.get()  + " / " + able.getMax() + " SP"), mouseX-guiLeft, mouseY-guiTop, -1, Minecraft.getInstance().font);
		}
	}
	
	public static void renderRedstoneFlux(MatrixStack matrixStack, int x, int y, int RF, int maxRF, int mouseX, int mouseY)
	{
		//Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 0, 0, 11, 72, 11, 72, 256, 256, ZLEVEL_FP_BARS);
		
		int p = (int) ((float)RF / (float)maxRF * 72F);
		
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (72-p), 114, 0+(72-p), 11, p, 11, p, 256, 256, ZLEVEL_FP_BARS + 1);
		
		if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+10, y+72))
		{
			drawHoveringTextFixedString(matrixStack, Arrays.asList(RF  + " / " + maxRF + " RF"), mouseX, mouseY, -1, Minecraft.getInstance().font);
		}
	}
	
	public static void renderExp(MatrixStack matrixStack, int x, int y, ITileXpStorage xp, int mouseX, int mouseY)
	{
		//Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 33, 0, 18, 66, 18, 66, 256, 256, ZLEVEL_FP_BARS);
		
		int p = (int) ((float)xp.getXp() / (float)xp.getMaxXp() * 66F);
			
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (66-p), 51, 0+(66-p), 18, p, 18, p, 256, 256, ZLEVEL_FP_BARS + 1);
		
		if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+18, y+66))
		{
			drawHoveringTextFixedString(matrixStack, Arrays.asList(xp.getXp() + "/" + (float)xp.getMaxXp() + " XP"), mouseX, mouseY, -1, Minecraft.getInstance().font);
		}
	}
	
	public static void renderTabBase(MatrixStack matrixStack, int x, int y, boolean right, int mouseX, int mouseY, boolean activated, int blitOffset)
	{
		//activated, hover, base; x+6 y+6 16x16
		
		int u = right ? 0 : 28;
		int v = 146;
		
		if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+28, y+28))
		{
			v = 118;
		}
		if(activated)
		{
			v = 90;
		}

		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, u, v, 28,  28, 28, 28, 256, 256, blitOffset);
	}
	
	/**
	 * Modes: 0 Black, 1 White, 2 Blue, 3 Yellow, 4 Green;
	 * @param matrixStack TODO
	 */
	public static void renderSmallBars(MatrixStack matrixStack, int x, int y, float f, int mode)
	{
		mode = mode % 5;
		Minecraft.getInstance().getTextureManager().bind(resEnergyBarTex);
		
		int w = 9;
		int h = 45;
		//Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 69, 0, w, h, w, h, 256, 256, ZLEVEL_FP_BARS);
		
		int p = (int) (f * h);
		
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (45-p), 69 + mode*w, 0+(45-p), w, p, w, p, 256, 256, ZLEVEL_FP_BARS + 1);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void renderFluidTank(int x, int y, int w, int h, IFluidTankInfo fluid, int mouseX, int mouseY, double zLvl)
	{
		if(fluid.isEmpty())
			return;
		
		Minecraft.getInstance().getTextureManager().bind(AtlasTexture.LOCATION_BLOCKS);
		
		AtlasTexture map = Minecraft.getInstance().getModelManager().getAtlas(AtlasTexture.LOCATION_BLOCKS);
		TextureAtlasSprite texture = map.getSprite(fluid.getFluidStack().getFluid().getAttributes().getStillTexture(fluid.getFluidStack()));
		
		int i = fluid.getFluidStack().getFluid().getAttributes().getColor(fluid.getFluidStack());
		
		int scale = 16;
		float fill =  (float)fluid.getFluidStack().getAmount() / (float)fluid.getCapacity();
		
		int fh = (int) (h*fill);

		float b = (i & 0xFF) / 255F;
		float g = ((i & 0xFF00) >> 8) / 255F;
		float r = ((i & 0xFF0000) >> 16) / 255F;
		GlStateManager._color4f(r, g, b, 1);
		drawTextureRect(x, y + h-fh, w, fh, texture, (float)w/scale, (float)fh/scale, zLvl);
		GlStateManager._color4f(1, 1, 1, 1);
	}
	
	public static void renderFluidTankTooltip(MatrixStack matrixStack, int x, int y, int w, int h, IFluidTankInfo fluid, int mouseX, int mouseY)
	{
        GlStateManager._disableLighting();
        GlStateManager._disableDepthTest();
        RenderHelper.turnOff();
		
		if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+w, y+h))
		{
			String s1 = fluid.getFluidStack().getDisplayName().getString();
			String s2 = TextFormatting.RESET + "" + fluid.getFluidStack().getAmount() + " / " + fluid.getCapacity() + " mB";
			
			PartRenderer.renderText(matrixStack, mouseX, mouseY, s1+"\n"+s2);
		}
		
        GlStateManager._enableLighting();
        GlStateManager._enableDepthTest();
        RenderHelper.turnBackOn();
	}
	
	private static void drawTextureRect(int x, int y, int w, int h, TextureAtlasSprite tex, float xCount, float yCount, double zLvl)
	{	
		int xc = (int) xCount;
		int yc = (int) yCount;
		double width = w / xCount;
		double height = h / yCount;		
		
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder vertexbuffer = tessellator.getBuilder();
		vertexbuffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX); //GL_QUADS = 7
			
		for(int i=0;i<xc;i++)
		{
			for(int j=0;j<yc;j++)
			{				
				fill(x +i*width, y + j*height, width, height, tex.getU0(), tex.getV0(), tex.getU1(), tex.getV1(), zLvl);
			}
		}	
		
		double lostV = ( yCount -yc);	
		double newY = y + yc*height;
		double newV = tex.getV0() + (tex.getV1()-tex.getV0())*lostV;
		double newH = height*lostV;
		
		for(int i=0;i<xc;i++)
		{
			fill(x +i*width, newY, width, newH, tex.getU0(), tex.getV0(), tex.getU1(), newV, zLvl);
		}	
		
		double lostU = (xCount -xc);
		double newX = x +xc*width;
		double newU = tex.getU0() + (tex.getU1()-tex.getU0())*lostU;
		double newW = width*lostU;
		
		for(int i=0;i<yc;i++)
		{
			fill(newX, y + i*height, newW, height, tex.getU0(), tex.getV0(), newU, tex.getV1(), zLvl);
		}	
		fill(newX, newY, newW, newH, tex.getU0(), tex.getV0(), newU, newV, zLvl);
		
		tessellator.end();
	}
	
	private static void fill(double xCoord, double yCoord, double w, double h, double minU, double minV, double maxU, double maxV, double zLvl)
	{
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder vertexbuffer = tessellator.getBuilder();   
		vertexbuffer.vertex(xCoord + 0, yCoord + h, zLvl).uv((float)minU, (float)maxV).endVertex();
		vertexbuffer.vertex(xCoord + w, yCoord + h, zLvl).uv((float)maxU, (float)maxV).endVertex();
		vertexbuffer.vertex(xCoord + w, yCoord + 0, zLvl).uv((float)maxU, (float)minV).endVertex();
		vertexbuffer.vertex(xCoord + 0, yCoord + 0, zLvl).uv((float)minU, (float)minV).endVertex();
	}
	
	@Deprecated
	public static void renderText(MatrixStack matrixStack, int x, int y, String string)
	{
		x += 12; 
		y += 5;
		renderHoverText(matrixStack, x, y, 450, string.split("\n"));
	}
	
	@Deprecated
	public static void renderText(MatrixStack matrixStack, int x, int y, ITextComponent string)
	{
		x += 12; 
		y += 5;
		renderHoverText(matrixStack, x, y, 450, Collections.singletonList(string));
	}


	
	@Deprecated
	public static void renderGradientBox(MatrixStack matrixStack, int x, int y, int w, int h, int blitOffset)
	{
		RenderSystem.disableBlend();
		fill(matrixStack, x-3, y-(h / 2)-2, x+w+3, y+(h / 2)+2, 0xff010111, blitOffset);
		fill(matrixStack, x-2, y-(h / 2)-3, x+w+2, y+(h / 2)+3, 0xff011101, blitOffset);
		fillGradient(x-2, y-(h / 2)-2, x+w+2, y+(h / 2)+2, 0xff330099, 0xff110055, blitOffset);
		fill(matrixStack, x-1, y-(h / 2)-1, x+w+1, y+(h / 2)+1, 0xff110101, blitOffset);
		GL11.glColor4f(1, 1, 1, 1);
	}
	
	
	public static void renderHoverText(MatrixStack matrixStack, int x, int y, int blit, List<ITextComponent> list)
	{
		renderHoverText(matrixStack, x, y, blit, list, Integer.MAX_VALUE);
	}
	
	public static final String autokratisch_aurisch = "\\u00a7#g";
	
	public static void renderHoverText(MatrixStack matrixStack, int x, int y, int blit, List<ITextComponent> list, int maxWidth)
	{
		RenderHelper.turnOff();
		
		RenderSystem.enableDepthTest();
		
		FontRenderer font = Minecraft.getInstance().font;
		ResourceLocation autokratischFont = HelperComponent.getAutokratischFont();
		
		int textWidth = 0;
		
		for(int i=0;i<list.size();i++)
		{
			ITextComponent text = list.get(i);
			String s = text.getString();
			if(s.startsWith(PartRenderer.autokratisch_aurisch))
			{
				s = s.substring(PartRenderer.autokratisch_aurisch.length());
				s = HelperComponent.toKryptikMessage(s);
				StringTextComponent tc = new StringTextComponent(s);
				tc.setStyle(text.getStyle().withFont(autokratischFont));
				text = tc;
				list.set(i, text);
			}
			textWidth = Math.max(font.width(text), textWidth);
		}
		
		int w = Math.min(textWidth, maxWidth) + 2;
		
		ArrayList<IReorderingProcessor> rp = new ArrayList<IReorderingProcessor>();
		for(ITextComponent tc : list)
			rp.addAll(font.split(tc, w-2));
		
		int h = font.lineHeight*rp.size()+1;

		renderGradientBox(matrixStack, x, y, w, h, blit);
		
		matrixStack.pushPose();
		matrixStack.translate(0, 0, blit);
		
		for(int i=0;i<rp.size();i++)
		{
			IReorderingProcessor wrappedText = rp.get(i);
			font.draw(matrixStack, wrappedText, x+1, y+1-(h/2)+i*font.lineHeight, 0xffffffff); // drawString
		}
		matrixStack.popPose();
	}
	
	@Deprecated
	public static void renderHoverText(MatrixStack matrixStack, int x, int y, int blit, String...textBase)
	{		
		renderHoverText(matrixStack, x, y, blit, Arrays.stream(textBase).map(StringTextComponent::new).collect(Collectors.toList()));
	}
	
	public static void fillGradient(int left, int top, int right, int bottom, int startColor, int endColor, int blitOffset)
	{
		float f = (startColor >> 24 & 255) / 255.0F;
		float f1 = (startColor >> 16 & 255) / 255.0F;
		float f2 = (startColor >> 8 & 255) / 255.0F;
		float f3 = (startColor & 255) / 255.0F;
		float f4 = (endColor >> 24 & 255) / 255.0F;
		float f5 = (endColor >> 16 & 255) / 255.0F;
		float f6 = (endColor >> 8 & 255) / 255.0F;
		float f7 = (endColor & 255) / 255.0F;
		GlStateManager._disableTexture();
		GlStateManager._enableBlend();
		GlStateManager._disableAlphaTest();
		GlStateManager._blendFuncSeparate(770, 771, 1, 0);
		GlStateManager._shadeModel(7425);
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder buf = tessellator.getBuilder();
		buf.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
		buf.vertex(right, top, blitOffset).color(f1, f2, f3, f);
		buf.vertex(left, top, blitOffset).color(f1, f2, f3, f);
		buf.vertex(left, bottom, blitOffset).color(f5, f6, f7, f4);
		buf.vertex(right, bottom, blitOffset).color(f5, f6, f7, f4);
		tessellator.end();
		GlStateManager._shadeModel(7424);
		GlStateManager._disableBlend();
		GlStateManager._enableAlphaTest();
		GlStateManager._enableTexture();
	}
	
	public static void fill(MatrixStack matrixStack, int left, int top, int right, int bottom, int color, int blitOffset)
	{
		matrixStack.translate(0, 0, blitOffset);
		AbstractGui.fill(matrixStack, left, top, right, bottom, color);
		matrixStack.translate(0, 0, -blitOffset);
	}
	

	
	public static void  drawTexturedModalRect(float x, float y, float textureX, float textureY, float width, float height, double blitOffset)
	{
		float f = 0.00390625F; // 1/256
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuilder();
		bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
		fill(x, y, width, height, textureX*f, textureY*f, (textureX+width)*f, (textureY+height)*f, blitOffset);
		tessellator.end();
	}
	
	public static void fill(double x0, double y0, double x1, double y1, int color, double blitOffset) 
	{
	      if (x0 < x1) {
	    	  double i = x0;
	         x0 = x1;
	         x1 = i;
	      }

	      if (y0 < y1) {
	    	  double j = y0;
	         y0 = y1;
	         y1 = j;
	      }

	      float f3 = (float)(color >> 24 & 255) / 255.0F;
	      float f = (float)(color >> 16 & 255) / 255.0F;
	      float f1 = (float)(color >> 8 & 255) / 255.0F;
	      float f2 = (float)(color & 255) / 255.0F;
	      Tessellator tessellator = Tessellator.getInstance();
	      BufferBuilder bufferbuilder = tessellator.getBuilder();
	      GlStateManager._enableBlend();
	      GlStateManager._disableTexture();
	      GlStateManager._blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA.value, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA.value, GlStateManager.SourceFactor.ONE.value, GlStateManager.DestFactor.ZERO.value);
	      bufferbuilder.begin(7, DefaultVertexFormats.POSITION_COLOR);
	      bufferbuilder.vertex(x0, y1, blitOffset).color(f, f1, f2, f3).endVertex();
	      bufferbuilder.vertex(x1, y1, blitOffset).color(f, f1, f2, f3).endVertex();
	      bufferbuilder.vertex(x1, y0, blitOffset).color(f, f1, f2, f3).endVertex();
	      bufferbuilder.vertex(x0, y0, blitOffset).color(f, f1, f2, f3).endVertex();
	      tessellator.end();
	      GlStateManager._enableTexture();
	      GlStateManager._disableBlend();
	   }
}
