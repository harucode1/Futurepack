package futurepack.common.gui;

import java.lang.reflect.Field;
import java.util.Optional;

import futurepack.common.block.inventory.TileEntityTechtable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.IRecipeHolder;
import net.minecraft.inventory.container.CraftingResultSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ICraftingRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.NonNullList;

public class SlotTechtableCrafting extends CraftingResultSlot
{
	private PlayerEntity thePlayer;
	private CraftingInventory craftMatrix;
	private TileEntityTechtable tech;
	
	public SlotTechtableCrafting(PlayerEntity player, CraftingInventory craftingInventory, TileEntityTechtable tech, int slotIndex, int xPosition, int yPosition)
	{
		super(player, craftingInventory, tech, slotIndex, xPosition, yPosition);
		thePlayer = player;
		craftMatrix = craftingInventory;
		this.tech = tech;
	}

	
	@Override
	public ItemStack onTake(PlayerEntity playerIn, ItemStack stack)
	{
		this.checkTakeAchievements(stack);
		net.minecraftforge.common.ForgeHooks.setCraftingPlayer(playerIn);
		Optional<ICraftingRecipe>  orecipe = thePlayer.level.getRecipeManager().getRecipeFor(IRecipeType.CRAFTING, this.craftMatrix, thePlayer.level);
		ICraftingRecipe rec = orecipe.get();
		NonNullList<ItemStack> aitemstack = rec.getRemainingItems(craftMatrix);
		net.minecraftforge.common.ForgeHooks.setCraftingPlayer(null);

		for (int i = 0; i < aitemstack.size(); ++i)
		{
			ItemStack itemstack1 = this.craftMatrix.getItem(i);
			ItemStack itemstack2 = aitemstack.get(i);

			if (!itemstack1.isEmpty())
			{
				boolean b = false;
				for(int j=10;j<tech.getContainerSize();j++)
				{
					ItemStack st1 = tech.getItem(j);
					if(!st1.isEmpty())
					{
						if(st1.sameItem(itemstack1) && ItemStack.tagMatches(st1, itemstack1))
						{
							tech.removeItem(j, 1);
							b = true;
							break;
						}
					}
				}
				this.craftMatrix.setItem(i, itemstack1);
				if(!b)
				{
					this.craftMatrix.removeItem(i, 1);
				} 
				
			}

			if (!itemstack2.isEmpty()) 
			{
				if (itemstack1.isEmpty()) 
				{
					this.craftMatrix.setItem(i, itemstack2);
				} 
				else if (ItemStack.isSame(itemstack1, itemstack2) && ItemStack.tagMatches(itemstack1, itemstack2)) 
				{
					itemstack2.grow(itemstack1.getCount());
					this.craftMatrix.setItem(i, itemstack2);
				}
				else if (!thePlayer.inventory.add(itemstack2)) 
				{
					thePlayer.drop(itemstack2, false);
				}
			 }
		}
		
		return stack;
	}
	
	@Override
	protected void checkTakeAchievements(ItemStack stack)
	{
		if (this.getAmount() > 0)
		{
			stack.onCraftedBy(this.thePlayer.level, this.thePlayer, this.getAmount());
			net.minecraftforge.fml.hooks.BasicEventHooks.firePlayerCraftingEvent(this.thePlayer, stack, craftMatrix);//EXACT COPY from vanilla (super), but with craftMatrix instead of this.inventory
		}

		setAmount(0);
		((IRecipeHolder)this.container).awardUsedRecipes(thePlayer);
	}
	
	private final static Field f_amound;
	static
	{
		Class<CraftingResultSlot> cls = CraftingResultSlot.class;
		
		f_amound = cls.getDeclaredFields()[2];
		f_amound.setAccessible(true);
	}
	
	private int getAmount()
	{
		try {
			return (int) f_amound.get(this);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private void setAmount(int amount)
	{
		try {
			f_amound.set(this, amount);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
