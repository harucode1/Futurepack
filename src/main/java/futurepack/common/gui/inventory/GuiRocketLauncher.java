package futurepack.common.gui.inventory;

import futurepack.api.Constants;
import futurepack.common.block.modification.TileEntityRocketLauncher;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;

public class GuiRocketLauncher extends GuiLaserEditor
{

	public GuiRocketLauncher(PlayerEntity pl, TileEntityRocketLauncher tile)
	{
		super(pl, tile);
		res = new ResourceLocation(Constants.MOD_ID, "textures/gui/turret_gui_munition.png");
		imageHeight = 204;
	}

}
