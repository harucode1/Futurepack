package futurepack.common.gui.inventory;

import java.util.ArrayList;
import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.common.block.multiblock.DeepCoreLogic;
import futurepack.common.block.multiblock.TileEntityDeepCoreMinerMain;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.SlotScrollable;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IContainerScrollable;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.CraftResultInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class GuiDeepCoreMiner extends GuiModificationBase<TileEntityDeepCoreMinerMain>
{

	public GuiDeepCoreMiner(PlayerEntity pl, TileEntityDeepCoreMinerMain tile)
	{
		super(new ContainerCoreMiner(pl.inventory , tile) ,"core_miner.png", pl.inventory);
		this.imageHeight = 184;
	}

	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		if(tile().getChipPower(EnumChipType.NAVIGATION)<=0)
		{
			blit(matrixStack, this.leftPos+94, this.topPos+16, 222, 0, 18, 54);
		}
		
		int mx = 199; //change this lense based -- but we should make this gray and just color it
		
		float r=1F,g=1F,b=1F;
		DeepCoreLogic logic = tile().getLogic();
		if(logic!=null)
		{
			if(logic.getLense()!=null)
			{
				int color = logic.getLense().getColor(logic.getLenseStack(), logic);
				b = ((color>>0) & 0xFF) /255F;
				g = ((color>>8) & 0xFF) /255F;
				r = ((color>>16) & 0xFF) /255F;
				GlStateManager._color4f(r, g, b, 1);
			}
			if(logic.needSupport())
			{
				int sp = tile().support.get();
				if(sp<50)
				{
					
				}
			}
		}
		
		
		PartRenderer.drawTexturedModalRect(leftPos+76, topPos+47, mx, 1, 3F, 19F * tile().getLogic().getProgress(), this.getBlitOffset());
		GlStateManager._color4f(1, 1, 1, 1);
		//progress for item damage
		float s = 16F * tile().getLogic().getDurability();
		PartRenderer.drawTexturedModalRect(leftPos+63, topPos+13+ (16-s), 179, 2 + (16-s), 2, s, this.getBlitOffset());
		
		ItemStack active = tile().getInternInvetory().getStackInSlot(4);
		if(!active.isEmpty())
		{
			GlStateManager._color4f(r, g, b, 1);	
			blit(matrixStack, leftPos+68, topPos+34, 179, 25, 19, 11);
			GlStateManager._color4f(1F, 1F, 1F, 1);
		}
		
		PartRenderer.renderSupport(matrixStack, leftPos+158, topPos+7, tile().support, mouseX, mouseY);
		
		if(logic!=null)
		{
			if(logic.needSupport())
			{
				int sp = tile().support.get();
				if(sp<50)
				{
					
					HelperComponent.renderSymbol(matrixStack, leftPos+17+158, topPos+7, getBlitOffset(), 18);
					HelperComponent.renderSupport(matrixStack, leftPos+17+158, topPos+7, getBlitOffset());
				}
			}
		}
		
		ContainerCoreMiner cont = (ContainerCoreMiner)this.getMenu();
		cont.visible = HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 125, 13, 125+18, 79);
		if(cont.visible)
		{
			int c = cont.slots.size();
			if(c>0)
			{
				int h = Math.min(6, c)*18;
				int w = (c/6);
				if(w*6<c)w++;
				w*=20;
				int x = leftPos -10 -w;
				int y = topPos+13+33 - h/2;
				
				GlStateManager._disableLighting();
				GlStateManager._color4f(1, 1, 1, 1);	
				AbstractGui.fill(matrixStack, x-2, y-2, x + w +2, y + h+2, 0xff5c89c1);
				
				AbstractGui.fill(matrixStack, x-2, y-1, x -1, y + h+1, 0xff394c66);
				AbstractGui.fill(matrixStack, x-2, y-2, x + w +1, y -1, 0xff394c66);
				
				AbstractGui.fill(matrixStack, x +w +1, y-1, x + w +2, y + h+1, 0xff83b0e7);
				AbstractGui.fill(matrixStack, x -1, y+h+1, x + w +2, y + h+2, 0xff83b0e7);
			}
		}
		
		
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		super.renderLabels(matrixStack, p_146979_1_, p_146979_2_);
		PartRenderer.renderSupportTooltip(matrixStack, leftPos, topPos, 158, 7, tile().support, p_146979_1_, p_146979_2_);
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int state)
	{
		if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 68, 34, 68+19, 34+11) && state == 0)
		{
			if(this.minecraft.player.inventory.getCarried().isEmpty())
			{
				this.slotClicked(((ContainerCoreMiner)getMenu()).slot, ((ContainerCoreMiner)getMenu()).slot.index, state, ClickType.PICKUP);
				return true;
			}
		}
		return super.mouseReleased(mouseX, mouseY, state);
	}
	
	@Override
	public List<ITextComponent> getTooltipFromItem(ItemStack stack)
	{
		List<ITextComponent> base = super.getTooltipFromItem(stack);
		CompoundNBT nbt = stack.getTagElement("scanresult");
		if(nbt != null)
		{
			float fill = nbt.getFloat("fill") * 100F;
			TextFormatting f = fill>75 ? TextFormatting.GREEN : fill>50 ? TextFormatting.YELLOW : fill>25 ? TextFormatting.GOLD : TextFormatting.RED;
			
			base.add(new StringTextComponent(String.format("Fillrate: %s%.4f%s%%", f, fill, TextFormatting.RESET)));
			base.add(new StringTextComponent(""));
			
			ListNBT list = nbt.getList("list", 10);
			for(INBT tag : list)
			{
				CompoundNBT c = (CompoundNBT) tag;
				base.add(new StringTextComponent(String.format("%s %.1f%%", c.getString("k"), (c.getInt("v")*0.1))));
			}
		}
		return base;
	}
	
	@Override
	public TileEntityDeepCoreMinerMain tile()
	{
		return ((ContainerCoreMiner)getMenu()).tile;
	}

	public static class ContainerCoreMiner extends ContainerSyncBase implements IContainerScrollable
	{
		TileEntityDeepCoreMinerMain tile;
		private List<Slot> slots;
		private IInventory inv;
		private int size;
		private boolean visible = false;
		
		protected Slot slot;
		
		public ContainerCoreMiner(PlayerInventory inv, TileEntityDeepCoreMinerMain tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			this.inv = tile.getCompressedOres();
			slots = new ArrayList<Slot>();
			
			this.addSlot(new SlotItemHandler(tile.getInternInvetory(), 0, 69, 13)); //Linse
					
			for(int i=0;i<3;i++)
				this.addSlot(new SlotItemHandler(tile.getInternInvetory(), 1+i, 95, 17 + 18*i)); //Filter
			
			slot = this.addSlot(new SlotScrollable(this, tile.getInternInvetory(), 4, 69, 31)); //Active linse
			
			IItemHandler handler = tile.getFakeSlots();		
			this.addSlot(new SlotItemHandler(handler, 0, 69, 72)); //69, 72 rift
			for(int i=0;i<3;i++)
				this.addSlot(new SlotItemHandler(handler, i+1, 126, 13 + 22*i));//126, 13, 22px abstand --- kisten
				
			HelperContainerSync.addInventorySlots(8, 102, inv, this::addSlot);//lamdas \o/  -- because its protected
			
		}

		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return HelperResearch.isUseable(playerIn, tile);
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
	        Slot slot = this.slots.get(index);

	        if (slot != null && slot.hasItem())
	        {
	            ItemStack itemstack1 = slot.getItem();
	            itemstack = itemstack1.copy();

	            if (index < 4)
	            {
	                if (!this.moveItemStackTo(itemstack1, 9, 9+4*9, true))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }
	            else if (index >=9 && !this.moveItemStackTo(itemstack1, 0, 4, false))
	            {
	                return ItemStack.EMPTY;
	            }

	            if (itemstack1.isEmpty())
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }
	        }

	        return itemstack;
		}
		
		
		
		@Override
		public Slot getSlot(int slotId)
		{
			if(tile.getLevel().isClientSide && slotId >= slots.size())
			{
				addSlot(new SlotDeepCore()); 
			}
			return super.getSlot(slotId);
		}
		
		@Override
		public void broadcastChanges()
		{
			if(!tile.getLevel().isClientSide)
			{
				while(size<inv.getContainerSize())
				{
					addSlot(new Slot(inv, size++, 0, 0));
				}
			}
			super.broadcastChanges();
		}
		
		
		@Override
		public int getScollIndex() { return 0; }

		@Override
		public int getRowWidth() { return 0; }

		@Override
		public int getRowCount() { return 0; }
		
		@Override
		public boolean isEnabled(SlotScrollable s)
		{
			return !tile.getLevel().isClientSide;
		}
		
		private class SlotDeepCore extends Slot
		{
			int lokal;
			
			public SlotDeepCore()
			{
				super(new CraftResultInventory(), 0, -16, 0);
				lokal = slots.size();
				slots.add(this);
			}
			
			@Override
			public boolean mayPickup(PlayerEntity playerIn)
			{
				return false;
			}
			
			@Override
			public boolean mayPlace(ItemStack stack)
			{
				return false;
			}

			@Override
			public boolean isActive()
			{
				int my = (slots.size()>6?6:slots.size()) * 9;
				y = 46 -my +(lokal%6)*18;
				int w = slots.size()/6;
				if(w*6 < slots.size())w++;
				x = -10 + -20*w + (lokal/6)*20; 
				return visible;
			}
			
			@Override
			public ItemStack getItem()
			{
				ItemStack st = super.getItem();
				if(st.hasTag())
				{
					CompoundNBT nbt = st.getTag();
					if(nbt.contains("c"))
						st.setCount(nbt.getInt("c"));
				}
				return st;
			}
		}

	}
}
