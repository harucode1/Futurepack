package futurepack.common.gui.inventory;

import futurepack.common.block.inventory.TileEntityTechtable;
import futurepack.common.recipes.crafting.InventoryCraftingForResearch;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;

public class InventoryCraftingTechtable extends InventoryCraftingForResearch
{
	private final TileEntityTechtable tile;
	private final Container eventHandler;

	public InventoryCraftingTechtable(Container con, int width, int height, PlayerEntity user, TileEntityTechtable tech)
	{
		super(con, width, height, user);
		eventHandler = con;
		tile = tech;
	}
	
	
	@Override
	public ItemStack getItem(int index) 
	{
		return tile.getItem(index);
	}
	
	@Override
	public ItemStack removeItemNoUpdate(int index)
	{
		ItemStack it = tile.removeItemNoUpdate(index);
		eventHandler.slotsChanged(this);
		return it;
	}
	
	@Override
	public ItemStack removeItem(int index, int count)
	{
		ItemStack it = tile.removeItem(index, count);
		eventHandler.slotsChanged(this);
		return it;
	}
	
	@Override
	public void setItem(int index, ItemStack stack)
	{
		tile.setItem(index, stack);
		eventHandler.slotsChanged(this);
	}
	
	@Override
	public void clearContent()
	{
		super.clearContent();
	}
}
