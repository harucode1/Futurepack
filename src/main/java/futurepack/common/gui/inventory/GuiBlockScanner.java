package futurepack.common.gui.inventory;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.client.sos.RenderScannerOS;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import futurepack.common.gui.ButtonAspect;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotUses;
import futurepack.common.gui.escanner.GuiResearchMainOverview;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.EnumAspects;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;

public class GuiBlockScanner extends ActuallyUseableContainerScreen<GuiBlockScanner.ContainerScannerBlock> implements Runnable
{
	private static ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/scanner_gui.png");
	private RenderScannerOS os;
	private boolean shouldActivate = false;
	
	public GuiBlockScanner(PlayerEntity pl, TileEntityScannerBlock tile)
	{
		super(new ContainerScannerBlock(pl.inventory, tile), pl.inventory, "gui.blockscanner");
		this.imageHeight = 210;
	}

	@Override
	public void init()
	{
		super.init();
		
		for(int i=0;i<EnumAspects.buttons.length;i++)
		{
			ButtonAspect but = new ButtonAspect(i, this.leftPos+131+(2*i/EnumAspects.buttons.length)*18, this.topPos+11+(i%(EnumAspects.buttons.length/2)*18), EnumAspects.values()[i], container().tile);
			but.activated = container().tile.getAspect(but.getAspect());
			this.addButton(but);
		}
		os = new RenderScannerOS(83, 80);
		os.setPosition(this.leftPos+40, this.topPos+15);
		os.setupScanning(this);
		children.add(os);
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		for(Object o: this.buttons)
		{
			if(o instanceof ButtonAspect)
			{
				ButtonAspect but = (ButtonAspect) o;
				if(but.active && but.isHovered())
				{
					renderTooltip(matrixStack, new TranslationTextComponent("aspect."+but.getAspect().id+".name"), mouseX-this.leftPos, mouseY-this.topPos);
				}
			}
		}
		
		//if(container().tile.state!=null)
		//	this.drawString(matrixStack, font, container().tile.state.getLocalizedText(), 10, 106, 0xffffffff);
		
		//super.drawGuiContainerForegroundLayer(matrixStack, mouseX, mouseY);
		
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 10, this.ySize - 96 + 2, 4210752);
	}
	
	private void updateButtons()
	{
		for(Object o: this.buttons)
		{
			if(o instanceof ButtonAspect)
			{
				ButtonAspect but = (ButtonAspect) o;
				but.activated = container().tile.getAspect(but.getAspect());
			}
		}
	}
	
	public void startScanning()
	{
		FPPacketHandler.syncWithServer(container());
	}
	
	@Override
	public void run()
	{
		container().tile.scanningState=null;
		startScanning();
		shouldActivate = true;
	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		this.minecraft.getTextureManager().bind(res);
        this.blit(matrixStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);
        
        
        this.blit(matrixStack, this.leftPos+17, this.topPos+34, 176, 18, 9, (int)(63 * container().tile.getProgress()));
        
        if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 114, 101, 128, 119))
        {
        	this.blit(matrixStack, this.leftPos+114, this.topPos+101, 194, 0, 14, 18);
        }
        
        if(shouldActivate && container().tile.scanningState!=null)
        {
        	os.setState(container().tile.scanningState, container().tile);
        	shouldActivate = false;
        	os.setupScanning(this);
        }
        os.render(matrixStack, mouseX, mouseY);
	}
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int state)
	{
		if(state == 0 && HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 114, 101, 128, 119))
        {
			GuiResearchMainOverview view = new GuiResearchMainOverview();	
			view.initBuffer(new Screen[4]);
			this.minecraft.setScreen(view);
			return true;
        }
		updateButtons();
		return super.mouseReleased(mouseX, mouseY, state);
	}
	
	private ContainerScannerBlock container()
	{
		return this.getMenu();
	}
	
	public static class ContainerScannerBlock extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		TileEntityScannerBlock tile;
		PlayerInventory inv;
		
		public ContainerScannerBlock(PlayerInventory inv, TileEntityScannerBlock tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			this.inv = inv;
			
			if(inv.player instanceof ServerPlayerEntity)
			{
				tile.searchForItems(inv.player, tile.getItem(0));
			}
			this.addSlot(new SlotForscher(tile, 0, 13, 14, inv.player));
			this.addSlot(new SlotUses(tile, 1, 93, 104));
			
			int x,y;
			
			for(y=0;y<3;y++)
			{
				for(x=0;x<9;x++)
				{
				
					this.addSlot(new Slot(inv, 9 + y*9 +x, 8+x*18, 129+y*18));
				}
			}
			
			for(x=0;x<9;x++)
			{
				this.addSlot(new Slot(inv, x, 8+x*18, 187));
			}
			broadcastChanges();
//			tile.searchForItems(inv.player, tile.getStackInSlot(0));
		}
		
		
		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			
			return true;
		}

		
		@Override
		public void removed(PlayerEntity playerIn)
		{
			tile.stopOpen(playerIn);
			super.removed(playerIn);
		}

		@Override
		public void writeToBuffer(PacketBuffer buf)
		{
			buf.writeVarInt(tile.getAspects());
			buf.writeVarInt(tile.getProperty(3));//progress
			buf.writeVarInt(tile.getProperty(2));//state
		}

		@Override
		public void readFromBuffer(PacketBuffer nbt)
		{
			try
			{
				tile.scanningState = null;
				this.broadcastChanges();
				tile.setAspects(nbt.readVarInt());
				tile.setProperty(3, nbt.readVarInt());
				tile.setProperty(2, nbt.readVarInt());
//				localSync.detectAndSendChanges(listeners);
				tile.searchForItems(inv.player, tile.getItem(0));
//				localSync.detectAndSendChanges(listeners);
				this.broadcastChanges();
			}
			catch(RuntimeException e)
			{
				//this is on the server
				tile.getLevel().getServer().submitAsync( () -> {
					throw e;
				});
			}
		}

		
		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
			Slot slot = this.getSlot(index);
			if (slot != null && slot.hasItem())
	        {
				ItemStack item = slot.getItem();
				itemstack = item.copy();
				
				if(index==0)
				{
					//Shift Click at scan input
					if(!this.moveItemStackTo(item, 2, 38, false))
					{	
						return ItemStack.EMPTY;
					}
				}
				else if(index==1)
				{
					//Shift Click at Crafting Output
					if(!this.moveItemStackTo(item, 2, 38, false))
					{	
						return ItemStack.EMPTY;
					}
				}
				else
				{
					//Shift Click at Inventory
					if(!this.moveItemStackTo(item, 0, 1, false))
					{	
						return ItemStack.EMPTY;
					}
				}
				
				if (item.getCount() == 0)
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }
				
				slot.onTake(playerIn, item);
	        }

	        return itemstack;
		}
		
		private static class SlotForscher extends Slot
		{
			private final PlayerEntity pl;
			
			public SlotForscher(TileEntityScannerBlock inventoryIn, int index, int xPosition, int yPosition, PlayerEntity pl)
			{
				super(inventoryIn, index, xPosition, yPosition);
				this.pl = pl;
			}
			
			@Override
			public void setChanged()
			{
				super.setChanged();
				if(pl instanceof ServerPlayerEntity)
					((TileEntityScannerBlock)this.container).searchForItems(pl,this.getItem());
			}
			
		}
	}
}
