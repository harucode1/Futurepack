package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.api.interfaces.IFluidTankInfo;
import futurepack.common.block.modification.machines.TileEntityGasTurbine;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class GuiGasTurbine extends ActuallyUseableContainerScreen<GuiGasTurbine.ContainerGasTurbine>
{
	private TileEntityGasTurbine tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/gas_turbine.png");
	private IFluidTankInfo gastank;
	public GuiGasTurbine(PlayerEntity pl, TileEntityGasTurbine tile)
	{
		super(new ContainerGasTurbine(pl.inventory, tile), pl.inventory, "gui.gasturbine");
		this.tile = tile;
		imageHeight+=20;
		gastank = tile.getGas();
	}
	
	

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
        
		if(!gastank.isEmpty())
		{
			int k = (this.width - this.imageWidth) / 2;
			int l = (this.height - this.imageHeight) / 2;
			PartRenderer.renderFluidTankTooltip(matrixStack, k+26, l+31, 16, 52, tile.getGas(), mouseX, mouseY);
		}
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		if(tile.getSpeed() > 0)
		{
			float width = Math.round((18.0f / 10.0f) * tile.getSpeed());
			this.blit(matrixStack, leftPos+128, topPos+41, 176, 0, (int)width, 25);
		}
		
		if(!gastank.isEmpty())
		{
			PartRenderer.renderFluidTank(k+26, l+31, 16, 52, gastank, mouseX, mouseY, 10);
		}
		
		PartRenderer.renderNeon(matrixStack, k+7, l+14, tile.energy, mouseX, mouseY);
	}
	
	
	public static class ContainerGasTurbine extends ContainerSyncBase
	{
		@SuppressWarnings("unused")
		private TileEntityGasTurbine tile;
		private IItemHandler handler;
		
		public ContainerGasTurbine(PlayerInventory pl, TileEntityGasTurbine tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			handler = tile.getGui();
			
			this.addSlot(new SlotItemHandler(handler, 0, 26, 9)); 
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 104 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 162));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			Slot slot = this.getSlot(index);
			if(!playerIn.level.isClientSide && slot.hasItem())
			{
				if(slot.container == playerIn.inventory)
				{
					this.moveItemStackTo(slot.getItem(), 0, 1, false);					
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 1, this.slots.size(), false);				
				}
				
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return true;
		}
	}


}
