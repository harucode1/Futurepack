package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.api.interfaces.IFluidTankInfo;
import futurepack.common.block.inventory.TileEntityFuelCell;
import futurepack.common.fluids.FPFluids;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.SlotUses;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;

public class GuiFuelZell extends ActuallyUseableContainerScreen<GuiFuelZell.ContainerFuellCell>
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/fuelcell.png");
	private IFluidTankInfo info;
	
	public GuiFuelZell(PlayerEntity pl, TileEntityFuelCell tile)
	{
		super(new ContainerFuellCell(pl.inventory, tile), pl.inventory, "gui.fuellzell");
		info = new IFluidTankInfo()
		{
			
			@Override
			public FluidStack getFluidStack() 
			{
				return new FluidStack(FPFluids.bitripentiumFluidStill, tile().getFuel());
			}
			
			@Override
			public int getCapacity() 
			{
				return tile.getMaxFuel();
			}
		};
	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int mouseX, int mouseY) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);

		PartRenderer.renderFluidTank(k+10, l+20, 16, 48, info, mouseX, mouseY, 10F);
		
		this.minecraft.getTextureManager().bind(res);
		GlStateManager._enableBlend();
		this.setBlitOffset(20);
		this.blit(matrixStack, leftPos+9, topPos+19, 176, 0, 18, 50);
		this.setBlitOffset(0);
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
        
        int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		PartRenderer.renderFluidTankTooltip(matrixStack, k+10, l+20, 16, 48, info, mouseX, mouseY);

    }
	
	private TileEntityFuelCell tile()
	{
		return this.getMenu().tile;
	}
	
	public static class ContainerFuellCell extends ContainerSyncBase
	{
		TileEntityFuelCell tile;
		
		public ContainerFuellCell(PlayerInventory inv, TileEntityFuelCell tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			int l,i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 3; ++i1)
				{
					this.addSlot(new SlotUses(tile, i1 + l * 3, 44 + i1 * 18, 18 + l * 18));
				}
			}
			
			this.addSlot(new SlotUses(tile, 9, 44 + 4 * 18, 36));
			
			for (l = 0; l < 3; ++l)
	        {
	            for (i1 = 0; i1 < 9; ++i1)
	            {
	                this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
	            }
	        }

	        for (l = 0; l < 9; ++l)
	        {
	            this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
	        }
		}

		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return true;
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			if(!playerIn.level.isClientSide)
			{
				Slot s = this.getSlot(index);
				if(s.hasItem())
				{
					if(index<11)
					{
						this.moveItemStackTo(s.getItem(), 11, this.slots.size(), false);
					}
					else
					{
						this.moveItemStackTo(s.getItem(), 0, 11, false);
					}
					if(s.getItem().getCount()<=0)
					{
						s.set(ItemStack.EMPTY);
					}
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
	}

}
