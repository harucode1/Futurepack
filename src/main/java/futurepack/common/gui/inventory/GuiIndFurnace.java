package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.api.event.IndustrieSmeltEvent;
import futurepack.api.interfaces.IFluidTankInfo;
import futurepack.common.block.inventory.TileEntityIndustrialFurnace;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.SlotUses;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.FurnaceResultSlot;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.FluidStack;

public class GuiIndFurnace extends ActuallyUseableContainerScreen<GuiIndFurnace.ContainerIndFurnace>
{
	
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/industrieofen.png");
	private IFluidTankInfo info;
	
	public GuiIndFurnace(PlayerEntity pl, TileEntityIndustrialFurnace tile)
	{
		super(new ContainerIndFurnace(pl.inventory,tile), pl.inventory, "gui.indfurnace");
		imageHeight+=20;
		info = new IFluidTankInfo()
		{
			@Override
			public FluidStack getFluidStack() 
			{
				return tile.getFluid();
			}
			
			@Override
			public int getCapacity() 
			{
				return tile.getCapacity();
			}
		};
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		//this.font.drawString(matrixStack, I18n.format("container.indfurn", new Object[0]), 32, 6, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 7, this.ySize - 96 + 4, 4210752);
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);

        int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		PartRenderer.renderFluidTankTooltip(matrixStack, k+8, l+28, 16, 52, info, mouseX, mouseY);
    }
	
	private TileEntityIndustrialFurnace tile()
	{
		return this.getMenu().tile;
	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int mouseX, int mouseY) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		PartRenderer.renderFluidTank(k+8, l+28, 16, 52, info, mouseX, mouseY, 10F);
		
		this.minecraft.getTextureManager().bind(res);
		
		int f2 = (int) (tile().getProgress() * 29);
		this.blit(matrixStack, k+113, l+40, 192, 0, f2, 18);
		for(int i=0;i<tile().uses.length;i++)
		{
			if(tile().uses[i])
				this.blit(matrixStack, k+37 + (i * 29), l+71, 192, 18, 8, 9);
		}
		
		GlStateManager._enableBlend();
		this.setBlitOffset(20);
		this.blit(matrixStack, leftPos+7, topPos+27, 176, 27, 18, 54);
		this.setBlitOffset(0);
	}
	
	public static class ContainerIndFurnace extends ContainerSyncBase
	{
		private TileEntityIndustrialFurnace tile;
//		int lp=0,lb=0;
		
		public ContainerIndFurnace(PlayerInventory pl, TileEntityIndustrialFurnace tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotUses(this.tile, 0, 8,6, 1/*Max getCount()*/)); //Lava Buckit Input
			for(int i=0;i<3;i++)
			{
				this.addSlot(new Slot(this.tile, 1+i, 33 + (29 * i), 47)); //Smelt Input
			}
			for(int i=0;i<3;i++)
			{
				this.addSlot(new FurnaceResultSlot(pl.player, this.tile, 4+i, 152,41 + (i * 18))); //Smelt Output
			}
			this.addSlot(new SlotUses(tile, 7, 116, 77)); //Obsidian Output
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 104 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 162));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			Slot slot = this.getSlot(par2);
			if(!pl.level.isClientSide && slot.hasItem())
			{
				if(slot.container == pl.inventory)
				{
					if(tile.canPlaceItem(0, slot.getItem()))
					{
						this.moveItemStackTo(slot.getItem(), 0, 1, false);
					}
					else
					{
						this.moveItemStackTo(slot.getItem(), 1, 4, false);
					}					
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 8, this.slots.size(), false);				
				}
				
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public ItemStack clicked(int slotId, int dragType, ClickType clickTypeIn, PlayerEntity player)
		{
			ItemStack it = super.clicked(slotId, dragType, clickTypeIn, player);
			if(it!=null)
			{
				MinecraftForge.EVENT_BUS.post(new IndustrieSmeltEvent(player, it));
			}
			return it;
		}
		
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
			return true;
		}
		
	}
}
