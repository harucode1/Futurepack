package futurepack.common.gui.inventory;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.common.block.logistic.monorail.TileEntityMonorailStation;
import futurepack.common.gui.SlotFakeItem;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageContainer;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

public class GuiMonorailStation extends ActuallyUseableContainerScreen<GuiMonorailStation.ContainerMonorailSation>
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/mono_station.png");
	int backup[] = new int[10];
	
	public GuiMonorailStation(PlayerEntity pl, TileEntityMonorailStation t)
	{
		super(new ContainerMonorailSation(pl.inventory, t), pl.inventory, "gui.monorail.station");
		this.imageWidth = 176;
		this.imageHeight = 165;
	}

	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		this.blit(matrixStack, k+6, l+6, 176, 0, 8, 8);
		
		if(!container().tile.blacklist_extract)
		{
			this.blit(matrixStack, k+7, l+56, 176, 8, 18, 10);
		}
		if(!container().tile.blacklist_insert)
		{
			this.blit(matrixStack, k+151, l+22, 176, 18, 18, 10);
		}
	}

	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		String s = container().tile.getDisplayName().getString();
        this.font.draw(matrixStack, s, (this.imageWidth- this.font.width(s)) / 2 + 4, 6, 4210752);
        //this.font.drawString(matrixStack, container().pl.getDisplayName().getUnformattedText(), 8, this.ySize - 96 + 5, 4210752);
		
        GlStateManager._disableDepthTest();
        
		for(int i = 0; i < 10; i++)
		{
			if(backup[i] > 64)
			{
				Slot slot = this.getMenu().slots.get(i);
				String str = "∞";
				try {
					str = new String(str.getBytes(), StandardCharsets.UTF_8.name());
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
				this.font.drawShadow(matrixStack, str, slot.x + 19 - 2 - this.font.width(str), slot.y + 6 + 3, 16777215);
			}
		}
		
		GlStateManager._enableDepthTest();
	}
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int state)
	{
		if(state==0)
		{
			int k = (this.width - this.imageWidth) / 2;
			int l = (this.height - this.imageHeight) / 2;
			boolean send = false;
			if(HelperComponent.isInBox(mouseX-k, mouseY-l, 7, 56, 7+18, 56+10))
			{
				container().tile.blacklist_extract = !container().tile.blacklist_extract;
				send = true;
			}
			if(HelperComponent.isInBox(mouseX-k, mouseY-l, 151, 22, 151+18, 22+10))
			{
				container().tile.blacklist_insert = !container().tile.blacklist_insert;
				send = true;
			}
			
			if(HelperComponent.isInBox(mouseX-k, mouseY-l, 6, 6, 14, 14))
			{
				container().open = true;
				send = true;
			}
			
			if(send)
				FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageContainer(container()));
		}
		return super.mouseReleased(mouseX, mouseY, state);
	}
	
	private ContainerMonorailSation container()
	{
		return this.getMenu();
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{		
		this.renderBackground(matrixStack);
		
		//backup and manipulate
		for(int i = 0; i < 10; i++)
		{
			backup[i] = this.getMenu().slots.get(i).getItem().getCount();
			if(backup[i] > 64)
			{
				this.getMenu().slots.get(i).getItem().setCount(1);
			}
		}
		
		//render
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		//restore
		for(int i = 0; i < 10; i++)
		{
			if(backup[i] > 64)
			{
				this.getMenu().slots.get(i).getItem().setCount(backup[i]);
			}
		}
		
		this.renderTooltip(matrixStack, mouseX, mouseY);		
	}
	
	public static class ContainerMonorailSation extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		TileEntityMonorailStation tile;
		PlayerInventory pl;
		boolean open = false;
		
		public ContainerMonorailSation(PlayerInventory inv, TileEntityMonorailStation tile)
		{
			super();
			this.tile = tile;
			this.pl = inv;
			
			int l;
			int i1;
			
			for (l = 0; l < 5; ++l)
			{
				this.addSlot(new SlotFakeItem(tile, 0+l, 44 + l * 18, 19));
				this.addSlot(new SlotFakeItem(tile, 5+l, 44 + l * 18, 53));
			}

			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack clicked(int slotId, int clickedButton, ClickType mode, PlayerEntity playerIn)
		{
			//clicked button 0==links, 1==rechts
			//mode Pickup==normal quick_move==shift click
			
			if(slotId >= 0 && (mode == ClickType.PICKUP || mode == ClickType.QUICK_MOVE))
			{
				Slot s = getSlot(slotId);
				if(s instanceof SlotFakeItem)
				{
					if(!playerIn.inventory.getCarried().isEmpty())
					{
						ItemStack inMouseStack = playerIn.inventory.getCarried().copy();
						if(mode == ClickType.PICKUP)
						{
							inMouseStack.setCount(1);
						}
						else
						{
							inMouseStack.setCount(65);
						}
						s.set(inMouseStack);
					}
					else
					{
						
						int add = (mode == ClickType.PICKUP) ? 1 : 10;
						
						if(clickedButton==0 && s.hasItem())
						{
							s.getItem().grow(add);
							if(s.getItem().getCount() > 64)
								s.getItem().setCount(65);
						}
						else if(clickedButton==1 && s.hasItem())
						{
							s.getItem().shrink(add);
							if(s.getItem().getCount()<=0)
								s.set(ItemStack.EMPTY);
						}
						
					}
				}
			}
			return super.clicked(slotId, clickedButton, mode, playerIn);
		}

		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			
			return true;
		}

		@Override
		public void writeToBuffer(PacketBuffer nbt)
		{
			nbt.writeBoolean(tile.blacklist_insert);
			nbt.writeBoolean(tile.blacklist_extract);
			nbt.writeBoolean(open);
			if(open)
				FPGuiHandler.RENAME_WAYPOINT.openGui(pl.player, tile.getBlockPos());
			open = false;
		}

		@Override
		public void readFromBuffer(PacketBuffer nbt)
		{
			tile.blacklist_insert = nbt.readBoolean();
			tile.blacklist_extract = nbt.readBoolean();
			open = nbt.readBoolean();
			
			if(open)
			{
				open = false;
				FPGuiHandler.RENAME_WAYPOINT.openGui(pl.player, tile.getBlockPos());
			}	
		}
		
	}
}
