package futurepack.common.gui.inventory;

import java.util.Arrays;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.block.modification.machines.TileEntityMachineSupport;
import futurepack.common.gui.PartRenderer;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;

public abstract class GuiMachineSupport<T extends TileEntityMachineSupport> extends GuiModificationBase<T>
{
    protected int sx = 158, sy = 7;

	public GuiMachineSupport(Container cont, String gui, PlayerInventory inv)
	{
		super(cont, gui, inv);
	}
	

	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		PartRenderer.renderSupport(matrixStack, leftPos+sx, topPos+sy, tile().getSupport(), mouseX, mouseY);

		if(tile().getChipPower(EnumChipType.SUPPORT) <= 0 && tile().getSupport().getType() == EnumEnergyMode.PRODUCE)
		{	
			HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos+18*2, getBlitOffset(), 6); //SP symbol
			HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos+18*2, getBlitOffset(), 18); //X symbol
		}
		this.minecraft.getTextureManager().bind(res);
	}

	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		super.renderLabels(matrixStack, mouseX, mouseY);
		PartRenderer.renderSupportTooltip(matrixStack, leftPos, topPos, sx, sy, tile().getSupport(), mouseX, mouseY);

		if(tile().getChipPower(EnumChipType.SUPPORT) <= 0 && tile().getSupport().getType() == EnumEnergyMode.PRODUCE)
		{
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos , -20, 18*2, -20+18, 18*2+18))
			{
				PartRenderer.drawHoveringTextFixedString(matrixStack, Arrays.asList(I18n.get("gui.futurepack.machine.no_support_chip").split("\n")), mouseX-leftPos +12, mouseY-topPos+4, -1, font);
			}
		}
	}	
}
