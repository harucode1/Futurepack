package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityPartPress;
import futurepack.common.gui.ContainerSyncBase;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiPartPress extends ActuallyUseableContainerScreen<GuiPartPress.ContainerPartPress> {

	private TileEntityPartPress tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/stanze.png");
	
	public GuiPartPress(PlayerEntity pl, TileEntityPartPress tile)
	{
		super(new ContainerPartPress(pl.inventory,tile), pl.inventory, "gui.partpress");
		this.tile = tile;
	}
	
	long update = 0;
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		//this.font.drawString(matrixStack, I18n.format("container.partpress", new Object[0]), 7, 4, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 7, this.ySize - 96 + 4, 4210752);
	}
    
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int var2, int var3) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		if(tile.isBruning())
		{
			int f = 14 - (int) (tile.getBurn() * 14D);
			this.blit(matrixStack, k+9, l+37+f, 176, 0+f, 14, 14-f);
		}
		
		int ff =  (int) (tile.getPressure()/100D * 22D);
		this.blit(matrixStack, k+79, l+31, 176, 14, 36, ff);
		
	}
	
	public static class ContainerPartPress extends ContainerSyncBase
	{
		private TileEntityPartPress tile;
//		int lp = 0;
//		int lb = 0;
//		int lm = 0;
		
		public ContainerPartPress(PlayerInventory pl, TileEntityPartPress tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new Slot(this.tile, 0, 8, 54));
			this.addSlot(new Slot(this.tile, 1, 80, 14));
			this.addSlot(new Slot(this.tile, 2, 98, 14));
			this.addSlot(new Slot(this.tile, 3, 80, 54));
			this.addSlot(new Slot(this.tile, 4, 98,54));
			int l;
			int i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = getSlot(par2);
				if(slot!=null && slot.hasItem())
				{
					if(pl.inventory == slot.container)
					{
						if(tile.canPlaceItem(0, slot.getItem()))
							this.moveItemStackTo(slot.getItem(), 0, 1, false);
						else
							this.moveItemStackTo(slot.getItem(), 1, 3, false);
					}
					else
					{
						this.moveItemStackTo(slot.getItem(), 5, this.slots.size(), false);
					}
					if(slot.getItem().getCount()<=0)
					{
						slot.set(ItemStack.EMPTY);
					}
				}
				this.broadcastChanges();
			}
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
			return true;
		}
		
//		@Override
//		public void addCraftingToCrafters(ICrafting c) 
//		{
//			super.addCraftingToCrafters(c);
//			c.sendProgressBarUpdate(this, 0, tile.pressure);
//			c.sendProgressBarUpdate(this, 1, tile.burn);
//			c.sendProgressBarUpdate(this, 2, tile.maxburn);
//		}
//		
//		public void detectAndSendChanges()
//	    {
//	        super.detectAndSendChanges();
//
//	        for (int i = 0; i < this.listeners.size(); ++i)
//	        {
//	            ICrafting icrafting = (ICrafting)this.listeners.get(i);
//
//	            if (this.lp != this.tile.pressure)
//	            {
//	                icrafting.sendProgressBarUpdate(this, 0, this.tile.pressure);
//	            }
//
//	            if (this.lb != this.tile.burn)
//	            {
//	                icrafting.sendProgressBarUpdate(this, 1, this.tile.burn);
//	            }
//
//	            if (this.lm != this.tile.maxburn)
//	            {
//	                icrafting.sendProgressBarUpdate(this, 2, this.tile.maxburn);
//	            }
//	        }
//
//	        this.lp = tile.pressure;
//	        this.lb = tile.burn;
//	        this.lm = tile.maxburn;
//	    }
//
//		 //@ TODO: OnlyIn(Dist.CLIENT)
//		 public void updateProgressBar(int id, int val)
//		 {
//			 if (id == 0)
//			 {
//				 this.tile.pressure = val;
//			 }
//
//			 if (id == 1)
//			 {
//				 this.tile.burn = val;
//			 }
//			 
//			 if (id == 2)
//			 {
//				 this.tile.maxburn = val;
//			 }
//		 }
//		
	}

}
