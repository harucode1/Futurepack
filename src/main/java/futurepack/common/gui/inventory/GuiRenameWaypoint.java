package futurepack.common.gui.inventory;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.api.interfaces.tilentity.ITileRenameable;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;

public class GuiRenameWaypoint extends ActuallyUseableContainerScreen
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/rename_gui.png");
	
	private TextFieldWidget nameField;
	private ITileRenameable rename;
	
	public GuiRenameWaypoint(PlayerEntity pl, ITileRenameable tile)
	{
		super(new ContainerRenameWaypoint(pl.inventory, tile), pl.inventory, "gui.rename.waypoint");
		this.imageWidth = 176;
		this.imageHeight = 20;
		rename = tile;
	}

	@Override
	public void init()
	{
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		int i = (this.width - this.imageWidth) / 2;
		int j = (this.height - this.imageHeight) / 2;
		this.nameField = new TextFieldWidget(this.font, i + 19, j + 6, 151, 8, new TranslationTextComponent("gui.reanme.waypoint.textbox"));
		this.nameField.setTextColor(10198015);
		this.nameField.setTextColorUneditable(0);
		this.nameField.setBordered(false);
		this.nameField.setMaxLength(30);
		this.nameField.setValue(rename.getName().getContents());
	}
	
	@Override
	public boolean keyPressed(int keycode, int p_keyPressed_2_, int p_keyPressed_3_) 
	{
		if(this.nameField.keyPressed(keycode, p_keyPressed_2_, p_keyPressed_3_))
		{
			rename.setName(nameField.getValue());
			FPPacketHandler.syncWithServer(container());
			return true;
		}
		else if(this.nameField.isFocused())
		{
			return true;
		}
		return super.keyPressed(keycode, p_keyPressed_2_, p_keyPressed_3_);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		this.nameField.mouseClicked(mouseX, mouseY, mouseButton);
		if(mouseButton == 0)
		{
			int k = (this.width - this.imageWidth) / 2;
			int l = (this.height - this.imageHeight) / 2;
			if(HelperComponent.isInBox(mouseX-k, mouseY-l, 6, 6, 14, 14))
			{
				this.minecraft.player.closeContainer();
				return true;
			}
		}
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		this.renderBackground(matrixStack);
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		GlStateManager._disableLighting();
		GlStateManager._disableBlend();
		this.nameField.render(matrixStack, mouseX, mouseY, partialTicks);
		this.renderTooltip(matrixStack, mouseX, mouseY);
	}
	
	@Override
	public boolean charTyped(char symbol, int key_code) 
	{
		if(this.nameField.charTyped(symbol, key_code))
		{
			rename.setName(nameField.getValue());
			FPPacketHandler.syncWithServer(container());
			return true;
		}
		return super.charTyped(symbol, key_code);
	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		this.minecraft.getTextureManager().bind(res);
		this.blit(matrixStack, leftPos, topPos, 0, 20, imageWidth, imageHeight);
		
	}
	
	public ContainerRenameWaypoint container()
	{
		return (ContainerRenameWaypoint) this.getMenu();
	}
	
	public static class ContainerRenameWaypoint extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		private ITileRenameable entity;
		
		public ContainerRenameWaypoint(PlayerInventory pl, ITileRenameable tile)
		{
			entity = tile;
		}
		
		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return HelperResearch.isUseable(playerIn, (TileEntity) entity);
		}

		@Override
		public void writeToBuffer(PacketBuffer buf)
		{
			buf.writeUtf(entity.getName().getString(), 255);
		}

		@Override
		public void readFromBuffer(PacketBuffer buf)
		{
			entity.setName(buf.readUtf(255));
		}
		
	}


	
}
