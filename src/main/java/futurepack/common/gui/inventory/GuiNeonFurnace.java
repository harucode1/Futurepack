package futurepack.common.gui.inventory;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.common.block.modification.machines.TileEntityNeonFurnace;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotBaseXPOutput;
import futurepack.common.gui.SlotUses;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

public class GuiNeonFurnace extends GuiMachineSupport<TileEntityNeonFurnace>
{
	public GuiNeonFurnace(PlayerEntity pl, TileEntityNeonFurnace tile)
	{
		super(new ContainerNeonFurnace(pl.inventory, tile), "neonfurnace.png", pl.inventory);
	}
	
	@Override
	public TileEntityNeonFurnace tile()
	{
		return ((ContainerNeonFurnace)getMenu()).tile;
	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
			
		if(tile().getProperty(TileEntityNeonFurnace.FIELD_PROGRESS)>0)
			this.blit(matrixStack, leftPos+57, topPos+53, 176, 0, 14, 13);
		
		int f = (int) (tile().getProperty(TileEntityNeonFurnace.FIELD_PROGRESS)/11F * 24F);
		this.blit(matrixStack, leftPos+79, topPos+34, 176, 14, f, 17);
		

		//PartRenderer.renderSupport(matrixStack, guiLeft+158, guiTop+7, tile().getSupport(), mouseX, mouseY);
	}

	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		super.renderLabels(matrixStack, p_146979_1_, p_146979_2_);
		//PartRenderer.renderSupportTooltip(matrixStack, guiLeft, guiTop, 158, 7, tile().getSupport(), p_146979_1_, p_146979_2_);
	}
	
	public static class ContainerNeonFurnace extends ContainerSyncBase
	{
		TileEntityNeonFurnace tile;
		
		public ContainerNeonFurnace(PlayerInventory inv, TileEntityNeonFurnace tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new Slot(tile, 0, 56, 34));
			this.addSlot(new SlotBaseXPOutput(inv.player, tile, 1, 116, 35));
			this.addSlot(new SlotUses(tile, 2, 116, 59));
			
			int l;
			int i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = this.slots.get(par2);
		        if(slot != null && slot.hasItem())
		        {
		        	if(slot.container == tile)
		        	{
		        		this.moveItemStackTo(slot.getItem(), 2, slots.size(), false);
//		        		{
//		        			slot.inventory.setInventorySlotContents(slot.getSlotIndex(), slot.getStack().getCount()==0 ? null : slot.getStack());
//		        		}
		        	}
		        	else
		        	{
		        		this.moveItemStackTo(slot.getItem(), 0, 1, false);
//		        		{
//		        			slot.inventory.setInventorySlotContents(slot.getSlotIndex(), slot.getStack().getCount()==0 ? null : slot.getStack());
//		        		}
		        	}
		        }
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
			return true;
		}
	}
	
}
