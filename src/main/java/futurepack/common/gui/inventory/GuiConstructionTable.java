package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.FuturepackTags;
import futurepack.common.block.modification.machines.BlueprintRecipe;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.recipes.crafting.InventoryCraftingForResearch;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageResearchResponse;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.CraftResultInventory;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.CraftingResultSlot;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.PacketDistributor;

public class GuiConstructionTable extends ActuallyUseableContainerScreen<GuiConstructionTable.ContainerConstructionTable>
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/rezeptmaschiene.png");

	public GuiConstructionTable(PlayerInventory inv, BlockPos pos)
	{
		super(new ContainerConstructionTable(inv, pos), inv, "gui.constructiontable");
	}
	
	@Override
	public void init(Minecraft p_init_1_, int p_init_2_, int p_init_3_) 
	{
		super.init(p_init_1_, p_init_2_, p_init_3_);
		HelperContainerSync.RestoreCursorPos();
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
    {
        this.font.draw(matrixStack, I18n.get("gui.futurepack.assembly.recipe.title", new Object[0]), 28-20, 6, 4210752);
        this.font.draw(matrixStack, I18n.get("container.inventory", new Object[0]), 28-15, this.imageHeight - 96 +3, 4210752);
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int mx,int my) 
	{
		 GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		 this.minecraft.getTextureManager().bind(res);
		 int k = (this.width - this.imageWidth) / 2;
		 int l = (this.height - this.imageHeight) / 2;
		 this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);		 
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int w) 
	{
		ContainerConstructionTable c = getMenu();
		if(HelperComponent.isInBox(mx -leftPos, my -topPos, 116, 10, 134, 28) && w==0)
		{		
			 c.back = true;
			 HelperContainerSync.SaveCursorPos();
			 FPPacketHandler.syncWithServer(c);
			 return true;
		}
		return super.mouseReleased(mx, my, w);
	}

	public static class ContainerConstructionTable extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		public CraftingInventory craftMatrix;
		public IInventory craftResult = new CraftResultInventory();
		public IInventory it1 = new CraftResultInventory();
		public IInventory it2 = new CraftResultInventory();
		private boolean back=false;
		private World worldObj;
		private PlayerEntity pl;
		private BlockPos pos;
		
		public ContainerConstructionTable(PlayerInventory par1InventoryPlayer, BlockPos pos)
		{
			this.pos = pos;
			this.worldObj = par1InventoryPlayer.player.level;
			this.pl = par1InventoryPlayer.player;
			
			craftMatrix = new InventoryCraftingForResearch(this, 3, 3, pl);
			
			this.addSlot(new CraftingResultSlot(par1InventoryPlayer.player, this.craftMatrix, this.craftResult, 0, 98, 40));
			this.addSlot(new Slot(it1, 0, 98, 11));
			this.addSlot(new SlotClick(it2,it1, 0, 134, 61));
			int l;
			int i1;

			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 3; ++i1)
				{
					this.addSlot(new Slot(this.craftMatrix, i1 + l * 3, 26 + i1 * 18, 22 + l * 18));
				}
			}

			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(par1InventoryPlayer, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(par1InventoryPlayer, l, 8 + l * 18, 142));
			}

			this.slotsChanged(this.craftMatrix);
			
			if(!pl.level.isClientSide && pl instanceof ServerPlayerEntity)
			{
				ServerPlayerEntity mp = (ServerPlayerEntity) pl;
				MessageResearchResponse mes =  new MessageResearchResponse(pl.getServer(), CustomPlayerData.getDataFromPlayer(pl));
				FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> mp), mes);
			}
		}
		
		@Override
	    public void slotsChanged(IInventory par1IInventory)
	    {
			IRecipe irec = worldObj.getRecipeManager().getRecipeFor(IRecipeType.CRAFTING, this.craftMatrix, this.worldObj).orElse(null);
			if(irec!=null)
			{
				 this.craftResult.setItem(0, irec.assemble(this.craftMatrix));
			}
			else
			{
				this.craftResult.setItem(0, ItemStack.EMPTY);
			}		
	      
	        
	        if(!it1.getItem(0).isEmpty() && FuturepackTags.item_crystals.contains(it1.getItem(0).getItem()))
	        {
	        	NonNullList<ItemStack> it = NonNullList.withSize(craftMatrix.getContainerSize(), ItemStack.EMPTY);
	        	for(int i=0;i<it.size();i++)
	        	{
	        		it.set(i, craftMatrix.getItem(i));	
	        	}
	        	ItemStack is = new ItemStack(MiscItems.crafting_recipe);
	        	ItemStack ist = craftResult.getItem(0);
	        	if(ist!=null && !ist.isEmpty())
	        	{
	        		BlueprintRecipe rec = new BlueprintRecipe(it, ist.getHoverName().getString(), pl.getGameProfile().getId(), worldObj);
	        		rec.addRecipeToItem(is);
	        		it2.setItem(0, is);
	        	}
	        }
	        else
	        {
	        	it2.setItem(0, ItemStack.EMPTY);
	        }
	    }

		@Override
		public ItemStack clicked(int slotId, int dragType, ClickType clickTypeIn, PlayerEntity player)
		{
			ItemStack it = super.clicked(slotId, dragType, clickTypeIn, player);
			slotsChanged(null);
			return it;
		}
		
		@Override
	    public void removed(PlayerEntity par1EntityPlayer)
	    {
	        super.removed(par1EntityPlayer);

	        if (!this.worldObj.isClientSide)
	        {
	            for (int i = 0; i < 9; ++i)
	            {
	                ItemStack itemstack = this.craftMatrix.removeItemNoUpdate(i);

	                if (itemstack != null)
	                {
	                    par1EntityPlayer.drop(itemstack, false);
	                }
	            }
	        }
	        ItemStack it = it1.getItem(0);
	        if(it!=null)
	        	par1EntityPlayer.drop(it, false);
	    }

		@Override
	    public boolean stillValid(PlayerEntity par1EntityPlayer)
	    {
	        return true;
	    }

		@Override
	    public ItemStack quickMoveStack(PlayerEntity par1EntityPlayer, int par2)
	    {
	        ItemStack itemstack = ItemStack.EMPTY;
	        Slot slot = this.slots.get(par2);

	        if (slot != null && slot.hasItem())
	        {
	            ItemStack itemstack1 = slot.getItem();
	            itemstack = itemstack1.copy();

	            if (par2 == 0)
	            {
	                if (!this.moveItemStackTo(itemstack1, 10, 46, true))
	                {
	                    return ItemStack.EMPTY;
	                }

	                slot.onQuickCraft(itemstack1, itemstack);
	            }
	            else if (par2 >= 10 && par2 < 37)
	            {
	                if (!this.moveItemStackTo(itemstack1, 37, 46, false))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }
	            else if (par2 >= 37 && par2 < 46)
	            {
	                if (!this.moveItemStackTo(itemstack1, 10, 37, false))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }
	            else if (!this.moveItemStackTo(itemstack1, 10, 46, false))
	            {
	                return ItemStack.EMPTY;
	            }

	            if (itemstack1.getCount() == 0)
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }

	            if (itemstack1.getCount() == itemstack.getCount())
	            {
	                return ItemStack.EMPTY;
	            }

	            slot.onTake(par1EntityPlayer, itemstack1);
	        }

	        return itemstack;
	    }

		
		@Override
	    public boolean canTakeItemForPickAll(ItemStack par1ItemStack, Slot par2Slot)
	    {
	        return par2Slot.container != this.craftResult && super.canTakeItemForPickAll(par1ItemStack, par2Slot);
	    }

		@Override
		public void writeToBuffer(PacketBuffer buf) 
		{
			buf.writeBoolean(back);
		}

		@Override
		public void readFromBuffer(PacketBuffer buf) 
		{			
			slotsChanged(craftMatrix);
			back = buf.readBoolean();
			if(back)
			{
				pl.getCommandSenderWorld().getBlockState(pos).use(pl.getCommandSenderWorld(), pl, pl.getUsedItemHand(), new BlockRayTraceResult(pl.getViewVector(0), Direction.UP, pos, false));
				//FPGuiHandler.OPTI_BENCH.openGui(pl, pos);
			}
			slotsChanged(null);
		}

		
	}
	
	public static class SlotClick extends Slot
	{
		private IInventory inv;
		public SlotClick(IInventory par1iInventory,IInventory par2iInventory, int par2, int par3, int par4)
		{
			super(par1iInventory, par2, par3, par4);
			inv = par2iInventory;
			
		}
		
		@Override
		public ItemStack onTake(PlayerEntity par1EntityPlayer, ItemStack par2ItemStack)
		{
			ItemStack it = inv.getItem(getSlotIndex());
			if(!it.isEmpty())
			{
				it.shrink(1);
				if(it.getCount()<=0)
				{
					it = ItemStack.EMPTY;
				}
				inv.setItem(getSlotIndex(), it);
			}
			
			return super.onTake(par1EntityPlayer, par2ItemStack);
		}
		
		@Override
		public boolean mayPlace(ItemStack stack)
		{
			return false;
		}
	}
}
