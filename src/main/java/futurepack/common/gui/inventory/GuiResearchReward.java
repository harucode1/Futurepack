package futurepack.common.gui.inventory;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.common.block.inventory.TileEntityForscher;
import futurepack.depend.api.helper.HelperContainerSync;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiResearchReward extends ActuallyUseableContainerScreen<GuiResearchReward.ContainerResearchReward> 
{
	private static final ResourceLocation CHEST_GUI_TEXTURE = new ResourceLocation("textures/gui/container/generic_54.png");
	
	public GuiResearchReward(PlayerEntity pl, TileEntityForscher.RewardInventory ri)
	{
		
		super(new ContainerResearchReward(pl.inventory, ri), pl.inventory, "Rewards");

		imageHeight = 97 + 17 + 8 * 18;

	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) {
		GlStateManager._color4f(1.0F, 1.0F, 1.0F, 1.0F);
		
		this.minecraft.getTextureManager().bind(CHEST_GUI_TEXTURE);
		
		int x = (this.width - this.imageWidth) / 2;
		int y = (this.height - this.imageHeight) / 2;
		
		int invY = 0;

		this.blit(matrixStack, x, y, 0, 0, this.imageWidth, 6 * 18 + 17);
		this.blit(matrixStack, x, y + 6*18+17, 0, 17, this.imageWidth, (8 - 6) * 18);

		invY = 8 * 18 + 17;
			
		this.blit(matrixStack, x, y + invY, 0, 125, this.imageWidth, 97);
		
	}
	
	
	public static class ContainerResearchReward extends ActuallyUseableContainer 
	{

		TileEntityForscher.RewardInventory inv;
		
		public ContainerResearchReward(PlayerInventory pl, TileEntityForscher.RewardInventory ri)
		{
			super();
			
			inv = ri;
			
			int x, y;
			x = (178 - 18 * 9) / 2; 
			y = 18;

			for (int l = 0; l < 8; ++l)
	        {
	            for (int i1 = 0; i1 < 9; ++i1)
	            {
	            	int id = i1 + l * 9;
	                this.addSlot(new SlotTakeOnly(ri, id, x + i1 * 18, y + l * 18));
	            }
	        }
			
			y += 8 * 18;
			
			y+=14;
			
			HelperContainerSync.addInventorySlots(8, y, pl, this::addSlot);
			
		}

		@Override
		public boolean stillValid(PlayerEntity playerIn) 
		{
			return true;
		}

		
		public static class SlotTakeOnly extends Slot 
		{

			public SlotTakeOnly(IInventory inventoryIn, int index, int xPosition, int yPosition) 
			{
				super(inventoryIn, index, xPosition, yPosition);
			}

			public boolean mayPlace(ItemStack stack) 
			{
				return false;
			}

		}
		
		@Override
		public void removed(PlayerEntity playerIn) 
		{
			if(!playerIn.getCommandSenderWorld().isClientSide)
				inv.compress();
			
			super.removed(playerIn);
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int sl)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot= getSlot(sl);
				if(!slot.hasItem())		
					return ItemStack.EMPTY;
				
				if(slot.container == pl.inventory)
				{
					//this.mergeItemStack(slot.getStack(), 0, slots, false);
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 9*8, this.slots.size(), false);
				}
			
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}

	}



	

}
