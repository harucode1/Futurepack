package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityModulT3;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.server.ServerWorld;

public class GuiModulT3 extends ActuallyUseableContainerScreen
{

	//private TileEntityBaterieBox tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/modul_t3.png");
				
	public GuiModulT3(PlayerEntity pl, TileEntityModulT3 tile)
	{
		super(new ContainerModulT3(pl.inventory, tile), pl.inventory, "gui.modul.t3");
		//this.tile = tile;
	}
		
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int mx, int my) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
		
		PartRenderer.renderExp(matrixStack, leftPos+79, topPos+11, tile(), mx, my);	
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int but) 
	{
		if(but == 0)
		{
			if(HelperComponent.isInBox(mx-leftPos, my-topPos, 43, 26, 43+18, 26+18))
			{
				((ContainerModulT3)getMenu()).up = true;
				FPPacketHandler.syncWithServer((ContainerModulT3)this.getMenu());
			}
			if(HelperComponent.isInBox(mx-leftPos, my-topPos, 43, 44, 43+18, 44+18))
			{
				((ContainerModulT3)getMenu()).up = false;
				FPPacketHandler.syncWithServer((ContainerModulT3)this.getMenu());
			}
		}
		return super.mouseReleased(mx, my, but);
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		//this.font.drawString(matrixStack, I18n.format("container.modulT3", new Object[0]), 6, 4, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 6, this.ySize - 96 + 4, 4210752);
	}
				
	private TileEntityModulT3 tile()
	{
			return ((ContainerModulT3)getMenu()).tile;
	}
		
	public static class ContainerModulT3 extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		TileEntityModulT3 tile;
		PlayerEntity pl;
//		int lp;
		boolean up;	
		
		public ContainerModulT3(PlayerInventory inv, TileEntityModulT3 tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			pl = inv.player;
			
			int l;
			int i1;
										
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
				
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			return ItemStack.EMPTY;
		}
			
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
				return HelperResearch.isUseable(var1, tile);
		}
						
//		@Override
//		public void addCraftingToCrafters(ICrafting c)
//		{
//			super.addCraftingToCrafters(c);
//			c.sendProgressBarUpdate(this, 0, (int)this.tile.getXp());
//		}
//					
//		@Override
//		public void detectAndSendChanges() 
//		{
//			super.detectAndSendChanges();
//			if(this.lp != (int)this.tile.getXp())
//			{
//				for (int i = 0; i < this.listeners.size(); ++i)
//				{
//					ICrafting c = (ICrafting)this.listeners.get(i);
//					c.sendProgressBarUpdate(this, 0, (int)this.tile.getXp());
//				}
//			}
//			this.lp = (int)this.tile.getXp();
//		}
//			
//		@Override
//		public void updateProgressBar(int id, int val)
//		{
//			super.updateProgressBar(id, val);
//			if(id==0)
//			{
//				this.tile.setXp(val);
//			}
//		}

		@Override
		public void writeToBuffer(PacketBuffer nbt)
		{
			nbt.writeBoolean(up);
		}

		@Override
		public void readFromBuffer(PacketBuffer nbt)
		{
//			float total = pl.experience;
			up = nbt.readBoolean();
			pl.level.getServer().submitAsync(() -> 
			{
				if(up)
				{
					if(pl.experienceLevel > 0 && tile.getXp() < tile.getMaxXp())
					{
						int xp = Math.min(tile.getMaxXp()-tile.getXp(), (int)pl.experienceProgress);
						if(xp==0)
						{
							pl.giveExperienceLevels(-1);
							int removed = pl.getXpNeededForNextLevel();
							xp = Math.min(tile.getMaxXp()-tile.getXp(), removed);
							tile.setXp(tile.getXp() + xp);
							if(removed > xp)
							{
								pl.giveExperiencePoints(removed - xp);
							}
						}
						else
						{
							pl.experienceProgress = 0;
							tile.setXp(tile.getXp() + xp);
						}		
					}				
				}
				else
				{
					if(tile.getXp() > 0)
					{
						int xp = (int) Math.min(pl.getXpNeededForNextLevel() - pl.experienceProgress, tile.getXp());		
						pl.giveExperiencePoints(xp);
						tile.setXp(tile.getXp() - xp);
					}
				}
				if(!pl.level.isClientSide)
				{
					ServerWorld serv = (ServerWorld) pl.level;
					serv.playSound(null, tile.getBlockPos(), SoundEvents.UI_BUTTON_CLICK, SoundCategory.PLAYERS, 0.75F, 1.0F);
				}
			});
		}
	}

}
