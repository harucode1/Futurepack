package futurepack.common.gui.inventory;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.api.interfaces.tilentity.ITileScrollableInventory;
import futurepack.common.gui.SlotScrollable;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.interfaces.IContainerScrollable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.items.IItemHandlerModifiable;

public class GuiScrollableInventory extends ActuallyUseableContainerScreen<GuiScrollableInventory.ContainerScollable>
{
	public ResourceLocation tex = new ResourceLocation(Constants.MOD_ID, "textures/gui/container_scrollable.png");
	
	public GuiScrollableInventory(PlayerEntity pl, IItemHandlerModifiable inv)
	{
		super(new ContainerScollable(pl.inventory, inv), pl.inventory, "gui.scrollable.inventory");
		imageWidth = 194;
		imageHeight = 222;
	}
	
	public GuiScrollableInventory(PlayerEntity pl, ITileScrollableInventory inv)
	{
		this(pl, inv.getScrollableInventory());
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
    {
        this.font.draw(matrixStack, new TranslationTextComponent("container.chest").getContents(), 8, 6, 4210752);
        this.font.draw(matrixStack, container().player.inventory.getDisplayName().getContents(), 8, this.imageHeight - 96 + 2, 4210752);
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		
		GlStateManager._color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.minecraft.getTextureManager().bind(tex);
        this.blit(matrixStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);
        int scollBar = 106; //height
        int w = 12; //weight
        int scollButton = 15; //height
        
        if(container().getMaxRows() > 6)
        {
        	float pos = (float)container().scrollIndex / (float)(container().getMaxRows()-6);
        	int h = (int) (topPos + 19 + (scollBar-scollButton)*pos);
        	this.blit(matrixStack, leftPos+174, h, 232, 0, w, h);
        }
        else
        {
        	this.blit(matrixStack, leftPos+174, topPos+19, 244, 0, w, scollButton);
        }
	}

	@Override
	public boolean mouseScrolled(double mx, double my, double dw) 
	{
		int mr = container().getMaxRows();
		if(mr <= container().getRowCount())
			return true;
//		int dw = Mouse.getDWheel();
		
		int s0 = container().scrollIndex;
		int s1 = container().scrollIndex+container().getRowCount();
		
		if(dw > 0 && s0 > 0)
		{
			container().scrollIndex--;
		}
		else if(dw < 0 && s1 < mr)
		{
			container().scrollIndex++;
		}
		
		return super.mouseScrolled(mx, my, dw);
	}
	
	private ContainerScollable container()
	{
		return this.getMenu();
	}

	public static class ContainerScollable extends ActuallyUseableContainer implements IContainerScrollable
	{
		private PlayerEntity player;
		private int extraSlots;
		private IItemHandlerModifiable handler;
		
		public int scrollIndex;
		
		public ContainerScollable(PlayerInventory inv, IItemHandlerModifiable handler)
		{
			player = inv.player;
			this.handler = handler;
			
			HelperContainerSync.addInventorySlots(8, 139, inv, this::addSlot);
			
			//if(!player.world.isRemote)
			{
				for(int i=0;i<handler.getSlots();i++)
				{
					this.addSlot(new SlotScrollable(this, handler, i, 8+(i%9)*18, 18));
				}
			}
		}
		
		public ContainerScollable(PlayerInventory inv, ITileScrollableInventory handler)
		{
			this(inv, handler.getScrollableInventory());
		}

		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return true;
		}

		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
	        Slot slot = this.slots.get(index);

	        if (slot != null && slot.hasItem())
	        {
	            ItemStack itemstack1 = slot.getItem();
	            itemstack = itemstack1.copy();

	            if (index < 4*9)
	            {
	                if (!this.moveItemStackTo(itemstack1, 4*9, this.slots.size(), true))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }
	            else if (!this.moveItemStackTo(itemstack1, 0, 4*9, false))
	            {
	                return ItemStack.EMPTY;
	            }

	            if (itemstack1.isEmpty())
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }
	        }

	        return itemstack;
		}
		
		@Override
		public Slot getSlot(int slotId)
		{
			if(player.level.isClientSide)
			{			
				while(slots.size() <= slotId)
				{
					this.addSlot(new SlotScrollable(this, handler, extraSlots, -6-getRowWidth()*18 +(extraSlots%getRowWidth())*18 , 39));
					if(handler.getSlots() > extraSlots+1)
						extraSlots++;
				}
			}
			return super.getSlot(slotId);
		}
		
		@Override
		public int getScollIndex()
		{
			return scrollIndex;
		}

		@Override
		public int getRowWidth()
		{
			return 9;
		}

		@Override
		public int getRowCount()
		{
			return 6;
		}
		
		public int getMaxRows()
		{
			int slots = this.slots.size() - 4*9;
			int rows = slots / getRowWidth();
			if(rows*getRowWidth() < slots)
				rows++;
			return rows;
		}
		
	}
}
