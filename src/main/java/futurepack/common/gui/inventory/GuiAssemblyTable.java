package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityAssemblyTable;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiAssemblyTable extends ActuallyUseableContainerScreen<GuiAssemblyTable.ContainerAssemblyTable>
{

	private TileEntityAssemblyTable tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/assembler.png");
	
	public GuiAssemblyTable(PlayerEntity pl, TileEntityAssemblyTable tile)
	{
		super(new ContainerAssemblyTable(pl.inventory,tile), pl.inventory, "gui.assemplytable");
		this.tile = tile;
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }

	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int var2, int var3) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
			
		
		
//		int ff = (int) (tile.getPower() / tile.getMaxPower() * 10);
//		for(int i=0;i<ff;i++)
//		{
//			this.blit(matrixStack, k+8, l+71 - (i*7), 205, 0, 8, 6);
//		}
//		
//		int f = (int) (tile.getSupport() / 100F * 10F);
//		for(int i=0;i<f;i++)
//		{
//			this.blit(matrixStack, k+159, l+71 - (i*7), 213, 0, 8, 6);
//		}
		
		PartRenderer.renderNeon(matrixStack, k+7, l+7, tile.power, var2, var3);
		PartRenderer.renderSupport(matrixStack, k+158, l+7, tile.support, var2, var3);
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		//super.drawGuiContainerForegroundLayer(matrixStack, p_146979_1_, p_146979_2_);
		PartRenderer.renderNeonTooltip(matrixStack, leftPos, topPos, 7, 7, tile.power, p_146979_1_, p_146979_2_);
		PartRenderer.renderSupportTooltip(matrixStack, leftPos, topPos, 158, 7, tile.support, p_146979_1_, p_146979_2_);
	}
	
	public static class ContainerAssemblyTable extends ContainerSyncBase
	{
		TileEntityAssemblyTable tile;
	
		public ContainerAssemblyTable(PlayerInventory inv, TileEntityAssemblyTable tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			tile.user = inv.player;
			
			this.addSlot(new Slot(tile, 0, 23, 22)); //Baterie slot
			for(int i=0;i<3;i++)
			{
				this.addSlot(new Slot(tile, i+1, 62 + (i*18), 22)); //Input
			}
			this.addSlot(new Slot(tile, 4, 133, 22)); //AI storage
			this.addSlot(new SlotAssembling(tile, 5, 81, 54,this)); //output

			int l;
			int i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}	
		
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				if(par2 == 5)
					return ItemStack.EMPTY; // output slot is not shift clickable
				
				Slot slot = this.getSlot(par2);
				if(!slot.hasItem())
					return ItemStack.EMPTY;
				if(slot.container == pl.inventory)
				{
					if(tile.canPlaceItem(0, slot.getItem()))
					{
						this.moveItemStackTo(slot.getItem(), 0, 1, false);
					}
					else if(tile.canPlaceItem(4, slot.getItem()))
					{
						this.moveItemStackTo(slot.getItem(), 4, 5, false);
					}
					else
					{
						this.moveItemStackTo(slot.getItem(), 1, 4, false);
					}
				}
				else
				{
					ItemStack stack = slot.getItem().copy();
					if(this.moveItemStackTo(slot.getItem(), 6, this.slots.size(), false))
					{
						slot.onTake(pl, stack);
					}
				}			
				
				if(slot.getItem().getCount()<=0)
				{				
					slot.set(ItemStack.EMPTY);
				}
			}
			
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
			return true;
		}
		
		@Override
		public void removed(PlayerEntity playerIn)
		{
			super.removed(playerIn);
			tile.user = null;
		}
	}
	
	private static class SlotAssembling extends Slot
	{
		Container c;
		public SlotAssembling(IInventory par1iInventory, int par2, int par3, int par4,Container c)
		{
			super(par1iInventory, par2, par3, par4);
			this .c = c;
		}

		@Override
		public ItemStack onTake(PlayerEntity pl, ItemStack it)
		{
			if(container instanceof TileEntityAssemblyTable && !it.isEmpty())
			{
				TileEntityAssemblyTable ass =  (TileEntityAssemblyTable)container;
				ass.onUse();
			}
			c.broadcastChanges();
			return super.onTake(pl, it);
		}
		
		@Override
		public boolean mayPlace(ItemStack par1ItemStack) 
		{
			return false;
		}
		
		@Override
		public boolean mayPickup(PlayerEntity playerIn) 
		{
			return playerIn.inventory.getCarried().isEmpty();
		}
	}
	
}

