package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityFermentationBarrel;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class GuiFermentationBarrel extends ActuallyUseableContainerScreen<GuiFermentationBarrel.ContainerFermentationBarrel>
{
	TileEntityFermentationBarrel tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/fermentation_barrel.png");

	public GuiFermentationBarrel(PlayerEntity pl, TileEntityFermentationBarrel tile)
	{
		super(new ContainerFermentationBarrel(pl.inventory, tile), pl.inventory, "gui.fermantation_barrel");
		this.tile = tile;
		imageHeight+=20;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
        
		if(!tile.getGas().isEmpty())
		{
			int k = (this.width - this.imageWidth) / 2;
			int l = (this.height - this.imageHeight) / 2;
			PartRenderer.renderFluidTankTooltip(matrixStack, k+82, l+24, 16, 52, tile.getGas(), mouseX, mouseY);
		}
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		if(!tile.getGas().isEmpty())
		{
			PartRenderer.renderFluidTank(k+82, l+24, 16, 52, tile.getGas(), mouseX, mouseY, 10);
		}
	}

	public static class ContainerFermentationBarrel extends ContainerSyncBase
	{
		private TileEntityFermentationBarrel tile;
		private IItemHandler handler;
		
		public ContainerFermentationBarrel(PlayerInventory pl, TileEntityFermentationBarrel tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			handler = tile.getGui();
			
			this.addSlot(new SlotItemHandler(handler, 0, 58, 24)); 
			this.addSlot(new SlotItemHandler(handler, 1, 152, 77)); 
			
			this.addSlot(new SlotItemHandler(handler, 2, 106, 24)); 
			this.addSlot(new SlotItemHandler(handler, 3, 106, 60)); 
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 104 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 162));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			Slot slot = this.getSlot(index);
			if(!playerIn.level.isClientSide && slot.hasItem())
			{
				if(slot.container == playerIn.inventory)
				{
					for(int i = 0; i < 4; i++)
					{
						if(handler.isItemValid(i, slot.getItem()))
							this.moveItemStackTo(slot.getItem(), 0, 4, false);	
					}
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 4, this.slots.size(), false);				
				}
				
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return true;
		}
	}
	
}


