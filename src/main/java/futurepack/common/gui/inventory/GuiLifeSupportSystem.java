package futurepack.common.gui.inventory;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.common.block.modification.TileEntityFluidPump;
import futurepack.common.block.modification.machines.TileEntityLifeSupportSystem;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.SlotUses;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class GuiLifeSupportSystem extends GuiModificationBase<TileEntityLifeSupportSystem>
{
	public GuiLifeSupportSystem(PlayerEntity pl, TileEntityLifeSupportSystem tile)
	{
		super(new ContainerLifeSupportSystem(pl.inventory, tile), "electrolyseur.png", pl.inventory);
		
	}
	
	int[][] tanks = {{28,28}, {103, 28}, {129, 28}};

	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);

		for(int i=0;i<tanks.length;i++)
		{
			FluidTankInfo info = tile().getFluid(i);
			if(!info.isEmpty())
			{
				PartRenderer.renderFluidTank(leftPos+tanks[i][0], topPos+tanks[i][1], 16, 48, info, mouseX, mouseY, 10F);
			}
		}

		
		this.minecraft.getTextureManager().bind(super.res);
		GlStateManager._enableBlend();
		this.setBlitOffset(20);
		for(int i=0;i<tanks.length;i++)
		{
			this.blit(matrixStack, leftPos+tanks[i][0]-1, topPos+tanks[i][1]-1, 176, 0, 18, 50);
		}
		this.setBlitOffset(0);
		
		int offset = 43 - (int) (tile().getProgress() * 43);
		this.blit(matrixStack, leftPos+65, topPos+25 + offset, 194, 0 + offset, 16, 43 - offset);
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
        
        for(int i=0;i<tanks.length;i++)
		{
			FluidTankInfo info = tile().getFluid(i);
			if(!info.isEmpty())
			{
				PartRenderer.renderFluidTankTooltip(matrixStack, leftPos+tanks[i][0], topPos+tanks[i][1], 16, 48, info, mouseX, mouseY);
			}
		}
    }
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		super.renderLabels(matrixStack, mouseX, mouseY);
	}
	
	@Override
	public TileEntityLifeSupportSystem tile()
	{
		return ((ContainerLifeSupportSystem)this.getMenu()).tile;
	}

	public static class ContainerLifeSupportSystem extends ContainerSyncBase
	{
		TileEntityLifeSupportSystem tile;

		public ContainerLifeSupportSystem(PlayerInventory inv, TileEntityLifeSupportSystem tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotUses(tile, 0, 152, 26));
			this.addSlot(new SlotUses(tile, 1, 152, 62));
//			
//			this.addSlot(new SlotItemHandler(tile.getGui(), 2, 80, 7));
//			
//			this.addSlot(new SlotItemHandler(tile.getGui(), 3, 116, 27));
//			this.addSlot(new SlotItemHandler(tile.getGui(), 4, 116, 63));
			
			int l;
			int i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}

		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return HelperResearch.isUseable(playerIn, tile);
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			
			return ItemStack.EMPTY;
		}
	}
}
