package futurepack.common.gui.inventory;

import java.util.Arrays;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.common.block.inventory.TileEntityBatteryBox;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.SlotUses;
import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiBatteryBox extends ActuallyUseableContainerScreen<GuiBatteryBox.ContainerBatteryBox>
{

	//private TileEntityBatteryBox tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/neon_zelle.png");
	
	public GuiBatteryBox(PlayerEntity pl, TileEntityBatteryBox tile)
	{
		super(new ContainerBatteryBox(pl.inventory,tile), pl.inventory, "gui.baettrybox");
		//this.tile = tile;
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int var2, int var3) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		
//		int ff = (int) (tile().getPower() / tile().getMaxPower() * 10);
//		for(int i=0;i<ff;i++)
//		{
//			this.blit(matrixStack, k+8, l+71 - (i*7), 176, 0, 8, 6);
//		}
		PartRenderer.renderNeon(matrixStack, k+7, l+7, tile().energy, var2, var3);
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mx, int my)
	{
		//this.font.drawString(matrixStack, I18n.format("container.bateriebox", new Object[0]), 90, 6, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 90, this.ySize - 96 + 2, 4210752);
		
		renderNeonTooltip(matrixStack, leftPos, topPos, 7, 7, tile().energy, mx, my);
	}

	private void renderNeonTooltip(MatrixStack matrixStack, int guiLeft, int guiTop, int nx, int ny, INeonEnergyStorage engine, int mouseX, int mouseY)
	{
		if(HelperComponent.isInBox(mouseX, mouseY, guiLeft+nx, guiTop+ny, guiLeft+nx+11, guiTop+ny+72))
		{
			String l1 = engine.get() + " / " + engine.getMax() + " NE";
			String l2 = "Input: +" + ((int)tile().getAverageEnergyAdded()) + " NE";
			String l3 = "Output:-" + ((int)tile().getAverageEnergyRemoved()) + " NE";
			int delta = (int) (tile().getAverageEnergyAdded() - tile().getAverageEnergyRemoved());
			String l4 = "Delta: " + (delta > 0 ? "+":"") + delta+ " NE";
			PartRenderer.drawHoveringTextFixedString(matrixStack, Arrays.asList(l1, l2, l3, l4), mouseX-guiLeft, mouseY-guiTop, -1, minecraft.font);
		}
	}
	
	private TileEntityBatteryBox tile()
	{
		return getMenu().tile;
	}
	
	
	public static class ContainerBatteryBox extends ContainerSyncBase
	{
		TileEntityBatteryBox tile;
//		int lp;
		
		public ContainerBatteryBox(PlayerInventory inv, TileEntityBatteryBox tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotUses(tile, 0, 62, 8));
			this.addSlot(new SlotUses(tile, 1, 62, 62));

			int l;
			int i1;
			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 4; ++i1)
				{
					this.addSlot(new SlotUses(tile, i1 + l * 4 + 2, 98 + i1 * 18, 16 + l * 18));
				}
			}
			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = this.slots.get(par2);
		        if(slot != null && slot.hasItem())
		        {
		        	if(slot.container == tile)
		        	{
		        		this.moveItemStackTo(slot.getItem(), 14, slots.size(), false);
		        	}
		        	else
		        	{
		        		this.moveItemStackTo(slot.getItem(), 0, 14, false);
		        	}
		        	if(slot.getItem().getCount()==0)
		        	{
		        		slot.set(ItemStack.EMPTY);
		        	}
		        }
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
			return true;
		}
		
//		@Override
//		public void addCraftingToCrafters(ICrafting c)
//		{
//			super.addCraftingToCrafters(c);
//			c.sendProgressBarUpdate(this, 0, (int)this.tile.power);
//		}
//		
//		@Override
//		public void detectAndSendChanges() 
//		{
//			super.detectAndSendChanges();
//			if(this.lp != (int)this.tile.power)
//			{
//				for (int i = 0; i < this.listeners.size(); ++i)
//				{
//					ICrafting c = (ICrafting)this.listeners.get(i);
//					c.sendProgressBarUpdate(this, 0, (int)this.tile.power);
//				}
//			}
//			this.lp = (int)this.tile.power;
//		}
//		
//		@Override
//		public void updateProgressBar(int id, int val)
//		{
//			super.updateProgressBar(id, val);
//			if(id==0)
//			{
//				this.tile.power=val;
//			}
//		}
	}
}
