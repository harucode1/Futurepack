package futurepack.common.gui.inventory;

import java.util.Arrays;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityOptiBenchCraftingModule;
import futurepack.common.block.modification.machines.TileEntityOptiBench;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.inventory.GuiOptiBench.ContainerOptiBench;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.util.ResourceLocation;

public class GuiOptiBenchCraftingModule extends ActuallyUseableContainerScreen<ContainerOptiBench<TileEntityOptiBenchCraftingModule>>
{
	protected ResourceLocation res;
	int nx=7, ny=7;
	
	public GuiOptiBenchCraftingModule(PlayerEntity pl, TileEntityOptiBenchCraftingModule tile)
	{
		super(new GuiOptiBench.ContainerOptiBench(pl.inventory,tile), pl.inventory, "gui.opti_crafting");
		res = new ResourceLocation(Constants.MOD_ID, "textures/gui/opti_crafting.png");
		
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
    public TileEntityOptiBenchCraftingModule tile()
    {
    	return ((GuiOptiBench.ContainerOptiBench<TileEntityOptiBenchCraftingModule>)this.getMenu()).tile;
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		this.minecraft.getTextureManager().bind(res);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);

        this.blit(matrixStack, leftPos+80, topPos+34, 176, 0, (int)(29 * (tile().getProperty(TileEntityOptiBench.FIELD_PROGRESS)/10D)), 18);
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		//Removed to remove default text (inventory and gui name) super.drawGuiContainerForegroundLayer(matrixStack, mouseX, mouseY);
	}

	@Override
	public boolean mouseReleased(double mx, double my, int but) 
	{
		if(HelperComponent.isInBox(mx-leftPos, my-topPos, 96, 10, 114, 28))
		{
			if(but == 0)
			{
				HelperContainerSync.SaveCursorPos();
				FPPacketHandler.syncWithServer((IGuiSyncronisedContainer) this.getMenu());
				return true;
			}
		}
		return super.mouseReleased(mx, my, but);
	}

}
