package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.gui.SlotFakeItem;
import futurepack.common.item.tools.ItemAirBrush;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.CraftResultInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;

public class GuiAirBrush extends ActuallyUseableContainerScreen<GuiAirBrush.ContainerAirBrush>
{

	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/airbrush.png");
	
	public GuiAirBrush(PlayerEntity pl)
	{
		super(new ContainerAirBrush(pl), pl.inventory, "gui.airbrush");
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int var2, int var3) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);

	}
	
	public static class ContainerAirBrush extends ActuallyUseableContainer
	{
		public PlayerInventory inv;
		public IInventory airbrush = new CraftResultInventory();
		private ItemStack it;
		
		public ContainerAirBrush(PlayerEntity pl)
		{
			inv = pl.inventory;
			it = pl.getMainHandItem();
			if(it ==null || it.getItem()!=ToolItems.airBrush)
			{
				it = pl.getOffhandItem();
				if(it==null|| it.getItem()!=ToolItems.airBrush)
				{
					pl.closeContainer();
					return;
				}
			}
			if(it.hasTag())
				airbrush.setItem(0, ItemAirBrush.getItem(it.getTag()));
			
			this.addSlot(new Slot(airbrush, 0, 98, 47));
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				if(inv.selected == l)
				{
					this.addSlot(new SlotFakeItem(inv, l, 8 + l * 18, 142));
				}
				else
				{
					this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
				}
				
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity par1EntityPlayer, int par2)
		{
			 Slot slot = this.slots.get(par2);
			 if(slot !=null)
			 {
				 Slot target = slots.get(0);
				 if(target!=null && target.mayPlace(slot.getItem()))
				 {
					  if(!target.hasItem())
					  {
						  target.set(slot.getItem());
						  slot.set(ItemStack.EMPTY);
						  return ItemStack.EMPTY;
					  }
				 }
				 if(slot == target && target!=null)
				 {
					 if(target.hasItem() && slot!=null)
					 {
						 if(this.moveItemStackTo(target.getItem(), 1, slots.size()-1, false))
						 {
							 target.set(ItemStack.EMPTY);
						 }
					 }
					 return ItemStack.EMPTY;
				 }
				
				 return ItemStack.EMPTY;
			 }
			return super.quickMoveStack(par1EntityPlayer, par2);
		}
		
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
			return true;
		}
		
		@Override
		public void removed(PlayerEntity pl)
		{
			if(it!=null)
			{
				if(!it.hasTag())
					it.setTag(new CompoundNBT());
				
				ItemAirBrush.setItem(it.getTag(), airbrush.getItem(0));
				//pl.setHeldItem(EnumHand.MAIN_HAND, it);
			}
			super.removed(pl);
		}
	}
}
