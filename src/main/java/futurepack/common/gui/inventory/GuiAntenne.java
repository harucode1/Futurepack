package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.misc.TileEntityAntenna;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.DyeColor;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

public class GuiAntenne extends ActuallyUseableContainerScreen<GuiAntenne.ContainerAntenne>
{
	public static ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/antenna_gui.png");

	public GuiAntenne(PlayerEntity pl, TileEntityAntenna tile)
	{
		super(new ContainerAntenne(pl.inventory, tile), pl.inventory, "gui.antenna");
		
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY) 
	{
		this.font.draw(matrixStack, "Range: " + getMenu().tile.getRange(), 8.0F, 6.0F, 4210752);
	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		blit(matrixStack, leftPos, topPos, 0, 0, imageWidth, imageHeight);
		
		int f = container().tile.getFrequenz();
		f &= 0xFFF; // 4096 possibilities
		
		int[] freq = new int[3];
		freq[0] = f & 0xF;
		freq[1] = (f >> 4) & 0xF;
		freq[2] = (f >> 8) & 0xF;
		
		for(int i=0;i<freq.length;i++)
		{
			int x = leftPos + 34 + i*54;
			int y = topPos + 43;
			
			GL11.glPushMatrix();
			GL11.glTranslatef(x, y, 0);
			double rot = 22.5D * freq[i];
			GL11.glRotated(rot, 0, 0, 1);
			
			blit(matrixStack, -18, -18, 176, 0, 36, 36);
			
			GL11.glPopMatrix();
			
			int bgr = DyeColor.byId(freq[i]).getColorValue();
			float r = (bgr & 0xFF) / 255F;
			float g = ((bgr >> 8) & 0xFF) / 255F;
			float b = ((bgr >> 16) & 0xFF) / 255F;			
			GL11.glColor4f(r, g, b, 1.0F);
			blit(matrixStack, x - 3, y-3, 212, 0, 6, 6);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			
		}
		
		
	}
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int state)
	{
		if(state == 0)
		{
			int f = container().tile.getFrequenz();
			f &= 0xFFF; // 4096 possibilities
			
			int[] freq = new int[3];
			freq[0] = f & 0xF;
			freq[1] = (f >> 4) & 0xF;
			freq[2] = (f >> 8) & 0xF;
			
			for(int i=0;i<freq.length;i++)
			{
				int x = leftPos + 34 + i*54;
				int y = topPos + 43;
				
				GL11.glPushMatrix();
				GL11.glTranslatef(x, y, 0);
				double rot = 22.5D * freq[i];
				GL11.glRotated(rot, 0, 0, 1);
				
				MatrixStack matrixStack = new MatrixStack();
				blit(matrixStack, -18, -18, 176, 0, 36, 36);
				
				GL11.glPopMatrix();
				
				double dx = mouseX-x;
				double dy = mouseY-y;
				
				if(HelperComponent.isInBox(dx, dy, -24, -24, 24, 24))
				{
					double d = dx / dy;
					if(d<0)
						d =-d;
						
					double reg = Math.toDegrees(Math.atan(d));
					if(dx>=0 && dy>0)
					{
						reg = 180 - reg;
					}
					else if(dx>0 && dy<0)
					{
						//richtig
					}
					else if(dx<0 && dy>0)
					{
						reg = 180 + reg;
					}
					else if(dx<0 && dy<=0)
					{
						reg = 360 - reg;
					}
						
					int w1 = (int)(reg / 22.5D);
					int w2 = (w1 +1 ) % 16;
						
					double d1 = w1 * 22.5D;
					double d2 = w2 * 22.5D;
					if(w1==15 && d2==0)
					{
						d2 = 360;
					}
					d1 = reg - d1;
					d2 = d2 - reg;
						
					if(d2 < d1)
					{
						freq[i] = w2;
					}
					else
					{
						freq[i] = w1;
					}
					
					int nf = (freq[2]<<8) | (freq[1]<<4) | freq[0];
					if(f != nf)
					{
						container().tile.setFrequenz(nf);
						FPPacketHandler.syncWithServer(container());
					}
					
					return true;
				}
			}
		}
		return super.mouseReleased(mouseX, mouseY, state);

	}
	
	private ContainerAntenne container()
	{
		return this.getMenu();
	}
	
	public static class ContainerAntenne extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		TileEntityAntenna tile;

		public ContainerAntenne(PlayerInventory inv, TileEntityAntenna tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			HelperContainerSync.addInventorySlots(8, 84, inv, this::addSlot);
		}

		@Override
		public boolean stillValid(PlayerEntity pl)
		{
			return HelperResearch.isUseable(pl, tile);
		}

		@Override
		public void writeToBuffer(PacketBuffer nbt) 
		{
			nbt.writeVarInt(tile.getFrequenz());
		}

		@Override
		public void readFromBuffer(PacketBuffer nbt) 
		{
			tile.setFrequenz(nbt.readVarInt());
		}
		
		
		
	}

}
