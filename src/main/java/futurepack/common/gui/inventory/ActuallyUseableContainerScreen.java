package futurepack.common.gui.inventory;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.util.text.TranslationTextComponent;

public abstract class ActuallyUseableContainerScreen<T extends Container> extends ContainerScreen<T> 
{

	public ActuallyUseableContainerScreen(T screenContainer, PlayerInventory inv, String toTranslate) 
	{
		super(screenContainer, inv, new TranslationTextComponent(toTranslate.contains("futurepack") ? toTranslate : "futurepack." + toTranslate));
	}

	@Deprecated
	public ActuallyUseableContainerScreen(T screenContainer, PlayerInventory inv) 
	{
		this(screenContainer, inv, "TODO");
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int x, int y)
	{
		//Removed to remove default text (inventory and gui name) super.drawGuiContainerForegroundLayer(matrixStack, x, y);
	}
}
