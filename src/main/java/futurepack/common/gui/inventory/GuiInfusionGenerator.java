package futurepack.common.gui.inventory;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.common.block.modification.machines.TileEntityInfusionGenerator;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotBaseXPOutput;
import futurepack.common.gui.SlotUses;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

public class GuiInfusionGenerator extends GuiModificationBase<TileEntityInfusionGenerator>
{	
	public GuiInfusionGenerator(PlayerEntity pl, TileEntityInfusionGenerator tile)
	{
		super(new ContainerInfusionGenerator(pl.inventory, tile), "plasma_generator.png", pl.inventory);
	}
	
	@Override
	public TileEntityInfusionGenerator tile()
	{
		return ((ContainerInfusionGenerator)getMenu()).tile;
	}

	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
			
		if(tile().isBurning())
			this.blit(matrixStack, leftPos+76, topPos+39, 176, 0, 24, 33);
	}

	
	public static class ContainerInfusionGenerator extends ContainerSyncBase
	{
		TileEntityInfusionGenerator tile;
//		int lp, le;
		
		public ContainerInfusionGenerator(PlayerInventory inv, TileEntityInfusionGenerator tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotUses(tile, 0, 62, 11));
			this.addSlot(new Slot(tile, 1, 98, 11));
			this.addSlot(new SlotBaseXPOutput(inv.player, tile, 2, 142, 59));

			int l;
			int i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = this.slots.get(par2);
		        if(slot != null && slot.hasItem())
		        {
		        	if(slot.container == tile)
		        	{
		        		this.moveItemStackTo(slot.getItem(), 3, slots.size(), false);
		        	}
		        	else
		        	{
		        		this.moveItemStackTo(slot.getItem(), 0, 2, false);			
		        	}
		        	if(slot.getItem().getCount()<=0)
		        	{
		        		slot.container.setItem(slot.getSlotIndex(), ItemStack.EMPTY);
		        	}
		        		
		        }
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
			return true;
		}
		
//		@Override
//		public void addCraftingToCrafters(ICrafting c)
//		{
//			super.addCraftingToCrafters(c);
//			c.sendProgressBarUpdate(this, 0, (int)tile.getProgress());
//			c.sendProgressBarUpdate(this, 1, (int)tile.getPower());			
//		}
//		
//		@Override
//		public void detectAndSendChanges()
//		{
//			super.detectAndSendChanges();
//			for(ICrafting c : (List<ICrafting>) this.listeners)
//			{
//				if(lp != (int)tile.getProgress())
//				{
//					c.sendProgressBarUpdate(this, 0, (int)tile.getProgress());
//				}
//				if(le != (int)tile.getPower())
//				{
//					c.sendProgressBarUpdate(this, 1, (int)tile.getPower());		
//				}
//				
//				lp = (int)tile.getProgress();
//				le = (int)tile.getPower();
//			}
//		}
//		
//		//@ TODO: OnlyIn(Dist.CLIENT)
//		@Override
//		public void updateProgressBar(int id, int val)
//		{
//			super.updateProgressBar(id, val);
//			if(id==0)
//			{
//				tile.setProgress(val);
//			}
//			if(id==1)
//			{
//				tile.setPower(val);
//			}
//		}
	}
	
}
