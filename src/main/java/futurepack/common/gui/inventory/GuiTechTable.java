package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.client.research.LocalPlayerResearchHelper;
import futurepack.common.FPConfig;
import futurepack.common.block.inventory.TileEntityTechtable;
import futurepack.common.block.inventory.TileEntityTechtable.NeighbourContainer;
import futurepack.common.gui.SlotScrollable;
import futurepack.common.gui.SlotTechtableCrafting;
import futurepack.common.recipes.crafting.InventoryCraftingForResearch;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.depend.api.interfaces.IContainerScrollable;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;

public class GuiTechTable extends ActuallyUseableContainerScreen<GuiTechTable.ContainerTechTable> implements Runnable
{

	ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/tech_crafting_table.png");
	
	public GuiTechTable(PlayerEntity pl, TileEntityTechtable tile)
	{
		super(new ContainerTechTable(pl.inventory, tile), pl.inventory, "gui.techtable");
		imageWidth = 176;
		imageHeight = 206;
	}

	@Override
	public void init()
	{
		super.init();
		LocalPlayerResearchHelper.requestServerData(this);
		
		if(FPConfig.IS_RESEARCH_DEBUG.getAsBoolean())
		{
			addButton(new Button(this.leftPos-82, this.topPos+20, 80, 20, new StringTextComponent("Create Recipe"), B -> {
				HelperJSON.generateRecipe(container().table);
			}));
		}
	}
	
	@Override
	public void run() { }
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		this.font.draw(matrixStack, I18n.get("container.crafting", new Object[0]), 28, 6, 4210752);
		this.font.draw(matrixStack, I18n.get("container.inventory", new Object[0]), 8, this.imageHeight - 96 + 2, 4210752);
	}

	private boolean srcolling = false;
	private boolean pressed = false;
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		srcolling = false;
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
//		int k = (this.width - this.xSize) / 2;
//		int l = (this.height - this.ySize) / 2;
		this.blit(matrixStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);
		
		if(HelperComponent.isInBox(mouseX-this.leftPos, mouseY - this.topPos, 80, 16, 91, 27))
		{
			this.blit(matrixStack, this.leftPos+80, this.topPos+16, 194, 0, 11, 11);
			if(pressed)
			{
				this.blit(matrixStack, this.leftPos+80, this.topPos+16, 194, 11, 11, 11);
			}
		}
		
		for(int x=0;x<3;x++)
		{
			for(int y=0;y<3;y++)
			{
				if(isGreen(y*3+x))
				{
					blit(matrixStack, this.leftPos+25+x*18, this.topPos+16+y*18, 176, 0, 18, 18);
				}
			}
		}
		
		if(container().extraSlots > 0)
		{	      		
			//drawing the ExtraInv
			this.blit(matrixStack, this.leftPos -14 -ContainerTechTable.ROW_WIDTH*18, this.topPos+31, 0, 0, 7+ContainerTechTable.ROW_WIDTH*18+1, 7);//obere schr�ge
			this.blit(matrixStack, this.leftPos -6, this.topPos+31, this.imageWidth-7, 0, 7, 7);//oben rechte ecke
			this.blit(matrixStack, this.leftPos -14 -ContainerTechTable.ROW_WIDTH*18, this.topPos+38, 0, this.imageHeight-ContainerTechTable.ROW_COUNT*18-7, 7, ContainerTechTable.ROW_COUNT*18 +7); //linke schr�ge
			this.blit(matrixStack, this.leftPos -6, this.topPos+38, this.imageWidth-7, this.imageHeight-ContainerTechTable.ROW_COUNT*18-7, 7, ContainerTechTable.ROW_COUNT*18 +7); //rechte schr�ge
			this.blit(matrixStack, this.leftPos -7, this.topPos+38, this.imageWidth-7, this.imageHeight-ContainerTechTable.ROW_COUNT*18-7, 1, ContainerTechTable.ROW_COUNT*18 +7); //rechte schr�ge 2
			this.blit(matrixStack, this.leftPos -7 -ContainerTechTable.ROW_WIDTH*18, this.topPos+38+ContainerTechTable.ROW_COUNT*18, 4, this.imageHeight-7, ContainerTechTable.ROW_WIDTH*18+1, 7); // un tere schr�ge   
			
			int maxRows = container().extraSlots / ContainerTechTable.ROW_WIDTH;
			int over = container().extraSlots % ContainerTechTable.ROW_WIDTH;
			if(over>0)
				maxRows++;
			if(maxRows > ContainerTechTable.ROW_COUNT)//we need scrolling
			{
				int notVisible = maxRows - ContainerTechTable.ROW_COUNT;
				float size = ContainerTechTable.ROW_COUNT*18F / (notVisible+1);
				fill(matrixStack, this.leftPos -6, this.topPos+38, this.leftPos -3, this.topPos+38+ContainerTechTable.ROW_COUNT*18, 0xFF8b8b8b);
				
				if(HelperComponent.isInBox(mouseX, mouseY, this.leftPos -14 -ContainerTechTable.ROW_WIDTH*18, this.topPos+38, this.leftPos, this.topPos+38+ContainerTechTable.ROW_COUNT*18))
				{
					srcolling = true;
				}			
				if(size < 2)
					size = 2;
				
				int x = this.leftPos -6;
				int y = (int) (this.topPos+38 + container().scrollIndex * size);
								
				fill(matrixStack, x, y, x+3, (int) (y+size), 0xFFffffff);
				fill(matrixStack, x+1, y+1, x+3, (int) (y+size), 0xFF555555);
				fill(matrixStack, x+1, y+1, x+2, (int) (y+size-1), 0xFFc6c6c6);
				
				GlStateManager._color4f(1, 1, 1, 1);
			}
				
			for(int x=0;x<ContainerTechTable.ROW_WIDTH;x++)
			{
				for(int y=0;y<ContainerTechTable.ROW_COUNT;y++)
				{
					if(x+(y+container().scrollIndex)*ContainerTechTable.ROW_WIDTH <= container().extraSlots)
						blit(matrixStack, this.leftPos -7 -(ContainerTechTable.ROW_WIDTH)*18 + x*18, this.topPos+38+y*18, 25, 16, 18, 18);
					else
						blit(matrixStack, this.leftPos -7 -(ContainerTechTable.ROW_WIDTH)*18 + x*18, this.topPos+38+y*18, 7, 16, 18, 18);
				}
			}
			
			NeighbourContainer[] cont = container().connected;
			for(int i=0;i<cont.length;i++)
			{
				int col = 0xFF888888;
				boolean sel = false;
				if(i == container().selectedTab)
				{
					col = 0xFFc6c6c6;
					sel = true;
				}
				
				boolean upsideDown = i >= 3;			
				int x = this.leftPos -14 -ContainerTechTable.ROW_WIDTH*18 + (i%3)*27 +4;
				int y = this.topPos+13 + (int)((i/3)*(ContainerTechTable.ROW_COUNT+1.8)*18);
				
				if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+22, y+18))
				{
					col = 0xFFDDDD88;
				}
				this.minecraft.getTextureManager().bind(res);
				int my = y;
				if(upsideDown)
				{
					my -= 2;
					if(sel)
					{
						my -= 3;
					}
				}			
				fill(matrixStack, x, my+1, x+22, my+19 + (sel?3:0), col);
				GlStateManager._color4f(1, 1, 1, 1);
				blit(matrixStack, x, y- (sel&&upsideDown ? 2 : 1), 176 + (upsideDown?22:0),  upsideDown && !sel ? 23 : 22, 22, sel ? 21 : 20);
				
				RenderHelper.turnBackOn();
				HelperComponent.renderItemStackNormal(matrixStack, cont[i].item, x+2, y, getBlitOffset(), false);
			}
		}
	}
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) 
	{
		if(button == 0)
		{
			pressed = true;
			
			if(srcolling)
			{
				int maxRows = container().extraSlots / ContainerTechTable.ROW_WIDTH;
				int over = container().extraSlots % ContainerTechTable.ROW_WIDTH;
				if(over>0)
					maxRows++;
				int notVisible = maxRows - ContainerTechTable.ROW_COUNT;
				float size = ContainerTechTable.ROW_COUNT*18F / (notVisible+1);
				
				if(mouseX >=  this.leftPos -6 && mouseX <= leftPos)
				{
					double dy = mouseY - this.topPos-38;					
					container().scrollIndex = (int) (dy / size);
					return true;
				}
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button)
	{
		if(button == 0)
		{
			pressed = false;
			
			if(HelperComponent.isInBox(mouseX-this.leftPos, mouseY - this.topPos, 80, 16, 91, 27))
			{
				container().setup = true;
				FPPacketHandler.syncWithServer(container());
				return true;
			}
			if(container().extraSlots > 0)
			{	  
				NeighbourContainer[] cont = container().connected;
				for(int i=0;i<cont.length;i++)
				{
					int x = this.leftPos -14 -ContainerTechTable.ROW_WIDTH*18 + (i%3)*27 +4;
					int y = this.topPos+13 + (int)((i/3)*(ContainerTechTable.ROW_COUNT+1.8)*18);
					if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+22, y+18))
					{
						changeTab(i);
						return true;
					}
				}
			}
		}
		return super.mouseReleased(mouseX, mouseY, button);
	}
	
	@Override
	public boolean mouseScrolled(double mx, double my, double dweel)
	{
		if(srcolling)
		{
			int maxRows = container().extraSlots / ContainerTechTable.ROW_WIDTH;
			int over = container().extraSlots % ContainerTechTable.ROW_WIDTH;
			if(over>0)
				maxRows++;
			
			int notVisible = maxRows - ContainerTechTable.ROW_COUNT;
			
			if(dweel > 0 && container().scrollIndex > 0)
				container().scrollIndex--;
			else if(dweel < 0 && container().scrollIndex < notVisible)
				container().scrollIndex++;
			return true;
		}	
		return super.mouseScrolled(mx, my, dweel);
	}
	
	
	public ContainerTechTable container()
	{
		return this.getMenu();
	}
	
	public boolean isGreen(int slot)
	{
		ItemStack stack = container().table.getItem(slot);
		
		for(int j=10;j<container().table.getContainerSize();j++)
    	{
			ItemStack item = container().table.getItem(j);
			if(stack!=null)
    		{
				if(stack.sameItem(item) && ItemStack.tagMatches(stack, item))
    			{
					return true;
    			}
    		}
    	}
		return false;
	}
	
	public void changeTab(int tab)
	{
		container().scrollIndex = 0;
		container().selectedTab = tab;
		container().initTab();
		FPPacketHandler.syncWithServer(container());
	}
	
	public static class ContainerTechTable extends ActuallyUseableContainer implements IGuiSyncronisedContainer, IContainerScrollable
	{
		private TileEntityTechtable table;
		private InventoryCraftingForResearch crafting;
		private PlayerInventory pl;
		
		protected NeighbourContainer[] connected;
		
		protected boolean setup = false;
		
		//amount in scripped slot rows
		public int scrollIndex = 0;
		public int selectedTab = 0;
		
		public int extraSlots = 0;
		
		public static final int ROW_WIDTH = 4;
		public static final int ROW_COUNT = 7;
		
		
		private int slotCount;
		
		public ContainerTechTable(PlayerInventory pl, TileEntityTechtable tile)
		{
			table = tile;
			this.pl = pl;
			connected = tile.getSurroundingInventories();
			
			crafting = new InventoryCraftingTechtable(this, 3, 3, pl.player, table);
			
			int x, y;
			
			for(y=0;y<3;y++)
			{
				for(x=0;x<3;x++)
				{		
					this.addSlot(new Slot(crafting,y*3+x, 26+x*18, 17+y*18)); //input
				}
			}
			this.addSlot(new SlotTechtableCrafting(pl.player, crafting, table, 9, 124, 35));//output
			
			for(x=0;x<9;x++)
			{
				for(y=0;y<2;y++)
				{
					this.addSlot(new Slot(table, 10 + y*9+x, 8+x*18, 75+y*18));//storage
				}
			}
			
			HelperContainerSync.addInventorySlots(8, 124, pl, this::addSlot);

			this.slotsChanged(crafting);
			
			slotCount = this.slots.size();
			
			initTab();
		}
		
		public void initTab()
		{
			if(connected==null)
				return;
			
			for(int i=slots.size()-1;i>=slotCount;i--)
			{
				this.slots.remove(i);
//				this.inventoryItemStacks.remove(i);
			}
			
			extraSlots = 0;
			if(!table.getLevel().isClientSide)
			{
				extraSlots = connected[selectedTab].inventory.getSlots() ;
				for(int i=0;i<extraSlots;i++)
				{
					Slot s = this.addSlot(new SlotScrollable(this, connected[selectedTab].inventory, i, 0, 0));//since this is on the server we dont care about the possition
//					for (int j = 0; j < this.listeners.size(); ++j)
//					{
//						((IContainerListener)this.listeners.get(j)).sendSlotContents(this, s.slotNumber, connected[selectedTab].inventory.getStackInSlot(i));
//					}
				}
				int last = extraSlots -1;
				ItemStack original = connected[selectedTab].inventory.getStackInSlot(last);
				connected[selectedTab].inventory.setStackInSlot(last, new ItemStack(Items.DEBUG_STICK));
				broadcastChanges();
				connected[selectedTab].inventory.setStackInSlot(last, original);
				broadcastChanges();
			}
		}
		
		@Override
		public Slot getSlot(int slotId)
		{
			if(table.getLevel().isClientSide)
			{			
				while(slots.size() <= slotId)
				{
					this.addSlot(new SlotScrollable(this, connected[selectedTab].inventory, extraSlots, -6-ROW_WIDTH*18 +(extraSlots%ROW_WIDTH)*18 , 39));
					if(connected[selectedTab].inventory.getSlots() > extraSlots+1)
						extraSlots++;
				}
			}
			return super.getSlot(slotId);
		}
		
		@Override
		public void slotsChanged(IInventory inventoryIn)
		{
			ItemStack it = ItemStack.EMPTY;
			table.last = table.getLevel().getRecipeManager().getRecipeFor(IRecipeType.CRAFTING, crafting, table.getLevel()).orElse(null);
			if(table.last!=null)
			{
				it = table.last.assemble(crafting);
			}
		
			table.setItem(9, it);
			
			super.slotsChanged(inventoryIn);
		}
		
		@Override
		public void broadcastChanges()
		{
			
			super.broadcastChanges();
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
			Slot slot = this.getSlot(index);
			if (slot != null && slot.hasItem())
	        {
				ItemStack item = slot.getItem();
				itemstack = item.copy();
				
				if(index<9)
				{
					//Shift Click at Crafting Field
					if(!this.moveItemStackTo(item, 9, 28, false))
					{
						if(!this.moveItemStackTo(item, 28, 64, false))
						{	
							return ItemStack.EMPTY;
						}
					}
					
				}
				else if(index==9)
				{
					//Shift Click at Crafting Output
					if(!this.moveItemStackTo(item, 28, 64, false))
					{	
						return ItemStack.EMPTY;
					}
					slot.setChanged();
				}
				else if(index<28)
				{
					//Shift Click at additionan Storage
					if(!this.moveItemStackTo(item, 28, 64 , false))
					{	
						return ItemStack.EMPTY;
					}
				}
				else if(index<46)
				{
					//Shift Click at additionan Storage
					if(!this.moveItemStackTo(item, 9, 28, false))
					{	
						return ItemStack.EMPTY;
					}
				}
				else
				{
					//Shift Click at Inventory
					if(!this.moveItemStackTo(item, 9, 28, false))
					{	
						return ItemStack.EMPTY;
					}
				}
				
				if (item.getCount() == 0)
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }
				
				slot.onTake(playerIn, item);
	        }

	        return itemstack;
		}
		
		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return true;
		}

		@Override
		public void writeToBuffer(PacketBuffer nbt)
		{
			nbt.writeVarInt(selectedTab);
			
			nbt.writeByte(setup?1:0);
			
			setup = false;
		}

		@Override
		public void readFromBuffer(PacketBuffer nbt)
		{
			selectedTab = nbt.readVarInt();
			initTab();
			
			if(nbt.readByte() == 1)
			{			
				//Server
				for(int i=0;i<9;i++)
				{
					quickMoveStack(this.pl.player, i);
				}
				slotsChanged(this.table);
				broadcastChanges();
			}
		}

		
		@Override
		public int getScollIndex()
		{
			return scrollIndex;
		}
		

		@Override
		public int getRowWidth() 
		{
			return ROW_WIDTH;
		}

		@Override
		public int getRowCount()
		{
			return ROW_COUNT;
		}

	}
}
