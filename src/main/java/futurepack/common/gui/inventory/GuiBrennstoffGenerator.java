package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.common.block.inventory.TileEntityBrennstoffGenerator;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.items.SlotItemHandler;

public class GuiBrennstoffGenerator extends ActuallyUseableContainerScreen<GuiBrennstoffGenerator.ContainerBrennstoffGenerator>
{

	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/brennstoff_generator.png");
	private TileEntityBrennstoffGenerator stile = null;
	
	public GuiBrennstoffGenerator(PlayerEntity pl, TileEntityBrennstoffGenerator tile)
	{
		super(new ContainerBrennstoffGenerator(pl.inventory, tile), pl.inventory);
		stile = tile;
	}
	
	private TileEntityBrennstoffGenerator tile()
	{
		return getMenu().tile;
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		//this.font.drawString(matrixStack, I18n.format("container.brennstoffgenerator", new Object[0]), 80, 4, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 80, this.ySize - 96 + 4, 4210752);
		
		PartRenderer.renderNeonTooltip(matrixStack, leftPos, topPos, 7, 7, tile().getCapability(CapabilityNeon.cap_NEON).orElse(null), mouseX, mouseY);
	}
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int var2, int var3) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
			
		if(stile.getBurntime() > 0 && stile.getMaxBurntime() > 0)
		{
			int burn = (int) (((float)stile.getBurntime() / (float)stile.getMaxBurntime()) * 14.0f);
			
			if(tile().isBurning())
				this.blit(matrixStack, k + 63, l + 42 - burn + 14, 176, 14 - burn, 14, burn);
		}

		PartRenderer.renderNeon(matrixStack, k+7, l+7, tile().getCapability(CapabilityNeon.cap_NEON).orElse(null), var2, var3);
	}

	
	public static class ContainerBrennstoffGenerator extends ContainerSyncBase
	{
		TileEntityBrennstoffGenerator tile;
		
		public ContainerBrennstoffGenerator(PlayerInventory inv, TileEntityBrennstoffGenerator tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotItemHandler(tile.getGui(), 0, 62, 59));
			this.addSlot(new SlotItemHandler(tile.getGui(), 1, 26, 11));

			int l;
			int i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = this.slots.get(par2);
		        if(slot != null && slot.hasItem())
		        {
		        	if(par2 <= 1)
		        	{
		        		this.moveItemStackTo(slot.getItem(), 3, slots.size(), false);
		        	}
		        	else
		        	{
		        		this.moveItemStackTo(slot.getItem(), 0, 1, false);		// 0 2	
		        	}
		        	if(slot.getItem().getCount()<=0)
		        	{
		        		slot.set(ItemStack.EMPTY);
		        	}
		        		
		        }
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(PlayerEntity var1)
		{
			return true;
		}
	}
	
}
