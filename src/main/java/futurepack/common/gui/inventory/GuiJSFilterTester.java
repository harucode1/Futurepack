package futurepack.common.gui.inventory;

import java.util.List;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperItemFilter;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.CraftResultInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.IntReferenceHolder;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;

public class GuiJSFilterTester extends ActuallyUseableContainerScreen<GuiJSFilterTester.ContainerJSFilterTester> 
{
	private Button save;
	private final ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/filter_tester.png");
	
	
	public GuiJSFilterTester(PlayerEntity pl) 
	{
		super(new ContainerJSFilterTester(pl.inventory), pl.inventory, "item.futurepack.script_filter");
		imageWidth = 176;
		imageHeight = 228;
	}
	
	@Override
	public void init(Minecraft game, int screenWidth, int screenHeight) 
	{	
		super.init(game, screenWidth, screenHeight);

		int w = 18+this.font.width("Edit Script");
		addButton(save = new Button(leftPos +imageWidth - 7 -w, topPos+17, w, 20, new StringTextComponent("Edit Script"), b ->  {
			getMenu().type = 0;
			FPPacketHandler.syncWithServer(getMenu());
		}));
		addButton(new Button(leftPos + 26, topPos+17, 18+this.font.width("Test"), 20, new StringTextComponent("Test"), b ->  {
			getMenu().type = 1;
			FPPacketHandler.syncWithServer(getMenu());
		}));
		
		
		//TODO: add multiple slot for testing & tets buttons
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) 
	{

		return super.mouseClicked(mouseX, mouseY, button);
	}
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) 
	{

		return super.mouseReleased(mouseX, mouseY, button);
	}
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
		
		
//		this.font.drawSplitString("Compiler: " + getContainer().compileMsg, guiLeft+5, guiTop + (ySize-56)+20+15, xSize-10, 0xFF11FF11);
		
		//TODO remove resizing, gui has mutliple filter slots; add area with data of filter.
		
		//93px height, 162px 
		
		int lines =  93 / this.font.lineHeight;
		ItemStack filter = getFilter();
		CompoundNBT nbt = filter.getOrCreateTagElement("display");
		if(nbt.contains("Lore"))
		{
			ListNBT list = nbt.getList("Lore", 8);//StringNBT
			String s = "";
			for(int i=0;i<Math.min(lines, list.size());i++)
			{
				s += list.getString(i);
			}
			List<IReorderingProcessor> widthWrapped = this.font.split(new StringTextComponent(s), 160);
			for(IReorderingProcessor rp : widthWrapped)
			{
				this.font.drawShadow(matrixStack, rp, leftPos +8, topPos +40, 0xFFFFDDDD);
			}
			
		}
		else
		{
			//get data string
			
			CompoundNBT script = filter.getTagElement("script");
			String s = "";
			if(script != null && script.contains("extraData"))
			{
				s = script.getCompound("extraData").toString();
			}

			List<IReorderingProcessor> widthWrapped = this.font.split(new StringTextComponent(s), 160);
			for(IReorderingProcessor rp : widthWrapped)
			{
				this.font.drawShadow(matrixStack, rp, leftPos +8, topPos +40, 0xFFFFFFFF);
			}
		}
		
		byte state = getMenu().state;
		HelperComponent.renderSymbol(matrixStack, leftPos+74, topPos+18, getBlitOffset(), state==0 ? 16 : (state==2 ? 18 : 19));

	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY) 
	{
		super.renderLabels(matrixStack, mouseX, mouseY);
		
		this.font.draw(matrixStack, this.title.getString(), 8.0F, 6.0F, 4210752);
		this.font.draw(matrixStack, this.inventory.getDisplayName().getString(), 8.0F, this.imageHeight - 96 + 2, 4210752);
	}
	
	@Override
	public void tick() 
	{
		super.tick();
	}
	
	@Override
	public void removed() 
	{
		super.removed();
	}
	
	@Override
	public void mouseMoved(double mouseX, double mouseY) 
	{
		super.mouseMoved(mouseX, mouseY);
	}
	
	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) 
	{
		if(hasControlDown() && keyCode == GLFW.GLFW_KEY_S)
		{
			save.mouseClicked(save.x+1, save.y+1, 0);//triggering the button with all actiaveted checks
			return true;
		}
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	private ItemStack getFilter()
	{
		return getMenu().pl.inventory.getItem(getMenu().selected);
	}
	
	public static class ContainerJSFilterTester extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		private PlayerEntity pl;
		private final int selected;
		private CraftResultInventory inventory;
		
		private byte type; 
		private byte state = 0;
		
		public ContainerJSFilterTester(PlayerInventory inv)
		{
			this.pl = inv.player;
			
			selected = inv.selected;
			//test slot at 8, 19s
			inventory = new CraftResultInventory();
			addSlot(new Slot(inventory, 0, 8, 19));
			
			HelperContainerSync.addInventorySlots(8, 146, inv, this::addSlot);
			
			addDataSlot(new IntReferenceHolder()
			{
				@Override
				public void set(int p) 
				{
					state = (byte) p;
				}
				
				@Override
				public int get() 
				{
					return state;
				}
			});
		}
		
		@Override
		public ItemStack clicked(int slotId, int dragType, ClickType clickTypeIn, PlayerEntity player) 
		{
			if(slotId > 0)
			{
				Slot s = getSlot(slotId);
				if(s.getSlotIndex() == selected)//can not move filter item
				{
					return ItemStack.EMPTY;
				}
			}
			
			return super.clicked(slotId, dragType, clickTypeIn, player);
		}
		
		@Override
		public boolean stillValid(PlayerEntity playerIn) 
		{
			return true;
		}

		
		@Override
		public void writeToBuffer(PacketBuffer buf) 
		{
			buf.writeByte(type);
		}

		@Override
		public void removed(PlayerEntity playerIn) 
		{
			InventoryHelper.dropContents(playerIn.level, playerIn, inventory); //what if everything was as easy as this?
			super.removed(playerIn);
		}

		private void testSlot(int inventorySlot)
		{
			state = 0;
			ItemStack filterItem = pl.inventory.getItem(selected);
			filterItem.getOrCreateTagElement("display").remove("Lore");
			IItemFilter filter = HelperItemFilter.getFilter(filterItem);
			ItemStack totest = inventory.getItem(inventorySlot);
			if(!totest.isEmpty())
			{
				boolean accepted = filter.test(totest);
				if(accepted)
				{
					filter.amountTransfered(totest);
				}
				state = (byte) (accepted ? 1 : 2);
			}
			
		}

		@Override
		public void readFromBuffer(PacketBuffer buf) 
		{
			type = buf.readByte();
			switch(type)
			{
			case 0:
				FPGuiHandler.JS_FILTER_EDITOR.openGui((ServerPlayerEntity)pl, (Object[])null);
				break;
			case 1:
				testSlot(0);
				break;
			}
		}
	}
}
