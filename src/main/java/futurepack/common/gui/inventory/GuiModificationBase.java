package futurepack.common.gui.inventory;

import java.util.Arrays;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.gui.PartRenderer;
import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.util.ResourceLocation;

public abstract class GuiModificationBase<T extends TileEntityModificationBase> extends ActuallyUseableContainerScreen
{
	protected ResourceLocation res;
	int nx=7, ny=7;
	
	public GuiModificationBase(Container cont, String gui, PlayerInventory inv)
	{
		super(cont, inv, "gui." + gui.replace(".png", ""));
		res = new ResourceLocation(Constants.MOD_ID, "textures/gui/" + gui);
		
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	public abstract T tile();
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		this.minecraft.getTextureManager().bind(res);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
		
		PartRenderer.renderNeon(matrixStack, leftPos+nx, topPos+ny, tile().energy, mouseX, mouseY);
		HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos, getBlitOffset(), 29);
		if(!tile().getInventory().canWork())
		{	
			HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos+18, getBlitOffset(), 23);
		}
		this.minecraft.getTextureManager().bind(res);
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int mouseX, int mouseY)
	{
		//Removed to remove default text (inventory and gui name) super.drawGuiContainerForegroundLayer(matrixStack, mouseX, mouseY);
		
		if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos , -20, 0, -20+18, 0+18))
		{
			PartRenderer.drawHoveringTextFixedString(matrixStack, Arrays.asList(I18n.get("gui.futurepack.machine.scrench")), mouseX-leftPos +12, mouseY-topPos+4, -1, font);
		}
		
		if(!tile().getInventory().canWork())
		{
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos , -20, 18, -20+18, 18+18))
			{
				PartRenderer.drawHoveringTextFixedString(matrixStack, Arrays.asList(I18n.get("gui.futurepack.machine.burned")), mouseX-leftPos +12, mouseY-topPos+4, -1, font);
			}
		}
		
		PartRenderer.renderNeonTooltip(matrixStack, leftPos, topPos, nx, ny, tile().energy, mouseX, mouseY);
	}

}
