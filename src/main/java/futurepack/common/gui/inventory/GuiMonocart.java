package futurepack.common.gui.inventory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockMonocartWaypoint;
import futurepack.api.interfaces.IItemNeon;
import futurepack.common.entity.monocart.EntityMonocart;
import futurepack.common.entity.monocart.EntryWaypoint;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.ISyncable;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.SlotItemHandler;

public class GuiMonocart extends ActuallyUseableContainerScreen<GuiMonocart.ContainerMonocart>
{
	private ResourceLocation ways = new ResourceLocation(Constants.MOD_ID, "textures/gui/monocart.png");
	private ResourceLocation inv = new ResourceLocation(Constants.MOD_ID, "textures/gui/monocart_inventar.png");
	
	public GuiMonocart(PlayerEntity pl, EntityMonocart cart)
	{
		super(new ContainerMonocart(pl.inventory, cart), pl.inventory, "gui.monocart");
		this.imageHeight = 186;
		this.imageWidth = 176;
	}

	
	private int rMX=0, rMY=0;
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
		rMX = mouseX;
		rMY = mouseY;
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(container().isInvMode() ? inv : ways);
		leftPos = (this.width - this.imageWidth) / 2;
		topPos = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);

		//Hover for the switch button
		if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 151, 5, 169, 15))
		{
			this.blit(matrixStack, leftPos+151, topPos+5, 176, 0, 18, 10);
		}
		renderEnergyBar(matrixStack, leftPos+100, topPos+7, container().cart.getPower() / container().cart.getMaxPower());
		
		if(!container().isInvMode())
		{
			renderWaypoint(matrixStack,leftPos,topPos,mouseX, mouseY);
		}
	}
	
	private void renderEnergyBar(MatrixStack matrixStack, int x, int y, float energy)
	{
		int pixl = (int)(49 * energy);
		this.blit(matrixStack, x +49-pixl, y, 176 +49-pixl, 10, pixl, 6);
	}
	
	int scroll1=0, scroll2=0;
	
	private void renderWaypoint(MatrixStack matrixStack, int x, int y, int mouseX, int mouseY)
	{
		if(container().C_mono==null || container().C_allWay==null)
		{
			return;
		}
		
		float maxscroll1 = container().C_allWay.size()-6;
		maxscroll1 = maxscroll1 <0?0:maxscroll1;
		float maxscroll2 = container().C_mono.size()-8;
		maxscroll2 = maxscroll2<0?0:maxscroll2;
		float bar1=0,bar2=0;
		if(maxscroll1>0)
			bar1 = scroll1/maxscroll1 * 39F;
		if(maxscroll2>0)
			bar2 = scroll2/maxscroll2 * 59F;
		
		blit(matrixStack, x+160, y+39 + (int)bar1, 177, 24, 7, 7);
		blit(matrixStack, x+160, y+104 + (int)bar2, 177, 24, 7, 7);
		
		
		for(int i=0;i<6;i++)
		{
			int pos = i+scroll1;
			if(pos >= container().C_allWay.size())
				break;
			
			String s = container().C_allWay.get(pos);
			if(s.length()>29)
			{
				s = s.substring(0, 29) + ".";
			}
			this.font.draw(matrixStack, s, x+11, y+33 + i*10, 0xffffff);
			this.minecraft.getTextureManager().bind(container().isInvMode() ? inv : ways);
			boolean hover=false;
			if(HelperComponent.isInBox(mouseX-x, mouseY-y, 150, 32+i*10, 159, 42+i*10))
			{
				hover=true;
			}
			this.blit(matrixStack, x+150, y+32 + i*10, 185, hover ? 34 : 24, 9, 10);
		}
		
		for(int i=0;i<8;i++)
		{
			int pos = i+scroll2;
			if(pos >= container().C_mono.size())
				break;
			
			String s = container().C_mono.get(pos);
			if(s.length()>29)
			{
				s = s.substring(0, 29) + ".";
			}
			this.font.draw(matrixStack, s, x+11, y+98 + i*10, 0xffffff);
			this.minecraft.getTextureManager().bind(container().isInvMode() ? inv : ways);
			boolean hover1=HelperComponent.isInBox(mouseX-x, mouseY-y, 132, 97+i*10, 141, 107+i*10);
			boolean hover2=HelperComponent.isInBox(mouseX-x, mouseY-y, 141, 97+i*10, 150, 107+i*10);
			boolean hover3=HelperComponent.isInBox(mouseX-x, mouseY-y, 150, 97+i*10, 159, 107+i*10);
			this.blit(matrixStack, x+132, y+97 + i*10, 194, hover1 ? 34 : 24, 9, 10);
			this.blit(matrixStack, x+141, y+97 + i*10, 203, hover2 ? 34 : 24, 9, 10);
			this.blit(matrixStack, x+150, y+97 + i*10, 212, hover3 ? 34 : 24, 9, 10);
			//177 24 7x7
		}
		
	}
	
	public ContainerMonocart container()
	{
		return this.getMenu();
	}
	
	@Override
	public boolean mouseScrolled(double mx, double my, double d) 
	{
		if(d!=0)
		{
			float maxscroll1 = container().C_allWay.size()-6;
			maxscroll1 = maxscroll1 <0?0:maxscroll1;
			float maxscroll2 = container().C_mono.size()-8;
			maxscroll2 = maxscroll2<0?0:maxscroll2;
			
			if(d<0)
				d=1;
			else if(d>0)
				d=-1;
			
			if(HelperComponent.isInBox(rMX-leftPos, rMY-topPos, 9, 32, 159, 92))
			{
				scroll1+=d;
				if(scroll1<0)
					scroll1=0;
				if(scroll1>maxscroll1)
					scroll1=(int) maxscroll1;
			}
			if(HelperComponent.isInBox(rMX-leftPos, rMY-topPos, 9, 97, 159, 177))
			{
				scroll2+=d;
				if(scroll2<0)
					scroll2=0;
				if(scroll2>maxscroll2)
					scroll2=(int) maxscroll2;
			}
		}
		return super.mouseScrolled(mx, my, d);
	}
	
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int state)
	{
		if(state==0)
		{
			int k = (this.width - this.imageWidth) / 2;
			int l = (this.height - this.imageHeight) / 2;
			if(HelperComponent.isInBox(mouseX-k, mouseY-l, 151, 5, 169, 15))
			{
				container().setInventoryMode(!container().isInvMode());
				return true;
			}
			
			if(HelperComponent.isInBox(mouseX-k, mouseY-l, 160, 32, 167, 39))
			{
				scroll1--;
				if(scroll1<0)
					scroll1=0;
				
				return true;
			}
			else if(HelperComponent.isInBox(mouseX-k, mouseY-l, 160, 85, 167, 92))
			{
				scroll1++;
				if(5+scroll1 >= container().C_allWay.size())
				{
					scroll1 = container().C_allWay.size()-6;
					if(scroll1<0)
						scroll1=0;
				}
				return true;
			}
			else if(HelperComponent.isInBox(mouseX-k, mouseY-l, 160, 97, 167, 104))
			{
				scroll2--;
				if(scroll2<0)
					scroll2=0;
				return true;
			}
			else if(HelperComponent.isInBox(mouseX-k, mouseY-l, 160, 170, 167, 177))
			{
				scroll2++;
				if(7+scroll2 >= container().C_mono.size())
				{
					scroll2 = container().C_mono.size()-8;
					if(scroll2<0)
						scroll2=0;
				}
				return true;
			}
			
			if(container().C_mono!=null && container().C_allWay!=null)
			{
				for(int i=0;i<6;i++)
				{
					int pos = i+scroll1;
					if(pos >= container().C_allWay.size())
						break;			
					String s = container().C_allWay.get(pos);
					if(HelperComponent.isInBox(mouseX-k, mouseY-l, 150, 32+i*10, 159, 42+i*10))
					{
						container().C_mono.add(s);
						FPPacketHandler.syncWithServer(container());
					}
				}
				
				for(int i=0;i<8;i++)
				{
					int pos = i+scroll2;
					if(pos >= container().C_mono.size())
						break;
					
					String s = container().C_mono.get(pos);
					if(HelperComponent.isInBox(mouseX-k, mouseY-l, 132, 97+i*10, 141, 107+i*10))
					{
						container().C_mono.remove(pos);
						FPPacketHandler.syncWithServer(container());
					}
					if(HelperComponent.isInBox(mouseX-k, mouseY-l, 141, 97+i*10, 150, 107+i*10))
					{
						if(pos+1<container().C_mono.size())
						{
							String way2 = container().C_mono.get(pos+1);
							container().C_mono.set(pos+1, s);
							container().C_mono.set(pos, way2);
						}
						FPPacketHandler.syncWithServer(container());
					}
					if(HelperComponent.isInBox(mouseX-k, mouseY-l, 150, 97+i*10, 159, 107+i*10))
					{
						if(pos>0)
						{
							String way2 = container().C_mono.get(pos-1);
							container().C_mono.set(pos-1, s);
							container().C_mono.set(pos, way2);
						}
						FPPacketHandler.syncWithServer(container());
					}
				}
				
			}
		}
		return super.mouseReleased(mouseX, mouseY, state);
	}

	
	public static class ContainerMonocart extends ActuallyUseableContainer implements IGuiSyncronisedContainer, ISyncable
	{
		private PlayerEntity player;
		private EntityMonocart cart;
		
		private List<Slot> buffer;
		private boolean invMode = false;
		
		//Server
		private ArrayList<Integer> S_mono;
		private ArrayList<EntryWaypoint> S_allWay;
		
		//Client
		private ArrayList<String> C_mono;
		private ArrayList<String> C_allWay;
		
		
		public ContainerMonocart(PlayerInventory inv, EntityMonocart cart)
		{
			this.player=inv.player;
			this.cart = cart;
			
			cart.setPaused(true);
			
			int l, i1;
			
			for (l = 0; l < 6; ++l)
			{
				this.addSlot(new SlotItemHandler(cart.getEnergyHandler(), l, 62 + l * 18, 18));
			}
			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new SlotItemHandler(cart.getGui(), i1 + l * 9, 8 + i1 * 18, 38 + l * 18));
				}
			}
				
								
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 104 + l * 18));
				}
			}
				
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 162));
			}
			
			buffer = new ArrayList<Slot>(this.slots);
			setInventoryMode(!player.level.isClientSide);
			
			
			if(!player.level.isClientSide)
			{
				Collection<ParentCoords> list = cart.getAllWaypoints();
				
				if(S_allWay==null)
				{
					S_allWay = new ArrayList<EntryWaypoint>();
					
					for(ParentCoords coord : list)
					{
						World w = cart.level;
						BlockState state = w.getBlockState(coord);
						IBlockMonocartWaypoint waypoint = (IBlockMonocartWaypoint) state.getBlock();
						if(waypoint.isWaypoint(w, coord, state))
						{
							String s = waypoint.getName(w, coord, state).getString();
							S_allWay.add(new EntryWaypoint(s, coord));
						}
					}
				}
				
				if(S_mono==null)
				{
					S_mono = new ArrayList<Integer>();
					if(cart.getWaypoint()!=null)
					{
						BlockPos[] poss = cart.getWaypoint();
						for(BlockPos pos : poss)
						{
							int id = S_allWay.indexOf(new EntryWaypoint("", new ParentCoords(pos, null)));
							if(id>=0)
							{
								S_mono.add(id);
							}
						}
						
					}
				}
			}
		}
		
		@Override
		public Slot getSlot(int slotId)
		{
			if(!isInvMode())
			{
				return buffer.get(slotId);
			}
			return super.getSlot(slotId);
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int index)
		{
			Slot slot = this.slots.get(index);

			if (slot != null && slot.hasItem())
			{
				if(slot.container == pl.inventory)
				{
					ItemStack itemst = slot.getItem();
					if(itemst.getItem() instanceof IItemNeon && ( ((IItemNeon)itemst.getItem()).isNeonable(itemst) || ((IItemNeon)itemst.getItem()).getNeon(itemst)>0 ))
					{
						this.moveItemStackTo(itemst, 0, 6, false);
					}
					else
					{
						this.moveItemStackTo(itemst, 6, 3*9+6, false);
					}
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 3*9+6, this.slots.size(), false);
				}
				
				if (slot.getItem().getCount() == 0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		
		private int synced = 0;
		
		@Override
		public void broadcastChanges() 
		{
			synced++;
			if(synced==2)
			{
				FPPacketHandler.sendToClient(this, player);
			}
			
			super.broadcastChanges();
		}
		
		@Override
		public void writeAdditional(DataOutputStream dat) throws IOException 
		{
			dat.writeInt(S_allWay.size());
			for(EntryWaypoint entry : S_allWay)
			{
				dat.writeUTF(entry.name);
			}
			dat.writeInt(S_mono.size());
			for(Integer entry : S_mono)
			{
				dat.writeInt(entry);
			}			
		}

		@Override
		public void readAdditional(DataInputStream data) throws IOException 
		{
			int size = data.readInt();
			C_allWay = new ArrayList<String>(size);
			for(int i=0;i<size;i++)
			{
				C_allWay.add(data.readUTF());
			}
			size = data.readInt();
			C_mono = new ArrayList<String>(size);//muss ids senden!!
			for(int i=0;i<size;i++)
			{
				int idd = data.readInt();
				C_mono.add(C_allWay.get(idd));
			}
		}
		
		@Override
		public boolean stillValid(PlayerEntity pl)
		{
			return HelperResearch.canOpen(pl, cart);
		}
		
		public void setInventoryMode(boolean enabled)
		{
			this.slots.clear();
			
			invMode = enabled;
			if(enabled)
			{
				this.slots.addAll(buffer);
			}
		}
		
		public boolean isInvMode()
		{
			return invMode;
		}
		
		@Override
		public void removed(PlayerEntity playerIn)
		{
			super.removed(playerIn);
			if (playerIn.level.getBlockEntity(this.cart.blockPosition()) == null) {
				cart.setPaused(false);
			}
		}

		@Override
		public void writeToBuffer(PacketBuffer buf) 
		{
			int[] ints = new int[C_mono.size()];
			
			for(int j=0;j<ints.length;j++)
			{
				String find = C_mono.get(j);
				ints[j]=-1;
				for(int i=0;i<C_allWay.size();i++)
				{
					if(find == C_allWay.get(i))
					{
						ints[j]=i;
						break;
					}
				}
			}
			buf.writeVarIntArray(ints);
		}

		@Override
		public void readFromBuffer(PacketBuffer buf) 
		{
			int[] ints = buf.readVarIntArray();
			ArrayList<BlockPos> pos = new ArrayList<BlockPos>(ints.length);
			for(int i=0;i<ints.length;i++)
			{
				if(ints[i]!=-1)
				{
					pos.add(S_allWay.get(ints[i]).coords);
				}
				
			}
			BlockPos[] bl = pos.toArray(new BlockPos[pos.size()]);
			cart.setWaypoints(bl);
		}
	}
}
