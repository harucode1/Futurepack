package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.api.interfaces.IItemNeon;
import futurepack.common.block.inventory.TileEntityDroneStation;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.items.SlotItemHandler;

public class GuiDroneStation extends ActuallyUseableContainerScreen<GuiDroneStation.ContainerDroneStation>
{

	//private TileEntityBaterieBox tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/drone_station.png");
	
	public GuiDroneStation(PlayerEntity pl, TileEntityDroneStation tile)
	{
		super(new ContainerDroneStation(pl.inventory,tile), pl.inventory, "gui.dronestation");
		imageHeight = 192;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int var2, int var3) 
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.minecraft.getTextureManager().bind(res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		//this.font.drawString(matrixStack, I18n.format("container.dronestation", new Object[0]), 6, 6, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 6, this.ySize - 96 + 2, 4210752);
	}
	
	
	public static class ContainerDroneStation extends ActuallyUseableContainer
	{
		TileEntityDroneStation tile;
		
		public ContainerDroneStation(PlayerInventory inv, TileEntityDroneStation tile)
		{
			this.tile = tile;
			slots.clear();
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new SlotItemHandler(tile.getGui(), i1 + l * 9, 8 + i1 * 18, 18 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new SlotItemHandler(tile.getGui(), 27+l, 8 + l * 18, 78));
			}
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 110 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 168));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity pl, int p_82846_2_)
		{
			Slot slot = this.slots.get(p_82846_2_);

			if (slot != null && slot.hasItem())
			{
				if(slot.container == pl.inventory)
				{
					ItemStack itemst = slot.getItem();
					if(itemst.getItem() instanceof IItemNeon && ( ((IItemNeon)itemst.getItem()).isNeonable(itemst) || ((IItemNeon)itemst.getItem()).getNeon(itemst)>0 ))
					{
						this.moveItemStackTo(itemst, 3*9, 4*9, false);
					}
					else
					{
						this.moveItemStackTo(itemst, 0, 3*9, false);
					}
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 4 * 9, this.slots.size(), false);
				}
				
				if (slot.getItem().getCount() == 0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}


		@Override
		public boolean stillValid(PlayerEntity p_75145_1_)
		{
			return true;
		}
		
//		private class SlotBaterie extends SlotUses
//		{
//
//			public SlotBaterie(IInventory par1iInventory, int par2, int par3, int par4) 
//			{
//				super(par1iInventory, par2, par3, par4);
//			}
//			
//			@Override
//			public int getSlotStackLimit() 
//			{
//				return 1;
//			}
//		}
	}
}
