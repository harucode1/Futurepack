package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;

import futurepack.api.Constants;
import futurepack.common.FuturepackTags;
import futurepack.common.block.modification.machines.BlueprintAssemblyRecipe;
import futurepack.common.gui.SlotUses;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageResearchResponse;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.CraftResultInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.PacketDistributor;

public class GuiAssemblyRezeptCreator extends ActuallyUseableContainerScreen<GuiAssemblyRezeptCreator.ContainerAssemblyRezeptCreator>
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/rezeptassembly.png");	
	
	public GuiAssemblyRezeptCreator(PlayerEntity pl, BlockPos pos)
	{
		super(new ContainerAssemblyRezeptCreator(pl, pos), pl.inventory, "gui.assemply.recipe.creator");
	}
	
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	public void init(Minecraft p_init_1_, int p_init_2_, int p_init_3_) 
	{
		super.init(p_init_1_, p_init_2_, p_init_3_);
		HelperContainerSync.RestoreCursorPos();
	}
	
	@Override
	protected void renderLabels(MatrixStack matrixStack, int p_146979_1_, int p_146979_2_)
    {
        this.font.draw(matrixStack, I18n.get("gui.futurepack.assembly.recipe.title", new Object[0]), 28-21, 6, 4210752);
        this.font.draw(matrixStack, I18n.get("container.inventory", new Object[0]), 28-15, this.imageHeight - 96 +3, 4210752);
    }
	
	@Override
	protected void renderBg(MatrixStack matrixStack, float var1, int mx,int my) 
	{
		 GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		 this.minecraft.getTextureManager().bind(res);
		 int k = (this.width - this.imageWidth) / 2;
		 int l = (this.height - this.imageHeight) / 2;
		 this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);		 
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int w) 
	{
		ContainerAssemblyRezeptCreator c = getMenu();
		if(HelperComponent.isInBox(mx -leftPos, my -topPos, 116, 10, 134, 28) && w==0)
		{		
			 c.back = true;
			 HelperContainerSync.SaveCursorPos();
			 FPPacketHandler.syncWithServer(c);
		}
		
		return super.mouseReleased(mx, my, w);
	}
	
	public static class ContainerAssemblyRezeptCreator extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		
		private boolean back = false;
		public IInventory in1 = new CraftResultInventory();
		public IInventory in2 = new CraftResultInventory();
		public IInventory in3 = new CraftResultInventory();
		
		public IInventory out = new CraftResultInventory();
		public IInventory recIn = new CraftResultInventory();
		public IInventory recOut = new CraftResultInventory();
		
		public PlayerEntity player;
		private BlockPos pos;
		
		public ContainerAssemblyRezeptCreator(PlayerEntity pl, BlockPos pos)
		{
			player = pl;
			this.pos = pos;
			int x=26,y=40;
			this.addSlot(new SlotInput(in1, 0, x,y));
			this.addSlot(new SlotInput(in2, 0, x+18,y));
			this.addSlot(new SlotInput(in3, 0, x+18*2,y));
			
			x=98;y=11;
			this.addSlot(new SlotInput(recIn, 0, x,y));
			x=134;y=61;
			this.addSlot(new GuiConstructionTable.SlotClick(recOut, recIn, 0, x,y));
			
			this.addSlot(new SlotOutput(out, 0, 98, 40));
			
			HelperContainerSync.addInventorySlots(8, 84, player.inventory, this::addSlot);
			
			if(!pl.level.isClientSide && pl instanceof ServerPlayerEntity)
			{
				ServerPlayerEntity mp = (ServerPlayerEntity) pl;
				MessageResearchResponse mes =  new MessageResearchResponse(pl.getServer(), CustomPlayerData.getDataFromPlayer(pl));
				FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> mp), mes);
			}
		}
		
		@Override
		public void writeToBuffer(PacketBuffer buf)  
		{
			buf.writeBoolean(back);
		}

		@Override
		public void readFromBuffer(PacketBuffer buf) 
		{
			if(buf.readBoolean())
			{
				FPGuiHandler.OPTI_ASSEMBLER.openGui(player, pos);
			}
		}

		@Override
		public boolean stillValid(PlayerEntity playerIn)
		{
			return true;
		}
		
		@Override
		public ItemStack quickMoveStack(PlayerEntity playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
	        Slot slot = this.slots.get(index);

	        if (slot != null && slot.hasItem())
	        {
	            ItemStack itemstack1 = slot.getItem();
	            itemstack = itemstack1.copy();

	            if(index < 6)
	            {
	            	if (!this.moveItemStackTo(itemstack1, 6, this.slots.size(), true))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }
	            else
	            {
	            	if (!this.moveItemStackTo(itemstack1, 0, 4, false))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }

	            if (itemstack1.isEmpty())
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }

	            if (itemstack1.getCount() == itemstack.getCount())
	            {
	                return ItemStack.EMPTY;
	            }

	            slot.onTake(playerIn, itemstack1);
	        }
			return ItemStack.EMPTY;
		}
		
		private void updateSlotContent(boolean useInput)
		{
			ItemStack[] in = new ItemStack[]{in1.getItem(0), in2.getItem(2), in3.getItem(3)};
			ItemStack output = ItemStack.EMPTY;
			AssemblyRecipe ass = FPAssemblyManager.instance.getMatchingRecipe(in);
			
			if(ass==null)
			{
				out.setItem(0, ItemStack.EMPTY);
				updateRecipe(null, ItemStack.EMPTY);
				return;
			}
			
			output = ass.getOutput(in);
			if(HelperResearch.isUseable(player, output))
			{
				out.setItem(0, output);
				updateRecipe(ass, output);
				
				if(useInput)
				{
					ass.useItems(in);
					in1.setItem(0, in[0]);
					in2.setItem(0, in[1]);
					in3.setItem(0, in[2]);
				}
			}
			else
			{
				updateRecipe(null, ItemStack.EMPTY);
				output = ItemStack.EMPTY;
				return;
			}
		}
		
		private void updateRecipe(AssemblyRecipe rec, ItemStack output)
		{
			ItemStack assemblyRec = ItemStack.EMPTY;
			
			ItemStack cristall = recIn.getItem(0);
			if(!cristall.isEmpty() && FuturepackTags.item_crystals.contains(cristall.getItem()))
			{
				if(rec !=null && output!=null)
				{
					assemblyRec = new BlueprintAssemblyRecipe(player, rec).createRecipeItem();
				}
				recOut.setItem(0, assemblyRec);
			}
		}
		
		@Override
		public void removed(PlayerEntity player)
		{
			IInventory[] inves = new IInventory[] {in1,in2,in3,recIn};
			for(IInventory inv : inves)
			{
				for(int i=0;i<inv.getContainerSize();i++)
				{
					ItemStack it = inv.getItem(i);
					if(!it.isEmpty())
					{
						player.drop(it, false);
						inv.setItem(i, ItemStack.EMPTY);
					}
				}
			}
			super.removed(player);
		}
		
		private class SlotOutput extends SlotUses
		{
			public SlotOutput(IInventory par1iInventory, int index, int x, int y)
			{
				super(par1iInventory, index, x, y);
			}
			
			@Override
			public ItemStack onTake(PlayerEntity thePlayer, ItemStack stack)
			{
				updateSlotContent(true);
				return super.onTake(thePlayer, stack);
			}
		}
		
		private class SlotInput extends SlotUses
		{

			public SlotInput(IInventory par1iInventory, int index, int x, int y)
			{
				super(par1iInventory, index, x, y);
			}
			
			@Override
			public void setChanged()
			{
				super.setChanged();
				updateSlotContent(false);
			}
		}
	}
}
