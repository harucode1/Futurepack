package futurepack.common.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.lwjgl.opengl.GL11;

import com.google.common.base.Predicates;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import futurepack.api.Constants;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

//TODO texture hat halb transparenz, die ist schwarz
public class GuiNotiz extends Screen
{
		
	public static final ResourceLocation note_base = new ResourceLocation(Constants.MOD_ID, "textures/gui/spawnnote_base.png");
	public static final ResourceLocation note_overlay = new ResourceLocation(Constants.MOD_ID, "textures/gui/spawnnote_overlay.png");
	
	private int guiX, guiY;
	private PlayerEntity pl;
	private List<NotePart> parts;
	
	private boolean[] state;
	
	public GuiNotiz(World worldObj, PlayerEntity pl, ItemStack st)
	{
		super(new TranslationTextComponent("gui.ahahahawhawhuiawdhuiawdhuiawdhuiasdhuipadhawdhu"));
		this.pl = pl;
		parts = new ArrayList<>();
		state = new boolean[9];
		StateSaver.load(st, state);
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{	
		this.renderBackground(matrixStack);
		GlStateManager._enableBlend();
		//FIXME: Check if this is nessessary GlStateManager.setProfile(Profile.TRANSPARENT_MODEL);
		this.minecraft.getTextureManager().bind(note_base);	
		GL11.glColor4f(1F, 1F, 1F, 1F);	
		this.blit(matrixStack, guiX, guiY, 0, 0, 256, 256);	
		this.minecraft.getTextureManager().bind(note_overlay);	
		parts.forEach(p -> p.render(matrixStack));
		GlStateManager._disableBlend();
	}
	
	
	@Override
	public void init()
	{
		super.init();
		guiX = (width - 256) / 2;
		guiY = (height - 256) / 2;	
		
		parts.add(new NotePart(22, 18, 52, 71).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[0], Predicates.not(hasResearch("get_started"))));
		parts.add(new NotePart(76, 18, 52, 71).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[1], Predicates.not(hasResearch("forschung"))));
		parts.add(new NotePart(130, 18, 52, 71).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[2], Predicates.not(hasResearch("upgradeT1"))));
		parts.add(new NotePart(184, 18, 52, 71).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[3], Predicates.not(hasResearch("motherboard"))));
		parts.add(new NotePart(184, 90, 52, 71).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[4], Predicates.not(hasResearch("raumfahrt"))));
		
		parts.add(new NotePart(22, 92, 155, 32).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[5], Predicates.not(hasResearch("story.menelaus"))));
		parts.add(new NotePart(22, 128, 155, 32).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[6], Predicates.alwaysTrue()));
		parts.add(new NotePart(22, 163, 214, 32).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[7], Predicates.not(hasResearch("story.tyros"))));
		parts.add(new NotePart(22, 198, 83, 32).setColor(0.02F, 0.02F, 0.02F).setVisiblity(state[8], Predicates.alwaysTrue()));
	}
	
	
	private com.google.common.base.Predicate<NotePart> hasResearch(final String name)
	{
		return (t -> CustomPlayerData.getDataFromPlayer(pl).hasResearch(name));
	}
	
	private class NotePart
	{
		private final int x,y,u,v,w,h;
		private Predicate<NotePart> pred;
		private float red,green,blue;
		private float visibility;
		private long created = -1;
		
		private long last;
		
		public NotePart(int x, int y, int u, int v, int w, int h)
		{
			this.x = x;
			this.y = y;
			this.u = u;
			this.v = v;
			this.w = w;
			this.h= h;
			visibility = 0.2F;
			setColor(1F, 1F, 1F);
		}
		
		public NotePart(int x, int y, int w, int h)
		{
			this(x,y,x,y,w,h);
		}
		
		public NotePart setVisiblity(boolean defaultV, Predicate<NotePart> display)
		{
			this.pred = display;
			visibility = defaultV ? 1.0F : 0.2F;
			return this;
		}
		
		public NotePart setColor(float red, float green, float blue)
		{
			this.red = red;
			this.green = green;
			this.blue = blue;
			return this;
		}
		
		private void updateVisiblity()
		{
			long t = System.currentTimeMillis();
			if(created == -1)
			{
				created = t;
			}
				
			
			if(t-created > 1000)
			{
				float delta = (t - last) / 2000F;
				float alpha = (pred.test(this) ? 1F : -1F) * delta;
				visibility += alpha;
				if(visibility<0.2F)
					visibility=0.2F;
				else if(visibility>1)
					visibility=1;
			}
		}
		
		public void render(MatrixStack matrixStack)
		{
			updateVisiblity();
			last = System.currentTimeMillis();
			if(visibility>0)
			{
				GL11.glColor4f(red, green, blue, visibility);	
				blit(matrixStack, guiX+x, guiY+y, u, v, w, h);
			}
		}
	}
	
	public static class StateSaver
	{

		public static void save(PlayerEntity pl, Hand hand)
		{
			ItemStack st = pl.getItemInHand(hand);
			String[] r = new String[]{"get_started", "forschung", "upgradeT1", "motherboard", "raumfahrt", "story.menelaus", "", "story.tyros", ""};
			int flags = 0;
			CustomPlayerData data = CustomPlayerData.getDataFromPlayer(pl);
			for(int i=0;i<r.length;i++)
			{
				if(!data.hasResearch(r[i]))
					flags |= (1<<i);
			}
			st.getOrCreateTagElement("note").putInt("last_view", flags);
		}
		
		public static void load(ItemStack st, boolean[] b)
		{
			CompoundNBT nbt = st.getTagElement("note");
			if(nbt!=null)
			{
				int flags = nbt.getInt("last_view");
				for(int i=0;i<b.length;i++)
				{
					b[i] = (flags & 1<<i ) > 0;
				}
			}
			
			
		}
	}
}
