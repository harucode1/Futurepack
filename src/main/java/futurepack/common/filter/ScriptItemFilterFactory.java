package futurepack.common.filter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.api.interfaces.filter.IItemFilterFactory;
import futurepack.common.item.misc.MiscItems;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;

public class ScriptItemFilterFactory implements IItemFilterFactory 
{

	@Override
	public IItemFilter createFilter(ItemStack stack) 
	{
		if(stack.getItem() == MiscItems.script_filter && stack.hasTag())
		{
			CompoundNBT nbt = stack.getTagElement("script");
			if(nbt != null)
			{
				String s = nbt.getString("script_name");
				File script = new File("./filter_scripts/", s);
				if(script.exists())
				{
					return new ScriptItemFilter(stack);
				}
			}
		}
		return null;
	}

	
	public static String getOrCreateFilterName(ItemStack stack)
	{
		if(stack.getItem() == MiscItems.script_filter)
		{
			if(!stack.hasTag())
			{
				stack.setTag(new CompoundNBT());
			}
			
			CompoundNBT nbt = stack.getOrCreateTagElement("script");
			if(nbt.contains("script_name"))
			{
				return nbt.getString("script_name");
			}
			else
			{
				int number = (int) System.currentTimeMillis();
				String hex = Integer.toHexString(number);
				String name = "item_" + hex+".js";
				nbt.putString("script_name", name);
				Writer out;
				try 
				{
					out = getFilterScriptWriter(name);
					out.write(getBaseScript());
					out.close();
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				return name;
			}
		}
		throw new IllegalArgumentException("not a filter item!");
	}
	
	public static Reader getFilterScript(String scriptname) throws FileNotFoundException
	{
		File script = new File("./filter_scripts/", scriptname);
		return new InputStreamReader(new FileInputStream(script), StandardCharsets.UTF_8);
	}
	
	public static Writer getFilterScriptWriter(String scriptname) throws FileNotFoundException
	{
		File scriptDir = new File("./filter_scripts/");
		scriptDir.mkdirs();
		File script = new File(scriptDir, scriptname);
		return new OutputStreamWriter(new FileOutputStream(script), StandardCharsets.UTF_8);
	}
	
	private static String getBaseScript()
	{
		return ""
				+ "//if you need to save additional information use the 'data' variable, it will keep the information accross sessions.\n"
				+ "\n"
				+ "/*\n"
				+ " * Needed.\n"
				+ " * This is called for each item passing the filter, see IItem for more info\n"
				+ "*/\n"	
				+ "function filterItem(item)\n"
				+ "{\n"
				+ "  return true;\n"
				+ "}\n"
				+ "\n"
				+ "/*\n"
				+ " * Optional.\n"
				+ " * If you need to do anything stacksize dependant here is a nice callback. This is called each time an item actually passes the filter with the amount that was acually tranfered.\n"
				+ "*/\n"
				+ "function transferItemCallback(item)\n"
				+ "{\n"
				+ "}\n\n";
	}
	
	public static Exception compileAndTest(String name)
	{
		Exception e = ScriptItemFilter.compileScript(name, null);
		if(e==null)
		{
			ItemStack dummy = new ItemStack(MiscItems.script_filter, 1);
			CompoundNBT nbt = dummy.getOrCreateTagElement("script");
			nbt.putString("script_name", name);
			ScriptItemFilter filter = new ScriptItemFilter(dummy);
			
			try
			{
				filter.loadBindings();
				filter.testUnsafe(new ItemStack(Blocks.DIRT));
				filter.amountTransferedUnsafe(new ItemStack(Blocks.DIRT));
			}
			catch(Exception ee)
			{
				return ee;
			}
		}
		return e;
	}
	
}
