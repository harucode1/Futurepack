package futurepack.common.filter;

import java.io.PrintStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import javax.annotation.Nullable;
import javax.script.Bindings;
import javax.script.CompiledScript;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.logging.log4j.Level;

import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.common.FPLog;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;

public class ScriptItemFilter implements IItemFilter 
{
	private final ItemStack scriptItem;
	
	private ScriptObjectMirror funcfilterItem;
	private final SimpleBindings scriptVars;
	
	private ScriptObjectMirror funcItemTrannsferedCallback;
	
	public ScriptItemFilter(ItemStack sciptItem) 
	{
		super();
		this.scriptItem = sciptItem;
		CompoundNBT nbt = sciptItem.getTagElement("script");
		if(nbt==null)
			throw new IllegalArgumentException("item has no script tag!");
		else
		{
			String scriptName = nbt.getString("script_name");
			scriptVars = new SimpleBindings();
			
			Exception e = compileScript(scriptName, this);
			if(e!=null)
			{
				if(e instanceof ScriptException)
				{
					writeExceptionIntoItem(sciptItem, e);
				}
				else
				{
					e.printStackTrace();
					writeExceptionIntoItem(sciptItem, e);
				}
			}
		}
		
	}
	
	public static Exception compileScript(String scriptName, @Nullable ScriptItemFilter rawFilter)
	{
		try
		{
			Reader read = ScriptItemFilterFactory.getFilterScript(scriptName);
			CompiledScript compiled = JSFilterFactory.compile(read);
			
			SimpleBindings scriptVars = rawFilter!=null ? rawFilter.scriptVars : new SimpleBindings();			
			compiled.eval(scriptVars);
			
			Bindings globalVars = (Bindings) scriptVars.get("nashorn.global");
			
			if(rawFilter!=null)
			{
				if(globalVars.containsKey("filterItem"))
				{
					rawFilter.funcfilterItem = (ScriptObjectMirror) globalVars.get("filterItem");
				}
				else
				{
					rawFilter.funcfilterItem = null;
					throw new ScriptException("no function 'filterItem' was defined");
				}
				
				if(globalVars.containsKey("transferItemCallback"))
				{
					rawFilter.funcItemTrannsferedCallback = (ScriptObjectMirror) globalVars.get("transferItemCallback");
				}
				else
				{
					rawFilter.funcItemTrannsferedCallback = null;
					throw new ScriptException("no function 'transferItemCallback' was defined");
				}
			}
			else
			{
				if(!globalVars.containsKey("filterItem"))
				{
					throw new ScriptException("no function 'filterItem' was defined");
				}
				if(!globalVars.containsKey("transferItemCallback"))
				{
					throw new ScriptException("no function 'transferItemCallback' was defined");
				}
			}
			
		}
		catch(Exception e)
		{			
			return e;
		}
		
		return null;
	}


	protected void loadBindings()
	{
		CompoundNBT script = scriptItem.getTagElement("script");
		CompoundNBT extra;
		if(script != null && script.contains("extraData"))
		{
			extra = script.getCompound("extraData");
		}
		else
		{
			extra = new CompoundNBT();
			script.put("extraData", extra);
		}
		scriptVars.put("data", new NBTWrapper(extra));
	}
	
	private void saveBindings()
	{
		NBTWrapper wrapper = (NBTWrapper) scriptVars.get("data");
		CompoundNBT script = scriptItem.getTagElement("script");
		script.put("extraData", wrapper.nbt);
	}
	

	@Override
	public boolean test(ItemStack t) 
	{
		if(funcfilterItem!=null)
		{
			loadBindings();
			try
			{
				return testUnsafe(t);
			}
			catch(Exception e)
			{
				writeExceptionIntoItem(scriptItem, e);
			}
		}
		return false;
	}
	
	public boolean testUnsafe(ItemStack t) throws ScriptException
	{
		Object result = funcfilterItem.call(scriptVars, new ItemImpl(t));
		if(result instanceof Boolean)
		{
			return (boolean)result;
		}
		else
		{
			throw new ScriptException("function 'filterItem' retuned '" + result + "' instead of boolean");
		}
	}
	
	@Override
	public void amountTransfered(ItemStack transfered) 
	{
		if(funcItemTrannsferedCallback!=null)
		{
			loadBindings();
			try
			{
				amountTransferedUnsafe(transfered);
				saveBindings();
			}
			catch(Exception e)
			{
				writeExceptionIntoItem(scriptItem, e);
			}
		}		
	}
	
	public void amountTransferedUnsafe(ItemStack transfered)
	{
		funcItemTrannsferedCallback.call(scriptVars, new ItemImpl(transfered));
	}
	
	public static void writeExceptionIntoItem(ItemStack item, Throwable e)
	{
		FPLog.logger.catching(Level.DEBUG, e);
		
		if(e instanceof ScriptException)
		{
			String[] error = new String[] {((ScriptException)e).getLocalizedMessage()};
			ListNBT lore = new ListNBT();
			for(String err : error)
				lore.add(StringNBT.valueOf(err));
			
			CompoundNBT nbt = item.getOrCreateTagElement("display");
			nbt.put("Lore", lore);
			return;
		}
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();		
		PrintStream pout = new PrintStream(out);
		while(e!=null)
		{
			pout.println(e);
			e = e.getCause();
			if(e!=null)
			{
				pout.print("Caused By ");
			}
		}
		pout.close();
		String[] error = out.toString(StandardCharsets.UTF_8).replaceAll("\t", "  ").replace('\r', ' ').split("\n");
		ListNBT lore = new ListNBT();
		for(String err : error)
			lore.add(StringNBT.valueOf(err));
		
		CompoundNBT nbt = item.getOrCreateTagElement("display");
		nbt.put("Lore", lore);
	}
}
