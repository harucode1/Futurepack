package futurepack.common.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.script.Bindings;

import net.minecraft.nbt.ByteArrayNBT;
import net.minecraft.nbt.ByteNBT;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.DoubleNBT;
import net.minecraft.nbt.FloatNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.IntArrayNBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.LongArrayNBT;
import net.minecraft.nbt.LongNBT;
import net.minecraft.nbt.ShortNBT;
import net.minecraft.nbt.StringNBT;

import java.util.Map.Entry;

public class NBTWrapper implements Bindings
{
	public final CompoundNBT nbt;

	public NBTWrapper(CompoundNBT base)
	{
		this.nbt = base;
	}

	@Override
	public int size() 
	{
		return nbt.size();
	}

	@Override
	public boolean isEmpty() 
	{
		return nbt.isEmpty();
	}

	@Override
	public boolean containsValue(Object value) 
	{
		return values().contains(value);
	}

	@Override
	public void clear() 
	{
		throw new UnsupportedOperationException("clear");
	}

	@Override
	public Set<String> keySet() 
	{
		return nbt.getAllKeys();
	}

	@Override
	public Collection<Object> values() 
	{
		Set<String> keys = keySet();
		ArrayList<Object> list = new ArrayList<Object>(keys.size());
		for(String k : keys)
		{
			list.add(get(k));
		}
		return list;
	}

	@Override
	public Set<Entry<String, Object>> entrySet() 
	{
		Set<String> keys = keySet();
		Map<String, Object> entries = new HashMap<>(keys.size());
		for(String k : keys)
		{
			entries.put(k, get(k));
		}
		return entries.entrySet();
	}

	@Override
	public Object put(String name, Object value) 
	{
		Object old = get(name);
		nbt.put(name, getNBTBase(value));
		return old;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> toMerge) 
	{
		toMerge.forEach(this::put);
	}

	@Override
	public boolean containsKey(Object key) 
	{
		return nbt.contains((String) key);
	}

	@Override
	public Object get(Object key) 
	{
		INBT base = nbt.get((String) key);
		return base == null ? null : getValue(base);
	}

	@Override
	public Object remove(Object key) 
	{
		Object ret = get(key);
		nbt.remove((String)key);
		return ret;
	}

	public static INBT getNBTBase(Object obj)
	{
		Class<?> c = obj.getClass();
		if(c==byte.class)
		{
			return ByteNBT.valueOf((byte) obj);
		}
		else if(c==short.class || c==Short.class)
		{
			return ShortNBT.valueOf((short) obj);
		}
		else if(c==int.class || c==Integer.class)
		{
			return IntNBT.valueOf((int) obj);
		}
		else if(c==long.class || c==Long.class)
		{
			return LongNBT.valueOf((long) obj);
		}
		else if(c==float.class)
		{
			return FloatNBT.valueOf((float) obj);
		}
		else if(c==double.class || c==Double.class)
		{
			return DoubleNBT.valueOf((double) obj);
		}
		else if(c==byte[].class)
		{
			return new ByteArrayNBT((byte[]) obj);
		}
		else if(c==String.class)
		{
			return StringNBT.valueOf((String) obj);
		}
		else if(c==int[].class)
		{
			return new IntArrayNBT((int[]) obj);
		}
		else if(c==long[].class)
		{
			return new LongArrayNBT((long[]) obj);
		}
		else if(c.isArray())
		{
			Object[] oa = (Object[]) obj;
			ListNBT list = new ListNBT();
			for(Object o : oa)
			{
				list.add(getNBTBase(o));
			}
			return list;
		}
		else if( obj instanceof Map)
		{
			Map<String, Object> map = (Map<String, Object>) obj;
			CompoundNBT compount = new CompoundNBT();
			map.forEach((k,v) -> compount.put(k, getNBTBase(v)));
			return compount;
		}
		else
		{
			throw new IllegalArgumentException("Object " + obj + " of class " + c + " could not be turned into an nbt tag");
		}
	}
	
	public static Object getValue(INBT base)
	{
		switch(base.getId()) {
	      case 0:
	         return null;
	      case 1:
	         return ((ByteNBT)base).getAsByte();
	      case 2:
	         return ((ShortNBT)base).getAsShort();
	      case 3:
	         return ((IntNBT)base).getAsInt();
	      case 4:
	         return ((LongNBT)base).getAsLong();
	      case 5:
	         return ((FloatNBT)base).getAsFloat();
	      case 6:
	         return ((DoubleNBT)base).getAsDouble();
	      case 7:
	         return ((ByteArrayNBT)base).getAsByteArray();
	      case 8:
	         return ((StringNBT)base).getAsString();
	      case 11:
	    	  return ((IntArrayNBT)base).getAsIntArray();
	      case 12:
	    	  return ((LongArrayNBT)base).getAsLongArray();
	      case 10:
	         return new NBTWrapper((CompoundNBT)base);
	      case 9:
	    	  ListNBT list = (ListNBT) base;
	    	  Object[] raw = list.toArray();
	    	  for(int i=0;i<raw.length;i++)
	    	  {
	    		  raw[i] = getValue((INBT) raw[i]);
	    	  }
	    	  return raw;
	      
	      default:
	         return new IllegalArgumentException("Unknown NBTType " + base.getClass() + " id " + base.getId() + " " + base);
	      }
	}
}
