package futurepack.common;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockValidator;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class SelectInterface implements IBlockValidator
{
	Class face;
	
	public SelectInterface(Class c)
	{
		face = c;
	}
	
	@Override
	public boolean isValidBlock(World w, ParentCoords pos)
	{
		TileEntity t = w.getBlockEntity(pos);
		if(t!=null)
		{
			return face.isAssignableFrom(t.getClass());
		}
		return false;
	}	
}
