package futurepack.common;

import java.util.Random;
import java.util.function.Consumer;

import futurepack.api.Constants;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.EntityForceField;
import futurepack.common.entity.EntityForstmaster;
import futurepack.common.entity.EntityMiner;
import futurepack.common.entity.EntityMonitor;
import futurepack.common.entity.living.EntityAlphaJawaul;
import futurepack.common.entity.living.EntityCrawler;
import futurepack.common.entity.living.EntityCyberZombie;
import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.common.entity.living.EntityEvilRobot;
import futurepack.common.entity.living.EntityGehuf;
import futurepack.common.entity.living.EntityHeuler;
import futurepack.common.entity.living.EntityJawaul;
import futurepack.common.entity.living.EntityWolba;
import futurepack.common.entity.monocart.EntityMonocart;
import futurepack.common.entity.throwable.EntityEgger;
import futurepack.common.entity.throwable.EntityGrenadeBase;
import futurepack.common.entity.throwable.EntityHook;
import futurepack.common.entity.throwable.EntityLaser;
import futurepack.common.entity.throwable.EntityLaserProjectile;
import futurepack.common.entity.throwable.EntityRocket;
import futurepack.common.entity.throwable.EntityWakurumIngot;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EntityType.Builder;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.entity.passive.WolfEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class FPEntitys 
{
	public static final EntityType<EntityWakurumIngot> WAKURIUM_THROWN = register("wakurium_thorwn", EntityClassification.MISC, EntityWakurumIngot::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityCyberZombie> CYBER_ZOMBIE = register("cyber_zombie", EntityClassification.MONSTER, EntityCyberZombie::new, EnumTrackingType.LIVINGS);
	public static final EntityType<EntityWolba> WOLBA = register("wolba", EntityClassification.CREATURE, EntityWolba::new, EnumTrackingType.LIVINGS, 1.1F, 1.4F);
	public static final EntityType<EntityEvilRobot> EVIL_ROBOT = register("evil_robot", EntityClassification.MONSTER, EntityEvilRobot::new, EnumTrackingType.LIVINGS);
	public static final EntityType<EntityMonitor> MONITOR = register("monitor", EntityClassification.MISC, EntityMonitor::new, EnumTrackingType.NONE);//TODO add custom spawn information to monitor
	public static final EntityType<EntityEgger> ENTITY_EGGER = register("egger", EntityClassification.MISC, EntityEgger::new, EnumTrackingType.THROWABLE);
	
	public static final EntityType<EntityRocket.EntityPlasmaRocket> ROCKET_PLASMA = register("rocket_plasma", EntityClassification.MISC, EntityRocket.EntityPlasmaRocket::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityRocket.EntityBlazeRocket> ROCKET_BLAZE = register("rocket_blaze", EntityClassification.MISC, EntityRocket.EntityBlazeRocket::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityRocket.EntityBioteriumRocket> ROCKET_BIOTERIUM = register("rocket_bioterium", EntityClassification.MISC, EntityRocket.EntityBioteriumRocket::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityRocket.EntityNormalRocket> ROCKET_NORMAL = register("rocket_normal", EntityClassification.MISC, EntityRocket.EntityNormalRocket::new, EnumTrackingType.THROWABLE);
	
	public static final EntityType<EntityMonocart> MONOCART = register("monocart", EntityClassification.MISC, EntityMonocart::new, EnumTrackingType.ROBOTS, 0.8F, 0.6F);
	
	public static final EntityType<EntityGrenadeBase.Plasma> GRENADE_PLASMA = register("grenade_plasma", EntityClassification.MISC, EntityGrenadeBase.Plasma::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Normal> GRENADE_NORMAL = register("grenade_normal", EntityClassification.MISC, EntityGrenadeBase.Normal::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Blaze> GRENADE_BLAZE = register("grenade_blaze", EntityClassification.MISC, EntityGrenadeBase.Blaze::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Slime> GRENADE_SLIME = register("grenade_slime", EntityClassification.MISC, EntityGrenadeBase.Slime::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Kompost> GRENADE_KOMPOST = register("grenade_kompost", EntityClassification.MISC, EntityGrenadeBase.Kompost::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Futter> GRENADE_FUTTER = register("grenade_futter", EntityClassification.MISC, EntityGrenadeBase.Futter::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Saat> GRENADE_SAAT = register("grenade_saat", EntityClassification.MISC, EntityGrenadeBase.Saat::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.EnityEggerFullGranade> GRENADE_ENTITY_EGGER = register("grenade_entity_egger", EntityClassification.MISC, EntityGrenadeBase.EnityEggerFullGranade::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityLaserProjectile> LASER_PROJECTILE = register("laser_projectile", EntityClassification.MISC, EntityLaserProjectile::new, EnumTrackingType.THROWABLE);
	
	public static final EntityType<EntityGehuf> GEHUF = register("gehuf", EntityClassification.CREATURE, EntityGehuf::new, EnumTrackingType.LIVINGS, 1.1F, 1.4F);
	public static final EntityType<EntityCrawler> CRAWLER = register("crawler", EntityClassification.MONSTER, EntityCrawler::new, EnumTrackingType.LIVINGS, 1.4F, 0.9F);
	public static final EntityType<EntityHeuler> HEULER = register("heuler", EntityClassification.CREATURE, EntityHeuler::new, EnumTrackingType.LIVINGS, 0.5F, 0.5F);
	
	public static final EntityType<EntityLaser> LASER = register("laser", EntityClassification.MISC, EntityLaser::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityForceField> FORCE_FIELD = register("force_field", EntityClassification.MISC, EntityForceField::new, EnumTrackingType.ROBOTS);
	public static final EntityType<EntityForstmaster> FORESTMASTER = register("forestmaster", EntityClassification.MISC, EntityForstmaster::new, EnumTrackingType.ROBOTS, 0.8F, 0.6F);
	public static final EntityType<EntityMiner> MINER = register("miner", EntityClassification.MISC, EntityMiner::new, EnumTrackingType.ROBOTS, 0.6F, 0.6F, b -> b.fireImmune());
	public static final EntityType<EntityHook> HOOK = register("hook", EntityClassification.MISC, EntityHook::new, EnumTrackingType.THROWABLE);
	
	public static final EntityType<EntityJawaul> JAWAUL = register("jawaul", EntityClassification.CREATURE, EntityJawaul::new, EnumTrackingType.LIVINGS, 0.6F, 0.85F);
	public static final EntityType<EntityDungeonSpider> DUNGEON_SPIDER = register("dungeon_spider", EntityClassification.CREATURE, EntityDungeonSpider::new, EnumTrackingType.LIVINGS, 0.6F, 0.3F);
	public static final EntityType<EntityAlphaJawaul> ALPHA_JAWAUL = register("alpha_jawaul", EntityClassification.CREATURE, EntityAlphaJawaul::new, EnumTrackingType.LIVINGS, 1.5F, 1.2F);
	
	
	private static <T extends Entity> EntityType<T> register(String id, EntityClassification classificationIn, EntityType.IFactory<T> constructor, EnumTrackingType type, float width, float height, Consumer<EntityType.Builder> c)
	{
		Builder<T> build = EntityType.Builder.of(constructor, classificationIn);
		type.setTracking(build).sized(width, height);
//		build.setCustomClientFactory((s,w) -> s.)
		if(c!=null)
			c.accept(build);
		EntityType<T> t = build.build("");
		t.setRegistryName(Constants.MOD_ID, id);
		return t;
	}
	
	private static <T extends Entity> EntityType<T> register(String id, EntityClassification classification, EntityType.IFactory<T> constructor, EnumTrackingType type, float width, float height)
	{
		return register(id, classification, constructor, type, width, height, null);
	}
	
	
	private static <T extends Entity> EntityType<T> register(String id, EntityClassification classificationIn, EntityType.IFactory<T> constructor, EnumTrackingType type)
	{
		if(type==EnumTrackingType.THROWABLE)
		{
			return register(id, classificationIn, constructor, type, 0.25F, 0.25F);
		}
		else if(type == EnumTrackingType.LIVINGS)
		{
			return register(id, classificationIn, constructor, type,  0.6F, 1.95F);
		}
		else
		{
			return register(id, classificationIn, constructor, type, 1F, 1F);
		}
	}

	public static void registerEntitys(RegistryEvent.Register<EntityType<?>> event)
	{
		IForgeRegistry<EntityType<?>> r = event.getRegistry();
		
		r.registerAll(WAKURIUM_THROWN, CYBER_ZOMBIE, WOLBA, EVIL_ROBOT, MONITOR, ENTITY_EGGER);
		r.registerAll(ROCKET_PLASMA, ROCKET_BLAZE, ROCKET_BIOTERIUM, ROCKET_NORMAL);
		r.registerAll(MONOCART);
		r.registerAll(GRENADE_PLASMA, GRENADE_NORMAL, GRENADE_BLAZE, GRENADE_SLIME, GRENADE_KOMPOST, GRENADE_FUTTER, GRENADE_SAAT, GRENADE_ENTITY_EGGER);
		r.registerAll(LASER_PROJECTILE);
		r.registerAll(GEHUF, CRAWLER, HEULER);
		r.registerAll(LASER, FORCE_FIELD, FORESTMASTER, MINER, HOOK);
		r.registerAll(JAWAUL, DUNGEON_SPIDER, ALPHA_JAWAUL);
		
		EntitySpawnPlacementRegistry.register(CRAWLER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::checkMonsterSpawnRules);
		EntitySpawnPlacementRegistry.register(CYBER_ZOMBIE, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::checkMonsterSpawnRules);
		EntitySpawnPlacementRegistry.register(EVIL_ROBOT, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::checkMonsterSpawnRules);
		
		EntitySpawnPlacementRegistry.register(WOLBA, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, FPEntitys::canAnimalSpawnMenelaus);
		EntitySpawnPlacementRegistry.register(GEHUF, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, FPEntitys::canAnimalSpawnMenelaus);
		EntitySpawnPlacementRegistry.register(HEULER, EntitySpawnPlacementRegistry.PlacementType.NO_RESTRICTIONS, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, FPEntitys::canAnimalSpawnMenelaus);
		EntitySpawnPlacementRegistry.register(JAWAUL, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, FPEntitys::canAnimalSpawnMenelaus);
		
	}
	
	public static boolean canAnimalSpawnMenelaus(EntityType<? extends AnimalEntity> animal, IWorld worldIn, SpawnReason reason, BlockPos pos, Random random) 
	{
		return worldIn.getBlockState(pos.below()).getBlock() == TerrainBlocks.sand_m && worldIn.getRawBrightness(pos, 0) > 8;
	}
	
	public static enum EnumTrackingType
	{
		NONE(40,Integer.MAX_VALUE,false),
		LIVINGS(30,3,true),
		THROWABLE(50,10,true),
		ROBOTS(30,15,true),
		FAST(40,5,true),
		;
		
		final int updateFrequenzy, range;
		final boolean sendVelocity;
		
		EnumTrackingType(int range, int updateFrequenzy, boolean sendVelocity)
		{
			this.range = range;
			this.updateFrequenzy = updateFrequenzy;
			this.sendVelocity = sendVelocity;
		}
		
		public <T extends Entity> EntityType.Builder<T> setTracking(EntityType.Builder<T> t)
		{			
			t.setTrackingRange(range).setUpdateInterval(updateFrequenzy).setShouldReceiveVelocityUpdates(sendVelocity);
			return t;
		}
	}
	
	public static void registerGlobalEntityAttributes(EntityAttributeCreationEvent event)
	{
		event.put(CRAWLER, EntityCrawler.registerAttributes().build());
		event.put(CYBER_ZOMBIE, EntityCyberZombie.registerAttributes().build());
		event.put(WOLBA, SheepEntity.createAttributes().build());
		event.put(EVIL_ROBOT, EntityEvilRobot.registerAttributes().build());
		event.put(GEHUF, CowEntity.createAttributes().build());
		event.put(HEULER, EntityHeuler.registerAttributes().build());
		event.put(JAWAUL, WolfEntity.createAttributes().build());
		event.put(DUNGEON_SPIDER, EntityDungeonSpider.registerAttributes().build());
		event.put(ALPHA_JAWAUL, EntityAlphaJawaul.registerAttributes().build());
		
	}
}
