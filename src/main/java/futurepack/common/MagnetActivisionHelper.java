package futurepack.common;

import futurepack.common.entity.CapabilityPlayerData;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.item.tools.compositearmor.CompositeArmorPart;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import futurepack.common.sync.KeyManager.EventFuturepackKey;
import futurepack.common.sync.MessageFPData;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.inventory.EquipmentSlotType.Group;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.network.PacketDistributor;

public class MagnetActivisionHelper
{
	@SubscribeEvent
	public static void onKeyPressed(EventFuturepackKey key)
	{		
		if(key.type==EnumKeyTypes.MODUL_MAGNET)
		{
			if(!hasMagnetInstalled(key.player))
			{
				setMagnet(key.player, false);
				key.player.sendMessage(new TranslationTextComponent("modular_armor.module.magnet.notinstalled"), Util.NIL_UUID);
			}
			else
			{	
				boolean on = isMagnetOn(key.player);
				setMagnet(key.player, !on);
				
				if(on)
				{
					key.player.sendMessage(new TranslationTextComponent("modular_armor.module.magnet.off"), Util.NIL_UUID);
				}
				else
				{
					key.player.sendMessage(new TranslationTextComponent("modular_armor.module.magnet.on"), Util.NIL_UUID);
				}
			}
		}
		
	}
	
	public static boolean isMagnetOn(PlayerEntity pl)
	{
		final CompoundNBT fp = CapabilityPlayerData.getPlayerdata(pl);
		return fp.getBoolean("magnet");
	}
	
	public static boolean hasMagnetInstalled(PlayerEntity pl)
	{
		for(EquipmentSlotType armor : EquipmentSlotType.values())
		{
			if(armor.getType() == Group.ARMOR)
			{
				CompositeArmorPart part = CompositeArmorPart.getInventory(pl.getItemBySlot(armor));
				if(part != null)
				{
					int slots = part.getSlots();
					for(int i=0;i<slots;i++)
					{
						ItemStack modul = part.getStackInSlot(i);
						if(!modul.isEmpty() && modul.getItem() == ToolItems.modul_magnet)
							return true;
					}
				}
			}
		}

		return false;
	}
	
	public static void setMagnet(PlayerEntity pl, boolean on)
	{
		final CompoundNBT fp = CapabilityPlayerData.getPlayerdata(pl);;
		fp.putBoolean("magnet", on);
		if(pl instanceof ServerPlayerEntity)
		{
			sync((ServerPlayerEntity) pl);
		}
	}
	
	public static void sync(ServerPlayerEntity pl)
	{
		final CompoundNBT fp = CapabilityPlayerData.getPlayerdata(pl);;
		FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> pl), new MessageFPData(fp, pl.getId()));
	}
};
