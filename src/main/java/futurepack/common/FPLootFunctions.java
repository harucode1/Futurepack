package futurepack.common;

import futurepack.world.loot.LootFunctionSetupBattery;
import futurepack.world.loot.LootFunctionSetupChip;
import futurepack.world.loot.LootFunctionSetupCore;
import futurepack.world.loot.LootFunctionSetupRam;
import net.minecraft.loot.ILootSerializer;
import net.minecraft.loot.LootFunctionType;
import net.minecraft.loot.functions.ILootFunction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;

public class FPLootFunctions 
{
	public static final LootFunctionType SETUP_CHIP = registerFunction("fp_setup_chip", new LootFunctionSetupChip.Serializer());
	public static final LootFunctionType SETUP_CORE = registerFunction("fp_setup_core", new LootFunctionSetupCore.Serializer());
	public static final LootFunctionType SETUP_RAM = registerFunction("fp_setup_ram", new LootFunctionSetupRam.Serializer());
	public static final LootFunctionType SETUP_BATTERY = registerFunction("fp_setup_battery", new LootFunctionSetupBattery.Serializer());

	/**
	 * Copy of LootFunctionManager
	 * 
	 * @param p_237451_0_
	 * @param p_237451_1_
	 * @return
	 */
	private static LootFunctionType registerFunction(String p_237451_0_, ILootSerializer<? extends ILootFunction> p_237451_1_) 
	{
		return Registry.register(Registry.LOOT_FUNCTION_TYPE, new ResourceLocation(p_237451_0_), new LootFunctionType(p_237451_1_));
	}
	
	public static void init()
	{
		FPLog.logger.info("Registered Loot Functions");
	}
}
