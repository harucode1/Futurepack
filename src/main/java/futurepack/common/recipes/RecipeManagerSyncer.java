package futurepack.common.recipes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Supplier;

import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.common.recipes.centrifuge.FPZentrifugeManager;
import futurepack.common.recipes.crushing.FPCrushingManager;
import futurepack.common.recipes.industrialfurnace.FPIndustrialFurnaceManager;
import futurepack.common.recipes.industrialfurnace.FPIndustrialNeonFurnaceManager;
import futurepack.common.recipes.recycler.FPRecyclerLaserCutterManager;
import futurepack.common.recipes.recycler.FPRecyclerShredderManager;
import futurepack.common.recipes.recycler.FPRecyclerTimeManipulatorManager;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageRequestClientRecipeHashes;
import futurepack.common.sync.MessageSendAllRecipesCorrect;
import futurepack.common.sync.MessageSendRecipeHashes;
import futurepack.common.sync.MessageSendRecipes;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.PacketDistributor;

public class RecipeManagerSyncer 
{
	public static RecipeManagerSyncer INSTANCE = new RecipeManagerSyncer();
	
	static
	{
		INSTANCE.registerRecipeManager(FPAssemblyManager.NAME, () -> FPAssemblyManager.instance);
		INSTANCE.registerRecipeManager(FPZentrifugeManager.NAME, () -> FPZentrifugeManager.instance);
		INSTANCE.registerRecipeManager(FPCrushingManager.NAME, () -> FPCrushingManager.instance);
		INSTANCE.registerRecipeManager(FPIndustrialFurnaceManager.NAME, () -> FPIndustrialFurnaceManager.instance);
		INSTANCE.registerRecipeManager(FPIndustrialNeonFurnaceManager.NAME, () -> FPIndustrialNeonFurnaceManager.instance);
		INSTANCE.registerRecipeManager(FPRecyclerLaserCutterManager.NAME, () -> FPRecyclerLaserCutterManager.instance);
		INSTANCE.registerRecipeManager(FPRecyclerShredderManager.NAME, () -> FPRecyclerShredderManager.instance);
		INSTANCE.registerRecipeManager(FPRecyclerTimeManipulatorManager.NAME, () -> FPRecyclerTimeManipulatorManager.instance);
		INSTANCE.registerRecipeManager(FPPlanetRegistry.NAME, () -> FPPlanetRegistry.instance);
	}
	
	private Map<String, ManagerEntry> managers = new TreeMap<>();
	
	private List<Consumer<ISyncedRecipeManager<?>>> onRecipeChanges = new ArrayList<Consumer<ISyncedRecipeManager<?>>>();
	
	public RecipeManagerSyncer() 
	{
		
	}
	
	public void registerRecipeManager(String name, Supplier<ISyncedRecipeManager<?>> manager)
	{
		managers.put(name, new ManagerEntry(name, manager));
	}
	
	public void registerRecipeListener(Consumer<ISyncedRecipeManager<?>> listener)
	{
		onRecipeChanges.add(listener);
	}
	
	/**
	 * Server side
	 * 
	 * @param event
	 */
	@SubscribeEvent
	public void onPlayerJoinServer(PlayerLoggedInEvent event)
	{
		PlayerEntity pl = event.getPlayer();
		if(pl instanceof ServerPlayerEntity)
		{
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)pl), new MessageRequestClientRecipeHashes());
		}	
	}
	
	/**
	 * Server Side
	 * 
	 * Send the update packet to all players on the server
	 * @param server
	 */
	public void onRecipeReload(MinecraftServer server)
	{
		managers.values().forEach(e -> e.hash = null);
		IPacket<?> pkt = FPPacketHandler.CHANNEL_FUTUREPACK.toVanillaPacket(new MessageRequestClientRecipeHashes(), NetworkDirection.PLAY_TO_CLIENT);
		server.getPlayerList().broadcastAll(pkt);
	}
	
	/**
	 * Client side
	 */
	public void onServerRequestHashes()
	{
		Map<String, byte[]> nameToHash = new HashMap<String, byte[]>(managers.size());
		managers.forEach((n,e) -> nameToHash.put(n, e.getHash()));
		
		MessageSendRecipeHashes msg = new MessageSendRecipeHashes(nameToHash);
		FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(msg);
	}
	
	/**
	 * Server side
	 * 
	 * @param nameToHash
	 * @param pl
	 */
	public void onClientResponseHashes(Map<String, byte[]> nameToHash, ServerPlayerEntity pl)
	{
		nameToHash.forEach((n,h) -> {
			ManagerEntry e = managers.get(n);
			if(e!=null)
			{
				if(!areHashesEqual(h, e.getHash()))//client has other recipes
				{
					sendClientRecipes(n, pl);
				}
				else
				{
					sendClientRecipesOk(n, pl);
				}
			}
		});
	}
	
	/**
	 * We will execute the same stuff at the client as if the server requested the hashes. The server will then either send new recipes or the OK.
	 */
	public void onClientRecipeReload()
	{
		if(Minecraft.getInstance().getConnection() != null) // this can happen when you edit mipmaps so the client reloads, but no server yet
			onServerRequestHashes();
	}
	
	

	public void sendClientRecipes(String managerName, ServerPlayerEntity pl)
	{
		Collection<?> recipes = managers.get(managerName).get().getRecipes();
		FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() ->pl), new MessageSendRecipes(managerName, recipes));
	}
	
	public void sendClientRecipesOk(String managerName, ServerPlayerEntity pl) 
	{
		FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() ->pl), new MessageSendAllRecipesCorrect(managerName));
	}
	

	@SuppressWarnings("unchecked")
	public void writeRecipes(String managerName, Collection<?> recipes, PacketBuffer buf) 
	{
		buf.writeVarInt(recipes.size());
		@SuppressWarnings("rawtypes")
		ISyncedRecipeManager manager = managers.get(managerName).get();
		for(Object t : recipes)
			manager.writeRecipeToBuffer(t, buf);
	}

	public Collection<?> readRecipes(String managerName, PacketBuffer buf) 
	{
		int size = buf.readVarInt();
		ArrayList<Object> list = new ArrayList<Object>(size);
		ISyncedRecipeManager<?> manager = managers.get(managerName).get();
		for(int i=0;i<size;i++)
		{
			list.add(manager.readRecipeFromBuffer(buf));
		}
		return list;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void overrideClientRecipes(String managerName, Collection recipes) 
	{
		ManagerEntry m = managers.get(managerName);
		m.hash = null;
		ISyncedRecipeManager manager = m.get();
		manager.getRecipes().clear();
		recipes.forEach(manager::addRecipe);
		
		onRecipeChanges.forEach(c -> c.accept(m.get()));
	}
	
	public void allRecipesOk(String managerName)
	{
		ManagerEntry m = managers.get(managerName);
		
		onRecipeChanges.forEach(c -> c.accept(m.get()));
	}
	
	public static boolean areHashesEqual(byte[] a, byte[] b)
	{
		if(a==b && a!=null)
			return true;
		return Arrays.equals(a, b);
	}
	
	private static class ManagerEntry
	{
		final Supplier<ISyncedRecipeManager<?>> manager;
		byte[] hash;
		
		public ManagerEntry(String name, Supplier<ISyncedRecipeManager<?>> manager) 
		{
			super();
			this.manager = manager;
		}

		public byte[] getHash()
		{
			if(hash == null)
			{
				return hash = manager.get().hashRecipes();
			}
			return hash;
		}
		
		public ISyncedRecipeManager<?> get()
		{
			return manager.get();
		}
	}
}
