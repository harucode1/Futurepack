package futurepack.common.recipes.centrifuge;

import java.util.Arrays;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IOptimizeable;
import futurepack.common.recipes.EnumRecipeSync;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

public class ZentrifugeRecipe implements IOptimizeable
{
	private ItemPredicateBase in;
	private ItemStack[] out;
	private int needeSupport;
	private int time = 10;
	
	public ZentrifugeRecipe(ItemPredicateBase in, ItemStack[] out, int support)
	{
		this.in = in;
		this.out = out;
		for(int i=0;i<out.length;i++)
		{
			if(out[i]==null)
				out[i]=ItemStack.EMPTY;
		}
		this.needeSupport = support;
	}
	
	public void setTime(int time)
	{
		this.time = time;
	}
	
	public boolean matches(ItemStack it, boolean ignoreStackSize)
	{
		if(it!=null)
		{
			return in.apply(it, ignoreStackSize);
		}
		return false;
	}
	
	public ItemStack[] getOutput()
	{
		ItemStack[] its = new ItemStack[out.length];
		for(int i=0;i<out.length;i++)
		{
			if(out[i]!=null && !out[i].isEmpty())
				its[i] = out[i].copy();
		}
		return its;
	}
	
	public ItemPredicateBase getInput()
	{
		return in;
	}
	
	public int getNeededSupport()
	{
		return needeSupport;
	}
	
	@Override
	public String toString()
	{
		return String.format("Zentrifuge: %s to %s, need: %sSP", in, Arrays.toString(out), needeSupport);
	}

	public int getTime()
	{
		return time;
	}
	
	private static Block centrifuge = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(Constants.MOD_ID, "centrifuge"));
	
	public boolean isLocalResearched()
	{
		return CustomPlayerData.createForLocalPlayer().canProduce(new ItemStack(centrifuge));
	}
	
	private int points = 0;
	
	@Override
	public int getPoints()
	{
		return points;
	}

	@Override
	public void addPoint()
	{
		points++;
	}

	@Override
	public void resetPoints()
	{
		points++;
	}
	
	public void write(PacketBuffer buf)
	{
		buf.writeVarInt(out.length);
		for(ItemStack o : out)
		{
			buf.writeItem(o);
		}
		EnumRecipeSync.writeUnknown(in, buf);
		buf.writeVarInt(needeSupport);
		buf.writeVarInt(time);
	}
	
	public static ZentrifugeRecipe read(PacketBuffer buf)
	{
		ItemStack[] out = new ItemStack[buf.readVarInt()];
		for(int i=0;i<out.length;i++)
		{
			out[i] = buf.readItem();
		}
		ZentrifugeRecipe r = new ZentrifugeRecipe((ItemPredicateBase) EnumRecipeSync.readUnknown(buf), out, buf.readVarInt());
		r.setTime(buf.readVarInt());
		return r;
	}
}
