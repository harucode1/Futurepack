package futurepack.common.recipes.crafting;

import java.util.UUID;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.container.Container;

public class InventoryCraftingForResearch extends CraftingInventory
{
	private Object user;
	
	public InventoryCraftingForResearch(Container p_i1807_1_, int width, int height, PlayerEntity user)
	{
		super(p_i1807_1_, width, height);
		this.user = user;
	}

	public InventoryCraftingForResearch(Container p_i1807_1_, int width, int height, UUID user)
	{
		super(p_i1807_1_, width, height);
		this.user = user;
	}
	
	public Object getUser()
	{
		return user;
	}

}
