package futurepack.common.recipes.crafting;

import com.google.gson.JsonObject;

import futurepack.api.Constants;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapelessRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class ShapelessRecipeWithResearch extends ShapelessRecipe
{

	public ShapelessRecipeWithResearch(ResourceLocation id, String group, ItemStack result, NonNullList<Ingredient> inputs)
	{
		super(id, group, result, inputs);		
	}
	
	public ShapelessRecipeWithResearch(ShapelessRecipe base)
	{
		this(base.getId(), base.getGroup(), base.getResultItem(), base.getIngredients());
	}

	@Override
	public boolean matches(CraftingInventory inv, World world)
	{
		if(inv instanceof InventoryCraftingForResearch)
		{
			InventoryCraftingForResearch research = (InventoryCraftingForResearch) inv;
			
			if(isUseable(research.getUser(), this.getResultItem(), world))
			{
				if(super.matches(inv, world))
				{
					return true;
				}
			}				
		}
		else
		{
			PlayerEntity pl = ShapedRecipeWithResearch.getPlayerFromWorkbench(inv);
			if(pl!=null)
			{
				if(isUseable(pl, this.getResultItem(), world))
				{
					return super.matches(inv, world);	
				}				
			}
		}
		return false;
	}
	
	public boolean isLocalResearched()
	{
		return isUseable(null, this.getResultItem(), null);
	}
	
	private static boolean isUseable(Object id, ItemStack it, World w)
	{
		return ShapedRecipeWithResearch.isUseable(id, it, w);
	}
	
	public static class Factory extends ShapelessRecipe.Serializer
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "research_shapeless");
		
		@Override
		public ShapelessRecipeWithResearch fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new ShapelessRecipeWithResearch(super.fromJson(recipeId, json));
		}

		@Override
		public ShapelessRecipeWithResearch fromNetwork(ResourceLocation recipeId, PacketBuffer buffer)
		{
			return new ShapelessRecipeWithResearch(super.fromNetwork(recipeId, buffer));
		}
	}
	
	@Override
	public IRecipeSerializer<?> getSerializer()
	{
		return FPSerializers.RESEARCH_SHAPELESS;
	}
}
