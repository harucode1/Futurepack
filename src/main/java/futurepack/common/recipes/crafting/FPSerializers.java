package futurepack.common.recipes.crafting;

import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class FPSerializers
{
	public static final IRecipeSerializer<?> RESEARCH_SHAPED = new ShapedRecipeWithResearch.Factory().setRegistryName(ShapedRecipeWithResearch.Factory.NAME);
	public static final IRecipeSerializer<?> RESEARCH_SHAPELESS = new ShapelessRecipeWithResearch.Factory().setRegistryName(ShapelessRecipeWithResearch.Factory.NAME);
	public static final IRecipeSerializer<?> MASCHIN_SHAPED = new RecipeMaschin.Factory().setRegistryName(RecipeMaschin.Factory.NAME);
	public static final IRecipeSerializer<?> OWNER_SHAPED = new RecipeOwnerBased.Factory().setRegistryName(RecipeOwnerBased.Factory.NAME);
	public static final IRecipeSerializer<?> DYING = new RecipeDyeing.Factory().setRegistryName(RecipeDyeing.Factory.NAME);
	
	
	public static void init(Register<IRecipeSerializer<?>> event)
	{
		//maguic
		IForgeRegistry<IRecipeSerializer<?>> reg = event.getRegistry();
		reg.registerAll(RESEARCH_SHAPED, RESEARCH_SHAPELESS, MASCHIN_SHAPED, OWNER_SHAPED, DYING);
	}
}
