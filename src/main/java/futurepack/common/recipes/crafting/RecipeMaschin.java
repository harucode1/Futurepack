package futurepack.common.recipes.crafting;

import java.util.function.Predicate;

import com.google.gson.JsonObject;

import futurepack.api.Constants;
import futurepack.common.block.modification.TileEntityElektroMagnet;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.block.modification.TileEntityModulT1Calculation;
import futurepack.common.item.CraftingItems;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

public class RecipeMaschin extends ShapedRecipeWithResearch
{
	public static final Predicate<ItemStack> IS_MASCHINEBOARD = new ItemStack(CraftingItems.maschineboard)::sameItem;
	public static final Predicate<ItemStack> IS_DOUBLE_MASCHINEBOARD = new ItemStack(CraftingItems.double_maschineboard)::sameItem;
	
	public RecipeMaschin(ShapedRecipe base)
    {
		super(base);
    }
	
	@Override
	public ItemStack assemble(CraftingInventory inv) 
	{
		ItemStack it = super.assemble(inv);
		if(IS_MASCHINEBOARD.test(it))
		{
			TileEntityModificationBase m = new TileEntityElektroMagnet();
			ItemStack ram = inv.getItem(1).copy(); //row & column : 1, 0
			ram.setCount(1);
			ItemStack core = inv.getItem(4).copy(); //row & column : 1, 1
			core.setCount(1);
			ItemStack chip = inv.getItem(7).copy(); //row & column : 1, 2
			chip.setCount(1);
			m.getInventory().setItem(0, chip);
			m.getInventory().setItem(9, core);
			m.getInventory().setItem(10, ram);
			CompoundNBT nbt = m.save(new CompoundNBT());
			nbt.remove("id");
			if(!it.hasTag())
				it.setTag(new CompoundNBT());
			
			it.getTag().put("tile", nbt);
		}
		else if(IS_DOUBLE_MASCHINEBOARD.test(it))
		{
			TileEntityModulT1Calculation m = new TileEntityModulT1Calculation();
			ItemStack ram = inv.getItem(7).copy();// row & column : 1,2
			ram.setCount(1);
			ItemStack core1 = inv.getItem(0).copy();// row & column : 0,0
			ItemStack core2 = inv.getItem(2).copy();// row & column : 2,0
			core1.setCount(1);
			core2.setCount(1);
			ItemStack chip = inv.getItem(4).copy();// row & column : 1,1
			chip.setCount(1);
			m.getInventory().setItem(0, chip);
			m.getInventory().setItem(3, core1);
			m.getInventory().setItem(4, core2);
			m.getInventory().setItem(5, ram);
			CompoundNBT nbt = new CompoundNBT();
			m.save(nbt);
			nbt.remove("id");
			if(!it.hasTag())
				it.setTag(new CompoundNBT());
			
			it.getTag().put("tile", nbt);
		}
		else
		{
			ItemStack board = null;
			for(int i=0;i<inv.getContainerSize();i++)
			{
				ItemStack c = inv.getItem(i);
				if(c!=null && c.hasTag() && c.getTag().contains("tile"))
				{
					if(board==null)
						board = c;
					else
					{
						CompoundNBT nbt = board.getTag().getCompound("tile");
						CompoundNBT tag = c.getTag().getCompound("tile");
						nbt.merge(tag);
						board.getTag().put("tile", nbt);
					}
				}
			}
			if(board != null)
			{
				if(board.getTag().contains("tile"))
				{
					board.getTag().getCompound("tile").remove("logisticFaces");
				}
				it.setTag(board.getTag().copy());
			}
			
		}
		return it;
	}
	
	public static class Factory extends ShapedRecipe.Serializer 
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "maschin_shaped");
	      
		@Override
		public RecipeMaschin fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new RecipeMaschin(super.fromJson(recipeId, json));
		} 

		@Override
		public RecipeMaschin fromNetwork(ResourceLocation recipeId, PacketBuffer buffer) 
		{
			return new RecipeMaschin(super.fromNetwork(recipeId, buffer));
		}
	}
	
	@Override
	public IRecipeSerializer<?> getSerializer()
	{
		return FPSerializers.MASCHIN_SHAPED;
	}
}
