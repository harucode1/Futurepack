package futurepack.common.recipes.crafting;

import java.util.UUID;

import com.google.gson.JsonObject;

import futurepack.api.Constants;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

public class RecipeOwnerBased extends ShapedRecipeWithResearch
{

	public RecipeOwnerBased(ShapedRecipe base)
	{
		super(base);
	}
	
	@Override
	public ItemStack assemble(CraftingInventory inv)
	{
		ItemStack it = super.assemble(inv);
		UUID player = null;
		int charges = 0;
		
		for(int i=0;i<inv.getContainerSize();i++)
		{
			ItemStack in = inv.getItem(i);
			if(in.getTagElement("owner") != null)
			{
				CompoundNBT nbt = in.getTagElement("owner");
				player = nbt.getUUID("creator");
				charges += nbt.getInt("charges");//special case for entity egger
			}
		}
		
		if(player==null)
		{
			if(inv instanceof InventoryCraftingForResearch)
			{
				InventoryCraftingForResearch research = (InventoryCraftingForResearch) inv;
				Object o = research.getUser();
				if(o instanceof UUID)
				{
					player = (UUID) o;
				}
				else if(o instanceof PlayerEntity)
				{
					player = ((PlayerEntity) o).getGameProfile().getId();
				}
			}
			else
			{
				PlayerEntity pl = getPlayerFromWorkbench(inv);
				if(pl!=null)
				{
						player = pl.getGameProfile().getId();		
				}
			}
		}
		charges++;
		
		if(player!=null)
		{
			CompoundNBT nbt = it.getOrCreateTagElement("owner");
			nbt.putUUID("creator", player);
			nbt.putInt("charges", charges);
		}
		return it;
	}
	
	public static class Factory extends ShapedRecipe.Serializer 
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "owner_shaped");

		@Override
		public RecipeOwnerBased fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new RecipeOwnerBased(super.fromJson(recipeId, json));
		} 

		@Override
		public RecipeOwnerBased fromNetwork(ResourceLocation recipeId, PacketBuffer buffer) 
		{
			return new RecipeOwnerBased(super.fromNetwork(recipeId, buffer));
		}
	}
	
	@Override
	public IRecipeSerializer<?> getSerializer() 
	{
		return FPSerializers.OWNER_SHAPED;
	}
}
