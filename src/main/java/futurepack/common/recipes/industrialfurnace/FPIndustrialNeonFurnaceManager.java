package futurepack.common.recipes.industrialfurnace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.common.FPLog;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.common.recipes.SingletonInventory;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.extensions.jei.FuturepackUids;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class FPIndustrialNeonFurnaceManager implements ISyncedRecipeManager<IndNeonRecipe>
{
	public static final String NAME = "indus_neon_furnace";
	
	public static FPIndustrialNeonFurnaceManager instance = new FPIndustrialNeonFurnaceManager();
	public ArrayList<IndNeonRecipe> recipes = new ArrayList<IndNeonRecipe>();
	private boolean[] uses;
	
	public static IndNeonRecipe addRecipe(String id, int support, ItemStack out, ItemPredicateBase...in)
	{
		return instance.addNeonIndRecipe(id, out, in, support);
	}
	
	public static IndNeonRecipe addRecipe(String id, int support, ItemStack out, ItemStack...in)
	{
		return instance.addNeonIndRecipe(id, out, Arrays.stream(in).map(ItemPredicate::new).toArray(ItemPredicateBase[]::new), support);
	}
	
	@Override
	public ArrayList<IndNeonRecipe> getRecipes()
	{
		return new ArrayList<IndNeonRecipe>(recipes);
	}
	
	public IndNeonRecipe addNeonIndRecipe(String id, ItemStack out, ItemPredicateBase[] in, int support)
	{
		IndNeonRecipe r = new IndNeonRecipe(id, support, out, in);
		recipes.add(r);
		FPLog.logger.debug(r.toString());
		return r;
	}
	
	@Override
	public void addRecipe(IndNeonRecipe t) 
	{
		if(t==null)
			throw new NullPointerException();
		recipes.add(t);
	}
	
	@Override
	public ResourceLocation getName() 
	{
		return FuturepackUids.INDUSTRIALNEONFURNACE;
	}
	
	public IndNeonRecipe[] getMatchingRecipes(ItemStack[] in, World w)
	{	
		boolean empty = true;
		for(ItemStack it : in)
		{
			if(!it.isEmpty())
			{
				empty = false;
				break;
			}
		}
		
		if(empty)
			return new IndNeonRecipe[0];
		
		ArrayList<IndNeonRecipe> l = new ArrayList<IndNeonRecipe>();
		boolean[] uses = new boolean[in.length];
		for(IndNeonRecipe rec : recipes)
		{
			if(rec.match(in, uses))
			{
				l.add(rec);
			}
		}
		if(l.isEmpty())
		{
			for(int i=0;i<in.length;i++)
			{
				if(in[i]!=null && !in[i].isEmpty())
				{
					SingletonInventory inv = new SingletonInventory(in[i]);
					Optional<FurnaceRecipe> out = w.getRecipeManager().getRecipeFor(IRecipeType.SMELTING, inv, w);
					if(out!=null && out.isPresent())
					{
						uses[i] = true;
						//ItemStack[] infurn = new ItemStack[in.length];
						//infurn[i] = in[i];
						ItemStack it = HelperOreDict.FuturepackConveter.getChangedItemSizeSensitiv(out.get().getResultItem());
						//ItemStack[] infurn = new ItemStack[in.length];
						//infurn[i] = in[i];
						ItemStack copy = in[i].copy();
						copy.setCount(1);
						IndNeonRecipe rec = new IndNeonRecipe("smelting_"+in[i].getItem().getRegistryName(), 0, it, new ItemPredicate(copy));
						l.add(rec);
					}
				}
				
			}
		}
		if(!l.isEmpty())
		{
			Collections.sort(l, new Comparator<IndNeonRecipe>()
			{
				@Override
				public int compare(IndNeonRecipe o1, IndNeonRecipe o2) 
				{
					return o1.input.length - o2.input.length;
				}
			});
		}
		this.uses = uses;
		return l.toArray(new IndNeonRecipe[l.size()]);
	}
	
	public IndNeonRecipe[] getMatchingRecipes(final String id)
	{
		return recipes.stream().filter(r -> r.id.equals(id)).toArray(IndNeonRecipe[]::new);
	}
	
	public boolean[] getAndClearUses()
	{
		if(this.uses !=null)
		{
			boolean[] usesB = Arrays.copyOf(this.uses, this.uses.length);
			this.uses = null;
			return usesB;
		}
		return new boolean[3];
	}
	
	public static void init(JsonArray recipes)
	{
		instance = new FPIndustrialNeonFurnaceManager();
		
		FPLog.logger.info("Setup Industrial NeonFurnace Recipes");
		for(JsonElement elm : recipes)
		{
			encodeJsonPart(elm.getAsJsonObject());
		}
	}
	
	public static void encodeJsonPart(JsonObject obj)
	{
		List<ItemStack> lout = HelperJSON.getItemFromJSON(obj.get("output"), false);
		
		for(ItemStack it : lout)
		{
			if(it.getItem().getRegistryName().getNamespace().equals(Constants.MOD_ID))
			{
				lout = Collections.singletonList(it);
				break;
			}
		}
		
		if(lout !=null)
		{
			JsonArray array = obj.get("input").getAsJsonArray();
			ItemPredicateBase[] its = new ItemPredicateBase[array.size()];
			for(int i=0;i<array.size();i++)
			{
				its[i] = HelperJSON.getItemPredicateFromJSON(array.get(i));
				if(its[i].collectAcceptedItems(new ArrayList<>()).isEmpty())
				{
					FPLog.logger.warn("Broken ind neon furnace recipe with empty input %s", array.get(i).toString());
					return;
				}
			}
			int support = obj.getAsJsonPrimitive("support").getAsInt();
			String name = obj.getAsJsonPrimitive("id").getAsString();
			
			for(ItemStack out : lout)
			{
				addRecipe(name, support, out, its);
			}
		}
	}
	
//	private static void addAllRecipes(int n, List<ItemStack>[] it, ItemStack out, ItemStack[] content, int support, String id)
//	{
//		for(int i=0;i<it[n].size();i++)
//		{
//			content[n] = it[n].get(i);
//			//System.out.println(Arrays.toString(content));
//			
//			if( (n+1) < it.length )
//				addAllRecipes(n+1, it, out, content, support, id);
//			else
//				instance.addNeonIndRecipe(id, out, content, support);
//		}
//	}
}
