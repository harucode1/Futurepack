package futurepack.common.recipes.industrialfurnace;

import java.util.Arrays;

import futurepack.api.ItemPredicateBase;
import futurepack.common.recipes.EnumRecipeSync;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;

public class IndNeonRecipe extends IndRecipe
{
	private int neededSupport;			
	
	
	public IndNeonRecipe(String id, int support, ItemStack out, ItemPredicateBase...in) 
	{
		super(id, out, in);
		this.neededSupport = support;
	}
	
	public int getInputCount()
	{
		return input.length;
	}
	
	@Override
	public String toString() 
	{
		return "IndNeonFurnace: " +id + " " +Arrays.toString(input) + " to " + output;
	}

	public int getSupport()
	{
		return this.neededSupport;
	}
	
	@Override
	public void write(PacketBuffer buf)
	{
		buf.writeUtf(id);
		buf.writeVarInt(neededSupport);
		buf.writeItem(output);
		EnumRecipeSync.writePredicates(input, buf);
	}
	
	public static IndNeonRecipe read(PacketBuffer buf)
	{
		return new IndNeonRecipe(buf.readUtf(), buf.readVarInt(), buf.readItem(), EnumRecipeSync.readPredicates(buf));
	}
}
