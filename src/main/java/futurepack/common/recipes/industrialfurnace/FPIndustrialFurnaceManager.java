package futurepack.common.recipes.industrialfurnace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.common.FPLog;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.common.recipes.SingletonInventory;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.extensions.jei.FuturepackUids;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
public class FPIndustrialFurnaceManager implements ISyncedRecipeManager<IndRecipe>
{
	public static final String NAME = "indus_furnace";
	
	public static FPIndustrialFurnaceManager instance = new FPIndustrialFurnaceManager();
	public ArrayList<IndRecipe> recipes = new ArrayList<IndRecipe>();
	private boolean[] uses;
	
	public static IndRecipe addRecipe(String id, ItemStack out, ItemPredicateBase...in)
	{
		return instance.addIndRecipe(id, out, in);
	}
	
	public static IndRecipe addRecipe(String id, ItemStack out, ItemStack...in)
	{
		return instance.addIndRecipe(id, out, Arrays.stream(in).map(ItemPredicate::new).toArray(ItemPredicateBase[]::new));
	}
	
	@Override
	public ArrayList<IndRecipe> getRecipes()
	{
		return new ArrayList<IndRecipe>(recipes);
	}
	
	public IndRecipe addIndRecipe(String id, ItemStack out, ItemPredicateBase[] in)
	{
		IndRecipe r = new IndRecipe(id, out, in);
		recipes.add(r);
		FPLog.logger.debug(r.toString());
		return r;
	}
	
	@Override
	public void addRecipe(IndRecipe t) 
	{
		recipes.add(t);
	}
	
	@Override
	public ResourceLocation getName() 
	{
		return FuturepackUids.INDUTRIALFURNACE;
	}
	
	public IndRecipe[] getMatchingRecipes(ItemStack[] in, World w)
	{
		boolean empty = true;
		for(ItemStack it : in)
		{
			if(!it.isEmpty())
			{
				empty = false;
				break;
			}
		}
		
		if(empty)
			return new IndRecipe[0];
		
		ArrayList<IndRecipe> l = new ArrayList<IndRecipe>();
		boolean[] uses = new boolean[in.length];
		for(IndRecipe rec : recipes)
		{
			if(rec.match(in, uses))
			{
				l.add(rec);
			}
		}
		if(l.isEmpty())
		{
			for(int i=0;i<in.length;i++)
			{
				if(in[i]!=null && !in[i].isEmpty())
				{
					SingletonInventory inv = new SingletonInventory(in[i]);
					Optional<FurnaceRecipe> out = w.getRecipeManager().getRecipeFor(IRecipeType.SMELTING, inv, w);
					if(out!=null && out.isPresent())
					{
						uses[i] = true;
						//ItemStack[] infurn = new ItemStack[in.length];
						//infurn[i] = in[i];
						ItemStack it = HelperOreDict.FuturepackConveter.getChangedItemSizeSensitiv(out.get().getResultItem());
						ItemStack input = in[i].copy();
						input.setCount(1);
						IndRecipe rec = new IndRecipe("smelting_"+in[i].getItem().getRegistryName() , it, new ItemPredicate(input));
						l.add(rec);
					}
				}
				
			}
		}
		if(!l.isEmpty())
		{
			Collections.sort(l, new Comparator<IndRecipe>()
			{
				@Override
				public int compare(IndRecipe o1, IndRecipe o2) 
				{
					return o1.input.length - o2.input.length;
				}
			});
		}
		this.uses = uses;
		return l.toArray(new IndRecipe[l.size()]);
	}
	
	public IndRecipe[] getMatchingRecipes(final String id)
	{
		return recipes.stream().filter(r -> r.id.equals(id)).toArray(IndRecipe[]::new);
	}
	
	public boolean[] getAndClearUses()
	{
		if(this.uses !=null)
		{
			boolean[] usesB = Arrays.copyOf(this.uses, this.uses.length);
			this.uses = null;
			return usesB;
		}
		return new boolean[3];
	}
	
	public static void encodeJsonPart(JsonObject obj)
	{
		List<ItemStack> lout = HelperJSON.getItemFromJSON(obj.get("output"), false);
		
		for(ItemStack it : lout)
		{
			if(it.getItem().getRegistryName().getNamespace().equals(Constants.MOD_ID))
			{
				lout = Collections.singletonList(it);
				break;
			}
		}
			
		if(lout !=null)
		{
			JsonArray array = obj.get("input").getAsJsonArray();
			ItemPredicateBase[] its = new ItemPredicateBase[array.size()];
			for(int i=0;i<array.size();i++)
			{
				its[i] = HelperJSON.getItemPredicateFromJSON(array.get(i));
				if(its[i].collectAcceptedItems(new ArrayList<>()).isEmpty())
				{
					FPLog.logger.warn("Broken ind furnace recipe with empty input %s", array.get(i).toString());
					return;
				}
			}
			String id = obj.getAsJsonPrimitive("id").getAsString();
			
			for(ItemStack out : lout)
			{
				//addAllRecipes(0, its, out, new ItemStack[array.size()], id);
				addRecipe(id, out, its);
			}
		}
	}

	public static void init(JsonArray recipes)
	{
		instance = new FPIndustrialFurnaceManager();
		
		FPLog.logger.info("Setup Industrial Furnace Recipes");

		for(JsonElement elm : recipes)
		{
			encodeJsonPart(elm.getAsJsonObject());
		}
	}

}
