package futurepack.common.recipes.crushing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IOptimizeable;
import futurepack.common.AsyncTaskManager;
import futurepack.common.FPLog;
import futurepack.common.FuturepackTags;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.extensions.jei.FuturepackUids;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;

public class FPCrushingManager implements ISyncedRecipeManager<CrushingRecipe>
{
	public static final String NAME = "crushing";
	
	public static FPCrushingManager instance = new FPCrushingManager();
	public ArrayList<CrushingRecipe> recipes = new ArrayList<CrushingRecipe>();
	
	public static CrushingRecipe addCrusherRecipe(ItemPredicateBase in, ItemStack out)
	{
		return instance.addRecipe(in, out);
	}
	
	public static CrushingRecipe addCrusherRecipe(ItemStack in, ItemStack out)
	{
		return instance.addRecipe(new ItemPredicate(in), out);
	}
	
	public CrushingRecipe addRecipe(ItemPredicateBase in, ItemStack out)
	{
		CrushingRecipe r = new CrushingRecipe(in, out);
		recipes.add(r);
		FPLog.logger.debug("Added Crusher Recipe %s",r);
		return r;
	}
	
	@Override
	public void addRecipe(CrushingRecipe t) 
	{
		recipes.add(t);
	}
	
	@Override
	public Collection<CrushingRecipe> getRecipes() 
	{
		return recipes;
	}
	
	@Override
	public ResourceLocation getName() 
	{
		return FuturepackUids.CRUSHER;
	}
	
	public CrushingRecipe getRecipe(ItemStack it)
	{
		Iterator<CrushingRecipe> iter = recipes.iterator();
		while(iter.hasNext())
		{
			CrushingRecipe r = iter.next();
			if(r.matches(it))
			{
				r.addPoint();
				if(r.getPoints() > 1000)
					resort();
				return r;
			}
		}
		return getDustFromItem(it.getItem());
	}
	
	private void resort()
	{
		final ArrayList<CrushingRecipe> copy = new ArrayList<CrushingRecipe>(recipes);
		Thread t = new Thread(new Runnable()
		{	
			@Override
			public void run()
			{
				IOptimizeable.sortList(copy);
				copy.forEach(IOptimizeable::resetPoints);
				recipes = copy;
			}
		});
		t.start();
	}
	
	private static List<ResourceLocation> getTagsFromNamespace(Predicate<ResourceLocation> pred, Item it)
	{
		Set<Entry<ResourceLocation, ITag<Item>>> set = ItemTags.getAllTags().getAllTags().entrySet();
		List<ResourceLocation> list = set.stream()
				.filter(e -> pred.test(e.getKey()))
				.filter(e -> e.getValue().contains(it))
				.map(Entry::getKey)
				.collect(Collectors.toList());
		return list;
	}
	
	public CrushingRecipe getDustFromItem(Item item)
	{
		int count = 0;
		List<ResourceLocation> list = null;
		
		if(FuturepackTags.GEMS.contains(item))
		{
			count = 1;
			list = getTagsFromNamespace(res -> "forge".equals(res.getNamespace()) && res.getPath().startsWith("gems/"), item);
		}
		else if(FuturepackTags.INGOTS.contains(item))
		{
			count = 1;
			list = getTagsFromNamespace(res -> "forge".equals(res.getNamespace()) && res.getPath().startsWith("ingots/"), item);
		}
		else if(FuturepackTags.ORES.contains(item))
		{
			count = 2;
			list = getTagsFromNamespace(res -> "forge".equals(res.getNamespace()) && res.getPath().startsWith("ores/"), item);
		}
		
		if(count > 0 && list!=null && !list.isEmpty())
		{
			ResourceLocation res = list.get(0);
			String name = res.getPath().split("/", 2)[1];
			ResourceLocation dust = new ResourceLocation("forge", "dusts/" + name);
			Item ddust = HelperOreDict.FuturepackConveter.getChangedItem(dust);
			if(ddust!=null)
			{
				return addCrusherRecipe(new ItemStack(item, 1), new ItemStack(ddust, count));
			}	
		}
		return null;
	}
	
	public static void init(JsonArray recipes)
	{
		instance = new FPCrushingManager();
		
		FPLog.logger.info("Setup Crusher Recipes");
		for(JsonElement elm : recipes)
		{
			setupObject(elm);
		}

		AsyncTaskManager.addTask(AsyncTaskManager.RESOURCE_RELOAD, new Runnable()
		{	
			@Override
			public void run() 
			{
				HelperOreDict.waitForTagsToLoad();
				long ntime = System.nanoTime();
				Arrays.asList(FuturepackTags.GEMS, FuturepackTags.ORES, FuturepackTags.INGOTS).stream().flatMap(t -> t.getValues().stream()).forEach(instance::getDustFromItem);
				FPLog.logger.info("Setup Crusher Recipes finised after: " + (System.nanoTime() - ntime) / 1000000.0 + " ms");
			}
		}, true);
	}
	
	private static void setupObject(JsonElement o)
	{
		if(o.isJsonObject())
		{
			JsonObject obj = o.getAsJsonObject();
			ItemPredicateBase in = HelperJSON.getItemPredicateFromJSON(obj.get("input"));
			if(in.collectAcceptedItems(new ArrayList<>()).isEmpty())
			{
				FPLog.logger.warn("Broken crushing recipe with empty input %s", obj.get("input").toString());
				return;
			}
			List<ItemStack> out = HelperJSON.getItemFromJSON(obj.get("output"), false);
			ItemStack itout = HelperOreDict.FuturepackConveter.getChangedItemSizeSensitiv(out.get(0));
			
			addCrusherRecipe(in, itout);
		}
		else
		{
			FPLog.logger.error("Wrong JSON Type for a Recipe:  \"" + o + "\"");
		}
	}
}

