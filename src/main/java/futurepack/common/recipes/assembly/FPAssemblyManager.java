package futurepack.common.recipes.assembly;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IOptimizeable;
import futurepack.common.FPLog;
import futurepack.common.item.ComputerItems;
import futurepack.common.item.ResourceItems;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.extensions.jei.FuturepackUids;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class FPAssemblyManager implements ISyncedRecipeManager<AssemblyRecipe>
{
	public static final String NAME = "assembly";
	
	public static FPAssemblyManager instance = new FPAssemblyManager();//dummy instance
	public ArrayList<AssemblyRecipe> recipes = new ArrayList<AssemblyRecipe>();
	
	public AssemblyRecipe getMatchingRecipe(ItemStack[] in/*, EntityPlayer user*/)
	{
		for(AssemblyRecipe r : recipes)
		{
			if(r.matches(in))
			{
				r.addPoint();
				if(r.getPoints() > 1000)
					resort();
				return r;
			}
				
		}
		return null;
	}
	
	public AssemblyRecipe[] getMatchingRecipes(final String id)
	{
		return recipes.stream().filter(r -> r.id.equals(id)).toArray(AssemblyRecipe[]::new);
	}
	
	private void resort()
	{
		final ArrayList<AssemblyRecipe> copy = new ArrayList<AssemblyRecipe>(recipes);
		Thread t = new Thread(new Runnable()
		{	
			@Override
			public void run()
			{
				IOptimizeable.sortList(copy);
				copy.forEach(IOptimizeable::resetPoints);
				recipes = copy;
			}
		});
		t.start();
	}
	
	public AssemblyRecipe addRecipe(String id, ItemStack out, ItemPredicateBase[] in)
	{
		AssemblyRecipe rec = new AssemblyRecipe(id, out, in);
		recipes.add(rec);
		FPLog.logger.debug(rec.toString());
		return rec;
	}
	
	@Deprecated
	public AssemblyRecipe addRecipe(String id, ItemStack out, ItemStack[] in)
	{
		ItemPredicateBase[] preds = Arrays.stream(in).map(ItemPredicate::new).toArray(ItemPredicate[]::new);
		return addRecipe(id, out, preds);
	}
	
	@Override
	public void addRecipe(AssemblyRecipe t) 
	{
		recipes.add(t);
	}
	
	@Override
	public Collection<AssemblyRecipe> getRecipes() 
	{
		return recipes;
	}
	
	@Override
	public ResourceLocation getName() 
	{
		return FuturepackUids.ASSEMBLY;
	}
	
	private static void setupObject(JsonElement o)
	{
		if(o.isJsonObject())
		{
			JsonObject obj = o.getAsJsonObject();
			List<ItemStack> out = HelperJSON.getItemFromJSON(obj.get("output"), false);
			
			JsonArray arr = obj.get("input").getAsJsonArray();
			ItemPredicateBase[] in = new ItemPredicateBase[arr.size()];
			for(int i=0;i<arr.size();i++)
			{
				in[i] = HelperJSON.getItemPredicateFromJSON(arr.get(i));
				if(in[i]==null || in[i].collectAcceptedItems(new ArrayList<>()).isEmpty())
				{
					FPLog.logger.warn("Broken assembly recipe with input " + arr.get(i).toString() + " and output " + obj.get("output"));
					return;
				}
			}
			
			String id = obj.getAsJsonPrimitive("id").getAsString();
			if(out.size()>1)
			{
				FPLog.logger.warn("A assembly recipe has multiple outputs, this is not intended id:" + id);
			}
			else if(out.isEmpty())
			{
				FPLog.logger.warn("A assembly recipe was empty id:" + id);
			}
			
			for(ItemStack it : out)
			{
				instance.addRecipe(id, it, in);
			}
		}
		else
		{
			FPLog.logger.error("Wrong JSON Type for a Recipe:  \"" + o + "\"");
		}
	}
	
	public static void init(JsonArray recipes)
	{
		instance = new FPAssemblyManager();
		
		FPLog.logger.info("Setup Assemply Recipes");
		
		for(JsonElement elm : recipes)
		{
			setupObject(elm);
		}
		
		instance.recipes.add(new AssemblyDualRecipe("core_torus", new ItemStack(ComputerItems.torus_core,1), new ItemStack[]{new ItemStack(ComputerItems.dungeon_core,1),new ItemStack(ResourceItems.parts_diamond),new ItemStack(ComputerItems.dungeon_core,1)}));
		instance.recipes.add(new AssemblyDualRecipe("ram_torus", new ItemStack(ComputerItems.torus_ram,1), new ItemStack[]{new ItemStack(ComputerItems.dungeon_ram,1),new ItemStack(ResourceItems.parts_neon),new ItemStack(ComputerItems.dungeon_ram,1)}));
	}
	
}
