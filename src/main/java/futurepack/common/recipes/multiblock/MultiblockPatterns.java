package futurepack.common.recipes.multiblock;

import futurepack.common.FuturepackTags;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.multiblock.BlockDeepCoreMiner;
import futurepack.common.block.multiblock.BlockDeepCoreMiner.EnumDeepCoreMiner;
import futurepack.common.block.multiblock.MultiblockBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.block.pattern.BlockMaterialMatcher;
import net.minecraft.block.pattern.BlockPattern;
import net.minecraft.block.pattern.BlockPatternBuilder;
import net.minecraft.block.pattern.BlockStateMatcher;
import net.minecraft.util.CachedBlockInfo;

public class MultiblockPatterns
{	
//	private static boolean isMetalFence(IBlockState state)
//	{
//		return state.getBlock() == FPBlocks.colorIronFence || (state.getBlock() == FPBlocks.metalFence && state.getValue(FPBlocks.META(1)) == 0);
//	}
//	
//	private static boolean isMetalBlock(IBlockState state)
//	{
//		return (state.getBlock() == FPBlocks.erzBlocke && (state.getValue(BlockErzeBlocke.types) == EnumOreBlockTypes.METAL || state.getValue(BlockErzeBlocke.types) == EnumOreBlockTypes.LUFTSCHACHT)) 
//				|| state.getBlock() == FPBlocks.colorIron
//				|| state.getBlock() == FPBlocks.colorLuftung;
//	}
	
	private static boolean isMainBlock(BlockState state)
	{
		return state.getBlock() == MultiblockBlocks.deepcore_miner && state.getValue(BlockDeepCoreMiner.variants) == EnumDeepCoreMiner.maschine;
	}
	
	private static boolean isLaser(BlockState state)
	{
		return state.getBlock() == ModifiableBlocks.entity_eater || state.getBlock() == ModifiableBlocks.entity_healer || state.getBlock() == ModifiableBlocks.entity_killer;
	}
	
	public static BlockPattern getDeepMinerPattern()
	{
		return BlockPatternBuilder.start().aisle("FBC", "FLC", "M~C")
				.where('~', CachedBlockInfo.hasState(BlockMaterialMatcher.forMaterial(Material.AIR)))
				.where('L', CachedBlockInfo.hasState(MultiblockPatterns::isLaser))
				.where('C', CachedBlockInfo.hasState(BlockStateMatcher.forBlock(InventoryBlocks.composite_chest)))
				.where('F', CachedBlockInfo.hasState(s -> s.is(FuturepackTags.METAL_FENCE)))
				.where('B', CachedBlockInfo.hasState(s -> s.is(FuturepackTags.METAL_BLOCK)))
				.where('M', CachedBlockInfo.hasState(MultiblockPatterns::isMainBlock))
				.build();
		
	}
	
//	public static BlockPattern getDeepMinerPatternActive()
//	{
//		return FactoryBlockPattern.start().aisle("FBC", "FLC", "M~C")
//				.where('~', BlockWorldState.hasState(BlockMaterialMatcher.forMaterial(Material.AIR)))
//				.where('L', BlockWorldState.hasState(BlockStateMatcher.forBlock(FPBlocks.entityEater)))
//				.where('C', BlockWorldState.hasState(BlockStateMatcher.forBlock(FPBlocks.composite_chest)))
//				.where('F', BlockWorldState.hasState(MultiblockPatterns::isMetalFence))
//				.where('B', BlockWorldState.hasState(MultiblockPatterns::isMetalBlock))
//				.where('M', BlockWorldState.hasState(MultiblockPatterns::isMainBlock))
//				.build();
//		
//	}
}
