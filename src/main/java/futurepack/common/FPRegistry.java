package futurepack.common;


import futurepack.api.Constants;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.multiblock.MultiblockBlocks;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.fluids.FPFluids;
import futurepack.common.item.ComputerItems;
import futurepack.common.item.CraftingItems;
import futurepack.common.item.FoodItems;
import futurepack.common.item.ResourceItems;
import futurepack.common.item.SpawnEggItems;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.recipes.crafting.FPSerializers;
import futurepack.world.dimensions.ChunkGeneratorAsteroidBelt;
import futurepack.world.dimensions.biomes.FPBiomes;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.potion.Effect;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.carver.WorldCarver;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = Constants.MOD_ID, bus = Bus.MOD)
public class FPRegistry
{
	static
	{
		Registry.register(Registry.CHUNK_GENERATOR, "futurepack:asteroid_belt", ChunkGeneratorAsteroidBelt.CODEC);
		FPBiomes.registerBiomeProvider();
	}
	
	@SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
	{		
		DecoBlocks.registerBlocks(event);
		TerrainBlocks.registerBlocks(event);
		ModifiableBlocks.registerBlocks(event);
		InventoryBlocks.registerBlocks(event);
		MiscBlocks.registerBlocks(event);
		PlantBlocks.registerBlocks(event);
		LogisticBlocks.registerBlocks(event);
		MultiblockBlocks.registerBlocks(event);
		
		FPFluids.registerBlocks(event);
		
		System.out.println("Registering Blocks");
	}
	
	@SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
	{
		ComputerItems.register(event);
		ToolItems.register(event);
		MiscItems.register(event);
		ResourceItems.register(event);
		CraftingItems.register(event);
		FoodItems.register(event);
		SpawnEggItems.registerItems(event);
		
		DecoBlocks.registerItems(event);
		TerrainBlocks.registerItems(event);
		ModifiableBlocks.registerItems(event);
		InventoryBlocks.registerItems(event);
		MiscBlocks.registerItems(event);
		PlantBlocks.registerItems(event);
		LogisticBlocks.registerItems(event);
		MultiblockBlocks.registerItems(event);
		
		FPFluids.registerItems(event);
		
		System.out.println("Registering Items");
	}
	
	@SubscribeEvent
    public static void registerBiomes(RegistryEvent.Register<Biome> event)
	{
//		FPBiomes.register(event);
		
//		FPBiomes.registerSurfaceBuilder(Forgereg);
//		FPBiomes.registerCarvers();
//		FPBiomes.registerFeatures();
	}
	
	@SubscribeEvent
    public static void registerSurfaceBuilder(RegistryEvent.Register<SurfaceBuilder<?>> event)
	{
		FPBiomes.registerSurfaceBuilder(event.getRegistry()::register); // called
	}
	
	@SubscribeEvent
    public static void registerFeatures(RegistryEvent.Register<Feature<?>> event)
	{		
		FPBiomes.registerFeatures(event.getRegistry()::register); //called
	}
	
	@SubscribeEvent
    public static void registerCavers(RegistryEvent.Register<WorldCarver<?>> event)
	{
		FPBiomes.registerCarvers(event.getRegistry()::register);//this is called
	}
	
	@SubscribeEvent
    public static void registerPlacement(RegistryEvent.Register<Placement<?>> event)
	{
		FPBiomes.registerPlacement(event.getRegistry()::register); //called
	}
	
	@SubscribeEvent
    public static void registerPotions(RegistryEvent.Register<Effect> event)
	{
		FPPotions.register(event);
	}
	
	@SubscribeEvent
    public static void registerSounds(RegistryEvent.Register<SoundEvent> event)
	{
		FPSounds.register(event);
	}
	
//	@SubscribeEvent
//	public static void registerDimension(RegistryEvent.Register<ModDimension> event)
//	{
//		Dimensions.register(event);
//	}
	
	@SubscribeEvent
	public static void registerTileEntityTypes(RegistryEvent.Register<TileEntityType<?>> event)
	{
		FPTileEntitys.registerTileEntitys(event);
	}
	
	@SubscribeEvent
	public static void registerEntityTypes(RegistryEvent.Register<EntityType<?>> event)
	{
		FPEntitys.registerEntitys(event);
	}
	
	@SubscribeEvent
	public static void registerSerializers(RegistryEvent.Register<IRecipeSerializer<?>> event)
	{
		FPSerializers.init(event);
	}
	
	@SubscribeEvent
	public static void registerFluids(RegistryEvent.Register<Fluid> event)
	{
		FPFluids.register(event);
	}
	
}
