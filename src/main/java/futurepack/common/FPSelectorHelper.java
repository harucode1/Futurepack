package futurepack.common;

import java.util.Collection;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.WeakHashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.AbstractIterator;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.common.network.NetworkManager;
import net.minecraft.client.Minecraft;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.Mutable;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.event.world.BlockEvent;

public class FPSelectorHelper
{
	private static Map<RegistryKey<World>, WeakHashMap<IBlockSelector, FPSelectorHelper>> mapDimHelper = new IdentityHashMap<RegistryKey<World>, WeakHashMap<IBlockSelector, FPSelectorHelper>>();
	
	
	public static Map<IBlockSelector, FPSelectorHelper> getHelperForWorld(World w)
	{
		if(!mapDimHelper.containsKey(w.dimension()))
		{
//			mapDimHelper.put(w.dimension.getDimensionId(), new FPSelectorHelper(w));
			WeakHashMap<IBlockSelector, FPSelectorHelper> map = new WeakHashMap<IBlockSelector, FPSelectorHelper>();
			mapDimHelper.put(w.dimension(), map);
			return map;
		}	
		return mapDimHelper.get(w.dimension()); 
	}

	
	
	public static FPBlockSelector getSelectorSave(World w, BlockPos pos, IBlockSelector bs, boolean onlyServerThread)
	{
		Map<IBlockSelector, FPSelectorHelper> map = getHelperForWorld(w);
		FPSelectorHelper help = map.get(bs);
		if(help==null)
		{
			help = new FPSelectorHelper(w);
			map.put(bs, help);
		}
		FPBlockSelector sel = help.getSelectorFor(pos);
		if(sel == null)
		{
			final FPBlockSelector sel2 = new FPBlockSelector(w, bs);
			if(onlyServerThread && !isWorldThread(w))
			{
				Runnable task = new Runnable()
				{	
					@Override
					public void run()
					{
						w.getProfiler().push("Block Selection");
						sel2.selectBlocks(pos);
						w.getProfiler().pop();
					}
				};
				//synchronized (task)
				{
					long start = System.currentTimeMillis();
					long warned = 0;
					CompletableFuture<Void> listener = null;
					if(!w.isClientSide)
					{
						listener = w.getServer().submitAsync(task);
					}
					else
					{
						listener = Minecraft.getInstance().submitAsync(task);
					}
					
					int i=0;
					while(sel2.getNeededTime()<0F)
					{
						i++;
						Thread.yield(); //Not using wait to prevent look of server and network thread
						if(i>1000)
						{
							i=0;
							if(System.currentTimeMillis() - start >= 1000)
							{
								if(System.currentTimeMillis() - warned >= 10 * 60 * 1000)
								{
									System.out.println("FPSelectorHelper.getSelectorSave() is taking oddly long, bloking Thread:"+ Thread.currentThread().getName());
									warned = System.currentTimeMillis();
								}
								
								if(listener.isDone() || listener.isCancelled())
								{
									start = System.currentTimeMillis();
									if(!w.isClientSide)
									{
										listener = w.getServer().submitAsync(task);
									}
									else
									{
										listener = Minecraft.getInstance().submitAsync(task);
									}
								}
							}
						}
					}
				}
			}
			else
			{					
				sel2.selectBlocks(pos);
			}
			help.init(pos, sel2);
			sel = sel2;
			
		}
		return sel;
	}
	
	@Deprecated
	public static FPBlockSelector getSelector(World w, BlockPos pos, IBlockSelector bs)
	{
		return getSelectorSave(w, pos, bs, !FPConfig.SERVER.enforceExtraThreadForNetwork.get());
	}
	
	public static boolean isWorldThread(World serv)
	{
		return NetworkManager.isWorldThread(serv);
	}
	
	public static void clean()
	{
		mapDimHelper.clear();
	}
	
	public static void onBlockUpdate(World w, BlockPos pos)
	{		
		RegistryKey<World> type = (w).dimension();
		Map<IBlockSelector, FPSelectorHelper> selectors = mapDimHelper.get(type);
		if(selectors!=null)
		{
			for(Entry<IBlockSelector, FPSelectorHelper> e : selectors.entrySet())
			{
				e.getValue().notifyBlockUpdate(pos);
			}
		}
	}
	
	public static void onBlockUpdate(World world, List<BlockSnapshot> replacedBlockSnapshots) 
	{
		RegistryKey<World> type = world.dimension();
		Map<IBlockSelector, FPSelectorHelper> selectors = mapDimHelper.get(type);
		BlockPos[] posses = replacedBlockSnapshots.stream().map(BlockSnapshot::getPos).toArray(BlockPos[]::new);
		if(selectors!=null)
		{
			for(Entry<IBlockSelector, FPSelectorHelper> e : selectors.entrySet())
			{
				for(BlockPos pos : posses)
				{
					e.getValue().notifyBlockUpdate(pos);
				}
			}
		}
	}
	
	@Nonnull	
	private final World w;
	@Nonnull
	private final Map<Vector3i,SelectorWrapper> PosToShipMap = new TreeMap<Vector3i,SelectorWrapper>();
	private final ReadWriteLock rwlock;
	
//	public FPSelectorHelper(int dimension)
//	{
//		this(MinecraftServer.getServer().worldServerForDimension(dimension));
//	}
	
	public FPSelectorHelper(World dimension)
	{
		this.w = dimension;
		rwlock = new ReentrantReadWriteLock();
	}
	
	public FPBlockSelector getSelectorFor(Vector3i xyz)
	{
		rwlock.readLock().lock();
		SelectorWrapper wrapper = PosToShipMap.get(xyz);
		rwlock.readLock().unlock();
		
		if(wrapper==null)
			return null;
		
		return  wrapper.getSelector();
	}
	
	public void remove(SelectorWrapper sel)
	{
		area = null;
		
		Collection<ParentCoords> blocks = sel.blocks.getAllBlocks();
		rwlock.writeLock().lock();
		for(Vector3i xyz : blocks)
		{
			PosToShipMap.remove(xyz);
		}
		rwlock.writeLock().unlock();
		sel.blocks.clear();
	}
	
	private void init(BlockPos pos, FPBlockSelector sel)
	{
		area = null;
		
		SelectorWrapper wrapper = new SelectorWrapper(sel);
		rwlock.writeLock().lock();
		SelectorWrapper old = PosToShipMap.put(pos, wrapper);
		rwlock.writeLock().unlock();
		if(old!=null)
			old.invalidate();
		
		Iterator<ParentCoords> iter = sel.getAllBlocks().iterator();
		rwlock.writeLock().lock();
		while(iter.hasNext())
		{
			old = PosToShipMap.put(iter.next(), wrapper);
			if(old!=null && old != wrapper)
				old.invalidate();
		}
		rwlock.writeLock().unlock();
	}
	
	private void removeIfPossible(Iterable<BlockPos.Mutable> pos)
	{		
		if(PosToShipMap.isEmpty())
			return;

		Set<SelectorWrapper> wrappers = new HashSet<SelectorWrapper>();
		for(Vector3i vec : pos)
		{
			debugPos(ParticleTypes.BARRIER, vec);
			
			rwlock.readLock().lock();
			SelectorWrapper s = PosToShipMap.get(vec);
			rwlock.readLock().unlock();
			
			if(s!=null)
			{
				wrappers.add(s);
			}
		}
		if(!wrappers.isEmpty())
		{
			w.getProfiler().push("RemovingSelector");
			wrappers.forEach(FPSelectorHelper.this::remove);
			wrappers.clear();
			wrappers = null;
			w.getProfiler().pop();
		}
	}
	
	public void notifyBlockUpdate(BlockPos pos)
	{
		if(getArea().isInside(pos))
		{
			debugPos(ParticleTypes.CAMPFIRE_SIGNAL_SMOKE, pos);
			
			AbstractIterator<BlockPos.Mutable> iter = new AbstractIterator<BlockPos.Mutable>()
			{
				int j=0;
				BlockPos.Mutable mut = pos.mutable();
	
				@Override
				protected Mutable computeNext() 
				{
					switch (j)
					{
					case 0:
						break;
					case 1:
						mut.move(Direction.UP);
						break;
					case 2:
						mut.move(Direction.DOWN, 2);
						break;
					case 3:
						mut.move(0,1,-1);
						break;
					case 4:
						mut.move(Direction.SOUTH, 2);
						break;
					case 5:
						mut.move(-1, 0, -1);
						break;
					case 6:
						mut.move(Direction.EAST, 2);
						break;
					default:
						endOfData();
						break;
					}
					j++;
					return mut;
				}
				
			};
			removeIfPossible(() -> iter);
		}
		else
		{
			debugPos(ParticleTypes.INSTANT_EFFECT, pos);
		}
	}

	@SuppressWarnings("unused")
	private void debugPos(IParticleData particle, Vector3i pos)
	{
		final boolean debug = false;
		if(debug && w instanceof ServerWorld)
		{
			((ServerWorld)w).sendParticles(particle, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 1, 0F, 0F, 0F, 0F);
		}
	}
	
	private MutableBoundingBox area = null;
	
	public MutableBoundingBox getArea()
	{
		if(area!=null)
		{
			return area;
		}
		else
		{
			int x0=Integer.MAX_VALUE,y0=Integer.MAX_VALUE,z0=Integer.MAX_VALUE;
			int x1=Integer.MIN_VALUE,y1=Integer.MIN_VALUE,z1=Integer.MIN_VALUE;
			rwlock.readLock().lock();
			for(Vector3i e : PosToShipMap.keySet())
			{
				x0 = Math.min(x0, e.getX());
				x1 = Math.max(x1, e.getX());
				y0 = Math.min(y0, e.getY());
				y1 = Math.max(y1, e.getY());
				z0 = Math.min(z0, e.getZ());
				z1 = Math.max(z1, e.getZ());
			}
			rwlock.readLock().unlock();
			x0-=2;
			y0-=2;
			z0-=2;
			x1+=2;
			y1+=2;
			z1+=2;
			return area = new MutableBoundingBox(x0, y0, z0, x1, y1, z1);
		}
	}

	
	
	private class SelectorWrapper
	{
		private final FPBlockSelector blocks;
		private final long creationTime;
		public boolean valid;
		
		SelectorWrapper(FPBlockSelector blocks)
		{
			valid = true;
			this.blocks = blocks;
			creationTime = System.currentTimeMillis();
		}
		
		public boolean isValid()
		{
			if(valid)
			{
				valid = System.currentTimeMillis() - creationTime < 1000 * 30; //not older then  30 seconds
				return valid;
			}
			else
			{
				return false;
			}
		}
		
		public void invalidate()
		{
			if(valid)
			{
				valid = false;
			}
		}
		
		@Nullable
		public FPBlockSelector getSelector()
		{
			if(isValid())
				return blocks;
			else
			{
				return null;
			}
		}
	}


}
