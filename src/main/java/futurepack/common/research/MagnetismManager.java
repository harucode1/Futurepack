package futurepack.common.research;

import java.util.Comparator;

import com.google.gson.JsonObject;

import futurepack.common.FPLog;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleRBTreeMap;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

public class MagnetismManager 
{
	private static final double MAG_MAX = 64;
	
	private static Object2DoubleMap<Block> map = new Object2DoubleRBTreeMap<Block>(new Comparator<Block>() 
	{
		@Override
		public int compare(Block o1, Block o2) 
		{
			return o2.hashCode() - o1.hashCode();
		}
	});
	
	public static void readJson(ResourceLocation id, JsonObject obj)
	{
		ResourceLocation res = new ResourceLocation(obj.get("block").getAsString());
		Block bl = ForgeRegistries.BLOCKS.getValue(res);
		if(bl == null || bl == Blocks.AIR)
		{
			FPLog.logger.warn("Block %s not found", res);
			return;
		}
		double magnetism = obj.get("magnetism").getAsDouble();
		if(magnetism > MAG_MAX)
		{
			FPLog.logger.warn("Magnetism of Block %s is too Strong! (%s), setting to %s", res, magnetism, MAG_MAX);
			magnetism = MAG_MAX;
		}
		
		map.put(bl, magnetism);
	}
	
	public static double getMagnetismOfBlock(Block bl)
	{
		return map.getOrDefault(bl, 0);
	}
}
