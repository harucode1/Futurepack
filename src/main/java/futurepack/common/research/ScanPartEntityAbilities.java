package futurepack.common.research;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.passive.horse.HorseEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class ScanPartEntityAbilities implements IScanPart
{

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		return null;
	}

	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI)
	{
		float h = e.getHealth();
		float mh = (float) e.getAttribute(Attributes.MAX_HEALTH).getValue();
		float speed = (float) e.getAttribute(Attributes.MOVEMENT_SPEED).getValue();
		float attack = 0;
		if(e.getAttribute(Attributes.ATTACK_DAMAGE)!=null)
			attack = (float) e.getAttribute(Attributes.ATTACK_DAMAGE).getValue();
		float jump = 0;
		if(e instanceof HorseEntity)
			jump = (float) ((HorseEntity)e).getCustomJump();
	
		return new StringTextComponent((inGUI ? TextFormatting.DARK_GREEN : TextFormatting.GREEN) + "HP:" + h + "/" + mh + " Speed:" + speed + (attack!=0?" Atk:" + attack:"") + (jump!=0?" Jump:" + jump : ""));
	}

}
