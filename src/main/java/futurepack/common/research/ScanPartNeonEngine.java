package futurepack.common.research;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Callable;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IScanPart;
import futurepack.common.block.modification.TileEntityModificationBase;
import net.minecraft.entity.LivingEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;

public class ScanPartNeonEngine implements IScanPart
{

	@Override
	public Callable<Collection<ITextComponent>> doBlockMulti(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		return () -> Arrays.asList(doBlocks(w, pos, inGUI, res));
		
	}

	private ITextComponent[] doBlocks(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		TileEntity[] t = {null};
		w.getServer().submit(() -> {
			t[0] = w.getBlockEntity(pos);
		}).join();
		TileEntity tile = t[0];
		if(tile!=null)
		{
			ITextComponent[] result = new ITextComponent[] {null,null};
			
			INeonEnergyStorage store = null;
			LazyOptional<INeonEnergyStorage> opt = tile.getCapability(CapabilityNeon.cap_NEON, res.getDirection());
			
			if(opt==null)
			{
				throw new NullPointerException("TileEntity " + tile + " returned null for NEON capability!");
			}
			
			if(!opt.isPresent())
			{
				opt = tile.getCapability(CapabilityNeon.cap_NEON, res.getDirection());
				if(!opt.isPresent())
				{
					return new ITextComponent[0];
				}
			}
			
			store = opt.orElseThrow(NullPointerException::new);
			if(store !=null)
			{
				float i = store.get();
				result[0] = new StringTextComponent( (inGUI ? TextFormatting.DARK_GREEN : TextFormatting.GREEN)  + "Power: " + TextFormatting.RESET + i + " NE");
			}
			
			if(tile instanceof TileEntityModificationBase)
			{
				TileEntityModificationBase mod = (TileEntityModificationBase) tile;
				float ticksPerSecond = mod.getPureRam();
				float energiePerTick = mod.getDefaultPowerUsage();
				boolean working = mod.isWorking();
				float energyPerSecond = ticksPerSecond * energiePerTick;
				String energyType = mod.getEnergyType() == EnumEnergyMode.USE ? "Needs " : (mod.getEnergyType() == EnumEnergyMode.PRODUCE ? "Produces " : "Does something with");
				result[1] = new StringTextComponent( (inGUI ? TextFormatting.DARK_GRAY : TextFormatting.GRAY)  + energyType + energyPerSecond + " NE/s and is " + (working ? "working" : "not working"));
			}
			return result;
		}
		return new ITextComponent[0];
	}
	
	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI)
	{
		return null;
	}

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res) 
	{
		return null;
	}

}
