package futurepack.common.research;

import java.util.Map;
import java.util.TreeMap;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ScanPartMagnetism implements IScanPart
{

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		long time = System.currentTimeMillis();
		double mag = getMagnetIntensity(w, pos, 4 * 16);//max value supported 64
		time = System.currentTimeMillis() - time;
		if(mag>0)
		{
			return new TranslationTextComponent("chat.escanner.magnetism", (float)mag);
		}
		return null;
	}

	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI)
	{
		return null;
	}
	
	public static double getMagnetIntensity(World w, BlockPos pos, int range)
	{
		double max = 0;
		BlockPos.Mutable xyz = new BlockPos.Mutable().set(pos);
		Map<Long, Chunk> m = new TreeMap<Long, Chunk>();
		
		for(int x=pos.getX()-range;x<pos.getX()+range+1;x++)
		{
			for(int z=pos.getZ()-range;z<pos.getZ()+range+1;z++)
			{
				Chunk c = m.computeIfAbsent(ChunkPos.asLong(x>>4, z>>4), l -> w.getChunk(ChunkPos.getX(l), ChunkPos.getZ(l)));
				for(int y=pos.getY()-range;y<pos.getY()+range+1;y++)
				{
					xyz.set(x, y, z);
					BlockState b = c.getBlockState(xyz);
					double i = MagnetismManager.getMagnetismOfBlock(b.getBlock());
					if(i>0)
					{
						double dis = pos.distSqr(x, y, z, true);
						max = Math.max(max, (i*i) / dis);
					}
				}
			}
		}
		return Math.sqrt(max);
	}
}
