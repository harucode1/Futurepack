package futurepack.common.research;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

import futurepack.api.Constants;
import futurepack.api.EnumScanPosition;
import futurepack.api.interfaces.IScanPart;
import futurepack.common.AsyncTaskManager;
import futurepack.common.FuturepackTags;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.living.EntityCrawler;
import futurepack.common.entity.living.EntityGehuf;
import futurepack.common.entity.living.EntityHeuler;
import futurepack.common.entity.living.EntityWolba;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageEScanner;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.item.minecart.AbstractMinecartEntity;
import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.entity.monster.CaveSpiderEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.monster.GhastEntity;
import net.minecraft.entity.monster.MagmaCubeEntity;
import net.minecraft.entity.monster.SlimeEntity;
import net.minecraft.entity.monster.WitchEntity;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.entity.passive.ChickenEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.entity.passive.PigEntity;
import net.minecraft.entity.passive.RabbitEntity;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.entity.passive.horse.DonkeyEntity;
import net.minecraft.entity.passive.horse.HorseEntity;
import net.minecraft.entity.passive.horse.LlamaEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.tags.FluidTags;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.ClickEvent.Action;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkDirection;

public class ScanningManager
{
	public static List<ScanEntry> list = new ArrayList<ScanEntry>();
	
	private static final ArrayList<IScanPart>[] scanList;
	
	static
	{
		scanList = new ArrayList[EnumScanPosition.values().length];
		for(int i=0;i<scanList.length;i++)
		{
			scanList[i] = new ArrayList<IScanPart>();
		}
		
		register(EnumScanPosition.HEADLINE, new ScanPartHeadline());
		register(EnumScanPosition.MAIN, new ScanPartMining());
		register(EnumScanPosition.MAIN, new ScanPartEntityAbilities());
		register(EnumScanPosition.MAIN, new ScanPartMagnetism());
		register(EnumScanPosition.MAIN, new ScanPartNeonEngine());
		register(EnumScanPosition.MAIN, new ScanPartSupport());
		register(EnumScanPosition.MAIN, new ScanPartTech());
		register(EnumScanPosition.FOOTER, new ScanPartNBTTags());
		register(EnumScanPosition.MAIN, new ScanPartAtmoshphere());
	}
	
	static
	{
		//Geologie
		new ScanEntry(Material.STONE,	"hs.geologie");
		new ScanEntry(Material.SAND,	"hs.geologie");
		new ScanEntry(Material.DIRT,	"hs.geologie");
		new ScanEntry(Material.CLAY,	"hs.geologie");
		
		//Chemie
		new ScanEntry(CreeperEntity.class, 	"hs.chemie");
		new ScanEntry(Blocks.TNT, 	       	"hs.chemie");
		new ScanEntry(GhastEntity.class, 	"hs.chemie");
		new ScanEntry(Blocks.BREWING_STAND, "hs.chemie");
		new ScanEntry(Blocks.CAULDRON, 	    "hs.chemie");
		new ScanEntry(WitchEntity.class, 	"hs.chemie");
		new ScanEntry(Blocks.CAULDRON, 	    "hs.chemie");
		
		//Tierhaltung
		new ScanEntry(CowEntity.class,     		"hs.tierhaltung");
		new ScanEntry(SheepEntity.class,   		"hs.tierhaltung");
		new ScanEntry(ChickenEntity.class, 		"hs.tierhaltung");
		new ScanEntry(PigEntity.class,     		"hs.tierhaltung");
		new ScanEntry(EntityGehuf.class,     	"hs.tierhaltung");
		new ScanEntry(HorseEntity.class,     	"hs.tierhaltung");
		new ScanEntry(DonkeyEntity.class,     	"hs.tierhaltung");
		new ScanEntry(RabbitEntity.class,     	"hs.tierhaltung");
		new ScanEntry(LlamaEntity.class,     	"hs.tierhaltung");
		new ScanEntry(Blocks.ACACIA_FENCE, 		"hs.tierhaltung");
		new ScanEntry(Blocks.ACACIA_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.BIRCH_FENCE,		"hs.tierhaltung");
		new ScanEntry(Blocks.BIRCH_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.DARK_OAK_FENCE,	"hs.tierhaltung");
		new ScanEntry(Blocks.DARK_OAK_FENCE_GATE,"hs.tierhaltung");
		new ScanEntry(Blocks.JUNGLE_FENCE,		"hs.tierhaltung");
		new ScanEntry(Blocks.JUNGLE_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.OAK_FENCE,			"hs.tierhaltung");
		new ScanEntry(Blocks.OAK_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.SPRUCE_FENCE,		"hs.tierhaltung");
		new ScanEntry(Blocks.SPRUCE_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.HAY_BLOCK,			"hs.tierhaltung");
		
		//Logistik
		new ScanEntry(Blocks.RAIL, 	      		"hs.logistik");
		new ScanEntry(Blocks.HOPPER, 	   		"hs.logistik");
		new ScanEntry(Blocks.DROPPER, 	   		"hs.logistik");
		new ScanEntry(Blocks.DISPENSER, 	   	"hs.logistik");
		new ScanEntry(Blocks.DETECTOR_RAIL,		"hs.logistik");
		new ScanEntry(Blocks.ACTIVATOR_RAIL,	"hs.logistik");
		new ScanEntry(Blocks.POWERED_RAIL, 	   	"hs.logistik");
		new ScanEntry(PigEntity.class, 	      	"hs.logistik");
		new ScanEntry(HorseEntity.class, 	    "hs.logistik");
		new ScanEntry(AbstractMinecartEntity.class,		"hs.logistik");
		new ScanEntry(FuturepackTags.wardrobe,	"hs.logistik");
		new ScanEntry(LlamaEntity.class,		"hs.logistik");
		new ScanEntry(Blocks.GRASS_PATH, 	   	"hs.logistik");
		
		//Biologie
		new ScanEntry(Material.LEAVES, 	    "hs.biologie");
		new ScanEntry(Material.CACTUS, 	    "hs.biologie");
		new ScanEntry(Material.PLANT, 	    "hs.biologie");
		new ScanEntry(Material.REPLACEABLE_WATER_PLANT, 	"hs.biologie");
		new ScanEntry(Material.WATER_PLANT,	"hs.biologie");
		new ScanEntry(Material.REPLACEABLE_PLANT,	"hs.biologie");
		new ScanEntry(Material.BAMBOO,		"hs.biologie");
		new ScanEntry(MobEntity.class,		"hs.biologie");
		new ScanEntry(Blocks.SLIME_BLOCK, 	"hs.biologie");
		new ScanEntry(Blocks.COAL_ORE, 		"hs.biologie");
		
		//Morphologie
		new ScanEntry(SlimeEntity.class, 			"hs.morphologie");
		new ScanEntry(MagmaCubeEntity.class,		"hs.morphologie");
		new ScanEntry(VillagerEntity.class, 		"hs.morphologie");
		new ScanEntry(ZombieEntity.class, 			"hs.morphologie");
		new ScanEntry(Blocks.SLIME_BLOCK, 			"hs.morphologie");
		new ScanEntry(Blocks.RED_MUSHROOM_BLOCK, 	"hs.morphologie");
		new ScanEntry(Blocks.BROWN_MUSHROOM_BLOCK, 	"hs.morphologie");
		new ScanEntry(Blocks.STICKY_PISTON, 		"hs.morphologie");
		new ScanEntry(Blocks.COAL_ORE, 				"hs.morphologie");
		
		//Metallurgie
		new ScanEntry(Blocks.IRON_ORE, 		"hs.metallurgie");
		new ScanEntry(Blocks.GOLD_ORE, 		"hs.metallurgie");
		new ScanEntry(Material.METAL, 		"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/iron")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/gold")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/copper")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/tin")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/zinc")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/silver")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/lead")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/nickel")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/cobalt")),	"hs.metallurgie");
		
		//Maschinenbau
		new ScanEntry(Blocks.PISTON, 					"hs.maschinenbau");
		new ScanEntry(Blocks.STICKY_PISTON, 			"hs.maschinenbau");
		new ScanEntry(Blocks.DISPENSER, 				"hs.maschinenbau");
		new ScanEntry(Blocks.DROPPER, 					"hs.maschinenbau");
		new ScanEntry(InventoryBlocks.block_breaker,	"hs.maschinenbau");
		new ScanEntry(Blocks.FURNACE,					"hs.maschinenbau");
		new ScanEntry(Blocks.OBSERVER,					"hs.maschinenbau");
		new ScanEntry(InventoryBlocks.techtable,		"hs.maschinenbau");
		new ScanEntry(InventoryBlocks.t0_generator,		"hs.maschinenbau");
		
		//Theromodynamik
		new ScanEntry(Blocks.FURNACE,	 	"hs.thermodynamic");
		new ScanEntry(FluidTags.LAVA, 		"hs.thermodynamic", null);
		new ScanEntry(Blocks.TORCH,  		"hs.thermodynamic");
		new ScanEntry(Blocks.WALL_TORCH,  		"hs.thermodynamic");
		new ScanEntry(Blocks.GLOWSTONE,  	"hs.thermodynamic");
		new ScanEntry(Blocks.JACK_O_LANTERN,  	"hs.thermodynamic");
		
		
		//Energieverbindungen
		new ScanEntry(Blocks.TRIPWIRE,	 		"hs.energieverbindungen");
		new ScanEntry(Blocks.TRIPWIRE_HOOK,	 	"hs.energieverbindungen");
		new ScanEntry(Blocks.REDSTONE_WIRE, 	"hs.energieverbindungen");
		new ScanEntry(Blocks.REDSTONE_BLOCK,	"hs.energieverbindungen");
		new ScanEntry(Blocks.REDSTONE_ORE,  	"hs.energieverbindungen");
		new ScanEntry(Blocks.REDSTONE_TORCH,	"hs.energieverbindungen");
		
		//Neonenergie
		new ScanEntry(FuturepackTags.block_crystals,  	"hs.neonenergie");
		new ScanEntry(FuturepackTags.neon_sand,			"hs.neonenergie");
		new ScanEntry(InventoryBlocks.t0_generator,		"hs.neonenergie");
		new ScanEntry(InventoryBlocks.battery_box_w,	"hs.neonenergie");
		new ScanEntry(InventoryBlocks.battery_box_g,	"hs.neonenergie");
		new ScanEntry(InventoryBlocks.battery_box_b,	"hs.neonenergie");
		new ScanEntry(InventoryBlocks.modul_1_w,		"hs.neonenergie");
		new ScanEntry(InventoryBlocks.modul_1_g,		"hs.neonenergie");
		new ScanEntry(InventoryBlocks.modul_1_s,		"hs.neonenergie");
		
		//Hydraulic
		new ScanEntry(Blocks.PISTON, 		"hs.hydraulic");
		new ScanEntry(Blocks.STICKY_PISTON, "hs.hydraulic");
		
		//Menelaus Surface
		new ScanEntry(TerrainBlocks.cobblestone_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.stone_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.sand_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.sandstone_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.sandstone_chiseled_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.sandstone_smooth_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.dirt_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.gravel_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.crawler_hive, "hs.menelaus.surface");
		
		//Menelaus Mendelberry
		new ScanEntry(PlantBlocks.mendel_berry , "hs.menelaus.mendel");
		
		//Menelaus Raw Earth
		new ScanEntry(TerrainBlocks.dirt_m, "hs.menelaus.metalhaltig");
		new ScanEntry(TerrainBlocks.gravel_m, "hs.menelaus.metalhaltig");
		
		//Menelaus Crystal
		new ScanEntry(TerrainBlocks.crystal_glowtite , "hs.menelaus.crystals");
		new ScanEntry(TerrainBlocks.crystal_retium , "hs.menelaus.crystals");
		new ScanEntry(TerrainBlocks.sand_glowtite , "hs.menelaus.crystals");
		new ScanEntry(TerrainBlocks.sand_retium , "hs.menelaus.crystals");
		
		//Menelaus Mushroom	
		new ScanEntry(TerrainBlocks.log_mushroom , "hs.menelaus.tree");
		new ScanEntry(TerrainBlocks.leaves_mushroom , "hs.menelaus.tree");
		
		//Menelaus Mycel
		new ScanEntry(TerrainBlocks.huge_mycel, "hs.menelaus.mycel");
		new ScanEntry(TerrainBlocks.blueshroom, "hs.menelaus.mycel");
		new ScanEntry(TerrainBlocks.bubbleshroom, "hs.menelaus.mycel");
		new ScanEntry(TerrainBlocks.peakhead, "hs.menelaus.mycel");
		new ScanEntry(TerrainBlocks.hyticus, "hs.menelaus.mycel");
		
		//Menelaus Enity
		new ScanEntry(EntityGehuf.class, "hs.menelaus.entity");
		new ScanEntry(EntityCrawler.class, "hs.menelaus.entity");
		new ScanEntry(EntityWolba.class, "hs.menelaus.entity");
		new ScanEntry(EntityHeuler.class, "hs.menelaus.entity");
		
		//Tyros prismid hs.tyros.prismid
		new ScanEntry(TerrainBlocks.prismide, "hs.tyros.prismid");
		new ScanEntry(TerrainBlocks.prismide_stone, "hs.tyros.prismid");
		
		//Tyros hs.tyros.crystals
		new ScanEntry(TerrainBlocks.crystal_bioterium, "hs.tyros.crystals");
		new ScanEntry(TerrainBlocks.sand_bioterium, "hs.tyros.crystals");
		
		//Tyros hs.tyros.tree
		new ScanEntry(TerrainBlocks.log_tyros , "hs.tyros.tree");
		new ScanEntry(PlantBlocks.leaves_tyros , "hs.tyros.tree");
		//what about the palirie tree?
		
		//Tyros hs.tyros.topinambur
		new ScanEntry(PlantBlocks.topinambur, "hs.tyros.topinambur");
		
		//Tyros hs.tyros.entity
		new ScanEntry(CaveSpiderEntity.class, "hs.tyros.entity");
		
	}
	
	
	
	public static void register(EnumScanPosition pos, IScanPart entry)
	{
		scanList[pos.ordinal()].add(entry);
	}
	
	public static void doBlock(World w, BlockPos pos, PlayerEntity pl, BlockRayTraceResult res)
	{
		if(!w.isClientSide)
		{
			for(ScanEntry se : list)
			{
				se.onBlock(w, pos, pl, res);
			}
			
			ForkJoinPool executor = AsyncTaskManager.getExecutor();
			ForkJoinTask<ITextComponent[]> chats = doBlock(w, pos, true, res, executor);
			Thread t = new Thread(() -> {
				MessageEScanner mes = new MessageEScanner(chats.join());
				FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(mes, ((ServerPlayerEntity) pl).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
			});
			t.setDaemon(true);
			t.start();
		}
	}

	public static ForkJoinTask<ITextComponent[]> doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res, ForkJoinPool executor)
	{		
		ArrayList<ForkJoinTask<Collection<ITextComponent>>> list = new ArrayList<>();
		
		for(int i=0;i<scanList.length;i++)
		{
			for(IScanPart part : scanList[i])
			{
				Callable<Collection<ITextComponent>> asyncTexts = part.doBlockMulti(w, pos, inGUI, res);
				if(asyncTexts!=null)
				{
					list.add(executor.submit(asyncTexts));
				}
				else
				{
					throw new NullPointerException(part + " returned null instead of callable");
				}
			}
		}
		return executor.submit(mergeTexts(list));
	}
	
	private static Callable<ITextComponent[]> mergeTexts(ArrayList<ForkJoinTask<Collection<ITextComponent>>> list)
	{
		return () -> 
		{
			try
			{
				ArrayList<ITextComponent> texts = new ArrayList<>(list.size() * 2);
				for(ForkJoinTask<Collection<ITextComponent>> task : list)
				{
					Collection<ITextComponent> col = task.get();
					if(col!=null)
						texts.addAll(col);
				}
				texts.removeIf(p -> p == null);
				return texts.toArray(new ITextComponent[texts.size()]);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				ArrayList<ITextComponent> texts = new ArrayList<>();
				Throwable t = e;
				do
				{
					texts.add(new StringTextComponent(t.toString()));
					texts.add(new StringTextComponent("    " + t.getStackTrace()[0].toString()));
					if(t.getCause() == t)
						break;
					
					t = t.getCause();
				}
				while(t!=null);
				
				return texts.toArray(new ITextComponent[texts.size()]);
			}
		};
	}
	
	public static void doEntity(World w, LivingEntity e, PlayerEntity pl)
	{
		if(!w.isClientSide)
		{
			for(ScanEntry se : list)
			{
				se.onEntity(w, e, pl);
			}
//			for(ITextComponent ch : doEntity(w, e, false))
//			{
//				pl.sendMessage(ch);
//			}
			ForkJoinPool executor = AsyncTaskManager.getExecutor();
			ForkJoinTask<ITextComponent[]> chats = doEntity(w, e, true, executor);
			Thread t = new Thread(() -> 
			{
				MessageEScanner mes = new MessageEScanner(chats.join());
				FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(mes, ((ServerPlayerEntity) pl).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
			});
			t.setDaemon(true);
			t.start();
		}
	}
	
	public static void openStart(World w, PlayerEntity pl)
	{
		if(!w.isClientSide)
		{
			List<ITextComponent> toDisplay = new ArrayList<ITextComponent>();
			
			Style blue_underlined = Style.EMPTY.withFont(Constants.unicode_font).withColor(TextFormatting.DARK_AQUA).setUnderlined(true);
			
			Research r_get_stared = ResearchManager.getResearch("get_started");
			StringTextComponent msg = new StringTextComponent((TextFormatting.DARK_GRAY) + "1: ");
			TranslationTextComponent tool = new TranslationTextComponent(r_get_stared.getTranslationKey());
			tool.setStyle(blue_underlined);
			Style open_research = Style.EMPTY.withFont(Constants.unicode_font).withClickEvent(new ClickEvent(Action.CHANGE_PAGE, "openresearch="+r_get_stared.getName()));
			msg.setStyle(open_research);
			msg.append(tool);
			toDisplay.add(msg);
			
			Research r_dungeon = ResearchManager.getResearch("story.tec_dungeon");
			msg = new StringTextComponent((TextFormatting.DARK_GRAY) + "2: ");
			tool = new TranslationTextComponent(r_dungeon.getTranslationKey());
			tool.setStyle(blue_underlined);
			open_research = Style.EMPTY.withFont(Constants.unicode_font).withClickEvent(new ClickEvent(Action.CHANGE_PAGE, "openresearch="+r_dungeon.getName()));
			msg.setStyle(open_research);
			msg.append(tool);
			toDisplay.add(msg);
			
			tool = new TranslationTextComponent("futurepack.escanner.open_techtree");
			Style open_techtree = Style.EMPTY.withFont(Constants.unicode_font).withClickEvent(new ClickEvent(Action.CHANGE_PAGE, "opentechtree="+r_dungeon.getName()));
			tool.setStyle(open_techtree);
			toDisplay.add(tool);
			
			MessageEScanner mes = new MessageEScanner(toDisplay.toArray(new ITextComponent[toDisplay.size()]));
			FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(mes, ((ServerPlayerEntity) pl).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
		}
	}
	
	public static ForkJoinTask<ITextComponent[]> doEntity(World w, LivingEntity e, boolean inGUI, ForkJoinPool executor)
	{
		ArrayList<ForkJoinTask<Collection<ITextComponent>>> list = new ArrayList<>();
		for(int i=0;i<scanList.length;i++)
		{
			for(IScanPart part : scanList[i])
			{
				Callable<Collection<ITextComponent>> asyncTexts = part.doEntityMulti(w, e, inGUI);
				if(asyncTexts!=null)
				{
					list.add(executor.submit(asyncTexts));
				}
				else
				{
					throw new NullPointerException(part + " returned null instead of callable");
				}
			}
		}
		return executor.submit(mergeTexts(list));
	}
	
	private static class ScanEntry
	{
		final String reserach;
		
		Class<? extends Entity> entity;
		Block bl;
		Material m;
		ITag<Block> ore;
		ITag<Fluid> fluid;
		
		public ScanEntry(Class<? extends Entity> entity, String r) 
		{
			this(r);
			this.entity = entity;
		}
		
		public ScanEntry(Block b, String r) 
		{
			this(r);
			bl=b;
		}
		
		public ScanEntry(Material m, String r) 
		{
			this(r);
			this.m = m;
		}
		
		public ScanEntry(ITag<Block> o, String r) 
		{
			this(r);
			this.ore = o;
		}
		
		private ScanEntry(String r)
		{
			reserach=r;
			list.add(this);
		}
		
		public ScanEntry(ITag<Fluid> fluidTag, String r, Object nullObj) 
		{
			this(r);
			fluid = fluidTag;
		}

		public boolean onEntity(World w, LivingEntity entity, PlayerEntity user)
		{
			checkUnderground(user);
			
			if(this.entity!=null)
			{
				if(this.entity.isAssignableFrom(entity.getClass()))
				{
					add(user);	
					return true;
				}
			}
			return false;
		}
		
		public boolean onBlock(World w, BlockPos pos, PlayerEntity user, RayTraceResult res)
		{
			checkUnderground(user);
			
			if(bl!=null)
			{
				BlockState state = w.getBlockState(pos);
				Block bl = state.getBlock();
				if(this.bl == bl)
				{
					add(user);	
					return true;
				}
			}
			if(m!=null)
			{
				BlockState state = w.getBlockState(pos);
				Block bl = state.getBlock();
				if(state.getMaterial() == m)
				{
					add(user);	
					return true;
				}
			}
			if(ore!=null)
			{
				BlockState state = w.getBlockState(pos);
				
				if(state.is(ore))
				{
					add(user);	
					return true;
				}
			}
			if(fluid!=null)
			{
				if(w.getFluidState(pos).is(fluid))
				{
					add(user);
					return true;
				}
			}
				
			
			return false;
		}
		
		private static void checkUnderground(PlayerEntity pl)
		{
			if(pl.getY() < 30)
			{
				if(pl.getCommandSenderWorld().dimension().location().equals(Dimensions.MENELAUS_ID)) 
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.menelaus.caves"));
				}
				else if(pl.getCommandSenderWorld().dimension().location().equals(Dimensions.TYROS_ID))
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.caves"));
				}
			}
			else if(pl.getY() > 60)
			{
				if(pl.getCommandSenderWorld().dimension().location().equals(Dimensions.TYROS_ID))
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.surface"));
				}
			}
			
			if(pl.level.getBiome(pl.blockPosition()).getRegistryName().toString().startsWith("futurepack:tyros_rockdesert"))
			{
				CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.deadlands"));
			}
			
		}
		
		private void add(PlayerEntity pl)
		{
			CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch(reserach));
		}
	}
}
