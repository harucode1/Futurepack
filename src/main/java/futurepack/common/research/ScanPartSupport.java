package futurepack.common.research;

import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IScanPart;
import net.minecraft.entity.LivingEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;

public class ScanPartSupport implements IScanPart
{

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		TileEntity[] t = {null};
		w.getServer().submit(() -> {
			t[0] = w.getBlockEntity(pos);
		}).join();
		TileEntity tile = t[0];
		
		if(tile!=null)
		{
			ISupportStorage store = null;
			LazyOptional<ISupportStorage> opt = tile.getCapability(CapabilitySupport.cap_SUPPORT, res.getDirection());
			
			if(opt==null)
			{
				throw new NullPointerException("TileEntity " + tile + " returned null for SUPPORT capability!");
			}
			
			if(!opt.isPresent())
			{
				opt = tile.getCapability(CapabilitySupport.cap_SUPPORT, res.getDirection());
				if(!opt.isPresent())
				{
					return null;
				}
			}
			store = opt.orElseThrow(NullPointerException::new);
			if(store != null)
			{
				float i = store.get();
				return new StringTextComponent( (inGUI ? TextFormatting.GOLD : TextFormatting.YELLOW)  + "Support: " + TextFormatting.RESET + i + " SP");
			}
			
		}
		return null;
	}

	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI)
	{
		return null;
	}

}
