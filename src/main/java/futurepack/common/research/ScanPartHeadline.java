package futurepack.common.research;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ScanPartHeadline implements IScanPart
{

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		BlockState state = w.getBlockState(pos);
		Item blIt = state.getBlock().asItem();
//		TileEntity tile = w.getTileEntity(pos);
		int redstone = w.getBestNeighborSignal(pos);

		ItemStack stack = new ItemStack(blIt,1);
		
		TranslationTextComponent trans = new TranslationTextComponent((inGUI ? TextFormatting.BLUE : TextFormatting.AQUA) + (blIt==Items.AIR?state.getBlock().getDescriptionId() : stack.getHoverName().getContents()));
		StringTextComponent hover = new StringTextComponent(state.toString());
		hover.setStyle(Style.EMPTY.withColor(inGUI ? TextFormatting.DARK_BLUE : TextFormatting.DARK_AQUA).withItalic(true));
		trans.append(hover);
		if(redstone>0)
			trans.append(new StringTextComponent( (inGUI ? TextFormatting.DARK_RED : TextFormatting.RED) + " Redstone-Level:" + redstone));
		return trans;
	}

	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI)
	{
		ResourceLocation name = null;
//TODO test if this is displayed correct		if(EntityList.CLASS_TO_NAME.containsKey(e.getClass()))	
//		{
//			name = EntityList.CLASS_TO_NAME.get(e.getClass()).toString();
//			id = EntityList.getEntityID(e);
//		}
//		else
		{
			EntityType<?> type = e.getType();
			if(type != null)
			{
				name = type.getRegistryName();
			}
		}
		String type = e.getMobType() + "";	
		TranslationTextComponent trans = new TranslationTextComponent((inGUI ? TextFormatting.BLUE : TextFormatting.AQUA) + name.toString());
		return trans;
	}

}
