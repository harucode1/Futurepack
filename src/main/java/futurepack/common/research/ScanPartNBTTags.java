package futurepack.common.research;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public class ScanPartNBTTags implements IScanPart {

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		TileEntity[] t = {null};
		w.getServer().submit(() -> {
			t[0] = w.getBlockEntity(pos);
		}).join();
		TileEntity tile = t[0];
		if(tile != null)
		{
			CompoundNBT nbt = new CompoundNBT();
			tile.save(nbt);
			return NBTtoChat(nbt);
		}
		return null;
	}

	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI)
	{
		CompoundNBT nbt = new CompoundNBT();
		e.saveWithoutId(nbt);
		nbt.remove("Attributes");
		return NBTtoChat(nbt);
	}
	
	private static StringTextComponent NBTtoChat(CompoundNBT nbt)
	{
		StringBuilder build = new StringBuilder();
		char[] c = nbt.toString().toCharArray();
		
		boolean insideString  = false;
		String sub = "";
		for(int i=0;i<c.length;i++)
		{
			build.append(c[i]);
			
			if(c[i] == '"')
			{
				insideString = !insideString;
			}
			else if(!insideString)
			{
				switch (c[i])
				{
				case ',':
					build.append('\n'+sub);
					break;
				case '{':
					sub+=" ";
					build.append('\n'+sub);
					break;
				case '}':
					sub = sub.substring(1);
					build.insert(build.length()-1, "\n"+sub);
					break;
				case '[':
					sub+=" ";
					build.append('\n' +sub);
					break;
				case ']':
					sub = sub.substring(1);
					build.insert(build.length()-1, "\n"+sub);
					break;
				}
			}
		}
			
		return new StringTextComponent(build.toString());
	}

}
