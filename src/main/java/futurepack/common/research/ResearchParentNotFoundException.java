package futurepack.common.research;

public class ResearchParentNotFoundException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6734169821258145735L;

	public ResearchParentNotFoundException(Research parentless, Throwable cause)
	{
		super("Exception while progressing " + parentless, cause);
	}
}
