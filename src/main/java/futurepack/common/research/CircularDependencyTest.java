package futurepack.common.research;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTables;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

public class CircularDependencyTest
{
	public static List<String> checkResearchTreeDeadLocks()
	{
		long start = System.currentTimeMillis();
		List<Research> errors = new ArrayList<>();
		
		CircularDependencyTest test = new CircularDependencyTest();
		
		for(String id : ResearchManager.getAllReseraches())
		{
			Research r = ResearchManager.getResearch(id);
			if(!test.canResearch(r))
			{
				errors.add(r);
			}
		}		
		
		List<String> list = errors.stream().sorted((r1, r2) -> r2.techLevel - r1.techLevel).map(Research::toString).collect(Collectors.toList());
		System.out.println("Took: " + (System.currentTimeMillis() - start) + " ms");
		return list;
	}
	
	private Set<Research> researched;
	private LinkedList<Research> currentlyCheking;
	
	public CircularDependencyTest()
	{
		researched = new TreeSet<Research>((r1,r2) -> r1.id - r2.id);
		currentlyCheking = new LinkedList<>();
	}
	
	public boolean canResearch(Research r)
	{		
		if(researched.contains(r))
			return true;
		else
		{
			if(checkResearchability(r))
			{
				researched.add(r);
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	private boolean checkResearchability(Research r)
	{
		if(currentlyCheking.contains(r))
		{
			System.out.println("[ERROR] CircularDependency detected at: " + r);
			System.out.println("	Tree: " + currentlyCheking.stream().map(Research::getName).map( s -> {return s + "->";}).collect(Collectors.joining()) + r.getName());
			return false;
		}
		
		currentlyCheking.addLast(r);
		boolean b = checkParents(r) && checkIngredients(r);
		currentlyCheking.removeLastOccurrence(r);
		
		return b;
	}
	
	private boolean checkParents(Research r)
	{
		for(Research p :  r.getParents())
		{
			if(!canResearch(p))//all parents must be doable
			{
				return false;
			}
		}
		return true;
	}
	
	private boolean checkIngredients(Research r)
	{
		ItemPredicateBase[] preds = r.getNeeded();
		for(ItemPredicateBase in : preds)
		{
			if(!canCreate(in))//all predictaes must be do able
			{
				return false;
			}
		}
		return true;
	}
	
	private boolean canCreate(ItemPredicateBase pred)
	{
		List<ItemStack> list = pred.collectAcceptedItems(new ArrayList<ItemStack>(16));
		for(ItemStack it : list)//any item is accepted
		{
			ResourceLocation id = it.getItem().getRegistryName();
			if(id.getNamespace().equals("minecraft"))
				return true;
			
			Set<Research> set = ResearchLoader.getReqiredResearch(it);
			if(set == null)
			{
				return true;
			}
			else
			{
				for(Research r : set)
				{
					if(!canResearch(r))
						return false;
				}
				return true;
			}
		}
		
		return false;
	}
	
	
	public static void checkIfAllBLocksHaveLoottables()
	{
		System.out.println("Testing loottables of Blocks!");
		boolean need_fixes = false;
		for(Entry<RegistryKey<Block>, Block> e : ForgeRegistries.BLOCKS.getEntries())
		{
			if(Constants.MOD_ID.equals(e.getKey().location().getNamespace()))
			{
				Block bl = e.getValue();
				ResourceLocation loottable = bl.getLootTable();
				
				if(loottable==LootTables.EMPTY)
					continue;
				
				File f = new File("./../src/main/resources/data/" + loottable.getNamespace() + "/loot_tables/" + loottable.getPath() + ".json");
				
				if(!f.exists())
				{
					System.err.println("Block " + e.getKey() + " has no loottable entry! searched at " + f);
					need_fixes = true;
				}
			}
		}
		System.out.println("Testing loottables of Blocks! - Done");
		if(need_fixes)
		{
			throw new RuntimeException("There are blocks without lottables, add loottables or switch them to LootTabkes.EMPTY");
		}
	}
}
