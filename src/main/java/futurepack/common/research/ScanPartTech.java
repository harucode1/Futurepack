package futurepack.common.research;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.Callable;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.ClickEvent.Action;
import net.minecraft.world.World;

public class ScanPartTech implements IScanPart
{

	@Override
	public Callable<Collection<ITextComponent>> doBlockMulti(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		BlockState bs = w.getBlockState(pos);
		return () -> 
		{
			Set<Research> set = ResearchLoader.getReqiredResearch(new ItemStack(bs.getBlock(), 1));
			if(set != null)
			{
				ArrayList<ITextComponent> list = new ArrayList(set.size());
				
				Style s = Style.EMPTY;
				s.applyFormat(inGUI ? TextFormatting.DARK_AQUA : TextFormatting.GRAY);
				s.setUnderlined(true);
				
				for(Research r : set)
				{
					TranslationTextComponent tool = new TranslationTextComponent(r.getTranslationKey());
					tool.setStyle(s);
					StringTextComponent msg = new StringTextComponent((inGUI ? TextFormatting.DARK_GRAY : TextFormatting.GRAY) + "Research: ");
					Style ss = Style.EMPTY;
					ss.withClickEvent(new ClickEvent(Action.CHANGE_PAGE, "openresearch="+r.getName()));
					msg.setStyle(ss);
					msg.append(tool);
					list.add(msg);
				}
				return list;
			}
			return null;
		};
	}

	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI)
	{
		return null;
	}

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		return null;
	}

}
