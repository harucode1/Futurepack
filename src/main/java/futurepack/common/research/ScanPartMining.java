package futurepack.common.research;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;

public class ScanPartMining implements IScanPart
{

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res) {
		BlockState state = w.getBlockState(pos);
		ToolType h = state.getBlock().getHarvestTool(state);
		int l = state.getBlock().getHarvestLevel(state);
		
		StringTextComponent msg = new StringTextComponent("");
		
		if(h != null)
		{
			TranslationTextComponent tool = new TranslationTextComponent("harvest.tool." + h.getName() + "." + l);
			tool.setStyle((Style.EMPTY).withColor((inGUI ? TextFormatting.DARK_GRAY : TextFormatting.GRAY)));
			msg.append(tool);	
		}		
		
		if(state.getMaterial() == Material.METAL)
		{
			TranslationTextComponent tool = new TranslationTextComponent("material.metall");
			tool.setStyle((Style.EMPTY).withColor((inGUI ? TextFormatting.DARK_GRAY : TextFormatting.GRAY)));
			msg.append("\n");
			msg.append(tool);
		}
		
		return msg;
	}

	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI) {
		return null;
	}

}
