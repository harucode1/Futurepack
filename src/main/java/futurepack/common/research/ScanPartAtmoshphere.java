package futurepack.common.research;

import java.util.Collection;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.IPlanet;
import futurepack.api.interfaces.IScanPart;
import futurepack.common.FPBlockSelector;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.LazyOptional;

public class ScanPartAtmoshphere implements IScanPart
{

	@Override
	public ITextComponent doBlock(World w, BlockPos pos, boolean inGUI, BlockRayTraceResult res)
	{
		IPlanet pl = FPPlanetRegistry.instance.getPlanetSafe(w.dimension());
		
		if(pl.hasBreathableAtmosphere())
		{
			return new TranslationTextComponent("chat.escanner.athmosphere.breathable");
		}
		BlockPos start = pos.relative(res.getDirection());
		FPBlockSelector sel = new FPBlockSelector(w, new SelectAir(pos, 16));
		sel.selectBlocks(start);
		double air = 0;
		double maxAir = 0;
		
		Collection<ParentCoords> list = sel.getAllBlocks();
		for(BlockPos p : list)
		{
			Chunk c = w.getChunkAt(p);
			LazyOptional<IChunkAtmosphere> opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, res.getDirection());
			if(opt.isPresent())
			{
				IChunkAtmosphere atm = opt.orElseThrow(NullPointerException::new);
				air += atm.getAirAt(p.getX() & 15, p.getY() & 255, p.getZ() & 15);
				maxAir += atm.getMaxAir();
				
			}
		}
		
		float filled = (float) (air / maxAir) * 100;
		
		int pec = (int)filled;
		int komma = (int)(filled * 100) - (pec*100);
		
		return new TranslationTextComponent("chat.escanner.athmosphere.filled", pec, komma, time((int) air));
	}

	@Override
	public ITextComponent doEntity(World w, LivingEntity e, boolean inGUI)
	{
		return null;
	}
	
	private String time(int ticks)
	{
		int seconds = (ticks / 20);
		int minuits = (seconds / 60);
		int hour = minuits / 60;
		minuits %= 60;
		seconds %= 60;
		
		return String.format("%sh %smin %ss", hour, minuits, seconds);
	}
	
	private static class SelectAir implements IBlockSelector
	{
		private final int maxDis;
		private final BlockPos start;
		
		public SelectAir(BlockPos start, int maxDis)
		{
			this.start = start;
			this.maxDis = maxDis*maxDis;
		}
		
		@Override
		public boolean isValidBlock(World w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			return w.isEmptyBlock(pos);
		}

		@Override
		public boolean canContinue(World w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			return start.distSqr(pos) <= maxDis;
		}
		
	}

}
