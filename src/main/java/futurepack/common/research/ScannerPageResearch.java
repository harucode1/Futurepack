package futurepack.common.research;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import futurepack.common.FPLog;
import futurepack.common.gui.escanner.ComponentAssembly;
import futurepack.common.gui.escanner.ComponentBuilding;
import futurepack.common.gui.escanner.ComponentCrafting;
import futurepack.common.gui.escanner.ComponentHeading;
import futurepack.common.gui.escanner.ComponentIndustrialNeonFurnace;
import futurepack.common.gui.escanner.ComponentIndustrialfurnace;
import futurepack.common.gui.escanner.ComponentPartpress;
import futurepack.common.gui.escanner.ComponentPicture;
import futurepack.common.gui.escanner.ComponentSmelting;
import futurepack.common.gui.escanner.ComponentText;
import futurepack.common.gui.escanner.ComponentTranslate;
import futurepack.common.gui.escanner.ComponentZentrifuge;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;

//@ TODO: OnlyIn(Dist.CLIENT)
public class ScannerPageResearch
{
//	private  String text;
//	public int maxpages;
//	public String[] pages;
//	
//	public ResearchPage(String t)
//	{
//		text = t;
//	}
//	
//	@Deprecated
//	public static ResearchPage createFromJson(JsonArray ob)
//	{
//		StringBuilder b = new StringBuilder();
//		
//		for(int i=0;i<ob.size();i++)
//		{
//			if(i>0)
//				b.append("\n\n");
//			b.append(ob.get(i).getAsString());		
//		}
//		ResearchPage page = new ResearchPage(b.toString());;
//		return page;
//	}
//	
	private static HashMap<String, Class<? extends IGuiComponent>> StringToClass = new HashMap();
	
	static
	{
		StringToClass.put("furnace", ComponentSmelting.class);
		StringToClass.put("crafting", ComponentCrafting.class);
		StringToClass.put("industrialfurnace", ComponentIndustrialfurnace.class);
		StringToClass.put("image", ComponentPicture.class);
		StringToClass.put("partpress", ComponentPartpress.class);
		StringToClass.put("assembly", ComponentAssembly.class);
		StringToClass.put("industrialneonfurnace", ComponentIndustrialNeonFurnace.class);
		StringToClass.put("translate", ComponentTranslate.class);
		StringToClass.put("zentrifuge", ComponentZentrifuge.class);
		StringToClass.put("building", ComponentBuilding.class);
	}
	
	public static IGuiComponent[] getComponetsFromName(String s)
	{
		IGuiComponent[] base = getComponetsFromJSON(ResearchLoader.instance.getLocalisated(s));
		IGuiComponent[] arr = new IGuiComponent[base.length+1];
		System.arraycopy(base, 0, arr, 1, base.length);
		arr[0] = new ComponentHeading(ResearchManager.instance.getHeading(s));
		return arr;
		
	}
	
	public static IGuiComponent[] getComponetsFromJSON(JsonArray ob)
	{		
		List<IGuiComponent> comps = new ArrayList<IGuiComponent>(ob.size()+1);
		for(int i=0;i<ob.size();i++)
		{
			JsonElement elm = ob.get(i);
			
			if(elm.isJsonPrimitive())
			{
				JsonPrimitive prim = elm.getAsJsonPrimitive();
				if(prim.isString())
				{
					String text = prim.getAsString();
					comps.add(new ComponentText(new StringTextComponent(text)));
				}		
			}
			else if(elm.isJsonObject())
			{
				JsonObject obj = elm.getAsJsonObject();
				
				JsonElement type = obj.get("type");
				if(type!=null)
				{
					Class<?  extends IGuiComponent> c = StringToClass.get(type.getAsString());
					
					if(c!=null)
					{
						try
						{
							Constructor<? extends IGuiComponent> cons = c.getConstructor(JsonObject.class);
							IGuiComponent comp = cons.newInstance(obj);
							comps.add(comp);
						}
						catch (IllegalAccessException e)
						{
							FPLog.logger.error("The registered Class %s has a private contruktor", c);
						}
						catch (NoSuchMethodException e)
						{
							FPLog.logger.error("The registered Class %s has not the cunstruktor C(JsonObject.class), deleting", c);
							StringToClass.remove(type.getAsString());
						}
						catch (SecurityException e)
						{
							FPLog.logger.error("The registered Class %s is not accesible", c);
						}
						catch (InstantiationException e)
						{
							e.printStackTrace();
						}
						catch (IllegalArgumentException e)
						{
							e.printStackTrace();
						}
						catch (InvocationTargetException e)
						{
							FPLog.logger.error("There was an error in the constructor of an gui component");
							FPLog.logger.catching(e.getCause());
						}
					}
				}
				else
				{
					JsonElement link = obj.get("link");
					if(link!=null && link.isJsonPrimitive())
					{
						ResourceLocation res = new ResourceLocation(link.getAsString());
						try
						{
							JsonArray arr = ResearchLoader.loadResource(res, JsonArray.class);
							IGuiComponent[] arrC = getComponetsFromJSON(arr);
							comps.addAll(Arrays.asList(arrC));
						}
						catch (IOException e)
						{
							e.printStackTrace();
							comps.add(new ComponentHeading(e.toString()));
						}
					}
				}
/**
*  {
    "type": "crafting",
    "slots": {
        "1": {#item#},
        "2": {#item#},
        "3": {#item#},
        "4": {#item#},
        "5": {#item#},
        "6": {#item#},
        "7": {#item#},
        "8": {#item#},
        "9": {#item#},
        "out": {#item#}
    }
}


{
    "type": "furnace",
    "slots": {
        "in": {#item#},
        "out": {#item#}
    }
}
*/
			}
			
		}
		
		return comps.toArray(new IGuiComponent[comps.size()]);
	}
	
//	private static ItemStack getStack(JsonObject iob)
//	{
//		Item it = Item.getByNameOrId(iob.get("id").getAsString());
//		int meta = iob.getAsJsonPrimitive("meta").getAsInt();
//		return new ItemStack(it, 1, meta);
//	}
//

//	public void setup(FontRenderer fontRenderer, int w, int h)
//	{
//		int maxlines = h / fontRenderer.FONT_HEIGHT;
//		List<String> l = fontRenderer.listFormattedStringToWidth(text, w);
//		
//		maxpages = l.size() / maxlines;
//		maxpages += Math.min(1, l.size() % maxlines);
//		
//		pages = new String[maxpages];
//		
//		for(int i=0;i<maxpages;i++)
//		{
//			ArrayList<String> sub = new ArrayList(l.subList(i*maxlines, Math.min(l.size(), i*maxlines + maxlines)));
//			pages[i] = "";
//			for(String ss : sub)
//			{
//				pages[i] += ss + "\n";				
//			}
//		}
//	}
	
	
}
