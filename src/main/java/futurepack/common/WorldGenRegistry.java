package futurepack.common;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import futurepack.api.Constants;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.world.dimensions.biomes.FPBiomes;
import futurepack.world.gen.WorldGenOres;
import futurepack.world.gen.feature.CrystalBubbleConfig;
import futurepack.world.gen.feature.DungeonFeatureConfig;
import net.minecraft.item.DyeColor;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.biome.Biome.Category;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.template.RuleTest;
import net.minecraft.world.gen.feature.template.TagMatchRuleTest;
import net.minecraft.world.gen.placement.ChanceConfig;
import net.minecraft.world.gen.placement.IPlacementConfig;
import net.minecraft.world.gen.placement.NoPlacementConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.minecraftforge.common.ForgeConfigSpec.IntValue;
import net.minecraftforge.event.world.BiomeLoadingEvent;

public class WorldGenRegistry 
{

	private static Consumer<BiomeLoadingEvent> test;
	
	@SuppressWarnings("unchecked")
	public static void init() 
	{
		register();
		WorldGenOres.register();
		
		
		ArrayList<Consumer<BiomeLoadingEvent>> list = new ArrayList<Consumer<BiomeLoadingEvent>>();
		list.add(new CrystalCaveInserter());
		list.add(new TecDungeonInserter());
		list.add(new BedrockRiftInserter());
		list.add(new DungeonEntranceInserter());
		list.add(new ErseInserter());
		
//		list.add(b -> {
//			if(b.getRegistryName().getNamespace().equals("minecraft"))
//			{
//				b.addCarver(Carving.AIR, Biome.createCarver(BiomeTyros.LAPUTA_ISLANDS, ICarverConfig.NONE));
//			}
//		});
		
		WorldGenOres.init(list);
		
		test = merge(list.toArray(new Consumer[list.size()]));		
	}
	
	private static <T> Consumer<T> merge(Consumer<T>[] consumers)
	{
		return t -> {
			for(Consumer<T> c : consumers)
			{
				c.accept(t);
			}
		};
	}
	
	private static ConfiguredFeature<?, ?> crystalcave_alutin, crystalcave_neon, crystalcave_retium, crystalcave_glowtite, crystalcave_bioterium;
	private static ConfiguredFeature[] tec_dungeons;
	private static ConfiguredFeature<?,?> bedrock_rift, dungeon_entrace, erse;
	
	public static void register() 
	{
		CrystalBubbleConfig alutin_config = new CrystalBubbleConfig(TerrainBlocks.sand_alutin.defaultBlockState(), DecoBlocks.block_crystal_alutin.defaultBlockState());
		CrystalBubbleConfig neon_config = new CrystalBubbleConfig(TerrainBlocks.sand_neon.defaultBlockState(), DecoBlocks.block_crystal_neon.defaultBlockState());
		CrystalBubbleConfig retium_config = new CrystalBubbleConfig(TerrainBlocks.sand_retium.defaultBlockState(), DecoBlocks.block_crystal_retium.defaultBlockState());
		CrystalBubbleConfig glowtite_config = new CrystalBubbleConfig(TerrainBlocks.sand_glowtite.defaultBlockState(), DecoBlocks.block_crystal_glowtite.defaultBlockState());
		CrystalBubbleConfig bioterium_config = new CrystalBubbleConfig(TerrainBlocks.sand_bioterium.defaultBlockState(), DecoBlocks.block_crystal_bioterium.defaultBlockState());
		
		crystalcave_alutin = genFeatureCrystalCave(alutin_config, FPConfig.WORLDGEN_CAVES.hole_alutin);
		crystalcave_neon = genFeatureCrystalCave(neon_config, FPConfig.WORLDGEN_CAVES.hole_neon);
		crystalcave_retium = genFeatureCrystalCave(retium_config, FPConfig.WORLDGEN_CAVES.hole_retium);
		crystalcave_glowtite = genFeatureCrystalCave(glowtite_config, FPConfig.WORLDGEN_CAVES.hole_glowtite);
		crystalcave_bioterium = genFeatureCrystalCave(bioterium_config, FPConfig.WORLDGEN_CAVES.hole_bioterium);
		
		Registry<ConfiguredFeature<?, ?>> registry = WorldGenRegistries.CONFIGURED_FEATURE;
		
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "crystalcave_alutin"), crystalcave_alutin);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "crystalcave_neon"), crystalcave_neon);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "crystalcave_retium"), crystalcave_retium);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "crystalcave_glowtite"), crystalcave_glowtite);
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "crystalcave_bioterium"), crystalcave_bioterium);
		
		DungeonFeatureConfig[] conf = new DungeonFeatureConfig[DyeColor.values().length];
		tec_dungeons = new ConfiguredFeature[conf.length];
		
		DyeColor[] tec_colors = new DyeColor[] {DyeColor.ORANGE, DyeColor.GREEN, DyeColor.LIGHT_GRAY, DyeColor.LIME, DyeColor.BROWN, DyeColor.RED, DyeColor.BLUE, DyeColor.LIGHT_BLUE, DyeColor.WHITE, DyeColor.MAGENTA};
		for(DyeColor dye : tec_colors)
		{
			ConfiguredFeature<?,?> tec_dungeon = genFeatureTecDungeon(dye, conf);
			WorldGenRegistry.tec_dungeons[dye.ordinal()] = Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "tec_dungeon_" + dye.getName()), tec_dungeon);
		}
		
		bedrock_rift = FPBiomes.BEDROCK_RIFT.configured(IFeatureConfig.NONE).decorated(Placement.NOPE.configured(IPlacementConfig.NONE));
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "configured_bedrock_rift"), bedrock_rift);
			
		
		DungeonFeatureConfig entrance = new DungeonFeatureConfig.Builder().addEntryAuto(1, "special_entrance_upper").build();
		dungeon_entrace = FPBiomes.DUNGEON.configured(entrance).decorated(FPBiomes.DUNGEON_ENTRANE.configured(IPlacementConfig.NONE));
		
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "dungeon_entrace"), dungeon_entrace);
		
		RuleTest IS_GRASS = new TagMatchRuleTest(FuturepackTags.erse_spawn_able);
		OreFeatureConfig ERSE = new OreFeatureConfig(IS_GRASS, PlantBlocks.erse.defaultBlockState(), 10);
		ChanceConfig FIFTHY_PERCENT = new ChanceConfig(2);

		erse = Feature.ORE.configured(ERSE)
		.decorated(Placement.HEIGHTMAP.configured(NoPlacementConfig.INSTANCE))
		.decorated(Placement.SQUARE.configured(NoPlacementConfig.INSTANCE))
		.decorated(Placement.CHANCE.configured(FIFTHY_PERCENT));
		
		Registry.register(registry, new ResourceLocation(Constants.MOD_ID, "configured_erse"), erse);
		
		
	}
	
	
	private static ConfiguredFeature<?, ?> genFeatureCrystalCave(CrystalBubbleConfig conf, IntValue configFile)
	{
		float percent = configFile.get() * 0.01F;
		int chance = (int)(1F / percent);
 	
		return FPBiomes.CRYSTAL_BUBBLE.configured(conf)
					.decorated(Placement.RANGE.configured(new TopSolidRangeConfig(10, 0, 40))
							.decorated(Placement.SQUARE.configured(NoPlacementConfig.INSTANCE)
									.decorated(Placement.CHANCE.configured(new ChanceConfig(chance)))));
	}

	private static ConfiguredFeature<?, ?> genFeatureTecDungeon(DyeColor dye, DungeonFeatureConfig[] conf)
	{
		if(conf[dye.ordinal()] == null)
		{
			try
			{
				DungeonFeatureConfig.Builder build = new DungeonFeatureConfig.Builder().addEntryAuto(4, "new_start_dungeon_"+dye.getName());
				conf[dye.ordinal()] = build.build();
			}
			catch(NullPointerException e)
			{
				throw new IllegalArgumentException("illegal color " + dye, e);
			}
			
		}
		int chance = (int)(1.0 / FPConfig.WORLDGEN.tecdungeon_spawnrate.get());
		return FPBiomes.DUNGEON.configured(conf[dye.ordinal()])
				.decorated(Placement.RANGE.configured(new TopSolidRangeConfig(10, 0, 50))
						.decorated(Placement.SQUARE.configured(NoPlacementConfig.INSTANCE)
								.decorated(Placement.CHANCE.configured(new ChanceConfig(chance)))));
	}
	
	public static class CrystalCaveInserter implements Consumer<BiomeLoadingEvent>
	{
		Predicate<BiomeLoadingEvent> alutin = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_alutin_caves);
		Predicate<BiomeLoadingEvent> neon = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_neon_caves);
		Predicate<BiomeLoadingEvent> retium = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_retium_caves);
		Predicate<BiomeLoadingEvent> glowtite = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_glowtite_caves);
		Predicate<BiomeLoadingEvent> bioterium = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_bioterium_caves);

		
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			if(alutin.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_alutin);
			}
			if(neon.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_neon);
			}
			if(retium.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_retium);
			}
			if(glowtite.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_glowtite);
			}
			if(bioterium.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_bioterium);
			}
		}
		
		
		
		public static Predicate<BiomeLoadingEvent> getRegistryMatcher(ConfigValue<List<? extends String>> config)
		{
			 return getRegistryMatcher(config.get());
		}
		
		public static Predicate<BiomeLoadingEvent> getRegistryMatcher(List<? extends String> list)
		{
			Predicate[] preds = list.stream().map(CrystalCaveInserter::getStringMatcher).map(CrystalCaveInserter::asRegistryMatcher).toArray(Predicate[]::new);
			return (BiomeLoadingEvent e) -> {
				for(Predicate<BiomeLoadingEvent> p : preds)
				{
					if(p.test(e))
						return true;
				}
				return false;
			}; 
		}
		
		public static Predicate<String> getStringMatcher(String regex)
		{
			 return Pattern.compile(regex).asPredicate();
		}
		
		public static Predicate<BiomeLoadingEvent> asRegistryMatcher(Predicate<String> s)
		{
			return (BiomeLoadingEvent e) -> s.test(e.getName().toString());
		}
	}
	
	public static class TecDungeonInserter implements Consumer<BiomeLoadingEvent>
	{
		DungeonFeatureConfig[] conf = new DungeonFeatureConfig[DyeColor.values().length];
		
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			List<? extends String> not_allowed_biomes = FPConfig.WORLDGEN.bl_tec_dungeons.get();
			ResourceLocation rname =  t.getName();
			if(rname==null)
				return; //broken registry
			if(not_allowed_biomes.contains(rname.toString()))
				return;
			
			DyeColor dye = getColor(t);
			if(dye == null)
				return;
			else
			{
				Supplier<ConfiguredFeature<?, ?>> sup = () -> tec_dungeons[dye.ordinal()];
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, sup);
			}
		}
		
		private static DyeColor getColor(BiomeLoadingEvent b)
		{
			if(b.getName()!=null)
			{
				switch(b.getCategory())
				{
				case SAVANNA:
				case DESERT:
				case BEACH:
					return DyeColor.ORANGE;//
				case FOREST:
				case JUNGLE:
					return DyeColor.GREEN;//
				case EXTREME_HILLS:
					return DyeColor.LIGHT_GRAY;//
				case PLAINS:
					return DyeColor.LIME;//
				case SWAMP:
				case MUSHROOM:
				case TAIGA:
					return DyeColor.BROWN;//
				case NETHER:
					return DyeColor.RED;//
				case RIVER:
					return DyeColor.BLUE;//
				case ICY:
					return DyeColor.LIGHT_BLUE;//
				case THEEND:
				case NONE:
					return DyeColor.WHITE;
				case MESA:
					return DyeColor.MAGENTA;
				default:
					return null;
				}
			}
			else
			{
				return null;
			}
		}
	}
	
	public static class BedrockRiftInserter implements Consumer<BiomeLoadingEvent>
	{
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> bedrock_rift);
		}
		
	}
	
	public static class DungeonEntranceInserter implements Consumer<BiomeLoadingEvent>
	{
		
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			if(t.getCategory() == Category.OCEAN)
				return;
			
			addFeature(t, Decoration.SURFACE_STRUCTURES, () -> dungeon_entrace);
		}
		
	}
	
	public static class ErseInserter implements Consumer<BiomeLoadingEvent>
	{
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			if(t.getClimate().downfall > 0.1 && t.getClimate().temperature > 0.4 && t.getCategory() != Category.DESERT)
			{
				addFeature(t, Decoration.TOP_LAYER_MODIFICATION, () -> erse);
			}
		}
	}
	
	public static void addFeature(BiomeLoadingEvent t, GenerationStage.Decoration deco, Supplier<ConfiguredFeature<?, ?>> feature)
	{
		t.getGeneration().getFeatures(deco).add(feature);
	}


	public static void post(BiomeLoadingEvent event) 
	{
		test.accept(event);
	}
}
