package futurepack.common.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.arguments.ILocationArgument;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.server.ServerWorld;

public class InteractivCommandUtil 
{
	private static InteractivCommandUtil currentProcess = null;
	
	public static int doBlockPosSelect(CommandContext<CommandSource> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new StringTextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == BlockPos.class)
		{
			ILocationArgument loc = src.getArgument("pos", ILocationArgument.class);
			currentProcess.result = 	loc.getBlockPos(src.getSource());
			return Command.SINGLE_SUCCESS;
		}
		else
		{
			src.getSource().sendFailure(new StringTextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	public static int doYesQuestion(CommandContext<CommandSource> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new StringTextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == boolean.class || currentProcess.expectedType == Boolean.class)
		{
			currentProcess.result = true;
			return Command.SINGLE_SUCCESS;
		}
		else
		{
			src.getSource().sendFailure(new StringTextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	public static int doNoQuestion(CommandContext<CommandSource> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new StringTextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == boolean.class || currentProcess.expectedType == Boolean.class)
		{
			currentProcess.result = false;
			return Command.SINGLE_SUCCESS;
		}
		else
		{
			src.getSource().sendFailure(new StringTextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	public static int doSkipQuestion(CommandContext<CommandSource> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new StringTextComponent("No structure porting in progress."));
			return 0;
		}
		
		currentProcess.result = new Object();
		return Command.SINGLE_SUCCESS;
	}
	
	public static int doAnswerQuestion(CommandContext<CommandSource> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new StringTextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == String.class)
		{
			currentProcess.result = src.getArgument("text", String.class);
			return Command.SINGLE_SUCCESS;
		}
		else
		{
			src.getSource().sendFailure(new StringTextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	public static int answerQuestionNoArgument(CommandContext<CommandSource> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new StringTextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == String.class)
		{
			for(ItemStack it : src.getSource().getEntity().getHandSlots())
			{
				if(!it.isEmpty())
				{
					currentProcess.result = it.getItem().getRegistryName().toString();
					return Command.SINGLE_SUCCESS;
				}
			}
			src.getSource().sendFailure(new StringTextComponent("Must hold an item in the hands."));
			return 0;
		}
		else
		{
			src.getSource().sendFailure(new StringTextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	
	private final CommandSource interact;
	
	public InteractivCommandUtil(CommandSource interact) 
	{
		super();
		this.interact = interact;
		
		if(currentProcess!=null)
			throw new IllegalStateException("one process is alreay running");
		
		currentProcess = this;
	}

	private Class<?> expectedType;
	private Object result;
	
	private boolean interrupted = false;
	
	@SuppressWarnings("unchecked")
	public <T> T waitForCommand(Class<T> type)
	{
		expectedType = type;
		result = null;
		
		while(result==null)
		{
			try 
			{
				Thread.sleep(50);//wait ms until try again
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
				interrupted = true;
				return null;
			}
		}
		return (T) result;
	}

	public void sendString(String string) 
	{
		interact.sendSuccess(new StringTextComponent(string), false);
	}

	public Vector3d getPos() 
	{
		return interact.getPosition();
	}

	public ServerWorld getWorld()
	{
		return interact.getLevel();
	}

	public void sendErrorMessage(StringTextComponent message)
	{
		interact.sendFailure(message);
	}

	public boolean isInterrupted() 
	{
		return interrupted;
	}

	public static void remove(InteractivCommandUtil util) 
	{
		if(currentProcess==util)
			currentProcess = null;
	}
}
