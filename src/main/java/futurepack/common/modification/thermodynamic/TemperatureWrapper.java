package futurepack.common.modification.thermodynamic;

import futurepack.common.modification.EnumChipType;
import futurepack.common.modification.IModificationPart;


import futurepack.common.modification.IModificationPart.EnumCorePowerType;

public class TemperatureWrapper implements IModificationPart
{
	private final IModificationPart part;
	private final TemperatureControler temp;
	
	public TemperatureWrapper(IModificationPart part, TemperatureControler temp)
	{
		super();
		this.part = part;
		this.temp = temp;
	}

	@Override
	public boolean isRam() 
	{
		return part.isRam();
	}
	
	@Override
	public boolean isCore() 
	{
		return part.isCore();
	}
	
	@Override
	public boolean isChip() 
	{
		return part.isChip();
	}
	
	@Override
	public float getRamSpeed()
	{
		return temp.applyHeat(part.getRamSpeed(), getMaximumTemperature());
	}
	
	@Override
	public float getMaximumTemperature() 
	{
		return part.getMaximumTemperature();
	}
	
	@Override
	public int getCorePower(EnumCorePowerType type) 
	{
		return part.getCorePower(type);
	}
	
	@Override
	public float getChipPower(EnumChipType type) 
	{
		return temp.applyHeat(part.getChipPower(type), getMaximumTemperature());
	}

}
