package futurepack.common.modification;

public interface IPartChip extends IModificationPart
{
	@Override
	default boolean isChip()
	{
		return true;
	}
	
	@Override
	default boolean isRam()
	{
		return false;
	}
	
	@Override
	default boolean isCore()
	{
		return false;
	}
	
	@Override
	default float getRamSpeed()
	{
		return 0;
	}
	
	@Override
	default int getCorePower(EnumCorePowerType type)
	{
		switch (type)
		{
		case NEEDED:
			return getCorePower();
		case BOTH:
			return - getCorePower();
		default:
			return 0;
		}
	}
	
	int getCorePower();
}
