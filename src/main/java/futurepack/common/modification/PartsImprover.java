package futurepack.common.modification;

import java.awt.Color;
import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;

public class PartsImprover 
{
	public static void scaleSum(float[] values, float expectedSum)
	{
		float realSum = 0F;
		for(float f : values)
		{
			realSum += f;
		}
		
		float scale = expectedSum / realSum;
		
		for(int i=0;i<values.length;i++)
		{
			values[i] *= scale;
		}
	}
	
	public static float[] randomValues(Random r, int valueCount, float expectedSum)
	{
		float[] values = new float[valueCount];
		for(int i=0;i<valueCount;i++)
		{
			values[i] = Math.abs((float) r.nextGaussian());
		}
		scaleSum(values, expectedSum);
		
		return values;
	}
	
	public static boolean isPartAnalyzed(ItemStack it)
	{
		return it.getOrCreateTag().contains("analyzed_part");
	}
	
	@Nullable
	public static float[] analyzePart(ItemStack it, Random r)
	{
		if(isPartAnalyzed(it))
		{
			CompoundNBT nbt = it.getTagElement("analyzed_part");
			int size = nbt.getInt("size");
			float[] values = new float[size];
			for(int i=0;i<values.length;i++)
			{
				values[i] = nbt.getFloat("p" + i);
			}
			return values;
		}
		IModificationPart part = PartsManager.getPartFromItem(it);
		if(part!=null)
		{
			EnumPartConfig conf = EnumPartConfig.getPartType(part);
			if(conf!=null)
			{
				float[] values = randomValues(r, conf.getComponentAmount(), conf.getPartValue(part, it));
				
				CompoundNBT nbt =  it.getOrCreateTagElement("analyzed_part");
				nbt.putInt("size", values.length);
				for(int i=0;i<values.length;i++)
				{
					nbt.putFloat("p" + i, values[i]);
				}
				return values;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	public static enum EnumPartConfig
	{
		CHIP(8),
		CORE(36),
		RAM(12);
		
		private final int component_count;
		
		private EnumPartConfig(int components) 
		{
			this.component_count = components;
		}
		
		public float getPartValue(IModificationPart part, ItemStack it) 
		{
			if(this == CORE)
			{
				return it.getOrCreateTag().getInt("core");
			}
			else if(this == RAM)
			{
				return it.getOrCreateTag().getInt("ram");
			}
			else if(this == CHIP)
			{
				EnumChipType[] types = EnumChipType.values();
				for(EnumChipType type : types)
				{
					if(type!=EnumChipType.LOGIC)
					{
						float f = part.getChipPower(type);
						if(f > 0)
						{
							return f;
						}
					}
				}
				
				float f = part.getChipPower(EnumChipType.LOGIC);
				if(f > 0)
				{
					return f;
				}
			}
			return 0;
		}

		public int getComponentAmount()
		{
			return component_count;
		}
		
		public int[] getXYofComponent(int component)
		{
			int x=0,y=0;
			
			if(this == CHIP)
			{
				x=12;y=15;
				x += 6 * (component % 4);
				y += 6 * (component / 4);
			}
			else if(this == CORE)
			{
				x=6;y=6;
				x += 6 * (component % 6);
				y += 6 * (component / 6);		
			}
			else if(this == RAM)
			{
				x=24;y=9;
				
				switch (component)
				{
				case 0:
					break;
				case 1:
				case 2:
				case 3:
					y+=6;
					x+= -6 + (component-1)*6;
					break;
				case 4:
				case 5:
				case 6:
				case 7:
					y+=12;
					x+= -12 + (component-4)*6;
					break;
				case 8:
				case 9:
				case 10:
					y += 3*6;
					x += -12 + (component-8)*6;
					break;
				case 11:
					y += 4*6;
					x -= 6;
				default:
					break;
				}
			}
			
			return new int[] {x,y};
		}
		
		
		
		public static int getColor(float value)
		{
			float hue = 120F / 360F * value;
			return Color.HSBtoRGB(hue, 0.87F, 0.7F);
		}
		
		public static EnumPartConfig getPartType(IModificationPart part)
		{
			return part==null ? null : part.isCore() ? EnumPartConfig.CORE : part.isChip() ? EnumPartConfig.CHIP : part.isRam() ? EnumPartConfig.RAM : null;
		}
	}

	public static boolean recalculateValues(ItemStack it) 
	{
		CompoundNBT nbt = it.getTagElement("analyzed_part");
		if(nbt==null)
			return false;
		
		float[] values = analyzePart(it, null);
		
		float sum = 0;
		for(float f : values)
		{
			sum += f;
		}
		
		IModificationPart part = PartsManager.getPartFromItem(it);
		if(part!=null)
		{
			EnumPartConfig conf = EnumPartConfig.getPartType(part);
			if(conf!=null)
			{
				if(conf == EnumPartConfig.CHIP)
				{
					it.getTag().putFloat("power", sum);
					return true;
				}
				else if(conf == EnumPartConfig.CORE)
				{
					it.getTag().putInt("core", (int)sum);
					return true;
				}
				else if(conf == EnumPartConfig.RAM)
				{
					it.getTag().putInt("ram", (int)sum);
					return true;
				}
			}
		}		
		
		return false;
	}
}
