package futurepack.common.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;


import net.minecraft.item.Item.Properties;

public class ItemMaschineBoard extends Item 
{

	public ItemMaschineBoard(Properties properties) 
	{
		super(properties);
	}

	@Override
	public boolean isFoil(ItemStack stack) 
	{
		return stack.hasTag() && stack.getTag().contains("tile");
	}
}
