package futurepack.common.item;

import net.minecraft.block.DispenserBlock;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IDispenseItemBehavior;
import net.minecraft.dispenser.IPosition;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;


import net.minecraft.item.Item.Properties;

public abstract class ItemDispensable extends Item implements IDispenseItemBehavior
{

	public ItemDispensable(Properties properties) 
	{
		super(properties);
		DispenserBlock.registerBehavior(this, this);
	}

	@Override
	public ItemStack dispense(IBlockSource src, ItemStack it) 
	{
		Direction enumfacing = src.getBlockState().getValue(DispenserBlock.FACING);
		IPosition pos = DispenserBlock.getDispensePosition(src);
		src.getLevel().levelEvent(1000, src.getPos(), 0);//souns
		int f = enumfacing.getStepX() + 1 + (enumfacing.getStepZ() + 1) * 3;
		src.getLevel().levelEvent(2000, src.getPos(), f);//ParticleTypes
		
		return dispense(src, it, pos, enumfacing);
	}
	
	public abstract ItemStack dispense(IBlockSource src, ItemStack it, IPosition pos, Direction enumfacing);

}
