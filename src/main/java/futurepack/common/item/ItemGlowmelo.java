package futurepack.common.item;

import futurepack.api.interfaces.IGranadleLauncherAmmo;
import futurepack.common.block.plants.BlockGlowmelo;
import futurepack.common.block.plants.PlantBlocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;


import net.minecraft.item.Item.Properties;

public class ItemGlowmelo extends BlockItem implements IGranadleLauncherAmmo
{

	public ItemGlowmelo(Properties builder) 
	{
		super(PlantBlocks.glowmelo, builder);
	}

	@Override
	public boolean isGranade(ItemStack it) 
	{
		return this == FoodItems.glowmelo;
	}

	@Override
	public Entity createGrenade(World w, ItemStack it, PlayerEntity pl, float strength)
	{
		Entity et = new FallingBlockEntity(w, pl.getX(), pl.getY()+pl.getEyeHeight(), pl.getZ(), PlantBlocks.glowmelo.defaultBlockState().setValue(BlockGlowmelo.AGE_0_6, 6));
		
		((FallingBlockEntity)et).setHurtsEntities(true);
		((FallingBlockEntity)et).time++;
		
		float f0 = -MathHelper.sin(pl.yRot * 0.017453292F) * MathHelper.cos(pl.xRot * 0.017453292F);
		float f1 = -MathHelper.sin((pl.xRot) * 0.017453292F);
		float f2 = MathHelper.cos(pl.yRot * 0.017453292F) * MathHelper.cos(pl.xRot * 0.017453292F);
		
		float ff = MathHelper.sqrt(f0 * f0 + f1 * f1 + f2 * f2);
		f0 = f0 / ff;
		f1 = f1 / ff;
		f2 = f2 / ff;
		f0 = f0 * strength;
		f1 = f1 * strength;
		f2 = f2 * strength;
		et.setDeltaMovement(f0, f1, f2);
		float fff = MathHelper.sqrt(f0 * f0 + f2 * f2);
		et.yRotO = et.yRot = (float)(MathHelper.atan2(f0, f2) * (180D / Math.PI));
		et.xRotO = et.xRot = (float)(MathHelper.atan2(f1, fff) * (180D / Math.PI));
		
		return et;
	}

}
