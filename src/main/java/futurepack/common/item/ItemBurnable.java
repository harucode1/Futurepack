package futurepack.common.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;


import net.minecraft.item.Item.Properties;

public class ItemBurnable extends Item 
{
	public final int burntime;
	
	public ItemBurnable(Properties properties, int burntime) 
	{
		super(properties);
		this.burntime = burntime;
	}
	
	@Override
	public int getBurnTime(ItemStack itemStack) 
	{
		return burntime;
	}

}
