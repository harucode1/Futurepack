package futurepack.common.item;

import futurepack.api.Constants;
import futurepack.common.block.plants.PlantBlocks;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class FoodItems 
{
	public static final Item.Properties food = new Item.Properties().tab(ItemGroup.TAB_FOOD);
	
	public static final Food mendel_berry_def = new Food.Builder().nutrition(2).saturationMod(0.2F).fast().build();
	public static final Food glowmelo_def = new Food.Builder().nutrition(20).saturationMod(100F).build();
	public static final Food astofood1_def = new Food.Builder().nutrition(20).saturationMod(20F).effect(new EffectInstance(Effects.HEALTH_BOOST, 3600, 4, false, false), 1F).effect(new EffectInstance(Effects.HEAL, 20, 20, false, false), 1F).build();
	public static final Food astofood2_def = new Food.Builder().nutrition(20).saturationMod(20F).effect(new EffectInstance(Effects.FIRE_RESISTANCE, 900, 1, false, false), 1F).effect(new EffectInstance(Effects.DAMAGE_RESISTANCE, 900, 1, false, false), 1F).build();
	public static final Food astofood3_def = new Food.Builder().nutrition(20).saturationMod(20F).effect(new EffectInstance(Effects.DIG_SPEED, 3600, 2, false, false), 1F).effect(new EffectInstance(Effects.MOVEMENT_SPEED, 3600, 2, false, false), 1F).build();
	public static final Food astrofoo4_def = new Food.Builder().nutrition(20).saturationMod(20F).effect(new EffectInstance(Effects.SATURATION, 3600, 1, false, false), 1F).effect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 3600, 3, false, false), 1F).effect(new EffectInstance(Effects.LUCK, 3600, 2, false, false), 1F).build();
	public static final Food ersemarmelade_def = new Food.Builder().nutrition(4).saturationMod(0.4F).build();
	public static final Food ersebrot_def = new Food.Builder().nutrition(10).saturationMod(0.8F).build();
	public static final Food glowshroom_raw_def = new Food.Builder().nutrition(1).saturationMod(0F).effect(new EffectInstance(Effects.POISON, 200, 3, false, true), 1F).build();
	public static final Food glowshroom_stew_def = new Food.Builder().nutrition(10).saturationMod(0.6F).build();
	public static final Food salad_def = new Food.Builder().nutrition(6).saturationMod(0.4F).build();
	public static final Food hufsteak_def = new Food.Builder().nutrition(3).saturationMod(0.3F).meat().build();
	public static final Food grillhufsteak_def = new Food.Builder().nutrition(10).saturationMod(0.9F).meat().build();
	public static final Food topinambur_potato_def = new Food.Builder().nutrition(4).saturationMod(0.6F).build();
	public static final Food palirie_nut_def = new Food.Builder().nutrition(1).saturationMod(0.4F).fast().build();
	
	public static final Item mendel_berry = new Item(food(mendel_berry_def)).setRegistryName(Constants.MOD_ID, "mendel_berry");
	public static final Item astrofood1 = new ItemContainedFood(food(astofood1_def), new ItemStack(CraftingItems.astrofood_empty), 128).setRegistryName(Constants.MOD_ID, "astrofood1");	//Fleisch mit Ketshup
	public static final Item astrofood2 = new ItemContainedFood(food(astofood2_def), new ItemStack(CraftingItems.astrofood_empty), 128).setRegistryName(Constants.MOD_ID, "astrofood2");	//Curryso�e
	public static final Item astrofood3 = new ItemContainedFood(food(astofood3_def), new ItemStack(CraftingItems.astrofood_empty), 128).setRegistryName(Constants.MOD_ID, "astrofood3");	//Gr�nzeug
	public static final Item astrofood4 = new ItemContainedFood(food(astrofoo4_def), new ItemStack(CraftingItems.astrofood_empty), 128).setRegistryName(Constants.MOD_ID, "astrofood4");	//Goo
	public static final Item ersemarmelade = new Item(food(ersemarmelade_def)).setRegistryName(Constants.MOD_ID, "ersemarmelade");
	public static final Item ersebrot = new Item(food(ersebrot_def)).setRegistryName(Constants.MOD_ID, "ersebrot");
	public static final Item glowshroom_raw = new ItemContainedFood(food(glowshroom_raw_def), new ItemStack(Items.BOWL)) .setRegistryName(Constants.MOD_ID, "glowshroom_raw");
	public static final Item glowshroom_stew = new ItemContainedFood(food(glowshroom_stew_def), new ItemStack(Items.BOWL)).setRegistryName(Constants.MOD_ID, "glowshroom_stew");
	public static final Item salad = new ItemContainedFood(food(salad_def), new ItemStack(Items.BOWL)).setRegistryName(Constants.MOD_ID, "salad");
	public static final Item hufsteak = new ItemContainedFood(food(hufsteak_def), new ItemStack(Items.BONE)).setRegistryName(Constants.MOD_ID, "hufsteak");
	public static final Item grillhufsteak = new ItemContainedFood(food(grillhufsteak_def), new ItemStack(Items.BONE)).setRegistryName(Constants.MOD_ID, "grillhufsteak");
	public static final Item topinambur_potato = new BlockItem(PlantBlocks.topinambur, food(topinambur_potato_def)).setRegistryName(Constants.MOD_ID, "topinambur_potato");
	public static final Item erse = new ItemErse(PlantBlocks.erse, food).setRegistryName(Constants.MOD_ID, "erse");
	public static final Item mendel_seed = new ItemMendelSeed(food).setRegistryName(Constants.MOD_ID, "mendel_seed");
	public static final Item oxades_seeds = new BlockItem(PlantBlocks.oxades, food).setRegistryName(Constants.MOD_ID, "oxades");
	public static final Item glowmelo = new ItemGlowmelo(food(glowmelo_def)).setRegistryName(Constants.MOD_ID, "glowmelo");
	public static final Item palirie_nut = new Item(food(palirie_nut_def)).setRegistryName(Constants.MOD_ID, "palirie_nut");

	public static void register(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
				
		r.registerAll(mendel_berry, glowmelo, astrofood1, astrofood2, astrofood3, astrofood4, ersemarmelade, ersebrot, glowshroom_raw, glowshroom_stew, salad, hufsteak, grillhufsteak, topinambur_potato);
		r.registerAll(erse, mendel_seed, oxades_seeds, palirie_nut);
	}
	
	private static Item.Properties food(Food food)
	{
		return new Item.Properties().tab(ItemGroup.TAB_FOOD).food(food);
	}
}
