package futurepack.common.item.misc;

import java.util.List;

import futurepack.common.gui.GuiNotiz;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageResearchResponse;
import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.PacketDistributor;

import net.minecraft.item.Item.Properties;

public class ItemSpawnNote extends Item
{

	public ItemSpawnNote(Properties properties) 
	{
		super(properties);
	}

	@Override
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("info.note.read"));
	}
	
	@Override
    public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
    {	
		if(w.isClientSide)
		{
			DistExecutor.runWhenOn(Dist.CLIENT, () -> {
				return new Runnable()
				{
					@Override
					public void run() 
					{
						Minecraft.getInstance().setScreen(new GuiNotiz(pl.level, pl, pl.getItemInHand(hand)));
					}
				};
			} );
			
		}	
		else
		{
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)pl) , new MessageResearchResponse(w.getServer(), CustomPlayerData.getDataFromPlayer(pl)));
			GuiNotiz.StateSaver.save(pl, hand);
		}
		return ActionResult.success(pl.getItemInHand(hand));
    }
}
