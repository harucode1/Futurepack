package futurepack.common.item.misc;

import java.util.List;

import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageEScanner;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.PacketDistributor;

import net.minecraft.item.Item.Properties;

public class ItemDungeonTablet extends Item 
{

	public ItemDungeonTablet(Properties properties) 
	{
		super(properties);
	}

	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand) 
	{
		ItemStack itemstack = pl.getItemInHand(hand);
		if(itemstack.hasTag())
		{
			String path = itemstack.getTag().getString("research_path");
			if(!path.isEmpty())
			{
				ActionResult.success(itemstack);
				StringTextComponent txt = new StringTextComponent(path);
				txt.setStyle(Style.EMPTY.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new StringTextComponent("load_research=" + path))));
				MessageEScanner msg = new MessageEScanner(true, txt);
				if(pl instanceof ServerPlayerEntity)
				{
					FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)pl), msg);
				}
				return ActionResult.success(itemstack);
			}		
		}
		return super.use(w, pl, hand);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		if(stack.hasTag())
		{
			String path = stack.getTag().getString("research_path");
			if(!path.isEmpty())
			{
				if(path.contains(":"))
				{
					path = path.split(":", 2)[1];
				}
				tooltip.add(new TranslationTextComponent("research." + path));
			}		
		}
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
}
