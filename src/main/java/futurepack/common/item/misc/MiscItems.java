package futurepack.common.item.misc;

import futurepack.api.Constants;
import futurepack.common.FPEntitys;
import futurepack.common.FPSounds;
import futurepack.common.FuturepackMain;
import futurepack.common.item.ItemGreandeBase;
import futurepack.common.item.recycler.ItemAnalyzer;
import futurepack.common.item.recycler.ItemLaserCutter;
import futurepack.common.item.recycler.ItemShredder;
import futurepack.common.item.recycler.ItemTimeManipulator;
import futurepack.common.item.tools.ItemMindControllMiningHelmet;
import net.minecraft.item.DyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.Rarity;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class MiscItems 
{
	public static final Item RESEARCH_BLUEPRINT = new ItemResearchBlueprint(new Item.Properties().stacksTo(16).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "research_blueprint");
	
	public static final Item lack_tank_empty = new Item(new Item.Properties().tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "lack_tank_empty");
	private static final Item.Properties tank = new Item.Properties().craftRemainder(lack_tank_empty).defaultDurability(256).tab(FuturepackMain.tab_tools);
	public static final Item lack_tank_black = new ItemLackTank(tank, DyeColor.BLACK).setRegistryName(Constants.MOD_ID, "lack_tank_black");
	public static final Item lack_tank_blue = new ItemLackTank(tank, DyeColor.BLUE).setRegistryName(Constants.MOD_ID, "lack_tank_blue");
	public static final Item lack_tank_brown = new ItemLackTank(tank, DyeColor.BROWN).setRegistryName(Constants.MOD_ID, "lack_tank_brown");
	public static final Item lack_tank_cyan = new ItemLackTank(tank, DyeColor.CYAN).setRegistryName(Constants.MOD_ID, "lack_tank_cyan");
	public static final Item lack_tank_gray = new ItemLackTank(tank, DyeColor.GRAY).setRegistryName(Constants.MOD_ID, "lack_tank_gray");
	public static final Item lack_tank_green = new ItemLackTank(tank, DyeColor.GREEN).setRegistryName(Constants.MOD_ID, "lack_tank_green");
	public static final Item lack_tank_lightblue = new ItemLackTank(tank, DyeColor.LIGHT_BLUE).setRegistryName(Constants.MOD_ID, "lack_tank_light_blue");
	public static final Item lack_tank_lightgray = new ItemLackTank(tank, DyeColor.LIGHT_GRAY).setRegistryName(Constants.MOD_ID, "lack_tank_light_gray");
	public static final Item lack_tank_lime = new ItemLackTank(tank, DyeColor.LIME).setRegistryName(Constants.MOD_ID, "lack_tank_lime");
	public static final Item lack_tank_magenta = new ItemLackTank(tank, DyeColor.MAGENTA).setRegistryName(Constants.MOD_ID, "lack_tank_magenta");
	public static final Item lack_tank_orange = new ItemLackTank(tank, DyeColor.ORANGE).setRegistryName(Constants.MOD_ID, "lack_tank_orange");
	public static final Item lack_tank_pink = new ItemLackTank(tank, DyeColor.PINK).setRegistryName(Constants.MOD_ID, "lack_tank_pink");
	public static final Item lack_tank_purple = new ItemLackTank(tank, DyeColor.PURPLE).setRegistryName(Constants.MOD_ID, "lack_tank_purple");
	public static final Item lack_tank_red = new ItemLackTank(tank, DyeColor.RED).setRegistryName(Constants.MOD_ID, "lack_tank_red");
	public static final Item lack_tank_white = new ItemLackTank(tank, DyeColor.WHITE).setRegistryName(Constants.MOD_ID, "lack_tank_white");
	public static final Item lack_tank_yellow = new ItemLackTank(tank, DyeColor.YELLOW).setRegistryName(Constants.MOD_ID, "lack_tank_yellow");
	
	private static final Item.Properties notstakable = new Item.Properties().stacksTo(1).tab(FuturepackMain.tab_items);
	public static final Item record_futurepack = new ItemFpRecord(FPSounds.SOUNDTRACK, notstakable).setRegistryName(Constants.MOD_ID, "record_futurepack");
	public static final Item record_menelaus = new ItemFpRecord(FPSounds.MENELAUS, notstakable).setRegistryName(Constants.MOD_ID, "record_fp_menelaus");
	public static final Item record_tyros = new ItemFpRecord(FPSounds.TYROS, notstakable).setRegistryName(Constants.MOD_ID, "record_fp_tyros");
	public static final Item record_entros = new ItemFpRecord(FPSounds.ENTROS, notstakable).setRegistryName(Constants.MOD_ID, "record_fp_entros");
	public static final Item record_unknown = new ItemFpRecord(FPSounds.UNKNOWN, notstakable).setRegistryName(Constants.MOD_ID, "record_fp_unknown");
	public static final Item record_envia = new ItemFpRecord(FPSounds.ENVIA, notstakable).setRegistryName(Constants.MOD_ID, "record_fp_envia");
	public static final Item record_lyrara = new ItemFpRecord(FPSounds.LYRARA, notstakable).setRegistryName(Constants.MOD_ID, "record_fp_lyrara");
	
	
	public static final Item aiFlash0 = new ItemAIFlash(notstakable,128).setRegistryName(Constants.MOD_ID, "aiflash0");
	public static final Item aiFlash1 = new ItemAIFlash(notstakable,256).setRegistryName(Constants.MOD_ID, "aiflash1");
	public static final Item aiFlash2 = new ItemAIFlash(notstakable,512).setRegistryName(Constants.MOD_ID, "aiflash2");
	public static final Item aiFlash3 = new ItemAIFlash(notstakable,1024).setRegistryName(Constants.MOD_ID, "aiflash3");
	public static final Item aiFlash4 = new ItemAIFlash(notstakable,2048).setRegistryName(Constants.MOD_ID, "aiflash4");
	public static final Item aiFlash5 = new ItemAIFlash(notstakable,4096).setRegistryName(Constants.MOD_ID, "aiflash5");
	
	public static final Item NormalBatery = new ItemBatterie(notstakable, 2000).setRegistryName(Constants.MOD_ID, "battery_n");
	public static final Item LargeBatery = new ItemBatterie(notstakable, 4000).setRegistryName(Constants.MOD_ID, "battery_l");
	public static final Item NeonBatery = new ItemBatterie(notstakable, 12000).setRegistryName(Constants.MOD_ID, "battery_neon");
	public static final Item EnergyCell = new ItemBatterie(notstakable, 80000).setRegistryName(Constants.MOD_ID, "energy_cell");
	public static final Item CompactEnergyCell = new ItemBatterie(notstakable, 320000).setRegistryName(Constants.MOD_ID, "compact_energy_cell");
	public static final Item CrystalEnergyCell = new ItemBatterie(notstakable, 1000000).setRegistryName(Constants.MOD_ID, "crystal_energy_cell");	
	
	public static final Item crafting_recipe = new ItemRecipe(notstakable).setRegistryName(Constants.MOD_ID, "crafting_recipe");
	public static final Item assembly_recipe = new ItemRecipe(notstakable).setRegistryName(Constants.MOD_ID, "assembly_recipe");
	
	public static final Item display = new ItemDisplay(new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "display");
	public static final Item lense_red = new ItemLenseRed(notstakable).setRegistryName(Constants.MOD_ID, "lense_red");
	public static final Item lense_green = new ItemLenseGreen(notstakable).setRegistryName(Constants.MOD_ID, "lense_green");
	public static final Item lense_white = new ItemLenseWhite(notstakable).setRegistryName(Constants.MOD_ID, "lense_white");
	public static final Item lense_purple = new ItemLensePurple(notstakable).setRegistryName(Constants.MOD_ID, "lense_purple");
	
	public static final Item kompost = new ItemKompost(new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "kompost");
	public static final Item shredder = new ItemShredder(new Item.Properties().defaultDurability(1024).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "shredder");
	public static final Item analyzer = new ItemAnalyzer(new Item.Properties().defaultDurability(256).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "analyzer");
	public static final Item lasercutter = new ItemLaserCutter(new Item.Properties().defaultDurability(2048).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "lasercutter");
	public static final Item timemanipulator = new ItemTimeManipulator(new Item.Properties().defaultDurability(8192).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "timemanipulator");
	
	public static final Item spawn_note = new ItemSpawnNote(new Item.Properties().rarity(Rarity.UNCOMMON)).setRegistryName(Constants.MOD_ID, "spawn_note");
	public static final Item spacecoordinats = new Item(new Item.Properties().stacksTo(1)).setRegistryName(Constants.MOD_ID, "spacecoordinats");
	
	private static final Item.Properties grenade = new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_items);
	public static final Item grenade_normal = new ItemGreandeBase(grenade, FPEntitys.GRENADE_NORMAL::create).setRegistryName(Constants.MOD_ID, "grenade_normal");
	public static final Item grenade_blaze = new ItemGreandeBase(grenade, FPEntitys.GRENADE_BLAZE::create).setRegistryName(Constants.MOD_ID, "grenade_blaze");
	public static final Item grenade_plasma = new ItemGreandeBase(grenade, FPEntitys.GRENADE_PLASMA::create).setRegistryName(Constants.MOD_ID, "grenade_plasma");
	public static final Item grenade_slime = new ItemGreandeBase(grenade, FPEntitys.GRENADE_SLIME::create).setRegistryName(Constants.MOD_ID, "grenade_slime");
	public static final Item grenade_futter = new ItemGreandeBase(grenade, FPEntitys.GRENADE_FUTTER::create).setRegistryName(Constants.MOD_ID, "grenade_futter");
	public static final Item grenade_saat = new ItemGreandeBase(grenade, FPEntitys.GRENADE_SAAT::create).setRegistryName(Constants.MOD_ID, "grenade_saat");
	public static final Item grenade_kompost = new ItemGreandeBase(grenade, FPEntitys.GRENADE_KOMPOST::create).setRegistryName(Constants.MOD_ID, "grenade_kompost");
	
	public static final Item.Properties rockets = new Item.Properties().stacksTo(1).defaultDurability(16).tab(FuturepackMain.tab_items);
	public static final Item rocket = new ItemRocket(rockets).setRegistryName(Constants.MOD_ID, "rocket");
	public static final Item rocket_plasma = new ItemRocket.ItemPlasmaRocket(rockets).setRegistryName(Constants.MOD_ID, "rocket_plasma");
	public static final Item rocket_blaze = new ItemRocket.ItemBlazeRocket(rockets).setRegistryName(Constants.MOD_ID, "rocket_blaze");
	public static final Item rocket_bioterium = new ItemRocket.ItemBioteriumRocket(rockets).setRegistryName(Constants.MOD_ID, "rocket_bioterium");
	
	public static final Item icon_plasma = new Item(new Item.Properties().stacksTo(1).rarity(Rarity.EPIC)).setRegistryName(Constants.MOD_ID, "icon_plasma");
	public static final Item icon_blaze = new Item(new Item.Properties().stacksTo(1).rarity(Rarity.EPIC)).setRegistryName(Constants.MOD_ID, "icon_blaze");
	public static final Item icon_slime = new Item(new Item.Properties().stacksTo(1).rarity(Rarity.EPIC)).setRegistryName(Constants.MOD_ID, "icon_slime");
	public static final Item telescope = new ItemTelescope(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "telescope");
	public static final Item script_filter = new ItemScriptFilter(new Item.Properties().stacksTo(1).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "script_filter");
	
	public static final Item composite_horse_armor = new ItemHorseArmorComposite().setRegistryName(Constants.MOD_ID, "composite_horse_armor");
	public static final Item dungeon_tablet = new ItemDungeonTablet(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)).setRegistryName(Constants.MOD_ID, "dungeon_tablet");
	public static final Item ancient_energy = new ItemAncientEnergy(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)).setRegistryName(Constants.MOD_ID, "ancient_energy");
	public static final Item hambone = new ItemHambone(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON).tab(FuturepackMain.tab_items)).setRegistryName(Constants.MOD_ID, "hambone");
	
	public static void register(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.register(RESEARCH_BLUEPRINT);
		r.registerAll(lack_tank_black, lack_tank_blue, lack_tank_brown, lack_tank_cyan, lack_tank_gray, lack_tank_green, lack_tank_lightblue, lack_tank_lightgray, lack_tank_lime, lack_tank_magenta, lack_tank_orange, lack_tank_pink, lack_tank_purple, lack_tank_red, lack_tank_white, lack_tank_yellow);
		r.register(lack_tank_empty);
		r.registerAll(record_entros, record_envia, record_futurepack, record_menelaus, record_tyros, record_unknown, record_lyrara);
		r.registerAll(aiFlash0, aiFlash1, aiFlash2, aiFlash3, aiFlash4, aiFlash5);
		r.registerAll(NormalBatery, LargeBatery, NeonBatery, EnergyCell, CompactEnergyCell, CrystalEnergyCell);
		r.registerAll(crafting_recipe, assembly_recipe);
		r.registerAll(display, lense_red, lense_green, lense_white, lense_purple, kompost);
		r.registerAll(shredder, analyzer, lasercutter, timemanipulator);
		r.registerAll(spawn_note, spacecoordinats);
		r.registerAll(grenade_normal, grenade_blaze, grenade_plasma, grenade_slime, grenade_futter, grenade_saat, grenade_kompost);
		r.registerAll(rocket, rocket_plasma, rocket_blaze, rocket_bioterium);
		r.registerAll(icon_plasma, icon_blaze, icon_slime, telescope, script_filter);
		r.registerAll(composite_horse_armor, dungeon_tablet, ancient_energy);
		r.registerAll(hambone);
	}
}
