package futurepack.common.item.misc;

import futurepack.common.FuturepackMain;
import net.minecraft.item.HorseArmorItem;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class ItemHorseArmorComposite extends HorseArmorItem {

	private static ResourceLocation ARMOR_TEXTURE = new ResourceLocation("futurepack:textures/entity/horse_armor_composite.png");
	
	public ItemHorseArmorComposite() {
		super(12, ARMOR_TEXTURE, (new Item.Properties()).stacksTo(1).tab(FuturepackMain.tab_items));
	}
}
