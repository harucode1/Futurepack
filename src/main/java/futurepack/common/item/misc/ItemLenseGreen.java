package futurepack.common.item.misc;

import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.interfaces.IDeepCoreLogic;
import futurepack.common.block.misc.TileEntityBedrockRift;
import futurepack.common.block.multiblock.TileEntityDeepCoreMinerMain;
import futurepack.common.item.ItemLenseBase;
import futurepack.world.scanning.ChunkData;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;


import net.minecraft.item.Item.Properties;

public class ItemLenseGreen extends ItemLenseBase 
{

	public ItemLenseGreen(Properties properties) 
	{
		super(properties, 100);
	}

	@Override
	public boolean isWorking(ItemStack item, IDeepCoreLogic logic) 
	{
		TileEntityBedrockRift rift = (TileEntityBedrockRift) logic.getRift();	
		if(rift==null || !rift.isScanned())
			return false;
		
		return logic.getTileEntity().getCapability(CapabilitySupport.cap_SUPPORT).map(s -> s.get() >= 50).orElse(false);
	}

	@Override
	public boolean updateProgress(ItemStack item, IDeepCoreLogic logic) 
	{
		return updateRegeneration(item, logic);
	}

	@Override
	public int getColor(ItemStack item, IDeepCoreLogic logic) 
	{
		return 0x11f511;
	}

	@Override
	public int getMaxDurability(ItemStack item, IDeepCoreLogic logic) 
	{
		return 10;
	}
	
	@Override
	public boolean isSupportConsumer(ItemStack item, IDeepCoreLogic logic) 
	{
		return true;
	}
	
	@Override
	public IParticleData randomParticle(ItemStack lenseStack, IDeepCoreLogic logic) 
	{
		return logic.getTileEntity().getLevel().random.nextInt(10)==0 ?ParticleTypes.HAPPY_VILLAGER : null;
	}

	public boolean updateRegeneration(ItemStack item, IDeepCoreLogic logic)
	{	
		TileEntityBedrockRift rift = (TileEntityBedrockRift) logic.getRift();
		TileEntityDeepCoreMinerMain main = (TileEntityDeepCoreMinerMain) logic.getTileEntity();
		if(rift==null || !rift.isScanned())
			return false;
		if(main.support.get() < 50)
			return false;
		
		ChunkData data = rift.getData();
		float total = data.getTotalOres();
		float maxTicks = total * 1200F / 300F; // *400F -> pro erz 20 sekunden
		
		CompoundNBT nbt = item.getOrCreateTagElement("restoring");
		int progress = nbt.getInt("progress");
		
		if(++progress > maxTicks && main.support.get() >= 50)
		{
			progress = 0;
			main.support.use(50);
			
			rift.regenerate(0.05D);
			
			nbt.putInt("progress", 0);
			logic.setProgress(0F);
			return true;
		}
		
		nbt.putInt("progress", progress);
		logic.setProgress(progress / maxTicks);
		return false;
	}
}
