package futurepack.common.item.misc;

import java.util.List;

import futurepack.api.interfaces.IItemSupport;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;

public class ItemAIFlash extends Item implements IItemSupport
{
	private final int defaultStorage;
	
	public ItemAIFlash(Item.Properties props, int u)
	{
		super(props.stacksTo(1));
		defaultStorage = u;
	}

	@Override
	public int getMaxSupport(ItemStack it) 
	{
		CompoundNBT nbt = it.getTagElement("support");
		if(nbt==null)
		{
			nbt = new CompoundNBT();
			nbt.putInt("maxSP", defaultStorage);
			it.addTagElement("support", nbt);
			
			return defaultStorage;
		}
		if(!nbt.contains("maxSP"))
		{
			return defaultStorage;
		}
		return nbt.getInt("maxSP");
	}
	
	@Override
	public void appendHoverText(ItemStack it, World w, List<ITextComponent> l, ITooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
	
	@Override
	public void fillItemCategory(ItemGroup tab, NonNullList<ItemStack> subItems)
	{
		if(allowdedIn(tab))
		{
			ItemStack st = new ItemStack(this);
			addSupport(st, getMaxSupport(st));
			subItems.add(st);
		}
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRgb(0.2F, 1.0F, (0.5F + (float)getSupport(stack) / (float)getMaxSupport(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getSupport(stack) / (double)getMaxSupport(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getSupport(stack) < getMaxSupport(stack);
	}
}
