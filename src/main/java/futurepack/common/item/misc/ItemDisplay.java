package futurepack.common.item.misc;

import futurepack.common.entity.EntityMonitor;
import net.minecraft.entity.item.HangingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;


import net.minecraft.item.Item.Properties;

public class ItemDisplay extends Item
{

	public ItemDisplay(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
//		if(true)
//		{
//			if(!context.getWorld().isRemote)
//			{
//				NBTTagCompound nbt = context.getItem().getOrCreateChildTag("pos");
//				
//				if(nbt.contains("xyz"))
//				{
//					StructureToJSON gen = new StructureToJSON(context.getWorld());
//					int[] xyz = nbt.getIntArray("xyz");
//					gen.generate(new BlockPos(xyz[0], xyz[1], xyz[2]), context.getPos());
//					context.getPlayer().sendMessage(new TextComponentString(gen.fileName));
//					nbt.remove("xyz");
//				}
//				else
//				{
//					nbt.putIntArray("xyz", new int[]{context.getPos().getX(), context.getPos().getY(), context.getPos().getZ()});
//				}
//			}
//			return EnumActionResult.SUCCESS;
//		}
		
		if (context.getClickedFace() == Direction.DOWN)
        {
            return ActionResultType.PASS;
        }
        else if (context.getClickedFace() == Direction.UP)
        {
        	return ActionResultType.PASS;
        }
        else
        {
            BlockPos blockpos1 = context.getClickedPos().relative(context.getClickedFace());
            PlayerEntity pl = context.getPlayer();
            World w = context.getLevel();
            ItemStack it = context.getItemInHand();
            
            if (!pl.mayUseItemAt(blockpos1, context.getClickedFace(), it))
            {
            	return ActionResultType.PASS;
            }
            else
            {
                HangingEntity entityhanging = new EntityMonitor(w, blockpos1, context.getClickedFace());

                if (entityhanging != null && entityhanging.survives())
                {
                    if (!w.isClientSide)
                    {
                    	entityhanging.playPlacementSound();
                        w.addFreshEntity(entityhanging);
                    }

                    it.shrink(1);
                }
                return ActionResultType.SUCCESS;
            }
        }
	}
	
}
