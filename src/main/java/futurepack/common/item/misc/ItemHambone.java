package futurepack.common.item.misc;

import java.util.List;

import futurepack.common.entity.living.EntityAlphaJawaul;
import futurepack.common.entity.living.EntityJawaul;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemHambone extends Item
{

	public ItemHambone(Properties properties)
	{
		super(properties);
	}

	@Override
	public ActionResultType interactLivingEntity(ItemStack stack, PlayerEntity playerIn, LivingEntity target, Hand hand)
	{
		if(target instanceof EntityJawaul)
		{
			EntityAlphaJawaul alpha = EntityAlphaJawaul.createFromJawaul((EntityJawaul)target);
			if(alpha==null)
				return ActionResultType.FAIL;
			else
			{
				target.remove();
				alpha.tame(playerIn);
				alpha.spawnAnim();
				
				ItemStack it = new ItemStack(ToolItems.entity_egger_full);
				it.setTag(new CompoundNBT());
				alpha.saveAsPassenger(it.getTag());
				it.setHoverName(alpha.getName());
				ItemEntity item = new ItemEntity(target.getCommandSenderWorld(), target.getX(), target.getY(), target.getZ(), it);
				target.getCommandSenderWorld().addFreshEntity(item);
				return ActionResultType.CONSUME;
			}
		}
		return super.interactLivingEntity(stack, playerIn, target, hand);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add(new TranslationTextComponent(this.getDescriptionId()+".tooltip"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
}
