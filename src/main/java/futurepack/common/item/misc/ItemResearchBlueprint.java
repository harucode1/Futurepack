package futurepack.common.item.misc;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Rarity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemResearchBlueprint extends Item
{
	public ItemResearchBlueprint(Item.Properties props)
	{
		super(props);
	}
	
	public static ItemStack getItemForResearch(Research r)
	{
		ItemStack it = new ItemStack(MiscItems.RESEARCH_BLUEPRINT, 1);
		it.setTag(new CompoundNBT());
		CompoundNBT nbt = it.getTag();
		nbt.putString("research", r.getName());	
		return it;
	}
	
	public static Research getResearchFromItem(ItemStack it)
	{
		CompoundNBT nbt = it.getTag();
		if(nbt!=null && nbt.contains("research"))
		{
			return ResearchManager.getResearch(nbt.getString("research"));
		}
		return null;
	}
	
	@Override
	public void fillItemCategory(ItemGroup tab, NonNullList<ItemStack> subItems)
	{
//		FIXME: blueprints in creative tab
//		if(this.isInGroup(tab) && FPConfig.CLIENT.enableBlueprintsInCreative.get())
//		{
//			for(int i=0;i<ResearchManager.getResearchCount();i++)
//			{
//				Research r = ResearchManager.getById(i);
//				subItems.add(getItemForResearch(r));
//			}
//		}
	}
	
	
	
	@Override
	public void appendHoverText(ItemStack stack, World w, List<ITextComponent> tooltip, ITooltipFlag advanced)
	{
		Research r = getResearchFromItem(stack);
		
		if(r!=null)
		{
			tooltip.add(r.getLocalizedName().plainCopy().setStyle(Style.EMPTY.withColor(TextFormatting.WHITE)));
			tooltip.addAll(getResources(r));
			tooltip.addAll(getPowers(r));
		}
		super.appendHoverText(stack, w, tooltip, advanced);
	}
	
	private static List<ITextComponent> getResources(Research r)
	{
		List<ITextComponent> s = new ArrayList<ITextComponent>();
		if(r.getNeeded()!=null)
		{
			s.add(new TranslationTextComponent("item.futurepack.research_blueprint.needed").setStyle(Style.EMPTY.withColor(TextFormatting.GRAY)));
			for(ItemPredicateBase ip : r.getNeeded())
			{
				if(ip!=null)
				{
					ItemStack it = ip.getRepresentItem();
					if(it!=null)
					{
						s.add(it.getHoverName().plainCopy().setStyle(Style.EMPTY.withColor(TextFormatting.GRAY)));
					}
				}		
			}
		}		
		return s;
	}
	
	private static List<ITextComponent> getPowers(Research r)
	{
		List<ITextComponent> s = new ArrayList<ITextComponent>();
		s.add(new StringTextComponent(new TranslationTextComponent("item.futurepack.research_blueprint.time").getString() + " " + r.getTime()).setStyle(Style.EMPTY.withColor(TextFormatting.WHITE)));
		s.add(new StringTextComponent(new TranslationTextComponent("item.futurepack.research_blueprint.ne").getString() + " " + r.getNeonenergie()).setStyle(Style.EMPTY.withColor(TextFormatting.AQUA)));
		s.add(new StringTextComponent(new TranslationTextComponent("item.futurepack.research_blueprint.support").getString() + " " + r.getSupport()).setStyle(Style.EMPTY.withColor(TextFormatting.YELLOW)));
		s.add(new StringTextComponent(new TranslationTextComponent("item.futurepack.research_blueprint.xp").getString() + " " + r.getExpLvl()).setStyle(Style.EMPTY.withColor(TextFormatting.GREEN)));
		return s;
	}
	
	@Override
	public Rarity getRarity(ItemStack stack)
	{
		Research r = getResearchFromItem(stack);
		if(r!=null)
		{
			switch(r.getTecLevel())
			{
			case 0: return Rarity.COMMON;
			case 1: return Rarity.UNCOMMON;
			case 2: return Rarity.RARE;
			case 3:
			default:return Rarity.EPIC;
			}
		}
		return super.getRarity(stack);
	}
}
