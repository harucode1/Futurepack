package futurepack.common.item.misc;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.interfaces.IPlanet;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.Util;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;


import net.minecraft.item.Item.Properties;

public class ItemFpRecord extends MusicDiscItem
{

	protected ItemFpRecord(SoundEvent soundIn, Properties builder) 
	{
		super(15, soundIn, builder);
	}
	
	   public ActionResult<ItemStack> use(World worldIn, PlayerEntity playerIn, Hand handIn) 
	   {
		   if(!worldIn.isClientSide)
		   {
				CustomPlayerData cd = CustomPlayerData.getDataFromPlayer(playerIn);	
				
				ItemStack itDisc = playerIn.getItemInHand(handIn);
					
				ResourceLocation dim = null;
					
				if(itDisc.getItem() == MiscItems.record_menelaus && cd.hasResearch("story.nav.menelaus"))
				{
					playerIn.sendMessage(new TranslationTextComponent("research.planet.discover", "Menelaus"), Util.NIL_UUID);				
					dim = Dimensions.MENELAUS_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_envia && cd.hasResearch("story.nav.envia"))
				{
					playerIn.sendMessage(new TranslationTextComponent("research.planet.discover", "Envia"), Util.NIL_UUID);				
					dim = Dimensions.ENVIA_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_tyros && cd.hasResearch("story.nav.tyros"))
				{
					playerIn.sendMessage(new TranslationTextComponent("research.planet.discover", "Tyros"), Util.NIL_UUID);				
					dim = Dimensions.TYROS_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_lyrara && cd.hasResearch("story.nav.lyrara"))
				{
					playerIn.sendMessage(new TranslationTextComponent("research.planet.discover", "Lyrara"), Util.NIL_UUID);				
					dim = Dimensions.LYRARA_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_entros && cd.hasResearch("story.nav.entros"))
				{
					playerIn.sendMessage(new TranslationTextComponent("research.planet.discover", "Entros"), Util.NIL_UUID);				
					dim = Dimensions.ENTROS_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_unknown && cd.hasResearch("story.nav.asteroid"))
				{
					playerIn.sendMessage(new TranslationTextComponent("research.planet.discover", "Asteroid"), Util.NIL_UUID);				
					dim = Dimensions.ASTEROID_BELT_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_futurepack && cd.hasResearch("story.nav.entros"))
				{
					playerIn.sendMessage(new TranslationTextComponent("research.planet.discover", "Mincra"), Util.NIL_UUID);
					
					ItemStack itCoord = FPPlanetRegistry.instance.getItemFromPlanet(FPPlanetRegistry.instance.MINECRAFT.get());
					ItemEntity ie = new ItemEntity(worldIn, playerIn.getX(), playerIn.getY(), playerIn.getZ(), itCoord);
					worldIn.addFreshEntity(ie);
					return ActionResult.success(itDisc);
				}
				
				if(dim != null)
				{	
					ArrayList<ResourceLocation> disabled = new ArrayList<ResourceLocation>();
					
					disabled.add(Dimensions.ENVIA_ID);
					disabled.add(Dimensions.LYRARA_ID);
					disabled.add(Dimensions.ENTROS_ID);
//					disabled.add(Dimensions.ASTEROID_BELT_ID);
					
					IPlanet pl = FPPlanetRegistry.instance.getPlanetByDimension(dim);
					if(pl == null || disabled.contains(dim))
					{
						playerIn.sendMessage(new TranslationTextComponent("research.record.sorry"), Util.NIL_UUID);
					}
					else
					{
						ItemStack itCoord = FPPlanetRegistry.instance.getItemFromPlanet(pl);
						ItemEntity ie = new ItemEntity(worldIn, playerIn.getX(), playerIn.getY(), playerIn.getZ(), itCoord);
						worldIn.addFreshEntity(ie);
					}
				}
				else
				{
					playerIn.sendMessage(new TranslationTextComponent("research.record.unresearched"), Util.NIL_UUID);		
				}
				
				
				playerIn.getCooldowns().addCooldown(this, 200);
				
				return ActionResult.success(itDisc);
		   }
		   
		   return super.use(worldIn, playerIn, handIn);			
	   }
	   
		@Override
		public void appendHoverText(ItemStack stack, World w, List<ITextComponent> tooltip, ITooltipFlag advanced)
		{
			super.appendHoverText(stack, w, tooltip, advanced);
			tooltip.add(new TranslationTextComponent("research.record.tooltip").setStyle(Style.EMPTY.withColor(TextFormatting.DARK_RED)));		
		}

}
