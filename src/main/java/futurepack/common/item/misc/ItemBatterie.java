package futurepack.common.item.misc;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;

public class ItemBatterie extends Item implements IItemNeon
{
	private final int defaultMaxNeon;
	
	public ItemBatterie(Item.Properties props, int in) 
	{
		super(props.setNoRepair().stacksTo(1));
		defaultMaxNeon = in;
	}
	
	public static ItemStack getDefaultStack(Item item)
	{
		ItemStack st = new ItemStack(item);
		
		CompoundNBT nbt = new CompoundNBT();
		nbt.putInt("maxNE", ((ItemBatterie)item).defaultMaxNeon);
		nbt.putInt("ne", 0);
		
		st.addTagElement("neon", nbt);

		return st;
	}
	
	public int getDefaultNeon()
	{
		return defaultMaxNeon;
	}
	
	@Override
	public void appendHoverText(ItemStack it, World w, List<ITextComponent> l, ITooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
	
	@Override
	public void fillItemCategory(ItemGroup tab, NonNullList<ItemStack> subItems)
	{
		if(allowdedIn(tab))
		{
			ItemStack st = new ItemStack(this);
			addNeon(st, getMaxNeon(st));
			subItems.add(st);
		}
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}

	@Override
	public int getMaxNeon(ItemStack it)
	{
		CompoundNBT nbt = it.getTagElement("neon");
		if(nbt==null)
		{
			nbt = new CompoundNBT();
			nbt.putInt("maxNE", defaultMaxNeon);
			it.addTagElement("neon", nbt);
			
			return defaultMaxNeon;
		}
		else if(nbt.contains("maxNE"))
		{
			return nbt.getInt("maxNE");
		}
		else
		{
			return defaultMaxNeon;
		}
		
	}
}
