package futurepack.common.item.misc;

import futurepack.api.interfaces.IPlanet;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemTelescope extends Item
{
	public ItemTelescope(Item.Properties props)
	{
		super(props);
	}
	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
	{
		if(!w.isClientSide && HelperResearch.isUseable(pl, this))
		{
			if(pl.level.dimension().location().equals(Dimensions.MENELAUS_ID))
			{
				pl.sendMessage(new TranslationTextComponent("research.planet.discover", "Tyros"), Util.NIL_UUID);
				IPlanet tyr = FPPlanetRegistry.instance.getPlanetByDimension(Dimensions.TYROS_ID);
				return ActionResult.success(FPPlanetRegistry.instance.getItemFromPlanet(tyr));
			}
			else
			{
				pl.sendMessage(new TranslationTextComponent("research.planet.discover", "Menelaus"), Util.NIL_UUID);
				IPlanet men = FPPlanetRegistry.instance.getPlanetByDimension(Dimensions.MENELAUS_ID);
				return ActionResult.success(FPPlanetRegistry.instance.getItemFromPlanet(men));
			}
		}
		return super.use(w, pl, hand);
	}
}
