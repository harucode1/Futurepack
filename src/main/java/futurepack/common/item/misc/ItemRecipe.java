package futurepack.common.item.misc;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemRecipe extends Item 
{
//	private IIcon empty;
	
	public ItemRecipe(Item.Properties props) 
	{
		super(props);
		
	}
	
//	@Override
//	public void registerIcons(IIconRegister r) 
//	{
//		empty = r.registerIcon(getIconString() + "_empty");
////		System.out.println(r + " " + r.getClass());
////		System.out.println(empty + " " + empty.getClass());
//		super.registerIcons(r);
//	}
//	
//	@Override
//	public IIcon getIconIndex(ItemStack it) 
//	{
//		if(it.hasTagCompound() && it.getTag().hasKey("recipe"))
//		{
//			return super.getIconIndex(it);
//		}
//		return empty;
//	}
	
	@Override
	public void appendHoverText(ItemStack it, World w, List<ITextComponent> l, ITooltipFlag par4) 
	{
		if(it.hasTag() && it.getTag().contains("recipe"))
		{
			CompoundNBT nbt = it.getTagElement("recipe");
			l.add(new TranslationTextComponent(nbt.getString("output")));
//			l.add("ignore Metadata: " + nbt.getBoolean("ignoremetas"));
		}
		if(it.getTagElement("assemblyRecipe")!=null)
		{
			CompoundNBT nbt = it.getTagElement("assemblyRecipe");
			l.add(new TranslationTextComponent(nbt.getString("output")));
//			l.add("ignore Metadata: " + nbt.getBoolean("ignoremetas"));
		}
		super.appendHoverText(it, w, l, par4);
	}
}
