package futurepack.common.item.misc;

import futurepack.api.interfaces.IDeepCoreLogic;
import futurepack.common.item.ItemLenseBase;
import net.minecraft.item.ItemStack;


import net.minecraft.item.Item.Properties;

public class ItemLensePurple extends ItemLenseBase 
{

	public ItemLensePurple(Properties properties) 
	{
		super(properties, Short.MAX_VALUE);
	}

	@Override
	public boolean isWorking(ItemStack item, IDeepCoreLogic logic) 
	{
		return false;
	}

	@Override
	public boolean updateProgress(ItemStack item, IDeepCoreLogic logic) 
	{
		return false;
	}

	@Override
	public int getColor(ItemStack item, IDeepCoreLogic logic) 
	{
		return 0x9600dd;
	}

	@Override
	public int getMaxDurability(ItemStack item, IDeepCoreLogic logic) 
	{
		return 10;
	}

}
