package futurepack.common.item.misc;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.GrassBlock;
import net.minecraft.block.IGrowable;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BoneMealItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemKompost extends Item
{

	public ItemKompost(Item.Properties props)
	{
		super(props);
	}

	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		if(applyCompost(context.getItemInHand(), context.getLevel(), context.getClickedPos(), context.getPlayer()))
		{
			return ActionResultType.SUCCESS;
		}
		return super.useOn(context);
	}
	
	public static boolean applyCompost(ItemStack it, World w, BlockPos pos, PlayerEntity pl)
	{
		BlockState IBlockState = w.getBlockState(pos);
		
		//on dirt/grass			
		Block b = w.getBlockState(pos).getBlock();
		BlockState b1 = w.getBlockState(pos.offset(0, 1, 0));
		if((b instanceof GrassBlock || b ==  Blocks.DIRT) && (w.isEmptyBlock(pos.offset(0, 1, 0)) || !b1.isSolidRender(w, pos.offset(0, 1, 0))))
		{
			w.playLocalSound(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, SoundEvents.GRASS_STEP, SoundCategory.NEUTRAL, 10.0F, 1.0F, true);
			
			if(w.isClientSide)
			{
				spawnCompostParticleTypes(w, w.random, pos.offset(0, 1, 0), 24, 1.0F, 0.1F);
			}
			
			w.setBlockAndUpdate(pos, Blocks.PODZOL.defaultBlockState());
			if(w.random.nextInt(4) == 0 && b1.getMaterial() == Material.AIR)
				w.setBlockAndUpdate(pos.offset(0, 1, 0), Blocks.TALL_GRASS.defaultBlockState());
			
	        if (!pl.isCreative())
	        {
	        	it.shrink(1);
	        }
	        
	        return true;
		}
		
		//auf Pflanzen
		if (IBlockState.getBlock() instanceof IGrowable)
		{
			if(((IGrowable)IBlockState.getBlock()).isValidBonemealTarget(w, pos, IBlockState, w.isClientSide))
			{
				
				w.playLocalSound(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, SoundEvents.GRASS_STEP, SoundCategory.NEUTRAL, 10.0F, 1.0F, true);

				if(w.isClientSide)
					spawnCompostParticleTypes(w, w.random, pos, 16, 0.8F, 0.1F);
				
				if(w.random.nextInt(2) == 0)
				{
					
	                if (BoneMealItem.applyBonemeal(it, w, pos, pl))
	                {
	                    if (!w.isClientSide)
	                    {
	                    	w.levelEvent(2005, pos, 0);
	                    } 
	                }
				}
                else if(!pl.isCreative())
    	        {
                	it.shrink(1);
    	        }
				
				return true;
			}	
		}
		
		return false;
	}
		
	private static void spawnCompostParticleTypes(World w, Random rand, BlockPos pos, int amount, float spread, float speed) 
	{
		
		for (int i = 0; i < amount; ++i) 
		{	
        	float x = rand.nextFloat() * spread - spread / 2.0F;
        	float z = rand.nextFloat() * spread - spread / 2.0F;
        	
        	if(rand.nextBoolean())
        	{
	            w.addParticle(
	            	new BlockParticleData(ParticleTypes.BLOCK, Blocks.OAK_LEAVES.defaultBlockState()), 
	            	pos.getX() + 0.5F + x,
	            	pos.getY() + rand.nextFloat() * 0.4F + 0.8F , 
	            	pos.getZ() + 0.5F + z, 
	            	rand.nextFloat() * speed * (x < 0.0F ? -1.0F : 1.0F), 
	            	rand.nextFloat() * -0.3, 
	            	rand.nextFloat() * speed * (z < 0.0F ? -1.0F : 1.0F)
	            );
        	}
        	else
        	{
        		w.addParticle(
        			new BlockParticleData(ParticleTypes.BLOCK, Blocks.SPRUCE_LOG.defaultBlockState()), 
	                pos.getX() + 0.5F + x,
	                pos.getY() + rand.nextFloat() * 0.4F + 0.8F , 
	                pos.getZ() + 0.5F + z, 
	                rand.nextFloat() * speed * (x < 0.0F ? -1.0F : 1.0F), 
	                rand.nextFloat() * -0.3, 
	                rand.nextFloat() * speed * (z < 0.0F ? -1.0F : 1.0F)
	            );
        	}
        }
	}
}
