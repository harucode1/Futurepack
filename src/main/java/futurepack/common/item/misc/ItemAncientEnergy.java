package futurepack.common.item.misc;

import java.util.List;
import java.util.WeakHashMap;

import javax.annotation.Nullable;

import futurepack.api.interfaces.IItemNeon;
import futurepack.api.Constants;
import futurepack.api.FacingUtil;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.INeonEnergyStorage;
import net.minecraft.advancements.Advancement;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.RedstoneParticleData;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.LazyOptional;

import net.minecraft.item.Item.Properties;

public class ItemAncientEnergy extends Item implements IItemNeon
{
	public ItemAncientEnergy(Properties props) 
	{
		super(props);
	}

	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) 
	{
		if(isSelected)
		{
			if(absorbEnergy(stack, worldIn, entityIn.blockPosition(), 5))
			{
				if(!worldIn.isClientSide && entityIn instanceof ServerPlayerEntity)
				{
					Advancement adv = worldIn.getServer().getAdvancements().getAdvancement(new ResourceLocation(Constants.MOD_ID, "energy_sponge"));
					((ServerPlayerEntity)entityIn).getAdvancements().award(adv, "hold_near_energy");
				}
			}
		}
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
	}
	
	private WeakHashMap<ItemStack, BlockPos> lastBlock = new WeakHashMap<ItemStack, BlockPos>(2);
	
	private static RedstoneParticleData neon_particle = new RedstoneParticleData(0F, 1F, 1F, 0.6F);//cyan
	
	public boolean absorbEnergy(ItemStack stack, World w, BlockPos pos, int range)
	{
		if(!w.isClientSide)
		{
			BlockPos src = lastBlock.compute(stack, (k,v) -> 
			{
				if(v!=null && pos.distSqr(v) < range*range)
					return v;
				else
				{
					int dx = w.random.nextInt(5) - w.random.nextInt(5);
					int dz = w.random.nextInt(5) - w.random.nextInt(5);
					for(int dy=5;dy>-5;dy--)
					{
						BlockPos xyz = pos.offset(dx,dy,dz);
						TileEntity t = w.getBlockEntity(xyz);
						if(t!=null)
						{
							LazyOptional<INeonEnergyStorage> lo = getCapability(t);
							if(lo!=null)
								return xyz;
						}
					}
					return null;
				}
				
			});
			if(src != null)
			{
				LazyOptional<INeonEnergyStorage> opt = getCapability(w.getBlockEntity(src));
				if(opt != null && opt.map(s -> addEnergy(stack, s)).orElse(false))
				{
					ServerWorld server = (ServerWorld) w;
					
					float sx = src.getX() - pos.getX();
					float sy = src.getY() - pos.getY();
					float sz = src.getZ() - pos.getZ();
					
					sx *= 0.3F;
					sy *= 0.3F;
					sz *= 0.3F;		
					
					server.sendParticles(neon_particle, src.getX()+0.5, src.getY()+1.5, src.getZ()+0.5, 5, sx, sy, sz, 0.5F);
					return true;
				}
				else
				{
					lastBlock.remove(stack);
				}
			}
		}
		return false;
	}
	
	@Nullable
	private LazyOptional<INeonEnergyStorage> getCapability(TileEntity tile)
	{
		if(tile==null)
			return null;
		
		for(Direction d : FacingUtil.VALUES)
		{
			LazyOptional<INeonEnergyStorage> opt = tile.getCapability(CapabilityNeon.cap_NEON, d);
			if(opt.isPresent())
				return opt;
		}
		return null;
	}
	
	private boolean addEnergy(ItemStack stack, INeonEnergyStorage storage)
	{
		if(isNeonable(stack))
		{
			int i = stack.getOrCreateTag().getInt("stored");
			int d = storage.use(100);
			i += d;
			stack.getTag().putInt("stored", 0);
			
			addNeon(stack, i);

			return d > 0;
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		tooltip.add(new TranslationTextComponent(this.getDescriptionId() + ".tooltip"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}

	public static float getFillState(ItemStack stack) 
	{
		int i = ((IItemNeon)MiscItems.ancient_energy).getNeon(stack);
		return i / 1e6F;
	}

	@Override
	public int getMaxNeon(ItemStack it)
	{
		return 100000000; //100M NE
	}
}
