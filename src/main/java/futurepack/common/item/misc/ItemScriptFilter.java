package futurepack.common.item.misc;

import java.util.List;

import futurepack.common.sync.FPGuiHandler;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public class ItemScriptFilter extends Item
{
	public ItemScriptFilter(Item.Properties props) 
	{
		super(props);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		CompoundNBT nbt = stack.getTagElement("script");
		if(nbt != null)
		{
			String s = nbt.getString("script_name");
			tooltip.add(new StringTextComponent(s));
		}
		tooltip.add(new StringTextComponent("Use/RightClick to edit code"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		
	}
	
	@Override
	public ActionResult<ItemStack> use(World worldIn, PlayerEntity pl, Hand hand) 
	{
		ItemStack it = pl.getItemInHand(hand);
		if(!it.isEmpty())
		{
			if(!worldIn.isClientSide)
			{
				FPGuiHandler.JS_FILTER_TEST.openGui((ServerPlayerEntity)pl, (Object[])null);
			}
			return ActionResult.success(it);
		}
		
		return super.use(worldIn, pl, hand);
	}
}
