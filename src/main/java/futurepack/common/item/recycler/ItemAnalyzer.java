package futurepack.common.item.recycler;

import java.util.ArrayList;
import java.util.Random;

import futurepack.api.EnumLogisticIO;
import futurepack.api.interfaces.IRecyclerTool;
import futurepack.common.block.modification.machines.TileEntityRecycler;
import futurepack.common.modification.EnumChipType;
import futurepack.common.modification.IModificationPart;
import futurepack.common.modification.IModificationPart.EnumCorePowerType;
import futurepack.common.modification.PartsManager;
import futurepack.common.recipes.recycler.IRecyclerRecipe;
import futurepack.common.recipes.recycler.RecyclerAnalyzerRecipe;
import futurepack.depend.api.ItemNBTPredicate;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemAnalyzer extends Item implements IRecyclerTool 
{

	public ItemAnalyzer(Item.Properties props)
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
//		setMaxDamage(256);
//		setMaxStackSize(1);
	}
/*
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return true;
	}*/
	
//	private ItemStack[] getAssemblyInput(ItemStack output)
//	{
//		for(AssemblyRecipe ar : FPAssemblyManager.instance.recipes)
//		{
//			if(output.isItemEqual(ar.getOutput()))
//			{
//				return ar.getInput();
//			}
//		}
//		return null;	
//	}

	
	@Override
	public EnumLogisticIO getSupportMode() 
	{
		return EnumLogisticIO.OUT;
	}

	@Override
	public boolean craftComplete(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
		int damage = tool.getDamageValue();
		
		if(damage + 1 >= tool.getMaxDamage())
		{
			tool.shrink(1);
		}
		else
		{
			tool.hurt(1, tile.getLevel().random, null);
		}
		
		tile.getSupport().add(((RecyclerAnalyzerRecipe)recipe).getSupport());
		return true;
	}

	@Override
	public ArrayList<ItemStack> getOutput(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in, Random random) 
	{
		ArrayList<ItemStack> res = new ArrayList<ItemStack>();
		return res;
	}

	@Override
	public IRecyclerRecipe updateRecipe(TileEntityRecycler tile, ItemStack tool, ItemStack in) 
	{
		in = TileEntityRecycler.getUntoastedItemStack(in).copy();
		in.setCount(1);
				
		float value = 0;
								
		IModificationPart part = PartsManager.getPartFromItem(in);
		
		if(part == null)
		{
			return null;
		}
		
		if(part.isCore())
		{
			value += part.getCorePower(EnumCorePowerType.PROVIDED) * 2F;
		}
		else if(part.isRam())
		{
			value += part.getRamSpeed() * 2F;
		}
		else if(part.isChip())
		{
			for(EnumChipType chip : EnumChipType.values())
			{
				float v = part.getChipPower(chip) - 1F;
				if(v >= 0)
					value += v * 5.0f + 1.0f;
			}
		}

		if(value >= 1.0f)
		{
			return new RecyclerAnalyzerRecipe(new ItemNBTPredicate(in), Math.max((int)value, 1));
		}
		else
		{
			return null;
		}
	}

	@Override
	public void tick(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
	}

	@Override
	public int getMaxProgress(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
		return Math.max(2, 5 * ((RecyclerAnalyzerRecipe)recipe).getSupport());
	}

	@Override
	public boolean canWork(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in, int ticks) 
	{
		return (tile.getSupport().get() < ticks * (tile.getSupport().getMax() - ((RecyclerAnalyzerRecipe)recipe).getSupport()));
	}
	
}
