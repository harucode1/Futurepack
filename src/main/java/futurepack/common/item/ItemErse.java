package futurepack.common.item;

import futurepack.common.FPConfig;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Food;
import net.minecraft.item.Item;

public class ItemErse extends BlockItem 
{
	private Food food;
	
	public ItemErse(Block crop, Item.Properties builder) 
	{
		super(crop, builder);
		food = new Food.Builder().nutrition(getHealAmount()).saturationMod(getSaturationModifier()).build();
	}

	public int getHealAmount() 
	{
		return FPConfig.COMMON.erse_food_value.get().intValue();
	}

	public float getSaturationModifier() 
	{
		return FPConfig.COMMON.erse_food_saturation.get().floatValue();
	}
	
	@Override
	public boolean isEdible() 
	{
		return food!=null;
	}
	
	@Override
	public Food getFoodProperties() 
	{
		if(food==null)
		{
			return super.getFoodProperties();
		}
		else
		{
			if(food.getNutrition() != getHealAmount() || food.getSaturationModifier() != getSaturationModifier())
			{
				food = new Food.Builder().nutrition(getHealAmount()).saturationMod(getSaturationModifier()).build();
				return food;
			}
			else
			{
				return food;
			}
		}
		
	}
}
