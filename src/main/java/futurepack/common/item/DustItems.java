package futurepack.common.item;

import futurepack.common.FuturepackMain;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class DustItems
{
	public static final Item.Properties dusts_64 = new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_resources);
	public static final Item dust_aluminium = new Item(dusts_64).setRegistryName("futurepack:dust_aluminium");
	public static final Item dust_bioterium = new Item(dusts_64).setRegistryName("futurepack:dust_bioterium");
	public static final Item dust_copper = new Item(dusts_64).setRegistryName("futurepack:dust_copper");
	public static final Item dust_glowtite = new Item(dusts_64).setRegistryName("futurepack:dust_glowtite");
	public static final Item dust_gold = new Item(dusts_64).setRegistryName("futurepack:dust_gold");
	public static final Item dust_iron = new Item(dusts_64).setRegistryName("futurepack:dust_iron");
	public static final Item dust_magnet = new Item(dusts_64).setRegistryName("futurepack:dust_magnet");
	public static final Item dust_neon = new Item(dusts_64).setRegistryName("futurepack:dust_neon");
	public static final Item dust_obsidian = new Item(dusts_64).setRegistryName("futurepack:dust_obsidian");
	public static final Item dust_quantanium = new Item(dusts_64).setRegistryName("futurepack:dust_quantanium");
	public static final Item dust_retium = new Item(dusts_64).setRegistryName("futurepack:dust_retium");
	public static final Item dust_tin = new Item(dusts_64).setRegistryName("futurepack:dust_tin");
	public static final Item dust_wakurium = new Item(dusts_64).setRegistryName("futurepack:dust_wakurium");
	public static final Item dust_zinc = new Item(dusts_64).setRegistryName("futurepack:dust_zinc");

	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.register(dust_aluminium);
		r.register(dust_bioterium);
		r.register(dust_copper);
		r.register(dust_glowtite);
		r.register(dust_gold);
		r.register(dust_iron);
		r.register(dust_magnet);
		r.register(dust_neon);
		r.register(dust_obsidian);
		r.register(dust_quantanium);
		r.register(dust_retium);
		r.register(dust_tin);
		r.register(dust_wakurium);
		r.register(dust_zinc);
	}

}
