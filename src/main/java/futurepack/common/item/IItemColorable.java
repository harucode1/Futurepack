package futurepack.common.item;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;

public interface IItemColorable 
{
	 /**
     * Return whether the specified ItemStack has a color.
     */
    public default boolean hasColor(ItemStack stack)
    {
        if (stack.getItem() instanceof IItemColorable)
        {
            CompoundNBT nbttagcompound = stack.getTag();
            return nbttagcompound != null && nbttagcompound.contains("display", 10) ? nbttagcompound.getCompound("display").contains("color", 3) : false;
        }
        
        return false;
    }

    /**
     * Return the color for the specified armor ItemStack.
     */
    public default int getColor(ItemStack stack)
    {
        if (!(stack.getItem() instanceof IItemColorable))
        {
            return 16777215;
        }
        else
        {
            CompoundNBT nbttagcompound = stack.getTag();

            if (nbttagcompound != null)
            {
                CompoundNBT nbttagcompound1 = nbttagcompound.getCompound("display");

                if (nbttagcompound1 != null && nbttagcompound1.contains("color", 3))
                {
                    return nbttagcompound1.getInt("color");
                }
            }

            return 10511680;
        }
    }

    /**
     * Remove the color from the specified armor ItemStack.
     */
    public default void removeColor(ItemStack stack)
    {
        if (stack.getItem() instanceof IItemColorable)
        {
            CompoundNBT nbttagcompound = stack.getTag();

            if (nbttagcompound != null)
            {
                CompoundNBT nbttagcompound1 = nbttagcompound.getCompound("display");

                if (nbttagcompound1.contains("color"))
                {
                    nbttagcompound1.remove("color");
                }
            }
        }
    }

    /**
     * Sets the color of the specified armor ItemStack
     */
    public default void setColor(ItemStack stack, int color)
    {
        if (!(stack.getItem() instanceof IItemColorable))
        {
            throw new UnsupportedOperationException("Object not an istance of IItemColorable");
        }
        else
        {
            CompoundNBT nbttagcompound = stack.getTag();

            if (nbttagcompound == null)
            {
                nbttagcompound = new CompoundNBT();
                stack.setTag(nbttagcompound);
            }

            CompoundNBT nbttagcompound1 = nbttagcompound.getCompound("display");

            if (!nbttagcompound.contains("display", 10))
            {
                nbttagcompound.put("display", nbttagcompound1);
            }

            nbttagcompound1.putInt("color", color);
        }
    }
}
