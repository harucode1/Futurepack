package futurepack.common.item.tools;

import futurepack.common.block.logistic.monorail.BlockMonorailBasic;
import futurepack.common.entity.monocart.EntityMonocart;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;


import net.minecraft.item.Item.Properties;

public class ItemMonocartbox extends Item 
{

	public ItemMonocartbox(Properties properties) 
	{
		super(properties);
	}

	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		World w = context.getLevel();
		if(!w.isClientSide)
		{
			ItemStack it = context.getItemInHand();
			BlockPos pos = context.getClickedPos();
			
			if(BlockMonorailBasic.isMonorail(w.getBlockState(pos)))
			{
				Entity cart = new EntityMonocart(w);
				if(it.hasTag() && it.getTag().contains("entity"))
				{
					cart.load(it.getTag().getCompound("entity"));
					cart.setUUID(MathHelper.createInsecureUUID(w.random));
				}
				cart.setPos(pos.getX()+0.5, pos.getY()+0.2, pos.getZ()+0.5);
				w.addFreshEntity(cart);
				it.shrink(1);
				return ActionResultType.SUCCESS;
			}
		}
		return super.useOn(context);
	}
}
