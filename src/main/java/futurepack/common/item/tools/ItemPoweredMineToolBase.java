package futurepack.common.item.tools;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;

public abstract class ItemPoweredMineToolBase extends Item implements IItemNeon
{

	protected float max_speed = 10F;
	
	public ItemPoweredMineToolBase(Item.Properties props) 
	{
		super(props.setNoRepair().defaultDurability(0).stacksTo(1));
	}
	
    public abstract boolean isMineable(BlockState bs);

	@Override
	public float getDestroySpeed(ItemStack it, BlockState b)
    {
        return isMineable(b) && getNeon(it)>0 ?max_speed : 1.0F;
    }
	
	@Override
	public boolean isCorrectToolForDrops(BlockState st)
	{
		return isMineable(st);
	}
	
	@Override
	public boolean mineBlock(ItemStack it, World w, BlockState state, BlockPos pos, LivingEntity p_150894_7_)
	{
		if (state.getDestroySpeed(w, pos) > 0.0D && getNeon(it)>0)
		{
			addNeon(it, -10);
		}	
		 
		return true;
	}

	@Override
	public int getMaxNeon(ItemStack it) 
	{
		return 2500;
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public void appendHoverText(ItemStack it, World w, List<ITextComponent> l, ITooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
}
