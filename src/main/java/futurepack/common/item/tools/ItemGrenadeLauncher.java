package futurepack.common.item.tools;

import java.util.List;

import javax.annotation.Nonnull;

import futurepack.api.interfaces.IGranadleLauncherAmmo;
import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPLog;
import futurepack.common.FPSounds;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BowItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class ItemGrenadeLauncher extends BowItem implements IItemNeon
{
	public ItemGrenadeLauncher(Item.Properties props)
	{
		super(props);
//        this.setMaxDamage(0);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Nonnull
	protected ItemStack findAmmo(PlayerEntity player)
    {
        if (this.isGrenade(player.getItemInHand(Hand.OFF_HAND)))
        {
            return player.getItemInHand(Hand.OFF_HAND);
        }
        else if (this.isGrenade(player.getItemInHand(Hand.MAIN_HAND)))
        {
            return player.getItemInHand(Hand.MAIN_HAND);
        }
        else
        {
            for (int i = 0; i < player.inventory.getContainerSize(); ++i)
            {
                ItemStack itemstack = player.inventory.getItem(i);

                if (this.isGrenade(itemstack))
                {
                    return itemstack;
                }
            }

            return ItemStack.EMPTY;
        }
    }
	
	private boolean isGrenade(ItemStack it)
	{
		if(it != null)
		{
			Item i = it.getItem();
			return ((i instanceof IGranadleLauncherAmmo) && ((IGranadleLauncherAmmo)i).isGranade(it));
		}
		return false;
	}

//	@Override
//	public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand)
//	{
//		boolean flag = this.findAmmo(playerIn) != null;
//
//		ActionResult<ItemStack> ret = net.minecraftforge.event.ForgeEventFactory.onArrowNock(itemStackIn, worldIn, playerIn, hand, flag);
//		if (ret != null) return ret;
//		
//		if (!playerIn.capabilities.isCreativeMode && !flag)
//		{
//			return !flag ? new ActionResult(EnumActionResult.FAIL, itemStackIn) : new ActionResult(EnumActionResult.PASS, itemStackIn);
//		}
//		else
//		{
//			playerIn.setActiveHand(hand);
//			return new ActionResult(EnumActionResult.SUCCESS, itemStackIn);
//		}
//	}
	
	@Override
	public void releaseUsing(ItemStack stack, World w, LivingEntity entityLiving, int timeLeft)
    {
		if (entityLiving instanceof PlayerEntity)
		{
			PlayerEntity pl = (PlayerEntity)entityLiving;
			boolean flag = pl.isCreative();
			boolean failed = false;
			ItemStack grenade = this.findAmmo(pl);

			int i = this.getUseDuration(stack) - timeLeft;
			
			if (i < 0) return;
			
			if (!grenade.isEmpty())
			{
				
				float f = getPowerForTime(i);
				
	                
				if (f >= 0.1D)
				{
					int nn = getNeon(stack);
					if(nn<10 && !flag)
					{
						f = Math.min(f, nn*0.1F);
					}
					
					if (!w.isClientSide)
					{
						
						Entity et = ((IGranadleLauncherAmmo)grenade.getItem()).createGrenade(w, grenade, pl, f);
						
						if(et == null)
							FPLog.logger.warn("Enity for Granade %s is null!", grenade);
						else	
						w.addFreshEntity(et);
					}
					else
					{
						return;
					}
					
					w.playSound((PlayerEntity)null, pl.getX(), pl.getY(), pl.getZ(), FPSounds.GRENADE_SHOT, SoundCategory.NEUTRAL, 1.0F, 1.0F / (random.nextFloat() * 0.4F + 1.2F) + f * 0.5F);
						
					
					if (!flag)
					{
						grenade.shrink(1);
						
						if (grenade.getCount() == 0)
						{
							pl.inventory.removeItem(grenade);
							addNeon(stack, (int) (f * -10));
						}
					}
					
					pl.awardStat(Stats.ITEM_USED.get(this));
				}
			}
		}
	}
	
	@Override	
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
    {
		ItemStack it = pl.getItemInHand(hand);
        boolean flag = this.findAmmo(pl) != null;

        if (!pl.isCreative() && !flag)
        {
            return !flag ? new ActionResult<ItemStack>(ActionResultType.FAIL, it) : new ActionResult<ItemStack>(ActionResultType.PASS, it);
        }
        else
        {
            pl.startUsingItem(hand);
            return new ActionResult<ItemStack>(ActionResultType.SUCCESS, it);
        }
    }
	
//	//@ TODO: OnlyIn(Dist.CLIENT)
//	@Override
//	public ModelResourceLocation getModel(ItemStack stack, EntityPlayer player, int useRemaining)
//	{
//		if(useRemaining==0)
//			return null;
//		
//		int j = this.getMaxItemUseDuration(stack) - useRemaining;
//		float f = (float)j /20F;
//        f = (f * f + f * 2.0F);
//        if(f<0.3F)
//        	return null;
//       
//        if(f>3.0F)
//        	f=3.0F;
//        f-=1;
//        if(f<0)
//        	f=0;
//		
//        int i = (int) f;
////		if(i==0)
////			return null;
////        
////		i--;
//		
//		ModelResourceLocation model = new ModelResourceLocation(Constants.MOD_ID +":items/grenade_launcher_pulling_" + i, "inventory");
//		return model;
//	}	

	@Override
	public int getMaxNeon(ItemStack it)
	{
		return 256;
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void appendHoverText(ItemStack stack, World w, List<ITextComponent> tooltip, ITooltipFlag advanced) 
	{
		tooltip.add(HelperItems.getTooltip(stack, this));
		super.appendHoverText(stack, w, tooltip, advanced);
	}
	
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1D - (double)getNeon(stack) / (double)getMaxNeon(stack);
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
}
