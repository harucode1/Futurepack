package futurepack.common.item.tools;

import futurepack.common.FuturepackTags;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;

import net.minecraft.item.Item.Properties;

public class ItemDrillMK1 extends ItemPoweredMineToolBase
{

	public ItemDrillMK1(Properties props)
	{
		super(props);
		max_speed = 50F;
	}
	
	@Override
	public boolean isMineable(BlockState bs)
	{
		return bs.getMaterial()==Material.STONE || bs.getBlock().is(FuturepackTags.BLOCK_ORES);
	}

}
