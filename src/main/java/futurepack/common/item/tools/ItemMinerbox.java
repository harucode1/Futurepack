package futurepack.common.item.tools;

import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.entity.EntityMiner;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;


import net.minecraft.item.Item.Properties;

public class ItemMinerbox extends Item 
{

	public ItemMinerbox(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		World w = context.getLevel();
		if(!w.isClientSide)
		{
			BlockPos pos = context.getClickedPos();
			Direction face = context.getClickedFace();
			
			if(w.getBlockState(pos).getBlock() == InventoryBlocks.drone_station)
			{
				EntityMiner m = new EntityMiner(w,pos.relative(face));
				m.setSide(face);
				w.addFreshEntity(m);
				context.getItemInHand().shrink(1);
				return ActionResultType.SUCCESS;
			}
		}
		return super.useOn(context);
	}
}
