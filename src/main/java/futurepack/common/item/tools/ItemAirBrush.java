package futurepack.common.item.tools;

import java.util.Collection;

import futurepack.common.item.misc.ItemLackTank;
import futurepack.common.recipes.airbrush.AirbrushRegistry;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.Property;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemAirBrush extends Item 
{
	
	public ItemAirBrush(Item.Properties props) 
	{
		super(props.stacksTo(1));
	}
	
	
	public static ItemStack getItem(CompoundNBT nbt)
	{
		if(nbt != null && nbt.contains("item"))
		{
			CompoundNBT tag = nbt.getCompound("item");
			return ItemStack.of(tag);
		}
		
		return ItemStack.EMPTY;
	}
	
	public static void setItem(CompoundNBT nbt, ItemStack it)
	{
		if(it != null)
		{
			CompoundNBT tag = it.save(new CompoundNBT());
			nbt.put("item", tag);
			return;
		}
		nbt.remove("item");
	}
	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(pl.getItemInHand(hand) == it && pl.isShiftKeyDown())
		{
			if(!w.isClientSide)
				FPGuiHandler.AIRBRUSH.openGui((ServerPlayerEntity)pl, (Object[])null);
			return ActionResult.success(it);
		}
		return ActionResult.pass(it);
	}

	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		PlayerEntity pl = context.getPlayer();
		if(pl.isShiftKeyDown())
			return ActionResultType.PASS;
		
		ItemStack it = context.getItemInHand();

		if(!context.getLevel().isClientSide)
		{
			if(!it.hasTag())
				it.setTag(new CompoundNBT());
			ItemStack inside = getItem(it.getTag());
			if(inside !=null && !inside.isEmpty())
			{
				BlockState state = context.getLevel().getBlockState(context.getClickedPos());
				boolean success = false;
				if(AirbrushRegistry.isUncolorItem(inside))
				{
					Block uncolored = AirbrushRegistry.getUncoloredBlock(state.getBlock());
					if(uncolored != null)
					{
						replaceBlock(context.getLevel(), context.getClickedPos(), pl, state, uncolored);
						success = true;
					}
				}
				else if(inside.getItem() instanceof ItemLackTank)
				{
					Block colored = AirbrushRegistry.getColoredBlock(state.getBlock(), ((ItemLackTank)inside.getItem()).getColor());
					if(colored != null)
					{
						replaceBlock(context.getLevel(), context.getClickedPos(), pl, state, colored);
						success = true;
					}
				}
				
				if(success && !pl.isCreative())
				{
					if(inside.isDamageableItem())
					{
						Item container = inside.getItem().getCraftingRemainingItem();
						inside.hurtAndBreak(1, pl, breaker -> {});
						if(inside.isEmpty())
						{
							inside = new ItemStack(container);
						}
					}
					else
					{
						inside.shrink(1);					
					}
					if(inside.getCount()<=0)
					{
						inside = null;
					}
					setItem(it.getTag(), inside);
				}
				
			}
		}
		return ActionResultType.SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public static void replaceBlock(World w, BlockPos pos, PlayerEntity pl, BlockState oldstate, Block newBlock)
	{
		BlockState newstate = newBlock.defaultBlockState();
		Collection<Property<?>> col = newstate.getProperties();
		for(@SuppressWarnings("rawtypes") Property prop : col)
		{
			if(oldstate.hasProperty(prop))
			{
				newstate = newstate.setValue(prop, oldstate.getValue(prop));
			}
		}
		
		w.levelEvent(2001, pos, Block.getId(newstate));//same event as in w.destroyBlock(BlockPos, boolean drop)
		w.setBlock(pos, newstate, 3);

		w.playLocalSound(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, SoundEvents.FIRE_EXTINGUISH, SoundCategory.BLOCKS, 2.0F, 0.2F, true);
	}
	
	public static boolean isInArray(Object[] arr, Object par2)
	{
		for(Object  t : arr)
		{
			if(par2.equals(t))
			{
				return true;
			}
		}
		return false;
	}
	
	public static boolean isInArray(int[] arr, int par2)
	{
		for(int  t : arr)
		{
			if(par2 == t)
			{
				return true;
			}
		}
		return false;
	}
}
