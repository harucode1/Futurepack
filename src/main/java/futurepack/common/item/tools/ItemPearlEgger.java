package futurepack.common.item.tools;

import java.util.List;
import java.util.UUID;

import futurepack.common.item.ItemDispensable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IPosition;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemPearlEgger extends ItemDispensable 
{
	
	public ItemPearlEgger(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public void appendHoverText(ItemStack it, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(it, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.items.pearlegger", it.getOrCreateTagElement("owner").getInt("charges")));
	}

	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		World w = context.getLevel();
		if(!w.isClientSide)
		{
			ItemStack it = context.getItemInHand();
			
			if(it.hasTag())
			{
				CompoundNBT nbt = it.getTagElement("owner");
				if(nbt!=null)
				{
					int charges = nbt.getInt("charges");
					if(charges > 0)
					{
						UUID uuid = nbt.getUUID("creator");
						PlayerEntity player = w.getPlayerByUUID(uuid);
						if(player!=null)
						{
							if(player.isSleeping())
							{
								player.stopSleepInBed(true, false); //wakePlayer
							}
							BlockPos pp = context.getClickedPos().relative(context.getClickedFace());
							player.teleportTo(pp.getX()+0.5, pp.getY()+0.2, pp.getZ()+0.5);
							player.fallDistance = 0.0F;
							charges--;
							nbt.putInt("charges", charges);
							return ActionResultType.SUCCESS;
						}
					}
					
				}
			}
		}
		return super.useOn(context);
	}
	
	@Override
	public ItemStack dispense(IBlockSource src, ItemStack it, IPosition pos, Direction enumfacing)
	{
		if(it.hasTag())
		{
			CompoundNBT nbt = it.getTagElement("owner");
			if(nbt!=null)
			{
				int charges = nbt.getInt("charges");
				if(charges > 0)
				{
					UUID uuid = nbt.getUUID("creator");
					PlayerEntity player = src.getLevel().getPlayerByUUID(uuid);
					if(player!=null)
					{
						if(player.isSleeping())
						{
							player.stopSleepInBed(true, false); //wakePlayer
						}
						player.teleportTo(pos.x()+0.5, pos.y()+0.2, pos.z()+0.5);
						player.fallDistance = 0.0F;
						charges--;
						nbt.putInt("charges", charges);
					}
				}
			}
		}
		return it;
	}
}
