package futurepack.common.item.tools;

import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class ItemPlasmaSchneider extends ItemPoweredMineToolBase
{

	public ItemPlasmaSchneider(Item.Properties props) 
	{
		super(props);
		max_speed = 250F;
	}
	
	@Override
	public boolean isMineable(BlockState bs)
	{
		return bs.getMaterial()==Material.METAL;
	}
}
