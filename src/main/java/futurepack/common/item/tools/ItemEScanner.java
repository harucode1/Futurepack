package futurepack.common.item.tools;

import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.ScanningManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.server.ServerWorld;

public class ItemEScanner extends Item
{
	//IIcon s;
	
	public ItemEScanner(Item.Properties props) 
	{
		super(props);
		
	}
	
//	@Override
//	public void registerIcons(IIconRegister r)
//	{
//		super.registerIcons(r);
//		s = r.registerIcon(getIconString()+"_s");
//	}
//	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
	{
		if(!w.isClientSide)
		{
			CustomPlayerData.getDataFromPlayer(pl).addAchievement(null);
		}
		ItemStack it = pl.getItemInHand(hand);
		
		final RayTraceResult pos = getPlayerPOVHitResult(w, pl, FluidMode.ANY);
		if(pos!=null && pos.getType() == Type.BLOCK)
		{
			ScanningManager.doBlock(w, ((BlockRayTraceResult)pos).getBlockPos(), pl, (BlockRayTraceResult)pos);	
			return ActionResult.success(it);
		}
		else
		{
			ScanningManager.openStart(pl.level, pl);
			return ActionResult.success(it);
		}
	}
	
	@Override
	public void inventoryTick(ItemStack it, World w, Entity e, int itemSlot, boolean isSelected) 
	{
		if(w.isClientSide)
			return;
		
		Chunk c = w.getChunkAt(e.blockPosition());
		if(!it.hasTag())
		{
			it.setTag(new CompoundNBT());
		}
		
		boolean flag = SharedSeedRandom.seedSlimeChunk(c.getPos().x, c.getPos().z, ((ServerWorld)w).getSeed(), 987234911L).nextInt(10) == 0;
		flag &= e.getY() < 40.0D;
		
        it.getTag().putBoolean("s", flag);
		super.inventoryTick(it, w, e, itemSlot, isSelected);
	}
	
	@Override
	public ActionResultType interactLivingEntity(ItemStack it, PlayerEntity pl, LivingEntity liv, Hand hand)
	{
//		pl.addStat(FPAchievements.theImpalpable, 1);
		ScanningManager.doEntity(pl.level, liv, pl);		
		return ActionResultType.SUCCESS;
	}
	
	
}
