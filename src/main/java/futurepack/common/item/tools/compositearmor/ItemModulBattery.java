package futurepack.common.item.tools.compositearmor;

import futurepack.api.interfaces.IItemNeon;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemModulBattery extends ItemModulNeonContainer implements IItemNeon
{

	public ItemModulBattery(int maxNE, Item.Properties props)
	{
		super(null, maxNE, props);
	}

	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory inv)
	{
		if(getNeon(it)<=0)
		{
			return;
		}
		
		chargeItem(it, player.getMainHandItem());
		chargeItem(it, player.getOffhandItem());
	}
	
	
	private void chargeItem(ItemStack it, ItemStack hand)
	{
		if(hand.getItem() instanceof IItemNeon)
		{		
			int ne = getNeon(it);
			int other_max = ((IItemNeon)hand.getItem()).getMaxNeon(hand);
			int other_ne = ((IItemNeon)hand.getItem()).getNeon(hand);
		
			int charge = Math.min(Math.min(ne, other_max-other_ne), 5);
			
			if(charge > 0)
			{
				addNeon(it, -charge);
				((IItemNeon)hand.getItem()).addNeon(hand, charge);
			}
		}
	}
	
	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlotType type, CompositeArmorPart armorPart)
	{
		return type.getType() == EquipmentSlotType.Group.ARMOR;
	}
	
	@Override
	public boolean isEnergyProvider()
	{
		return true;
	}
	
	@Override
	public boolean isEnergyConsumer()
	{
		return true;
	}

}
