package futurepack.common.item.tools.compositearmor;

import futurepack.common.FPSounds;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemModulReanimation extends ItemModulShield
{

	public ItemModulReanimation(Properties props)
	{
		super(props, 1000);
		initStatus=1000;
	}
	
	@Override
	public boolean canBlockDamage(World world, PlayerEntity player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		return player.getHealth() - amount <= 0F;
	}
	
	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory armor)
	{
		super.onArmorTick(world, player, it, armor);
		
		CompoundNBT nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundNBT();
			it.setTag(nbt);
		}
		if(!world.isClientSide)
		{
			if(nbt.getInt("status") == 38)
			{
				player.setHealth(player.getMaxHealth());
			}
		}
		
			
	}
	
	@Override
	public boolean onWearerDamaged(World world, PlayerEntity player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		CompoundNBT nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundNBT();
			it.setTag(nbt);
		}
		boolean block = canBlockDamage(world, player, it, inv, src, amount);
		if(!block)
			return false;
		
		if(nbt.getInt("status")>=getMaxStatus(nbt))
		{
			if(!world.isClientSide)
			{
				nbt.putInt("status", 0);
				player.setHealth(1F);
				world.playSound(null, player.getX(), player.getY(), player.getZ(), FPSounds.DEFIBRILATOR, SoundCategory.PLAYERS, 0.8F, 1.0F);
			}
				
			
			return true;
		}
		else if(nbt.getInt("status") < 40)
		{
			return true;
		}
		
		return false;
	}

}
