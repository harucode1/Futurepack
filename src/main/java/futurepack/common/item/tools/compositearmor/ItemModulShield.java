package futurepack.common.item.tools.compositearmor;

import java.util.List;

import futurepack.common.FPSounds;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.DamageSource;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

public class ItemModulShield extends ItemModulNeonContainer
{
	protected float initDamage=2F;
	protected int initStatus=20;
	
	public ItemModulShield(Item.Properties props)
	{
		super(EquipmentSlotType.CHEST, 100, props);
	}
	
	public ItemModulShield(Item.Properties props, int neStorage)
	{
		super(EquipmentSlotType.CHEST, neStorage, props);
	}

	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory armor)
	{
		pullEnergy(armor, it);
		
		CompoundNBT nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundNBT();
			it.setTag(nbt);
		}
		
		if(getNeon(it)>0)
		{
			int st = nbt.getInt("status");
			if(st < nbt.getInt("maxstatus"))
			{
				st++;
				addNeon(it, -1);
				nbt.putInt("status", st);
			}
		}
	}
	
	
	public boolean canBlockDamage(World world, PlayerEntity player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		CompoundNBT nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundNBT();
			it.setTag(nbt);
		}
		
		String[] type;
		
		if(nbt.contains("damage_types"))
		{
			type = nbt.getString("damage_types").toLowerCase().split("\\+");
		}
		else
		{
			type = default_type;
		}
		
		for(String s : type)
		{
			if(src.msgId.toLowerCase().contains(s))
			{
				return true;
			}
		}
		return false;
	}
	
	public float applyDamageReduction(World world, PlayerEntity player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		return amount;
	}
	
	/**
	 * @return true if the event should get canceled
	 */
	private static final String[] default_type = {"mob", "player", "arrow", "fireball", "thrown", "indirectMagic", "explosion", "lightningBolt", "cactus", "magic", "fallingBlock", "dragonBreath", "fireworks", "neonDamage", "bee.hive", "bee.aggressive", "sting", "sweetBerryBush", "trident"};
	
	public boolean onWearerDamaged(World world, PlayerEntity player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		CompoundNBT nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundNBT();
			it.setTag(nbt);
		}
		boolean block = canBlockDamage(world, player, it, inv, src, amount);
		
		if(!block)
			return false;
		
		float state = (float)nbt.getInt("status") / (float)getMaxStatus(nbt);
		
		if(state>0)
		{
			float dmg = getDamage(nbt) * state;
			if(amount >= dmg)
			{
				nbt.putInt("status", 0);
				amount -= dmg;
				
				player.hurt(src, amount);
			}
			else
			{
				float f = amount / dmg;
				f = state -f;
				
				nbt.putInt("status", (int) (getMaxStatus(nbt) * f));		
			}
			
			if(!world.isClientSide)
				world.playSound(null, player.getX(), player.getY(), player.getZ(), FPSounds.SHIELD_HIT, SoundCategory.PLAYERS, 0.2F, 1.6F);
			
			return true;
		}	
		
		return false;
	}
	
	protected float getDamage(CompoundNBT nbt)
	{
		if(nbt.contains("damage"))
		{
			return nbt.getFloat("damage");
		}
		nbt.putFloat("damage", initDamage);
		return initDamage;
	}
	
	protected int getMaxStatus(CompoundNBT nbt)
	{
		if(nbt.contains("maxstatus"))
		{
			return nbt.getInt("maxstatus");
		}
		nbt.putInt("maxstatus", initStatus);
		return initStatus;
	}
	
	public static void onLivingDamaged(LivingAttackEvent event)
	{
		if(event.isCanceled())
			return;
		
		if(event.getEntityLiving() instanceof PlayerEntity)
		{
			PlayerEntity pl = (PlayerEntity) event.getEntityLiving();
			for(int slot=0;slot<pl.inventory.armor.size();slot++)
			{
				ItemStack armor = pl.inventory.armor.get(slot);
				if(armor.isEmpty())
					continue;
				CompositeArmorPart armorPart = CompositeArmorPart.getInventory(armor);
				if(armorPart!=null)
				{
					for(int i=0;i<armorPart.getSlots();i++)
					{
						ItemStack sub = armorPart.getStackInSlot(i);
						if(sub.isEmpty())
							continue;
						if(sub.getItem() instanceof ItemModulShield)
						{
							if(((ItemModulShield)sub.getItem()).onWearerDamaged(event.getEntity().level, pl, sub, armorPart, event.getSource(), event.getAmount()))
							{
								event.setCanceled(true);
								return;
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void fillItemCategory(ItemGroup tab, NonNullList<ItemStack> subItems)
	{
		if(allowdedIn(tab))
		{
			ItemStack stack = new ItemStack(this);
			stack.setTag(new CompoundNBT());
			
			stack.getTag().putInt("maxstatus", initStatus);
			stack.getTag().putFloat("damage", initDamage);
			
			subItems.add(stack);
		}
	}
	
	@Override
	public void appendHoverText(ItemStack it, World w, List<ITextComponent> l, ITooltipFlag p_77624_4_)
	{
		super.appendHoverText(it, w, l, p_77624_4_);
		if(it.getTag()!=null)
		{
			CompoundNBT nbt = it.getTag();
			l.add(new StringTextComponent(nbt.getInt("status") +  "/" + nbt.getInt("maxstatus")));
		}
	}
	
	@Override
	public boolean isEnergyConsumer()
	{
		return true;
	}
}
