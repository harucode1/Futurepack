
package futurepack.common.item.tools.compositearmor;

import java.util.UUID;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import com.google.common.collect.Multimap;

import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemModulHealthboost extends ItemModulStatChange
{
	private final double hpBoost;
	
	public ItemModulHealthboost(Properties props, double hpBoost)
	{
		super(null, props);
		this.hpBoost = hpBoost;
	}

	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory inv)
	{
		
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlotType slot)
	{
		if(slot == EquipmentSlotType.MAINHAND || slot == EquipmentSlotType.OFFHAND)
		{
			return ImmutableMultimap.of();
		}

		Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		UUID uuid = getUUID(slot);
		builder.put(Attributes.MAX_HEALTH, new AttributeModifier(uuid, "HP Boost", this.hpBoost, AttributeModifier.Operation.ADDITION));
		return builder.build();
	}

}
