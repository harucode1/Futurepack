package futurepack.common.item.tools.compositearmor;

import futurepack.api.interfaces.IAirSupply;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemModulOxygenTank extends ItemModulOxygenContainer
{

	public ItemModulOxygenTank(Item.Properties props) 
	{
		super(null, 6000, props);
	}

	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory inv)
	{
		if(!world.isClientSide)
		{
			IAirSupply supply = AtmosphereManager.getAirSupplyFromEntity(player);
			if(supply.getAir() >= 300)
			{	
				int filled = addOxygen(it, 6);
				
				if(filled > 0)
					supply.reduceAir(filled);
			}
		}
		
		
	}
	
	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlotType type, CompositeArmorPart armorPart)
	{
		return type.getType() == EquipmentSlotType.Group.ARMOR;
	}

}
