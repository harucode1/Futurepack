package futurepack.common.item.tools.compositearmor;

import futurepack.common.ManagerGleiter;
import futurepack.common.item.IItemColorable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;

public class ItemModulParaglider extends ItemModulArmorBase implements IItemColorable
{

	public ItemModulParaglider(Item.Properties props) 
	{
		super(EquipmentSlotType.CHEST, props);
	}

	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory inv)
	{
		if(player.fallDistance > 3.0f)
		{
			ManagerGleiter.setGleiterOpen(player, true);
		}
	}
	
	@Override
	public int getColor(ItemStack stack)
    {
		 CompoundNBT tag = stack.getTag();

         if (tag != null)
         {
             CompoundNBT nbt = tag.getCompound("display");

             if (nbt != null && nbt.contains("color", 3))
             {
                 return nbt.getInt("color");
             }
         }

         return 0x3df9f7;
    }

}
