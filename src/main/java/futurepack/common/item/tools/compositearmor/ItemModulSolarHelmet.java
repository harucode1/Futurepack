package futurepack.common.item.tools.compositearmor;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemModulSolarHelmet extends ItemModulNeonContainer
{
	public ItemModulSolarHelmet(Item.Properties props) 
	{
		super(EquipmentSlotType.HEAD, 750, props);
	}

	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory inv)
	{	
		pushEnergy(inv, it);
		
		if(world.isDay() && world.canSeeSky(player.blockPosition()))
		{
			if(getNeon(it) < getMaxNeon(it))
			{
				addNeon(it, 1);
			}
		}
	}
	
	@Override
	public boolean isEnergyProvider()
	{
		return true;
	}
	
}
