package futurepack.common.item.tools.compositearmor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public abstract class ItemModulArmorBase extends Item
{
	public final EquipmentSlotType position;
	
	public ItemModulArmorBase(EquipmentSlotType slot, Item.Properties props)
	{
		super(props.stacksTo(1));
		this.position = slot;
//		if(slot.getSlotType()!=Type.ARMOR)
//			throw new IllegalStateException("can not add an ArmorModul to a non armor position");	
	}
	
	public abstract void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory inv);

	public boolean isSlotFitting(ItemStack stack, EquipmentSlotType type, @Nullable CompositeArmorPart armorPart)
	{
		return type == position;
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void appendHoverText(ItemStack stack, World w, List<ITextComponent> tooltip, ITooltipFlag advanced)
	{
		tooltip.add(new TranslationTextComponent("tooltip.composite.can_be_installed"));
		for(int i = EquipmentSlotType.values().length - 1; i > 0; i--)
		{
			EquipmentSlotType slot = EquipmentSlotType.values()[i];
			if(isSlotFitting(stack, slot, null))
				tooltip.add(new TranslationTextComponent("tooltip.composite.part." + slot.getName()));
		}
		super.appendHoverText(stack, w, tooltip, advanced);
	}
}
