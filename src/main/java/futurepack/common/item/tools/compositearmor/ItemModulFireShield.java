package futurepack.common.item.tools.compositearmor;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemModulFireShield extends ItemModulShield
{

	public ItemModulFireShield(Properties props)
	{
		super(props);
		initDamage *= 2;
	}
	
	@Override
	public boolean canBlockDamage(World world, PlayerEntity player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		return src.isFire();
	}

	@Override
	public float applyDamageReduction(World world, PlayerEntity player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		return amount * 0.5F;
	}
	
	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlotType type, CompositeArmorPart armorPart)
	{
		return super.isSlotFitting(stack, type, armorPart) || type == EquipmentSlotType.LEGS || type == EquipmentSlotType.FEET;
	}
}
