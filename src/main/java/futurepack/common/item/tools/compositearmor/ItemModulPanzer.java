package futurepack.common.item.tools.compositearmor;

import java.util.UUID;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import com.google.common.collect.Multimap;

import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemModulPanzer extends ItemModulStatChange
{

	private final int damageReduceAmount;
	private final float toughness;
	protected final float knockbackResistance;
	
	public ItemModulPanzer(Properties props, IArmorMaterial materialIn)
	{
		super(null, props);
		this.damageReduceAmount = materialIn.getDefenseForSlot(EquipmentSlotType.HEAD);
		this.toughness = materialIn.getToughness();
		this.knockbackResistance = materialIn.getKnockbackResistance();
	}
	
	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory inv)
	{
		
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlotType slot)
	{
		if(slot == EquipmentSlotType.MAINHAND || slot == EquipmentSlotType.OFFHAND)
		{
			return ImmutableMultimap.of();
		}
	
		Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		UUID uuid = getUUID(slot);
		builder.put(Attributes.ARMOR, new AttributeModifier(uuid, "Armor modifier", (double)this.damageReduceAmount, AttributeModifier.Operation.ADDITION));
		builder.put(Attributes.ARMOR_TOUGHNESS, new AttributeModifier(uuid, "Armor toughness", (double)this.toughness, AttributeModifier.Operation.ADDITION));
		if (this.knockbackResistance > 0)
		{
			builder.put(Attributes.KNOCKBACK_RESISTANCE, new AttributeModifier(uuid, "Armor knockback resistance", (double)this.knockbackResistance, AttributeModifier.Operation.ADDITION));
		}
		return builder.build();
	}

}
