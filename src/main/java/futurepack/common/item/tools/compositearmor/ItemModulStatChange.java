package futurepack.common.item.tools.compositearmor;

import java.util.EnumMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Multimap;

import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;

import net.minecraft.item.Item.Properties;

public abstract class ItemModulStatChange extends ItemModulArmorBase
{
	protected static final long major = 0x54235678789L;
	protected static final long head = 0x121290902390L;
	protected static final long chest = 0x8909034590909034L;
	protected static final long legs = 0x34897895675670L;
	protected static final long feet = 0x123123456567789L;
	protected static long[] ARMOR_MODIFIERS = new long[]{head, chest, legs, feet};
	
	protected Map<EquipmentSlotType, Multimap<Attribute, AttributeModifier>> attributes = new EnumMap<EquipmentSlotType, Multimap<Attribute,AttributeModifier>>(EquipmentSlotType.class);
	
	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlotType slot, ItemStack stack)
	{
		return attributes.computeIfAbsent(slot, this::getDefaultAttributeModifiers);
	}
	
	public ItemModulStatChange(EquipmentSlotType slot, Properties props)
	{
		super(slot, props);
	}

	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlotType type, @Nullable CompositeArmorPart armorPart)
	{
		if(type.getType() == EquipmentSlotType.Group.ARMOR)
		{
			if(armorPart!=null)
			{
				for(int i=0;i<armorPart.getSlots();i++)
				{
					ItemStack st = armorPart.getStackInSlot(i);
					if(st.isEmpty())
						continue;
					else if(st.getItem() == this)
						return false;
				}
			}
			return true;
		}
		
		return false;
	}
	
	@Override
	public abstract Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlotType slot);
	
	protected UUID getUUID(EquipmentSlotType slot)
	{
		int materialName = this.getRegistryName().toString().hashCode();
		UUID uuid = new UUID(major ^materialName, ARMOR_MODIFIERS[slot.getIndex()] ^materialName);
		return uuid;
	}
}
