package futurepack.common.item.tools.compositearmor;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public abstract class ItemModulOxygenContainer extends ItemModulArmorBase
{
	private final int defaultMaxOxygen;
	
	public ItemModulOxygenContainer(EquipmentSlotType slot, int maxOxygen, Item.Properties props)
	{
		super(slot, props);
		defaultMaxOxygen = maxOxygen;	
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRgb(0.62F, 1.0F, (0.5F + (float)getOxygen(stack) / (float)getMaxOxygen(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getOxygen(stack) / (double)getMaxOxygen(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getOxygen(stack) < getMaxOxygen(stack);
	}
	

	public static int addOxygen(ItemStack it, int i)
	{
		int ox = getOxygen(it) + i;
		int max = getMaxOxygen(it);
		
		if(ox > max)
		{
			i = max - ox + i;
			ox = max;
		}
			
		CompoundNBT nbt = it.getTagElement("oxygen");
		if(nbt==null)
		{
			nbt = constructNBT(it);	
		}
		nbt.putInt("amount", ox);
		it.addTagElement("oxygen", nbt);
		
		return i;
	}

	public static int getMaxOxygen(ItemStack it)
	{
		CompoundNBT nbt = it.getTagElement("oxygen");
		if(nbt==null)
		{
			nbt = constructNBT(it);	
			return ((ItemModulOxygenContainer)it.getItem()).defaultMaxOxygen;
		}
		return nbt.getInt("max");
	}
	
	public static int getOxygen(ItemStack it)
	{
		CompoundNBT nbt = it.getTagElement("oxygen");
		if(nbt==null)
		{
			nbt = constructNBT(it);	
			return 0;
		}
		return nbt.getInt("amount");
	}

	private static CompoundNBT constructNBT(ItemStack it)
	{
		CompoundNBT nbt = new CompoundNBT();
		nbt.putInt("max", ((ItemModulOxygenContainer)(it.getItem())).defaultMaxOxygen);
		nbt.putInt("amount", 0);
		it.addTagElement("oxygen", nbt);
		return nbt;
	}
	
	@Override
	public void appendHoverText(ItemStack it, World w, List<ITextComponent> l, ITooltipFlag p_77624_4_) 
	{
		l.add(new StringTextComponent(getOxygen(it) + "/" + getMaxOxygen(it) + " O\u2082"));
		super.appendHoverText(it, w, l, p_77624_4_);
	}

}
