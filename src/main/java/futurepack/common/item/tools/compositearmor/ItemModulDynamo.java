package futurepack.common.item.tools.compositearmor;

import java.util.WeakHashMap;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class ItemModulDynamo extends ItemModulNeonContainer
{
	private static final WeakHashMap<PlayerEntity, Vector3d> last_pos = new WeakHashMap<PlayerEntity, Vector3d>();
	
	public ItemModulDynamo(Item.Properties props) 
	{
		super(EquipmentSlotType.LEGS, 1000, props);
	}

	@Override
	public void onArmorTick(World world, PlayerEntity player, ItemStack it, CompositeArmorInventory inv)
	{	
		pushEnergy(inv, it);
		
		Vector3d last = last_pos.get(player);
		
		Vector3d pos = player.position();
		
		last_pos.put(player, pos);
		
		if(last == null)
			return;
		
		if(player.isPassenger() || !player.isOnGround())
			return;
		
		float speed = (float) last.distanceTo(player.position());
			
		if(speed <= 0.05)
			return;
		
		if(world.random.nextFloat() < speed)
		{
			if(getNeon(it) < getMaxNeon(it))
			{
				addNeon(it, 1);	
				player.causeFoodExhaustion(0.05F);
			}
		}
	}
	
	@Override
	public boolean isEnergyProvider()
	{
		return true;
	}
	
}
