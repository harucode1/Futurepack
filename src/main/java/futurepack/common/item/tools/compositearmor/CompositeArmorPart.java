package futurepack.common.item.tools.compositearmor;

import java.util.WeakHashMap;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemStackHandler;

public class CompositeArmorPart extends ItemStackHandler
{
	public final EquipmentSlotType type;
	public final ItemStack container;	
	
	public CompositeArmorPart(ItemStack it, EquipmentSlotType type, int slotCount)
	{
		super(slotCount);
		this.type = type;
		this.container = it;
		this.deserializeNBT(it.getOrCreateTagElement("modules"));
	}	
	
	@Override
	public int getSlotLimit(int slot)
	{
		return 1;
	}
	
	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if(isItemValid(slot, stack))	
		{
			return super.insertItem(slot, stack, simulate);			
		}
		return stack;
	}
	
	@Override
	public boolean isItemValid(int slot, ItemStack stack)
	{
		if(stack.getItem() instanceof ItemModulArmorBase)	
		{
			ItemModulArmorBase modul = (ItemModulArmorBase) stack.getItem();
			if(modul.isSlotFitting(stack, type, this))
			{
				return true;
			}			
		}
		return false;
	}
	
	public void onArmorTick(World w, PlayerEntity pl, CompositeArmorInventory armor)
	{
		for(ItemStack it : stacks)
		{
			if(!it.isEmpty())
			{
				((ItemModulArmorBase)it.getItem()).onArmorTick(w, pl, it, armor);
			}
		}
	}
	
	public void saveInventoryInItem()
	{
		CompoundNBT tag = this.serializeNBT();
		
		if(!container.hasTag())
		{
			container.setTag(new CompoundNBT());
		}
		container.getTag().put("modules", tag);
	}
	
	private static WeakHashMap<ItemStack, CompositeArmorPart> map = new WeakHashMap<ItemStack, CompositeArmorPart>();
	
	public static CompositeArmorPart getInventory(ItemStack it)
	{
		if(it.getItem() instanceof ItemCompositeArmor)
		{
			if ( ((ItemCompositeArmor)it.getItem()).isBroken(it) )
			{
				map.remove(it);
				return null;
			}
				
			
			CompositeArmorPart inv = map.get(it);
			if(inv==null)
			{
				ItemCompositeArmor armor = (ItemCompositeArmor) it.getItem();
				inv = armor.createArmorPartInstance(it);
				map.put(it, inv);
			}
			return inv;
		}
		return null;
	}
	
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers() 
	{
		Multimap<Attribute, AttributeModifier> mmap = HashMultimap.create();
		
		for(int i=0;i<this.getSlots();i++)
		{
			ItemStack stack = this.getStackInSlot(i);
			if(!stack.isEmpty())
			{
				Multimap<Attribute, AttributeModifier> imap = stack.getAttributeModifiers(type);
				mmap.putAll(imap);
			}
		}
		return mmap;
	}
}
