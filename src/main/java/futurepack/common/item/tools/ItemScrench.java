package futurepack.common.item.tools;

import java.util.List;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.item.Rarity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemScrench extends Item 
{

	public ItemScrench(Item.Properties props) 
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		ItemStack it = context.getItemInHand();
		World w = context.getLevel();
		BlockPos pos = context.getClickedPos();
		
		if(it.getOrCreateTag().getBoolean("rotate"))
		{
			TileEntity t = w.getBlockEntity(pos);
			CompoundNBT nbt = new CompoundNBT();
			if(t!=null)
			{
				t.save(nbt);
			}
			BlockState state = w.getBlockState(pos);
//			Block bl = state.getBlock();
//			Random rand = new Random();

			
			BlockState changed	= state.rotate(w, pos, Rotation.COUNTERCLOCKWISE_90);
			if(state != changed)
			{
				w.setBlock(pos, changed, 3);
			}
			else if(state.hasProperty(BlockRotateableTile.FACING))
			{
				changed = state.setValue(BlockRotateableTile.FACING, state.getValue(BlockRotateableTile.FACING).getOpposite());
				if(state != changed)
				{
					w.setBlock(pos, changed, 3);
				}
			}

			t = w.getBlockEntity(pos);
			if(t!=null)
			{
				t.clearCache();
				t.load(changed, nbt);
				t.setChanged();
			}
			return ActionResultType.SUCCESS;
		}
		else if(w.getBlockEntity(pos) instanceof TileEntityModificationBase)
		{
			if(!w.isClientSide)
				FPGuiHandler.CHIPSET.openGui(context.getPlayer(), pos);
			
			return ActionResultType.SUCCESS;
		}
		return ActionResultType.PASS;
	}
	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(!pl.isShiftKeyDown())
		{
			if(it.getOrCreateTag().getBoolean("rotate"))
			{
				it.getOrCreateTag().putBoolean("rotate", false);
			}
			else
			{
				it.getOrCreateTag().putBoolean("rotate", true);
			}
		}
		return ActionResult.success(it);
	}
	
	@Override
	public Rarity getRarity(ItemStack stack)
	{
		return Rarity.UNCOMMON;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World w, List<ITextComponent> tooltip, ITooltipFlag advanced)
	{
		tooltip.add(new TranslationTextComponent(stack.getOrCreateTag().getBoolean("rotate") ? "tooltip.items.scrench.rotate":"tooltip.items.scrench.modify"));
	}
	
}
