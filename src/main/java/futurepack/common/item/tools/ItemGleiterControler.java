package futurepack.common.item.tools;

import futurepack.common.ManagerGleiter;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.IDyeableArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

public class ItemGleiterControler extends ArmorItem implements IDyeableArmorItem
{
	public ItemGleiterControler(Item.Properties props) 
	{
		super(ArmorMaterial.LEATHER, EquipmentSlotType.CHEST, props);
//		
//		super(ArmorMaterial.LEATHER, 0, EntityEquipmentSlot.CHEST);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
	{
//		ItemStack it = pl.getHeldItem(hand);
		if(!w.isClientSide)
		{
			boolean open = ManagerGleiter.isGleiterOpen(pl);
			ManagerGleiter.setGleiterOpen(pl, !open);		
		}
		return super.use(w, pl, hand);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type)
	{
		if(slot== EquipmentSlotType.CHEST)
		{
			if(type!=null && type.equalsIgnoreCase("overlay"))
			{
				return "futurepack:textures/models/armor/paragleiter_over.png";
			}
			else
			{	
				return "futurepack:textures/models/armor/paragleiter.png";
			}
		}
		return super.getArmorTexture(stack, entity, slot, type);
	}
	
	@Override
	public int getColor(ItemStack stack)
    {
		CompoundNBT nbttagcompound = stack.getTagElement("display");
		return nbttagcompound != null && nbttagcompound.contains("color", 99) ? nbttagcompound.getInt("color") : 0xaa2111;
    }
}
