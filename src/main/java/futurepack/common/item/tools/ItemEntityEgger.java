package futurepack.common.item.tools;

import futurepack.common.entity.throwable.EntityEgger;
import futurepack.common.item.ItemDispensable;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IPosition;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.world.World;


import net.minecraft.item.Item.Properties;

public class ItemEntityEgger extends ItemDispensable 
{

	public ItemEntityEgger(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand handIn)
	{
		ItemStack it = pl.getItemInHand(handIn);
		if(!w.isClientSide)
		{
			EntityEgger egg = new EntityEgger(w, pl);
			egg.shootFromRotation(pl, pl.xRot, pl.yRot, 0F, 1.5F, 1F);
			w.addFreshEntity(egg);
		}
		it.shrink(1);
		return ActionResult.success(it);
	}
	
	@Override
	public ItemStack dispense(IBlockSource src, ItemStack it, IPosition pos, Direction enumfacing)
	{
		it.shrink(1);
		EntityEgger egg = new EntityEgger(src.getLevel(), pos.x(), pos.y(), pos.z());
		double x,y,z;
		x = enumfacing.getStepX();
		y = enumfacing.getStepY();
		z = enumfacing.getStepZ();
		x +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		y +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		z +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		egg.setDeltaMovement(x,y,z);
		src.getLevel().addFreshEntity(egg);
		return it;
	}

}
