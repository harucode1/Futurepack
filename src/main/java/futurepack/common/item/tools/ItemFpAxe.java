package futurepack.common.item.tools;

import net.minecraft.item.AxeItem;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;

public class ItemFpAxe extends AxeItem
{
	public ItemFpAxe(IItemTier tier, float eff, float attackSpeedIn, Item.Properties builder) 
	{
		super(tier, eff, attackSpeedIn, builder);
	}	
}
