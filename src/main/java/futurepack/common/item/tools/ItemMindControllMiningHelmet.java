package futurepack.common.item.tools;

import java.util.List;
import java.util.WeakHashMap;

import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import futurepack.common.FuturepackTags;
import futurepack.common.commands.OreSearcher;
import futurepack.depend.api.helper.HelperEntities;
import futurepack.depend.api.helper.HelperFluid;
import net.minecraft.block.BlockState;
import net.minecraft.client.settings.ParticleStatus;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.client.gui.widget.ModListWidget.ModEntry;


public class ItemMindControllMiningHelmet extends ArmorItem
{
	
    public ItemMindControllMiningHelmet(IArmorMaterial mat, EquipmentSlotType slots, Item.Properties builder)
	{
		super(mat, slots, builder);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type)
	{
		if(slot== EquipmentSlotType.CHEST||slot== EquipmentSlotType.FEET || slot== EquipmentSlotType.HEAD)
			return "futurepack:textures/models/armor/mindcontroll_mining_layer_1.png";
		else if(slot== EquipmentSlotType.LEGS)
			return "futurepack:textures/models/armor/mindcontroll_mining_layer_2.png";
		else
			return super.getArmorTexture(stack, entity, slot, type);
	}

	@Override
	public ActionResultType interactLivingEntity(ItemStack stack, PlayerEntity playerIn, LivingEntity target, Hand hand)
	{
		if(target.getItemBySlot(getSlot()).isEmpty())
		{
			target.setItemSlot(getSlot(), stack);
			return ActionResultType.CONSUME;
		}
		return super.interactLivingEntity(stack, playerIn, target, hand);
	}
	
	private static WeakHashMap<MobEntity, OreSearcher> map = new WeakHashMap<MobEntity, OreSearcher>();
	
	public static boolean onMobArmorTick(MobEntity mob)
	{
		World w = mob.getCommandSenderWorld();
		if(!w.isClientSide)
		{
			ItemStack helm = mob.getItemBySlot(EquipmentSlotType.HEAD);
			
			if(!helm.isEmpty() && helm.getItem().is(FuturepackTags.MINING_MINDCONTROL))
			{
				final OreSearcher search = map.computeIfAbsent(mob, m -> new OreSearcher(new BlockPos(m.blockPosition())) );
				search.maxrange = 16 * 5;
				
				if(mob.getPassengers().isEmpty())
				{
					List<ParentCoords> ores = search.getOres(w);
					
					if(ores.isEmpty())
					{
						map.remove(mob);
					}
					else
					{
						BlockPos orePos = ores.get(0);
						
						boolean air = false;
						for(Direction dir : FacingUtil.VALUES)
						{
							if(w.isEmptyBlock(orePos.relative(dir)))
							{
								air = true;
								break;
							}
						}
						
						if(air)
						{
							double d = 0;
							((ServerWorld)w).sendParticles(ParticleTypes.BARRIER, orePos.getX()+0.5, orePos.getY()+0.5, orePos.getZ()+0.5, 1, 0, 0, 0, 0);
							if((d = orePos.distSqr(mob.position(), true)) < 5)
							{
								BlockState state = w.getBlockState(orePos);
								if(state.isAir(w, orePos))
								{
									ores.remove(0);
								}
								else
								{	
									FallingBlockEntity block = new FallingBlockEntity(w, orePos.getX()+0.5, orePos.getY()+0.5, orePos.getZ()+0.5, state);
									w.addFreshEntity(block);
									block.tick();
									block.startRiding(mob, true);
								}
							}
							else
							{
								HelperEntities.navigateEntity(mob, orePos, false);
							}
						}
						else
						{
							ores.remove(0);
						}
					}
				}
				else
				{
					BlockPos start = search.getStartPos();
					
					if(start.distSqr(mob.position(), true) < 5)
					{
						//att start
						mob.getPassengers().stream().filter(p -> p instanceof FallingBlockEntity).forEach(p -> ((FallingBlockEntity)p).time += 100);
						
					}
					else
					{
						HelperEntities.navigateEntity(mob, start, false);
					}
					
				}
				
				return true;
			}
			
			

		}
		
		return false;
	}
	
	
}
