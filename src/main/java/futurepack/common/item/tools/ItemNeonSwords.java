package futurepack.common.item.tools;

import java.util.List;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import futurepack.api.interfaces.IItemNeon;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;

public class ItemNeonSwords extends Item implements IItemNeon
{
	private final float dmg;
	
	private final Multimap<Attribute, AttributeModifier> stats_dmg;
	private final Multimap<Attribute, AttributeModifier> stats_no_dmg;
	
	public ItemNeonSwords(float damage, Item.Properties props)
	{
		super(props);
		this.dmg = damage;
		stats_dmg = ImmutableMultimap.of(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", dmg, AttributeModifier.Operation.ADDITION),
				Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", +1.2, AttributeModifier.Operation.ADDITION));
		stats_no_dmg = ImmutableMultimap.of(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", dmg, AttributeModifier.Operation.ADDITION),
				Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", +1.2, AttributeModifier.Operation.ADDITION));
	}

	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public int getMaxNeon(ItemStack it)
	{
		return 800;
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlotType equipmentSlot, ItemStack it)
    {
        if (equipmentSlot == EquipmentSlotType.MAINHAND)
        {
           return getNeon(it) > 0 ? stats_dmg : stats_no_dmg;
        }

        return super.getAttributeModifiers(equipmentSlot, it);
    }
	
	@Override
	public boolean hurtEnemy(ItemStack stack, LivingEntity target, LivingEntity attacker)
	{
		if(getNeon(stack)>0)
			addNeon(stack, -1);
		
		if(this == ToolItems.sword_glowtite && target.isInvertedHealAndHarm())
		{
			target.setSecondsOnFire(10);
		}
		else if(this == ToolItems.sword_bioterium)
		{
			EffectInstance eff = new EffectInstance(Effects.POISON, 15*20 , 2);
			target.addEffect(eff);
		}
		return super.hurtEnemy(stack, target, attacker);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		tooltip.add(HelperItems.getTooltip(stack, this));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
	
	@Override
	public void fillItemCategory(ItemGroup group, NonNullList<ItemStack> items) 
	{
		if(this.allowdedIn(group))
		{
			ItemStack it = new ItemStack(this, 1);
			addNeon(it, getMaxNeon(it));
			items.add(it);
		}
	}
	
	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state)
    {
        Block block = state.getBlock();

        if (block == Blocks.COBWEB)
        {
            return 15.0F;
        }
        else
        {
            Material material = state.getMaterial();
            return material != Material.PLANT && material != Material.REPLACEABLE_PLANT && material != Material.CORAL && material != Material.LEAVES && material != Material.VEGETABLE ? 1.0F : 1.5F;
        }
    }
	
	@Override
    public boolean isCorrectToolForDrops(BlockState blockIn)
    {
        return blockIn.getBlock() == Blocks.COBWEB;
    }
}
