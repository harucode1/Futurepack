package futurepack.common.item.tools;

import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageAirFilledRoom;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.network.PacketDistributor;

public class ItemRoomAnalyzer extends Item
{
	private final static int cw = 3, cd = 3, ch = 3;
	
	public static byte[] getAirData(World w, BlockPos pos)
	{
		if(AtmosphereManager.hasWorldOxygen(w))
			return null;
		
		byte[] data = new byte[4096 * cw * cd * ch]; 
		int cx = (pos.getX()>>4) -1;
		int cz = (pos.getZ()>>4) -1;
		int cy = (pos.getY()>>4) -1;
		
		int sx = cx * 16;
		int sy = cy * 16;
		int sz = cz * 16;
		int ex = sx + cw * 16;
		int ey = sy + ch * 16;
		int ez = sz + cd * 16;
		
		for(int fx=sx;fx<ex;fx++)
		{
			for(int fz=sz;fz<ez;fz++)
			{
				Chunk c = w.getChunk(fx >> 4, fz >> 4);
				LazyOptional<IChunkAtmosphere> opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
				int x = fx, z = fz;
				opt.ifPresent(atm -> 
				{
					int maxair = atm.getMaxAir();
					
					for(int y=sy;y<ey;y++)
					{
						int air = atm.getAirAt(x&15,y&255,z&15);
						int index = (x-sx)*cd*ch*256 + (y-sy)*cd*16 + (z-sz);
						if(air==0)
						{
							data[index] = -128;
						}
						else
						{
							int airCeiled = (air * 255 / maxair);
							if(airCeiled<0)
								airCeiled = 0;
							byte b = (byte) (airCeiled - 128);
									
							data[index] = b;
						}
					}
				});
			}
		}
		
		return data;
	}
	
	public ItemRoomAnalyzer(Item.Properties props)
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand handIn)
	{
		ItemStack held = pl.getItemInHand(handIn);
		if(!w.isClientSide)
		{
			BlockPos pos = pl.blockPosition();
			byte[] data = getAirData(w,pos);
			
			if(data==null)
			{
				pl.sendMessage(new TranslationTextComponent("chat.escanner.athmosphere.breathable"), Util.NIL_UUID);
			}
			else
			{
				FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with( () -> (ServerPlayerEntity)pl), new MessageAirFilledRoom(pos, data, new Vector3i(cw,ch,cd)));
			}
		}
		pl.getCooldowns().addCooldown(this, 20 * 2);
		return new ActionResult<ItemStack>(ActionResultType.SUCCESS, held);
	}
}
