package futurepack.common.item.tools;

import java.util.List;

import futurepack.api.interfaces.tilentity.ITileHologramAble;
import futurepack.common.FPLog;
import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.NonNullList;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemHologramControler extends Item
{
	
	public ItemHologramControler(Item.Properties props)
	{
		super(props);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World w, List<ITextComponent> l, ITooltipFlag advanced)
	{
		l.add(new TranslationTextComponent(getDescriptionId()+".tooltip.sneak_click"));
		l.add(new TranslationTextComponent(getDescriptionId()+".tooltip.normal_click"));
//		l.add("sneak-click to select Block");
//		l.add("normal-click to set Hologram");
		super.appendHoverText(stack, w, l, advanced);
	}
	
	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		PlayerEntity pl = context.getPlayer();
		World w = context.getLevel();
		ItemStack it = context.getItemInHand();
		BlockPos pos = context.getClickedPos();
		
		if(!w.isClientSide)
		{
			BlockState state = w.getBlockState(pos);
//			state = state.getActualState(w, pos);
			
			if(pl.isShiftKeyDown())
			{
				if(state.getRenderShape()!= BlockRenderType.MODEL)
				{
					pl.sendMessage(new TranslationTextComponent("hologram.error.select.tileentity"), Util.NIL_UUID);
					return ActionResultType.FAIL;
				}
				HelperHologram.saveInItem(it, state);
				if(!it.hasTag())
				{
					FPLog.logger.fatal("[PANIC] Hologram item has no NBT after setting BlockStateData");
					pl.sendMessage(new StringTextComponent("An internal ERROR prevented saving the NBT."), Util.NIL_UUID);
				}
				else if(!it.getTag().contains("hologram"))
				{
					FPLog.logger.error("Cant add NBTTag 'BlockStateContainer' to Item");
					pl.sendMessage(new StringTextComponent("An internal ERROR prevented adding the BlockState to the Item. (Item has an NBT tag)"), Util.NIL_UUID);
				}
			}
			else
			{
				if(w.getBlockEntity(pos)==null)
				{
					pl.sendMessage(new TranslationTextComponent("hologram.error.set.notileentity"), Util.NIL_UUID);
					return ActionResultType.FAIL;
				}
				
				state = HelperHologram.loadFormItem(it);
				
				if(state == null)
				{
					pl.sendMessage(new TranslationTextComponent("hologram.error.set.nonbt"), Util.NIL_UUID);
					return ActionResultType.FAIL;
				}
				
				TileEntity t = w.getBlockEntity(pos);
				if(t instanceof ITileHologramAble)
				{
					((ITileHologramAble)t).setHologram(state);
					t.setChanged();
					w.sendBlockUpdated(pos, state, state, 2); //TODO: change back if now working -> w.markBlockRangeForRenderUpdate(pos.add(-1, -1, -1), pos.add(1,1,1));
				}
				else
				{
					pl.sendMessage(new TranslationTextComponent("hologram.error.set.notsupported"), Util.NIL_UUID);
					return ActionResultType.FAIL;
				}
			}
		}
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public void fillItemCategory(ItemGroup group, NonNullList<ItemStack> items) 
	{
		super.fillItemCategory(group, items);
		if(allowdedIn(group))
		{
			//FIXME: confi sub holograms are nolonger available
//			for(String s : FPConfig.CLIENT.creativeQuickHologram.get())
//			{
//				try
//				{
//					ItemStack item = new ItemStack(this, 1);
//					NBTTagCompound nbt = new NBTTagCompound();
//					NBTTagCompound state = JsonToNBT.getTagFromJson(s);
//					nbt.put("hologram", state);
//					item.setTag(nbt);
//					items.add(item);
//				}
//				catch(CommandSyntaxException e)
//				{
//					ItemStack it = new ItemStack(Blocks.STONE);
//					it.setDisplayName(new TextComponentString(s));
//					NBTTagList list = new NBTTagList();
//					list.add(new NBTTagString(e.toString()));
//					it.getOrCreateChildTag("display").put("Lore", list);
//					items.add(it);
//				}
//			}
		}
	}
}
