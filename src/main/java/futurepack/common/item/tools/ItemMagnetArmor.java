package futurepack.common.item.tools;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemMagnetArmor extends ArmorItem
{

	public ItemMagnetArmor(IArmorMaterial mat, EquipmentSlotType slots, Item.Properties builder)
	{
		super(mat, slots, builder);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type)
	{
		if(slot== EquipmentSlotType.CHEST||slot== EquipmentSlotType.FEET || slot== EquipmentSlotType.HEAD)
			return "futurepack:textures/models/armor/magnet_layer_1.png";
		else if(slot== EquipmentSlotType.LEGS)
			return "futurepack:textures/models/armor/magnet_layer_2.png";
		else
			return super.getArmorTexture(stack, entity, slot, type);
	}
	
	@Override
	public void onArmorTick(ItemStack it, World w, PlayerEntity pl)
	{
		//if(!w.isRemote)
		{
			int range = 0;
			for(ItemStack its : pl.inventory.armor)
			{
				if(its!=null && its.getItem() instanceof ItemMagnetArmor)
				{
					range++;
				}
			}
			List<ItemEntity> list = w.getEntitiesOfClass(ItemEntity.class, pl.getBoundingBox().inflate(range, range, range));
			for(ItemEntity item : list)
			{
				double d3 = (pl.getX()) - item.getX() ;
				double d4 = (pl.getY()) - item.getY() ;
				double d5 = (pl.getZ()) - item.getZ() ;
				
				double dis = Math.sqrt(pl.distanceToSqr(item.getX(), item.getY(), item.getZ()));
				
				d3 = d3/dis ;
				d4 = d4/dis ;
				d5 = d5/dis ;
				
				//FIXME: Is this nessesary? item.setAgeToCreativeDespawnTime();
				if(w.isClientSide)
				{	
					item.setDeltaMovement(d3, d4, d5);
				}
				else
				{
					item.setPos(pl.getX(), pl.getY(), pl.getZ());
				}
			}
		}
	}
}
