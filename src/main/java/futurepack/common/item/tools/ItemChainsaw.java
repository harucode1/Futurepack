package futurepack.common.item.tools;

import futurepack.world.dimensions.TreeUtils;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemChainsaw extends ItemPoweredMineToolBase
{

	public ItemChainsaw(Item.Properties props) 
	{
		super(props);
	}
	
	@Override
	public boolean isMineable(BlockState bs)
	{
		return bs.getMaterial()==Material.WOOD;
	}
    
    @Override
    public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, PlayerEntity player)
    {
    	World w = player.getCommandSenderWorld();
    	if(!w.isClientSide)
    	{
	    	if(getNeon(itemstack) > 0)
			{
				BlockPos log = TreeUtils.getFarWoodInHeight(w, pos);
				if(pos.equals(log))
				{
					TreeUtils.selectTree(w, log, Direction.fromYRot(player.getYHeadRot()).getOpposite());
					return true;
				}
			}
    	}
    	return super.onBlockStartBreak(itemstack, pos, player);
    }
}
