package futurepack.common.item.tools;

import java.util.Optional;

import futurepack.common.FPEntitys;
import futurepack.common.entity.throwable.EntityGrenadeBase;
import futurepack.common.item.ItemGreandeBase;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IPosition;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemEntityEggerFull extends ItemGreandeBase 
{

	public ItemEntityEggerFull(Properties properties) 
	{
		super(properties, FPEntitys.GRENADE_ENTITY_EGGER::create);
	}

	@Override
	public boolean isFoil(ItemStack it)
	{
		return true;
	}
	
	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		World w = context.getLevel();
		
		if(!w.isClientSide)
		{
			BlockPos pos = context.getClickedPos();
			Direction face = context.getClickedFace();
			PlayerEntity pl = context.getPlayer();
			ItemStack it = context.getItemInHand();
			
			Optional<Entity> opt = EntityType.create(it.getTag(), w);
			if(opt.isPresent())
			{
				Entity e = opt.orElseThrow(NullPointerException::new);
				e.setPos(pos.relative(face).getX()+0.5,pos.relative(face).getY()+0.1,pos.relative(face).getZ()+0.5);
				e.setUUID(MathHelper.createInsecureUUID(w.random));
				w.addFreshEntity(e);
				
				pl.setItemInHand( pl.getMainHandItem() == it ? Hand.MAIN_HAND: Hand.OFF_HAND, ItemStack.EMPTY);
				if(!pl.isCreative())
				{
					it = getEmpty();
				}

				ItemEntity item = new ItemEntity(w, pl.getX(), pl.getY(), pl.getZ(), it);
				item.setPickUpDelay(5);
				item.setDeltaMovement(Vector3d.ZERO);
				w.addFreshEntity(item);
			}
			return ActionResultType.SUCCESS;
		}
		return ActionResultType.SUCCESS;
	}
	
	private ItemStack getEmpty()
	{
		return new ItemStack(ToolItems.entity_egger);
	}
	
	@Override
	public ItemStack dispense(IBlockSource src, ItemStack it, IPosition pos, Direction enumfacing)
	{
		ItemStack item = it.split(1);
		
		Optional<Entity> opt = EntityType.create(it.getTag(), src.getLevel());
		opt.ifPresent( e -> {
			e.setPos(pos.x(), pos.y(), pos.z());
			src.getLevel().addFreshEntity(e);
			ItemEntity itemegg = new ItemEntity(src.getLevel(), pos.x(), pos.y(), pos.z(), getEmpty());
			src.getLevel().addFreshEntity(itemegg);
		});
		
//		ArrayList<SlotContent> list = new ArrayList<HelperInventory.SlotContent>();
//		list.add(new SlotContent(null, 0, itemegg.getEntityItem(),itemegg));
//		List<SlotContent> done = HelperInventory.putItemsOld(src.getWorld(), src.getBlockPos().up(), EnumFacing.DOWN, list);
//		for(SlotContent slot : done)
//		{
//			slot.remove();
//		}
		return it;
	}
	
	@Override
	protected EntityGrenadeBase getGrande(World w, ItemStack it, PlayerEntity pl)
	{
		EntityGrenadeBase base = new EntityGrenadeBase.EnityEggerFullGranade(w, pl, it.getTag());
		return base;
	}
}
