package futurepack.common.item.tools;

import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.PickaxeItem;

public class ItemFpPickaxe extends PickaxeItem
{
	public ItemFpPickaxe(IItemTier tier, int attackDamageIn, float attackSpeedIn, Item.Properties builder) 
	{
		super(tier, attackDamageIn, attackSpeedIn, builder);
	}	
}
