package futurepack.common.item.tools;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPSounds;
import futurepack.common.entity.throwable.EntityLaserProjectile;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class ItemLaserRiffle extends Item implements IItemNeon
{
	public ItemLaserRiffle(Item.Properties props)
	{
		super(props);
//		setMaxStackSize(1);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(getNeon(it) >= 18)
		{
			pl.getCooldowns().addCooldown(this, 18);
			addNeon(it, -18);	
			
			if(!w.isClientSide)
			{
				EntityLaserProjectile laser = new EntityLaserProjectile(w, pl);
				laser.shootFromRotation(pl, pl.xRot, pl.yRot, 0F, 3F, 0.5F);
				w.addFreshEntity(laser);
			}
			
			w.playSound(pl, pl.blockPosition(), FPSounds.LOAD, SoundCategory.NEUTRAL, 0.5F, 3F);
			return ActionResult.success(it);
		}
		return ActionResult.pass(it);
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}


	@Override
	public int getMaxNeon(ItemStack it)
	{
		return 900;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World w, List<ITextComponent> tooltip, ITooltipFlag advanced)
	{
		tooltip.add(HelperItems.getTooltip(stack, this));
		if(getNeon(stack) < 18)
			tooltip.add(new TranslationTextComponent("tooltip.items.noenergy").setStyle(Style.EMPTY.withColor(TextFormatting.DARK_RED)));
		
		super.appendHoverText(stack, w, tooltip, advanced);
	}
}
