package futurepack.common.item.tools;

import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.entity.throwable.EntityHook;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;

public class ItemGrablingHook extends Item implements IItemNeon
{
	private Map<LivingEntity, EntityHook> Client_map = new WeakHashMap<LivingEntity, EntityHook>();
	private Map<LivingEntity, EntityHook> Server_map = new WeakHashMap<LivingEntity, EntityHook>();
	//IIcon inUse;
	
	public void clean()
	{
		Client_map.clear();
		Server_map.clear();
	}
	
	public ItemGrablingHook(Item.Properties props) 
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
//		setMaxStackSize(1);
//		setFull3D();
	}
	
	@Override
	public ActionResult<ItemStack> use(World w, PlayerEntity pl, Hand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(getNeon(it)>1)
		{
			if(!map(w).containsKey(pl))
			{
				if(!w.isClientSide)
				{
					w.playSound(null, pl.blockPosition(), SoundEvents.ARROW_SHOOT, SoundCategory.NEUTRAL, 0.5F, 0.4F / (w.random.nextFloat() * 0.4F + 0.8F));
						
					EntityHook h = new EntityHook(w,pl);
					h.shootFromRotation(pl, pl.xRot, pl.yHeadRot, 0F, 1.6F, 1F);
					map(w).put(pl, h);
					w.addFreshEntity(h);
				}				
			}
			else
			{
				EntityHook hook = map(w).get(pl);
				hook.remove();
				//it.damageItem(1, pl);
				addNeon(it, -1);
				map(w).remove(pl);		
			}
		}
		return ActionResult.success(it);
	}
	
	public Map<LivingEntity, EntityHook> map(World w)
	{
		return map(w.isClientSide);
	}  
	
	public Map<LivingEntity, EntityHook> map(boolean remote)
	{
		return remote ? Client_map : Server_map;
	}  

	@Override
	public int getMaxNeon(ItemStack it) 
	{
		return 250;
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public void appendHoverText(ItemStack it, World w, List<ITextComponent> l, ITooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
}
