package futurepack.common.item.tools;

import java.lang.reflect.Field;

import futurepack.common.DirtyHacks;
import futurepack.common.item.ResourceItems;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.item.FishingRodItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

public class ItemFpFishingRod extends FishingRodItem
{
	public ItemFpFishingRod(Item.Properties props)
	{
		super(props);
	
//		this.setMaxDamage(512);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public int getEnchantmentValue()
	{
		return 4;
	}
	
	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) 
	{
		if(entityIn  instanceof PlayerEntity)
		{
			PlayerEntity pl = (PlayerEntity) entityIn;
			Hand held = null;
			if(stack == pl.getMainHandItem())
			{
				held = Hand.MAIN_HAND;
				
			}
			else if(stack == pl.getOffhandItem())
			{
				held = Hand.OFF_HAND;
			}
			else
			{
				return;
			}
				
			if (pl.fishing != null)
			{
				FishingBobberEntity hook = pl.fishing;
				boolean fish = isFishHocked(hook);
				
				if(fish)
				{
					int i = pl.fishing.retrieve(stack);
					stack.hurtAndBreak(i, pl, breaker -> {});
					pl.swing(held);
					worldIn.playSound((PlayerEntity)null, pl.getX(), pl.getY(), pl.getZ(), SoundEvents.FISHING_BOBBER_RETRIEVE, SoundCategory.NEUTRAL, 1.0F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
				}
			}
		}
	}
	
	@Override
	public boolean isValidRepairItem(ItemStack toRepair, ItemStack repair)
	{
		ItemStack mat = new ItemStack(ResourceItems.composite_metal);
		
		if(!mat.isEmpty() && repair.sameItem(mat))
			return true;
		return super.isValidRepairItem(toRepair, repair);
	}
	
	private boolean isFishHocked(FishingBobberEntity hook)
	{		
		return hook.nibble > 0 && hook.nibble < 20;
	}
	
}
