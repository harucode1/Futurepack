package futurepack.common.item;

import futurepack.api.Constants;
import futurepack.common.FPEntitys;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class SpawnEggItems 
{
	public static final int MENELAUS = 0xdbb615;
	
	public static final int FUTUREPACK = 0x82dcdc;
	
	
	public static Item.Properties eggs = new Item.Properties().stacksTo(64).tab(ItemGroup.TAB_MISC);
	
	public static final Item CYBER_ZOMBIE = new SpawnEggItem(FPEntitys.CYBER_ZOMBIE, 0x799C65, FUTUREPACK, eggs);
	public static final Item GEHUF = new SpawnEggItem(FPEntitys.GEHUF, 4470310, MENELAUS, eggs);
	public static final Item WOLBA = new SpawnEggItem(FPEntitys.WOLBA, 15198183, MENELAUS, eggs);
	public static final Item EVIL_ROBOT = new SpawnEggItem(FPEntitys.EVIL_ROBOT, 0x858585, FUTUREPACK, eggs);
	public static final Item CRAWLER = new SpawnEggItem(FPEntitys.CRAWLER, 0x726852, MENELAUS, eggs);
	public static final Item HEULER = new SpawnEggItem(FPEntitys.HEULER, 0xc9ff4d, MENELAUS, eggs);
	public static final Item JAWAUL = new SpawnEggItem(FPEntitys.JAWAUL, 0xa55600, MENELAUS, eggs);
	public static final Item DUNGEON_SPIDER = new SpawnEggItem(FPEntitys.DUNGEON_SPIDER, 0x7300a5, FUTUREPACK, eggs);
	
	
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		Item[] items = new Item[] {CYBER_ZOMBIE, GEHUF, WOLBA, EVIL_ROBOT, CRAWLER, HEULER, JAWAUL, DUNGEON_SPIDER};
		
		for(Item i : items)
		{
			i.setRegistryName(Constants.MOD_ID, "spawn_egg_" + ((SpawnEggItem)i).getType(null).getRegistryName().getPath());
		}
		
		r.registerAll(items);
	}	
}
