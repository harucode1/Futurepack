package futurepack.common.item;

import java.util.function.BiFunction;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemThrowable extends Item
{
	private final BiFunction<World, PlayerEntity, ThrowableEntity> throwable;

	public ItemThrowable(Properties properties, BiFunction<World, PlayerEntity, ThrowableEntity> func)
	{
		super(properties);
		throwable = func;
	}
	
	@Override
	public ActionResult<ItemStack> use(World worldIn, PlayerEntity playerIn, Hand handIn)
	{
		if(throwable==null)
			return super.use(worldIn, playerIn, handIn);
		
		ItemStack itemstack = playerIn.getItemInHand(handIn);
		if (!playerIn.abilities.instabuild) 
		{
			itemstack.shrink(1);
		}

		worldIn.playSound((PlayerEntity)null, playerIn.getX(), playerIn.getY(), playerIn.getZ(), SoundEvents.SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
		if (!worldIn.isClientSide) 
		{
			ThrowableEntity e = throwable.apply(worldIn, playerIn);
			e.shootFromRotation(playerIn, playerIn.xRot, playerIn.yRot, 0.0F, 1.5F, 1.0F);
			worldIn.addFreshEntity(e);
		}

		playerIn.awardStat(Stats.ITEM_USED.get(this));
		return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
	}

}
