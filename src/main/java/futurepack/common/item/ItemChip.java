package futurepack.common.item;

import java.util.List;

import futurepack.api.interfaces.IItemWithRandom;
import futurepack.common.modification.EnumChipType;
import futurepack.common.modification.IPartChip;
import futurepack.common.modification.thermodynamic.TemperatureManager;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemChip extends Item implements IItemWithRandom
{
//	static String[] iconname = new String[]{"LogicChip", "KIChip", "LogistcChip", "NavigationChip", "NetworkChip", "ProduktionChip", "RedstoneChip", "SuportChip", "TacticChip", "UltimateChip","DamageControlChip"};
//	public static boolean[] bigchip = new boolean[]{false, true, false, false, true, false, true, true, false, true, false};
//	//static IIcon[] icons = new IIcon[iconname.length];
//	
//	public static final int LogicChip = 0;
//	public static final int KIChip = 1;
//	public static final int LogisticChip = 2;
//	public static final int NavigationChip = 3;
//	public static final int NetworkChip = 4;
//	public static final int ProduktionChip = 5;
//	public static final int RedstoneChip = 6;
//	public static final int SupportChip = 7;
//	public static final int TacticChip = 8;
//	public static final int UltimateChip = 9;
//	public static final int DamageControlChip = 10;
//	
//	static
//	{
//		Class c = ItemChip.class;
//		try
//		{
//			Field[] fields = c.getFields();
//			for(int i=0;i<fields.length;i++)
//			{
//				if(fields[i].getType()==int.class)
//				{
//					String name = "chip." + fields[i].getName();
//					int val = (Integer) fields[i].get(null); //its static
//					HelperJSON.addVar(name, val);
//				}
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//	
	private final EnumChipType type;
	private final boolean bigChip;
	
	public ItemChip(Properties prop, EnumChipType type, boolean bigChip) 
	{
		super(prop);
//		this.setCreativeTab(FPMain.tab_items);
//		this.setHasSubtypes(true);
		this.type = type;
		this.bigChip = bigChip;
	}
	
	public static IPartChip getChip(ItemStack it)
	{
		if(it != null && it.getItem() instanceof ItemChip)
		{
			final ItemChip chip = (ItemChip) it.getItem();
					
			if(!it.hasTag())
				it.setTag(new CompoundNBT());
			final float f = chip.getChipPower(it);
			final float temp = TemperatureManager.getTemp(it);
			
			return new IPartChip()
			{
				@Override
				public int getCorePower()
				{
					return chip.getNeededCore(it);
				}

				@Override
				public float getChipPower(EnumChipType type)
				{
					if(type == EnumChipType.LOGIC && chip.bigChip)
						return 1.0F;
					else if(type == chip.type)
						return f;
					return 0F;
				}

				@Override
				public float getMaximumTemperature() 
				{
					return temp;
				}
				
			};
		}
		return null;
	}
	
	public int getNeededCore(ItemStack it)
	{
		return bigChip ? 2 : 1;
	}

	public float getChipPower(ItemStack it)
	{
		if(!it.hasTag())
			it.setTag(new CompoundNBT());
			
		if(it.getTag().contains("power"))
		{
			return it.getTag().getFloat("power");
		}
		else
		{
			it.getTag().putFloat("power", 1.0F);
			return 1F;
		}
	}
	@Override
	public void appendHoverText(ItemStack it, World w, List<ITextComponent> l, ITooltipFlag par4) 
	{
		float f = getChipPower(it);
		l.add(new TranslationTextComponent("tooltip.futurepack.item.chip_power", f));
		l.add(new TranslationTextComponent("tooltip.futurepack.item.core_power", getNeededCore(it)));

		if(type == EnumChipType.DAMAGE_CONTROL)
		{
			l.add(new TranslationTextComponent("tooltip.futurepack.item.shutdown_temp", f*100));
		}
		l.add(new TranslationTextComponent("tooltip.futurepack.item.max_temp", TemperatureManager.getTemp(it)));
		super.appendHoverText(it, w, l, par4);
	}

	@Override
	public void setRandomNBT(ItemStack it, int random) 
	{
		it.setTag(new CompoundNBT());
		it.getTag().putFloat("power", (random*0.2F)+1);
	}
	
//	@Override
//	public int getMaxMetas() 
//	{
//		return iconname.length;
//	}
//	@Override
//	public String getMetaName(int meta) 
//	{
//		return iconname[meta];
//	}
	
	public static ItemStack getFromToasted(ItemStack it)
	{
		if(!it.isEmpty() && it.getItem() == ComputerItems.toasted_chip && it.hasTag())
		{
			CompoundNBT nbt = it.getTag();
			return ItemStack.of(nbt);
		}
		return null;
	}
	
	
}
