package futurepack.common.item;

import futurepack.api.Constants;
import futurepack.common.FuturepackMain;
import net.minecraft.item.Item;
import net.minecraft.item.Rarity;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class CraftingItems 
{
	public static final Item.Properties crafting_64 = new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_items);
	public static final Item.Properties crafting_16 = new Item.Properties().stacksTo(16).tab(FuturepackMain.tab_items);
	public static final Item.Properties mainboard = new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON);
	public static final Item.Properties crafting_5 = new Item.Properties().stacksTo(5).tab(FuturepackMain.tab_items);
	public static final Item.Properties crafting_1 = new Item.Properties().stacksTo(1).tab(FuturepackMain.tab_items);
	
	public static final Item iron_stick = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "iron_stick");
	public static final Item drone_engine = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "drone_engine");
	public static final Item astrofood_empty = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "astrofood_empty");
	public static final Item laserdiode = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "laserdiode");
	public static final Item fragment_core = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "fragment_core");
	public static final Item fragment_ram = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "fragment_ram");
	public static final Item fuel_rods = new Item(crafting_16).setRegistryName(Constants.MOD_ID, "fuel_rods");
	public static final Item fibers = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "fibers");
	public static final Item sword_handle = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "sword_handle");
	public static final Item wood_chips = new ItemBurnable(crafting_64, 200).setRegistryName(Constants.MOD_ID, "wood_chips");
	
	public static final Item dungeon_key_0 = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "dungeon_key_0");
	public static final Item dungeon_key_1 = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "dungeon_key_1");
	public static final Item dungeon_key_2 = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "dungeon_key_2");
	public static final Item dungeon_key_3 = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "dungeon_key_3");
	public static final Item topinambur_flower = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "topinambur_flower");
	public static final Item mendel_flower = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "mendel_flower");
	
	public static final Item maschineboard = new ItemMaschineBoard(mainboard).setRegistryName(Constants.MOD_ID, "maschineboard");
	public static final Item double_maschineboard = new ItemMaschineBoard(mainboard).setRegistryName(Constants.MOD_ID, "double_maschineboard");
	
	public static final Item chemicals_a = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "chemicals_a");
	public static final Item chemicals_b = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "chemicals_b");
	public static final Item chemicals_c = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "chemicals_c");
	public static final Item grenade_empty = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "grenade_empty");
	public static final Item item_filter = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "item_filter");
	
	public static final Item upgrade_range = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "upgrade_range");
	public static final Item upgrade_luck = new Item(crafting_5).setRegistryName(Constants.MOD_ID, "upgrade_luck");
	public static final Item upgrade_silktouch = new Item(crafting_1).setRegistryName(Constants.MOD_ID, "upgrade_silktouch");
	
	public static final Item tin_graphene_katalysator = new Item(crafting_64).setRegistryName(Constants.MOD_ID, "tin_graphene_katalysator");
	
	
	public static void register(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.registerAll(iron_stick, drone_engine, astrofood_empty, laserdiode, fragment_core, fragment_ram, fuel_rods, fibers, sword_handle, wood_chips);
		r.registerAll(dungeon_key_0, dungeon_key_1, dungeon_key_2, dungeon_key_3, topinambur_flower, mendel_flower);
		r.registerAll(double_maschineboard, maschineboard);
		r.registerAll(chemicals_a, chemicals_b, chemicals_c, grenade_empty, item_filter);
		r.registerAll(upgrade_range, upgrade_luck, upgrade_silktouch, tin_graphene_katalysator);
	}	
}
