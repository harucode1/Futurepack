package futurepack.common.item;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;


import net.minecraft.item.Item.Properties;

public class ItemContainedFood extends Item
{
	private final ItemStack remaining;
	private final int use_duration;
	
	public ItemContainedFood(Properties properties, ItemStack remain, int use_duration) 
	{
		super(properties);
		this.remaining = remain;
		this.use_duration = use_duration;
	}

	public ItemContainedFood(Properties properties, ItemStack remain) 
	{
		this(properties, remain, -1);
	}
	
	@Override
	public int getUseDuration(ItemStack stack) 
	{
		if(use_duration == -1)
			return super.getUseDuration(stack);
		
		return use_duration;
	}
	
	@Override
	public ItemStack finishUsingItem(ItemStack stack, World worldIn, LivingEntity entityLiving)
	{
		ItemStack remains = remaining;
		
		if(remains == null)
			return super.finishUsingItem(stack, worldIn, entityLiving);
	    	
	    	
		super.finishUsingItem(stack.copy(), worldIn, entityLiving);

		if(entityLiving instanceof PlayerEntity)
		{
			if(!((PlayerEntity)entityLiving).addItem(remains.copy()))
			{
				if(stack.getCount() <= 1)
				{
					return remains.copy();
	        		}
				else
				{
					((PlayerEntity)entityLiving).drop(remains.copy(), true);
				}
			}
		}
		stack.shrink(1);
		return stack;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add(new TranslationTextComponent(getDescriptionId() + ".tooltip"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
}
