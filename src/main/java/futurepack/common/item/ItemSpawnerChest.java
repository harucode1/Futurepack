package futurepack.common.item;

import futurepack.common.FPConfig;
import futurepack.depend.api.helper.HelperEntities;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemSpawnerChest extends Item
{

	public ItemSpawnerChest(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public ActionResultType useOn(ItemUseContext context)
	{
		World w = context.getLevel();
		if(!w.isClientSide)
		{
			ItemStack it = context.getItemInHand();
			BlockPos pos = context.getClickedPos();
			boolean b;
			if(it.getTagElement("spawner")!=null)
			{
				b = placeDownSpawner(w, pos.relative(context.getClickedFace()), it);
				if(b)
				{
					it.removeTagKey("spawner");
				}
			}
			else
			{
				b = pickUpSpawner(w, pos, it);
			}
			return b? ActionResultType.SUCCESS : ActionResultType.PASS;
		}
			
		return super.useOn(context);
	}
	
 

	private boolean pickUpSpawner(World w, BlockPos pos, ItemStack it)
	{
		if(FPConfig.SERVER.disableSpawnerChest.get())
			return false;
		
		if(w.getBlockState(pos).getBlock() == Blocks.SPAWNER)
		{
			TileEntity tile = w.getBlockEntity(pos);
			if(tile!=null)
			{
				CompoundNBT nbt = it.getOrCreateTagElement("spawner");
				tile.save(nbt);
				HelperEntities.disableItemSpawn();
				w.removeBlock(pos, false);
				HelperEntities.enableItemSpawn();
				return true;
			}
		}
		return false;
	}
	
	private boolean placeDownSpawner(World w, BlockPos pos, ItemStack it)
	{
		if(w.getBlockState(pos).isAir(w, pos))
		{
			CompoundNBT nbt = it.getTagElement("spawner");
			if(nbt!=null)
			{
				w.setBlockAndUpdate(pos, Blocks.SPAWNER.defaultBlockState());
				TileEntity tile = TileEntity.loadStatic(Blocks.SPAWNER.defaultBlockState(), nbt);
				w.setBlockEntity(pos, tile);
				return true;
			}
		}
		return false;
	}
}
