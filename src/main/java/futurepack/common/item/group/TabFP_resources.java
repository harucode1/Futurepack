package futurepack.common.item.group;

import futurepack.common.item.ResourceItems;
import net.minecraft.item.ItemStack;

public class TabFP_resources extends TabFB_Base
{

	public TabFP_resources(String par2Str) 
	{
		super(par2Str);
	}

	@Override
	public ItemStack makeIcon() 
	{
		return new ItemStack(ResourceItems.composite_metal);
	}

}
