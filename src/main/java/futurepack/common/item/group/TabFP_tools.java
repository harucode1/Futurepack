package futurepack.common.item.group;

import futurepack.common.item.tools.ToolItems;
import net.minecraft.item.ItemStack;

public class TabFP_tools extends TabFB_Base 
{

	public TabFP_tools(String label) 
	{
		super(label);
	}

	@Override
	public ItemStack makeIcon()
	{
		return new ItemStack(ToolItems.escanner);
	}

}
