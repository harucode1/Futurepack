package futurepack.common.item;

import java.util.List;
import java.util.function.Function;

import futurepack.api.interfaces.IGranadleLauncherAmmo;
import futurepack.common.entity.throwable.EntityGrenadeBase;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IPosition;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ItemGreandeBase extends ItemDispensable implements IGranadleLauncherAmmo
{
	private final Function<World, EntityGrenadeBase> constructor;
	
	public ItemGreandeBase(Properties properties, Function<World, EntityGrenadeBase> constructor) 
	{
		super(properties);
		this.constructor = constructor;
	}

	@Override
	public ItemStack dispense(IBlockSource src, ItemStack it, IPosition pos, Direction enumfacing)
	{
		ItemStack item = it.split(1);
		
		int size = 1;
		if(item.hasTag() && item.getTag().contains("size"))
		{
			size = item.getTag().getInt("size");
		} 
		
		EntityGrenadeBase grenade = constructor.apply(src.getLevel());
		grenade.setSize(size);

		grenade.setPos(src.x()+enumfacing.getStepX(), src.y()+enumfacing.getStepY(), src.z()+enumfacing.getStepZ());				
		
		double x, y, z;
		x = enumfacing.getStepX();
		y = enumfacing.getStepY();
		z = enumfacing.getStepZ();
		x +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		y +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		z +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		grenade.setDeltaMovement(x, y, z);
		
		src.getLevel().addFreshEntity(grenade);

		return it;
	}

	@Override
	public boolean isGranade(ItemStack it) 
	{
		return true;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.futurepack.item.grenade.power", stack.getOrCreateTag().getInt("size")));
		
	}

	@Override
	public Entity createGrenade(World w, ItemStack it, PlayerEntity pl, float strength)
	{
		if(!it.isEmpty())
		{
			EntityGrenadeBase base = getGrande(w, it, pl);
			base.shootFromRotation(pl, pl.xRot, pl.yRot, 0.0F, 4.5F *strength, 1.0F);
			return base;
		}
		return null;
	}

	
	protected EntityGrenadeBase getGrande(World w, ItemStack it, PlayerEntity pl)
	{
		EntityGrenadeBase base = constructor.apply(w);
		base.setThrower(pl);
		
		int size = 1;
		if(it.hasTag() && it.getTag().contains("size"))
		{
			size = it.getTag().getInt("size");
		}
		base.setSize(size);
		return base;
	}
}
