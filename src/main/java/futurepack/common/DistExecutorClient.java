package futurepack.common;

import javax.annotation.Nullable;

import futurepack.api.Constants;
import futurepack.client.ClientEvents;
import futurepack.client.color.ItemBlockColoring;
import futurepack.client.render.RenderDefaultGrenade;
import futurepack.client.render.RenderLaserBowArrow;
import futurepack.client.render.RenderMonitor;
import futurepack.client.render.RenderRocket;
import futurepack.client.render.block.*;
import futurepack.client.render.dimension.DimensionRenderTypeMenelaus;
import futurepack.client.render.dimension.DimensionRenderTypeTyros;
import futurepack.client.render.dimension.DimensionRenderTypeAsteroidBelt;
import futurepack.client.render.dimension.DimensionRenderTypeEnvia;
import futurepack.client.render.entity.RenderAlphaJawaul;
import futurepack.client.render.entity.RenderCrawler;
import futurepack.client.render.entity.RenderCyberZombie;
import futurepack.client.render.entity.RenderDungeonSpider;
import futurepack.client.render.entity.RenderEvilRobot;
import futurepack.client.render.entity.RenderForceField;
import futurepack.client.render.entity.RenderForstmaster;
import futurepack.client.render.entity.RenderGehuf;
import futurepack.client.render.entity.RenderHeuler;
import futurepack.client.render.entity.RenderHook;
import futurepack.client.render.entity.RenderJawaul;
import futurepack.client.render.entity.RenderLaserProjektile;
import futurepack.client.render.entity.RenderMiner;
import futurepack.client.render.entity.RenderMonoCart;
import futurepack.client.render.entity.RenderWolba;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.plants.MenelausMushroom;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.throwable.EntityGrenadeBase;
import futurepack.common.fluids.FPFluids;
import futurepack.common.gui.GuiFPLoading;
import futurepack.common.item.tools.ItemGrablingHook;
import futurepack.common.item.tools.ItemLogisticEditor;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.recipes.RecipeManagerSyncer;
import futurepack.common.sync.KeyBindAuto;
import futurepack.extensions.albedo.AlbedoMain;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.MainMenuScreen;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.entity.SpriteRenderer;
import net.minecraft.client.resources.ReloadListener;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.client.world.DimensionRenderInfo;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRendersAsItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.ItemModelsProperties;
import net.minecraft.item.ItemStack;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IReloadableResourceManager;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;

public class DistExecutorClient extends DistExecutorBase 
{

	@Override
	public void earlySetup() 
	{
		MinecraftForge.EVENT_BUS.register(ClientEvents.INSTANCE);
	}
	
	@Override
	public void postInitClient(FMLClientSetupEvent event)
	{
		bindTileRenderers();
		binEntityRenderers();
		bindItemProperties();
		registerDimensionRenderers();
		
		AlbedoMain.INSTANCE.init();
		
		IResourceManager mng = Minecraft.getInstance().getResourceManager();
		if(mng instanceof IReloadableResourceManager)
		{
			((IReloadableResourceManager)mng).registerReloadListener(new ReloadListener<RecipeManagerSyncer>()
			{

				@Override
				protected RecipeManagerSyncer prepare(IResourceManager resourceManagerIn, IProfiler profilerIn) 
				{
					return RecipeManagerSyncer.INSTANCE;
				}

				@Override
				protected void apply(RecipeManagerSyncer splashList, IResourceManager resourceManagerIn, IProfiler profilerIn) 
				{
					splashList.onClientRecipeReload();
				}
			});
		}
		
		RenderTypeLookup.setRenderLayer(InventoryBlocks.t0_generator, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.crystal_neon, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.crystal_alutin, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.crystal_bioterium, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.crystal_glowtite, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.crystal_retium, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.grass_t, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(MiscBlocks.extern_cooler, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(ModifiableBlocks.external_core, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(InventoryBlocks.flash_server_b, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(InventoryBlocks.flash_server_g, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(InventoryBlocks.flash_server_w, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(ModifiableBlocks.fluid_pump, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.fluid_tank, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.fluid_tank_mk2, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.fluid_tank_mk3, RenderType.cutoutMipped());
		RenderTypeLookup.setRenderLayer(MiscBlocks.force_field, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_white, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_orange, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_magenta, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_light_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_yellow, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_lime, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_pink, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_light_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_cyan, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_purple, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_brown, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_green, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_red, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_slab_black, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.metal_gitter_slab, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_white, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_orange, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_magenta, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_light_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_yellow, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_lime, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_pink, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_light_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_cyan, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_purple, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_brown, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_green, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_red, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_stair_black, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.metal_stair_gitter, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_white, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_orange, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_magenta, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_light_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_yellow, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_lime, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pink, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_light_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_cyan, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_purple, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_brown, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_green, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_red, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_black, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.metal_block_gitter, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_white, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_orange, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_magenta, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_light_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_yellow, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_lime, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_pink, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_light_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_cyan, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_purple, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_brown, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_green, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_red, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_gitter_pane_black, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.metal_gitter_pane, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_white, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_orange, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_magenta, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_light_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_yellow, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_lime, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_pink, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_light_gray, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_cyan, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_purple, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_blue, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_brown, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_green, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_red, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.color_glass_black, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.metal_glass, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(DecoBlocks.ironladder, RenderType.cutout());

		RenderTypeLookup.setRenderLayer(LogisticBlocks.monorail, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.monorail_booster, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.monorail_charger, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.monorail_detector, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.monorail_lift, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.monorail_oneway, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.monorail_station, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.monorail_waypoint, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.plasma_core_t1, RenderType.solid());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.plasma_pipe_t1, RenderType.solid());

		RenderTypeLookup.setRenderLayer(TerrainBlocks.prismide, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.blueshroom, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.bubbleshroom, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.hyticus, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.peakhead, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(TerrainBlocks.door_tyros, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.door_menelaus_mushroom, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.door_tyros, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.door_palirie, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(TerrainBlocks.trapdoor_menelaus_mushroom, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.trapdoor_palirie, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(TerrainBlocks.trapdoor_tyros, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(DecoBlocks.composite_door, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(DecoBlocks.glass_door, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.glass_trapdoor, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(DecoBlocks.quartz_glass, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(PlantBlocks.mendel_berry, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(PlantBlocks.erse, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(PlantBlocks.glowmelo, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(PlantBlocks.leaves_tyros, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(PlantBlocks.oxades, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(PlantBlocks.sapling_mushroom, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(PlantBlocks.sapling_palirie, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(PlantBlocks.sapling_tyros, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(PlantBlocks.topinambur, RenderType.cutout());
		

		RenderTypeLookup.setRenderLayer(LogisticBlocks.pipe_normal, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.pipe_neon, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.pipe_redstone, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.pipe_support, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.insert_node, RenderType.cutout());
		
		RenderTypeLookup.setRenderLayer(FPFluids.neonFluidStill, RenderType.translucent());
		RenderTypeLookup.setRenderLayer(FPFluids.neonFluid_flowing, RenderType.translucent());
		RenderTypeLookup.setRenderLayer(FPFluids.salt_waterStill, RenderType.translucent());
		RenderTypeLookup.setRenderLayer(FPFluids.salt_water_flowing, RenderType.translucent());
		
		RenderTypeLookup.setRenderLayer(LogisticBlocks.plasma_jar, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.plasma_converter_t0, RenderType.cutout());
		RenderTypeLookup.setRenderLayer(LogisticBlocks.plasma_2_neon_t0, RenderType.translucent());

		RenderTypeLookup.setRenderLayer(ModifiableBlocks.radar, RenderType.cutout());
	}
	
	private void bindTileRenderers()
	{

		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.COMPOSITE_CHEST, RenderCompositeChest::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.WIRE_REDSTONE, RenderWireRedstone::new);

	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.RESEARCH_EXCHANGE, RenderTectern::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.AIRLOCK_DDOR, RenderAirlockDoor::new);
		
		//Die sind eigentlich okay nur die TE Renderer sind unvollständig
		
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.WIRE_NETWORK, RenderFastHologram::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.WIRE_NORMAL, RenderFastHologram::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.WIRE_SUPER, RenderFastHologram::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.WIRE_SUPPORT, RenderFastHologram::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.PIPE_NORMAL, RenderPipe::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.PIPE_NEON, RenderPipe::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.PIPE_SUPPORT, RenderPipe::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.INSERT_NODE, RenderPipe::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.ENTITY_EATER, RenderLaserBase::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.ENTITY_HEALER, RenderLaserBase::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.ENTITY_KILLER, RenderLaserBase::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.ROCKET_LAUNCHER, RenderLaserBase::new);
		
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.NEON_ENGINE, RenderNeonEngine::new);
		ClientRegistry.bindTileEntityRenderer(FPTileEntitys.FALLING_TREE, RenderFallingTreeFast::new);
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.DUNGEON_SPAWNER, RenderDungeonSpawner::new);
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.FORCE_FIELD, RenderForceFieldBlock::new);
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.PLASMA_CORE_T1, RenderPlasmaTank::new);
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.DEEPCORE_MAIN, RenderDeepCoreMiner::new);
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.CLAIME, RenderClaime::new);
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.MODULAR_DOOR, RenderModularDoor::new);
	    
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.FLUID_PUMP, RenderFluidPump::new);
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.FLUID_TANK, RenderFluidTank::new);
	  
	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.MOVING_BLOCKS, RenderMovingBlocksFast::new);

	    ClientRegistry.bindTileEntityRenderer(FPTileEntitys.RADAR, RenderRadar::new);

	}
	
	private void binEntityRenderers()
	{
		entityRenderItem(FPEntitys.ENTITY_EGGER);
		entityRenderItem(FPEntitys.GRENADE_PLASMA);
		entityRenderItem(FPEntitys.GRENADE_BLAZE);
		entityRenderItem(FPEntitys.GRENADE_SLIME);
		entityRenderItem(FPEntitys.WAKURIUM_THROWN);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.LASER_PROJECTILE, RenderLaserProjektile::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.CRAWLER, RenderCrawler::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.GEHUF, RenderGehuf::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.WOLBA, RenderWolba::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.MONOCART, RenderMonoCart::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.MINER, RenderMiner::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.HEULER, RenderHeuler::new);
		entityGrenade(FPEntitys.GRENADE_NORMAL, "textures/model/granate_geschoss.png");
		entityGrenade(FPEntitys.GRENADE_KOMPOST, "textures/model/kompost_granate_geschoss.png");
		entityGrenade(FPEntitys.GRENADE_SAAT, "textures/model/saat_granate_geschoss.png");
		entityGrenade(FPEntitys.GRENADE_FUTTER, "textures/model/futter_granate_geschoss.png");
		entityRenderItem(FPEntitys.GRENADE_ENTITY_EGGER);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.MONITOR, RenderMonitor::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.LASER, RenderLaserBowArrow::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.ROCKET_BIOTERIUM, RenderRocket::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.ROCKET_BLAZE, RenderRocket::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.ROCKET_NORMAL, RenderRocket::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.ROCKET_PLASMA, RenderRocket::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.EVIL_ROBOT, RenderEvilRobot::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.CYBER_ZOMBIE, RenderCyberZombie::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.FORESTMASTER, RenderForstmaster::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.FORCE_FIELD, RenderForceField::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.HOOK, m -> new RenderHook(m, Minecraft.getInstance().getItemRenderer()));
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.JAWAUL, RenderJawaul::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.DUNGEON_SPIDER, RenderDungeonSpider::new);
		RenderingRegistry.registerEntityRenderingHandler(FPEntitys.ALPHA_JAWAUL, RenderAlphaJawaul::new);
	}
	
	public void bindItemProperties()
	{
		ItemModelsProperties.register(ToolItems.spawner_chest, new ResourceLocation("full"), new IItemPropertyGetter()
        {
            @Override
            public float call(ItemStack stack, ClientWorld worldIn, LivingEntity entityIn)
            {
            	if(stack.getTagElement("spawner")!=null)
            		return 1F;
            	
                return 0F;
            }
        });
		ItemModelsProperties.register(ToolItems.escanner, new ResourceLocation("color"), new IItemPropertyGetter()
        {
            @Override
            public float call(ItemStack stack, ClientWorld worldIn, LivingEntity entityIn)
            {
            	if(stack.hasTag() && stack.getTag().getBoolean("s"))
            		return 1F;
            	
                return 0F;
            }
        });
		ItemModelsProperties.register(ToolItems.grappling_hook, new ResourceLocation("throwing"), new IItemPropertyGetter()
        {
			@Override
			public float call(ItemStack p_call_1_, ClientWorld w, LivingEntity entityIn)
			{
				return entityIn != null && ((ItemGrablingHook)ToolItems.grappling_hook).map(true).containsKey(entityIn) ? 1F : 0F;
			}
        });
		ItemModelsProperties.register(ToolItems.grenade_launcher, new ResourceLocation("pull"), new IItemPropertyGetter()
        {
            @Override
			@OnlyIn(Dist.CLIENT)
            public float call(ItemStack stack, ClientWorld worldIn, LivingEntity entityIn)
            {
                if (entityIn == null)
                {
                    return 0.0F;
                }
                else
                {
                    ItemStack itemstack = entityIn.getUseItem();
                    return itemstack != null && itemstack.getItem() == ToolItems.grenade_launcher ? (stack.getUseDuration() - entityIn.getUseItemRemainingTicks()) / 20.0F : 0.0F;
                }
            }
        });
		ItemModelsProperties.register(ToolItems.laser_bow, new ResourceLocation("pull"), new IItemPropertyGetter()
        {
            @Override
            public float call(ItemStack stack, @Nullable ClientWorld worldIn, @Nullable LivingEntity entityIn)
            {
                if (entityIn == null)
                {
                    return 0.0F;
                }
                else
                {
                    ItemStack itemstack = entityIn.getUseItem();
                    return itemstack != null && itemstack.getItem() == ToolItems.laser_bow ? (stack.getUseDuration() - entityIn.getUseItemRemainingTicks()) / 20.0F : 0.0F;
                }
            }
        });
		ItemModelsProperties.register(ToolItems.logisticEditor, new ResourceLocation("modes"), new IItemPropertyGetter()
		{	
			@Override
			public float call(ItemStack stack, ClientWorld w, LivingEntity base)
			{
				float f = ItemLogisticEditor.getModeFromItem(stack).ID / 10F;
				return f;
			}
		});
		ItemModelsProperties.register(ToolItems.scrench, new ResourceLocation("rotate"), new IItemPropertyGetter()
        {
            @Override
            public float call(ItemStack stack, @Nullable ClientWorld worldIn, @Nullable LivingEntity entityIn)
            {
            	if(stack.hasTag() && stack.getTag().getBoolean("rotate"))
            		return 1F;
            	
                return 0F;
            }
        });
	}
	
	private static final <T extends Entity & IRendersAsItem> void entityRenderItem(EntityType<T> cls)
	{
		RenderingRegistry.registerEntityRenderingHandler(cls, m -> new SpriteRenderer<>(m, Minecraft.getInstance().getItemRenderer()));
	}
	
	private static final void entityGrenade(EntityType<? extends EntityGrenadeBase> cls, String texture)
	{
		RenderingRegistry.registerEntityRenderingHandler(cls, m -> new RenderDefaultGrenade(m, new ResourceLocation(Constants.MOD_ID,texture)));
	}
	
	private void registerDimensionRenderers()
	{
		DimensionRenderInfo.EFFECTS.put(new ResourceLocation(Constants.MOD_ID, "menelaus"), new DimensionRenderTypeMenelaus());
		DimensionRenderInfo.EFFECTS.put(new ResourceLocation(Constants.MOD_ID, "tyros"), new DimensionRenderTypeTyros());
		DimensionRenderInfo.EFFECTS.put(new ResourceLocation(Constants.MOD_ID, "envia"), new DimensionRenderTypeEnvia());
		DimensionRenderInfo.EFFECTS.put(new ResourceLocation(Constants.MOD_ID, "asteroid_belt"), new DimensionRenderTypeAsteroidBelt());
	}
	
	@Override
	public void setupFinished(FMLLoadCompleteEvent event) 
	{
//		FuturepackDefaultResources.downloadFuturepackRersources(); TODO reanble this and rework JSOn resource system
		
		super.setupFinished(event);
		
		ItemBlockColoring.setupColoring();
		KeyBindAuto.init();
		
		Thread t = new Thread(new Runnable()
		{	
			@Override
			public void run() 
			{
				FPLog.logger.info("Starting Menue Thread");
				while(FPConfig.CLIENT.futurepackStartMenu.get())
				{
					AbstractGui g = Minecraft.getInstance().screen;
					if(g instanceof MainMenuScreen)
					{
						Minecraft.getInstance().setScreen(new GuiFPLoading());
						FPLog.logger.info("Hacked Menue");
						break;
					}
					try {
						Thread.sleep(1);
					} catch (InterruptedException e){
						e.printStackTrace();
					}
				}
			}
		}, "FP-Menue Thread");
	    t.start();
	    
//	    IReloadableResourceManager res = (IReloadableResourceManager) Minecraft.getInstance().getResourceManager();
//	    res.addReloadListener(new DataFolderWalker.RecipeReloadListener(null));
	}
	
	
}
