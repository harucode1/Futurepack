package futurepack.common;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

public class DirtyHacks
{

	public static <T extends Object> void replaceData(T target, T data, Class<T> cls)
	{
		Field[] vars = cls.getDeclaredFields();
		for(Field var : vars)
		{
			var.setAccessible(true);
			int mod = var.getModifiers();
			if(!Modifier.isStatic(mod) && !Modifier.isFinal(mod))
			try {
				var.set(target, var.get(data));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Method findMethod(Class cls, String devName, String obfName, Class...args)
	{
		return findMethod(cls, new String[] {obfName, devName}, args);
	}
	
	@Nullable
	public static Method findMethod(Class cls, String[] names, Class[] args)
	{
		for(String name : names)
		{
			try
			{
				return cls.getDeclaredMethod(name, args);
			}
			catch(NoSuchMethodException e)
			{ }
		}
		return null;
	}
	
	public static Field findField(Class cls, String devName, String obfName, Class type)
	{
		return findField(cls, new String[]{obfName, devName}, type);
	}
	
	public static Field findField(Class cls, String[] names, Class type)
	{
		for(String name : names)
		{
			try
			{
				Field f = cls.getDeclaredField(name);
				if(f.getType() == type)
				{
					return f;
				}
			}
			catch (NoSuchFieldException e)
			{ }
		}
		FPLog.logger.warn("Could not find fields %s in %s, but found fields of same type: [%s]", 
				Arrays.toString(names), 
				cls.toString(),
				Arrays.stream(cls.getDeclaredFields()).filter(c -> c.getType() == type).map(c -> c.getName()).collect(Collectors.joining(", ")));
		return null;
	}
	
}
