package futurepack.common.network;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.util.math.BlockPos;

public class FunkPacketCalculation extends PacketBase
{
	public final EnumCalculationType type;
	public final int toCalculate;
	private int inPorgress = 0;
	private int calcModules = 0;
	
	public FunkPacketCalculation(BlockPos src, ITileNetwork net, EnumCalculationType type, int amount)
	{
		super(src, net);
		this.type=type;
		toCalculate = amount;
	}
	
	public void registerCalculationAmount(int inprogress)
	{
		this.inPorgress += inprogress;
		this.calcModules++;
	}
	
	public int getInProgress()
	{
		return this.inPorgress;
	}
	
	public int getWorkingCalculationModules()
	{
		return this.calcModules;
	}
	
	public static enum EnumCalculationType
	{
		NEON,
		SUPPORT,
		EXP;
	}

}
