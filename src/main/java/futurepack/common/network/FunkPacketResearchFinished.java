package futurepack.common.network;

import java.util.UUID;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.research.Research;
import net.minecraft.util.math.BlockPos;

public class FunkPacketResearchFinished extends PacketBase 
{
	private final Research research;
	private final UUID fromPlayer;
	
	public FunkPacketResearchFinished(BlockPos src, ITileNetwork net, Research r, UUID from) 
	{
		super(src, net);
		this.research = r;
		this.fromPlayer = from;
	}

	public Research getResearch() 
	{
		return research;
	}

	public UUID getFromPlayer() 
	{
		return fromPlayer;
	}
}
