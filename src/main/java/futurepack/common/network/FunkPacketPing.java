package futurepack.common.network;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.util.math.BlockPos;

public class FunkPacketPing extends PacketBase
{
	private List<ITileNetwork> reachable;
	
	public FunkPacketPing(BlockPos src, ITileNetwork net)
	{
		super(src, net);
		reachable = new ArrayList<ITileNetwork>();
	}

	public void pong(ITileNetwork net)
	{
		reachable.add(net);
	}

	public List<ITileNetwork> getReachable()
	{
		return reachable;
	}
}
