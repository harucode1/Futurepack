package futurepack.common.network;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.util.math.BlockPos;

public class FunkPacketExperience extends PacketBase
{
	public final int needed;
	public int collected = 0;
	
	public FunkPacketExperience(BlockPos src, ITileNetwork net, int xp)
	{
		super(src, net);
		needed = xp;
	}

}
