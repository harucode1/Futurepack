package futurepack.common.network;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Predicates;

import futurepack.api.PacketBase;
import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.concurrent.ThreadTaskExecutor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;

public class NetworkManager implements Runnable
{
	private static NetworkManager instance = new NetworkManager(); 
 	private static IBlockSelector selectorNetwork = new IBlockSelector()
	{	
		@Override
		public boolean isValidBlock(World w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			if(dia)
				return false;
			
			if(w.getBlockEntity(pos) instanceof ITileNetwork)
			{
				ITileNetwork net = (ITileNetwork) w.getBlockEntity(pos);
				return net.isNetworkAble();
			}
			return false;
		}
		
		@Override
		public boolean canContinue(World w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{	
			ITileNetwork net = (ITileNetwork) w.getBlockEntity(pos);
			return net.isWire();
		}
	};	
	private static IBlockValidator selectorMachines = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(World w, ParentCoords pos)
		{
			ITileNetwork net = (ITileNetwork) w.getBlockEntity(pos);
			return net!=null && !net.isWire();
		}
		
	};
	
	private static Map<DimensionType,Set<WeakReference<Consumer<EventWirelessFunk>>>> Networks = new IdentityHashMap<DimensionType, Set<WeakReference<Consumer<EventWirelessFunk>>>>();
	
	
	public static List<ITileNetwork> pingNetwork(INetworkUser user)
	{
		FunkPacketPing pkt = new FunkPacketPing(user.getSenderPosition(), (ITileNetwork) user.getSenderWorld().getBlockEntity(user.getSenderPosition()));
		sendPacketThreaded(user, pkt);
		return pkt.getReachable();
	}
	
	public static void sendPacketThreaded(INetworkUser user, PacketBase pkt)
	{
		instance.sendSave(user, pkt);
	}
	
	public static void sendPacketUnthreaded(INetworkUser user, PacketBase pkt)
	{
		instance.send(user, pkt);
	}

	private Thread thread;
	private ArrayList<Entry> todo = new ArrayList<Entry>();
	private long ping = 0;
	
	public NetworkManager()
	{
		thread = new Thread(this, "Network-Thread");
		thread.setDaemon(true);
		thread.start();
	}
	
	private void send(INetworkUser user, PacketBase pkt)
	{
		//System.out.println("Ping "+(System.currentTimeMillis() - ping));
		//System.out.println("NetworkManager.send()");
		FPBlockSelector sel = FPSelectorHelper.getSelectorSave(user.getSenderWorld(), user.getSenderPosition(), selectorNetwork, true); //TileEnties are only available in main thread
		if(!isWorldThread(user.getSenderWorld()))
		{
			Collection<ParentCoords> machines = supplyAsync(user.getSenderWorld(), () -> sel.getValidBlocks(selectorMachines));//only in mian thread!
			Set<ITileNetwork> tiles = machines.parallelStream()
					.map(p -> supplyAsync(user.getSenderWorld(), () -> user.getSenderWorld().getBlockEntity(p)))//this will take some ticks so make it in parallel for large networks
					.filter(t -> !t.isRemoved())
					.map(t -> (ITileNetwork)t)
					.collect(Collectors.toSet());
			for(ITileNetwork net : tiles)
			{
				pkt.post(net);
			}
		}
		else
		{
			Collection<ParentCoords> machines = sel.getValidBlocks(selectorMachines);//only in mian thread!
			for(ParentCoords p : machines)
			{
				TileEntity t =  user.getSenderWorld().getBlockEntity(p);
				if(t.isRemoved())
					continue;
				pkt.post((ITileNetwork)t);
			}
		}
		user.postPacketSend(pkt);
	}
	
	
	
	public void sendSave(INetworkUser user, PacketBase pkt)
	{
		if(isBusy())
		{
			send(user, pkt); //Tunneling new packets so even if the network thread is blocked it is not important.
		}
		else if(thread.isAlive())
		{
			synchronized (todo)
			{
				todo.add(new Entry(user, pkt));
			}
		}
		else
		{
			System.err.println("Network Thread DIED");
		}
	}
	
	private static boolean isSendingEvents = false;
	private static ArrayList<Runnable> tasks = new ArrayList<Runnable>();
	
 	public static void registerWirelessTile(Consumer<EventWirelessFunk> t, World w)
	{		
 		if(isSendingEvents)
 		{
 			tasks.add(() -> registerWirelessTile(t, w));
 		}
 		else
 		{	
 			Set<WeakReference<Consumer<EventWirelessFunk>>> set = Networks.computeIfAbsent(w.dimensionType(), type -> new HashSet<>());
 			set.add(new WeakReference<Consumer<EventWirelessFunk>>(t));
 		}
	}
	
	public static void unregisterWirelessTile(Consumer<EventWirelessFunk> t, World w)
	{
		if(isSendingEvents)
		{
			tasks.add(() -> unregisterWirelessTile(t, w));
		}
		else
		{
			Set<WeakReference<Consumer<EventWirelessFunk>>> set = Networks.get(w.dimensionType());
			Iterator<WeakReference<Consumer<EventWirelessFunk>>> iter = set.iterator();
			while(iter.hasNext())
			{
				WeakReference<Consumer<EventWirelessFunk>> ref = iter.next();
				if(ref == null)
					iter.remove();
				else if(ref.get() == null)
					iter.remove();
				else if(ref.get() == t)
				{
					iter.remove();
					break;
				}
			}
		}
	}
	
	public static void sendEvent(EventWirelessFunk e, World dim)
	{
		isSendingEvents = true;
		Stream<Consumer<EventWirelessFunk>> s = Networks.get(dim.dimensionType()).stream().map(w -> w.get());
		s.filter(Predicates.notNull()).forEach(c -> c.accept(e));
		isSendingEvents = false;
		
		tasks.stream().forEach(Runnable::run);
	}

	@Override
	public void run()
	{
		while(true)
		{
			ArrayList<Entry> todo = null;
			synchronized (this.todo)
			{
				todo = this.todo;
				this.todo = new ArrayList<Entry>(todo.size());
			}	
			
			ping = System.currentTimeMillis();
			for(Entry next : todo)
			{
				if(next!=null)
				{
					send(next.user, next.pkt);	
				}
			}	
			
			try
			{
				Thread.sleep(50);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean isBusy()
	{
		return System.currentTimeMillis() - ping > 1000;
	}
	
	private static class Entry
	{
		final INetworkUser user;
		final PacketBase pkt;
		
		public Entry(INetworkUser user, PacketBase pkt)
		{
			super();
			this.user = user;
			this.pkt = pkt;
		}
	}
	
	public static boolean isWorldThread(World w)
	{
		return getExecutor(w).isSameThread();
	}
	
	public static ThreadTaskExecutor<? extends Runnable> getExecutor(World w)
	{
		if(w.getServer()!=null)
		{
			return w.getServer();
		}
		else
		{
			return DistExecutor.callWhenOn(Dist.CLIENT, () -> new Callable<ThreadTaskExecutor<Runnable>>()
			{
				@Override
				public ThreadTaskExecutor<Runnable> call() throws Exception 
				{
					return Minecraft.getInstance();
				}
			});
		}
	}
	
	public static <V> V supplyAsync(World w, Supplier<V> supplier)
	{
		ThreadTaskExecutor<? extends Runnable> t = getExecutor(w);
		return t.isSameThread() ? supplier.get() : CompletableFuture.supplyAsync(encapsuledSupplier(supplier), t).join();
	}
	
	private static <V> Supplier<V> encapsuledSupplier(Supplier<V> supplier)
	{
		return () -> {
			try
			{
				return supplier.get();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		};
	}
}
