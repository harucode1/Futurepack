package futurepack.common.network;

import java.util.ArrayList;

import futurepack.api.PacketBase;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.eventbus.api.Event;

public class EventWirelessFunk extends Event
{
	public int frequenz;
	public int range;
	public BlockPos source;
	public PacketBase objPassed;
	
	private ArrayList<TileEntity> allredyrecived = new ArrayList<TileEntity>();
	
	public EventWirelessFunk(BlockPos src, int frequenz, int range, PacketBase pkt)
	{
		this.source = src;
		this.frequenz = frequenz;
		this.range = range;
		this.objPassed = pkt;
	}
	
	
	public boolean canRecive(TileEntity t)
	{
		if(t.getBlockPos().equals(source))
		{
			return false;
		}
		if(allredyrecived.contains(t))
		{
			return false;
		}
		allredyrecived.add(t);
		return true;
	}
}
