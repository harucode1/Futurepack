package futurepack.common;

import futurepack.api.Constants;
import net.minecraft.loot.LootTables;
import net.minecraft.util.ResourceLocation;

public class FPLootTables
{
	public static final ResourceLocation TEC_DUNGEON = make("chests/tec_dungeon");
	public static final ResourceLocation BIG_TEC_DUNGEON = make("chests/big_tec_dungeon");
	
	
	
	private static ResourceLocation make(String s)
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, s);
		return LootTables.register(res);
	}
}
