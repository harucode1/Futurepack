package futurepack.common;

import java.util.WeakHashMap;

import futurepack.api.Constants;
import futurepack.common.item.tools.compositearmor.CompositeArmorInventory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.PotionEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Constants.MOD_ID)
public class FPPotions
{
	public static final Effect paralyze = new PotionParalyze().setRegistryName(new ResourceLocation(Constants.MOD_ID, "paralyze"));
	
	public static void register(RegistryEvent.Register<Effect> w)
	{
		w.getRegistry().register(paralyze);
	}
	
	@SubscribeEvent
	public static void onLivingTick(LivingUpdateEvent event)
	{
//		BlockPos pos = new BlockPos(event.getEntityLiving());
//		IBlockState state = event.getEntityLiving().world.getBlockState(pos);
//		if(state.getBlock() == FPBlocks.neonLiquid)
//		{
//			if(!CompositeArmorInventory.hasSetBonus(event.getEntityLiving()))
//			{
//				event.getEntityLiving().addPotionEffect(new PotionEffect(FPPotions.paralyze, 20*2, 1));
//			}	
//		}
		
		Effect effect = paralyze;
		EffectInstance eff = event.getEntityLiving().getEffect(effect);
		if(eff!=null)
		{
			effect.applyEffectTick(event.getEntityLiving(), eff.getAmplifier());
		}
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	@SubscribeEvent
	public static void onClientTick(TickEvent.ClientTickEvent event)
	{
		if(event.phase==Phase.END)
		{
			Minecraft mc = Minecraft.getInstance();
			if(mc.player!=null)
			{
				GameRenderer render = mc.gameRenderer;
				if(mc.player.hasEffect(paralyze) && render.currentEffect() == null)
				{
					render.loadEffect(new ResourceLocation(Constants.MOD_ID, "shader/post/paralyze.json"));
				}
				else if(!mc.player.hasEffect(paralyze) && render.currentEffect() != null)
				{
					render.shutdownEffect();
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onLivingAttack(LivingAttackEvent event)
	{
		if(event.getSource() instanceof EntityDamageSource)
		{
			if(event.getSource().getEntity() instanceof LivingEntity)
			{
				LivingEntity base = (LivingEntity) event.getSource().getEntity();
				if(base.hasEffect(paralyze))
				{
					event.setCanceled(true);
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onNewPotionEffect(PotionEvent.PotionAddedEvent event)
	{
		
	}
	
	@SubscribeEvent
	public static void isPotionEffectApplicable(PotionEvent.PotionApplicableEvent event)
	{
		if(allowed.get(event.getEntityLiving()) != Boolean.TRUE)
		{
			if(CompositeArmorInventory.hasAirTightSetBonus(event.getEntityLiving()))
			{
				if(event.getPotionEffect().getEffect() == Effects.JUMP && CompositeArmorInventory.hasDungeonSetBonus(event.getEntityLiving()))
				{
					event.setResult(Result.ALLOW);
				}
				else
				{
					event.setResult(Result.DENY);
				}
				
			}
		}
	}
	
	@SubscribeEvent
	public static void onRemovePotionEffect(PotionEvent.PotionRemoveEvent event)
	{
		
	}
	
	@SubscribeEvent
	public static void onItemUseFinsih(LivingEntityUseItemEvent.Tick event)
	{
		if(!event.getEntity().level.isClientSide)
		{
			if(event.getDuration() <= 1 && CompositeArmorInventory.hasAirTightSetBonus(event.getEntityLiving()))
			{
				allowed.put(event.getEntityLiving(), Boolean.TRUE);
			}
		}
	}
	
	private static WeakHashMap<LivingEntity, Boolean> allowed = new WeakHashMap<>();
	
	@SubscribeEvent
	public static void onWorldTick(WorldTickEvent event)
	{
		if(event.phase == Phase.START)
		{
			allowed.clear();
		}
	}
}
