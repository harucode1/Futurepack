package futurepack.common.spaceships;

import futurepack.api.Constants;
import net.minecraft.util.ResourceLocation;

public class PlanetUNKNOWN extends PlanetBase
{
	private ResourceLocation dim=new ResourceLocation("overword");
	
	public PlanetUNKNOWN()
	{
		super(new ResourceLocation("overworld"), new ResourceLocation(Constants.MOD_ID, "textures/gui/undefinierbarer_datensatz.png"), "UNKNOWN", new String[0]);
	}

	protected void initDim(ResourceLocation id)
	{
		this.dim = id;
	}
	
	@Override
	public ResourceLocation getDimenionId()
	{
		return dim;
	}
}
