package futurepack.common.spaceships;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;

public class SpaceshipCashServer
{
	private static HashMap<UUID, MovingShip> map = new HashMap<UUID, MovingShip>();
	
	public static MovingShip getShipByUUID(UUID uid)
	{
		checkMap();
		
		MovingShip ship = map.get(uid);
		if(ship==null)
		{
			return null;
		}
		else if(ship.isValid())
		{
			return ship;
		}
		else
		{
			map.remove(uid);
		}
		return null;
		
	}
	
	private static long last = 0;
	
	private static void checkMap()
	{
		if(System.currentTimeMillis() - last > 60*1000)
		{
			removeInvalidEntrys();
			last = System.currentTimeMillis();
		}
	}
	
	public static void removeInvalidEntrys()
	{
		Iterator<Entry<UUID, MovingShip>> iter = map.entrySet().iterator();
		while(iter.hasNext())
		{
			if(!iter.next().getValue().isValid())
			{
				iter.remove();
			}
		}
	}

	public static void register(UUID shipID, MovingShip movingShip)
	{
		checkMap();
		map.put(shipID, movingShip);
	}
}
