package futurepack.common.spaceships.moving;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import futurepack.common.FPBlockSelector;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.logistic.frames.TileEntityMovingBlocks;
import futurepack.depend.api.MiniWorld;
import futurepack.depend.api.helper.HelperItems;
import futurepack.world.dimensions.TreeUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.PushReaction;
import net.minecraft.fluid.FluidState;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.NextTickListEntry;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerTickList;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants.BlockFlags;

public class MovingBlocktUtil 
{
	public static TileEntityMovingBlocks beginMoveBlocks(FPBlockSelector selected, Vector3i direction, Consumer<SimpleCollision> callback)
	{
		if(!selected.getWorld().isClientSide())
		{
			SimpleCollision col = new SimpleCollision(selected, direction, pos -> selected.getWorld().isEmptyBlock(pos) || selected.getWorld().getBlockState(pos).getPistonPushReaction() == PushReaction.DESTROY);
			
			if(col.canMove())
			{
				MiniWorld mw = createFromBlockSelector(selected);
				//original blocks are still there
				
				if(callback!=null)
					callback.accept(col);
				
				ArrayList<NextTickListEntry<Block>> pendingTicks = copyTickList((ServerWorld) selected.getWorld(), col, direction);
				
				replaceBlocksFast(selected.getWorld(), col);
				//on old positons is air
				//an all positions where block will be are barrier blocks
				
				BlockPos randomNewPos = col.getPositionsToCheck().stream().findAny().get();
				
				selected.getWorld().setBlock(randomNewPos, LogisticBlocks.moving_blocks.defaultBlockState(), BlockFlags.DEFAULT | 128);
				TileEntityMovingBlocks mb = (TileEntityMovingBlocks) selected.getWorld().getBlockEntity(randomNewPos);
	//			mw.start = randomNewPos;
				
				mb.setMiniWorld(mw);
				mb.setDirection(direction);
				mb.setPendingTicks(pendingTicks);
				
				return mb;
			}
		}
		return null;
	}
	
	public static void endMoveBlocks(World w, MiniWorld mw, Vector3i direction, @Nullable List<NextTickListEntry<Block>> pendingTicks)
	{
		if(!w.isClientSide())
		{
			place(mw, w, mw.start.offset(direction));
			if(pendingTicks!=null)
			{
				ServerTickList<Block> tickList = ((ServerTickList<Block>)w.getBlockTicks());
				pendingTicks.forEach(tickList::addTickData);
			}
		}
	}
	
	public static MiniWorld createFromBlockSelector(FPBlockSelector selected)
	{
		return TreeUtils.copyFromWorld((ISeedReader)selected.getWorld(), selected.getAllBlocks());
	}
	
	public static void replaceBlocksFast(World w, SimpleCollision collision)
	{
		int flag = BlockFlags.DEFAULT | BlockFlags.IS_MOVING | 128 | BlockFlags.NO_NEIGHBOR_DROPS; //the 128 prevents light updates - yes this will look ugly probaply, but its fast
		//HelperItems.disableItemSpawn();
		
		
		Consumer<BlockPos> remTileEntity = p -> w.removeBlockEntity(p);
		
		collision.getOldPositions().forEach(remTileEntity);
		collision.getUnchangedPositions().forEach(remTileEntity);
		collision.getPositionsToCheck().forEach(remTileEntity);
		
		collision.getOldPositions().forEach(pos -> {
			FluidState fluidstate = w.getFluidState(pos);
			w.setBlock(pos, fluidstate.createLegacyBlock(), BlockFlags.BLOCK_UPDATE); //no neighbour updates
	    });
		collision.getUnchangedPositions().forEach(pos -> w.setBlock(pos, Blocks.BARRIER.defaultBlockState(), flag));
		collision.getPositionsToCheck().forEach(pos -> w.setBlock(pos, Blocks.BARRIER.defaultBlockState(), flag));
		
		//HelperItems.enableItemSpawn();
	}
	
	
	
	public static void place(MiniWorld mw, World w, BlockPos pos)
	{
		BlockPos.Mutable mut = new BlockPos.Mutable();
		BlockState void_air = Blocks.VOID_AIR.defaultBlockState();
		for(int x=0;x<mw.width;x++)
		{
			mut.setX(x + pos.getX());
			
			for(int y=0;y<mw.height;y++)
			{
				mut.setY(y + pos.getY());
				
				for(int z=0;z<mw.depth;z++)
				{
					mut.setZ(z + pos.getZ());
					
					if(mw.states[x][y][z]!=void_air)
					{
						w.setBlock(mut, mw.states[x][y][z], BlockFlags.DEFAULT_AND_RERENDER);
						if(mw.tiles[x][y][z]!=null)
						{
							mw.tiles[x][y][z].clearCache();
							mw.tiles[x][y][z].clearRemoved();
							w.setBlockEntity(mut, mw.tiles[x][y][z]); //I am using the original instance, hopefully wont break stuff
						}
					}
				}
			}
		}
	}
	
	public static ArrayList<NextTickListEntry<Block>> copyTickList(ServerWorld w, SimpleCollision collision, Vector3i direction)
	{
		ServerTickList<Block> list = w.getBlockTicks();
		AxisAlignedBB box =  collision.getSize();
		MutableBoundingBox bb = MutableBoundingBox.createProper((int)box.minX, (int)box.minY, (int)box.minZ, (int)box.maxX+1, (int)box.maxY+1, (int)box.maxZ+1);//+1 becuase id does a block.x < box.maxX check and nto a a <= check 
		
		final boolean removePendingTicks = true;
		final boolean ignoreAlreadyTicked = true;
		List<NextTickListEntry<Block>> pendingBlocks = list.fetchTicksInArea(bb, removePendingTicks, ignoreAlreadyTicked);
		ArrayList<NextTickListEntry<Block>> movedBlocks = new ArrayList<>(pendingBlocks.size());
		
		Iterator<NextTickListEntry<Block>> iter = pendingBlocks.iterator();
		Set<BlockPos> movedBlocksSet = new HashSet<>(collision.getUnchangedPositions());
		movedBlocksSet.addAll(collision.getOldPositions());
		
		while(iter.hasNext())
		{
			NextTickListEntry<Block> entry = iter.next();
			if(movedBlocksSet.contains(entry.pos))
			{
				movedBlocks.add(moveTicks(entry, direction));
				iter.remove();
			}
		}
		movedBlocksSet = null;
		iter = null;
		
		pendingBlocks.forEach(list::addTickData);
		
		
		movedBlocks.trimToSize();
		return movedBlocks;
	}
	
	public static <T> NextTickListEntry<T> moveTicks(NextTickListEntry<T> entry, Vector3i direction)
	{
		return new NextTickListEntry<T>(entry.pos.offset(direction), entry.getType(), entry.triggerTick, entry.priority);
	}
}
