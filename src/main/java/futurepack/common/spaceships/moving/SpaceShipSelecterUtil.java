package futurepack.common.spaceships.moving;

import java.util.List;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.ISpaceshipSelector;
import futurepack.common.FPBlockSelector;
import futurepack.common.PredicateStatisticsManager;
import futurepack.common.block.logistic.frames.TileEntityMovingBlocks;
import futurepack.common.spaceships.FPSpaceShipSelector;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class SpaceShipSelecterUtil 
{
	
	/**
	 * This is highly optimized to be as fast as possible and thus make as few world calls as possible. ALso the chunk is tried to cache to reduce getCHunk calls.
	 */
	public static IBlockSelector wrap(final ISpaceshipSelector selector)
	{
		if(selector.getHeight() + selector.getWidth() + selector.getDepth() < 3)
		{
			throw new IllegalArgumentException("Spaceship Selector needs to select a ship first!");
		}
		
		return new IBlockSelector()
		{	
			
			int chunkX, chunkZ;
			Chunk c;
			
			@Override
			public boolean isValidBlock(World w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent) 
			{
				if(w.isInWorldBounds(pos))
				{
					if(selector.isInArea(pos))
					{
						if(c==null || pos.getX() >>4 != chunkX || pos.getZ() >> 4 != chunkZ)
						{
							c = w.getChunk(chunkX = pos.getX() >>4, chunkZ = pos.getZ() >> 4);
						}
						
						if(c.getBlockState(pos).isAir())
						{
							return false;
						}
						else
						{
							return true;
						}
					}
				}
				return false;
			}
			
			@Override
			public boolean canContinue(World w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent) 
			{
				return true;
			}
		};
	}
	
	public static TileEntityMovingBlocks moveSpaceship(World w, BlockPos pos, Vector3i direction)
	{
		
		
		return moveSpaceship(selectShip(w, pos), pos, direction);
	}
	
	public static FPSpaceShipSelector selectShip(World w, BlockPos pos)
	{
		PredicateStatisticsManager stats = FPSpaceShipSelector.getSpaceshipStatManager();
		FPBlockSelector blocks = new FPBlockSelector(w, FPBlockSelector.base, stats);
		FPSpaceShipSelector base = new FPSpaceShipSelector(blocks);
		base.selectShip(pos, Material.METAL);
		return base;
	}
	
	
	public static TileEntityMovingBlocks moveSpaceship(ISpaceshipSelector selector, BlockPos pos, Vector3i direction)
	{
		FPBlockSelector blocks = new FPBlockSelector(selector.getWorld(), wrap(selector));
		blocks.selectBlocks(pos);
		

		TileEntityMovingBlocks tile = MovingBlocktUtil.beginMoveBlocks(blocks, direction, c -> SpaceShipSelecterUtil.moveEntities(c, selector));
		
		return tile;
	}
	
	public static void moveEntities(SimpleCollision col, ISpaceshipSelector selector)
	{
		AxisAlignedBB moving = col.getSize();
		
		List<Entity> entities = col.getSelector().getWorld().getEntities((Entity)null, moving, e -> {
			BlockPos pos = new BlockPos(e.position());
			return selector.isInArea(pos.getX(), pos.getY(), pos.getZ()) || selector.isInArea(pos.getX(), pos.getY()-1, pos.getZ()) || selector.isInArea(pos.getX(), pos.getY()+1, pos.getZ());
		});
		
		entities.forEach(e ->  
		{
			Vector3i dir = col.getDirection();
			Vector3d pos = e.getPosition(0f).add(dir.getX(), dir.getY(),dir.getZ());
			e.setPos(pos.x, pos.y, pos.z);
		});
		
		
	}
	
	
}
