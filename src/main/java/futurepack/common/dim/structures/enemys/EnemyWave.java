package futurepack.common.dim.structures.enemys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.INBTSerializable;

/**
 * Wave:
 * {
 *     "coolDown":0,
 *     "entrys":[
 *         { "id":"minecraft:pig", "count":2},
 *         { "id":"miencraft:slime", "data":{"size":10} , count:10}
 *     ]
 * }
 */
public class EnemyWave implements INBTSerializable<CompoundNBT>
{
	public final List<EnemyEntry> enemys;
	public final int coolDown;
	public final String id;
	public IFormattableTextComponent subtitle = null;
	
	public ArrayList<Entity> toDefeat = new ArrayList<Entity>();
	
	private List<UUID> cashedEntitys;
	
	private EnemyWave(List<EnemyEntry> e, int coolDown, String res) 
	{
		this.enemys = e;
		this.coolDown = coolDown;
		id = res;
	}
	
	public EnemyWave(JsonObject wave, String res)
	{
		id = res;
		coolDown = wave.has("coolDown") ? wave.get("coolDown").getAsInt() : 200; // 200 ticks  = 10 sekunden
		enemys = new ArrayList<EnemyEntry>();
		for(JsonElement jo : wave.getAsJsonArray("entrys"))
		{
			try 
			{
				enemys.add(new EnemyEntry(jo.getAsJsonObject()));
			}
			catch (CommandSyntaxException e)
			{
				System.err.println(jo);
				e.printStackTrace();
			}
		}
		
		if(wave.has("message"))
		{
			subtitle = ITextComponent.Serializer.fromJson(wave.get("message").toString());
        }
	}
	
	public int getEnemyTotal()
	{
		int count = 0;
		for(EnemyEntry ent : enemys)
		{
			count += ent.getEnemyCount();
		}
		return count;
	}
	
	public void startWave(World w, BlockPos pos)
	{
		enemys.forEach(e -> toDefeat.addAll(e.getEntitys(w))); 
		
		double x = pos.getX() + 0.5;
		double y = pos.getY() + 0.1;
		double z = pos.getZ() + 0.5;
		toDefeat.forEach(e -> 
		{
			e.setPos(x, y, z);
			w.addFreshEntity(e);
		});			
	}
	
	public boolean isComplete(World w)
	{
		if(cashedEntitys!=null)
		{
			List<UUID> toFind = Collections.synchronizedList(cashedEntitys);
			toDefeat.ensureCapacity(toFind.size());
			ServerWorld ws = (ServerWorld) w;
			toFind.stream().map(e -> ws.getEntity(e)).filter(e -> e!=null).forEach(toDefeat::add);
			cashedEntitys = null;
		}
		if(!toDefeat.isEmpty())
		{
			for(Entity e : toDefeat)
			{
				if(e instanceof LivingEntity)
				{
					LivingEntity liv = (LivingEntity) e;
					if(liv.removed)
					{
						liv.revive();
					}
				}
			}
		}
			
		removeDefeated();
		return toDefeat.isEmpty();
	}
	
	public boolean isAktive(World w)
	{
		if(cashedEntitys!=null)
		{
			return true;
		}
			
		return !toDefeat.isEmpty();
	}
	
	private void removeDefeated()
	{
		toDefeat.removeIf(this::isEntityDefeated); //strange Lamda thingy
	}
	
	private boolean isEntityDefeated(Entity e)
	{
		if(e == null)
			return true;
		if(e instanceof LivingEntity)
			return isDying((LivingEntity) e);
		else
			return !e.isAlive();
	}
	
	public void clear()
	{			
		toDefeat.forEach(Entity::remove);
	}
	
	public int getCoolDown()
	{
		return coolDown;
	}

	@Override
	public CompoundNBT serializeNBT()
	{
		CompoundNBT nbt = new CompoundNBT();
		nbt.putString("id", id);
		
		ListNBT list = new ListNBT();
		for(Entity e : toDefeat)
		{
			String uid = e.getStringUUID();
			list.add(StringNBT.valueOf(uid));
		}
		nbt.put("toDefeat", list);
		
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt)
	{
		cashedEntitys = new ArrayList<UUID>();
		ListNBT list = nbt.getList("toDefeat", 8);
		for(int i=0;i<list.size();i++)
		{
			cashedEntitys.add(UUID.fromString(list.getString(i)));
		}
	}

	private boolean isDying(LivingEntity entity)
	{
		if( entity.deathTime > 0 || (!entity.removed && entity.getHealth() <= 0))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
