package futurepack.common.dim.structures.enemys;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import futurepack.common.FPLog;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;

public class EnemyEntry
{
	private final ResourceLocation id;
	private final CompoundNBT entityData;
	private final int count;
	
	public EnemyEntry(String id, CompoundNBT entityData, int count)
	{
		super();
		this.id = new ResourceLocation(id);
		this.entityData = entityData;
		this.count = count;
	}
	
	public EnemyEntry(JsonObject obj) throws CommandSyntaxException
	{
		this(obj.get("id").getAsString(), obj.has("data") ? getData(obj.get("data")) : new CompoundNBT(), obj.has("count") ? obj.get("count").getAsInt() : 1);
	}
	
	public List<Entity> getEntitys(World w)
	{
		List<Entity> list = new ArrayList<Entity>(count);
		for(int i=0;i<count;i++)
		{
			list.add(create(w));
		}
		return list;
	}
	
	public int getEnemyCount()
	{
		return count;
	}
	
	private Entity create(World w)
	{
		try
		{
			Entity e =  ForgeRegistries.ENTITIES.getValue(id).create(w);
			if(e==null)
			{
				FPLog.logger.error("Cant find Entity \"%s\" -> use fallback of pig to prevent NullPointer.", id);
				return EntityType.PIG.create(w);
			}
			e.load(entityData);
			return e;
		}
		catch(Exception ee)
		{
			FPLog.logger.fatal("Exception while loading Entity of %s entityData id [%s]", id, entityData.toString());
			throw ee;
		}
	}
	
	private static CompoundNBT getData(JsonElement elm) throws CommandSyntaxException
	{
		String s = "";
		try
		{
			s = elm.getAsString();
		}
		catch(UnsupportedOperationException e)
		{
			s = elm.toString();
		}
		return JsonToNBT.parseTag(s);
	}

	public int getDangerColor(float saturation)
	{
		EntityType type = ForgeRegistries.ENTITIES.getValue(id);
		if(type!=null)
		{
			SpawnEggItem egg = SpawnEggItem.byId(type);
			if(egg!=null)
			{
				int rgb = egg.getColor(0) & 0xFFFFFF;
				int end = 96+ (int) (159*saturation);
				end <<= 24;
				return end | rgb;
			}
		}
		return 0xFFFFFFFF;
	}
}
