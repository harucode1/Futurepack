package futurepack.common.dim.structures;

import java.util.Random;

import futurepack.api.Constants;
import futurepack.common.FPLog;
import futurepack.common.block.logistic.IInvWrapper;
import net.minecraft.inventory.IInventory;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootParameterSets;
import net.minecraft.loot.LootParameters;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.LootTableManager;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.server.ServerWorld;

public class DungeonChest
{
	private BlockPos pos;
	private ResourceLocation loottable;
	private StructureBase base;
	
	public DungeonChest(BlockPos pos, ResourceLocation res, StructureBase base)
	{
		super();
		this.pos = pos;
		loottable = res;
		this.base = base;
	}
	
	public DungeonChest(BlockPos pos, StructureBase base)
	{
		this(pos, new ResourceLocation(Constants.MOD_ID), base);
	}	
	
	public void genLoot(ServerWorld w, Random rand, BlockPos start, CompoundNBT nbt)
	{
		genLoot(w, rand, start, nbt, w.getServer().getLootTables());
	}	
	
	public void genLoot(IServerWorld w, Random rand, BlockPos start, CompoundNBT nbt, LootTableManager manager)
	{
		BlockPos lootpos = start.offset(pos);
		TileEntity tile =  w.getBlockEntity(lootpos);	    	
		if(tile==null)
		{
			FPLog.logger.error("The TileEntity is null! This mistake is in " + base);
			return;
		}
		IInventory inv = null;
		if(tile instanceof IInventory)
		{
			inv = (IInventory) tile;
		}
		else
		{
			inv = new IInvWrapper(tile);
		}
			
		LootTable loottable;
		int lvl = nbt.getInt("tecLevel");
		ResourceLocation res = new ResourceLocation(this.loottable+"_"+lvl);
		loottable = manager.get(res);
		
		if(loottable==LootTable.EMPTY)
		{
			loottable = manager.get(this.loottable);
			
			if(loottable==LootTable.EMPTY)
			{
				FPLog.logger.error("Missing Loot Table detected: " + res.toString() + " and " + this.loottable.toString());
			}
		}
		
		LootContext.Builder builder = new LootContext.Builder((ServerWorld)w.getLevel());
		builder.withParameter(LootParameters.ORIGIN, Vector3d.atCenterOf(lootpos));
		
		loottable.fill(inv, builder.create(LootParameterSets.CHEST));
		
		lvl = nbt.getInt("level");
		if(lvl>=3)
		{
			if(rand.nextInt(100) < 5)
			{
				res = new ResourceLocation(Constants.MOD_ID, "loot_tables/chests/blueprints/laserbow");
				loottable = manager.get(this.loottable);
				loottable.fill(inv, builder.create(LootParameterSets.CHEST));
			}
		}
	}
}
