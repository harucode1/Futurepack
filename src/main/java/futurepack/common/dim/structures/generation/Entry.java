package futurepack.common.dim.structures.generation;

import futurepack.common.dim.structures.StructureBase;

public class Entry
{
	public final StructureBase structure;
	public final EnumGenerationType type;
	public final int weight;
	
	public Entry(StructureBase structure, EnumGenerationType type, int weight)
	{
		super();
		this.structure = structure;
		this.type = type;
		this.weight = weight;
	}	
}
