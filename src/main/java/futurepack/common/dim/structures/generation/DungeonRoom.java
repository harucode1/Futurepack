package futurepack.common.dim.structures.generation;

import futurepack.common.dim.structures.StructureBase;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;

public class DungeonRoom
{
	public final StructureBase structure;
	public final BlockPos pos;
	public final CompoundNBT extra;
	
	public DungeonRoom(StructureBase structure, BlockPos pos, CompoundNBT extra)
	{
		super();
		this.structure = structure;
		this.pos = pos;
		this.extra = extra;
	}
}
