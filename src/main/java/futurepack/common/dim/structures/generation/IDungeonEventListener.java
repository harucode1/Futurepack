package futurepack.common.dim.structures.generation;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;

public interface IDungeonEventListener
{
	/**
	 * Called when every structure is placed & protection has been applied
	 * 
	 * @param w the World where this dungeon is
	 * @param dungeon the baked dungeon instance
	 * @param pos the start position of the 1st room. SO most likely somewhere in the center of the dungeon.
	 */
	public void onDungeonFinished(ServerWorld w, BakedDungeon dungeon, BlockPos pos);
}
