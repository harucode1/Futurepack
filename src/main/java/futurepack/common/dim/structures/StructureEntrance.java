package futurepack.common.dim.structures;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import futurepack.world.protection.FPDungeonProtection;
import net.minecraft.block.BlockState;
import net.minecraft.loot.LootTableManager;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.gen.Heightmap;

public class StructureEntrance extends StructureBase
{
	StructureBase upper;

	Map<BlockPos, BlockPos> map = new HashMap<>();
	
	public StructureEntrance(StructureBase base)
	{
		super(base);
		upper = ManagerDungeonStructures.get("special_entrance_upper", 0);
		upper.hide = false;
	}

	@Override
	public void addChestContentBase(IServerWorld w, BlockPos start, Random rand, CompoundNBT extraData, LootTableManager manager)
	{
		super.addChestContentBase(w, start, rand, extraData, manager);
		
		BlockPos entrace = map.remove(start);
		if(entrace!=null)
			upper.addChestContentBase(w, entrace, rand, extraData, manager);
	}
	
	@Override
	public void generate(World w, BlockPos start, List<MutableBoundingBox> spawnedRooms)
	{
		super.generate(w, start, spawnedRooms);
		
		BlockPos p00 = w.getHeightmapPos(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, start).below();
		BlockPos p01 = w.getHeightmapPos(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, start.offset(0,0,getDepth())).below();
		BlockPos p10 = w.getHeightmapPos(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, start.offset(getWidth(), 0, 0)).below();
		BlockPos p11 = w.getHeightmapPos(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, start.offset(getWidth(), 0, getDepth())).below();
		
		BlockState s00 = w.getBlockState(p00);
		BlockState s01 = w.getBlockState(p01);
		BlockState s10 = w.getBlockState(p10);
		BlockState s11 = w.getBlockState(p11);
		
		int h = (p00.getY() + p01.getY() + p10.getY() + p11.getY()) / 4;
		h--;
		if(h <= start.getY()+this.getHeight())
		{
			h = start.getY() + 1 + this.getHeight();
		}
		BlockPos entrace = new BlockPos(start.getX() +1, h, start.getZ()+1);
		
		upper.generate(w, entrace, spawnedRooms);
		map.put(start, entrace);	
		FPDungeonProtection.addProtection(w, upper.getBoundingBox(entrace));
	}
	
	
}
