package futurepack.common.dim.structures;

import java.util.ArrayList;
import java.util.Random;

import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityDungeonCheckpoint;
import net.minecraft.block.BlockState;
import net.minecraft.loot.LootTableManager;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.IServerWorld;

public class StructureCheckpoint extends StructureBase
{
	private BlockPos[] teleporter;
	
	public StructureCheckpoint(StructureBase base)
	{
		super(base);
		BlockState[][][] blocks = getBlocks();
		
		ArrayList<BlockPos> teleporter = new ArrayList<BlockPos>();
		
		for(int x=0;x<blocks.length;x++)
		{
			for(int y=0;y<blocks[x].length;y++)
			{
				for(int z=0;z<blocks[x][y].length;z++)
				{
					if(blocks[x][y][z]!=null)
					{
						if(blocks[x][y][z].getBlock() == MiscBlocks.dungeon_checkpoint)
						{
							teleporter.add(new BlockPos(x,y,z));
						}
					}
				}
			}
		}
		this.teleporter = teleporter.toArray(new BlockPos[teleporter.size()]);
	}

	@Override
	public void addChestContentBase(IServerWorld w, BlockPos start, Random rand, CompoundNBT extraData, LootTableManager manager)
	{
		super.addChestContentBase(w, start, rand, extraData, manager);
		
		int tecLvl = extraData.getInt("tecLevel");
		String dungeonName = extraData.getString("name");
		int floor = extraData.getInt("level");
		
		TextFormatting[] light = {TextFormatting.BLACK, TextFormatting.DARK_AQUA, TextFormatting.DARK_BLUE, TextFormatting.GOLD, TextFormatting.DARK_GREEN, TextFormatting.DARK_PURPLE, TextFormatting.DARK_RED};
		dungeonName = light[rand.nextInt(light.length)].toString() + dungeonName + TextFormatting.RESET.toString();
		
		String teleporterName = String.format("Floor:%s %s L:%s", floor, dungeonName, tecLvl);
		
		for(BlockPos pos : teleporter)
		{
			TileEntity e = w.getBlockEntity(start.offset(pos));
			if(e!=null)
			{
				TileEntityDungeonCheckpoint base = (TileEntityDungeonCheckpoint) e;
				base.setName(teleporterName);
				
			}
		}
	}

}
