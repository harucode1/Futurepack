package futurepack.common.dim.structures;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import futurepack.common.FPLog;
import futurepack.common.dim.structures.generation.IDungeonEventListener;
import net.minecraft.block.BlockState;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.loot.LootTableManager;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.IWorldWriter;
import net.minecraft.world.World;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.surfacebuilders.ISurfaceBuilderConfig;

public class StructureBase
{
	private final BlockState[][][] blocks;
	private DungeonChest[] chests=null;
	private OpenDoor[] doors;
	public boolean hide = true;
	public final String name;
	
	public StructureBase(@Nullable String name, BlockState[][][] states)
	{
		blocks = states;
		this.name = name;
	}
	
	public StructureBase(StructureBase base)
	{
		blocks = base.blocks;
		chests = base.chests;
		doors = base.doors;
		name = base.name;
	}
	
	public int getWidth()
	{
		return blocks.length;
	}
	
	public int getHeight()
	{
		return blocks[0].length;
	}
	
	public int getDepth()
	{
		return blocks[0][0].length;
	}
	
	/*
	 * n * 3 int array n is the count of the chest and 3 the dimensions
	 */
	public void setChests(DungeonChest[] pos)
	{
		chests = pos;
	}
	
	public void setOpenDoors(OpenDoor[] doorA)
	{
		for(int i=0;i<doorA.length;i++)
		{
			for(int j=i;j<doorA.length;j++)
			{
				if(i!=j && doorA[i].equals(doorA[j]))
				{
					throw new IllegalArgumentException("Contains door entries with are identical");
				}
			}
		}
		
		doors = doorA;		
	}
	
	public OpenDoor[] getRawDoors()
	{
		return doors;
	}
	
	public void generate(World w, BlockPos start, List<MutableBoundingBox> spawnedRooms)
	{
//		for(int x=0;x<getWidth();x++)
//		{
//			for(int y=0;y<getHeight();y++)
//			{
//				for(int z=0;z<getDepth();z++)
//				{
//					BlockPos xyz = start.add(x,y,z);
//					w.setBlockState(xyz, Blocks.bookshelf.getDefaultState(), 2);			
//				}
//			}
//		}
		spawnedRooms.add(getBoundingBox(start));
		
		generateBase(w, start);
		
		final int h =  getHeight();
		final int ww = getWidth();
		final int d = getDepth();
		
		if(hide)
		{
			BlockPos top = start.offset(0, h, 0);
			BlockPos xz = w.getHeightmapPos(Type.WORLD_SURFACE, top);
			boolean tooBig = false;
			if(top.getY()>xz.getY())
			{
				tooBig = true;
			}
			else
			{
				top = start.offset(ww, h, 0);
				xz = w.getHeightmapPos(Type.WORLD_SURFACE, top);
				if(top.getY()>xz.getY())
				{
					tooBig = true;
				}
				else
				{
					top = start.offset(0, h, d);
					xz = w.getHeightmapPos(Type.WORLD_SURFACE, top);
					if(top.getY()>xz.getY())
					{
						tooBig = true;
					}
					else
					{
						top = start.offset(ww, h, d);
						xz = w.getHeightmapPos(Type.WORLD_SURFACE, top);
						if(top.getY()>xz.getY())
						{
							tooBig = true;
						}
					}
				}
			}
			if(tooBig)
				hideWithBlocks(w, start);
		}
		
		AxisAlignedBB bb = new AxisAlignedBB(start, start.offset(ww, h, d));
		List<ItemEntity> l = w.getEntitiesOfClass(ItemEntity.class, bb);
		if(!l.isEmpty())
		{
			FPLog.logger.debug("Generatring Dungeon Strulkture " + name + " dropped items");
		}
		l.forEach(ItemEntity::remove);
	}
	
	public void generateBase(IWorldWriter w, BlockPos start)
	{
		final int h =  getHeight();
		final int ww = getWidth();
		final int d = getDepth();
		final int x0 = start.getX(), y0 = start.getY(), z0 = start.getZ();
		
		BlockPos.Mutable mut = new BlockPos.Mutable();
		for(int y=0;y<h;y++)
		{		
			for(int x=0;x<ww;x++)
			{
				for(int z=0;z<d;z++)
				{
					if(blocks[x][y][z]!=null)
					{
						mut.set(x0+x, y0+y, z0+z);
						w.setBlock(mut, blocks[x][y][z], 2);
					}				
				}
			}
		}
	}
	
	private void hideWithBlocks(World w, BlockPos start)
	{
		int h =  getHeight();
		int ww = getWidth();
		int d = getDepth();
		
		for(int x=-1;x<ww+1;x++)
		{
			for(int z=-1;z<d+1;z++)
			{
				BlockPos xyz = start.offset(x,h,z);
				ISurfaceBuilderConfig config = w.getBiome(xyz).getGenerationSettings().getSurfaceBuilderConfig();
				w.setBlockAndUpdate(xyz, config.getTopMaterial());
				
				if(x==-1 || z==-1 || x==ww || z==d)
				{
					Direction face = x==-1 ? Direction.EAST : x==ww ? Direction.WEST : null;
					if(face==null)
						face = z==-1 ? Direction.SOUTH : z==d ? Direction.NORTH : null;
					
					fillDownExcept(w, xyz.below(), config.getUnderMaterial(), face);
				}
			}
		}	
		
//		int e = 1;
//
//		for(int x=-e;x<ww+e;x++)
//		{
//			if(x==-e || x==ww+e-1)
//			{
//				for(int z=-e;z<d+e;z++)
//				{
//					BlockPos xyz = start.add(x,h,z);
//					Biome bio = w.getBiome(xyz);
//					fillDown(w, xyz.down(), bio.fillerBlock);
//				}
//			}
//			else
//			{
//				BlockPos xyz = start.add(x,h,-e);
//				Biome bio = w.getBiome(xyz);
//				fillDown(w, xyz.down(), bio.fillerBlock);
//					
//				xyz = start.add(x,h,d+e);
//				bio = w.getBiome(xyz);
//				fillDown(w, xyz.down(), bio.fillerBlock);
//			}
//		}
	}
	
	private void fillDownExcept(World w, BlockPos pos, BlockState state, Direction face)
	{
		BlockPos xyz = pos;
		if(w.isEmptyBlock(xyz))
		{
			while(w.isEmptyBlock(xyz))
			{
				w.setBlock(xyz, state, 2);
				xyz = xyz.below();
			}
			if(Direction.NORTH != face && w.isEmptyBlock(pos.north()) && w.isEmptyBlock(pos.north().below()))
				fillDownExcept(w, pos.below(1).north(), state, face);
			if(Direction.SOUTH != face && w.isEmptyBlock(pos.south()) && w.isEmptyBlock(pos.south().below()))
				fillDownExcept(w, pos.below(1).south(), state, face);
			if(Direction.WEST  != face && w.isEmptyBlock(pos.west( )) && w.isEmptyBlock(pos.west( ).below()))
				fillDownExcept(w, pos.below(1).west( ), state, face);
			if(Direction.EAST  != face && w.isEmptyBlock(pos.east( )) && w.isEmptyBlock(pos.east( ).below()))
				fillDownExcept(w, pos.below(1).east( ), state, face);
		}
	}
	
	
	public void addChestContentBase(IServerWorld w, BlockPos start, Random rand, CompoundNBT extraData, LootTableManager manager)
	{
		for(DungeonChest ints : chests)
		{
			ints.genLoot(w, rand, start, extraData, manager);
		}
	}

	public MutableBoundingBox getBoundingBox(BlockPos start)
	{
		return new MutableBoundingBox(start, start.offset(getWidth()-1,getHeight()-1,getDepth()-1));
	}
	
	@Override
	public String toString()
	{
		return String.format("StructureBase(name=%s,%s,%s,%s|Chest:%s|Doors:%s)",name, getWidth(),getHeight(),getDepth(), chests==null?0 : chests.length, doors==null?0:doors.length);
	}
	
	public BlockState[][][] getBlocks()
	{
		return this.blocks;
	}
	
	public IDungeonEventListener getEventListener()
	{
		return null;
	}
}
