package futurepack.common.dim.structures;

import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.loot.LootTableManager;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.IWorldWriter;

public class StructureTecDungeon extends StructureBase 
{

	public StructureTecDungeon(StructureBase base) 
	{
		super(base);
	}

	@Override
	public void generateBase(IWorldWriter w, BlockPos start) 
	{
		super.generateBase(w, start);
		
		if(w instanceof IBlockReader)
		{
			IBlockReader read = (IBlockReader) w;
			
			for(OpenDoor door : getRawDoors())
			{
				if(door.getDepth()==1 && door.getWeidth()==1 && door.getHeight()==1)
				{
					if(door.getDirection() == Direction.UP)
					{
						int x = door.getPos().getX();
						int y = door.getPos().getY();
						int z = door.getPos().getZ();
						BlockState state = getBlocks()[x][y][z];
						if(state!=null)
						{
							BlockState s1,s2,s3,s4;
							s1 = getBlocks()[x+1][y][z];
							s2 = getBlocks()[x-1][y][z];
							s3 = getBlocks()[x][y][z+1];
							s4 = getBlocks()[x][y][z-1];
							
							BlockPos.Mutable mut = new BlockPos.Mutable().set(start);
							mut.move(x, y, z);
							while(mut.getY() < 250)
							{
								mut.move(Direction.UP);
								if(read.getBlockState(mut).isAir())
								{
									break;
								}
								else
								{
									w.setBlock(mut, state, 2);
									w.setBlock(mut.move(1,0,0), s1, 2);
									w.setBlock(mut.move(-2,0,0), s2, 2);
									w.setBlock(mut.move(1,0,1), s3, 2);
									w.setBlock(mut.move(0,0,-2), s4, 2);
									mut.move(0, 0, 1);
								}
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	public void addChestContentBase(IServerWorld w, BlockPos start, Random rand, CompoundNBT extraData, LootTableManager manager)
	{		
		super.addChestContentBase(w, start, rand, extraData, manager);
	}
}
