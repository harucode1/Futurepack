package futurepack.common.dim.structures;

import futurepack.common.block.misc.TileEntityDungeonCore;
import futurepack.common.dim.structures.generation.BakedDungeon;
import futurepack.common.dim.structures.generation.IDungeonEventListener;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.server.ServerWorld;

public class StructureEndRoom extends StructureBase implements IDungeonEventListener
{

	public StructureEndRoom(StructureBase base)
	{
		super(base);
	}

	
	@Override
	public IDungeonEventListener getEventListener()
	{
		return this;
	}


	@Override
	public void onDungeonFinished(ServerWorld w, BakedDungeon dungeon, BlockPos pos)
	{
		MutableBoundingBox box = dungeon.getBoundingBox(pos);
		
		TileEntityDungeonCore[] obj = w.blockEntityList.stream()
				.filter(t -> t.getClass() == TileEntityDungeonCore.class)
				.filter(t -> box.isInside(t.getBlockPos()))
				.toArray(TileEntityDungeonCore[]::new);
		for(TileEntityDungeonCore core : obj)
		{
			core.setDungeon(dungeon);
		}
	}
}
