package futurepack.common.dim.structures;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import futurepack.common.FuturepackTags;
import futurepack.common.block.misc.TileEntityForceField;
import futurepack.common.block.modification.InventoryModificationBase;
import futurepack.common.block.modification.TileEntityLaserBase;
import futurepack.common.item.ComputerItems;
import futurepack.common.item.CraftingItems;
import futurepack.common.item.misc.ItemResearchBlueprint;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchLoader;
import net.minecraft.block.BlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootTableManager;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IServerWorld;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class StructureTecLock extends StructureBase
{
	private BlockPos[] laser;
	private BlockPos[] force;
	private BlockPos[] chests;
	
	public StructureTecLock(StructureBase base)
	{
		super(base);
		BlockState[][][] blocks = getBlocks();
		
		ArrayList<BlockPos> laser = new ArrayList<BlockPos>();
		ArrayList<BlockPos> force = new ArrayList<BlockPos>();
		ArrayList<BlockPos> chests = new ArrayList<BlockPos>();
		
		for(int x=0;x<blocks.length;x++)
		{
			for(int y=0;y<blocks[x].length;y++)
			{
				for(int z=0;z<blocks[x][y].length;z++)
				{
					if(blocks[x][y][z]!=null)
					{
						if(blocks[x][y][z].is(FuturepackTags.force_field))
						{
							force.add(new BlockPos(x,y,z));
						}
						if(blocks[x][y][z].is(FuturepackTags.entity_laser))
						{
							laser.add(new BlockPos(x,y,z));
						}
						if(blocks[x][y][z].is(FuturepackTags.wardrobe))
						{
							chests.add(new BlockPos(x,y,z));
						}
					}
				}
			}
		}
		this.laser = laser.toArray(new BlockPos[laser.size()]);
		this.force = force.toArray(new BlockPos[force.size()]);
		this.chests = chests.toArray(new BlockPos[chests.size()]);
	}

	@Override
	public void addChestContentBase(IServerWorld w, BlockPos start, Random rand, CompoundNBT extraData, LootTableManager manager)
	{
		super.addChestContentBase(w, start, rand, extraData, manager);
		
		int tecLvl = extraData.getInt("tecLockLevel");
		
		for(BlockPos pos : laser)
		{
			TileEntity e = w.getBlockEntity(start.offset(pos));
			if(e!=null)
			{
				TileEntityLaserBase base = (TileEntityLaserBase) e;
				
				InventoryModificationBase inv = base.getInventory();
				inv.core.set(0, new ItemStack(ComputerItems.dungeon_core, 1));
				inv.chipset.set(0, new ItemStack(ComputerItems.ai_chip, 1));
				inv.ram.set(0, new ItemStack(ComputerItems.dungeon_ram, 1));
				
				base.setConfig("attack.player", true);
				base.setConfig("attack.neutral", true);
				base.setConfig("attack.mobs", true);
				
				switch (tecLvl)
				{
				case 3:
					base.setConfig("wait.10", true);
					break;
				case 2:
					base.setConfig("wait.15", true);
					break;
				case 1:
					base.setConfig("wait.30", true);
					break;
				case 0:
					base.setConfig("wait.60", true);
					break;
				default:
					break;
				}
			}
		}
		
		for(BlockPos pos : force)
		{
			TileEntity e = w.getBlockEntity(start.offset(pos));
			if(e!=null)
			{
				TileEntityForceField tile = (TileEntityForceField) e;
				tile.neededItem = getKey(tecLvl);
			}
		}
		
		ItemStack blueprint = getBlueprint(tecLvl);
		
		Upper:
		for(BlockPos pos : chests)
		{
			TileEntity e = w.getBlockEntity(start.offset(pos));
			if( e!=null)
			{
				LazyOptional<IItemHandler> opt = e.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, Direction.DOWN);
				if(opt.isPresent())
				{
					IItemHandler handler = opt.orElseThrow(NullPointerException::new);
					int slots = handler.getSlots();
					for(int i=0;i<slots;i++)
					{
						blueprint = handler.insertItem(i, blueprint, false);
						if(blueprint.isEmpty())
						{
							break Upper;
						}
					}
				}
			}	
		}
		
	}
	
	private ItemStack getKey(int tecLvl)
	{
		Item[] keys = new Item[]{CraftingItems.dungeon_key_0, CraftingItems.dungeon_key_1, CraftingItems.dungeon_key_2, CraftingItems.dungeon_key_3};
		return new ItemStack(keys[tecLvl]);
	}
	
	private ItemStack getBlueprint(int tecLvl)
	{
		ItemStack key = getKey(tecLvl);
		Set<Research> set = ResearchLoader.getReqiredResearch(key);
		if(set == null || set.isEmpty())
		{
			ItemStack paper = new ItemStack(Items.PAPER);
			paper.setHoverName(new StringTextComponent("This should be a Blueprint"));
			CompoundNBT display = paper.getOrCreateTagElement("display");
			ListNBT list = new ListNBT();
			list.add(StringNBT.valueOf("This should be the Blueprint with enalbes you"));
			list.add(StringNBT.valueOf(key.toString()));
			display.put("Lore", list);
			return paper;
		}
		
		return ItemResearchBlueprint.getItemForResearch(set.stream().findAny().get());
	}
}
