package futurepack.common.entity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Stream;

import com.google.common.base.Predicates;

import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import net.minecraft.client.Minecraft;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.World;

public class LazyAStar
{	
	private int maxDisSq = 256*256;
	
	protected ArrayList<ParentCoords> openPoints = new ArrayList<ParentCoords>();
	protected HashMap<ParentCoords, Boolean> state = new HashMap<ParentCoords, Boolean>();
	protected final World w;
	protected final BlockPos target;
	protected final BlockPos start;
	
	private ParentCoords finished;
	
	public AxisAlignedBB hitBox;
	
	public LazyAStar(World w, BlockPos start, BlockPos target)
	{
		openPoints.add(new ParentCoords(start, null));
		this.w=w;
		this.target = target;
		this.start = start;
		double dis = start.distSqr(target);
		if(dis > maxDisSq)
		{
			maxDisSq = (int) (2 * dis);
		}
		finished = null;
		hitBox = new AxisAlignedBB(0, 0, 0, 1, 1, 1);
	}
	
	
	/**
	 * 
	 * @param timeoutMillis amount of time in milliseconds this operation should pause after
	 * @return true if it has finished, false if not
	 */
	public EnumPathSearch searchPathWithTimeout(int timeoutMillis)
	{
		long ti = System.currentTimeMillis(); 
		
		if(finished!=null)
		{
			return EnumPathSearch.FOUND;
		}
		if(!isFreeBlock(start))
		{
			return EnumPathSearch.START_BLOCKED;
		}
		if(!isFreeBlock(target))
		{
			return EnumPathSearch.TARGET_BLOCKED;
		}
		
		while(!openPoints.isEmpty())
		{
			Iterator<ParentCoords> iter = openPoints.iterator();
			openPoints = new ArrayList<ParentCoords>();
			while(iter.hasNext())
			{
				ParentCoords par = iter.next();
				if(allDirections(par))
				{
					finished = par;
					return EnumPathSearch.FOUND;
				}
				
				long gone = System.currentTimeMillis() - ti;
				if(gone >= timeoutMillis)
				{
					return EnumPathSearch.TIMEOUT;
				}
			}
		}
		
		ParentCoords min = null;
		double dis = -1;
		for(Entry<ParentCoords, Boolean> e : state.entrySet())	
		{
			double dis2 = target.distSqr(e.getKey());
			if(dis2 < dis || min==null)
			{
				dis = dis2;
				min = e.getKey();
			}
		}
		
		finished = new ParentCoords(target, min);
		
		return EnumPathSearch.FOUND;
	}
	
	public ParentCoords getFinish()
	{
		if(w.isClientSide)
		{
			showPath();
		}
		return finished;
	}
	
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	protected void showPath()
	{
		if(Minecraft.getInstance().options.renderDebug)
		{
			ParentCoords pos = finished;
			while(pos != null)
			{
				w.addParticle(ParticleTypes.BARRIER, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 0F, 0F, 0F);
				pos = pos.getParent();
			}
		}
	}
	
	
	protected boolean isFreeBlock(BlockPos pos)
	{
		AxisAlignedBB box = hitBox.move(pos);
		//getCollisionShapes
		Stream<AxisAlignedBB> l = w.getBlockCollisions(null, box).map(VoxelShape::toAabbs).flatMap(List::stream).filter(Predicates.notNull());
		Optional<AxisAlignedBB> collided = l.filter(box::intersects).findAny();
		return !collided.isPresent();
	}
	
	protected boolean isFreeBlock(BlockPos pos, Direction insert)
	{
		BlockPos p2 = pos.relative(insert, -1);
		AxisAlignedBB box = hitBox.move(pos).minmax(hitBox.move(p2));
		//getCollisionShapes
		Stream<AxisAlignedBB> l = w.getBlockCollisions(null, box).map(VoxelShape::toAabbs).flatMap(List::stream).filter(Predicates.notNull());
		Optional<AxisAlignedBB> collided = l.filter(box::intersects).findAny();
		return !collided.isPresent();
	}
	
	protected boolean allDirections(ParentCoords coords)
	{
		if(coords.equals(target))
		{
			return true;
		}
		else if(coords.distSqr(start) > maxDisSq)
		{
			return false;
		}
		
		ArrayList<ParentCoords> valid = new ArrayList<ParentCoords>();
		for(Direction fac : FacingUtil.VALUES)
		{
			ParentCoords pos = new ParentCoords(coords.relative(fac), coords);
			if(state.get(pos)!=null)
			{
				continue;
			}
			if(isFreeBlock(pos, fac) && isPositionkValid(coords, fac, pos))
			{
				valid.add(pos);
				state.put(pos, true);
			}
			else
			{
				state.put(pos, false);
			}
				
		}
		openPoints.addAll(valid);
		openPoints.sort(new Comparator<ParentCoords>()
		{
			@Override
			public int compare(ParentCoords o1, ParentCoords o2)
			{			
				return (int) (o1.distSqr(target) - o2.distSqr(target));
			}
		});
		
//		if(!valid.isEmpty())
//		{
//			return allDirections(valid.get(0));
//		}
		return false;
	}

	/**
	 * 
	 * @param base the previous Position that is in this Path
	 * @param dir the Direction <b>base</b> was moved to get to <b>pos</b>
	 * @param pos the Position to test
	 * @return if <b>pos</b> is a valid position
	 */
	protected boolean isPositionkValid(ParentCoords base, Direction dir, ParentCoords pos)
	{
		return true;
	}
	
	public void clear()
	{
		openPoints.clear();
		openPoints = null;
		state.clear();
		state = null;
	}
	
	public enum EnumPathSearch
	{
		FOUND,
		TIMEOUT,
		START_BLOCKED,
		TARGET_BLOCKED;
	}
}
