package futurepack.common.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public abstract class EntityNeonPowered extends Entity
{
	private static final DataParameter<Float> power = EntityDataManager.defineId(EntityNeonPowered.class, DataSerializers.FLOAT);

	/**
	 * This is the Maximal amount of power that can be stored
	 */
	private final float maxPowerStorage;
	
	protected boolean consumePowerForTick = true;
	
	public EntityNeonPowered(EntityType<? extends EntityNeonPowered> type, World w, float maxStorage)
	{
		super(type, w);
		maxPowerStorage = maxStorage;
	}

	@Override
	protected void defineSynchedData()
	{
		this.entityData.define(power, 0F);//power
	}

	@Override
	protected void readAdditionalSaveData(CompoundNBT nbt)
	{
		setPower(nbt.getFloat("power"));
	}

	@Override
	protected void addAdditionalSaveData(CompoundNBT nbt)
	{
		nbt.putFloat("power", getPower());
	}
	
	public float getPower()
	{
		return this.entityData.get(power);
	}	
	public void setPower(float f)
	{
		this.entityData.set(power, f);
	}

	public float getMaxPower()
	{
		return maxPowerStorage;
	}
	
	/**
	 * @return how many Energie used per Tick
	 */
	protected abstract float getEnergieUse();
	
	public boolean consumePower()
	{
		float power = getPower();
		if(power>getEnergieUse())
		{
			power-= getEnergieUse();
			setPower(power);
			return true;
		}
		return false;
	}
	
	@Override
	public void tick() 
	{
		if(getPower()<getMaxPower())
		{
			tryCharge();
		}
		if(consumePowerForTick)
		{
			if(consumePower())
			{
				super.tick();
			}
		}
		else
		{
			super.tick();
		}
	}
	
	protected abstract void tryCharge();

	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
