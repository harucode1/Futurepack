package futurepack.common.entity;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeMod;

public class NavigatorWallCrawler extends Navigator 
{

	public NavigatorWallCrawler(LivingEntity entity) 
	{
		super(entity);
	}

	@Override
	protected LazyAStar initPathSearch(World w, BlockPos start, BlockPos target) 
	{
		return new WallCrawlerPathFinder(w, start, target);
	}
	
	@Override
	public boolean calcRotaion(Vector3d vec) 
	{
		super.calcRotaion(vec);
		return true;
	}
	
	@Override
	protected Vector3d getMotionToBlock() 
	{
		Vector3d entity = this.entity.position();
		Vector3d mot = Vector3d.atLowerCornerOf(getTarget()).add(0.5, this.entity.getEyeHeight(), 0.5).subtract(entity);
		if(mot.lengthSqr()>0.1)
		{
			mot = mot.normalize();
		}
		double gravity = ((LivingEntity)this.entity).getAttribute(ForgeMod.ENTITY_GRAVITY.get()).getValue();
		mot = mot.add(0, -gravity, 0);
		return mot;
	}
	
	public boolean isFinished()
	{
		if(target.isEmpty())
			return true;
		
		BlockPos target = this.getTarget();
		AxisAlignedBB bb  =new AxisAlignedBB(0.4, 0.0, 0.4, 0.6, 0.2, 0.6).move(target).move(0.0, this.entity.getEyeHeight(), 0.0);		
		if(bb.contains(entity.position()))
		{
			nextTarget();
			return this.target.isEmpty();
		}
		return false;
	}
}
