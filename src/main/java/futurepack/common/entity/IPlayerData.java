package futurepack.common.entity;

import net.minecraft.nbt.CompoundNBT;

public interface IPlayerData
{
	public CompoundNBT getTag();
	
	public void addAll(CompoundNBT nbt);
}
