package futurepack.common.entity.monocart;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class EntityMonocart extends EntityMonocartBase
{
	private final ItemStackHandler handler;
	private final ItemStackHandler energeHandler;
	
	public static final byte isInventoryEmpty = 8;
	
	public EntityMonocart(World w)
	{
		this(FPEntitys.MONOCART, w);
	}	
	
	public EntityMonocart(EntityType<EntityMonocart> type, World w)
	{
		super(type, w);
		handler = new ItemStackHandler(27);
		energeHandler = new EnergyHandler();
	}	

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
	}
	
	@Override
	public boolean hurt(DamageSource ds, float par2) 
	{
		if(ds.getDirectEntity() != null)
		{
			if(!ds.getDirectEntity().hurt(ds, par2))
			{
				return ds.getDirectEntity().hurt(FuturepackMain.NEON_DAMAGE, par2);
			}
			return true;
		}
		return super.hurt(ds, par2);
	}
	
	@Override
	public ActionResultType interact(PlayerEntity pl, Hand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(it != null && it.getItem() == ToolItems.scrench && !level.isClientSide)
		{
			
			ItemStack stack = new ItemStack(ToolItems.monocartbox);
			stack.setTag(new CompoundNBT());
			CompoundNBT nbt = new CompoundNBT();
			this.saveAsPassenger(nbt);
			
			stack.getTag().put("entity", nbt);
			stack.setHoverName(this.getName());
			ItemEntity ei = new ItemEntity(level,getX(),getY(),getZ(), stack);
			
			level.addFreshEntity(ei);
			remove();
			return ActionResultType.SUCCESS;
		}
		else
		{
			if(HelperResearch.canOpen(pl, this))
			{
				FPGuiHandler.MONOCART.openGui(pl, this);
				return ActionResultType.SUCCESS;
			}
		}
		return super.interact(pl, hand);
	}
	
	@Override
	public void addAdditionalSaveData(CompoundNBT nbt)
	{
		super.addAdditionalSaveData(nbt);
		nbt.put("itemContainer", handler.serializeNBT());
		nbt.put("energyContainer", energeHandler.serializeNBT());
	}
	
	@Override
	public void readAdditionalSaveData(CompoundNBT nbt)
	{
		super.readAdditionalSaveData(nbt);
		handler.deserializeNBT(nbt.getCompound("itemContainer"));
		energeHandler.deserializeNBT(nbt.getCompound("energyContainer"));
	}
	
	
	@Override
	public void baseTick()
	{
		super.baseTick();
		
		float f = getPower() / getMaxPower();
		
		for(int i=0;i<energeHandler.getSlots();i++)
		{
			ItemStack it = energeHandler.getStackInSlot(i);
			if(it!=null && !it.isEmpty())
			{
				if(it.getItem() instanceof IItemNeon)
				{
					IItemNeon neon = (IItemNeon) it.getItem();
//					if(neon.isNeonable(it))
					{
						if(f > 0.8 && neon.getNeon(it) < neon.getMaxNeon(it))
						{
							neon.addNeon(it, 1);
							setPower(getPower()-1);
						}
						else if(neon.getNeon(it) > 0 && f<0.2F)
						{
							neon.addNeon(it, -1);
							setPower(getPower()+1);
						}
					}
				}
			}
		}
		
		if(!level.isClientSide)
		{
			boolean empty = true;
			for(int i=0;i<handler.getSlots();i++)
			{
				if(!handler.getStackInSlot(i).isEmpty())
				{
					empty = false;
					break;
				}
			}
			if(isInventotyEmpty() != empty)
			{
				setInventotyEmpty(empty);
			}
			
			if(empty && !this.isVehicle())
			{
				List<LivingEntity> list = level.getEntitiesOfClass(LivingEntity.class, this.getBoundingBox());
				if(!list.isEmpty())
				{
					list.get(0).startRiding(this);
				}
			}
		}
	}
	
	public boolean isInventotyEmpty()
	{
		return getBool(this.entityData.get(state), isInventoryEmpty);
	}
	public void setInventotyEmpty(boolean b)
	{
		if(!b && this.isVehicle())
		{
			this.ejectPassengers();
		}
			
		int by = this.entityData.get(state);
		this.entityData.set(state, setByte(by, isInventoryEmpty, b));
	}

	@Override
	protected boolean canAddPassenger(Entity passenger)
	{
		return isInventotyEmpty() && super.canAddPassenger(passenger);	
	}
	
	@Override
	public double getPassengersRidingOffset()
	{
		return super.getPassengersRidingOffset()-0.75;
	}
	
	private LazyOptional<IItemHandler> item_opt = null;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(item_opt!=null)
			{
				return (LazyOptional<T>) item_opt;
			}
			else
			{
				item_opt = LazyOptional.of(()->handler);
				item_opt.addListener(p -> item_opt = null);
				return (LazyOptional<T>) item_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	public ItemStackHandler getGui()
	{
		return handler;
	}
	
	public ItemStackHandler getEnergyHandler()
	{
		return energeHandler;
	}
	
	public class EnergyHandler extends ItemStackHandler
	{
		private EnergyHandler()
		{
			super(6);
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			if(isItemValid(slot, stack))
			{
				return super.insertItem(slot, stack, simulate);
			}
			return stack;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack it)
		{
			return it!=null && it.getItem() instanceof IItemNeon;
		}
	}
}
