package futurepack.common.entity.monocart;

import futurepack.api.ParentCoords;

public class EntryWaypoint
{
	public final String name;
	public final ParentCoords coords;
	
	public EntryWaypoint(String name, ParentCoords coords)
	{
		this.name = name;
		this.coords = coords;
	}
	
	@Override
	public int hashCode()
	{
		return coords.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof EntryWaypoint)
		{
			return coords.equals(((EntryWaypoint) obj).coords);
		}
		return false;
	}
}
