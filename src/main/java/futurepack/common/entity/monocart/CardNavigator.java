package futurepack.common.entity.monocart;

import java.util.LinkedList;

import futurepack.api.ParentCoords;
import futurepack.common.block.logistic.monorail.BlockMonorailBasic;
import futurepack.common.block.logistic.monorail.BlockMonorailBasic.EnumMonorailStates;
import net.minecraft.block.BlockState;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.particles.RedstoneParticleData;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.server.ServerWorld;

public class CardNavigator
{
	private final EntityMonocartBase cart;
	private LinkedList<Vector3d> path; 
	
	public CardNavigator(EntityMonocartBase par1)
	{
		cart = par1;
	}
	
	/**
	 * Clears the current path and calculate the new one.
	 * @param target
	 */
	public void initPath(ParentCoords target)
	{
		path = new LinkedList<Vector3d>();
		while(target!=null)
		{
			BlockState state = cart.level.getBlockState(target);
			if(state.getBlock() instanceof BlockMonorailBasic)
			{
				EnumMonorailStates es = state.getValue(BlockMonorailBasic.getPropertie((BlockMonorailBasic) state.getBlock()));
				if(es.isYLoocked() || path.isEmpty())
				{
					path.addFirst(convert(target));
				}
				else
				{
					Vector3d vec1=new Vector3d(target.getX(), target.getY(), target.getZ());
					Vector3d vec2=new Vector3d(target.getX(), target.getY(), target.getZ());
					switch(es)
					{
					case ASCENDING_EAST:
						vec1 = vec1.add(0, 0.5, 0.5);
						vec2 = vec2.add(1, 1.5, 0.5);						
						break;
					case ASCENDING_NORTH:
						vec1 = vec1.add(0.5, 0.5, 1);
						vec2 = vec2.add(0.5, 1.5, 0);						
						break;
					case ASCENDING_SOUTH:
						vec1 = vec1.add(0.5, 0.5, 0);
						vec2 = vec2.add(0.5, 1.5, 1);						
						break;
					case ASCENDING_WEST:
						vec1 = vec1.add(1, 0.5, 0.5);
						vec2 = vec2.add(0, 1.5, 0.5);						
						break;
					case UP_DOWN:
						vec1 = vec1.add(0.5, 0.25, 0.5);
						vec2 = vec2.add(0.5, 0.75, 0.5);	
						break;
					case NORTH_EAST_SOUTH_WEST_DOWN:
						vec1 = vec1.add(0.5, 0.5, 0.5);
						vec2 = vec2.add(0.5, 0, 0.5);
						break;
					case NORTH_EAST_SOUTH_WEST_UP:
						vec1 = vec1.add(0.5, 1, 0.5);
						vec2 = vec2.add(0.5, 0.5, 0.5);				
						break;
					default:break;
					}
					
					if(vec1.distanceToSqr(path.getFirst()) > vec2.distanceToSqr(path.getFirst()))
					{
						addChecked(vec2);
						addChecked(vec1);
					}
					else
					{
						addChecked(vec1);
						addChecked(vec2);
					}
				}
				
				if(cart.level.isClientSide)
				{
					
					cart.level.addParticle(new RedstoneParticleData(1, 0, 0, 1), target.getX()+0.5, target.getY()+0.5, target.getZ()+0.5, 0, 0, 0);
				}
			}
			
			target = target.getParent();
		}
	}
	
	private void addChecked(Vector3d vec)
	{
		Vector3d now = path.getFirst();
		if(!now.equals(vec))
		{
			if(cart.level.isClientSide)
			{
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
				cart.level.addParticle(ParticleTypes.MYCELIUM, vec.x, vec.y, vec.z, 0, 0, 0);
			}
			path.addFirst(vec);
		}
	}
	
	protected Vector3d getPathEnd()
	{
		return this.path.getLast();
	}
	
	protected Vector3d getNext()
	{
		return path.getFirst();
	}
	
	public void doTasks()
	{
			
//		BlockPos pos = new BlockPos(cart);
		
		if(path!=null)
		{
			Vector3d next = getNext();
			
			if(!cart.canPass(next))
			{
				cart.setDeltaMovement(Vector3d.ZERO);
				deletePath();
				
				
				if(cart.level instanceof ServerWorld)
				{
					((ServerWorld)cart.level).sendParticles(ParticleTypes.BARRIER, next.x, next.y, next.z, 1, 0F,0F,0F, 0.1);
				}
				
				return;
			}
			
			Vector3d motion = getMotionToBlock(next);
			calcRotaion(motion);
			
			float f = cart.getSpeed();

			cart.setDeltaMovement(motion.scale(f));
			
			AxisAlignedBB bb = cart.getBoundingBox();
			bb = bb.inflate(Math.abs(cart.getDeltaMovement().x), Math.abs(cart.getDeltaMovement().y), Math.abs(cart.getDeltaMovement().z)).move(0, -0.2, 0);
			boolean flag1 = bb.contains(next);
			if(flag1)
			{
				path.removeFirst();
				if(path.isEmpty())
				{
					path = null;
					if(cart.level instanceof ServerWorld)
					{
						((ServerWorld)cart.level).sendParticles(ParticleTypes.HEART, next.x, next.y, next.z, 1, 0F,0F,0F, 0.1);
					}
				}
			}
		}
		else
		{
			
			cart.setDeltaMovement(0, cart.getDeltaMovement().y, 0);
		}
	}
	
	
	
	private Vector3d getMotionToBlock(Vector3d pos)
	{
		Vector3d entity = cart.position();
		return  pos.subtract(entity).normalize();
	}
	
	private void calcRotaion(Vector3d vec)
	{
			
		double pitch = -Math.asin(vec.y); //neigung
		double yaw = -Math.atan2(vec.x, vec.z); //drehwinkel
		cart.xRot = (float) Math.toDegrees(pitch);
		cart.yRot = (float) Math.toDegrees(yaw);
		
		int[] pitch1 = new int[]{45,0,-45};
		int[] yaw1 = new int[]{0, 90, 180, -90, -180};
		
		BlockPos pos = cart.blockPosition();
		if(cart.level.isEmptyBlock(pos))
		{
			pos = pos.below();
		}
		BlockState state = cart.level.getBlockState(pos);
				
		if(state.getBlock() instanceof BlockMonorailBasic)
		{
			EnumMonorailStates es = state.getValue(BlockMonorailBasic.getPropertie((BlockMonorailBasic) state.getBlock()));
			pitch1 = es.getPitch();
		}
		cart.xRot = getNearest(Math.round(cart.xRot), pitch1);
	}
	
	private int getNearest(int t1, int... ints)
	{
		if(ints.length==0)
			return t1;
			
		int[] t2 = new int[ints.length];
		for(int i=0;i<ints.length;i++)
		{
			t2[i] = Math.abs(ints[i] - t1);
		}
		
		int pos = 0;
		for(int i=0;i<t2.length;i++)
		{
			if(t2[i] < t2[pos])
			{
				pos = i;
			}
		}
		
		return ints[pos];
	}
	

	public boolean pathless()
	{
		return path==null;
	}

	public void deletePath()
	{
		path = null;
	}
	
	private static Vector3d convert(BlockPos pos)
	{
		return pos==null ? null : new Vector3d(pos.getX()+0.5, pos.getY()+0.4, pos.getZ()+0.5);
	}
}
