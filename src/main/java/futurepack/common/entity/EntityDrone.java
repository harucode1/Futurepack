package futurepack.common.entity;

import java.util.ArrayList;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FuturepackTags;
import futurepack.common.block.inventory.TileEntityDroneStation;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.world.protection.FPDungeonProtection;
import net.minecraft.block.BlockState;
import net.minecraft.block.FlowingFluidBlock;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.World;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemHandlerHelper;

public abstract class EntityDrone extends EntityNeonPowered
{
	private static final DataParameter<BlockPos> inventory = EntityDataManager.defineId(EntityDrone.class, DataSerializers.BLOCK_POS);
	private static final DataParameter<Direction> side = EntityDataManager.defineId(EntityDrone.class, DataSerializers.DIRECTION);
	
	private static final DataParameter<Float> mining_progress = EntityDataManager.defineId(EntityDrone.class, DataSerializers.FLOAT);
	private static final DataParameter<String> claime = EntityDataManager.defineId(EntityDrone.class, DataSerializers.STRING);
	private static final DataParameter<Boolean> done =  EntityDataManager.defineId(EntityDrone.class, DataSerializers.BOOLEAN);
	private static final DataParameter<Boolean> working =  EntityDataManager.defineId(EntityDrone.class, DataSerializers.BOOLEAN);
	private static final DataParameter<Boolean> repeat =  EntityDataManager.defineId(EntityDrone.class, DataSerializers.BOOLEAN);
	

	public ArrayList<String> todo = new ArrayList<String>();	
	public int currentDone;
	
	
	public EntityDrone(EntityType<? extends EntityDrone> type, World w, float maxStorage)
	{
		super(type, w, maxStorage);
	}

	@Override
	protected void defineSynchedData() 
	{
		super.defineSynchedData();

		this.entityData.define(inventory, new BlockPos(0,0,0) );//inventory
		this.entityData.define(side, null );//side
		
		this.entityData.define(mining_progress, 0F);
		this.entityData.define(claime, "");
		this.entityData.define(done, false);//done
		this.entityData.define(working, false);//working
		this.entityData.define(repeat, false);//repeat
	}
	
	@Override
	public void readAdditionalSaveData(CompoundNBT nbt)
	{
		super.readAdditionalSaveData(nbt);
		setInventoryPos(new BlockPos(nbt.getInt("InventoryX"), nbt.getInt("InventoryY"), nbt.getInt("InventoryZ")));
		setSide(Direction.from3DDataValue(nbt.getByte("side")));
		
		this.entityData.set(mining_progress, nbt.getFloat("mining_progress"));
		this.entityData.set(claime, nbt.getString("curentclaime"));//Note: dont use direct methods because this would cause a teleport of the entity and a break;
		this.entityData.set(working, nbt.getBoolean("working"));
		setDone(nbt.getBoolean("done"));
		
		todo.clear();
		ListNBT list = nbt.getList("claimes", 8);
		for(int i=0;i<list.size();i++)
		{
			todo.add(list.getString(i));
		}
		currentDone = nbt.getInt("claimesCount");
		setRepeat(nbt.getBoolean("repeat"));
	}

	@Override
	public void addAdditionalSaveData(CompoundNBT nbt)
	{
		super.addAdditionalSaveData(nbt);
		BlockPos c = getInventoryPos();
		nbt.putInt("InventoryX", c.getX());
		nbt.putInt("InventoryY", c.getY());
		nbt.putInt("InventoryZ", c.getZ());
		nbt.putByte("side", (byte) getSide().ordinal());
		nbt.putFloat("mining_progress", getMiningProgress());
		nbt.putString("curentclaime", getClaime());
		nbt.putBoolean("working", isWorking());
		nbt.putBoolean("done", isDone());
		
		ListNBT list = new ListNBT();
		for(int i=0;i<todo.size();i++)
		{
			list.add(StringNBT.valueOf(todo.get(i)));
		}
		nbt.put("claimes", list);
		nbt.putInt("claimesCount", currentDone);
		nbt.putBoolean("repeat", isRepeat());
	}
	
	protected void setInventoryPos(BlockPos pos)
	{
		this.entityData.set(inventory, pos);
	}	
	
	public BlockPos getInventoryPos()
	{	
		return this.entityData.get(inventory);
	}
	
	public void setSide(Direction f)
	{
		this.entityData.set(side, f);
	}
	
	protected Direction getSide()
	{
		return this.entityData.get(side);
	}
	
	@Override
	protected void tryCharge()
	{
		if(!level.isClientSide)
		{
			BlockPos c = getInventoryPos();
			Direction dir = getSide();
			BlockPos xyz = c.relative(dir,-1);
			
			TileEntity t = level.getBlockEntity(xyz);
			IItemHandler handler = HelperInventory.getHandler(t, dir);
			if(handler!=null)
			{
				for(int i=0;i<handler.getSlots();i++)
				{
					ItemStack it = handler.getStackInSlot(i);
					if(it!=null && it.getItem() instanceof IItemNeon)
					{
						if(handler instanceof IItemHandlerModifiable)
						{
							IItemHandlerModifiable mod = (IItemHandlerModifiable) handler;
							ItemStack is = it.copy();
							IItemNeon ch = (IItemNeon) is.getItem();
							if(ch.getNeon(is) >= 10)
							{
								ch.addNeon(is, -1);
								setPower(getPower() + 1F);
							}
							mod.setStackInSlot(i, is);					
							
							
						}
						else
						{
							ItemStack is = handler.extractItem(i, 1, false);
							if(is!=null)
							{
								IItemNeon ch = (IItemNeon) is.getItem();
								if(ch.getNeon(is) >= 10)
								{
									ch.addNeon(is, -1);
									setPower(getPower() + 1F);															
								}
								ItemStack rest = handler.insertItem(i, is, false);
								if(rest!=null)
								{
									rest = ItemHandlerHelper.insertItem(handler, rest, false);
									if(rest!=null)
									{
										ItemEntity itemE = new ItemEntity(level, getX(), getY(), getZ(), rest);
										level.addFreshEntity(itemE);
									}
								}
							}
						}
					}
					
					if(getPower()>getMaxPower())
						return;
				}
			}
		}
	}
	
	
	
	protected boolean isHomeBlock(BlockPos pos)
	{
		BlockPos c = getInventoryPos();
		Direction dir = getSide();
		
		return c.relative(dir).equals(pos);
	}
	
	protected float getMiningProgress()
	{
		return this.entityData.get(mining_progress);
	}	
	
	protected void addMiningProgress()
	{
		this.entityData.set(mining_progress, getMiningProgress()+getMiningProgressModifier());
	}	
	
	protected void resetMiningProgress()
	{
		this.entityData.set(mining_progress, 0F);
	}
	
	protected abstract float getMiningProgressModifier();
	
	public void setClaime(String s)
	{
		this.entityData.set(claime, s);
		if(isWorking())
		{
			ClaimeData d = ClaimeData.getCurrentData(this);
			BlockPos c = d.c;
			setPos(c.getX()+0.5, c.getY()+0.2 , c.getZ()+0.5);
			setDone(false);
		}
	}	
	public String getClaime()
	{		
		return entityData.get(claime);
	}
	
	public boolean isWorking()
	{
		return this.entityData.get(working);
	}
	public void setWorking(boolean b)
	{
		this.entityData.set(working, b);
		if(b && !level.isClientSide)
		{
			ClaimeData d = ClaimeData.getCurrentData(this);
			BlockPos c = d.c;
			setPos(c.getX()+0.5, c.getY()+0.2 , c.getZ()+0.5);
			setDone(false);
		}
	}
	
	/**
	 * @return if the the miner is at his start position
	 */
	public boolean isDone()
	{
		return this.entityData.get(done); 
	}
	public void setDone(boolean b)
	{
		this.entityData.set(done, b);
	}
	
	public boolean isRepeat()
	{
		return this.entityData.get(repeat);
	}
	public void setRepeat(boolean b)
	{
		this.entityData.set(repeat, b);
	}
	
	protected boolean isBlockLess(BlockPos xyz, BlockState state)
	{
		state = state.getBlockState();
		
		if(isHomeBlock(xyz))
			return true;
		if(state.is(FuturepackTags.not_miner_breakable))
			return true;
		if(getHardness(state, xyz) < 0)
			return true;
		
		if(state.isAir(level, xyz))
			return true;
		if(state.getBlock() instanceof FlowingFluidBlock)
		{
			return true;
		}
		
		return false;
	}
	
	protected float getHardness(BlockState state, BlockPos jkl)
	{
		if(FPDungeonProtection.isUnbreakable(level, jkl))
		{
			return -1F;
		}
		
		float f = state.getDestroySpeed(level,jkl);
		return f;
	}
	
	public boolean isInArea(double x, double y, double z)
	{		
		ClaimeData data = ClaimeData.getCurrentData(this);
		BlockPos c = data.getCurentMiddle();
		
		double dx = Math.abs(x - (c.getX()+0.5));
        double dy = (c.getY()+0.5) - y;
        double dz = Math.abs(z - (c.getZ()+0.5));
        
		return dx < data.getMaxX() && dy < data.getMaxY() && dy > 0.2 && dz < data.getMaxZ();
	}
	
	public boolean isInArea(Vector3i vec)
	{
		return isInArea(vec.getX()+0.5, vec.getY()+0.5, vec.getZ()+0.5);
	}
	
	public TileEntityDroneStation getDroneStattion()
	{
		return (TileEntityDroneStation) level.getBlockEntity(getInventoryPos().relative(getSide(), -1));
	}
}
