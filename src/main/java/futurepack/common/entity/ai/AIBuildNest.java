package futurepack.common.entity.ai;

import java.util.EnumSet;

import futurepack.common.entity.living.EntityCrawler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;

public class AIBuildNest extends Goal
{
	private final EntityCrawler crawler;
	private final BlockState nest;
	private int buildingProgress = 0;
	private final int buildTime;
	
    public AIBuildNest(EntityCrawler crawler, BlockState nest, int buildTime)
    {
       this.crawler = crawler;
       this.nest = nest;
       this.buildTime = buildTime;
       this.setFlags(EnumSet.of(Goal.Flag.JUMP, Goal.Flag.MOVE));
    }

    
	@Override
	public boolean canUse() 
	{
		if(crawler.canBuildNest())
		{
			return this.crawler.getRandom().nextInt(5) == 0;
		}
		return false;
	}

	@Override
	public void start() 
	{
		super.start();
		
		Vector3d pos = RandomPositionGenerator.getLandPos(crawler, 30, 20);
		if(pos==null)
		{
			pos = RandomPositionGenerator.getPos(crawler, 15, 10);
		}
		if(pos!=null)
			if(crawler.getCommandSenderWorld().isEmptyBlock(new BlockPos(pos)))
			{
				crawler.getNavigation().moveTo(pos.x, pos.y, pos.z, crawler.getSpeed());
			}
		crawler.setHiding(false);
	}
	
	@Override
	public void stop() 
	{
		super.stop();
		buildingProgress = 0;
		crawler.setBuilding(false);
	}

	@Override
	public boolean canContinueToUse() 
	{
		if(!crawler.getNavigation().isDone())
			return true;
		
		if(buildingProgress < buildTime)
			return true;
		
		return super.canContinueToUse();
	}
	
	private final Vector3d motion = new Vector3d(0, 0.5, 0);
	
	@Override
	public void tick() 
	{
		if(crawler.getNavigation().isDone())
		{
			BlockPos pos = new BlockPos(crawler.position());
			if(Block.canSupportCenter(crawler.level, pos.below(), Direction.UP))//is top solid test
			{
				buildingProgress++;
				crawler.setBuilding(true);
				if(buildingProgress >= buildTime)
				{
					crawler.level.setBlockAndUpdate(pos, nest);
					crawler.setDeltaMovement(motion);
					crawler.onBuildNest();
				}
			}
			else
			{
				crawler.setBuilding(false);
				buildingProgress = 0;
				Vector3d path = RandomPositionGenerator.getLandPos(crawler, 5, 3);
				if(path!=null)
					crawler.getNavigation().moveTo(path.x, path.y, path.z, crawler.getSpeed()*2);
			}
			
		}
		super.tick();
	}
}
