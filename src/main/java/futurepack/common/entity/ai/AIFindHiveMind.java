package futurepack.common.entity.ai;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Predicate;

import it.unimi.dsi.fastutil.longs.Long2ByteAVLTreeMap;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.chunk.Chunk;

public class AIFindHiveMind extends WaterAvoidingRandomWalkingGoal 
{
	protected Long2ByteAVLTreeMap checkedChunks;
	protected final Predicate<TileEntity> isHive;
	private List<BlockPos> found;
	
	
	public AIFindHiveMind(CreatureEntity creature, Predicate<TileEntity> predicate, double speedIn, float probabilityIn) 
	{
		super(creature, speedIn, probabilityIn);
		setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.JUMP));
		checkedChunks = new Long2ByteAVLTreeMap();
		isHive = predicate;
		found = new LinkedList<BlockPos>();
	}
	
	@Override
	public boolean canUse() 
	{
		if(mob.getRestrictCenter() != BlockPos.ZERO)
			return false;
		else
			return super.canUse();
	}
	
	@Override
	public boolean canContinueToUse() 
	{
		return super.canContinueToUse();
	}
	
	@Override
	public void start() 
	{
		if(found.isEmpty())
		{
			ChunkPos currentChunk = new ChunkPos(mob.blockPosition());
			byte b = getChunkStatus(currentChunk);
			if((b & 0b11) == 0b01)
			{
				//chunk is empty
				super.start();
			}
		}
		else
		{
			double dis = found.get(0).distSqr(mob.position(), true);
			if( dis < 4)
			{
				mob.restrictTo(found.get(0), 16 * 4);
				found.clear();
				checkedChunks.clear();
				return;
			}
			
			wantedX = found.get(0).getX();
			wantedY = found.get(0).getY();
			wantedZ = found.get(0).getZ();
			
			super.start();
		}
		
	}
	
	@Override
	protected Vector3d getPosition() 
	{
		if(!found.isEmpty())
		{
			BlockPos p = found.get(0);
			return new Vector3d(p.getX()+0.5, p.getY()+0.5, p.getZ()+0.5);
		}
		Vector3d rndPos = super.getPosition();
		ChunkPos pos;
		if(rndPos!=null)
		{
			pos = new ChunkPos(new BlockPos(rndPos));
			if(checkedChunks.getOrDefault(pos.toLong(), (byte)0) == 0)
			{
				return rndPos;
			}
		}
		for(int i=0;i<10;i++)
		{
			rndPos = RandomPositionGenerator.getLandPos(this.mob, 10 +i*3, 7 + i);
			if(rndPos!=null)
			{
				pos = new ChunkPos(new BlockPos(rndPos));
				if(checkedChunks.getOrDefault(pos.toLong(), (byte)0) == 0)
				{
					return rndPos;
				}
			}
		}
		
		return super.getPosition();
	}
	
	protected byte getChunkStatus(ChunkPos pos)
	{
		return checkedChunks.compute(pos.toLong(), this::calcChunkStatus);
	}
	
	protected byte calcChunkStatus(long chunkPos, Byte currentStatus)
	{
		if(currentStatus == null)
			currentStatus = 0;
		else if( currentStatus != 0)
				return currentStatus;
		
		Chunk c = mob.getCommandSenderWorld().getChunk(ChunkPos.getX(chunkPos), ChunkPos.getZ(chunkPos));
		for(Entry<BlockPos, TileEntity> e : c.getBlockEntities().entrySet())
		{
			if(isHive.test(e.getValue()))
			{
				found.add(e.getKey());
				currentStatus = (byte) (currentStatus.byteValue() | 0b10);
			}
		}
		currentStatus = (byte) (currentStatus.byteValue() |0b1);
		
		return currentStatus;
	}

}
