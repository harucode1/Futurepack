package futurepack.common.entity.ai;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropsBlock;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.pathfinding.Path;
import net.minecraft.util.math.BlockPos;

public class EntityAIEatCroops extends Goal
{
	public final Block crops;
	private final MobEntity entity;
	private final double speed;
	
	private int searching = 0;
	private BlockPos target = null;
	
	public EntityAIEatCroops(MobEntity grassEaterEntityIn, Block bl, double speed)
	{
		super();
		this.entity = grassEaterEntityIn;
		this.crops = bl;
		this.speed = speed;
		if(!bl.defaultBlockState().getProperties().contains(CropsBlock.AGE))
		{
			throw new IllegalArgumentException("The provided Block "+bl+" is not a crop");
		}
	}
	
	@Override
	public boolean canUse()
	{
		if(lowHealth())
		{
			BlockPos pos = getNextCrop();
			if(pos!=null)
			{
				Path p = entity.getNavigation().createPath(pos, 0);
				if(p!=null)
				{
					entity.getNavigation().moveTo(p, speed);
					searching = 40;
					target = pos;
					return true;
				}
			}
		}
		return false;
	}

	public BlockPos getNextCrop()
	{
		for(int y=-3;y<4;y++)
		{
			for(int i=0;i<8;i++)
			{
				for(int z=-i;z<i;z++)
				{
					BlockPos pos = entity.blockPosition().offset(-i,y,z);
					if(isValid(pos))
						return pos;
					pos = entity.blockPosition().offset(i,y,z);
					if(isValid(pos))
						return pos;
				}
				for(int x=-i+1;x<i-1;x++)
				{
					BlockPos pos = entity.blockPosition().offset(x,y,-i);
					if(isValid(pos))
						return pos;
					pos = entity.blockPosition().offset(x,y,i);
					if(isValid(pos))
						return pos;
				}
			}
		}
		return null;
	}
	
	public boolean isValid(BlockPos pos)
	{
		BlockState state = entity.getCommandSenderWorld().getBlockState(pos);
		if(state.getBlock() == crops)
		{
			return state.getValue(CropsBlock.AGE) == 7;
		}
		return false;
	}
	
	private boolean lowHealth()
	{
		return entity.getHealth() <= entity.getMaxHealth()-2F;
	}
	
	@Override
	public boolean canContinueToUse()
	{
		return !entity.getNavigation().isDone() || searching>0;
	}
	
	@Override
	public void tick()
	{
		if(entity.getNavigation().isDone())
		{
			BlockPos pos = entity.blockPosition();
			if(!pos.equals(target))
			{
				if(pos.distSqr(target)<=1)
					pos = target;
			}
			
			if(isValid(pos))
			{
				if(searching>4)
				{
					if(searching>=40)
					{
						entity.level.broadcastEntityEvent(entity, (byte)12);
					}
					searching--;
					return;
				}
					
				BlockState state = entity.level.getBlockState(pos).setValue(CropsBlock.AGE, 4);
				entity.level.setBlockAndUpdate(pos, state);
				entity.level.levelEvent(2001, pos, Block.getId(state));
				
				entity.heal(2F);
				
			}
			else if(lowHealth())
			{
				pos = getNextCrop();
				if(pos!=null)
				{
					Path p = entity.getNavigation().createPath(pos, 0);
					if(p!=null)
					{
						entity.getNavigation().moveTo(p, entity.getSpeed());
						return;
					}
				}
			}
			searching = 0;
		}
		else
		{
			entity.getNavigation().tick();
		}
	}
}
