package futurepack.common.entity.ai;

import futurepack.common.DirtyHacks;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.controller.MovementController;


import net.minecraft.entity.ai.controller.MovementController.Action;

public class ExtendedMovementController extends MovementController 
{
	public ExtendedMovementController(MobEntity mob) 
	{
		super(mob);
		DirtyHacks.replaceData(this, mob.moveControl, MovementController.class);
	}

	public boolean isWaiting()
	{
		return this.operation == Action.WAIT;
	}
	
}
