package futurepack.common.entity.ai.hivemind;

import java.util.List;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PickUpItemsTask extends HVTask 
{
	public BlockPos itemPos;
	
	
	public PickUpItemsTask(BlockPos itemPos) 
	{
		this.itemPos = itemPos;
	}
	
	public PickUpItemsTask(CompoundNBT nbt)
	{
		super(nbt);
	}

	@Override
	public CompoundNBT serializeNBT() 
	{
		CompoundNBT nbt = new CompoundNBT();
		HelperSerialisation.putBlockPos(nbt, "itemPos", itemPos);
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) 
	{
		itemPos = HelperSerialisation.getBlockPos(nbt, "itemPos");
	}

	@Override
	public BlockPos getSpiderPosition() 
	{
		return itemPos;
	}
	
	@Override
	public boolean canExecute(EntityDungeonSpider spider, AssignedTask task) 
	{
		return spider.getMainHandItem().isEmpty();
	}
	
	@Override
	public boolean execute(EntityDungeonSpider spider) 
	{
		AxisAlignedBB bb = new AxisAlignedBB(itemPos).inflate(1, 1, 1);
		
		List<ItemEntity> items = spider.getCommandSenderWorld().getEntitiesOfClass(ItemEntity.class, bb, i -> i.isAlive());
		
		int amount = items.size();
		if(amount -1 <= 0)
		{
			isDone = true;
		}
		if(amount > 0)
		{
			spider.setItemInHand(Hand.MAIN_HAND, items.get(0).getItem());
			items.get(0).remove();
		}
		
		return !spider.getMainHandItem().isEmpty();
	}

	@Override
	public boolean isFinished(World w) 
	{
		AxisAlignedBB bb = new AxisAlignedBB(itemPos).inflate(1, 1, 1);
		List<ItemEntity> items = w.getEntitiesOfClass(ItemEntity.class, bb, i -> i.isAlive());
		return items.isEmpty();
	}
}
