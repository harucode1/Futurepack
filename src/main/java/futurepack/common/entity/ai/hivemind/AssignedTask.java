package futurepack.common.entity.ai.hivemind;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import futurepack.common.entity.living.EntityDungeonSpider;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import net.minecraftforge.common.util.INBTSerializable;

public class AssignedTask implements INBTSerializable<CompoundNBT>
{
	private HVTask task;
	private List<WeakReference<EntityDungeonSpider>> workingSpiders;

	public AssignedTask(HVTask task) 
	{
		super();
		this.task = task;
		workingSpiders = new ArrayList<WeakReference<EntityDungeonSpider>>();
	}

	public AssignedTask(CompoundNBT nbt)
	{
		deserializeNBT(nbt);
	}
	
	@Override
	public CompoundNBT serializeNBT() 
	{
		CompoundNBT nbt = new CompoundNBT();
		nbt.put("task", HVTask.save(task));
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) 
	{
		task = HVTask.load(nbt.getCompound("task"));
	}
	
	public boolean isFinished(World w)
	{
		return task.isFinished(w);
	}
	
	public HVTask getTask()
	{
		return task;
	}
	
	
	public int getAssignedSpidersCount()
	{
		workingSpiders.removeIf(ref -> (ref.get()==null || !ref.get().isAlive()));
		return workingSpiders.size();
	}
	
	public Stream<EntityDungeonSpider> getAssignedSpiders()
	{
		return workingSpiders.stream().map(WeakReference::get).filter(e -> e!=null && e.isAlive());
	}
	
	public boolean canExecute(EntityDungeonSpider spider)
	{
		return task.canExecute(spider, this);
	}

	public HVTask assign(EntityDungeonSpider spider) 
	{
		workingSpiders.add(new WeakReference<EntityDungeonSpider>(spider));
		return task;
	}
}