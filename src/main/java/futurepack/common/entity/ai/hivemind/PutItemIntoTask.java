package futurepack.common.entity.ai.hivemind;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class PutItemIntoTask extends HVTask 
{
	private BlockPos pos;
	private Direction dir;
	
	
	public PutItemIntoTask(BlockPos pos, Direction dir) 
	{
		super();
		this.pos = pos;
		this.dir = dir;
	}

	public PutItemIntoTask(CompoundNBT nbt) 
	{
		super(nbt);
	}
	
	@Override
	public CompoundNBT serializeNBT() 
	{
		CompoundNBT nbt = new CompoundNBT();
		HelperSerialisation.putBlockPos(nbt, "pos", pos);
		nbt.putInt("direction", dir.get3DDataValue());
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) 
	{
		pos = HelperSerialisation.getBlockPos(nbt, "pos");
		dir = Direction.from3DDataValue(nbt.getInt("direction"));
	}

	@Override
	public BlockPos getSpiderPosition() 
	{
		return pos;
	}

	@Override
	public boolean execute(EntityDungeonSpider spider) 
	{
		if(!spider.getMainHandItem().isEmpty())
		{
			BlockPos invntory = pos.relative(dir);
			TileEntity tile = spider.getCommandSenderWorld().getBlockEntity(invntory);
			if(tile!=null)
			{
				IItemHandler handler = HelperInventory.getHandler(tile, dir.getOpposite());
				if(handler != null)
				{
					ItemStack stack = spider.getMainHandItem();
					spider.setItemInHand(Hand.MAIN_HAND, ItemHandlerHelper.insertItem(handler, stack, false));
				}
				else
				{
					return isDone = true;
				}
			}
			else
			{
				return isDone = true;
			}
		}
		return isDone = spider.getMainHandItem().isEmpty();
	}
	
	@Override
	public boolean canExecute(EntityDungeonSpider spider, AssignedTask assignedTask) 
	{
		return !spider.getMainHandItem().isEmpty();
	}

}
