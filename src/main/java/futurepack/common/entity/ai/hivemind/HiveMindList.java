package futurepack.common.entity.ai.hivemind;

import java.util.Map;
import java.util.Map.Entry;

import futurepack.common.entity.living.EntityDungeonSpider;
import it.unimi.dsi.fastutil.objects.Object2ObjectRBTreeMap;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.WorldSavedData;

public class HiveMindList extends WorldSavedData
{
	private static final String name = "futurepack_hive_mind";
	
	private final ServerWorld world;
	private Map<BlockPos, HiveMind> pos2mind;
	
	public HiveMindList(ServerWorld world) 
	{
		super(name);
		this.world = world;
		pos2mind = new Object2ObjectRBTreeMap<BlockPos, HiveMind>();
	}

	public static HiveMindList getHiveMindsIn(ServerWorld world)
	{
		return world.getDataStorage().computeIfAbsent(() -> new HiveMindList(world), name);
	}

	@Override
	public void load(CompoundNBT nbt) 
	{
		ListNBT list = nbt.getList("minds", nbt.getId());
		for(INBT tag : list)
		{
			HiveMind mind = new HiveMind(world, (CompoundNBT) tag);
			pos2mind.put(mind.getPosition(), mind);
		}
	}

	@Override
	public CompoundNBT save(CompoundNBT compound) 
	{
		ListNBT list = new ListNBT();
		for(Entry<BlockPos, HiveMind> e : pos2mind.entrySet())
		{
			list.add(e.getValue().serializeNBT());
		}
		compound.put("minds", list);
		
		return compound;
	}
	
	public HiveMind getHiveMindFor(EntityDungeonSpider spider)
	{
		BlockPos pos = spider.getRestrictCenter();
		return pos2mind.computeIfAbsent(pos, p -> new HiveMind(world, p));
	}
}
