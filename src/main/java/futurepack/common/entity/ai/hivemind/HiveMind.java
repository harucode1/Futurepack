package futurepack.common.entity.ai.hivemind;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.INBTSerializable;

public class HiveMind implements INBTSerializable<CompoundNBT>
{
	private final ServerWorld world;
	private BlockPos pos;
	
	private List<AssignedTask> taskList = new ArrayList<AssignedTask>();
	
	public HiveMind(ServerWorld world, BlockPos pos) 
	{
		super();
		this.world = world;
		this.pos = pos;
	}
	
	public HiveMind(ServerWorld world, CompoundNBT nbt) 
	{
		this.world = world;
		deserializeNBT(nbt);
	}

	@Override
	public CompoundNBT serializeNBT() 
	{
		CompoundNBT nbt = new CompoundNBT();
		HelperSerialisation.putBlockPos(nbt, "pos", pos);
		ListNBT list = new ListNBT();
		for(AssignedTask t : taskList)
		{
			list.add(t.serializeNBT());
		}
		nbt.put("tasks", list);
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) 
	{
		pos = HelperSerialisation.getBlockPos(nbt, "pos");
		ListNBT list = nbt.getList("tasks", nbt.getId());
		taskList.clear();
		for(INBT tag : list)
		{
			taskList.add(new AssignedTask((HVTask) tag));
		}
	}
	
	public BlockPos getPosition()
	{
		return pos;
	}
	
	public void updateTaskList()
	{
		List<AssignedTask> newTasks = taskList.stream()
				.filter(t -> t.isFinished(world))
				.map(AssignedTask::getTask)
				.map(t -> t.getFollowUpTasks(world))
				.flatMap(t -> StreamSupport.stream(Spliterators.spliteratorUnknownSize(t.iterator(), Spliterator.ORDERED), false))
				.map(AssignedTask::new)
				.collect(Collectors.toList());
		taskList.removeIf(t -> t.isFinished(world));
		taskList.addAll(newTasks);
	}
	
	public HVTask getTaskFor(EntityDungeonSpider spider)
	{
		updateTaskList();
		Optional<TaskWithDistance> opt = taskList.stream()
				.filter(t -> t.canExecute(spider)).map(t -> new TaskWithDistance(t, spider.position())).sorted((a,b) -> a.distance == b.distance ? 0 :  (a.distance > b.distance ? 1 : -1)).findFirst();
		if(opt.isPresent())
		{
			return opt.get().task.assign(spider);
		}
		if(!spider.getMainHandItem().isEmpty())
		{
			AssignedTask task = new AssignedTask(new PutItemIntoTask(pos.above(), Direction.DOWN));
			taskList.add(task);
			return task.assign(spider);
		}
		
		if(world.random.nextInt(100) == 0)
		{
			BlockPos pos = this.pos.offset(world.random.nextInt(10)-5, 0, world.random.nextInt(10)-5);
			while(world.isEmptyBlock(pos))
			{
				pos = pos.below();
			}
			while(!world.isEmptyBlock(pos))
			{
				pos = pos.above();
			}
			
			AssignedTask task = new AssignedTask(new BreakBlockTask(pos.below(), pos));
			taskList.add(task);
			return task.assign(spider);
		}
		
		return null;
	}
}