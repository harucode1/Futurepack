package futurepack.common.entity.ai.hivemind;

import java.util.Arrays;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BreakBlockTask extends HVTask 
{
	protected BlockPos blockToBreak;
	protected BlockPos spiderPos;
	
	
	public BreakBlockTask(BlockPos blockToBreak, BlockPos sPiderPos)
	{
		super();
		this.blockToBreak = blockToBreak;
		this.spiderPos = sPiderPos;
	}
	
	public BreakBlockTask(CompoundNBT nbt)
	{
		super(nbt);
	}

	@Override
	public CompoundNBT serializeNBT() 
	{
		CompoundNBT nbt = new CompoundNBT();
		HelperSerialisation.putBlockPos(nbt, "break", blockToBreak);
		HelperSerialisation.putBlockPos(nbt, "spider", spiderPos);
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) 
	{
		blockToBreak = HelperSerialisation.getBlockPos(nbt, "break");
		spiderPos = HelperSerialisation.getBlockPos(nbt, "spider");
	}

	@Override
	public BlockPos getSpiderPosition() 
	{
		return spiderPos;
	}
	
	@Override
	public boolean execute(EntityDungeonSpider spider) 
	{
		spider.getCommandSenderWorld().destroyBlock(blockToBreak, true);
		isDone = true;
		return true;
	}
	
	@Override
	public Iterable<HVTask> getFollowUpTasks(World w) 
	{
		BlockPos.Mutable mut = new BlockPos.Mutable().set(blockToBreak);
		while(mut.getY() > 1 && w.isEmptyBlock(mut))
		{
			mut.move(Direction.DOWN);
		}
		mut.move(Direction.UP);
		
		return Arrays.asList(new PickUpItemsTask(mut.immutable()));
	}

}
