package futurepack.common.entity.ai.hivemind;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

import futurepack.common.entity.living.EntityDungeonSpider;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.INBTSerializable;

public abstract class HVTask implements INBTSerializable<CompoundNBT>
{	
	protected boolean isDone = false;
	
	public HVTask()
	{
		
	}
	
	public HVTask(CompoundNBT nbt)
	{
		deserializeNBT(nbt);
	}
	
	public abstract BlockPos getSpiderPosition();

	/**
	 * @param spider the Entity executing this
	 * @return if this Entity is finished with the task
	 */
	public abstract boolean execute(EntityDungeonSpider spider);
	
	/**
	 * @return if this Task is finished
	 */
	public boolean isFinished(World w)
	{
		return isDone;
	}
	
	public Iterable<HVTask> getFollowUpTasks(World w)
	{
		return Collections.emptyList();
	}
	
	public boolean canExecute(EntityDungeonSpider spider, AssignedTask assignedTask)
	{
		return true;
	}
	
	//only 4 entries so we can use brute forcing
	private static Map<Class<? extends HVTask>, String> class2IdMap = new Object2ObjectArrayMap<Class<? extends HVTask>, String>(4);
	private static Map<String, Function<CompoundNBT, HVTask>> constructorMap = new Object2ObjectArrayMap<String, Function<CompoundNBT,HVTask>>(4);

	static
	{
		register(BreakBlockTask.class, BreakBlockTask::new, "break_block");
		register(GoToTask.class, GoToTask::new, "go_to");
		register(PickUpItemsTask.class, PickUpItemsTask::new, "pickup_items");
		register(PutItemIntoTask.class, PutItemIntoTask::new, "put_item_into");
	}
	
	public static <T extends HVTask> void register(Class<T> cls, Function<CompoundNBT, T> func, String name)
	{
		if(constructorMap.containsKey(name))
			throw new IllegalArgumentException("This name " + name + " is already registered");
		
		class2IdMap.put(cls, name);
		constructorMap.put(name, (Function<CompoundNBT, HVTask>) func);
	}
	
	public static HVTask load(CompoundNBT compound) 
	{
		String s = compound.getString("taskID");
		Function<CompoundNBT, HVTask> func = constructorMap.get(s);
		if(func!=null)
		{
			return func.apply(compound);
		}
		else
		{
			throw new IllegalArgumentException("Unknown Task id " + s);
		}
	}

	public static CompoundNBT save(HVTask task) 
	{
		CompoundNBT nbt = task.serializeNBT();
		nbt.putString("taskID", class2IdMap.getOrDefault(task.getClass(), task.getClass().getName()));
		return nbt;
	}
}
