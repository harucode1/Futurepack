package futurepack.common.entity.ai;

import java.util.EnumSet;

import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.Heightmap.Type;

public class AIFindFireflyes extends Goal
{
	private final MobEntity parentEntity;
	private final ExtendedMovementController controller;
	
	private BlockPos fireflieys;
	
	protected BlockState toFind;
	
	public AIFindFireflyes(MobEntity ghast)
	{
		this.parentEntity = ghast;
		ghast.moveControl = controller = new ExtendedMovementController(ghast);
		
		this.setFlags(EnumSet.of(Goal.Flag.MOVE));
		toFind = TerrainBlocks.fireflies.defaultBlockState();
	}
	
	@Override
	public boolean canUse()
	{
		if(this.parentEntity.level.isDay())
			return false;
		
		World w = this.parentEntity.level;
		BlockPos liv = new BlockPos(this.parentEntity.position());

		for(BlockPos pos : BlockPos.betweenClosed(liv.offset(-5, -5, -5), liv.offset(5,5,5)))
		{
			 if(w.getBlockState(pos) == toFind)
			 {
				 fireflieys = pos.immutable();
				 return true;
			 }
		}
		
		liv = this.parentEntity.level.getHeightmapPos(Type.WORLD_SURFACE, liv);
		for(BlockPos pos : BlockPos.betweenClosed(liv.offset(-5, -5, -5), liv.offset(5,5,5)))
		{
			 if(w.getBlockState(pos) == toFind)
			 {
				 fireflieys = pos.immutable();
				 return true;
			 }
		}
	
		return false;
	}
	
	@Override
	public void start()
	{		
		double x = fireflieys.getX() + 0.5;
		double y = fireflieys.getY() + 0.5;
		double z = fireflieys.getZ() + 0.5;
		this.parentEntity.getMoveControl().setWantedPosition(x, y, z, 1.0D);
	}
	
	@Override
	public boolean canContinueToUse()
	{
		if(fireflieys==null)
			return false;
		
		World w = this.parentEntity.level;
		
		if(w.getBlockState(fireflieys) == toFind)
		{
			double x = this.parentEntity.getMoveControl().getWantedX();
			double y = this.parentEntity.getMoveControl().getWantedY();
			double z = this.parentEntity.getMoveControl().getWantedZ();
			BlockPos target = new BlockPos(x,y,z);
			if(!fireflieys.equals(target))
			{
				x = fireflieys.getX() + 0.5;
				y = fireflieys.getY() + 0.5;
				z = fireflieys.getZ() + 0.5;
				this.parentEntity.getMoveControl().setWantedPosition(x, y, z, 1.0F);
			}
			else
			{
				if(this.parentEntity.distanceToSqr(x, y, z) < 1.0)
				{
					fireflieys = null;
					return false;
				}
				else if(controller.isWaiting())
				{
					fireflieys = null;
					
				}
			}
			return true;
		}
		else
		{
			fireflieys = null;
		}
		return false;
	}
	
	@Override
	public void stop()
	{
		super.stop();
		fireflieys = null;
	}
}
