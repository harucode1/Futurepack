package futurepack.common.entity.ai;

import net.minecraft.entity.LivingEntity;

public interface IHideableCreature
{
	public void setHiding(boolean hiding);
	
	public boolean isHiding();
	
	public void onHiddenAttack(LivingEntity base);
	
	public boolean canHide();
}
