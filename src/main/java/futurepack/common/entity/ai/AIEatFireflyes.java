package futurepack.common.entity.ai;

import java.util.EnumSet;

import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class AIEatFireflyes extends Goal
{
	private final AnimalEntity parentEntity;
	
	private BlockPos firefliyes; 
	protected BlockState toFind;
	
	public AIEatFireflyes(AnimalEntity ghast)
	{
		this.parentEntity = ghast;
		//this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE));
		this.setFlags(EnumSet.of(Goal.Flag.JUMP));
		toFind = TerrainBlocks.fireflies.defaultBlockState();
	}
	
	@Override
	public boolean canUse()
	{
		if(this.parentEntity.level.isDay())
			return false;
		firefliyes = new BlockPos(this.parentEntity.position());
		World w = this.parentEntity.level;
		return w.getBlockState(firefliyes) == toFind;
	}
	
	@Override
	public void start()
	{	
		if(firefliyes!=null)
		{
			if(this.parentEntity.isBaby())
			{
				parentEntity.ageUp((int)(-parentEntity.getAge() / 20 * 0.1F), true);
			}
			else
			{
				
				this.parentEntity.setInLove(null);

				CompoundNBT nbt = new CompoundNBT();
				parentEntity.addAdditionalSaveData(nbt);
				int inLove = nbt.getInt("InLove");
				inLove = Math.max(inLove, 1200);//1 minutie
				nbt.putInt("InLove", inLove + 600);
				parentEntity.readAdditionalSaveData(nbt);
			}
			
			this.parentEntity.level.removeBlock(firefliyes, false);
			if(parentEntity.level.random.nextInt(4)==0)
			{
				parentEntity.spawnAtLocation(Items.GLOWSTONE_DUST, 1);
			}
			firefliyes = null;
		}		
	}
	
	@Override
	public void stop()
	{
		firefliyes = null;
	}
}
