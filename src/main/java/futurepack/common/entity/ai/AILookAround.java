package futurepack.common.entity.ai;

import java.util.EnumSet;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.math.MathHelper;

public class AILookAround extends Goal
{
	private final MobEntity parentEntity;

	public AILookAround(MobEntity ghast)
	{
		this.parentEntity = ghast;
		this.setFlags(EnumSet.of(Goal.Flag.LOOK));
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean canUse()
	{
		return true;
	}

	/**
	 * Updates the task
	 */
	@Override
	public void tick()
	{
		if (this.parentEntity.getTarget() == null)
		{
			this.parentEntity.yRot = -((float)MathHelper.atan2(this.parentEntity.getDeltaMovement().x, this.parentEntity.getDeltaMovement().z)) * (180F / (float)Math.PI);
			this.parentEntity.yBodyRot = this.parentEntity.yRot;
		}
		else
		{
			LivingEntity entitylivingbase = this.parentEntity.getTarget();
			double d0 = 64.0D;

			if (entitylivingbase.distanceToSqr(this.parentEntity) < d0*d0)
			{
				double d1 = entitylivingbase.getX() - this.parentEntity.getX();
				double d2 = entitylivingbase.getZ() - this.parentEntity.getZ();
				this.parentEntity.yRot = -((float)MathHelper.atan2(d1, d2)) * (180F / (float)Math.PI);
				this.parentEntity.yBodyRot = this.parentEntity.yRot;
			}
		}
	}
}
