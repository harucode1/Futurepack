package futurepack.common.entity.living;

import javax.annotation.Nullable;

import futurepack.api.Constants;
import futurepack.common.FPEntitys;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.entity.ai.EntityAIEatCroops;
import net.minecraft.block.Blocks;
import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.ChestContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class EntityGehuf extends CowEntity implements IInventory, INamedContainerProvider
{
	private static DataParameter<Boolean> chest = EntityDataManager.defineId(EntityGehuf.class, DataSerializers.BOOLEAN);

	public EntityGehuf(EntityType<EntityGehuf> type, World w)
	{
		super(type, w);
	}
	
	private int eatTimer = 0;
	
	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(4, new EntityAIEatCroops(this, PlantBlocks.mendel_berry, 1.0));
	}
	
	@Override
	public EntityType<?> getType() 
	{
		return FPEntitys.GEHUF;
	}
	
	@Override
	public EntityGehuf getBreedOffspring(ServerWorld world, AgeableEntity p_90011_1_)
	{
		return new EntityGehuf(FPEntitys.GEHUF, world);
	}
	
	@Override
	protected void defineSynchedData() 
	{
		super.defineSynchedData();		
		this.entityData.define(chest, false);
	}
	
	@Override
	public void handleEntityEvent(byte id)
	{
		if(id==12)
		{
			eatTimer = 40;
		}
		super.handleEntityEvent(id);
	}
	
	@Override
	public void aiStep()
	{
		if (this.level.isClientSide)
		{
			eatTimer = eatTimer>0?eatTimer-1:0;
		}
		
		super.aiStep();
	}
	
	private void setChest(boolean b)
	{
		this.entityData.set(chest, b);
	}
	
	public boolean hasChest()
	{
		return this.entityData.get(chest);
	}
	
	@Override
	public ActionResultType mobInteract(PlayerEntity pl, Hand hand)
    {
		ActionResultType b = super.mobInteract(pl, hand);
		if(!isBaby())
		{
			if(hasChest())
			{
//				pl.openContainer(p_213829_1_)
				
				if(pl instanceof ServerPlayerEntity)
				{
					((ServerPlayerEntity)pl).openMenu(this);
				}
				
				return ActionResultType.SUCCESS;
			}
			ItemStack itemstack = pl.inventory.getSelected();
	
			if (!itemstack.isEmpty() && itemstack.getItem() == Blocks.CHEST.asItem())
			{
				if(!pl.isCreative())
					itemstack.shrink(1);
				 
				setChest(true);
				return ActionResultType.SUCCESS;
			}
		}
		return b;
	}

	private NonNullList<ItemStack> chestContents = NonNullList.withSize(27, ItemStack.EMPTY);
	
	@Override
	public int getContainerSize() 
	{
		return chestContents.size();
	}

	@Override
    public ItemStack getItem(int slot)
    {
        return this.chestContents.get(slot);
    }

    @Override
    public ItemStack removeItem(int index, int count)
    {
    	 ItemStack itemstack = ItemStackHelper.removeItem(chestContents, index, count);

         if (!itemstack.isEmpty())
         {
             this.setChanged();
         }

         return itemstack;
    }

    @Override
    public ItemStack removeItemNoUpdate(int index)
    {
    	 return ItemStackHelper.takeItem(chestContents, index);
    }

    @Override
    public void setItem(int index, ItemStack stack)
    {
    	 chestContents.set(index, stack);

         if (stack.getCount() > this.getMaxStackSize())
         {
             stack.setCount(this.getMaxStackSize());
         }

         this.setChanged();
    }

//    @Override
//    public String getInventoryName()
//    {
//        return "container.chest";
//    }
//    
//    @Override
//    public boolean hasCustomName()
//    {
//        return false;
//    }
    
    @Override
    public int getMaxStackSize()
    {
        return 64;
    }
    
//    @Override
//    public boolean isUseableByPlayer(EntityPlayer p_70300_1_)
//    {
//        return getDistanceToEntity(p_70300_1_) <= 64.0D;
//    }
    
    @Override
    public boolean stillValid(PlayerEntity player)
    {
    	return distanceToSqr(player) <= 64.0D;
    }
    
    @Override
    public boolean canPlaceItem(int p_94041_1_, ItemStack p_94041_2_)
    {
        return true;
    }

	@Override
	public void setChanged() {}


	@Override
	public void readAdditionalSaveData(CompoundNBT nbt)
	{
		super.readAdditionalSaveData(nbt);
		setChest(nbt.getBoolean("Chest"));
		ListNBT nbttaglist = nbt.getList("Items", 10);
        for (int i = 0; i < nbttaglist.size(); ++i)
        {
            CompoundNBT nbttagcompound1 = nbttaglist.getCompound(i);
            int j = nbttagcompound1.getByte("Slot") & 255;

            if (j >= 0 && j < this.chestContents.size())
            {
            	setItem(j, ItemStack.of(nbttagcompound1));
            }
        }
	}
	
	@Override
	public void addAdditionalSaveData(CompoundNBT nbt)
	{
		super.addAdditionalSaveData(nbt);
		nbt.putBoolean("Chest", hasChest());
	
		ListNBT nbttaglist = new ListNBT();

        for (int i = 0; i < this.chestContents.size(); ++i)
        {
            if (!this.chestContents.get(i).isEmpty())
            {
                CompoundNBT nbttagcompound1 = new CompoundNBT();
                nbttagcompound1.putByte("Slot", (byte)i);
                this.chestContents.get(i).save(nbttagcompound1);
                nbttaglist.add(nbttagcompound1);
            }
        }

        nbt.put("Items", nbttaglist);

	}
   
	@Override
	public void die(DamageSource p_70645_1_)
	{
		if(hasChest())
		{
			for(ItemStack it : chestContents)
			{
				if(it==null || it.isEmpty())
					continue;
				
				ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), it);
				level.addFreshEntity(item);
			}
		}
			
		super.die(p_70645_1_);
	}

	@Override
	public void startOpen(PlayerEntity player) { }

	@Override
	public void stopOpen(PlayerEntity player) { }

	@Override
	public void clearContent() {}

	@Override
	public boolean isEmpty()
	{
		return !hasChest();
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
    public float getHeadRotationPointY(float p_70894_1_)
    {
        return this.eatTimer <= 0 ? 0.0F : (this.eatTimer >= 4 && this.eatTimer <= 36 ? 1.0F : (this.eatTimer < 4 ? (this.eatTimer - p_70894_1_) / 4.0F : -(this.eatTimer - 40 - p_70894_1_) / 4.0F));
    }

    //@ TODO: OnlyIn(Dist.CLIENT)
    public float getHeadRotationAngleX(float p_70890_1_)
    {
        if (this.eatTimer > 4 && this.eatTimer <= 36)
        {
            float f = (this.eatTimer - 4 - p_70890_1_) / 32.0F;
            return ((float)Math.PI / 5F) + ((float)Math.PI * 7F / 100F) * MathHelper.sin(f * 28.7F);
        }
        else
        {
            return this.eatTimer > 0 ? ((float)Math.PI / 5F) : this.xRot * 0.017453292F;
        }
    }
    
    @Nullable
    @Override
    protected ResourceLocation getDefaultLootTable()
    {
    	return new ResourceLocation(Constants.MOD_ID, "entities/gehuf");
    }

	@Override
	public Container createMenu(int id, PlayerInventory inv, PlayerEntity pl) 
	{
		return ChestContainer.threeRows(id, inv, this);
	}
}
