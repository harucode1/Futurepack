package futurepack.common.entity.living;

import futurepack.common.FPEntitys;
import futurepack.common.FPPotions;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.AttributeModifierMap.MutableAttribute;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.world.World;

public class EntityCyberZombie extends ZombieEntity
{

	public EntityCyberZombie(EntityType<EntityCyberZombie> type, World worldIn)
	{
		super(type, worldIn);
	}
	
	public EntityCyberZombie(World worldIn)
	{
		this(FPEntitys.CYBER_ZOMBIE, worldIn);
	}
	
	
	public static MutableAttribute registerAttributes()
    {
		return ZombieEntity.createAttributes().add(Attributes.ARMOR, 4.0D);
    }
	
	@Override
	public boolean doHurtTarget(Entity entityIn)
    {
        boolean flag = super.doHurtTarget(entityIn);

        if (flag && this.getMainHandItem().isEmpty() && entityIn instanceof LivingEntity)
        {
            float f = this.level.getCurrentDifficultyAt(this.blockPosition()).getEffectiveDifficulty();
            ((LivingEntity)entityIn).addEffect(new EffectInstance(FPPotions.paralyze, 14 * (int)f));
        }

        return flag;
    }

	@Override
	protected ItemStack getSkull()
    {
        return ItemStack.EMPTY;
    }
}
