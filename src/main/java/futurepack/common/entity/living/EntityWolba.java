package futurepack.common.entity.living;

import java.util.Random;

import javax.annotation.Nullable;

import futurepack.common.FPEntitys;
import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.item.DyeColor;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class EntityWolba extends SheepEntity
{
	public EntityWolba(EntityType<EntityWolba> type, World w)
	{
		super(type, w);
	}
	
	public EntityWolba(World w)
	{
		this(FPEntitys.WOLBA, w);
	}
	
	@Override
	public EntityType<?> getType() 
	{
		return FPEntitys.WOLBA;
	}
	
	@Override
	public EntityWolba getBreedOffspring(ServerWorld world, AgeableEntity p_90011_1_)
	{
		return new EntityWolba(world);
	}
	
	@Nullable
	@Override
    public ILivingEntityData finalizeSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
    {
		spawnDataIn =  super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
        this.setColor(EntityWolba.getRandomSheepColor(this.level.random));
        return spawnDataIn;
    }
	
	public static DyeColor getRandomSheepColor(Random random)
    {
        int i = random.nextInt(100);
        return i < 7 ? DyeColor.BROWN : (i < 12 ? DyeColor.GRAY : (i < 17 ? DyeColor.YELLOW : (i < 20 ? DyeColor.BLACK : (random.nextInt(500) == 0 ? DyeColor.LIME : (random.nextInt(500) == 0 ? DyeColor.LIGHT_BLUE : DyeColor.ORANGE)))));
    }
}
