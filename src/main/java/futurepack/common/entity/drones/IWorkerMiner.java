package futurepack.common.entity.drones;

import net.minecraft.util.math.BlockPos;

public interface IWorkerMiner extends IWorker 
{
	
	/**
	 * Mines the target BlockPos and transfer drop to Station
	 * @return
	 */
	boolean mineAndDeliver(BlockPos target);
	
}
