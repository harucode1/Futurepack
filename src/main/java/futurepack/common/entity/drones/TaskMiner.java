package futurepack.common.entity.drones;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.Mutable;
import net.minecraft.world.World;

public class TaskMiner extends TaskBase 
{
	public TaskMiner(BlockPos min, BlockPos max) 
	{
		super();
		
		this.min = min;
		this.max = max;
		size = new BlockPos(max.getX() - min.getX() + 1, max.getY() - min.getY() + 1, max.getZ() - min.getZ() + 1);
		
		current = 0;	
	}

	BlockPos min;
	BlockPos max;
	BlockPos size;
	
	int current;
	
	private Mutable getPosFromIdx(int k)
	{
		Mutable pos = new Mutable();
		
		pos.setX(k % size.getX());
		k = k / size.getX();
		pos.setZ(k % size.getZ());
		k = k / size.getZ();
		pos.setY(k % size.getY());

		if (pos.getZ() % 2 != 0)
			pos.setX(size.getX() - pos.getX() - 1);

		if (pos.getY() % 2 != 0)
			pos.setZ(size.getZ() - pos.getZ() - 1);
		
		pos.offset(min);
		
		return pos;
	}

	@Override
	public boolean isReady() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean step() 
	{
		IWorkerMiner miner = (IWorkerMiner)worker;
		
		if(!miner.isFinised())
			return false;
		
		//timeout?
		
		World w = ((IWorkerMiner)worker).getWorld();	
		
		long time = System.nanoTime() + 100;	//max 50ns for scan
		
		do
		{
			current ++;
					
			if(current >= size.getX() * size.getY() * size.getZ())
			{
				current = 0;
				return true;
			}
			
			Mutable p = getPosFromIdx(current);
			
			if(!w.getBlockState(p).isAir())
			{
				BlockPos mPos = miner.getPosition();
				if(mPos.distManhattan(p) <= 1)
				{
					miner.mineAndDeliver(p);
					return false;
				}
				else
				{
					miner.moveOrTeleport(getPosFromIdx(Math.max(0, current - 1)));
						
					return false;
				}
			}
		}
		while(System.nanoTime() < time);
		
		return false;
	}

	@Override
	public boolean isReapeatable() {
		// TODO Auto-generated method stub
		return false;
	}

}
