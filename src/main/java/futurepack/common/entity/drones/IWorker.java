package futurepack.common.entity.drones;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface IWorker 
{
	/**
	 * 
	 * @return Current Enity Position
	 */
	BlockPos getPosition();
	
	/**
	 * Try to move to BlockPos, teleport if walking failed
	 * @param target
	 * @return is moving possible
	 */
	boolean moveOrTeleport(BlockPos target);
	
	
	boolean isFinised();
	
	World getWorld();

}
