package futurepack.common.entity;

import futurepack.api.Constants;
import futurepack.common.FPLog;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = Constants.MOD_ID, bus = Bus.FORGE)
public class CapabilityPlayerData implements IPlayerData, INBTSerializable<CompoundNBT>
{
	
	@CapabilityInject(IPlayerData.class)
	public static final Capability<IPlayerData> cap_PLAYERDATA = null;
	
	public static class Storage implements IStorage<IPlayerData>
	{
		@Override
		public INBT writeNBT(Capability<IPlayerData> capability, IPlayerData instance, Direction side)
		{
			return instance.getTag();
		}

		@Override
		public void readNBT(Capability<IPlayerData> capability, IPlayerData instance, Direction side, INBT nbt)
		{
			instance.addAll((CompoundNBT)nbt);
		}
	}
	
	private CompoundNBT nbt;
	
	public CapabilityPlayerData() 
	{
		nbt = new CompoundNBT();
	}
	
	
	@Override
	public CompoundNBT serializeNBT() 
	{
		return getTag();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) 
	{
		addAll(getTag());
	}

	@Override
	public CompoundNBT getTag() 
	{
		return nbt;
	}

	@Override
	public void addAll(CompoundNBT nbt) 
	{
		this.nbt.merge(nbt);
	}
	
	
	public static CompoundNBT getPlayerdata(PlayerEntity pl)
	{
		if(pl.isAlive())
			return pl.getCapability(cap_PLAYERDATA).map(IPlayerData::getTag).orElseThrow(NullPointerException::new);
		else
			return new CompoundNBT();
	}
	
	@SubscribeEvent
	public static void addDataCapsToPlayer(AttachCapabilitiesEvent<Entity> e)
	{
		if(e.getGenericType()==Entity.class)
		{
			if(e.getObject() instanceof PlayerEntity)
			{	
				e.addCapability(new ResourceLocation(Constants.MOD_ID, "additionaldata"), new PlayerDataProvider());
			}
		}
	}
	
	@SubscribeEvent
	public static void copyData(PlayerEvent.Clone e)
	{
		CompoundNBT original = e.getOriginal().getCapability(cap_PLAYERDATA).map(IPlayerData::getTag).orElse(null);
		if(original!=null)
		{
			CompoundNBT newPlayer = e.getPlayer().getCapability(cap_PLAYERDATA).map(IPlayerData::getTag).orElseThrow(NullPointerException::new);
			newPlayer.merge(original);
		}
		else
		{
			FPLog.logger.warn("Old player %s did not have futurepack-data capability", e.getOriginal());
		}
	}
	
	private static class PlayerDataProvider implements ICapabilityProvider, INBTSerializable<CompoundNBT>
	{
		LazyOptional<IPlayerData> opt = LazyOptional.of(cap_PLAYERDATA::getDefaultInstance);
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
		{
			if(capability==cap_PLAYERDATA)
			{
				return opt.cast();
			}
			return LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT()
		{
			return opt.map(d -> d.getTag()).orElseGet(CompoundNBT::new);
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt)
		{
			opt.ifPresent(d -> d.addAll(nbt));
		}
		
	}
	
}
