package futurepack.common.entity;

import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class WallCrawlerPathFinder extends LazyAStar 
{

	public WallCrawlerPathFinder(World w, BlockPos start, BlockPos target) 
	{
		super(w, start, target);
	}

	@Override
	protected boolean isPositionkValid(ParentCoords base, Direction dir, ParentCoords pos) 
	{
		if(hasSolidsAround(pos))
		{
			return true;
		}
		else if(isDiagonalSolid(pos))//so the path is going diagonal to an edge
		{
			return hasSolidsAround(base);
		}
		spawnParticle(pos, ParticleTypes.BARRIER);
		return false;
	}
	
	protected boolean hasSolidsAround(BlockPos pos)
	{
		for(Direction dir : FacingUtil.VALUES)
		{
			BlockPos testPos = pos.relative(dir);
			if(w.getBlockState(testPos).isFaceSturdy(w, testPos, dir.getOpposite()))
			{
				spawnParticle(pos, ParticleTypes.HAPPY_VILLAGER);
				return true;
			}
		}
		return false;
	}
	
	protected boolean isDiagonalSolid(BlockPos pos)
	{
		for(Direction dir : FacingUtil.VALUES)
		{
			BlockPos testPos = pos.relative(dir);
			if(hasSolidsAround(testPos))
				return true;
		}
		return false;
	}
	
	private void spawnParticle(BlockPos pos, IParticleData particle)
	{	
		//For Debug
//		if(w instanceof ServerWorld)
//		{
//			((ServerWorld) w).spawnParticle(particle, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 1, 0, 0, 0, 0);
//		}
	}
}
