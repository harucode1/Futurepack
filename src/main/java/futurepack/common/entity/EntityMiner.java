package futurepack.common.entity;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Predicate;

import futurepack.common.FPEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperInventory.SlotContent;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

//gespeiechert wird die position richtig beim laden wirds falsch
//
public class EntityMiner extends EntityDrone
{	
	private int last = 0;

	public double dis;
	public float rot;
	
	private int couldwon;
//	private Ticket chunckLoader;
	
	
	public EntityMiner(World w) 
	{
		this(FPEntitys.MINER, w);	
	}
	
	public EntityMiner(EntityType<EntityMiner> type, World w) 
	{
		super(type, w, 100F);	
		noPhysics=true;
		couldwon = 20;
	}
	
	public EntityMiner(World w, BlockPos pos)
	{
		this(w);
		setPos(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5);
		setInventoryPos(pos);
		w.destroyBlock(pos, true);
	}

	@Override
	protected void defineSynchedData() 
	{
		super.defineSynchedData();
	}
	
//	@Override
//	public AxisAlignedBB getCollisionBoundingBox() 
//	{
//		if(b)
//			return null;
//		return this.getBoundingBox();
//	}
	
	@Override
	public AxisAlignedBB getBoundingBoxForCulling() 
	{
		return super.getBoundingBox();
	}
	
	@Override
	protected boolean isMovementNoisy()
	{
		return false;
	}
	
	@Override
	public boolean isPickable() 
	{
		return !isAlive()==false;
	}
	
	@Override
	public boolean hurt(DamageSource ds, float par2) 
	{
		if(ds.getDirectEntity() != null)
		{
			if(!ds.getDirectEntity().hurt(ds, par2))
			{
				return ds.getDirectEntity().hurt(FuturepackMain.NEON_DAMAGE, par2);
			}
			return true;
		}
		return super.hurt(ds, par2);
	}
		
	@Override
	public ActionResultType interact(PlayerEntity pl, Hand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(it != null && it.getItem() == ToolItems.scrench && !level.isClientSide)
		{
			remove();
			ItemEntity ei = new ItemEntity(level,getX(),getY(),getZ(), new ItemStack(ToolItems.minerbox));
			level.addFreshEntity(ei);
			return ActionResultType.SUCCESS;
		}
		else if(!level.isClientSide)
		{
			setWorking(false);
			if(HelperResearch.canOpen(pl, this))
			{
				FPGuiHandler.DRONE_CLAIME.openGui(pl, this);
				return ActionResultType.SUCCESS;
			}
		}
		return ActionResultType.PASS;
	}
	
	@Override
	public void readAdditionalSaveData(CompoundNBT nbt)
	{
		super.readAdditionalSaveData(nbt);
//		setInventoryPos(new BlockPos(nbt.getInteger("InventoryX"), nbt.getInteger("InventoryY"), nbt.getInteger("InventoryZ")));
//		setSide(EnumFacing.byIndex(nbt.getByte("side")]);
		
		//hightLevel = nbt.getInteger("hightLevel");;
		
	}

	@Override
	public void addAdditionalSaveData(CompoundNBT nbt)
	{
		super.addAdditionalSaveData(nbt);
		
		
	}
	
	int waitItems = 0;
	
	@Override
	public void baseTick() 
	{
		super.baseTick();
		
		if(isWorking())
		{
			Direction dir = Direction.fromYRot(this.yRot);
			BlockPos xyz = this.blockPosition();
			
			for(int i=0;i<2;i++)
			{
				BlockPos jkl = xyz.relative(dir, i);
				Block b = level.getBlockState(jkl).getBlock();
				if(!level.isEmptyBlock(jkl))// && !(b instanceof BlockLiquid))
				{
					dis = Math.sqrt(distanceToSqr(jkl.getX()+0.5, jkl.getY()+0.5, jkl.getZ()+0.5));
					break;
				}
			}
			
			BlockPos jkl = xyz.relative(dir);	
			BlockState state = level.getBlockState(jkl);
			
			if(!isBlockLess(jkl, state))
			{
				float f = getHardness(state, jkl);
									
				if(getMiningProgress()>= f)
				{
					dropBlockAt(jkl);
					setPower(getPower()-1F);
					resetMiningProgress();
					waitItems=6;
				}
				else
				{
					addMiningProgress();
					this.rot -= 0.25F;
					if(this.rot < -1.5F)
						this.rot = -1.5F;
				}
			}
			else
			{
				if(!level.isClientSide)
				{
					if(dir.getStepX()==0)
						this.setPos(blockPosition().getX()+0.5, getY(), getZ());
					if(dir.getStepY()==0)
						this.setPos(getX(), blockPosition().getY()+0.2, getZ());
					if(dir.getStepZ()==0)
						this.setPos(getX(), getY(), blockPosition().getZ() + 0.5);
					
					teleportTo(getX(),getY(),getZ());
				}
				
				double speed = getSpeed();	
				
				if(!level.isClientSide) {

					move(MoverType.SELF, new Vector3d(dir.getStepX() * speed, dir.getStepY() * speed, dir.getStepZ() * speed));	
				
				}
				
				this.rot += 0.25F;
				if(this.rot > 0)
					this.rot = 0;
			}
			
			if(last <= 0 && isAtRotationPoint(xyz))//super important: 'last' must prevent the 'isAtRotationPoint' call because it changes the rotation; if not the miner will just spin 
			{
					setPos(xyz.getX() +0.5, xyz.getY()+0.2, xyz.getZ()+0.5);
					last = 10;
			}
			last--;
	
			if(waitItems<=0 && (!isInArea() || isBlockLess()))
			{
				if(!level.isClientSide)
				{
					yRot = 0;
					
					ClaimeData data = ClaimeData.getCurrentData(this);
					BlockPos c = data.getCurentMiddle();
					
					int hightLevel = xyz.getY() - c.getY();
					hightLevel--;
					
			        //porting to Next Claime
					setPos(c.getX()+0.5, c.getY()+0.2 + hightLevel, c.getZ()+0.5);
			        
					setDone(false);
					if(Math.abs(hightLevel)>=data.getMaxY())
					{
						if(currentDone<todo.size())
						{
							setClaime("");
							currentDone++;
							data = ClaimeData.getCurrentData(this);
							c = data.getCurentMiddle();
							setPos(c.getX()+0.5, c.getY()+0.2, c.getZ()+0.5);
						}
						else
						{
							setWorking(isRepeat());
							c = getInventoryPos();
							setPos(c.getX()+0.5, c.getY()+0.2, c.getZ()+0.5);
							if(isRepeat())
							{
								currentDone = 0;
							}
						}						
						
					}
					else
					{
						dropBlockAt(c.offset(0,hightLevel,0));
					}
				}
				
			}
			if(waitItems>0)
				waitItems--;
				
			if(this.yRot >= 360F)
			{
				this.yRot -= 360F;
			}
			if(this.yRot < 0F)
			{
				this.yRot += 360F;
			}
		}
	}
	
	
	
	private boolean isBlockLess()
	{
		ClaimeData data = ClaimeData.getCurrentData(this);
		BlockPos c = data.getCurentMiddle();
		
		int y = MathHelper.floor(this.getY());
		
		if(y==0)
			return true;
		
		BlockPos.Mutable xyz = new BlockPos.Mutable();
		for(int x=c.getX()-(data.getMaxX()-1);x<c.getX()+data.getMaxX();x++)
		{
			for(int z=c.getZ()-(data.getMaxZ()-1);z<c.getZ()+data.getMaxZ();z++)
			{
				xyz.set(x, y, z);
				if(!isBlockLess(xyz, level.getBlockState(xyz)))					
				{
					return false;
				}
			}
		}
		return true;
	}
	

	
	private boolean isInArea()
	{		
		return isInArea(getX(), getY(), getZ());
	}
	
	/**
	 * @param xyz the position of this entity as block
	 */
	private boolean isAtRotationPoint(BlockPos xyz)
	{
		ClaimeData data = ClaimeData.getCurrentData(this);
		BlockPos c = data.getCurentMiddle();
		int j = (xyz.getX() - c.getX());
		int k = (xyz.getY() - c.getY());
		int l = (xyz.getZ() - c.getZ());
		
		if(isDone())
		{     	
			boolean flag1 = l==-(data.getMaxZ()-1);
			boolean flag2 = l==(data.getMaxZ()-1);
			if(flag1 || flag2) // der Miner arbietet sich in X richtung vor. heist an den Z eckpunkten dreht er
			{
				if(j>=-(data.getMaxX()-1) && j < data.getMaxX())
				{
					if((flag1 && ((j+(data.getMaxX()-1)) % 2 == 0)) || (flag2 && ((j+(data.getMaxX()-1)) % 2 != 0)))
					{     				
						this.yRot = 90F;
						return true;
					}
					if((flag1 && ((j+(data.getMaxX()-1)) % 2 != 0)) || (flag2 && ((j+(data.getMaxX()-1)) % 2 == 0)))
					{
						boolean blockless = true;
						for(int gz=-(data.getMaxZ()-1);gz<data.getMaxZ();gz++)
						{
							BlockPos posss = new BlockPos(xyz.getX(), xyz.getY(), c.getZ()+gz);
							BlockState b = level.getBlockState(posss);
							if(!isBlockLess(posss, b))		      					
							{
								blockless = false;
								break;
							}
							
							posss = posss.west();
							b = level.getBlockState(posss);
							if(!isBlockLess(posss, b))				      					
							{
								blockless = false;
								break;
							}
						}
						if(blockless)
						{
							return true;
						}
						if(flag1)
							this.yRot = 0F;
						if(flag2)
							this.yRot = 180F;
						return true;
					}
				}
			}
			else
			{
				Direction dir = Direction.fromYRot(this.yRot);
				if(dir.getAxis()==Axis.Z)
				{
					BlockPos block = xyz;
					boolean blockless = true;
					while(block.getZ() - c.getZ()>=-(data.getMaxZ()-1) && block.getZ() - c.getZ() < data.getMaxZ())
					{
						BlockState b = level.getBlockState(block);
						if(isBlockLess(block, b))		      					
						{
							block = block.west();
							b = level.getBlockState(block);
							if(isBlockLess(block, b))		      					
							{
								block = block.relative(dir);
								continue;
							}
						}
						
						blockless = false;
						break;
					}
					if(blockless)
					{
						this.yRot = 90F; //here
						return true;
					}
				}
				else
				{
					dir = Direction.fromYRot( (j+(data.getMaxX()-1) ) % 2 == 0 ? 180 : 0);
					boolean blockless = true;
					BlockPos block = xyz;
					while(block.getZ() - c.getZ()>=-(data.getMaxZ()-1) && block.getZ() - c.getZ() < data.getMaxZ())
					{
						BlockState b = level.getBlockState(block);
						if(!isBlockLess(block, b))		      					
						{
							blockless = false;
							break;
						}
						
						BlockPos west = block.west();
						b = level.getBlockState(west);
						if(!isBlockLess(west, b))		      					
						{
							blockless = false;
							break;
						}
						block = block.relative(dir);
					}
					if(!blockless)
					{
						if( ( j+(data.getMaxX()-1) ) % 2 == 0)
						{
							this.yRot = 180F;
						}
						else
						{
							this.yRot = 0F; //here
						}
						return true;
					}
				}
				
			}
		}
		else
		{
			
			if(j==0 && l==0)
			{
				this.yRot = 0F;
				return true;
			}
			
			if(j==0 && l==data.getMaxZ()-1)
			{
				this.yRot = 270F;
				return true;
			}
			
			if(j==data.getMaxX()-1 && l==data.getMaxZ()-1)
			{
				this.yRot = 180F;
				setDone(true);
				return true;
			}
			
		}
		
		
		return false;
	}

	
	@Override
	public void tick() 
	{
//		Log.info(this.toString());
		
		if(couldwon>0)
		{
			couldwon--;
			return;
		}
		
		dis = 0;
		
		if(getPower()<getMaxPower())
		{
			tryCharge();
		}
		if(consumePower())
		{
			super.tick();
		}
		
		if(!level.isClientSide)
		{
			AxisAlignedBB bb = getBoundingBox().inflate(1, 1, 1);
			
			level.getEntitiesOfClass(FallingBlockEntity.class, bb, new Predicate<FallingBlockEntity>() //getEntitiesWithinAABBExcludingEntity
			{
				@Override
				public boolean apply(FallingBlockEntity e)
				{
					if(e.isAlive()==false)
						return false;
					e.time=1000;
					return false;
				}
			});
			
			List<ItemEntity> item = level.getEntitiesOfClass(ItemEntity.class, bb, new Predicate<ItemEntity>() //getEntitiesWithinAABBExcludingEntity
			{	
				@Override
				public boolean apply(ItemEntity e)
				{
					if(e.isAlive()==false)
						return false;					
								
					return true;
				}
			});	
			if(!item.isEmpty())
			{
				final ArrayList<SlotContent> items = new ArrayList<>(item.size());
				for(ItemEntity e : item)
				{
					e.setPos(EntityMiner.this.getX(), EntityMiner.this.getY(), EntityMiner.this.getZ());
					items.add(new SlotContent(null, 0, e.getItem(), e));
				}
				
				ArrayList<SlotContent> done = (ArrayList<SlotContent>) HelperInventory.insertItems(getDroneStattion(), getSide(), items);
				for(SlotContent slot : done)
				{
					slot.remove();
				}
			}
		}
		
		HelperChunks.forceloadChunksForEntity(this);
	}
	
//	@Override
//	public AxisAlignedBB getCollisionBox(Entity e)
//	{
//		return null;
//	}
	
	@Override
	public void onRemovedFromWorld() 
	{
		super.onRemovedFromWorld();
		if(!level.isClientSide)
		{
			HelperChunks.removeTicketIfNeeded((ServerWorld) level, this);
		}
	}
	
	
	
	
	private void dropBlockAt(BlockPos pos)
	{
		//TODO add check if block is in miner area
		
		if(level.isEmptyBlock(pos))
			return;
		if( isHomeBlock(pos))
			return;
//		
		BlockState state = level.getBlockState(pos);
		if(isBlockLess(pos, state))
			return;
		
//		if(state.getBlock() == FPBlocks.claime || state.getBlock() == Blocks.BEDROCK)		
//			return;
//		
		
		if(!level.isClientSide)
		{	
			level.destroyBlock(pos, true);
		}
		
		playSound(SoundEvents.REDSTONE_TORCH_BURNOUT, 1F, 1F);
		for(int i=0;i<10;i++)
		{
			level.addParticle(ParticleTypes.LARGE_SMOKE, pos.getX()+random.nextFloat(), pos.getY()+random.nextFloat(), pos.getZ()+random.nextFloat(), 0, 0, 0);
		}
	}

	
//	private DataWatcher.WatchableObject getObject(int id)
//	{
//		List<DataWatcher.WatchableObject> list = this.dataManager.getAllWatched();
//		
//		if(id < list.size())
//		{
//			for(DataWatcher.WatchableObject w : list)
//			{
//				if(w.getDataValueId()==id)
//				{
//					return w;
//				}
//			}
//		}
//		return null;
//	}
	
	@Override
	protected float getMiningProgressModifier()
	{
		return 0.1F;
	}
	
	@Override
	protected float getEnergieUse()
	{
		return 0.1F;
	}
	
	private float getSpeed()
	{
		return 0.15F;
	}
}
