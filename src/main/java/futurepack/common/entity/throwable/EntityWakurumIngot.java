package futurepack.common.entity.throwable;

import futurepack.common.FPEntitys;
import futurepack.common.item.ResourceItems;
import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRendersAsItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
@OnlyIn(
	value = Dist.CLIENT,
	_interface = IRendersAsItem.class
)
public class EntityWakurumIngot extends ThrowableEntity implements IRendersAsItem
{
	public EntityWakurumIngot(EntityType<EntityWakurumIngot> type, World w)
	{
		super(type, w);
	}

	public EntityWakurumIngot(World w, LivingEntity throwerIn)
	{
		super(FPEntitys.WAKURIUM_THROWN, throwerIn, w);
	}

	@Override
	protected void onHit(RayTraceResult result)
	{
		if(result.getType() ==Type.BLOCK)
		{
			BlockRayTraceResult block = (BlockRayTraceResult) result;
			BlockState state = level.getBlockState(block.getBlockPos());
			if(!state.getCollisionShape(level, block.getBlockPos()).isEmpty())
			{
				Vector3d mot = this.getDeltaMovement();
				switch(block.getDirection().getAxis())
				{
				case X:
					setDeltaMovement(mot.multiply(-0.95, 1, 1));
					break;
				case Y:
					setDeltaMovement(mot.multiply(1, -0.95, 1));
					break;
				case Z:
					setDeltaMovement(mot.multiply(1, 1, -0.95));
					break;
				default:
					break;		
				}
				setPos(block.getLocation().x, block.getLocation().y, block.getLocation().z);
			}		
		}
		
		if(result.getType() == Type.ENTITY)
		{
			((EntityRayTraceResult)result).getEntity().hurt(DamageSource.mobAttack((LivingEntity) getOwner()), 1.8F);
		}
		else if(!level.isClientSide)
		{
			double dis = this.getDeltaMovement().lengthSqr();
			
			if(dis<0.1)
			{
				remove();
				ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), new ItemStack(ResourceItems.ingot_wakurium));
				item.setDeltaMovement(this.getDeltaMovement());
				level.addFreshEntity(item);
			}
		}
	}

	@Override
	protected void defineSynchedData() 
	{
		
	}

	@Override
	public ItemStack getItem() 
	{
		return new ItemStack(ResourceItems.ingot_wakurium);
	}

	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
