package futurepack.common.entity.throwable;


import java.util.ArrayList;
import java.util.List;

import futurepack.common.FPEntitys;
import futurepack.common.item.tools.ItemGrablingHook;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageHookLines;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRendersAsItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.fml.network.PacketDistributor;
@OnlyIn(
	value = Dist.CLIENT,
	_interface = IRendersAsItem.class
)
public class EntityHook extends ThrowableEntity implements IRendersAsItem
{
	private static final DataParameter<Integer> ACTIVE = EntityDataManager.defineId(EntityHook.class, DataSerializers.INT);
//	private static final DataParameter<Integer> THROWER = EntityDataManager.createKey(EntityHook.class, DataSerializers.VARINT);
//	
	boolean collided = false; // <-- needs to be synced
	
	private LivingEntity clientThrower;
	private List<Vector3d> list = null;
	
	public EntityHook(World w, LivingEntity lb)
	{
		super(FPEntitys.HOOK, lb, w);
		
		this.setDeltaMovement(this.getDeltaMovement().add(lb.getDeltaMovement()));

		if(!level.isClientSide)
			list = new ArrayList<>();
	}

	public EntityHook(EntityType<EntityHook> type, World p_i1776_1_) 
	{
		super(type, p_i1776_1_);
		if(!level.isClientSide)
			list = new ArrayList<>();
	}

	@Override
	protected void defineSynchedData()
	{
		this.entityData.define(ACTIVE, -1);
//		super.registerData();
	}
	
	
	
	@Override
	public void shoot(double d1, double d2, double d3, float p_70186_7_, float p_70186_8_)
    {
		 float f2 = MathHelper.sqrt(d1 * d1 + d2 * d2 + d3 * d3);
		 d1 /= f2;
		 d2 /= f2;
		 d3 /= f2;
//		 d1 += this.rand.nextGaussian() * 0.007499999832361937D * (double)p_70186_8_;
//		 d2 += this.rand.nextGaussian() * 0.007499999832361937D * (double)p_70186_8_;
//		 d3 += this.rand.nextGaussian() * 0.007499999832361937D * (double)p_70186_8_;
		 d1 *= p_70186_7_;
		 d2 *= p_70186_7_;
		 d3 *= p_70186_7_;
		 this.setDeltaMovement(new Vector3d(d1*2.5, d2*2.5, d3*2.5));
		 float f3 = MathHelper.sqrt(d1 * d1 + d3 * d3);
		 this.yRotO = this.yRot = (float)(Math.atan2(d1, d3) * 180.0D / Math.PI);
		 this.xRotO = this.xRot = (float)(Math.atan2(d2, f3) * 180.0D / Math.PI);
		 
    }
	
	
	
	@Override
	protected void onHit(RayTraceResult mov) 
	{
		if(level.isClientSide && mov.getType() == Type.BLOCK)
		{
			this.setDeltaMovement(Vector3d.ZERO);
		}
		if(getOwner()==null)
		{
			return;
		}
		if(mov.getType() == Type.BLOCK)
		{
			this.setDeltaMovement(Vector3d.ZERO);
			
			Vector3d hitVec = ((BlockRayTraceResult)mov).getLocation();
			Vector3d vec = hitVec.add(-this.getX(), -this.getY(), -this.getZ());
			double l = vec.length();
			
			this.setPos(hitVec.x - (vec.x/l * 0.0625), hitVec.y - (vec.y/l * 0.0625), hitVec.z - (vec.z/l * 0.0625));

			collided = true;
			sync();
		}

	}
	
	private void sync()
	{
		if(!level.isClientSide)
		{
			ServerPlayerEntity mp = (ServerPlayerEntity) getOwner();
			MessageHookLines mes = new MessageHookLines(this.getId(), mp.getId(), list);
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> mp), mes);
		}
	}
	
	@Override
	public void tick() 
	{
		super.tick(); 

		if(collided)
		{
			this.setDeltaMovement(Vector3d.ZERO);

			Entity thrower = getOwner();
			if(thrower == null)
			{
				//this happens when e.g. you are atached and the player si teleportewt away.
				remove();
			}
			thrower.fallDistance = 0F;
//			List<EntityPlayer> players = worldObj.getEntitiesWithinAABB(EntityPlayer.class, AxisAlignedBB.fromBounds(this.getPosX()-0.5, this.getPosY()-0.5, this.getPosZ()-0.5, this.getPosX()+0.5, this.getPosY()+0.5, this.getPosZ()+0.5));
//			if(!players.contains(throrer))
			{
				
				Vector3d pos = getFirstRopePart();
				
				level.addParticle(ParticleTypes.CRIT, pos.x, pos.y, pos.z, 0F, 0F, 0F);
				
				
				
				double d = Math.sqrt(pos.distanceToSqr(thrower.getX(), thrower.getY(), thrower.getZ()));
				
				if(!level.isClientSide && d <= thrower.getBbWidth() && getRopeParts() > 0)
				{
					removeFirstRopePart();
				}
					
				d = Math.sqrt(pos.distanceToSqr(thrower.getX(), thrower.getY()-thrower.getEyeHeight(), thrower.getZ()));
				d = d < 1 ? 1 : d;
				double x = pos.x - thrower.getX();
				double y = pos.y - (thrower.getY());//-throrer.getStandingEyeHeight());
				double z = pos.z - thrower.getZ();

				thrower.setDeltaMovement(new Vector3d(x/d, y/d, z/d));
			}
			
			if(thrower.isAlive()==false)
				remove();
		}
		else if(!level.isClientSide)
		{
			if(getOwner()==null)
			{
				remove();
				return;
			}
			subdivideLine();
		}
		if(!level.isClientSide)
		{
			EntityHook other = ((ItemGrablingHook )ToolItems.grappling_hook).map(level).get(getOwner());
			if(other != this)
			{
				this.remove();
			}
		}
	}	
	
	private void removeFirstRopePart()
	{
		if(!level.isClientSide)
		{
			list.remove(0);
			int active = entityData.get(ACTIVE);
			active++;
			entityData.set(ACTIVE,active);
		}
	}

	private void subdivideLine()
	{
		Vector3d a = this.position();
		
		Vector3d b = null;
		if(getRopeParts() == 0)
		{
			b = getOwner().position().add(0, getOwner().getEyeHeight(), 0);
		}
		else
		{
			b = list.get(list.size()-1);
		}
		
		BlockRayTraceResult ray = level.clip(new RayTraceContext(b, a, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.NONE, getOwner()));
		if(ray!=null && ray.getType()==Type.BLOCK)
		{
			Vector3d hit = ray.getLocation();
			
//			Vector3d ah = hit.subtract(a);
//			Vector3d bh = hit.subtract(b);
//			
//			Vector3d away = ah.add(bh).normalize();
			
			Vector3d point = hit;
			point = point.add(Vector3d.atLowerCornerOf(ray.getDirection().getNormal()));
			
			addRopePart(point);
		}
	}
	
	private int getRopeParts()
	{
		if(list==null)
			return -1;
		
		return list.size();
	}
	
	private void addRopePart(Vector3d vec)
	{
		if(level.isClientSide)
			return;
		
		list.add(vec);
	}
	
	private Vector3d getFirstRopePart()
	{
		if(level.isClientSide)
		{
			int active = this.entityData.get(ACTIVE);
			if(list!=null && active >= 0 && active < list.size())
			{
				return list.get(active);
			}
			else
			{
				list = null;
			}
			return position();
		}
		else
		{
			if(list.isEmpty())
			{	
				return position();
			}
			else
			{
				return list.get(0);
			}
		}
	}
	
	
	
	@Override
	public Entity getOwner()
	{
		if(level.isClientSide)
			return getClientThrower();
		return super.getOwner();
	}
	
	public LivingEntity getClientThrower()
	{
		return clientThrower;
	}
	
	public void setThrower(int id) //client only
	{
		if(level.isClientSide)
		{
			this.clientThrower = (LivingEntity) level.getEntity(id);
		}
	}
	
	public void setList(List<Vector3d> list) //client Only
	{
		this.list = list;
		for(Vector3d vec : list)
		{
			level.addParticle(ParticleTypes.BARRIER, vec.x, vec.y, vec.z, 0F, 0F, 0F);
		}
		collided = true;
	}
	
	@Override
	public void remove()
	{
//		if(world.isRemote)
//		{
//			IllegalStateException e = new IllegalStateException("killed on client");
//			e.printStackTrace();
//		}
		super.remove();
	}

	@Override
	public ItemStack getItem() 
	{
		return new ItemStack(Items.SLIME_BALL);
	}
	
	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
