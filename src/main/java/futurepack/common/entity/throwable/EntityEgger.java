package futurepack.common.entity.throwable;

import futurepack.common.FPEntitys;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRendersAsItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.boss.dragon.EnderDragonPartEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
@OnlyIn(
	value = Dist.CLIENT,
	_interface = IRendersAsItem.class
)
public class EntityEgger extends ThrowableEntity implements IRendersAsItem
{

	public EntityEgger(World w, double x, double y, double z) 
	{
		super(FPEntitys.ENTITY_EGGER, x, y, z, w);
	}

	public EntityEgger(World w, LivingEntity p_i1777_2_)
	{
		super(FPEntitys.ENTITY_EGGER, p_i1777_2_, w);
	}

	public EntityEgger(EntityType<EntityEgger> type, World w) 
	{
		super(type, w);
	}

	@Override
	protected void onHit(RayTraceResult pos) 
	{		
		if(level.isClientSide)
			return;
		
		if(pos.getType()==Type.ENTITY)
		{
			Entity hit = ((EntityRayTraceResult)pos).getEntity();
			
			if(hit == getOwner())
				return;
			
			if(hit instanceof EnderDragonPartEntity)
			{
				EnderDragonPartEntity p = (EnderDragonPartEntity) hit;
				if(p.parentMob!=null)
				{
					hit = p.parentMob;
				}
			}
			EntityType id = hit.getType();
			if(id == null || id == EntityType.PLAYER)
			{
				ItemStack it = new ItemStack(ToolItems.entity_egger);
				ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), it);
				level.addFreshEntity(item);
				remove();
				return;
			}
			
			ItemStack it = new ItemStack(ToolItems.entity_egger_full);
			it.setTag(new CompoundNBT());
			hit.saveAsPassenger(it.getTag());
			hit.remove();	
			it.setHoverName(hit.getName());
			ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), it);
			level.addFreshEntity(item);
			remove();
		}
		else
		{
			ItemStack it = new ItemStack(ToolItems.entity_egger);
			ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), it);
			level.addFreshEntity(item);
			remove();
		}
	}

	@Override
	protected void defineSynchedData() 
	{
		
	}

	@Override
	public ItemStack getItem() 
	{
		return new ItemStack(ToolItems.entity_egger);
	}

	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
