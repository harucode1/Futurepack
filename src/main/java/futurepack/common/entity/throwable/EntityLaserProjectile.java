package futurepack.common.entity.throwable;

import futurepack.common.FPEntitys;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.particles.RedstoneParticleData;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityLaserProjectile extends ThrowableEntity
{

	public EntityLaserProjectile(World worldIn, LivingEntity th)
	{
		super(FPEntitys.LASER_PROJECTILE, th, worldIn);
	}
	
	public EntityLaserProjectile(EntityType<EntityLaserProjectile> type, World w)
	{
		super(type, w);
	}

	@Override
	protected void onHit(RayTraceResult pos)
	{
		if(pos.getType() == Type.BLOCK)
		{
			BlockRayTraceResult bl = (BlockRayTraceResult) pos;
			if(level.isClientSide)
			{
				float[] vel = new float[3]; 
				
				vel[0] = 0.15F * bl.getDirection().getStepX() * random.nextFloat();
				vel[1] = 0.15F * bl.getDirection().getStepY() * random.nextFloat();		
				vel[2] = 0.15F * bl.getDirection().getStepZ() * random.nextFloat();

				for(int i=0;i<vel.length;i++)
				{
					if(vel[i]==0)
					{
						vel[i] = random.nextFloat() * 0.05F;
					}
				}
				
				level.addParticle(ParticleTypes.FLAME, bl.getLocation().x, bl.getLocation().y, bl.getLocation().z, vel[0], vel[1], vel[2]);
//				worldObj.addParticle(ParticleTypes.FLAME, pos.hitVec.xCoord, pos.hitVec.yCoord, pos.hitVec.zCoord, vel[3], vel[4], vel[5]);
			}
		}
		else if(pos.getType() == Type.ENTITY)
		{
			EntityRayTraceResult ent = (EntityRayTraceResult) pos;
			if(ent.getEntity() == getOwner())
				return ;
			
			ent.getEntity().hurt(DamageSource.mobAttack((LivingEntity) getOwner()), 18);
		}
		if(!level.isClientSide)
			this.remove();
	}

	@Override
	public void tick()
	{		
		Vector3d m = this.getDeltaMovement();
		double mx = m.x;
		double my = m.y;
		double mz = m.z;
		
		level.addParticle(new RedstoneParticleData(1F, 0F, 0F, 1F), getX(), getY(), getZ(), 0,0,0);
		
		super.tick();
		
		float f1 = MathHelper.sqrt(mx * mx + mz * mz);
		this.yRotO = this.yRot = (float)(MathHelper.atan2(mx, mz) * 180.0D / Math.PI);
        this.xRotO = this.xRot = (float)(MathHelper.atan2(my, f1) * 180.0D / Math.PI);		
        
		this.setDeltaMovement(m);
	}
	
	@Override
	public float getBrightness()
	{
		return level.getRawBrightness(blockPosition(), 15);
	}

	@Override
	protected void defineSynchedData() 
	{
		
	}
	
	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
}
