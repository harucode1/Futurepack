package futurepack.common.entity.throwable;

import java.util.Random;
import java.util.UUID;

import javax.annotation.Nullable;

import futurepack.common.FPEntitys;
import futurepack.common.FPPotions;
import futurepack.common.player.FakePlayerFactory;
import futurepack.common.player.FuturepackPlayer;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.entity.AreaEffectCloudEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.potion.Potion;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.Explosion;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityRocket extends ThrowableEntity
{
	private Entity cache=null;
	protected static final DataParameter<Integer> TARGET = EntityDataManager.defineId(EntityRocket.class, DataSerializers.INT);
	
	public boolean doPlayerDamage = false;
	public Vector3d doTransport = null;

	private double startDis = -1;
	private Vector3d startVec = null;
	
	public EntityRocket(EntityType<? extends EntityRocket> type, World worldIn)
	{
		super(type, worldIn);
	}
	
	public EntityRocket(EntityType<? extends EntityRocket> type, World w, LivingEntity base)
	{
		super(type, base, w);
		setTarget(base);
	}

	@Override
	protected void defineSynchedData()
	{
		this.entityData.define(TARGET, -1);
	}
	
	public void setTarget(Entity e)
	{
		cache=e;
		this.entityData.set(TARGET, e.getId());
	}

	
	public Entity getTarget()
	{
		if(cache==null)
		{
			int id = this.entityData.get(TARGET);
			this.cache = level.getEntity(id);
		}
		return cache;
	}
	
	@Override
	public void readAdditionalSaveData(CompoundNBT nbt)
	{
		if(nbt.contains("target"))
		{
			UUID uuid = nbt.getUUID("target");
			ServerWorld ws = (ServerWorld) level;
			setTarget(ws.getEntity(uuid));
		}
		
	}

	@Override
	public void addAdditionalSaveData(CompoundNBT nbt)
	{
		if(getTarget()!=null)
		{
			Entity e = getTarget();
			nbt.putUUID("target", e.getUUID());
		}
		
	}

	@Override
	public void tick()
	{
		//Kill Rockets that are to long alive, to make the bug that rockets "dance" and not despawn less visible
		if(this.tickCount > 250) {
			this.remove();
		}
		//if(this.ticksExisted>5)
		//	return;
		
		Entity target = getTarget();
		if(target != null)
		{
			if(!target.isAlive())
				this.remove();
			
			if(startDis < 0)
			{
				startDis = this.distanceToSqr(target);
				if(startDis < 100)
					startDis = 100;
				Random r = new Random(this.getUUID().getMostSignificantBits());
				startVec = (new Vector3d(r.nextDouble()-0.5, r.nextDouble()-0.5, r.nextDouble()-0.5)).normalize().scale(5);
			}
			Vector3d Vector3d = new Vector3d(target.getX(),target.getY()+target.getEyeHeight(),target.getZ());
			
			if(startVec!=null && this.distanceToSqr(target) > startDis / 4)
			{
				Vector3d = Vector3d.add(startVec);
			}
			
			Vector3d vec = Vector3d.subtract(getX(), getY(), getZ());
			if(vec.lengthSqr() > 1)
				vec = vec.normalize();
			
			Vector3d mot = this.getDeltaMovement();
			mot = mot.scale(2).add(vec).scale(1/3F);
			this.setDeltaMovement(mot);
			
			AxisAlignedBB box = target.getBoundingBox();
			if(box.contains(this.position()))
			{
				onHit(new EntityRayTraceResult(target, this.position()));
			}
		}
		else {
			this.remove();
		}
		super.tick();
		
		
		
		Vector3d mot = this.getDeltaMovement();
		double mx = mot.x;
		double my = mot.y;
		double mz = mot.z;	
		if(level.isClientSide)
		{
			level.addParticle(ParticleTypes.CLOUD, getX(), getY(), getZ(), -mx*0.2, -my*0.2, -mz*0.2);
			level.addParticle(ParticleTypes.CLOUD, getX(), getY(), getZ(), 0, 0, 0);
			level.addParticle(ParticleTypes.CLOUD, getX(), getY(), getZ(), -mx*0.1, -my*0.1, -mz*0.1);
		}
		float f1 = MathHelper.sqrt(mx * mx + mz * mz);
		this.yRotO = this.yRot;
        this.xRotO = this.xRot;
		this.yRotO = this.yRot = (float)(MathHelper.atan2(mx, mz) * 180.0D / Math.PI);
        this.xRotO = this.xRot = (float)(MathHelper.atan2(my, f1) * 180.0D / Math.PI);	
		
	}

	@Override
	protected void onHit(RayTraceResult mov)
	{
		if(!level.isClientSide)
		{
			Entity e = this;
			if(doPlayerDamage)
			{
				FuturepackPlayer player = FakePlayerFactory.INSTANCE.getPlayer((ServerWorld) level);
				player.setPos(this.getX(), this.getY(), this.getZ());
				e = player;
			}
			attackTarget(e, mov);
			if(mov.getType() == Type.ENTITY)
			{
				((EntityRayTraceResult)mov).getEntity().setDeltaMovement(this.getDeltaMovement());
				
				if(doTransport!=null)
				{
					HelperItems.moveItemTo(level, doTransport, ((EntityRayTraceResult)mov).getEntity().position());
				}
			}
			
			this.remove();
		}
	}
	
	public void attackTarget(Entity damagingEntity, RayTraceResult mov)
	{
		Vector3d hitVec = mov.getLocation();
		level.explode(damagingEntity, hitVec.x, hitVec.y, hitVec.z, 1.0F, level.getGameRules().getBoolean(GameRules.RULE_MOBGRIEFING) ? Explosion.Mode.BREAK : Explosion.Mode.NONE);
	}
	
	public static class EntityPlasmaRocket extends EntityRocket
	{

		public EntityPlasmaRocket(World w, LivingEntity base)
		{
			super(FPEntitys.ROCKET_PLASMA, w, base);
		}
		
		public EntityPlasmaRocket(EntityType<EntityPlasmaRocket> type, World worldIn)
		{
			super(type, worldIn);
		}
		
		@Override
		public void attackTarget(@Nullable Entity damagingEntity, RayTraceResult mov)
		{
			Vector3d hitVec = mov.getLocation();
			level.explode(damagingEntity, hitVec.x, hitVec.y, hitVec.z, 2.0F, Explosion.Mode.NONE);
		}
	}
	
	public static class EntityBlazeRocket extends EntityRocket
	{

		public EntityBlazeRocket(World w, LivingEntity base)
		{
			super(FPEntitys.ROCKET_BLAZE, w, base);
		}
		
		public EntityBlazeRocket(EntityType<EntityBlazeRocket> type, World worldIn)
		{
			super(type, worldIn);
		}
		
		@Override
		public void attackTarget(@Nullable Entity damagingEntity, RayTraceResult mov)
		{
			Vector3d hitVec = mov.getLocation();
			level.explode(damagingEntity, hitVec.x, hitVec.y, hitVec.z, 2.0F, true, Explosion.Mode.NONE);
		}
	}
	
	public static class EntityBioteriumRocket extends EntityRocket
	{

		public EntityBioteriumRocket(World w, LivingEntity base)
		{
			super(FPEntitys.ROCKET_BIOTERIUM, w, base);
		}
		
		public EntityBioteriumRocket(EntityType<EntityBioteriumRocket> type, World worldIn)
		{
			super(type, worldIn);
		}
		
		@Override
		public void attackTarget(Entity damagingEntity, RayTraceResult mov)
		{
			if(mov.getType() == Type.ENTITY && ((EntityRayTraceResult)mov).getEntity() instanceof LivingEntity)
			{
				LivingEntity liv = (LivingEntity) ((EntityRayTraceResult)mov).getEntity();
				liv.addEffect(new EffectInstance(FPPotions.paralyze, 20 * 10, 2));
			}
			AreaEffectCloudEntity cloud = new AreaEffectCloudEntity(level, mov.getLocation().x, mov.getLocation().y, mov.getLocation().z);
			cloud.setOwner((LivingEntity) this.getOwner());
			cloud.setDuration(20  * 45);
			cloud.setRadius(6.0F);
			cloud.setRadiusOnUse(-0.4F);
			cloud.setWaitTime(10);
	        cloud.setRadiusPerTick((-cloud.getRadius() / cloud.getDuration()) * 0.5F);
	        cloud.setPotion(new Potion("Death", new EffectInstance(Effects.POISON, 20 * 20, 3)));
	        level.addFreshEntity(cloud);
	        Vector3d hitVec = mov.getLocation();
			level.explode(damagingEntity, hitVec.x, hitVec.y, hitVec.z, 2.0F, false, Explosion.Mode.NONE);
		}
	}
	
	public static class EntityNormalRocket extends EntityRocket
	{

		public EntityNormalRocket(EntityType<EntityNormalRocket> type, World worldIn)
		{
			super(type, worldIn);
		}
		
		public EntityNormalRocket(World w, LivingEntity base)
		{
			super(FPEntitys.ROCKET_NORMAL, w, base);
		}
	}
	
	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
