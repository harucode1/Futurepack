package futurepack.common.entity.throwable;

import futurepack.common.FPEntitys;
import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityLaser extends ThrowableEntity
{
	private static DataParameter<Boolean> critical = EntityDataManager.defineId(EntityLaser.class, DataSerializers.BOOLEAN);
	private static DataParameter<Byte> explPower = EntityDataManager.defineId(EntityLaser.class, DataSerializers.BYTE);

	private float power;
	
	public EntityLaser(World w)
	{
		super(FPEntitys.LASER, w);
	}
	
	public EntityLaser(EntityType<EntityLaser> type, World w)
	{
		super(type, w);
	}
	
	@Override
	protected void defineSynchedData() 
	{
		this.entityData.define(critical, false);
		this.entityData.define(explPower, (byte)0);
	}

	public void setIsCritical(boolean b)
    {
		 this.entityData.set(critical, b);
    }

    public boolean getIsCritical()
    {
    	return this.entityData.get(critical);
    }
	
    public void setExplosionPower(byte b)
    {
    	this.entityData.set(explPower, b);
    }
    
    private byte getExplosionPower()
    {
    	return this.entityData.get(explPower);
    }
    
	public EntityLaser(World w, LivingEntity base, float power)
	{
		super(FPEntitys.LASER, base, w);
//		this.getMotion().x *= 2;
//		this.getMotion().y *= 2;
//		this.getMotion().z *= 2;
		
		this.power = power;
	}

//	@Override
//	public void setThrowableHeading(double x, double y, double z, float velocity, float inaccuracy)
//    {
//        x *= (double)velocity;
//        y *= (double)velocity;
//        z *= (double)velocity;
//        this.getMotion().x = x;
//        this.getMotion().y = y;
//        this.getMotion().z = z;
//    }
	
	@Override
	protected void onHit(RayTraceResult mov)
	{
		if(level.isClientSide)
			return;
			
		if(mov.getType() == Type.BLOCK)
		{
			BlockPos pos = new BlockPos(mov.getLocation());
			BlockState b = level.getBlockState(pos);
			if(!b.isSolidRender(level, pos))
			{
				return;
			}
		}
		else if(mov.getType() == Type.ENTITY)
		{
			if(((EntityRayTraceResult)mov).getEntity() == getOwner() && tickCount<20)
				return;
			
			((EntityRayTraceResult)mov).getEntity().hurt(DamageSource.mobAttack((LivingEntity) getOwner()), 10 * power);
			((EntityRayTraceResult)mov).getEntity().setSecondsOnFire(1);
		}
		float p = getIsCritical()?0.2F : 0F;
		p += getExplosionPower();
	
		if(p>0)
			level.explode(getOwner(), mov.getLocation().x, mov.getLocation().y, mov.getLocation().z, p, false, Explosion.Mode.NONE);
		
		remove();
	}

	@Override
	public void tick() 
	{
		Vector3d mot = this.getDeltaMovement();
		super.tick();		
		this.setDeltaMovement(mot);

	}
	
	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
