package futurepack.common.entity.throwable;

import java.util.List;
import java.util.Optional;

import futurepack.common.FPEntitys;
import futurepack.common.item.misc.ItemKompost;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRendersAsItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;

public abstract class EntityGrenadeBase extends ThrowableEntity
{
	private int radius = 1;

	protected EntityGrenadeBase(EntityType<? extends EntityGrenadeBase> type, World worldIn, LivingEntity th, int size)
	{
		super(type, th, worldIn);
		this.radius = size;
	}
	
	protected EntityGrenadeBase(EntityType<? extends EntityGrenadeBase> type, World worldIn)
	{
		super(type, worldIn);
	}

	public void setSize(int size)
	{
		if(level.isClientSide)
			throw new IllegalStateException("This shouldbe called only on Server side");
		
		this.radius = size;
	}
	
	public void setThrower(LivingEntity th)
	{
		if(level.isClientSide)
			throw new IllegalStateException("This shouldbe called only on Server side");
		
		CompoundNBT nbt = new CompoundNBT();
		addAdditionalSaveData(nbt);
		nbt.put("owner", NBTUtil.createUUID(th.getUUID()));
		readAdditionalSaveData(nbt);
//		this.owner = th;
		//FIXME: maxbe the missing owner is important
		this.setPos(th.getX(), th.getY() + th.getEyeHeight() - 0.1F, th.getZ());
	}
	
	@Override
	protected abstract void onHit(RayTraceResult mov);
	
	@Override
	protected void defineSynchedData() 
	{

	}
	
	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
	@OnlyIn(
		value = Dist.CLIENT,
		_interface = IRendersAsItem.class
	)
	public static class Plasma extends EntityGrenadeBase implements IRendersAsItem
	{
		public Plasma(World worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_PLASMA, worldIn, throwerIn, size);
		}
		
		public Plasma(EntityType<Plasma> type, World worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(RayTraceResult mov)
		{
			if (!this.level.isClientSide)
	        {
	            this.level.explode(this, this.getX(), this.getY(), this.getZ(), 6F * super.radius, Mode.NONE);
	            this.remove();
	        }
		}

		@Override
		public ItemStack getItem() 
		{
			return new ItemStack(MiscItems.icon_plasma);
		}
	}
	
	public static class Normal extends EntityGrenadeBase
	{
		public Normal(World worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_NORMAL, worldIn, throwerIn, size);
		}
		
		public Normal(EntityType<Normal> type, World worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(RayTraceResult mov)
		{
			if (!this.level.isClientSide)
	        {
	            boolean flag = this.level.getGameRules().getBoolean(GameRules.RULE_MOBGRIEFING);
	            this.level.explode(this, this.getX(), this.getY(), this.getZ(), 3F * super.radius, flag ? Mode.BREAK : Mode.NONE);
	            this.remove();
	        }
		}
		
		@Override
		public void tick()
		{		
			double f1 = this.getDeltaMovement().length();
			double mx = this.getDeltaMovement().x;
			double mz = this.getDeltaMovement().z;
			double my = this.getDeltaMovement().y;
			this.yRotO = this.yRot = (float)(MathHelper.atan2(mx, mz) * 180.0D / Math.PI);
	        this.xRotO = this.xRot = (float)(MathHelper.atan2(my, f1) * 180.0D / Math.PI);		
	        
			super.tick();
		}
	}
	
	@OnlyIn(
		value = Dist.CLIENT,
		_interface = IRendersAsItem.class
	)
	public static class Blaze extends EntityGrenadeBase implements IRendersAsItem
	{
		public Blaze(World worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_BLAZE, worldIn, throwerIn, size);
		}
		
		public Blaze(EntityType<Blaze> type, World worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(RayTraceResult mov)
		{
			if (!this.level.isClientSide)
	        {
				int r=3 * super.radius;
	            this.level.explode(this, this.getX(), this.getY(), this.getZ(), 2F * super.radius, true, Mode.NONE);
	            for(int x=-r;x<r;x++)
	            {
	            	for(int y=-r;y<r;y++)
	 	            {
	            		for(int z=-r;z<r;z++)
	     	            {
	            			BlockPos pos = new BlockPos(mov.getLocation()).offset(x, y, z);
	            			if(level.isEmptyBlock(pos))
	            			{
	            				level.setBlockAndUpdate(pos, Blocks.FIRE.defaultBlockState());
	            			}
	     	            }	
	 	            }	
	            }
	            AxisAlignedBB bb =   new AxisAlignedBB(mov.getLocation().add(-r, -r, -r), mov.getLocation().add(r, r, r));
	            List<LivingEntity> list = level.getEntitiesOfClass(LivingEntity.class, bb);
	            for(LivingEntity liv : list)
	            {
	            	liv.setSecondsOnFire(100);	
	            }
	            this.remove();
	        }
		}

		@Override
		public ItemStack getItem() 
		{
			return new ItemStack(MiscItems.icon_blaze);
		}
	}
	
	@OnlyIn(
		value = Dist.CLIENT,
		_interface = IRendersAsItem.class
	)
	public static class Slime extends EntityGrenadeBase implements IRendersAsItem
	{
		public Slime(World worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_SLIME, worldIn, throwerIn, size);
		}
		
		public Slime(EntityType<Slime> type, World worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(RayTraceResult mov)
		{
			if(tickCount<20 && mov.getType()==Type.ENTITY && level.isClientSide && ((EntityRayTraceResult)mov).getEntity() instanceof PlayerEntity)
				return;
				
			int r = 3 * super.radius;
			if (!this.level.isClientSide)
	        {
				AxisAlignedBB bb =   new AxisAlignedBB(mov.getLocation().add(-r, -r, -r), mov.getLocation().add(r, r, r));
	            List<LivingEntity> list = level.getEntitiesOfClass(LivingEntity.class, bb);
	            for(LivingEntity liv : list)
	            {
	            	slowDown(liv);	
	            }

	            
	        }
			else 
			{	
				for(int x=-r;x<r;x++)
	            {
	            	for(int y=-r;y<r;y++)
	 	            {
	            		for(int z=-r;z<r;z++)
	     	            {
	            			try {
	            				level.addParticle(ParticleTypes.ITEM_SLIME, true, getX()+x+0.5, getY()+y+0.5, getZ()+z+0.5, 0, 0, 0);
	            			} catch(NoSuchMethodError e) {
	            				e.printStackTrace();
	            			}
	     	            }	
	 	            }	
	            }
            }
			this.remove();
		}
		
		@Override
		public void remove()
		{
			int r = 3;
			if (this.level.isClientSide)
			{	
				for(int x=-r;x<r;x++)
	            {
	            	for(int y=-r;y<r;y++)
	 	            {
	            		for(int z=-r;z<r;z++)
	     	            {
	            			try {
	            				level.addParticle(ParticleTypes.ITEM_SLIME, true, getX()+x+0.5, getY()+y+0.5, getZ()+z+0.5, 0, 0, 0);
	            			} catch(NoSuchMethodError e) {
	            				e.printStackTrace();
	            			}
	     	            }	
	 	            }	
	            }
            }
			super.remove();
		}
		
		private void slowDown(LivingEntity base)
		{
			EffectInstance eff = new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 20*30, 4);
			base.addEffect(eff);
		}

		@Override
		public ItemStack getItem() 
		{
			return new ItemStack(MiscItems.icon_slime);
		}
	}

	public static class Kompost extends EntityGrenadeBase
	{
		public Kompost(World w, LivingEntity thrower, int size)
		{
			super(FPEntitys.GRENADE_KOMPOST, w, thrower, size);
		}
		
		public Kompost(EntityType<Kompost> type, World worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(RayTraceResult mov)
		{
			if(getOwner() instanceof PlayerEntity)
			{
				PlayerEntity pl = (PlayerEntity) getOwner();
			
				if(mov.getType() == RayTraceResult.Type.BLOCK)
				{
					int r = super.radius;
					for(int x=-r;x<r;x++)
					{
						for(int z=-r;z<r;z++)
						{
							for(int y=-r;y<r;y++)
							{
								if(r*r >= x*x + z*z +y*y)
								{
									ItemKompost.applyCompost(new ItemStack(MiscItems.kompost, 2), level, ((BlockRayTraceResult)mov).getBlockPos().offset(x,y,z), pl);
								}
							}
							
						}
					}
					this.remove();
				}
			}
			
		}
	}

	public static class Futter extends EntityGrenadeBase
	{
		public Futter(World worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_FUTTER, worldIn, throwerIn, size);
		}
		
		public Futter(EntityType<Futter> type, World worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(RayTraceResult mov)
		{
			if(!level.isClientSide)			
			{
				int r = 3 * super.radius;
				int animalsNum = 9 * super.radius;
				AxisAlignedBB bb =   new AxisAlignedBB(mov.getLocation().add(-r, -r, -r), mov.getLocation().add(r, r, r));
				List<AnimalEntity> list = this.level.getEntitiesOfClass(AnimalEntity.class, bb);
				for (AnimalEntity anim : list)
				{
					if(animalsNum>0)
					{
						if(anim.getAge()==0 && !anim.isInLove())
						{
							animalsNum--;
							if(getOwner() instanceof PlayerEntity)
								anim.setInLove((PlayerEntity) getOwner());
							else 
								anim.setInLove(null);
						}
					}
					else
					{
						break;
					}
				}	
				remove();
			}	
		}
	}

	public static class Saat extends EntityGrenadeBase
	{
		public Saat(World worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_SAAT, worldIn, throwerIn, size);
		}
		
		public Saat(EntityType<Saat> type, World worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(RayTraceResult mov)
		{
			if(!level.isClientSide && mov.getType() == RayTraceResult.Type.BLOCK)
			{
				int r = 1 * super.radius;
				for(int x=-r;x<=r;x++)
				{
					for(int z=-r;z<=r;z++)
					{
						for(int y=-1;y<=r;y++)
						{
							BlockState state = Blocks.WHEAT.defaultBlockState();
							BlockPos pos = ((BlockRayTraceResult)mov).getBlockPos().offset(x,y,z);
							if(state.canSurvive(level, pos))
							{
								level.setBlockAndUpdate(pos, state);
								break;
							}		
						}
					}
				}
				remove();
			}
		}		
	}	
	
	@OnlyIn(
		value = Dist.CLIENT,
		_interface = IRendersAsItem.class
	)
	public static class EnityEggerFullGranade extends EntityGrenadeBase implements IRendersAsItem
	{
		
		private CompoundNBT enity_tag;
		
		public EnityEggerFullGranade(World worldIn, LivingEntity throwerIn, CompoundNBT nbt)
		{
			super(FPEntitys.GRENADE_ENTITY_EGGER, worldIn, throwerIn, 1);
			enity_tag = nbt;
		}
		
		public EnityEggerFullGranade(EntityType<EnityEggerFullGranade> type, World worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(RayTraceResult mov)
		{
			if(enity_tag == null)
			{
				remove();
				return;
			}
			
			if(mov.getType() == RayTraceResult.Type.BLOCK || mov.getType() == RayTraceResult.Type.ENTITY)
			{
				Optional<Entity> o = EntityType.create(enity_tag, this.level);
				o.ifPresent(e -> 
				{
					if(e instanceof MobEntity)
					{
						MobEntity liv = (MobEntity) e;
						e.setPos(mov.getLocation().x, mov.getLocation().y + liv.getMyRidingOffset(), mov.getLocation().z);
						e.setUUID(MathHelper.createInsecureUUID(this.random));
						level.addFreshEntity(e);
					}
				});
				remove();
			}
		}
		
		@Override
		public ItemStack getItem() 
		{
			return new ItemStack(ToolItems.entity_egger_full);
		}
	}
}
