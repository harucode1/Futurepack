package futurepack.common.entity;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import futurepack.api.ParentCoords;
import futurepack.common.entity.LazyAStar.EnumPathSearch;
import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

/**
 * A navigator for Entity
 * Only used by ForstMaster
 */
public class Navigator
{
	protected final Entity entity;
	
	protected LinkedList<BlockPos> target = new LinkedList<BlockPos>(); 
	
	protected LazyAStar searching;
	
	public Navigator(Entity entity)
	{
		this.entity = entity;
		searching = null;
	}
	
	public void addTarget(BlockPos pos)
	{
		target.addLast(pos);
	}
	
	public void move(float speed)
	{
		entity.level.getProfiler().push("move");
		
		if(isFinished())
			return; 
		
		BlockPos that = entity.blockPosition();
		BlockPos him = getTarget();
		
		if(entity.getCommandSenderWorld() instanceof ServerWorld)
			((ServerWorld)entity.getCommandSenderWorld()).sendParticles(ParticleTypes.BUBBLE_POP, him.getX()+0.5, him.getY()+0.5, him.getZ()+0.5, 0, 0, 0, 0, 0);
		
		Vector3d motion = getMotionToBlock().scale(speed);
//		if(motion.lengthSquared()<1.0e-10D)
//		{
//			isFinished();
//			motion = getMotionToBlock().scale(speed);
////			move(speed);
////			return;
//		}
		if(calcRotaion(motion))
		{
			entity.setDeltaMovement(motion);
			entity.move(MoverType.SELF, motion);
		}
		
		boolean movedHorizontal =Math.max( Math.abs(motion.x) ,  Math.abs(motion.z)) >= 0.0625*0.0625;
		boolean movedVerticaly = Math.abs(motion.y) >= 0.0625;
		
		if(movedHorizontal && entity.horizontalCollision || movedVerticaly && entity.verticalCollision)
		{
			entity.level.getProfiler().push("findPath");
			lazyAStar(motion);
			entity.level.getProfiler().pop();
		}
		entity.level.getProfiler().pop();
	}
	
	protected LazyAStar initPathSearch(World world, BlockPos start, BlockPos target)
	{
		return new LazyAStar(world, start, target);
	}
	
	protected void lazyAStar(Vector3d motion)
	{
		if(searching == null)
		{
			searching = initPathSearch(entity.level, entity.blockPosition(), getTarget());
			searching.hitBox = entity.getBoundingBox().move(-entity.getX() +0.5, -entity.getY() +0.5-this.entity.getEyeHeight()/2, -entity.getZ() +0.5);
		}
		EnumPathSearch path = searching.searchPathWithTimeout(10);
		
		if(path == EnumPathSearch.TIMEOUT )
		{
			return;
		}
		else if(path == EnumPathSearch.START_BLOCKED)
		{
			entity.move(MoverType.SELF, motion.scale(-1));
			BlockPos pos = entity.blockPosition();
			//For Debug
			entity.getCommandSenderWorld().addParticle(ParticleTypes.BARRIER, pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, 0, 0, 0);
			
			
			BlockPos end = pos.relative(Direction.getNearest((float)-motion.x, (float)-motion.y, (float)-motion.z));
			searching.clear();
			searching = null;
			nextTarget();
			target.addFirst(end);
			target.addFirst(pos);
			return;
		}
		else if(path == EnumPathSearch.TARGET_BLOCKED)
		{
			//For Debug
			BlockPos p = getTarget();
			entity.getCommandSenderWorld().addParticle(ParticleTypes.BARRIER, p.getX()+0.5F, p.getY()+0.5F, p.getZ()+0.5F, 0, 0, 0);
			
			searching.clear();
			searching = null;
			nextTarget();
			return;
		}
		
		
		ParentCoords coords = searching.getFinish();
		searching.clear();
		searching = null;
		if(coords==null)
		{
			nextTarget();
		}
		BlockPos offset = new BlockPos(-10,-10,-10); 
		ParentCoords last = coords;
		while(coords!=null)
		{
			BlockPos newoff = coords.subtract(last);
			if(newoff.equals(offset))
			{												
				this.target.set(0, new BlockPos(coords));
			}
			else
			{																			
				this.target.addFirst(new BlockPos(coords));
				
				offset = newoff;
			}
			
			last = coords;
			coords = coords.getParent();
		}
		cleanPath();
	}

	
	
	
	protected Vector3d getMotionToBlock()
	{
		Vector3d entity = this.entity.position();
		Vector3d mot = Vector3d.atLowerCornerOf(getTarget()).add(0.5, 0.5 -this.entity.getEyeHeight()/2, 0.5).subtract(entity);
		if(mot.lengthSqr()>0.1)
		{
			mot = mot.normalize();
		}
		return mot;
	}
	
	public boolean calcRotaion(Vector3d vec)
	{		
		final float delta = 90F / 5F;
		
		float pitch = (float) Math.toDegrees(-Math.asin(vec.y)); //neigung
		float yaw = (float) Math.toDegrees(-Math.atan2(vec.x, vec.z)); //drehwinkel
		entity.xRotO = entity.xRot;
		entity.yRotO = entity.yRot;
		
//		System.out.println(entity.rotationPitch + ", " + entity.rotationYaw);
		
		float dpitch = pitch - entity.xRot;
		float dyaw = yaw - entity.yRot;
		
		if(dyaw > 180)
			dyaw -= 360;
		if(dyaw < -180)
			dyaw += 360;
		
		if(dpitch > delta)
			dpitch = delta;
		if(dpitch < -delta)
			dpitch = -delta;
		
		if(dyaw > delta)
			dyaw = delta;
		if(dyaw < -delta)
			dyaw = -delta;
		
		entity.xRot += dpitch;
		entity.yRot += dyaw;
		
		if(entity.yRot > 180)
			entity.yRot -= 360;
		if(entity.yRot < -180)
			entity.yRot += 360;
		
//		System.out.println(entity.rotationPitch + ", " + entity.rotationYaw);
		
		return dyaw <= 0.1 && dpitch <= 0;
		
	}
	
	public boolean calcRotaion(Vector3i vec)
	{
		return calcRotaion(Vector3d.atLowerCornerOf(vec).add(0.5, 0.5-this.entity.getEyeHeight()/2, 0.5));
	}
	
	public boolean isFinished()
	{
		if(target.isEmpty())
			return true;
		
		BlockPos target = this.getTarget();
		AxisAlignedBB bb  =new AxisAlignedBB(0.4, 0.0, 0.4, 0.6, 0.2, 0.6).move(target).move(0.0, 0.4 -this.entity.getEyeHeight()/2, 0.0);		
		if(bb.contains(entity.position()))
		{
			nextTarget();
			return this.target.isEmpty();
		}
		return false;
	}

	public BlockPos getTarget()
	{
		if(target.isEmpty())
			return null;
		return target.getFirst();
	}
	
	public void nextTarget()
	{
		if(!target.isEmpty())
		{
			target.removeFirst();
		}
	}
	
	public void clear()
	{
		target.clear();
	}

	public void setPath(Collection<BlockPos> path)
	{
		target = new LinkedList<BlockPos>(path);
	}
	
	public BlockPos getFinish()
	{
		if(target.isEmpty())
			return null;
		return target.getLast();
	}
	
	/**Removed double entryis*/
	protected void cleanPath()
	{
		Iterator<BlockPos> iter = target.iterator();
		BlockPos last = null;
		while(iter.hasNext())
		{
			BlockPos pos = iter.next();
			if(pos.equals(last))
			{
				iter.remove();
			}
			else
			{
				last = pos;
			}
		}
	}
}
