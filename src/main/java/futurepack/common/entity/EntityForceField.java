package futurepack.common.entity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.google.common.base.Predicate;

import futurepack.common.FPEntitys;
import futurepack.common.FuturepackMain;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityForceField extends Entity
{
	private final static DataParameter<Float> SIZE = EntityDataManager.defineId(EntityForceField.class, DataSerializers.FLOAT);
	private final static DataParameter<Optional<UUID>> OWNER = EntityDataManager.defineId(EntityForceField.class, DataSerializers.OPTIONAL_UUID);
	
	public static EntityForceField createFromEntity(LivingEntity base)
	{
		if(base.level.isClientSide)
			return null;
		
		EntityForceField field = new EntityForceField(base.level);
		field.setSize(5);
		field.teleportTo(base.getX(), base.getY(), base.getZ());
		
		if(base.level.addFreshEntity(field))
		{
			field.setOwner(base);	
			return field;
		}
		return null;
	}
	
	public EntityForceField(World worldIn)
	{
		this(FPEntitys.FORCE_FIELD, worldIn);
	}

	public EntityForceField(EntityType<EntityForceField> type, World worldIn)
	{
		super(type, worldIn);
	}

	
	@Override
	protected void defineSynchedData()
	{
		this.getEntityData().define(SIZE, 1F);
		this.getEntityData().define(OWNER, Optional.<UUID>empty());
	}
	
	@Override
	public boolean isPickable() 
	{
		return !isAlive()==false;
	}
	
	public float getSize()
	{
		return this.getEntityData().get(SIZE);
	}
	
	public void setSize(float size)
	{
		this.getEntityData().set(SIZE, size);
	}
	
	public void setOwner(LivingEntity e)
	{
		if(e!=null)
		{
			UUID id = e.getUUID();
			this.getEntityData().set(OWNER, Optional.<UUID>of(id));
		}
		else
		{
			this.getEntityData().set(OWNER, Optional.<UUID>empty());
		}
	}
	
	private LivingEntity owner;
	
	public LivingEntity getOwner()
	{
		if(owner!=null)
			return owner;
		
		Optional<UUID> uu = this.getEntityData().get(OWNER);
		if(uu.isPresent())
		{
			MinecraftServer serv = level.getServer();
			if(serv!=null)
			{
				ServerWorld ws = serv.getLevel(level.dimension());
				owner = (LivingEntity) ws.getEntity(uu.get());
				return owner;
			}
			else
			{
				List<LivingEntity> entitys = level.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(getX(), getY(), getZ(), getX(), getY(), getZ()).inflate(16, 16, 16), new Predicate<LivingEntity>()
				{
					@Override
					public boolean apply(LivingEntity input)
					{
						return input.getUUID().equals(uu.get());
					}			
				});
				if(!entitys.isEmpty())
				{
					owner = entitys.get(0);
					return owner;
				}
			}
			
		}
		return null;
	}
	
	@Override
	public void onSyncedDataUpdated(DataParameter<?> key)
	{
		if(key == SIZE)
		{
			float i = getSize();
			AxisAlignedBB bb = new AxisAlignedBB(getX()-i/2, getY(), getZ()-i/2, getX()+i/2, getY()+i, getZ()+i/2);
			setBoundingBox(bb);
		}
		else if(key == OWNER)
		{
			owner = null;
		}
		super.onSyncedDataUpdated(key);
	}
	
//	@Override
//	public AxisAlignedBB getCollisionBox(Entity entityIn)
//	{
//		if(entityIn instanceof LivingEntity)
//		{
//			return entityIn.getBoundingBox();
//		}
//		return super.getCollisionBox(entityIn);
//	}
	
	@Override
	protected void readAdditionalSaveData(CompoundNBT nbt)
	{
		setSize(nbt.getFloat("field_size"));
	}

	@Override
	protected void addAdditionalSaveData(CompoundNBT nbt)
	{
		nbt.putFloat("field_size", getSize());
	}

	@Override
	public void baseTick()
	{
		super.baseTick();
		LivingEntity base = getOwner();
		if(!level.isClientSide)
		{	
			
			if(base==null || base.isAlive()==false)
			{
				this.setSize(0F);
				this.setBoundingBox(new AxisAlignedBB(0, 0, 0, 0, 0, 0));
				this.remove();
				teleportTo(0, 0, 0); //bugfix: after death the hitboxes remain server side
				return;
			}
			
			if(getSize()<0.1)
			{
				teleportTo(0, 0, 0);//bugfix: after death the hitboxes remain server side
				remove();
			}
			
			if(base.getHealth() > base.getMaxHealth()/2F)
			{
				setSize(getSize() - 0.1F);
			}	
		}	
		else
		{
			float i = getSize();
			AxisAlignedBB bb = new AxisAlignedBB(getX()-i/2, getY(), getZ()-i/2, getX()+i/2, getY()+i, getZ()+i/2);
			setBoundingBox(bb);
		}
		
	}
	
	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if(source.getDirectEntity() instanceof LivingEntity)
		{
			source.getDirectEntity().hurt(FuturepackMain.NEON_DAMAGE, 1F);
		}
		return true;
	}

	@Override
	public IPacket<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
