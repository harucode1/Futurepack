package futurepack.common.entity;

import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class EntityMovingShipBase extends Entity
{

	public EntityMovingShipBase(EntityType<?> entityTypeIn, World worldIn) 
	{
		super(entityTypeIn, worldIn);
	}
	public abstract  void setShipID(UUID shipID);
	
	public abstract void setDestination(BlockPos endCoords);
	
	public abstract void addChair(LivingEntity liv);
	
	public abstract UUID getShipID();

}
