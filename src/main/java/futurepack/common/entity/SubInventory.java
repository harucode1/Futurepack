package futurepack.common.entity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class SubInventory implements IInventory
{
	private int offset, length;
	private final IInventory inv;

	public SubInventory(IInventory inv, int offset, int length)
	{
		super();
		this.inv = inv;
		this.offset = offset;
		this.length = length;
	}
	
	public SubInventory(IInventory inv, int offset)
	{
		this(inv, offset, inv.getContainerSize() -offset);
	}
	
	public SubInventory(IInventory inv)
	{
		this(inv, 0);
	}

	@Override
	public void clearContent()
	{
		inv.clearContent();
	}

	@Override
	public int getContainerSize()
	{
		return length;
	}

	@Override
	public boolean isEmpty()
	{
		for(int i = 0; i < this.getContainerSize(); ++i) 
		{
			ItemStack itemstack = this.getItem(i);
			if (!itemstack.isEmpty()) 
			{
				return false;
			}
		}
		return true;
	}

	@Override
	public ItemStack getItem(int index)
	{
		return inv.getItem(index +offset);
	}

	@Override
	public ItemStack removeItem(int index, int count)
	{
		return inv.removeItem(index +offset, count);
	}

	@Override
	public ItemStack removeItemNoUpdate(int index)
	{
		return inv.removeItemNoUpdate(index +offset);
	}
	
	@Override
	public void setItem(int index, ItemStack stack)
	{
		inv.setItem(index +offset, stack);
	}

	@Override
	public int getMaxStackSize()
	{
		return inv.getMaxStackSize();
	}

	@Override
	public void setChanged()
	{
		inv.setChanged();
	}

	@Override
	public boolean stillValid(PlayerEntity player)
	{
		return inv.stillValid(player);
	}

	@Override
	public void startOpen(PlayerEntity player)
	{
		inv.startOpen(player);
	}

	@Override
	public void stopOpen(PlayerEntity player)
	{
		inv.stopOpen(player);
	}

	@Override
	public boolean canPlaceItem(int index, ItemStack stack)
	{
		return inv.canPlaceItem(index +offset, stack);
	}	
	
}
