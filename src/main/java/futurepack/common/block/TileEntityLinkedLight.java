package futurepack.common.block;

import java.util.function.Predicate;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.deco.TileEntityNeonLamp;
import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;

public class TileEntityLinkedLight extends TileEntity implements ITickableTileEntity
{
	private int cooldown=5;
	private BlockState state;
	private BlockPos otherPos;
	public Predicate<TileEntityLinkedLight> stillValid;
	
	
	public TileEntityLinkedLight() 
	{
		super(FPTileEntitys.LINKED_LIGHT);
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		if(otherPos!=null && state!=null)
		{
			nbt.putIntArray("otherBlockPos", new int[]{otherPos.getX(), otherPos.getY(), otherPos.getZ()});
			nbt.put("otherBlock", HelperHologram.toNBT(state));
			nbt.putBoolean("callback", stillValid != null);
		}
		
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		if(nbt.contains("otherBlockPos") && nbt.contains("otherBlock"))	
		{
			int[] ppos = nbt.getIntArray("otherBlockPos");
			otherPos = new BlockPos(ppos[0], ppos[1], ppos[2]);
			state = HelperHologram.fromNBT(nbt.getCompound("otherBlock"));
			if(nbt.getBoolean("callback"))
			{
				this.stillValid = l -> {
					TileEntity t = l.level.getBlockEntity(otherPos);
					if(t!=null)
						if(t instanceof TileEntityNeonLamp)
							l.stillValid = ((TileEntityNeonLamp)t)::isStillValid;
						else
							stillValid = null;
					else
						stillValid = null;
					return true;
				};
			}
		}
		super.load(state, nbt);
	}

	@Override
	public void tick() 
	{
		if(level.isClientSide)
			return;
		
		if(otherPos==null || state==null)
		{
			this.setRemoved();
			level.setBlockAndUpdate(worldPosition, Blocks.AIR.defaultBlockState());
		}
		else
		{
			if(cooldown>0)
				cooldown--;
			else
			{
				cooldown = 10 * 20 + level.random.nextInt(20);

				if(level.getBlockState(otherPos) != state)
				{
					this.setRemoved();
					level.setBlockAndUpdate(worldPosition, Blocks.AIR.defaultBlockState());
				}
				else
				{
					if(stillValid != null && !stillValid.test(this))
					{
						this.setRemoved();
						level.setBlockAndUpdate(worldPosition, Blocks.AIR.defaultBlockState());
					}
					else
					{
						TileEntityNeonLamp.burnZombies(level, worldPosition, 6);
					}
				}
			}
		}
	}
	
	public void setLinkedBlock(BlockPos pos, BlockState state)
	{
		this.otherPos = pos;
		this.state = state;
	}

}
