package futurepack.common.block.logistic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import futurepack.api.FacingUtil;
import futurepack.common.block.logistic.RedstoneSystem.EnumRedstone;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class BlockWireRedstone extends BlockWireBase
{
	public static final BooleanProperty REDSTONE = BooleanProperty.create("redstone");
	public static final BooleanProperty REDSTONE_DUST = BooleanProperty.create("redstone_dust");
	
	public BlockWireRedstone(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.futurepack.block.conduct.redstone"));
	}
	
	@Override
	public boolean canConnectRedstone(BlockState state, IBlockReader world, BlockPos pos, Direction side)
	{
		return true;
	}
	
	@Override
	public int getSignal(BlockState state, IBlockReader w, BlockPos pos, Direction face)
	{
		return RedstoneSystem.getWeakPower(state, w, pos, face);
	}
	
	
	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block block, BlockPos blockPos, boolean isMoving)
	{
		RedstoneSystem.neighborChanged(this, w, pos);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onPlace(BlockState state, World w, BlockPos jkl, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, jkl, oldState, isMoving);
		RedstoneSystem.onBlockAdded(state, w, jkl, oldState, isMoving);
		this.checkRedstoneRender(state, w, jkl);
	}
	
	private void checkRedstoneRender(BlockState state, World w, BlockPos pos) {
		
		ArrayList<Direction> neighbours = new ArrayList<>();
		
		for(Direction direction : FacingUtil.VALUES) {
			BlockState neighbor = w.getBlockState(pos.relative(direction));
			if(!(neighbor.getBlock() instanceof BlockWireRedstone) && !(neighbor.getBlock() instanceof BlockPipeRedstone) && neighbor.canConnectRedstone(w, pos.relative(direction), direction.getOpposite())) {
				neighbours.add(direction);
				
				BlockState newState = this.updateShape(state, direction, neighbor, w, pos, pos.relative(direction));
				if(!newState.equals(state))
					w.setBlock(pos, this.updateShape(state, direction, neighbor, w, pos, pos.relative(direction)), 2);
			}
		}
		
		BlockState downState = w.getBlockState(pos.below());
		
		boolean showDust = (neighbours.contains(Direction.NORTH) || neighbours.contains(Direction.EAST) || neighbours.contains(Direction.SOUTH) || neighbours.contains(Direction.WEST)) 
				&& !neighbours.contains(Direction.DOWN) && downState.isRedstoneConductor(w, pos.below()) && !(downState.getBlock() instanceof BlockWireBase);

		if(state.getValue(REDSTONE) != !neighbours.isEmpty() || state.getValue(REDSTONE_DUST) != showDust) {
			w.setBlock(pos, state.setValue(REDSTONE, !neighbours.isEmpty()).setValue(REDSTONE_DUST, showDust), 2);
		}
	}
	
	@Override
	public boolean canConnect(IWorld w, BlockPos pos, BlockPos xyz, BlockState facingState, @Nullable TileEntity tile, Direction face)
	{
		return RedstoneSystem.canConnectToRedstone(w, pos, xyz, facingState, face) || super.canConnect(w, pos, xyz, facingState, tile, face);
	}
	
	@Override
	public boolean canConnect(IWorld w, BlockPos pos, BlockPos xyz, BlockState state, BlockState facingState, TileEntityWireBase wire, @Nullable TileEntity tile, Direction face)
	{		
		return RedstoneSystem.canConnectToRedstone(w, pos, xyz, facingState, face) || super.canConnect(w, pos, xyz, state, facingState, wire, tile, face);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos jkl, Random rand)
	{
		super.tick(state,w, jkl,  rand);
		RedstoneSystem.tick(state, w, jkl, rand);	
		this.checkRedstoneRender(state, w, jkl);
	}
	
	@Override
	public boolean isSignalSource(BlockState state)
	{
		return true;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityWireRedstone();
	}

	@Override
	protected int getMaxNeon() 
	{
		return 500;
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		return super.getStateForPlacement(context).setValue(REDSTONE, false).setValue(RedstoneSystem.STATE, EnumRedstone.OFF).setValue(REDSTONE_DUST, false);
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(RedstoneSystem.STATE, REDSTONE, REDSTONE_DUST);
	}
}
