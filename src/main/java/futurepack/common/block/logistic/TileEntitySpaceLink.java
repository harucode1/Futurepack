package futurepack.common.block.logistic;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.spongepowered.asm.mixin.injection.InjectionPoint.RestrictTargetLevel;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.network.EventWirelessFunk;
import futurepack.common.network.NetworkManager;
import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import net.minecraft.block.BlockState;
import net.minecraft.block.DirectionalBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class TileEntitySpaceLink extends TileEntity implements ITileNetwork, INetworkUser
{
	private final Consumer<EventWirelessFunk> method; 
	private static final int FREQUENZ = -42;
	
	private static Map<TileEntityType<TileEntity>, Predicate<TileEntity>> beaconValidatorMap;
	
	static
	{
		beaconValidatorMap = new Object2ObjectAVLTreeMap<>((a,b) -> a.hashCode() - b.hashCode());
		registerBeaconValidator(TileEntityType.BEACON, b -> b.getLevels() > 0);
		registerBeaconValidator(FPTileEntitys.TELEPORTER_DUNGEON, t -> t.getBlockState().getBlock() == MiscBlocks.beam);
	}
	
	public static <T extends TileEntity> void registerBeaconValidator(TileEntityType<T> type, Predicate<T> validator)
	{
		beaconValidatorMap.put((TileEntityType<TileEntity>)type, (Predicate<TileEntity>)validator);
	}
	
	public TileEntitySpaceLink(TileEntityType<? extends TileEntitySpaceLink> type) 
	{
		super(type);
		method = this::onFunkEvent;
	}
	
	public TileEntitySpaceLink() 
	{
		this(FPTileEntitys.SPACE_LINK);
	}

	@Override
	public void onLoad()
	{
		super.onLoad();
		if(getOverworld() != null)
		{
			NetworkManager.registerWirelessTile(method, getOverworld());
		}
	}
	
	private ServerWorld getOverworld()
	{
		if(hasLevel() && getLevel().getServer() != null)
			return getLevel().getServer().getLevel(World.OVERWORLD);
		else
			return null;
	}
	
	@Override
	public void onChunkUnloaded()
	{
		if(getOverworld() != null)
		{
			NetworkManager.unregisterWirelessTile(method, getOverworld());
		}
		super.onChunkUnloaded();
	}
	
	public void onFunkEvent(EventWirelessFunk event)
	{
		if(hasBeacon())
		{
			if(!level.isClientSide)
			{	
				if(this.isRemoved())
				{
					if(getOverworld() != null)
					{
						NetworkManager.unregisterWirelessTile(method, getOverworld());
					}
					return;
				}
				if(!level.hasChunkAt(worldPosition))
				{
					if(getOverworld() != null)
					{
						NetworkManager.unregisterWirelessTile(method, getOverworld());
					}
					return;
				}
				
				if(event.canRecive(this))
				{
					if(event.frequenz == FREQUENZ)
					{
						if(event.range == -1)
						{
							NetworkManager.sendPacketUnthreaded(this, event.objPassed);
						}
					}		
				}
			}
			else if(getOverworld() != null)
			{
				NetworkManager.unregisterWirelessTile(method, getOverworld());
			}
		}
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(hasBeacon())
			NetworkManager.sendEvent(new EventWirelessFunk(getSenderPosition(), FREQUENZ, -1, pkt), getOverworld());
	}

	@Override
	public void postPacketSend(PacketBase pkt)
	{
		
	}

	@Override
	public World getSenderWorld()
	{
		return this.level;
	}
	
	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}	
	
	
	private boolean hasBeacon()
	{
		if(getLevel().isClientSide())
			return false;
		else
		{
			Direction dir = getBlockState().getValue(DirectionalBlock.FACING);
			TileEntity beacon;
			try 
			{
				beacon = getLevel().getServer().submit((Supplier<TileEntity>) () -> getLevel().getBlockEntity(this.getBlockPos().relative(dir))).get();
				if(beacon!=null)
				{
					return beaconValidatorMap.getOrDefault(beacon.getType(), t -> false).test(beacon);
				}
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			
			
			return false;
		}
	}
}
