package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;

public class TileEntityWireRedstone extends TileEntityWireBase 
{

	public TileEntityWireRedstone() 
	{
		super(FPTileEntitys.WIRE_REDSTONE);
	}

	@Override
	public int getMaxEnergy() 
	{
		return 500;
	}

}
