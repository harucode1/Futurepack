package futurepack.common.block.logistic;

import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockPipeNormal extends BlockPipeBase 
{

	protected BlockPipeNormal(Properties props) 
	{
		super(props);
	}

	@Override
	public boolean hasSpecial() 
	{
		return false;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityPipeNormal();
	}
}
