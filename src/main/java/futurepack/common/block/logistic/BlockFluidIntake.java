package futurepack.common.block.logistic;

import futurepack.common.block.BlockHoldingTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public class BlockFluidIntake extends BlockHoldingTile 
{

	public BlockFluidIntake(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
	}


	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityFluidIntake();
	}
	
}
