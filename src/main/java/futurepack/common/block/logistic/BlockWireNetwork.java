package futurepack.common.block.logistic;

import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockWireNetwork extends BlockWireBase 
{

	public BlockWireNetwork(Properties props) 
	{
		super(props);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityWireNetwork();
	}

	@Override
	protected int getMaxNeon() 
	{
		return 500;
	}
	
	@Override
	public boolean canConnect(IWorld w, BlockPos pos, BlockPos xyz, BlockState facingState, TileEntity tile, Direction face)
	{
		if(super.canConnect(w, pos, xyz, facingState, tile, face))
			return true;
	
		if(tile!=null)
		{
			if(tile instanceof ITileNetwork)
			{
				ITileNetwork p = (ITileNetwork) tile;
				if(p.isNetworkAble())
				{	
					return true;
				}	
			}
		}
		
		return false;
	}
	
}
