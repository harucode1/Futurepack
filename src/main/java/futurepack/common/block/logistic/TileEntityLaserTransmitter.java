package futurepack.common.block.logistic;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.TileEntityNetworkMaschine;
import futurepack.common.network.NetworkManager;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityLaserTransmitter extends TileEntityNetworkMaschine implements INetworkUser
{
	public TileEntityLaserTransmitter() 
	{
		super(FPTileEntitys.LASER_TRASNMITTER);
	}
	
	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		sendPacket(pkt);
	}
	
	private BlockPos lastPos;
	private long gameTime;
	
	private void sendPacket(PacketBase pkt)
	{
		BlockState state = getBlockState();
		Direction face = state.getValue(BlockRotateableTile.FACING);
		
		if(level.getGameTime() - gameTime < 20 * 60)
		{
			TileEntity tile = level.getBlockEntity(lastPos);
			if(tile instanceof TileEntityLaserTransmitter)
			{
				TileEntityLaserTransmitter laser = (TileEntityLaserTransmitter) tile;
				if(laser.getBlockState().getValue(BlockRotateableTile.FACING) == face.getOpposite())
				{
					laser.onPacketRecived(pkt);
					return;
				}
			}
		}
		BlockPos.Mutable xyz = new BlockPos.Mutable().set(worldPosition.relative(face));
		for(int i=0;i<1024;i++)
		{
			if(level.isEmptyBlock(xyz))
			{
				xyz.move(face);
				TileEntity tile = level.getBlockEntity(xyz);
				if(tile instanceof TileEntityLaserTransmitter)
				{
					lastPos = xyz.immutable();
					gameTime = level.getGameTime();
					
					TileEntityLaserTransmitter laser = (TileEntityLaserTransmitter) tile;
					if(laser.getBlockState().getValue(BlockRotateableTile.FACING) == face.getOpposite())
					{
						laser.onPacketRecived(pkt);
						return;
					}
				}
			}
			else
			{
				break;
			}
		}
	}
	
	private void onPacketRecived(PacketBase pkt)
	{
		NetworkManager.sendPacketUnthreaded(this, pkt);
	}

	@Override
	public void postPacketSend(PacketBase pkt){	}

	@Override
	public World getSenderWorld()
	{
		return this.level;
	}
	
	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}
}
