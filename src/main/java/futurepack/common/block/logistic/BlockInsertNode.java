package futurepack.common.block.logistic;

import java.util.HashMap;
import java.util.List;

import futurepack.common.block.logistic.TileEntityPipeBase.ItemPath;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.DirectionalBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class BlockInsertNode extends BlockPipeBase
{
	protected static final VoxelShape AABB_UP    = VoxelShapes.or(Block.box(2, 0, 2, 14, 4, 14), BlockPipeBase.BOX_BASE);
	protected static final VoxelShape AABB_DOWN  = VoxelShapes.or(Block.box(2, 12, 2, 14, 16, 14), BlockPipeBase.BOX_BASE);
    protected static final VoxelShape AABB_NORTH = VoxelShapes.or(Block.box(2, 2, 12D, 14, 14, 16D), BlockPipeBase.BOX_BASE);
    protected static final VoxelShape AABB_SOUTH = VoxelShapes.or(Block.box(2, 2, 0D, 14, 14, 4D), BlockPipeBase.BOX_BASE);
    protected static final VoxelShape AABB_WEST  = VoxelShapes.or(Block.box(12, 2, 2, 16D, 14, 14), BlockPipeBase.BOX_BASE);
    protected static final VoxelShape AABB_EAST  = VoxelShapes.or(Block.box(0, 2, 2, 4, 14, 14), BlockPipeBase.BOX_BASE);
	
    public static final DirectionProperty FACING = DirectionalBlock.FACING;
    
	public BlockInsertNode(Block.Properties props)
	{
		super(props, true);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
	}
	
//	@Override
//	public boolean isOpaqueCube(IBlockState state) 
//	{
//		return false;
//	}
	
//	@Override
//	public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos)
//	{
//		return false;
//	}

	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.futurepack.items.insertnode.joke"));//"It is an Item Retriever, the name is a german joke."
	}
	
//	@Override
//	public Direction[] getValidRotations(BlockState state, IBlockReader world, BlockPos pos)
//	{
//		return FacingUtil.VALUES;
//	}
	
	@Override
	public BlockState rotate(BlockState state, IWorld world, BlockPos pos, Rotation direction)
	{
		return rotate(state, direction);
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn)
	{
		return state.setValue(FACING, mirrorIn.mirror(state.getValue(FACING)));
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		Direction face = context.getClickedFace();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}
	
	protected static HashMap<Integer, VoxelShape> voxelShadeInsertNodeCache = new HashMap<Integer, VoxelShape>();
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader w, BlockPos pos, ISelectionContext sel)
	{
		if(Boolean.TRUE.equals(state.getValue(HOLOGRAM))) {
			return super.getShape(state, w, pos, sel);
		}
		
		Direction enumfacing = state.getValue(FACING);
	      
		Integer cacheKey = getVoxelShadeCacheKey(state) | (enumfacing.get3DDataValue() << 12);
		
		VoxelShape shape = voxelShadeInsertNodeCache.get(cacheKey);
		
		if(shape != null)
		{
			return shape;
		}
			
		shape = super.getShape(state, w, pos, sel);
		
        switch (enumfacing)
        {
            case EAST:
            	shape = VoxelShapes.or(AABB_EAST, shape); break;
            case WEST:
            	shape = VoxelShapes.or(AABB_WEST, shape); break;
            case SOUTH:
            	shape = VoxelShapes.or(AABB_SOUTH, shape); break;
            case NORTH:
            	shape = VoxelShapes.or(AABB_NORTH, shape); break;
            case UP:
            	shape = VoxelShapes.or(AABB_UP, shape); break;
            case DOWN:
            	shape = VoxelShapes.or(AABB_DOWN, shape); break;
            default:
            	return shape;
        }
        
        voxelShadeInsertNodeCache.put(cacheKey, shape);

        return shape;
	}

	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(FACING);
	}
	

	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(pl.getItemInHand(Hand.MAIN_HAND)!=null && pl.getItemInHand(Hand.MAIN_HAND).getItem()==ToolItems.scrench)
		{
			return super.use(state, w, pos, pl, hand, hit);
		}
		else if(!w.isClientSide)
		{
			FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
			return ActionResultType.SUCCESS;
		}
		return ActionResultType.PASS;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityInsertNode();
	}

	@Override
	public boolean hasSpecial() 
	{
		return false;
	}

}
