package futurepack.common.block.logistic;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.DirectionProperty;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockSyncronizer extends BlockRotateableTile
{
	public static final DirectionProperty FACING = BlockRotateableTile.FACING;

	public BlockSyncronizer(Block.Properties props)
	{
		super(props);	
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos , PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.SYNCRONIZER.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntitySyncronizer();
	}	


}
