package futurepack.common.block.logistic;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.FacingUtil;
import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;

public class TileEntityFluidTank extends TileEntity implements ITickableTileEntity
{
	private FluidTank tank;
	private int ticksdown=0;
	
	private int lastFail = 0;
	
	private int[] fluidInput;
	
	public int pixelsFilled;//0 to 32 
	private LazyOptional<IFluidHandler>[] fluidOpt;
	
	private boolean wasEmpty = true;
	
	@SuppressWarnings("unchecked")
	public TileEntityFluidTank(TileEntityType<? extends TileEntityFluidTank> type, int capacity)
	{
		super(type);
		tank = new FluidTankSided(capacity);
//		tank.setTileEntity(this);
		fluidInput = new int[FacingUtil.VALUES.length];
		fluidOpt = new LazyOptional[6];
		
	}
	
	public TileEntityFluidTank() 
	{
		this(8000);
	}
	
	public TileEntityFluidTank(int capacity) 
	{
		this(FPTileEntitys.FLUID_TANK, capacity);
	}
	
	@Override
	public void tick()
	{
		ticksdown++;
		if(ticksdown>4)
		{
			lastTank = new FluidTankInfo(tank, 0);
			
			for(int i=0;i<fluidInput.length;i++)
			{
				if(fluidInput[i]>0)
					fluidInput[i]-=4;
				
				if(fluidInput[i]<0)
					fluidInput[i]=0;
			}
			
			if(this.tank.getFluidAmount()>0 && --lastFail<=0)
			{
				LazyOptional<IFluidHandler> optdown = null;
				if(tank.getFluid().getFluid().getAttributes().isGaseous(tank.getFluid()))
				{
					optdown = FluidUtil.getFluidHandler(level, worldPosition.above(), Direction.DOWN);
				}
				else
				{
					optdown = FluidUtil.getFluidHandler(level, worldPosition.below(), Direction.UP);
				}
				
				optdown.ifPresent(down -> 
				{
					int now = tank.getFluidAmount();
					FluidStack stack = FluidUtil.tryFluidTransfer(down, tank, 1000, true);
					if(stack==null || stack.isEmpty())
					{
						lastFail = 10;
					}
					else if(now == tank.getFluidAmount() && stack.getAmount()>0)
					{
						lastFail = 10;
					}
				});
			}
			if(this.tank.getFluidAmount()>0 && !level.isClientSide)
			{
				List<TileEntityFluidTank> list = new ArrayList<TileEntityFluidTank>();
				for(Direction face : FacingUtil.HORIZONTAL)
				{
					TileEntity tile = level.getBlockEntity(worldPosition.relative(face));
					if(tile!=null)
					{
						if(tile instanceof TileEntityFluidTank)
						{
							TileEntityFluidTank tank = (TileEntityFluidTank)tile;
							FluidStack fs1 = tank.tank.getFluid();
							FluidStack fs2 = this.tank.getFluid();
							
							if(tank.tank.getCapacity() == this.lastTank.getCapacity())
							{
								if(fs2.equals(fs1) || fs1.isEmpty())
								{
									list.add(tank);
								}
							}
						}
					}
				}
				
				if(!list.isEmpty() && this.tank.getFluid()!=null)
				{
					int total = this.tank.getFluidAmount();
					for(TileEntityFluidTank tank : list)
					{
						total += tank.tank.getFluidAmount();
					}
					float each = total / (1F + list.size());
					
					if(this.tank.getFluidAmount() > (int)each)
					{
						int fill = (int)each;
						if(fill<=5 && this.tank.getFluid()!=null)
						{		
							int next = level.random.nextInt(list.size());
							TileEntityFluidTank tank = list.get(next);
							
							if(tank.tank.getFluidAmount()>0)
								tank.tank.getFluid().grow(1);
							else
								tank.tank.setFluid(new FluidStack(this.tank.getFluid(), 1));
							
							tank.setChanged();
							
							this.tank.drain(1, FluidAction.EXECUTE);
							setChanged();
						}
						else
						{
							this.tank.getFluid().setAmount(total -  list.size()*fill);
							for(TileEntityFluidTank tank : list)
							{
								if(tank.tank.getFluidAmount()>0)
									tank.tank.getFluid().setAmount(fill);
								else
									tank.tank.setFluid(new FluidStack(this.tank.getFluid(), fill));
								tank.setChanged();
							}
							setChanged();
						}
						
					}
					
				}
			}
			
			
			ticksdown=0;
		}	
		
		if(wasEmpty != tank.isEmpty())
		{
			wasEmpty = tank.isEmpty();
			level.getLightEngine().checkBlock(worldPosition);
		}
	}
	
	@Override
	public void setChanged()
	{
		int now = tank.getFluidAmount() / 250 +1;
		if(tank.getFluidAmount()==0 && now>0)
		{
			now = 0;
		}
		
		if(now!=pixelsFilled)
		{
			pixelsFilled = now;
		
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
		}
		super.setChanged();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing == null)
			return LazyOptional.empty();
			
		if(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY==capability)
		{
			if(fluidOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) fluidOpt[facing.get3DDataValue()];
			}
			else
			{
				fluidOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new FluidHandlerTank(tank, facing));
				fluidOpt[facing.get3DDataValue()].addListener(p -> fluidOpt[facing.get3DDataValue()] = null);
				return (LazyOptional<T>) fluidOpt[facing.get3DDataValue()];
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidOpt);
		super.setRemoved();
	}
	
	public int getLastFailTime()
	{
		return this.lastFail;
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		CompoundNBT tank = new CompoundNBT();
		this.tank.writeToNBT(tank);
		tank.putInt("capacity", this.tank.getCapacity());
		
		nbt.put("tank", tank);
		nbt.putInt("fillDir", TileEntityScannerBlock.booleanToInt(new boolean[]{fluidInput[0]>0, fluidInput[1]>0, fluidInput[2]>0, fluidInput[3]>0, fluidInput[4]>0, fluidInput[5]>0}));
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		CompoundNBT tank = nbt.getCompound("tank");
		this.tank.readFromNBT(tank);
		if(tank.contains("capacity"))
		{
			this.tank.setCapacity(tank.getInt("capacity"));
		}
		
		boolean[] bools = TileEntityScannerBlock.intToBool(new boolean[6], nbt.getInt("fillDir"));
		for(int i=0;i<bools.length;i++)
		{
			fluidInput[i] = bools[i] ? 10 : Math.max(0, fluidInput[i]);
		}
		super.load(state, nbt);
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		load(getBlockState(), pkt.getTag());
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pack;
	}
	
	public void addFilling(Direction side)
	{
		if(!level.isClientSide)
		{	
			if(side!=null)
			{
				fluidInput[side.ordinal()] = 10;
				setChanged();
			}
		}
	}
	
	public boolean isInputAktiv(Direction face)
	{
		return fluidInput[face.ordinal()] > 0;
	}
	
	private class FluidTankSided extends FluidTank
	{
		public FluidTankSided(int capacity)
		{
			super(capacity);
		}
		
		@Override
		protected void onContentsChanged()
		{
			setChanged();
		}
		
		@Override
		public int fill(FluidStack resource, FluidAction doFill)
		{
			if(getFluidAmount() >= getCapacity())
			{
				if(resource!=null && resource.getFluid()!=null)
				{
					boolean gas = resource.getFluid().getAttributes().isGaseous(resource);
					TileEntity tile = null;
					if(gas)
					{
						tile = level.getBlockEntity(worldPosition.below());
					}
					else
					{
						tile = level.getBlockEntity(worldPosition.above());
					}
					if(tile!=null && tile instanceof TileEntityFluidTank)
					{
						Direction side = gas? Direction.UP : Direction.DOWN;
						tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, side).ifPresent(h -> {
							h.fill(resource, doFill);
						});
					}
				}
			}
			return super.fill(resource, doFill);
		}
	}
	
	private class FluidHandlerTank implements IFluidHandler
	{
		final FluidTank delegate;
		final Direction side;
		
		public FluidHandlerTank(FluidTank delegate, Direction side) 
		{
			super();
			this.delegate = delegate;
			this.side = side;
		}
		
		@Override
		public int getTanks() 
		{
			return delegate.getTanks();
		}
		@Override
		public FluidStack getFluidInTank(int tank) 
		{
			return delegate.getFluidInTank(tank);
		}
		@Override
		public int getTankCapacity(int tank) 
		{
			return delegate.getTankCapacity(tank);
		}
		@Override
		public boolean isFluidValid(int tank, FluidStack stack) 
		{
			return delegate.isFluidValid(tank, stack);
		}
		@Override
		public int fill(FluidStack resource, FluidAction action) 
		{
			int lvlA = delegate.getFluidAmount();
			int f = delegate.fill(resource, action);
			if(f>0 && delegate.getFluidAmount() > lvlA)//can not use only f because tank can fill to connected tanks
			{
				addFilling(side);
			}
			return f;
		}
		
		@Override
		public FluidStack drain(FluidStack resource, FluidAction action) 
		{
			return delegate.drain(resource, action);
		}
		@Override
		public FluidStack drain(int maxDrain, FluidAction action) 
		{
			return delegate.drain(maxDrain, action);
		}
	}
	
	
	private FluidTankInfo lastTank;

	public FluidTankInfo getTankInfo()
	{
		return new FluidTankInfo(tank, 0);
	}
	
	public FluidTankInfo getLastTankInfo()
	{
		if(lastTank==null)
		{
			return getTankInfo();
		}
			
		if(lastTank.getFluidStack().isEmpty() && !tank.getFluid().isEmpty())
		{
			lastTank = getTankInfo();
		}
//		else if(!lastTank.getFluidStack().isEmpty() && lastTank.getFluidStack().getAmount()==0)
//		{
//			lastTank = new FluidTankInfo(new FluidStack(lastTank.fluid, 1), 1);
//		}
		return lastTank;
	}

//	@Override
//	public boolean shouldRenderInPass(int pass)
//	{
//		return pass==1;
//	}
}
