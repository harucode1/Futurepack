package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.common.item.tools.ToolItems;
import futurepack.depend.api.helper.HelperChunks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;

public class BlockFluidTube extends Block
{
	private static EnumProperty<EnumSideState> upS = EnumProperty.create("s_up", EnumSideState.class),
	downS = EnumProperty.create("s_down", EnumSideState.class),
	northS = EnumProperty.create("s_north", EnumSideState.class),
	eastS = EnumProperty.create("s_east", EnumSideState.class),
	southS = EnumProperty.create("s_south", EnumSideState.class),
	westS = EnumProperty.create("s_west", EnumSideState.class);
//	private static PropertyEnum<EnumLogisticIO> upC = PropertyEnum.create("c_up", EnumLogisticIO.class),
//	downC = PropertyEnum.create("c_down", EnumLogisticIO.class),
//	northC = PropertyEnum.create("c_north", EnumLogisticIO.class),
//	eastC = PropertyEnum.create("c_east", EnumLogisticIO.class),
//	southC = PropertyEnum.create("c_south", EnumLogisticIO.class),
//	westC = PropertyEnum.create("c_west", EnumLogisticIO.class);
	
	public static VoxelShape shape = Block.box(4, 4, 4, 12, 12, 12);
	
	public BlockFluidTube(Block.Properties props)
	{
		super(props.noOcclusion());
		
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
		registerDefaultState(this.stateDefinition.any().setValue(upS, EnumSideState.NOT).setValue(downS, EnumSideState.NOT).setValue(northS, EnumSideState.NOT).setValue(eastS, EnumSideState.NOT).setValue(southS, EnumSideState.NOT).setValue(westS, EnumSideState.NOT));
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
    {
        return BlockRenderType.MODEL;
    }
	
	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, IWorld world, BlockPos currentPos, BlockPos facingPos) 
	{
		state = super.updateShape(state, facing, facingState, world, currentPos, facingPos);
		EnumProperty<EnumSideState> prop = null;
		switch (facing)
		{
		case UP:    prop = upS; break;
		case DOWN:  prop = downS; break;
		case NORTH: prop = northS; break;
		case EAST:  prop = eastS; break;
		case SOUTH: prop = southS; break;
		case WEST:  prop = westS; break;
		default: break;
		}
		state = state.setValue(prop, getSideState(world, currentPos, facing));
		return state;
	}
	
	@Override
	public void onPlace(BlockState state, World w, BlockPos pos, BlockState oldState, boolean isMoving) 
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(oldState.getBlock()!=this)
		{
			state = state.setValue(upS, getSideState(w, pos, Direction.UP));
			state = state.setValue(downS, getSideState(w, pos, Direction.DOWN));
			state = state.setValue(northS, getSideState(w, pos, Direction.NORTH));
			state = state.setValue(eastS, getSideState(w, pos, Direction.EAST));
			state = state.setValue(southS, getSideState(w, pos, Direction.SOUTH));
			state = state.setValue(westS, getSideState(w, pos, Direction.WEST));
			
			w.setBlockAndUpdate(pos, state);
		}
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		return shape;
	}
	
	private EnumSideState getSideState(IWorldReader w, BlockPos pos, Direction face)
	{
		BlockPos tank = pos.relative(face);
		TileEntityFluidTube own = (TileEntityFluidTube) w.getBlockEntity(pos);
		if(own.getModeForFace(face, EnumLogisticType.FLUIDS)==EnumLogisticIO.NONE)
		{
			return EnumSideState.NOT;
		}
		TileEntity tile = w.getBlockEntity(tank);
		
		if(tile!=null)
		{
			if(tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face.getOpposite()).isPresent())
			{
				if(tile instanceof TileEntityFluidTube)
				{
					if(own.getModeForFace(face, EnumLogisticType.FLUIDS) != EnumLogisticIO.INOUT)
					{
						return EnumSideState.MACHINE;
					}
					else
					{
						return EnumSideState.NORMAL;
					}
				}
				else
				{
					return EnumSideState.MACHINE;
				}
			}
		}
		return EnumSideState.NOT;
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit) 
	{
		ItemStack held  = pl.getItemInHand(hand);
		Direction facing = hit.getDirection();
		if(!held.isEmpty())
		{
			if(held.getItem() == ToolItems.scrench)
			{
				TileEntityFluidTube tube = (TileEntityFluidTube) w.getBlockEntity(pos);
				TileEntity other = w.getBlockEntity(pos.relative(facing));
				if(tube.getModeForFace(facing, EnumLogisticType.FLUIDS)!=EnumLogisticIO.NONE)
				{
					tube.setModeForFace(facing, EnumLogisticIO.NONE, EnumLogisticType.FLUIDS);
					if(other instanceof TileEntityFluidTube)
					{
						if(((TileEntityFluidTube) other).setModeForFace(facing.getOpposite(), EnumLogisticIO.NONE, EnumLogisticType.FLUIDS))
						{
							other.setChanged();
							HelperChunks.renderUpdate(w, pos);
						}
					}
				}
				else
				{
					tube.setModeForFace(facing, EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS);
					if(other instanceof TileEntityFluidTube)
					{
						if(((TileEntityFluidTube) other).setModeForFace(facing.getOpposite(), EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS))
						{
							other.setChanged();
							HelperChunks.renderUpdate(w, pos);
						}
					}
				}
				w.sendBlockUpdated(pos, Blocks.AIR.defaultBlockState(), w.getBlockState(pos), 2);
				return ActionResultType.SUCCESS;
			}
		}
		return super.use(state, w, pos, pl, hand, hit);
	}
	
//	private EnumLogisticIO getConnectionState(IWorldReader w, BlockPos pos, EnumFacing face)
//	{
//		TileEntityFluidTube tube = (TileEntityFluidTube) w.getTileEntity(pos);
//		if(tube!=null)
//		{
//			return tube.getModeForFace(face, EnumLogisticType.FLUIDS);
//		}
//		return EnumLogisticIO.NONE;
//	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder) 
	{
		super.createBlockStateDefinition(builder);
		builder.add(upS, downS, northS, eastS, southS, westS);
	}
	
	public static enum EnumSideState implements IStringSerializable
	{
		NOT, //not connected to anything
		NORMAL, //normal conection to another tube
		MACHINE; //connection to a fluid conainer (or special between tubes) 

		@Override
		public String getSerializedName()
		{
			return name().toLowerCase();
		}
	}

	@Override
	public boolean hasTileEntity(BlockState state) 
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityFluidTube();
	}
}
