package futurepack.common.block.logistic;

import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockWireNormal extends BlockWireBase 
{

	public BlockWireNormal(Properties props) 
	{
		super(props);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityWireNormal();
	}

	@Override
	protected int getMaxNeon() 
	{
		return 500;
	}

}
