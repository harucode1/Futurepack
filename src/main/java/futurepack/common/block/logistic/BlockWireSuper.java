package futurepack.common.block.logistic;

import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockWireSuper extends BlockWireBase 
{

	public BlockWireSuper(Properties props) 
	{
		super(props);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityWireSuper();
	}

	@Override
	protected int getMaxNeon() 
	{
		return 1000;
	}

}
