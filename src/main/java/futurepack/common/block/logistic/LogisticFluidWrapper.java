package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticType;
import futurepack.api.capabilities.ILogisticInterface;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;


import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;

public class LogisticFluidWrapper implements IFluidHandler
{
	public final ILogisticInterface log;
	public final IFluidHandler base;
	
	public LogisticFluidWrapper(ILogisticInterface log, IFluidHandler base)
	{
		super();
		this.log = log;
		this.base = base;
	}

	@Override
	public int getTanks() 
	{
		return base.getTanks();
	}

	@Override
	public FluidStack getFluidInTank(int tank) 
	{
		return base.getFluidInTank(tank);
	}

	@Override
	public int getTankCapacity(int tank) 
	{
		return base.getTankCapacity(tank);
	}

	@Override
	public boolean isFluidValid(int tank, FluidStack stack) 
	{
		return base.isFluidValid(tank, stack);
	}

	@Override
	public int fill(FluidStack resource, FluidAction action) 
	{
		if(log.getMode(EnumLogisticType.FLUIDS).canInsert())
		{
			return base.fill(resource, action);
		}
		return 0;
	}

	@Override
	public FluidStack drain(FluidStack resource, FluidAction action) 
	{
		if(log.getMode(EnumLogisticType.FLUIDS).canExtract())
		{
			return base.drain(resource, action);
		}
		return FluidStack.EMPTY;
	}

	@Override
	public FluidStack drain(int maxDrain, FluidAction action) 
	{
		if(log.getMode(EnumLogisticType.FLUIDS).canExtract())
		{
			return base.drain(maxDrain, action);
		}
		return FluidStack.EMPTY;
	}

}
