package futurepack.common.block.logistic;

import java.util.List;

import javax.annotation.Nullable;

import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.ISupportStorage;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.util.LazyOptional;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockPipeSupport extends BlockPipeBase 
{
	public static final BooleanProperty POWERED = BlockStateProperties.POWERED;
	
	protected BlockPipeSupport(Properties props) 
	{
		super(props);
	}

	@Override
	public boolean hasSpecial() 
	{
		return true;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityPipeSupport();
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(POWERED);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.futurepack.block.conduct.support"));
	}
	
	@Override
	protected EnumSide getAdditionalConnections(TileEntityPipeBase pipe, @Nullable TileEntity otherTile, Direction face) {
		
		if(otherTile != null)
		{
			LazyOptional<ISupportStorage> supOpt = otherTile.getCapability(CapabilitySupport.cap_SUPPORT, face.getOpposite());
			if(supOpt.isPresent())
			{
				ISupportStorage store = supOpt.orElseThrow(NullPointerException::new);
				if(store.getType()!=EnumEnergyMode.NONE)
				{	
					return EnumSide.CABLE;//CABLE because when this is called there is not an inventory to connect to just support
				}	
			}
		}
		
		return EnumSide.OFF;
	}
}
