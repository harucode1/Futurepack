package futurepack.common.block.logistic;

import java.util.List;

import futurepack.common.block.misc.BlockDoorMarker;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;

public class BlockSpaceLink extends BlockDoorMarker implements IWaterLoggable
{
	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
	
	public BlockSpaceLink(AbstractBlock.Properties props) 
	{
		super(props);
		registerDefaultState(this.stateDefinition.any().setValue(WATERLOGGED, false));
	}
	
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(WATERLOGGED);
	}
	
	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}
	
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntitySpaceLink();
	}
	
	@Override
	public void appendHoverText(ItemStack p_190948_1_, IBlockReader p_190948_2_, List<ITextComponent> l, ITooltipFlag p_190948_4_) 
	{
		l.add(new TranslationTextComponent(this.getDescriptionId() + ".tooltip"));
		super.appendHoverText(p_190948_1_, p_190948_2_, l, p_190948_4_);
	}
}
