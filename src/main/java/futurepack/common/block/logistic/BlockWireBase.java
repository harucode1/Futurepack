package futurepack.common.block.logistic;

import java.util.List;

import javax.annotation.Nullable;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.common.block.BlockHologram;
import futurepack.common.item.tools.ItemLogisticEditor;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.IBucketPickupHandler;
import net.minecraft.block.ILiquidContainer;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;

public abstract class BlockWireBase extends BlockWaterLoggedHologram
{
	public static final BooleanProperty upB = BlockStateProperties.UP,
			downB = BlockStateProperties.DOWN,
			northB = BlockStateProperties.NORTH,
			eastB = BlockStateProperties.EAST,
			southB = BlockStateProperties.SOUTH,
			westB = BlockStateProperties.WEST;
	
	 
	
	public static BooleanProperty hologram = BooleanProperty.create("hologram");
	
	
	private final VoxelShape box;
	
	public BlockWireBase(Block.Properties props, boolean hasNBTCustomDrops) 
	{
		super(props, hasNBTCustomDrops);//hasNBTCustomDrops default false
		box = VoxelShapes.box(0.25F, 0.25F, 0.25F, 0.75F, 0.75F, 0.75F);
		registerDefaultState(this.stateDefinition.any().setValue(hologram, false).setValue(upB, false).setValue(downB, false).setValue(northB, false).setValue(southB, false).setValue(eastB, false).setValue(westB, false).setValue(WATERLOGGED, false));
	}
	
	public BlockWireBase(Block.Properties props) 
	{
		this(props, false);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader w, BlockPos pos, ISelectionContext sel)
	{
		if(Boolean.TRUE.equals(state.getValue(hologram))) {
			return super.getShape(state, w, pos, sel);
		}
		
		return box;
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
    {
		if(state.getValue(hologram))
			return BlockRenderType.INVISIBLE;
		
        return BlockRenderType.MODEL;
    }
	
	@Override
	public abstract TileEntity createTileEntity(BlockState state, IBlockReader world);
	
	protected abstract int getMaxNeon();
	
	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.futurepack.block.conduct.neon", getMaxNeon()));
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(upB, downB, eastB, westB, southB, northB, hologram);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		BlockState state = super.getStateForPlacement(context);
		BlockPos pos = context.getClickedPos();
		World w = context.getLevel();
		for(Direction face : FacingUtil.VALUES)
		{
			BlockPos xyz = pos.relative(face);
			BlockState facingState = w.getBlockState(xyz);
			TileEntity tile = w.getBlockEntity(xyz);
			state = withConnection(state, face, canConnect(w, pos, xyz, facingState, tile, face));
		}
		return state;
	}
	
	/**
	 * This is called pre placed
	 * 
	 * @param w	the World
	 * @param pos the wire position 
	 * @param xyz the position to connect to
	 * @param facingState the state of the block to connect to
	 * @param tile the TileEntity of the block to connect to, can be null
	 * @param face the direct to connect
	 * @return
	 */
	public boolean canConnect(IWorld w, BlockPos pos, BlockPos xyz, BlockState facingState, @Nullable TileEntity tile, Direction face)
	{
		if(tile!=null)
		{
			LazyOptional<INeonEnergyStorage> capT = tile.getCapability(CapabilityNeon.cap_NEON, face.getOpposite());
			if(capT.isPresent())
			{	
				INeonEnergyStorage store = capT.orElseThrow(NullPointerException::new);
				if(store.getType()!=EnumEnergyMode.NONE)
				{	
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Called when the wire is already placed
	 * 
	 * @param w	the World
	 * @param pos the wire position 
	 * @param xyz the position to connect to
	 * @param state the wire state
	 * @param facingState the state of the block to connect to
	 * @param wire the Wire TileEntity
	 * @param tile the TileEntity of the block to connect to, can be null
	 * @param face the direct to connect
	 * @return
	 */
	public boolean canConnect(IWorld w, BlockPos pos, BlockPos xyz, BlockState state, BlockState facingState, TileEntityWireBase wire, @Nullable TileEntity tile, Direction face)
	{
		if(canConnect(w, pos, xyz, facingState, tile, face))
		{
			LazyOptional<INeonEnergyStorage> capW = wire.getCapability(CapabilityNeon.cap_NEON, face);
			if(capW.isPresent())
			{
				return true;
			}
		}
		return false;
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction face, BlockState facingState, IWorld w, BlockPos pos, BlockPos xyz)
	{
		TileEntity tile = w.getBlockEntity(xyz);
		TileEntity tile2 = w.getBlockEntity(pos);
		try
		{
			TileEntityWireBase wire = (TileEntityWireBase) tile2;
			
			boolean connect = canConnect(w, pos, xyz, state, facingState, wire, tile, face);
			
			state = withConnection(state, face, connect);
	
			return state;
		}
		catch(ClassCastException e)
		{
			e.printStackTrace();
			return this.defaultBlockState();
		}
	}
	
	protected static BlockState withConnection(BlockState state, Direction face, boolean connect)
	{
		switch (face)
		{
		case UP:
			return state.setValue(upB, connect);
		case DOWN:
			return state.setValue(downB, connect);
		case NORTH:
			return state.setValue(northB, connect);
		case SOUTH:
			return state.setValue(southB, connect);
		case WEST:
			return state.setValue(westB, connect);
		case EAST:
			return state.setValue(eastB, connect);
		default:
			return state;
		}
	}
	
	protected boolean isConnected(BlockState state, Direction face)
	{
		switch (face)
		{
		case UP:
			return state.getValue(upB);
		case DOWN:
			return state.getValue(downB);
		case NORTH:
			return state.getValue(northB);
		case SOUTH:
			return state.getValue(southB);
		case WEST:
			return state.getValue(westB);
		case EAST:
			return state.getValue(eastB);
		default:
			return false;
		}
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(pl.getItemInHand(Hand.MAIN_HAND)!=null && pl.getItemInHand(Hand.MAIN_HAND).getItem()==ToolItems.scrench)
		{
			if(!w.isClientSide)
			{
				TileEntityWireBase wire = (TileEntityWireBase) w.getBlockEntity(pos);
				wire.getCapability(CapabilityLogistic.cap_LOGISTIC, hit.getDirection()).ifPresent(inter -> 
				{
					if(inter.getMode(EnumLogisticType.ENERGIE) != EnumLogisticIO.NONE)
					{
						inter.setMode(EnumLogisticIO.NONE, EnumLogisticType.ENERGIE);
					}
					else
					{
						inter.setMode(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
					}
					wire.setChanged();
					w.sendBlockUpdated(pos, Blocks.AIR.defaultBlockState(), w.getBlockState(pos), 2);
				});
			}
			else
			{
				ItemLogisticEditor.sheduledRenderUpdate(pos, w);
			}
			return ActionResultType.SUCCESS;
		}
		return super.use(state, w, pos, pl, hand, hit);
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot) {	
		boolean north = false;
		boolean east  = false;
		boolean south = false;
		boolean west  = false;
		
		if(rot != Rotation.NONE) {
			if(state.getValue(BlockWireBase.northB)) {
				switch(rot) {
					case CLOCKWISE_90: east = true; break;
					case CLOCKWISE_180: south = true; break;
					case COUNTERCLOCKWISE_90: west = true; break;
					default: break;
				}
			}
			if(state.getValue(BlockWireBase.eastB)) {
				switch(rot) {
					case CLOCKWISE_90: south = true; break;
					case CLOCKWISE_180: west = true; break;
					case COUNTERCLOCKWISE_90: north = true; break;
					default: break;
				}
			}
			if(state.getValue(BlockWireBase.southB)) {
				switch(rot) {
					case CLOCKWISE_90: west = true; break;
					case CLOCKWISE_180: north = true; break;
					case COUNTERCLOCKWISE_90: east = true; break;
					default: break;
				}
			}
			if(state.getValue(BlockWireBase.westB)) {
				switch(rot) {
					case CLOCKWISE_90: north = true; break;
					case CLOCKWISE_180: east = true; break;
					case COUNTERCLOCKWISE_90: south = true; break;
					default: break;
				}
			}
			
			return this.defaultBlockState().setValue(northB, north).setValue(eastB, east).setValue(southB, south).setValue(westB, west).setValue(upB, state.getValue(upB)).setValue(downB, state.getValue(downB));
		}
		
		return state;
	}	
	
	
	
}
