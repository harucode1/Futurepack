package futurepack.common.block.logistic;

import java.util.List;

import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.ISupportStorage;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraftforge.common.util.LazyOptional;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockWireSupport extends BlockWireBase 
{
	public BlockWireSupport(Properties props) 
	{
		super(props);
	}

	public static final BooleanProperty POWERED = BlockStateProperties.POWERED;

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityWireSupport();
	}

	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.futurepack.block.conduct.support")); //"Transports: "+TextFormatting.YELLOW + "Support");
	}
	
	@Override
	public boolean canConnect(IWorld w, BlockPos pos, BlockPos xyz, BlockState state, BlockState facingState, TileEntityWireBase wire, TileEntity tile, Direction face)
	{
		if(super.canConnect(w, pos, xyz, state, facingState, wire, tile, face))
			return true;
		
		if(canConnect(w, pos, xyz, facingState, tile, face))
		{
			LazyOptional<ISupportStorage> wireC = wire.getCapability(CapabilitySupport.cap_SUPPORT, face);
			if(wireC.isPresent())
			{
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public boolean canConnect(IWorld w, BlockPos pos, BlockPos xyz, BlockState facingState, TileEntity tile, Direction face)
	{
		if(super.canConnect(w, pos, xyz, facingState, tile, face))
			return true;
		
		if(tile!=null)
		{
			LazyOptional<ISupportStorage> supO = tile.getCapability(CapabilitySupport.cap_SUPPORT, face.getOpposite());
			if(supO.isPresent())
			{
				ISupportStorage store = supO.orElseThrow(NullPointerException::new);
				if(store.getType()!=EnumEnergyMode.NONE)
				{	
					return true;
				}
			}
		}
		return false;
	}

	@Override
	protected int getMaxNeon() 
	{
		return 500;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(POWERED);
	}
}
