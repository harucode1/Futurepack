package futurepack.common.block.logistic;

import java.util.ArrayList;
import java.util.Collection;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.LogisticStorage;
import futurepack.api.ParentCoords;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;

public class TileEntityFluidTube extends TileEntity
{
	private LogisticStorage logistic;
	
	private LazyOptional<IFluidHandler>[] fluidOpt;
	private LazyOptional<ILogisticInterface>[] logOpt;
	
	protected final float BASE_AMOUNT;
	
	protected IBlockSelector selector = new IBlockSelector()
	{
		@Override
		public boolean isValidBlock(World w, BlockPos pos, Material m, boolean dia, ParentCoords p)
		{
			if(dia)
				return false;	
			
			TileEntity t = w.getBlockEntity(pos);
			if(t==null)
				return false;
			
			Direction face = FacingUtil.getSide(pos, p);
			
			if(p!=null)
			{
				TileEntityFluidTube parent = (TileEntityFluidTube) w.getBlockEntity(p);
				Direction pFace = face.getOpposite();
				
				if(! parent.logistic.canExtract(pFace, EnumLogisticType.FLUIDS))//stops if parent pipe cant output there
				{
					return false;
				}
			}
				
			if(t instanceof TileEntityFluidTube)
			{				
				return ((TileEntityFluidTube) t).logistic.canInsert(face, EnumLogisticType.FLUIDS); //stops if this pipe cant insert here
			}
			
			return t.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face).isPresent();
			
		}
		
		@Override
		public boolean canContinue(World w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			TileEntity t = w.getBlockEntity(pos);
			return (t.getType() == TileEntityFluidTube.this.getType()) && (t.getBlockState().getBlock() == TileEntityFluidTube.this.getBlockState().getBlock());
		}
	};
	protected IBlockValidator sorter = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(World w, ParentCoords pos)
		{
			return !(w.getBlockEntity(pos) instanceof TileEntityFluidTube);//only end nodes
		}
	};
	
	@SuppressWarnings("unchecked")
	public TileEntityFluidTube(TileEntityType<TileEntityFluidTube> type, float base_amount)
	{
		super(type);
		
		logistic = new LogisticStorage(this::onLogisticChange, EnumLogisticType.FLUIDS); //Yeah this is needed because else the Object contructor is sued -.-
		logistic.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS);
		
		fluidOpt = new LazyOptional[6];
		logOpt = new LazyOptional[6];
		
		BASE_AMOUNT = base_amount;
	}
	
	public TileEntityFluidTube() 
	{
		this(FPTileEntitys.FLUID_TUBE, 625F);
	}
	
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.FLUIDS)
		{
			if(fluidOpt[face.get3DDataValue()]!=null)
			{
				fluidOpt[face.get3DDataValue()].invalidate();
				fluidOpt[face.get3DDataValue()] = null;
			}
		}
		
	}
	
		
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction side)
	{
		if(side==null)
			return LazyOptional.empty();
		
		if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			return HelperEnergyTransfer.getFluidCap(fluidOpt, side, this::getLogisticStorage, () -> new FluidHandler(side));
		}
		else if(capability == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logOpt[side.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) logOpt[side.get3DDataValue()];
			}
			else
			{
				logOpt[side.get3DDataValue()] = LazyOptional.of(() -> getLogisticStorage().getInterfaceforSide(side));
				logOpt[side.get3DDataValue()].addListener(p -> logOpt[side.get3DDataValue()] = null);
				return (LazyOptional<T>) logOpt[side.get3DDataValue()];
			}
		}
		return super.getCapability(capability, side);
	}

	public LogisticStorage getLogisticStorage() 
	{
		return logistic;
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		logistic.write(nbt); 
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		logistic.read(nbt);		
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		if(level.isClientSide)
		{
			load(getBlockState(), pkt.getTag());
			HelperChunks.renderUpdate(level, worldPosition);
		}
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pack;
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidOpt);
		super.setRemoved();
	}
	
	private int pumpFluid(FluidStack fluid, Direction side, FluidAction doFill)
	{
		BlockPos insert = worldPosition.relative(side);
		
		if(logistic.canInsert(side, EnumLogisticType.FLUIDS))
		{
			float amount = (BASE_AMOUNT * (1000F / fluid.getFluid().getAttributes().getViscosity()));
			
			if(amount > fluid.getAmount())
				amount = fluid.getAmount();
			
			if(amount<1 && amount>0)
			{
				amount = 1F;
			}
			
			fluid = new FluidStack(fluid, (int) amount);
			
			
			ArrayList<IFluidHandler> tanks = getConnectedTanks(insert);
			
			int liquidInserted = 0;
			for(IFluidHandler handler : tanks)
			{
				int filled = handler.fill(fluid, doFill);
				if(filled>0)
				{
					amount -= filled;
					liquidInserted += filled;
					fluid = new FluidStack(fluid, (int) amount);
				}	
				
				if(fluid.getAmount()<= 0)
				{
					break;
				}
			}
			return liquidInserted;
		}
		return 0;	
	}

	private class FluidHandler implements IFluidHandler
	{
		private final Direction side;
		
		public FluidHandler(Direction side)
		{
			this.side = side;
		}

		@Override
		public int getTanks() 
		{
			return 1;
		}

		@Override
		public FluidStack getFluidInTank(int tank) 
		{
			return FluidStack.EMPTY;
		}

		@Override
		public int getTankCapacity(int tank) 
		{
			return 1000;
		}

		@Override
		public boolean isFluidValid(int tank, FluidStack stack) 
		{
			return true;
		}

		@Override
		public int fill(FluidStack resource, FluidAction action) 
		{
			return pumpFluid(resource, side, action);
		}

		@Override
		public FluidStack drain(FluidStack resource, FluidAction action) 
		{
			return FluidStack.EMPTY;
		}

		@Override
		public FluidStack drain(int maxDrain, FluidAction action) 
		{
			return FluidStack.EMPTY;
		}
	
	}

	public ArrayList<IFluidHandler> getConnectedTanks(BlockPos src)
	{
		FPBlockSelector sel = FPSelectorHelper.getSelectorSave(level, worldPosition, selector, true); //tile entity only available for main thread
		Collection<ParentCoords> list = sel.getValidBlocks(sorter);
		ArrayList<IFluidHandler> tanks = new ArrayList<IFluidHandler>(list.size());
		for(ParentCoords pos : list)
		{
			if(!src.equals(pos))
			{
				Direction face = FacingUtil.getSide(pos, pos.getParent());
				TileEntity tile = level.getBlockEntity(pos);
				if(tile == null)
					continue;
				
				IFluidHandler handler = tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face).orElse(null);
				if(handler != null)
				{
					tanks.add(handler);
				}
			}
		}
		return tanks;
	}

	public EnumLogisticIO getModeForFace(Direction face, EnumLogisticType mode)
	{
		return logistic.getModeForFace(face, mode);
	}

	public boolean setModeForFace(Direction face, EnumLogisticIO inout, EnumLogisticType mode)
	{
		return logistic.setModeForFace(face, inout, mode);
	}
}
