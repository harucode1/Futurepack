package futurepack.common.block.logistic;

import java.util.List;

import futurepack.common.block.BlockHoldingTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;

public class BlockFluidTank extends BlockHoldingTile
{	
	public static final EnumProperty<EnumConnection> up = EnumProperty.create("zcon",EnumConnection.class);
	
	public final int tank_capacity;
	
	public BlockFluidTank(Block.Properties props, int capacity)
	{
		super(props);
		this.tank_capacity = capacity;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(up);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, IWorld w, BlockPos pos, BlockPos facingPos) 
	{
		BlockState bs =  super.updateShape(stateIn, facing, facingState, w, pos, facingPos);
		bs = bs.setValue(up, EnumConnection.get(pos, w, this));
		return bs;
	}
	
	public static boolean check(Direction face, BlockPos pos, IWorldReader w, Block bl)
	{
		return w.getBlockState(pos.relative(face)).getBlock() == bl;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean skipRendering(BlockState state, BlockState adjacentBlockState, Direction side)
	{
		if(adjacentBlockState.getBlock() == this)
		{
			return true;
		}
		return super.skipRendering(state, adjacentBlockState, side);
	}
	
	public static enum EnumConnection implements IStringSerializable
	{
		none,up,down,both;

		@Override
		public String getSerializedName()
		{
			return name().toLowerCase();
		}
		
		public static EnumConnection get(BlockPos pos, IWorldReader w, Block bl)
		{
			boolean flag1 = w.getBlockState(pos.above()).getBlock()==bl;
			boolean flag2 = w.getBlockState(pos.below()).getBlock()==bl;
			if(flag1&&flag2)
				return both;
			else if(flag1)
				return down;
			else if(flag2)
				return up;
			
			return none;
		}
	}


	@Override
	public boolean hasAnalogOutputSignal(BlockState state)
	{
		return true;
	}
	
	@Override
	public int getAnalogOutputSignal(BlockState blockState, World w, BlockPos pos)
	{
		TileEntityFluidTank tank = (TileEntityFluidTank) w.getBlockEntity(pos);
		if(!tank.getTankInfo().isEmpty())
		{
			return (15 * tank.getTankInfo().getFluidStack().getAmount()) / tank.getTankInfo().getCapacity();
		}
		return super.getAnalogOutputSignal(blockState, w, pos);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityFluidTank(tank_capacity);
	}
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) 
	{
		if(FluidUtil.interactWithFluidHandler(player, handIn, worldIn, pos, hit.getDirection()))
		{
			return ActionResultType.SUCCESS;
		}
		return super.use(state, worldIn, pos, player, handIn, hit);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		tooltip.add(new TranslationTextComponent("%s mb", this.tank_capacity));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
	
	@Override
	public boolean propagatesSkylightDown(BlockState state, IBlockReader reader, BlockPos pos) 
	{
		return true;
	}
	
	@Override
	public int getLightValue(BlockState state, IBlockReader world, BlockPos pos) 
	{
		int light = super.getLightValue(state, world, pos);
		TileEntity tile = world.getBlockEntity(pos);
		if(tile==null)
			return light;
		light = Math.max(light, tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, Direction.UP).map(handler -> 
		{
			Integer li = 0;
			for(int i=0;i<handler.getTanks();i++)
			{
				FluidStack stack = handler.getFluidInTank(i);
				if(!stack.isEmpty())
				{
					li = Math.max(li, stack.getFluid().getAttributes().getLuminosity(stack));
				}
			}
			return li;
		}).orElse(0));
		return light;
	}
}
