package futurepack.common.block.logistic.monorail;

import java.util.Random;

import futurepack.api.interfaces.IBlockMonocartWaypoint;
import futurepack.common.entity.monocart.EntityMonocart;
import futurepack.common.entity.monocart.EntityMonocartBase;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockMonorailStation extends BlockMonorailBasic implements IBlockMonocartWaypoint
{

//	public static final BooleanProperty loading = BooleanProperty.create("loading");
	public static final EnumProperty<EnumMonorailStation> loading = EnumProperty.create("loading", EnumMonorailStation.class);
	
	public BlockMonorailStation(Properties props) 
	{
		super(props);
	}
	
	@Override
	public boolean isBendable()
	{
		return false;
	}
	
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityMonorailStation();
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.MONORAIL_STATION.openGui(pl, pos);
		}	
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block neighborBlock, BlockPos npos, boolean isMoving) 
	{
		super.neighborChanged(state, w, pos, neighborBlock, npos, isMoving);
		TileEntityMonorailStation stat = (TileEntityMonorailStation) w.getBlockEntity(pos);
		stat.onNeighbourChange();
	}
	
	@Override
	public void onMonocartPasses(World w, BlockPos pos, BlockState state, EntityMonocartBase e)
	{	
		if(state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.ready)
		{
			TileEntityMonorailStation stat = (TileEntityMonorailStation) w.getBlockEntity(pos);
			if(e instanceof EntityMonocart)
			{
				EntityMonocart mono = (EntityMonocart) e;
				BlockPos[] posses = mono.getWaypoint();
				if(posses!=null)
				{
					for(BlockPos wayp : posses)
					{
						if(pos.equals(wayp))
						{
							e.setPaused(true);
							w.setBlockAndUpdate(pos, state.setValue(BlockMonorailStation.loading, EnumMonorailStation.transfer));
							w.getBlockTicks().scheduleTick(pos, this, 20);
							stat.progressMonoCart(mono);
							break;
						}
					}
				}		
			}
			
		}
		else if(state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.cooldown)
		{
			e.setPaused(false);
		}
	}	
	
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random rand)
	{
		if(state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.transfer)
		{
			if (!w.hasNeighborSignal(pos)) {
			  w.setBlockAndUpdate(pos, state.setValue(BlockMonorailStation.loading, EnumMonorailStation.cooldown));
			  w.getBlockTicks().scheduleTick(pos, this, 40);
			} else {
				w.getBlockTicks().scheduleTick(pos, this, 20);
			}
		}
		else if(state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.cooldown)
		{
			w.setBlockAndUpdate(pos, state.setValue(BlockMonorailStation.loading, EnumMonorailStation.ready));
			TileEntityMonorailStation stat = (TileEntityMonorailStation) w.getBlockEntity(pos);
			stat.onNeighbourChange();
		}
		
		super.tick( state, w, pos,rand);
	}
	
	@Override
	public int getSignal(BlockState state, IBlockReader blockAccess, BlockPos pos, Direction side)
	{
		return state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.transfer ? 15 : 0;
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//		if(meta>5)
//		{
//			return this.getDefaultState().with(loading, EnumMonorailStation.transfer).with(State_bendless, EnumMonorailStates.getFromMeta(meta%6));
//		}
//        return this.getDefaultState().with(loading, EnumMonorailStation.ready).with(State_bendless, EnumMonorailStates.getFromMeta(meta));
//    }
//
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//        return state.get(State_bendless).getMeta() + (state.get(BlockMonorailStation.loading)==EnumMonorailStation.transfer ? 6 : 0);
//    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(loading);
		super.createBlockStateDefinition(builder);
	}
	
	public static enum EnumMonorailStation implements IStringSerializable
	{
		ready,//green
		transfer,//red
		cooldown;//yellow

		@Override
		public String getSerializedName()
		{
			return name();
		}
		
	}

	@Override
	public ITextComponent getName(World w, BlockPos pos, BlockState state)
	{
		TileEntityMonorailStation tile = (TileEntityMonorailStation) w.getBlockEntity(pos);
		return tile.getName();
	}

	@Override
	public boolean isWaypoint(World w, BlockPos pos, BlockState state)
	{
		return true;
	}
}
