package futurepack.common.block.logistic.monorail;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockMonorailLift extends BlockMonorailBasic 
{
	private final VoxelShape shape_rail_and_collum;
	private final VoxelShape shape_collum;
	
	public BlockMonorailLift(Properties props) 
	{
		super(props);
		shape_collum = VoxelShapes.or(VoxelShapes.or(Block.box(0, 0, 0, 2, 16, 2), Block.box(14, 0, 0, 16, 16, 2)), VoxelShapes.or(Block.box(14, 0, 14, 16, 16, 16), Block.box(0, 0, 14, 2, 16, 16)));
		shape_rail_and_collum = VoxelShapes.or(shape_collum, VoxelShapes.box(0, 0, 0, 1F, 0.0625F, 1F));
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		if(state.getValue(getPropertie(this)) == EnumMonorailStates.UP_DOWN)
			return shape_collum;
		else
			return shape_rail_and_collum;
	}
	
	@Override
	public boolean isBendable()
	{
		return false;
	}
	
	@Override
	public boolean isLift()
	{
		return true;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean canSurvive(BlockState state, IWorldReader worldIn, BlockPos pos)
	{
		EnumMonorailStates monorail = state.getValue(getPropertie(this));
		if(monorail == EnumMonorailStates.UP_DOWN || monorail == EnumMonorailStates.NORTH_EAST_SOUTH_WEST_DOWN)
		{
			return true;
		}
		return super.canSurvive(state, worldIn, pos);
	}
	
	
}
