package futurepack.common.block.logistic.monorail;

import java.util.Arrays;

import futurepack.api.interfaces.tilentity.ITileRenameable;
import futurepack.common.FPTileEntitys;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

public class TileEntityMonorailWaypoint extends TileEntity implements ITileRenameable
{
	private String name = null;
	
	
	public TileEntityMonorailWaypoint()
	{
		super(FPTileEntitys.MONORAIL_WAYPOINT);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		if(name==null)
			name = "Monorail Waypoint " +Arrays.toString(new int[]{worldPosition.getX(),worldPosition.getY(),worldPosition.getZ()});
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		if(name!=null)
		nbt.putString("name", name);
		return super.save(nbt);
	}

	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		if(nbt.contains("name"))
			name = nbt.getString("name");
	}
	
	@Override
	public boolean hasCustomName()
	{
		return name!=null;
	}
	
	@Override
	public void setName(String name)
	{
		this.name = name;
		this.setChanged();
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		SUpdateTileEntityPacket tile = new SUpdateTileEntityPacket(worldPosition, -1, save(new CompoundNBT()));
		return tile;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		load(getBlockState(), pkt.getTag());
		super.onDataPacket(net, pkt);
	}

	@Override
	public ITextComponent getName() 
	{
		return new StringTextComponent(name);
	}

	@Override
	public ITextComponent getCustomName() 
	{
		return getName();
	}
	
//	@Override
//	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{
//		return oldState.getBlock() != newSate.getBlock();
//	}
}
