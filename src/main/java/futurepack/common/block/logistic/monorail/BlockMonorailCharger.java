package futurepack.common.block.logistic.monorail;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.common.entity.monocart.EntityMonocartBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockMonorailCharger extends BlockMonorailBasic
{
	public BlockMonorailCharger(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public boolean isBendable()
	{
		return false;
	}

	@Override
	public void onMonocartPasses(World w, BlockPos pos, BlockState state, EntityMonocartBase cart)
	{
		TileEntityMonorailCharger ch = (TileEntityMonorailCharger) w.getBlockEntity(pos);
		if(ch!=null)
		{
			cart.setPaused(cart.getPower()+10 < cart.getMaxPower());		
			
			if(cart.getPower()+1 < cart.getMaxPower())
			{
				ch.getCapability(CapabilityNeon.cap_NEON, Direction.UP).ifPresent(n -> {
					if(n.use(1) > 0)
					{
						cart.setPower(cart.getPower()+1);
					}
				});
			}
		}
		super.onMonocartPasses(w, pos, state, cart);
	}

	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityMonorailCharger();
	}
}
