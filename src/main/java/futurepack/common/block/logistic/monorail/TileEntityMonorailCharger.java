package futurepack.common.block.logistic.monorail;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;


import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;

public class TileEntityMonorailCharger extends TileEntity
{
	private NeonStorage power = new NeonStorage();
	private LazyOptional<INeonEnergyStorage> neon_opt;
	
	public TileEntityMonorailCharger() 
	{
		super(FPTileEntitys.MONORAIL_CHARGER);
	}
	
//	@Override
//	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
//	{
//		if(capability == CapabilityNeon.cap_NEON)
//		{
//			return true;
//		}
//		return super.hasCapability(capability, facing);
//	}
//	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neon_opt!=null)
				return (LazyOptional<T>) neon_opt;
			else
			{
				neon_opt = LazyOptional.of(() -> power);
				neon_opt.addListener(p -> neon_opt = null);
				return (LazyOptional<T>) neon_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neon_opt);
		super.setRemoved();
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("energy", power.serializeNBT());
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
	}

	
	public static class NeonStorage extends CapabilityNeon
	{
		public NeonStorage()
		{
			super(500, EnumEnergyMode.USE);
		}
		
		@Override
		public boolean canTransferTo(INeonEnergyStorage other)
		{
			return false;
		}
	}
}
