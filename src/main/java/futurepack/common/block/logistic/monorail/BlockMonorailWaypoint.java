package futurepack.common.block.logistic.monorail;

import futurepack.api.interfaces.IBlockMonocartWaypoint;
import futurepack.common.entity.monocart.EntityMonocartBase;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockMonorailWaypoint extends BlockMonorailBasic implements IBlockMonocartWaypoint
{
	public static final BooleanProperty powered = BlockStateProperties.POWERED;
	
	public BlockMonorailWaypoint(Properties props) 
	{
		super(props);
	}

	
	@Override
	public boolean isBendable()
	{
		return false;
	}

	@Override
	public void onMonocartPasses(World w, BlockPos pos, BlockState state, EntityMonocartBase cart)
	{
		boolean rd = w.hasNeighborSignal(pos);
		if (cart.isPaused() != rd ) {
			cart.setPaused(rd);
		}
		super.onMonocartPasses(w, pos, state, cart);
	}

	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block neighborBlock, BlockPos nBlock, boolean isMoving)
	{
		boolean rd = w.hasNeighborSignal(pos);
		state = state.setValue(powered, rd);
		w.setBlockAndUpdate(pos, state);
		super.neighborChanged(state, w, pos, neighborBlock, nBlock, isMoving);
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		//if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.RENAME_WAYPOINT.openGui(pl, pos);
		}	
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityMonorailWaypoint();
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//		if(meta>5)
//		{
//			return this.getDefaultState().with(powered, true).with(State_bendless, EnumMonorailStates.getFromMeta(meta%6));
//		}
//        return this.getDefaultState().with(powered, false).with(State_bendless, EnumMonorailStates.getFromMeta(meta));
//    }
//
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//        return state.get(State_bendless).getMeta() + (state.get(powered)? 6 : 0);
//    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(powered);
		super.createBlockStateDefinition(builder);
	}

	@Override
	public ITextComponent getName(World w, BlockPos pos, BlockState state)
	{
		return ((TileEntityMonorailWaypoint)w.getBlockEntity(pos)).getName();
	}

	@Override
	public boolean isWaypoint(World w, BlockPos pos, BlockState state)
	{
		return true;
	}
	
}
