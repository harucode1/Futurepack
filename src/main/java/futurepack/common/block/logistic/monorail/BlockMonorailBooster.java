package futurepack.common.block.logistic.monorail;

import futurepack.common.entity.monocart.EntityMonocartBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockMonorailBooster extends BlockMonorailBasic
{
	public static final BooleanProperty powered = BooleanProperty.create("powered");
	
	public BlockMonorailBooster(Block.Properties props)
	{
		super(props);
		this.registerDefaultState(this.defaultBlockState().setValue(powered, false));
	}
	
	@Override
	public boolean isBendable()
	{
		return false;
	}

	@Override
	public void onMonocartPasses(World w, BlockPos pos, BlockState state, EntityMonocartBase cart)
	{
		boolean rd = w.hasNeighborSignal(pos);
		cart.setHighSpeed(rd);
		super.onMonocartPasses(w, pos, state, cart);
	}

	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block neighborBlock, BlockPos nBlock, boolean isMoving)
	{
		boolean rd = w.hasNeighborSignal(pos);
		state = state.setValue(powered, rd);
		w.setBlockAndUpdate(pos, state);
		super.neighborChanged(state, w, pos, neighborBlock, nBlock, isMoving);
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(powered);
		super.createBlockStateDefinition(builder);
	}
}
