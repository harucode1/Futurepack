package futurepack.common.block.logistic;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPTileEntitys;

public class TileEntityWireNetwork extends TileEntityWireBase implements ITileNetwork
{

	public TileEntityWireNetwork() 
	{
		super(FPTileEntitys.WIRE_NETWORK);
	}

	@Override
	public int getMaxEnergy() 
	{
		return 500;
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}
	@Override
	public boolean isWire()
	{
		return true;
	}
	
	@Override
	public void onFunkPacket(PacketBase pkt) {}
}
