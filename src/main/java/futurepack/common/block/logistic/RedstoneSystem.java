package futurepack.common.block.logistic;

import java.util.Collection;
import java.util.Random;

import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.common.FPBlockSelector;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ComparatorBlock;
import net.minecraft.block.RedstoneWireBlock;
import net.minecraft.block.RepeaterBlock;
import net.minecraft.block.material.Material;
import net.minecraft.state.EnumProperty;
import net.minecraft.util.Direction;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class RedstoneSystem {
	public static boolean justUpdateNeighbours = false;
	
	public static final EnumProperty<EnumRedstone> STATE = EnumProperty.create("state", EnumRedstone.class);
	
	private static IBlockValidator ELEMENTS_OFF = new IBlockValidator() {
		
		@Override
		public boolean isValidBlock(World w, ParentCoords pos) 
		{			
			return w.getBlockState(pos).hasProperty(STATE) && w.getBlockState(pos).getValue(STATE) == EnumRedstone.OFF;
		}
	};
			
	private static IBlockValidator ELEMENTS_ON = new IBlockValidator() {
		
		@Override
		public boolean isValidBlock(World w, ParentCoords pos) 
		{			
			return w.getBlockState(pos).hasProperty(STATE) && w.getBlockState(pos).getValue(STATE) == EnumRedstone.ON;
		}
	};
	
	private static IBlockValidator ELEMENTS_SOURCE = new IBlockValidator() {
		
		@Override
		public boolean isValidBlock(World w, ParentCoords pos) 
		{			
			return w.getBlockState(pos).hasProperty(STATE) && w.getBlockState(pos).getValue(STATE) == EnumRedstone.SOURCE;
		}
	};
	
	private static IBlockValidator ELEMENTS_ALL = new IBlockValidator() {
		
		@Override
		public boolean isValidBlock(World w, ParentCoords pos) 
		{			
			return w.getBlockState(pos).hasProperty(STATE);
		}
	};
			
	private static IBlockSelector ELEMENTS_SELECT = new IBlockSelector() {
				
				@Override
				public boolean isValidBlock(World w, BlockPos jkl, Material m, boolean dia, ParentCoords parent) 
				{
					if(dia)
						return false;
					
					return w.getBlockState(jkl).hasProperty(STATE);
				}
				
				@Override
				public boolean canContinue(World w, BlockPos jkl, Material m, boolean dia, ParentCoords parent)
				{
					return true;
				}
			};
	
	
	public static int getWeakPower(BlockState state, IBlockReader w, BlockPos pos, Direction face) {
		EnumRedstone m = state.getValue(STATE);
		
		if(m == EnumRedstone.ON || m == EnumRedstone.SOURCE)
		{
			Direction dir = face.getOpposite();
			if(!w.getBlockState(pos.relative(dir)).hasProperty(STATE))
			{
				if(m == EnumRedstone.ON)
					return 15;
				
				BlockState b = w.getBlockState(pos.relative(face));
				
				int in = b.getSignal(w, pos.relative(face), face);
				if(in > 0)
				{
					return in-1;
				}
				
			}
		}
		return 0;
	}
	
	public static void neighborChanged(Block currentBlock, World w, BlockPos pos) {
		//if(!justUpdateNeighbours)
			w.getBlockTicks().scheduleTick(new BlockPos(pos), currentBlock, 1);
	}
	
	public static void onBlockAdded(BlockState state, World w, BlockPos jkl, BlockState oldState, boolean isMoving) {
		if(oldState.getBlock() != state.getBlock())
			updateWireSystem(state, w, jkl);
	}
	
	public static void tick(BlockState state, ServerWorld w, BlockPos jkl, Random rand) {
		updateWireSystem(state, w, jkl);
	}
	
	public static boolean canConnectToRedstone(IWorld w, BlockPos pos, BlockPos xyz, BlockState facingState, Direction face) {
		
		if(facingState.hasProperty(STATE)) {
			return true;
		}
		
		if(!(facingState.hasProperty(STATE)) && facingState.canConnectRedstone(w, xyz, face.getOpposite())) {
			
			BlockState downState = w.getBlockState(pos.below());
			
			return (downState.getBlock() instanceof BlockWireBase || downState.isAir(w, pos.below()) 
					|| xyz.asLong() == pos.below().asLong()) && !(downState.getBlock() instanceof RepeaterBlock || downState.getBlock() instanceof ComparatorBlock);
		}
		
		return false;
	}
	
	//Internal Methods
	
	private static boolean isGettingPowered(World world, BlockPos pos) {		
		boolean power = false;
		
		for(Direction direction : FacingUtil.VALUES) {
			if(direction != Direction.DOWN
					&& world.getBlockState(pos.relative(direction)).hasProperty(RedstoneWireBlock.POWER) 
					&& world.getBlockState(pos.relative(direction)).getValue(RedstoneWireBlock.POWER) > 0) {
				power = true;
				break;
			}
		}
		return power || world.getBestNeighborSignal(pos) > 0;
	}
	
	private static void updateWireSystem(BlockState state, World world, BlockPos pos) {
		EnumRedstone ownState = state.getValue(STATE);
		
		FPBlockSelector sel = new FPBlockSelector(world, ELEMENTS_SELECT);
		sel.selectBlocks(pos);
		
		if(ownState == EnumRedstone.OFF) {
			if(isGettingPowered(world, pos)) {
				justUpdateNeighbours = true;
				world.setBlockAndUpdate(pos, state.setValue(STATE, EnumRedstone.SOURCE));
				justUpdateNeighbours = false;
				
				updateSystem(sel, world);
				
			}
			else {
				updateSystem(sel, world);
			}
		}
		else if(ownState == EnumRedstone.SOURCE && !isGettingPowered(world, pos)) {
			
			justUpdateNeighbours = true;
			world.setBlockAndUpdate(pos, state.setValue(STATE, EnumRedstone.ON));
			justUpdateNeighbours = false;
			
			Collection<ParentCoords> sources = sel.getValidBlocks(ELEMENTS_SOURCE);
			
			for(ParentCoords c : sources) {
				world.getBlockTicks().scheduleTick(new BlockPos(c), world.getBlockState(c).getBlock(), 1);
			}
						
			updateSystem(sel, world);
		}
		else if(ownState == EnumRedstone.ON) {
			updateSystem(sel, world);
		}
	}
	
	private static void updateSystem(FPBlockSelector sel, World world) {
		Collection<ParentCoords> list = sel.getValidBlocks(ELEMENTS_ALL);
		
		Collection<ParentCoords> sources = sel.getValidBlocks(ELEMENTS_SOURCE);
		
		EnumRedstone targetState = sources.isEmpty() ? EnumRedstone.OFF : EnumRedstone.ON;
		
		justUpdateNeighbours = true;
		
		for(ParentCoords coords : list) {
			BlockState wire = world.getBlockState(coords);
			
			if(wire.getValue(STATE) != EnumRedstone.SOURCE && wire.getValue(STATE) != targetState) {
				world.setBlockAndUpdate(coords, wire.setValue(STATE, targetState));
			}
		}
		
		justUpdateNeighbours = false;
	}
	
	public enum EnumRedstone implements IStringSerializable
	{
		OFF,
		ON,
		SOURCE;
		@Override
		public String getSerializedName() 
		{
			return toString().toLowerCase();
		}
	}
}
