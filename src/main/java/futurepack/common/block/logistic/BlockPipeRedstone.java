package futurepack.common.block.logistic;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class BlockPipeRedstone extends BlockPipeBase 
{	
	public BlockPipeRedstone(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	protected EnumSide getAdditionalConnections(TileEntityPipeBase pipe, @Nullable TileEntity otherTile, Direction face) {
		
		BlockPos jkl = pipe.getBlockPos().relative(face);
		
		BlockState b = pipe.getLevel().getBlockState(jkl);
		if(b.getBlock().canConnectRedstone(b, pipe.getLevel(), jkl, face.getOpposite()))
			return EnumSide.CABLE; // CABLE because no inv is present when this is called
		
		return EnumSide.OFF;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.futurepack.block.conduct.redstone"));
	}	
	
	@Override
	public boolean canConnectRedstone(BlockState state, IBlockReader world, BlockPos pos, Direction side)
	{
		if(side == null)
		{
			side = Direction.DOWN;
		}
			
		TileEntityPipeBase p = (TileEntityPipeBase) world.getBlockEntity(pos);
		if(p == null)
			return false;
		
		return (p.isSideLocked(side.getOpposite()) ? p.isIgnoreLockSub(side.getOpposite()) : true);
	}
	
	@Override
	public int getSignal(BlockState state, IBlockReader w, BlockPos pos, Direction face)
	{
		TileEntityPipeBase p = (TileEntityPipeBase) w.getBlockEntity(pos);
		
		if(p != null && p.isSideLocked(face.getOpposite()) ? p.isIgnoreLockSub(face.getOpposite()) : true)
		{
			return RedstoneSystem.getWeakPower(state, w, pos, face);
		}
		return 0;
	}
	
	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block block, BlockPos blockPos, boolean isMoving)
	{
		RedstoneSystem.neighborChanged(this, w, pos);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onPlace(BlockState state, World w, BlockPos jkl, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, jkl, oldState, isMoving);
		RedstoneSystem.onBlockAdded(state, w, jkl, oldState, isMoving);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos jkl, Random rand)
	{
		RedstoneSystem.tick(state, w, jkl, rand);
	}
		
	@Override
	public void onRemove(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
	      if (state.getBlock() != newState.getBlock()) {
	         super.onRemove(state, worldIn, pos, newState, isMoving);
	      }
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityPipeNormal();
	}

	@Override
	public boolean hasSpecial() 
	{
		return true;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(RedstoneSystem.STATE);
	}
}
