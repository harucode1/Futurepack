package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;

public class TileEntityWireSuper extends TileEntityWireBase 
{

	public TileEntityWireSuper() 
	{
		super(FPTileEntitys.WIRE_SUPER);
	}

	@Override
	public int getMaxEnergy() 
	{
		return 1000;
	}

}
