package futurepack.common.block.logistic.plasma;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.modification.machines.TileEntitySolarPanel;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.world.TickPriority;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityPlasma2NeonT0 extends TileEntitySolarPanel
{
	private Boolean active = null;
	final CapabilityPlasma plasma;
	private LazyOptional<IPlasmaEnergyStorage> optPlasma;
	
	public TileEntityPlasma2NeonT0(TileEntityType<? extends TileEntityPlasma2NeonT0> type, long maxTransfer) 
	{
		super(type);
		plasma = new CapabilityPlasma(maxTransfer, true, false);
	}
	
	public static TileEntityPlasma2NeonT0 converterT0()
	{
		return new TileEntityPlasma2NeonT0(FPTileEntitys.PLASMA_2_NEON_T0, 5);
	}
	
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("plasma", plasma.serializeNBT());
		return nbt;
	}

	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		plasma.deserializeNBT(nbt.getCompound("plasma"));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if (capability == CapabilityPlasma.cap_PLASMA)
		{
			if (optPlasma != null)
				return (LazyOptional<T>) optPlasma;
			else
			{
				optPlasma = LazyOptional.of(() -> plasma);
				optPlasma.addListener(p -> optPlasma = null);
				return (LazyOptional<T>) optPlasma;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved()
	{
		HelperEnergyTransfer.invalidateCaps(optPlasma);
		super.setRemoved();
	}

	@Override
	public void updateTile(int ticks) 
	{
		if(!level.isClientSide)
		{
			light = plasma.get() > 0 ? 15 : 0;
			if(light > 0)
			{
				int ne = (int) (500 * Math.min(1 + getChipPower(EnumChipType.INDUSTRIE) * 0.2F, 2F));	
				if(energy.get() + ne <= energy.getMax())
				{
					power = ne;
					energy.add(ne);
					plasma.add(-1);
				}
			}
			
			
			if(!items.get(0).isEmpty() && energy.get() > 0)
			{		
				if(items.get(0).getItem()instanceof IItemNeon)
				{
					IItemNeon ch = (IItemNeon) items.get(0).getItem();
					ItemStack it = items.get(0);
					if(ch.isNeonable(it) && ch.getNeon(it) < ch.getMaxNeon(it))
					{
						ch.addNeon(it, energy.use(ticks));
					}					
				}		
			}
			Boolean plasma = this.plasma.get() > 0;
			if(plasma != active)
			{
				this.level.getBlockTicks().scheduleTick(getBlockPos(), this.getBlockState().getBlock(), 1, TickPriority.LOW);
				this.active = plasma;
			}
		}
	}
	
	@Override
	public boolean isWorking()
	{
		return power > 0;
	}
}
