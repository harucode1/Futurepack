package futurepack.common.block.logistic.plasma;

import java.util.Collection;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.common.FPBlockSelector;
import futurepack.common.FuturepackTags;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class SelectorTank 
{
	public static class SelectTank implements IBlockSelector
	{
		private int tested = 0;
		private final EnumPlasmaTiers tier;
		private final int maxBlocks;
		
		private boolean error = false;
		public boolean errorToBig = false;
		public ParentCoords wrongBlock = null;
		
		
		public SelectTank(EnumPlasmaTiers tier) 
		{
			this.tier = tier;
			maxBlocks = tier.getMaxBlockAmount();
		}
		
		@Override
		public boolean isValidBlock(World w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent) 
		{
			if(error)
			{
				return false;
			}
			
			if(tested>maxBlocks)
			{
				error = true;
				errorToBig = true;
				throw new IllegalArgumentException("tank size too large");
			}
			
			ParentCoords pp;
			if(pos instanceof ParentCoords)
				pp = (ParentCoords) pos;
			else
				pp = new ParentCoords(pos, parent);
			
			boolean airflag = selectAir.isValidBlock(w, pp);
			boolean wallflag = selectTankWall.isValidBlock(w, pp);
			
			if(!airflag && !wallflag)
			{
				error = true;
				wrongBlock = pp;
				throw new IllegalArgumentException("not air or wall block");
			}
			return airflag && wallflag;
		}
		
		@Override
		public boolean canContinue(World w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent) 
		{
			tested++;
			return selectAir.isValidBlock(w, new ParentCoords(pos, parent));
		}
	};
	
	public static final IBlockValidator selectAir = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(World w, ParentCoords pos) 
		{
			return w.isEmptyBlock(pos);
		}
	};
	
	public static final IBlockValidator selectTankWall = new IBlockValidator()
	{
		
		@Override
		public boolean isValidBlock(World w, ParentCoords pos) 
		{
			return FuturepackTags.plasmatank_wall.contains(w.getBlockState(pos).getBlock());
		}
	};
	
	public static boolean startSelecting(ServerWorld w, BlockPos pos, EnumPlasmaTiers tier, TileEntityPlasmaStorageCore core, PlayerEntity pl)
	{
		if(!w.isEmptyBlock(pos))
		{
			throw new IllegalArgumentException("Must be air block inside tank to start at");
		}
		SelectTank select = new SelectTank(tier);
		FPBlockSelector result = new FPBlockSelector(w, select);
		try
		{
			result.selectBlocks(pos);
			Collection<ParentCoords> innerTank = result.getValidBlocks(selectAir);
			Collection<ParentCoords> tankWalls = result.getValidBlocks(selectTankWall);
			
			core.activateTank(innerTank, tankWalls);
			return true;
		}
		catch(IllegalArgumentException e)
		{ }
		
		if(select.error)
		{
			if(select.errorToBig)
			{
				pl.sendMessage(new TranslationTextComponent("chat.plasma_core.toobig", select.tested, select.maxBlocks), Util.NIL_UUID);
			}
			else if(select.wrongBlock !=null)
			{
				pl.sendMessage(new TranslationTextComponent("chat.plasma_core.wrongblock", select.wrongBlock.toString()), Util.NIL_UUID);
			}
		}
		
		return false;
	}
}
