package futurepack.common.block.logistic.plasma;

import java.util.function.LongSupplier;

public class DynamicSizedPlasmaTank extends CapabilityPlasma
{
	private LongSupplier tankSize;
	
	public DynamicSizedPlasmaTank(LongSupplier tankSize, boolean in, boolean out)
	{
		super(-1, in, out);
		this.tankSize = tankSize;
	}
	
	@Override
	public long getMax() 
	{
		return tankSize.getAsLong();
	}
}
