package futurepack.common.block.logistic.plasma;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.common.util.LazyOptional;

public abstract class TileEntityPlasmaStorageWall extends TileEntity
{

	public TileEntityPlasmaStorageWall(TileEntityType<?> tileEntityTypeIn) 
	{
		super(tileEntityTypeIn);
	}
	
	protected abstract LazyOptional<TileEntityPlasmaStorageCore> getCore();

	private LazyOptional<IPlasmaEnergyStorage> plasmaOpt;
	
	protected LazyOptional<IPlasmaEnergyStorage> getCorePlasma()
	{
		if(plasmaOpt==null)
		{
			LazyOptional<TileEntityPlasmaStorageCore> core = getCore();
			plasmaOpt = core.lazyMap(m -> m.getCapability(CapabilityPlasma.cap_PLASMA, null).orElse(CapabilityPlasma.EMPTY));
			core.addListener(p -> plasmaOpt = null);
			plasmaOpt.addListener(p -> plasmaOpt = null);
			return plasmaOpt;
		}
		else
		{
			return plasmaOpt;
		}
	}
	
	public abstract boolean isActive();
	
	public void onMultiblockTankChange(boolean isActive, TileEntityPlasmaStorageCore core)
	{
		
	}
}
