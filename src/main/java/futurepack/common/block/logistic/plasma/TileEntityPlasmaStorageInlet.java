package futurepack.common.block.logistic.plasma;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityPlasmaStorageInlet extends TileEntityPlasmaStorageWall 
{
	protected final long tranferRate;
	private BlockPos corePos;
	
	
	public TileEntityPlasmaStorageInlet(TileEntityType<?> tileEntityTypeIn, long tranferRate) 
	{
		super(tileEntityTypeIn);
		this.tranferRate = tranferRate;
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt) 
	{
		if(corePos!=null)
			nbt.putIntArray("corePos", new int[] {corePos.getX(), corePos.getY(), corePos.getZ()});
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt) 
	{
		if(nbt.contains("corePos"))
		{
			int[] p = nbt.getIntArray("corePos");
			corePos = new BlockPos(p[0], p[1], p[2]);
		}
		super.load(state, nbt);
	}
	
	private LazyOptional<IPlasmaEnergyStorage> opt;

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		if(cap == CapabilityPlasma.cap_PLASMA)
		{
			if(opt!=null)
				return (LazyOptional<T>) opt;
			
			else
			{
				LazyOptional<IPlasmaEnergyStorage> core = getCorePlasma();
				if(core.isPresent())
				{
					opt = getCorePlasma().lazyMap(c -> new LimitedPlasma(c, tranferRate));
					opt.addListener(p -> opt = null);
					core.addListener(c -> opt.invalidate());
					return (LazyOptional<T>) opt;
				}
				else
				{
					return (LazyOptional<T>) core;
				}
				
			}
		}
		return super.getCapability(cap, side);
	}
	
	public static class LimitedPlasma implements IPlasmaEnergyStorage
	{
		private final IPlasmaEnergyStorage storage;
		private final long maxTransfer;
		
		public LimitedPlasma(IPlasmaEnergyStorage storgae, long maxTransfer) 
		{
			super();
			this.storage = storgae;
			this.maxTransfer = maxTransfer;
		}

		@Override
		public boolean canAcceptFrom(IPlasmaEnergyStorage other) 
		{
			return storage.canAcceptFrom(other);
		}

		@Override
		public boolean canTransferTo(IPlasmaEnergyStorage other) 
		{
			return storage.canTransferTo(other);
		}

		@Override
		public long get() 
		{
			return storage.get();
		}

		@Override
		public long getMax() 
		{
			return storage.getMax();
		}

		@Override
		public long use(long used) 
		{
			return storage.use(Math.min(used, maxTransfer));
		}

		@Override
		public long add(long added) 
		{
			return storage.add(Math.min(added, maxTransfer));
		}
		
		
	}
	
	private LazyOptional<TileEntityPlasmaStorageCore> core;

	@Override
	protected LazyOptional<TileEntityPlasmaStorageCore> getCore() 
	{
		if(isActive() && core == null)
		{
			TileEntityPlasmaStorageCore tile = (TileEntityPlasmaStorageCore) level.getBlockEntity(corePos);
			if(tile!=null)
			{
				core = tile.getCore();
				return core;
			}
			else
			{
				corePos = null;
			}
		}
		return core == null ? LazyOptional.empty() : core;
	}

	@Override
	public boolean isActive() 
	{
		return corePos != null;
	}
	
	@Override
	public void onMultiblockTankChange(boolean isActive, TileEntityPlasmaStorageCore core) 
	{
		if(isActive && !isActive())
		{
			this.corePos = core.getBlockPos();
			this.core = core.getCore();
			this.core.addListener(p -> {
				this.core.invalidate();
				this.corePos = null;
			});
		}
		else if(!isActive && isActive())
		{
			if(getCore().isPresent())
			{
				TileEntityPlasmaStorageCore currentCore = getCore().orElse(null);
				if(core == currentCore)
				{
					core = null;
					corePos = null;
				}
			}
		}
		super.onMultiblockTankChange(isActive, core);
	}
}
