package futurepack.common.block.logistic.plasma;

import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.energy.EnergyStorage;

public class RfStorage extends EnergyStorage 
{
	public RfStorage(int capacity)
	{
		super(capacity);
	}

	public RfStorage(int capacity, int maxTransfer)
	{
		this(capacity, maxTransfer, maxTransfer, 0);
	}
	
	public RfStorage(int capacity, int maxReceive, int maxExtract)
	{
		this(capacity, maxReceive, maxExtract, 0);
	}

	public RfStorage(int capacity, int maxReceive, int maxExtract, int energy)
	{
		super(capacity, maxReceive, maxExtract, energy);
			
	}
	
	
	public CompoundNBT serializeNBT()
	{
		CompoundNBT nbt = new CompoundNBT();
		nbt.putInt("p", energy);
		nbt.putInt("c", capacity);
		nbt.putInt("r", maxReceive);
		nbt.putInt("e", maxExtract);
		return nbt;
	}

	
	public void deserializeNBT(CompoundNBT nbt)
	{
		this.energy = nbt.getInt("p");
		this.capacity = nbt.getInt("c");
		this.maxReceive = nbt.getInt("r");
		this.maxExtract = nbt.getInt("e");
	}

	/**
	 * CLient only
	 * @param rf
	 */
	public void setEnergy(int rf)
	{
		this.energy = rf;
	}
}
