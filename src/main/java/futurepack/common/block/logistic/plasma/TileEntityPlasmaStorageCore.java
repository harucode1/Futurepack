package futurepack.common.block.logistic.plasma;

import java.util.Collection;
import java.util.Set;

import futurepack.api.ParentCoords;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import it.unimi.dsi.fastutil.objects.ObjectRBTreeSet;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class TileEntityPlasmaStorageCore extends TileEntityPlasmaStorageWall
{
	public final EnumPlasmaTiers tier;
	
	final CapabilityPlasma plasma;
	private LazyOptional<IPlasmaEnergyStorage> optPlasma;
	
	protected MutableBoundingBox box;
	private Set<BlockPos> airBlocks, wallBlocks;
	protected boolean isActive = false;
	
	public TileEntityPlasmaStorageCore(TileEntityType<?> tileEntityTypeIn, EnumPlasmaTiers tier)
	{
		super(tileEntityTypeIn);
		this.tier = tier;
		plasma = new DynamicSizedPlasmaTank(this::getMaxTankSize, true, false);
	}
	
	public long getMaxTankSize()
	{
		if(airBlocks!=null)
		{
			long air = airBlocks.size();
			return air * tier.getPlasmaPerBlock();
		}
		return 0L;
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("plasma", plasma.serializeNBT());
		nbt.putBoolean("activated", isActive);
		
		if(isActive)
		{
			int blocks[] = new int[airBlocks.size()*3];
			int i=0;
			for(BlockPos p : airBlocks)
			{
				blocks[i++] = p.getX();
				blocks[i++] = p.getY();
				blocks[i++] = p.getZ();
			}
			nbt.putIntArray("airBlocks", blocks);
			
			blocks = new int[wallBlocks.size()];
			i=0;
			for(BlockPos p : wallBlocks)
			{
				blocks[i++] = p.getX();
				blocks[i++] = p.getY();
				blocks[i++] = p.getZ();
			}
			nbt.putIntArray("wallBlocks", blocks);
		}
		return nbt;
	}
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		plasma.deserializeNBT(nbt.getCompound("plasma"));
		isActive = nbt.getBoolean("activated");
		
		if(isActive)
		{
			this.airBlocks = new ObjectRBTreeSet<BlockPos>();
			this.wallBlocks = new ObjectRBTreeSet<BlockPos>();
			
			int[] air = nbt.getIntArray("airBlocks");
			int[] walls = nbt.getIntArray("wallBlocks");
			
			for(int i=0;i<air.length;i+=3)
			{
				BlockPos p = new BlockPos(air[i], air[i+1], air[i+2]);
				airBlocks.add(p);
			}
			for(int i=0;i<walls.length;i+=3)
			{
				BlockPos p = new BlockPos(walls[i], walls[i+1], walls[i+2]);
				wallBlocks.add(p);
			}
			calcBox();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(isActive && capability == CapabilityPlasma.cap_PLASMA)
		{
			if(optPlasma!=null)
				return (LazyOptional<T>) optPlasma;
			else
			{
				optPlasma = LazyOptional.of(() -> plasma);
				optPlasma.addListener(p -> optPlasma = null);
				return (LazyOptional<T>) optPlasma;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(optPlasma, optCore);
		super.setRemoved();
		MinecraftForge.EVENT_BUS.unregister(this);
	}
	
	private LazyOptional<TileEntityPlasmaStorageCore> optCore = null;
	
	@Override
	protected LazyOptional<TileEntityPlasmaStorageCore> getCore() 
	{
		if(optCore == null)
		{
			optCore = LazyOptional.of(() -> this);
			optCore.addListener(c -> optCore = null);
		}
		
		return optCore;
	}
	
	public MutableBoundingBox getSize()
	{
		return new MutableBoundingBox(-3, 1, -3, 4, 8, 4);
	}

	public float getFillrate() 
	{
		double d = (double)plasma.get() / (double)plasma.getMax();
		return (float) d;
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() 
	{
		return INFINITE_EXTENT_AABB;
	}
	
	@Override
	public void onLoad() 
	{
		super.onLoad();
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	public void onTankBroken(BlockPos breakPos)
	{
		isActive = false;
		if(optPlasma!=null)
			optPlasma.invalidate();

		airBlocks = null;
		wallBlocks = null;
		box = null;
		setChanged();
		onMultiblockTankChange(false, this);
	}
	
	private void checkTankMultiBlock(BlockPos pos)
	{
		if(isActive && box.isInside(pos))
		{
			boolean broken = false;
			if(airBlocks.contains(pos))
			{
				if(!SelectorTank.selectAir.isValidBlock(level, new ParentCoords(pos, null)))
				{
					broken = true;
				}
			}
			else if(wallBlocks.contains(pos))
			{
				if(!SelectorTank.selectTankWall.isValidBlock(level, new ParentCoords(pos, null)))
				{
					broken = true;
				}
			}
			if(broken)
			{
				onTankBroken(pos);
			}
		}
	}
	
	@SubscribeEvent
	public void onBlockBroken(BlockEvent event)
	{
		checkTankMultiBlock(event.getPos());
	}
	
	public void activateTank(Collection<ParentCoords> airBlocks, Collection<ParentCoords> wallBlocks)
	{
		this.airBlocks = new ObjectRBTreeSet<BlockPos>(airBlocks);
		this.wallBlocks = new ObjectRBTreeSet<BlockPos>(wallBlocks);
		isActive = true;
		if(optPlasma!=null)
			optPlasma.invalidate();
		
		calcBox();
		setChanged();
		onMultiblockTankChange(true, this);
	}
	
	protected void calcBox()
	{
		box = new MutableBoundingBox(worldPosition, worldPosition);
		for(Vector3i vec : wallBlocks) //we can assume that all walls enclose the air blocks
		{
			box.x0 = Math.min(box.x0, vec.getX());
			box.x1 = Math.max(box.x1, vec.getX());
			box.y0 = Math.min(box.y0, vec.getY());
			box.y1 = Math.max(box.y1, vec.getY());
			box.z0 = Math.min(box.z0, vec.getZ());
			box.z1 = Math.max(box.z1, vec.getZ());
		}
		box.x1++;box.y1++;box.z1++;
		box.x0--;box.y0--;box.z0--;
	}
	
	@Override
	public boolean isActive() 
	{
		return isActive;
	}
	
	@Override
	public void onMultiblockTankChange(boolean isActive, TileEntityPlasmaStorageCore core) 
	{
		for(BlockPos pos : wallBlocks)
		{
			TileEntity tile = level.getBlockEntity(pos);
			if(tile instanceof TileEntityPlasmaStorageWall)
			{
				((TileEntityPlasmaStorageWall)tile).onMultiblockTankChange(isActive, core);
			}
		}
	}
}
