package futurepack.common.block.logistic.plasma;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockPlasmaCore extends BlockRotateableTile
{
	public BlockPlasmaCore(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityPlasmaStorageCoreTier1();
	}
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit)
	{
		if(!worldIn.isClientSide)
		{
			TileEntityPlasmaStorageCore core = (TileEntityPlasmaStorageCore) worldIn.getBlockEntity(pos);

			if(player.getItemInHand(hand).getItem() == ToolItems.scrench)
			{
				BlockPos tankInner = pos.relative(state.getValue(BlockRotateableTile.FACING));
				return SelectorTank.startSelecting((ServerWorld) worldIn, tankInner, core.tier, core, player) ? ActionResultType.SUCCESS : ActionResultType.FAIL;
			}
		
			return ActionResultType.PASS; //maybe add a GUI here 
		}
		else
		{
			return ActionResultType.SUCCESS;
		}
	}
}
