package futurepack.common.block.logistic.plasma;

import java.util.function.Supplier;

import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockPlasmaConverter extends BlockPlasmaTransferPipe 
{

	private VoxelShape full    = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,16,16}}, 0, 0);
	
	
	public BlockPlasmaConverter(Properties properties, Supplier<TileEntity> sup) 
	{
		super(properties, sup);
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand handIn, BlockRayTraceResult hit) 
	{
		if(HelperResearch.isUseable(pl, state))
		{
			FPGuiHandler.PLASMA_CONVERTER.openGui(pl, pos);
			return ActionResultType.SUCCESS;
		}
		return ActionResultType.PASS;
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		return full;
	}
}
