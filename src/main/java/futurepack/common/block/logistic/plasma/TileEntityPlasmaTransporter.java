package futurepack.common.block.logistic.plasma;

import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.world.TickPriority;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

/**
 * Can transfer plasma in one direction
 * 
 * @author MCenderdragon
 */
public class TileEntityPlasmaTransporter extends TileEntity implements ITickableTileEntity
{
	final CapabilityPlasma plasma;
	private LazyOptional<IPlasmaEnergyStorage> optPlasma;

	private Boolean noPlasma = null;
	
	public TileEntityPlasmaTransporter(TileEntityType<?> tileEntityTypeIn, long maxTransfer)
	{
		super(tileEntityTypeIn);
		plasma = new CapabilityPlasma(maxTransfer, true, false);
	}

	public static TileEntityPlasmaTransporter pipeT1()
	{
		return new TileEntityPlasmaTransporter(FPTileEntitys.PLASMA_PIPE_T1, EnumPlasmaTiers.Tier1.getPlasmaTranferPipe());
	}
	
	public static TileEntityPlasmaTransporter pipeT0()
	{
		return new TileEntityPlasmaTransporter(FPTileEntitys.PLASMA_PIPE_T0, EnumPlasmaTiers.Tier0.getPlasmaTranferPipe());
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("plasma", plasma.serializeNBT());
		return nbt;
	}

	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		plasma.deserializeNBT(nbt.getCompound("plasma"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if (capability == CapabilityPlasma.cap_PLASMA)
		{
			if (optPlasma != null)
				return (LazyOptional<T>) optPlasma;
			else
			{
				optPlasma = LazyOptional.of(() -> plasma);
				optPlasma.addListener(p -> optPlasma = null);
				return (LazyOptional<T>) optPlasma;
			}
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public void setRemoved()
	{
		HelperEnergyTransfer.invalidateCaps(optPlasma);
		super.setRemoved();
	}

	private LazyOptional<IPlasmaEnergyStorage> otherPlasma;

	@Override
	public void tick()
	{
		if (otherPlasma == null)
		{
			Direction dir = getOutput();
			TileEntity tile = level.getBlockEntity(worldPosition.relative(dir));
			if (tile != null)
			{
				otherPlasma = tile.getCapability(CapabilityPlasma.cap_PLASMA, dir.getOpposite());
				otherPlasma.addListener(p -> otherPlasma = null);
			}
		}
		if (otherPlasma != null)
		{
			otherPlasma.ifPresent(this::transferTo);
		}
		
		Boolean empty = this.plasma.get() == 0;
		if(empty != this.noPlasma)
		{
			this.level.getBlockTicks().scheduleTick(getBlockPos(), this.getBlockState().getBlock(), 1, TickPriority.LOW);
			this.noPlasma = empty;
		}
	}

	private void transferTo(IPlasmaEnergyStorage storage)
	{
		if (storage.canAcceptFrom(plasma))
		{
			// ignoring result of plasma.canTransferTo(other) as it will be false
			plasma.use(storage.add(plasma.get()));
		}
	}

	public Direction getOutput()
	{
		return getBlockState().getValue(BlockStateProperties.FACING);
	}
}
