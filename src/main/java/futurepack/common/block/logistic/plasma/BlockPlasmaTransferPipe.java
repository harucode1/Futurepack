package futurepack.common.block.logistic.plasma;

import java.util.Random;
import java.util.function.Supplier;

import futurepack.common.block.BlockRotateableTile;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.server.ServerWorld;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockPlasmaTransferPipe extends BlockRotateableTile
{
	private VoxelShape up    = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 0, 0);
	private VoxelShape down  = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 180, 0);
	private VoxelShape north = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 90, 0);
	private VoxelShape south = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 90, 180);
	private VoxelShape west  = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 90, 90);
	private VoxelShape east  = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 90, 270);
	
	private final Supplier<TileEntity> sup;
	
	public BlockPlasmaTransferPipe(Properties properties, Supplier<TileEntity> sup) 
	{
		super(properties);
		this.sup = sup;
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		switch (state.getValue(FACING))
		{
		case UP:return up;
		case DOWN:return down;
		case NORTH:return north;
		case SOUTH: return south;
		case WEST:return west;
		case EAST: return east;
		default:
			return super.getShape(state, worldIn, pos, context);
		}
	}
	
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return sup.get();
	}
	
	@Override
	public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand)
	{
		//called when plasma state changes between empty and filled
		super.tick(state, worldIn, pos, rand);
	}
}
