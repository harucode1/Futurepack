package futurepack.common.block.logistic.plasma;

import futurepack.common.FPTileEntitys;

public class TileEntityPlasmaStorageCoreTier1 extends TileEntityPlasmaStorageCore 
{

	public TileEntityPlasmaStorageCoreTier1() 
	{
		super(FPTileEntitys.PLASMA_CORE_T1, EnumPlasmaTiers.Tier1);
	}

}
