package futurepack.common.block.logistic.plasma;

import java.util.Random;
import java.util.function.Supplier;

import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockPlasma2NeonT0 extends BlockPlasmaConverter
{
	public static final BooleanProperty LIT = RedstoneLampBlock.LIT;
	

	public BlockPlasma2NeonT0(Properties properties, Supplier<TileEntity> sup)
	{
		super(properties, sup);
	}

	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.SOLAR_PANEL.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}
	
	protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) 
	{
		super.createBlockStateDefinition(builder);
		builder.add(LIT);
	}
	
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random rand)
	{
		super.tick(state, w, pos, rand);
		w.setBlockAndUpdate(pos, state.setValue(LIT, ((TileEntityPlasma2NeonT0)w.getBlockEntity(pos)).plasma.get()>0));
	}
}
