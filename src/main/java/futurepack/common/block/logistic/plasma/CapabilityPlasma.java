package futurepack.common.block.logistic.plasma;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.LongNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.util.INBTSerializable;

public class CapabilityPlasma implements IPlasmaEnergyStorage, INBTSerializable<CompoundNBT>
{
	@CapabilityInject(IPlasmaEnergyStorage.class)
	public static final Capability<IPlasmaEnergyStorage> cap_PLASMA = null;
	
	public static class Storage implements IStorage<IPlasmaEnergyStorage>
	{

		@Override
		public INBT writeNBT(Capability<IPlasmaEnergyStorage> capability, IPlasmaEnergyStorage instance, Direction side)
		{
			return LongNBT.valueOf(instance.get());
		}

		@Override
		public void readNBT(Capability<IPlasmaEnergyStorage> capability, IPlasmaEnergyStorage instance, Direction side, INBT nbt)
		{
			if(nbt instanceof LongNBT)
			{
				long power = ((LongNBT)nbt).getAsLong();
				
				if(power > instance.get())
				{
					instance.add(power - instance.get());
				}
				else if(power < instance.get())
				{
					instance.use(instance.get() - power);
				}
			}
		}

	}
	
	public static final IPlasmaEnergyStorage EMPTY = new IPlasmaEnergyStorage()
	{
		@Override
		public long use(long used) 
		{
			return 0;
		}
		
		@Override
		public long getMax() 
		{
			return 0;
		}
		
		@Override
		public long get() 
		{
			return 0;
		}
		
		@Override
		public boolean canTransferTo(IPlasmaEnergyStorage other) 
		{
			return false;
		}
		
		@Override
		public boolean canAcceptFrom(IPlasmaEnergyStorage other) 
		{
			return false;
		}
		
		@Override
		public long add(long added) 
		{
			return 0;
		}
	};
	
	protected long energy = 0;
	private final long maxenergy;
	private final boolean in, out;

	public CapabilityPlasma()
	{
		this(Short.MAX_VALUE, true, true);
	}
	
	public CapabilityPlasma(long maxpower, boolean in, boolean out)
	{
		this.maxenergy = maxpower;
		this.energy = 0;
		this.in = in;
		this.out = out;
	}


	@Override
	public boolean canTransferTo(IPlasmaEnergyStorage other)
	{
		return out;
	}

	@Override
	public boolean canAcceptFrom(IPlasmaEnergyStorage other)
	{
		return in;
	}

	@Override
	public CompoundNBT serializeNBT()
	{
		CompoundNBT nbt = new CompoundNBT();
		nbt.putLong("plasma", get());
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt)
	{
		this.energy = nbt.getLong("plasma");
	}

	@Override
	public long get()
	{
		return energy;
	}

	@Override
	public long getMax()
	{
		return maxenergy;
	}

	@Override
	public long use(long used)
	{
		if(energy >= used)
		{
			energy -= used;
			return used;
		}
		else
		{
			long p = energy;
			energy = 0;
			return p;
		}
	}

	@Override
	public long add(long added)
	{
		if(energy + added <= getMax())
		{
			energy += added;
			return added; 
		}
		else
		{
			long p = getMax() - energy;
			energy = getMax();
			return p;
		}
	}

	public void set(long e)
	{
		energy = e;
	}
}
