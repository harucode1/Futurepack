package futurepack.common.block.logistic.plasma;

import futurepack.common.block.BlockRotateableTile;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityPlasmaStorageOutlet extends TileEntityPlasmaStorageInlet implements ITickableTileEntity
{
	
	public TileEntityPlasmaStorageOutlet(TileEntityType<?> tileEntityTypeIn, long tranferRate) 
	{
		super(tileEntityTypeIn, tranferRate);
	}

	private LazyOptional<IPlasmaEnergyStorage> opt = null;
	
	@Override
	public void tick() 
	{
		if(opt == null)
		{
			Direction face = getBlockState().getValue(BlockRotateableTile.FACING);
			TileEntity tile = level.getBlockEntity(worldPosition.relative(face));
			if(tile!=null)
			{
				opt = tile.getCapability(CapabilityPlasma.cap_PLASMA, face.getOpposite());
				opt.addListener(p -> opt = null);
			}
		}
		else
		{
			opt.ifPresent(this::transfer);
		}
	}
	
	private void transfer(IPlasmaEnergyStorage storage)
	{
		getCorePlasma().ifPresent(core -> {
			if(storage.canAcceptFrom(core))
			{
				core.use(storage.add(Math.min(tranferRate, core.get())));
			}
		});
		
	}
	

}
