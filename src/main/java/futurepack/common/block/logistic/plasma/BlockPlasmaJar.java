package futurepack.common.block.logistic.plasma;

import java.util.Random;
import java.util.function.Supplier;

import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.particles.RedstoneParticleData;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockPlasmaJar extends BlockPlasmaTransferPipe
{
	public static final BooleanProperty LIT = RedstoneLampBlock.LIT;
	
	private VoxelShape up    = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 0, 0);
	private VoxelShape down  = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 180, 0);
	private VoxelShape north = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 90, 0);
	private VoxelShape south = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 90, 180);
	private VoxelShape west  = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 90, 90);
	private VoxelShape east  = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 90, 270);
	
	
	public BlockPlasmaJar(Properties properties, Supplier<TileEntity> sup)
	{
		super(properties, sup);
	}

	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random rand)
	{
		super.tick(state, w, pos, rand);
		w.setBlockAndUpdate(pos, state.setValue(LIT, ((TileEntityPlasmaTransporter)w.getBlockEntity(pos)).plasma.get()>0));
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		switch (state.getValue(FACING))
		{
		case UP:return up;
		case DOWN:return down;
		case NORTH:return north;
		case SOUTH: return south;
		case WEST:return west;
		case EAST: return east;
		default:
			return super.getShape(state, worldIn, pos, context);
		}
	}
	
	@Override
	public void animateTick(BlockState state, World w, BlockPos pos, Random rand)
	{
		super.animateTick(state, w, pos, rand);
		if(state.getValue(LIT))
		{
			int r = rand.nextInt(100);
			if(r < 5)
				w.addAlwaysVisibleParticle(ParticleTypes.FLASH, pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, 0, 0, 0);
			else if (r < 20)
				w.addAlwaysVisibleParticle(new RedstoneParticleData(1.0F, 0.5F, 0.0F, 1.0F), pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, 0.0, 0.0, 0.0);
		}
	}
	
	protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) 
	{
		super.createBlockStateDefinition(builder);
		builder.add(LIT);
	}
}
