package futurepack.common.block.logistic;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.depend.api.helper.HelperItemFilter;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

public class FilteredWrapper implements IItemHandler
{
	private final IItemHandler base;
	private final IItemFilter insert;
	
	public FilteredWrapper(@Nonnull IItemHandler base, @Nullable IItemFilter insert)
	{
		super();
		if(base == null)
			throw new NullPointerException("IItemHandler was null");
		this.base = base;
		this.insert = insert;
	}

	@Override
	public int getSlots()
	{
		return base.getSlots();
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return base.getStackInSlot(slot);
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if(insert == null)
		{
			return base.insertItem(slot, stack, simulate);
		}
		else
		{
			if(insert.test(stack))
			{
				ItemStack nottranfered =  base.insertItem(slot, stack, simulate);
				HelperItemFilter.tranfer(simulate, stack, nottranfered, insert);
				return nottranfered;
			}
			else
			{
				return stack;
			}
		}
	}
	
	@Override
	public boolean isItemValid(int slot, ItemStack stack)
	{
		if(insert!=null)
		{
			return insert.test(stack) && base.isItemValid(slot, stack);
		}
		return base.isItemValid(slot, stack);
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		return base.extractItem(slot, amount, simulate);
	}

	@Override
	public int getSlotLimit(int slot)
	{
		return base.getSlotLimit(slot);
	}
	
}
