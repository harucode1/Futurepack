package futurepack.common.block.logistic;

import java.util.List;

import javax.annotation.Nullable;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.util.LazyOptional;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockPipeNeon extends BlockPipeBase 
{

	protected BlockPipeNeon(Properties props) 
	{
		super(props);
	}

	@Override
	public boolean hasSpecial() 
	{
		return true;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityPipeNeon();
	}

	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.futurepack.block.conduct.neon", 500));
	}
	
	@Override
	protected EnumSide getAdditionalConnections(TileEntityPipeBase pipe, @Nullable TileEntity otherTile, Direction face) {
		
		if(otherTile != null)
		{
			LazyOptional<INeonEnergyStorage> neonOpt = otherTile.getCapability(CapabilityNeon.cap_NEON, face.getOpposite());
			if(neonOpt.isPresent())
			{
				INeonEnergyStorage p = neonOpt.orElseThrow(NullPointerException::new);
				if(p.getType()!=EnumEnergyMode.NONE)
				{	
					return EnumSide.CABLE;//CABLE because when this is called there is not an inventory to connect to just neon
				}	
			}
		}
		
		return EnumSide.OFF;
	}
	
}
