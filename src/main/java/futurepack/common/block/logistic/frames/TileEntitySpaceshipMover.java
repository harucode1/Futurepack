package futurepack.common.block.logistic.frames;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.spaceships.moving.SpaceShipSelecterUtil;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3i;

public class TileEntitySpaceshipMover extends TileEntity implements ITickableTileEntity
{

	private boolean redstone = true;
	
	public TileEntitySpaceshipMover(TileEntityType<? extends TileEntitySpaceshipMover> type) 
	{
		super(type);
	}
	
	public TileEntitySpaceshipMover() 
	{
		this(FPTileEntitys.SPACESHIP_MOVER);
	}
	
	@Override
	public void load(BlockState p_230337_1_, CompoundNBT nbt) 
	{
		super.load(p_230337_1_, nbt);
		redstone = nbt.getBoolean("redstone");
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt) 
	{
		nbt.putBoolean("redstone", redstone);
		return super.save(nbt);
	}
	

	@Override
	public void tick() 
	{
		if(!level.isClientSide)
		{
			boolean now = level.getBestNeighborSignal(worldPosition)>0;
			if(now && !redstone)
			{
				redstone = now;
				move();
			}
			redstone = now;
		}
	}
	
	public void move()
	{
		BlockPos pos = this.getBlockPos();
		Vector3i direction = this.getBlockState().getValue(BlockRotateableTile.FACING).getOpposite().getNormal();
		
		TileEntityMovingBlocks mb = SpaceShipSelecterUtil.moveSpaceship(getLevel(), pos, direction);
//		mb.maxticks = mb.ticks = 20 * 10;
	}

}
