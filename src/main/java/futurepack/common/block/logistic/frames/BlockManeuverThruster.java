package futurepack.common.block.logistic.frames;

import java.util.Random;

import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockManeuverThruster extends BlockRotateableTile 
{
	public static final BooleanProperty powered = BlockStateProperties.POWERED;

	public BlockManeuverThruster(Properties props) 
	{
		super(props);
		registerDefaultState(this.stateDefinition.any().setValue(powered, false));
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntitySpaceshipMover();
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(powered);
		super.createBlockStateDefinition(builder);
	}
	
	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) 
	{
		super.neighborChanged(state, w, pos, blockIn, fromPos, isMoving);
		boolean bb = w.getBestNeighborSignal(pos) > 0;
		w.setBlock(pos, state.setValue(powered, bb), 3);
	}
	
	@Override
	public void animateTick(BlockState state, World w, BlockPos pos, Random rand)
	{
		if(state.getValue(powered))
		{
			Direction f = state.getValue(FACING);
			IParticleData red = ParticleTypes.SMOKE;
			w.addParticle(red, pos.getX()+w.random.nextFloat(), pos.getY()+w.random.nextFloat(), pos.getZ()+w.random.nextFloat(), f.getStepX()/2D*w.random.nextFloat(),f.getStepY()/2D*w.random.nextFloat(),f.getStepZ()/2D*w.random.nextFloat());
	        w.addParticle(red, pos.getX()+w.random.nextFloat(), pos.getY()+w.random.nextFloat(), pos.getZ()+w.random.nextFloat(), f.getStepX()/2D*w.random.nextFloat(),f.getStepY()/2D*w.random.nextFloat(),f.getStepZ()/2D*w.random.nextFloat());
		
		}
		super.animateTick(state, w, pos, rand);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		return super.getStateForPlacement(context);
	}

}
