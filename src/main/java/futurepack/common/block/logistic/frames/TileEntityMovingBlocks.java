package futurepack.common.block.logistic.frames;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import futurepack.common.FPTileEntitys;
import futurepack.common.spaceships.moving.MovingBlocktUtil;
import futurepack.common.spaceships.moving.SpaceShipSelecterUtil;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.MiniWorld;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.NextTickListEntry;
import net.minecraft.world.TickPriority;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerTickList;
import net.minecraftforge.registries.ForgeRegistries;

public class TileEntityMovingBlocks extends TileEntityWithMiniWorldBase implements ITickableTileEntity
{

	public TileEntityMovingBlocks(TileEntityType<TileEntityMovingBlocks> type)
	{
		super(type);
	}
	
	public TileEntityMovingBlocks()
	{
		super(FPTileEntitys.MOVING_BLOCKS);
	}

	private Vector3i direction;
	private List<NextTickListEntry<Block>> pendingTicks = null;
	private ListNBT pending = null;

	@Override
	public void tick()
	{
		if(level.isClientSide)
		{
			this.level.addParticle(ParticleTypes.POOF, true, this.worldPosition.getX() + 0.5,  this.worldPosition.getY() + 0.5,  this.worldPosition.getZ() + 0.5, 0D, 0D, 0D);
			
			
		}
		
		if(!level.isClientSide() && ticks +(maxticks/10) == maxticks)
		{
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
			System.out.println("resending");
		}
		
		ticks--;
//		ticks = 15;
		if(!level.isClientSide)
		{
	//		BlockPos start = w.start.add(w.rotationpoint.xCoord, w.rotationpoint.yCoord, w.rotationpoint.zCoord);
			if(w==null || ticks<=0)
			{			
				level.removeBlock(worldPosition, false);
				
				MovingBlocktUtil.endMoveBlocks(getLevel(), getMiniWorld(), direction, pendingTicks);
			}
		}
		
	}	
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		nbt.putByteArray("direction", new byte[] {(byte) direction.getX(), (byte) direction.getY(), (byte) direction.getZ()});
		
		ListNBT pending = saveTickList(ForgeRegistries.BLOCKS::getKey, pendingTicks, this.getLevel().getGameTime());
		nbt.put("pendingBlockTicks", pending);
		
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		byte[] d = nbt.getByteArray("direction");
		direction = new Vector3i(d[0], d[1], d[2]);
		
		ListNBT listnbt = nbt.getList("pendingBlockTicks", nbt.getId());
		if(this.level!=null)
			setPendingTicks(loadTickList(ForgeRegistries.BLOCKS::getValue, listnbt, this.level.getGameTime()));
		else
			pending = listnbt;
		
		super.load(state, nbt);
	}
	
	public static <T> ListNBT saveTickList(Function<T, ResourceLocation> p_219502_0_, Iterable<NextTickListEntry<T>> p_219502_1_, long gameTime) 
	{
		ListNBT listnbt = new ListNBT();
		
		for(NextTickListEntry<T> nextticklistentry : p_219502_1_) 
		{
			CompoundNBT compoundnbt = new CompoundNBT();
			compoundnbt.putString("i", p_219502_0_.apply(nextticklistentry.getType()).toString());
			compoundnbt.putInt("x", nextticklistentry.pos.getX());
			compoundnbt.putInt("y", nextticklistentry.pos.getY());
			compoundnbt.putInt("z", nextticklistentry.pos.getZ());
			compoundnbt.putInt("t", (int)(nextticklistentry.triggerTick - gameTime));
			compoundnbt.putInt("p", nextticklistentry.priority.getValue());
			listnbt.add(compoundnbt);
		}

		return listnbt;
	}
	
	public static <T> List<NextTickListEntry<T>> loadTickList(Function<ResourceLocation, T> p_219502_0_, ListNBT listnbt, long gameTime) 
	{
		return listnbt.stream().map(n -> (CompoundNBT)n).map(nbt -> {
			BlockPos pos = new BlockPos(nbt.getInt("x"), nbt.getInt("y"), nbt.getInt("z"));
			NextTickListEntry<T> e = new NextTickListEntry<>(pos, p_219502_0_.apply(new ResourceLocation(nbt.getString("i"))), nbt.getInt("t") + gameTime, TickPriority.byValue(nbt.getInt("p")));
			return e;
		}).collect(Collectors.toList());

	}
	
	@Override
	public void setLevelAndPosition(World worldIn, BlockPos pos) 
	{
		super.setLevelAndPosition(worldIn, pos);
		if(pending!=null)
		{
			setPendingTicks(loadTickList(ForgeRegistries.BLOCKS::getValue, pending, worldIn.getGameTime()));
		}
	}
	

	
	@Override
	public CompoundNBT getUpdateTag()
	{
		CompoundNBT nbt = super.getUpdateTag();
		nbt.putByteArray("direction", new byte[] {(byte) direction.getX(), (byte) direction.getY(), (byte) direction.getZ()});
		return nbt;
	}
	
	@Override
	public void setMiniWorld(MiniWorld w)
	{
		this.w = w;
		w.rotationpoint = Vector3d.atLowerCornerOf(this.getBlockPos()).subtract(Vector3d.atLowerCornerOf(w.start)).add(0.5, 0, 0.5);
		if(w.face==null)
			w.face = Direction.UP;
	
		maxticks = ticks = Math.max(10, w.depth * w.height * w.width / 500);
	}
	
	public void setDirection(Vector3i dir)
	{
		this.direction =  dir;
		maxticks = ticks = maxticks * ( Math.abs(dir.getX()) + Math.abs(dir.getY()) + Math.abs(dir.getZ()) ) ;
	}
	
	public Vector3i getDirection()
	{
		return direction;
	}

	public void setPendingTicks(List<NextTickListEntry<Block>> pendingTicks) 
	{
		this.pendingTicks = pendingTicks;
	}
}
