package futurepack.common.block.logistic.frames;

import net.minecraft.block.BarrierBlock;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public class BlockMovingBlocks extends BarrierBlock 
{

	public BlockMovingBlocks(Properties p_i48447_1_) 
	{
		super(p_i48447_1_);
	}
	
	@Override
	public boolean hasTileEntity(BlockState state) 
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityMovingBlocks();
	}
	
}
