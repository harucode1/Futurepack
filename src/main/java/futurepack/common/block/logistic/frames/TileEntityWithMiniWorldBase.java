package futurepack.common.block.logistic.frames;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

import futurepack.common.block.misc.TileEntityFallingTree;
import futurepack.depend.api.MiniWorld;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.World;

public class TileEntityWithMiniWorldBase extends TileEntity 
{

	public TileEntityWithMiniWorldBase(TileEntityType<? extends TileEntityWithMiniWorldBase> p_i48289_1_) 
	{
		super(p_i48289_1_);
	}
	
	public static String getSaveString(CompoundNBT nbt)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try
		{
			CompressedStreamTools.writeCompressed(nbt, out);
			out.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		String base = Base64.getEncoder().encodeToString(out.toByteArray());
		return base;
	}
	
	public static CompoundNBT fromSaveString(String s)
	{		
		ByteArrayInputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(s));
		try
		{
			CompoundNBT tag = CompressedStreamTools.readCompressed(in);
			in.close();
			return tag;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public int ticks = 1200;
	public int maxticks = 1200;
	

	protected World ww;
	protected MiniWorld w;

	protected CompoundNBT storedMiniWorld = null;
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		if(w!=null)
		{
			CompoundNBT tag = w.serializeNBT();
//			tag.remove("light");
			tag.remove("red");
			nbt.putString("mini", getSaveString(tag));
		}		
		
		nbt.putInt("ticks", ticks);
		
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
//		System.out.println("loaded " + nbt.getAsString());
		CompoundNBT tag = fromSaveString(nbt.getString("mini"));
		if(tag!=null && (level!=null || ww!=null))
			setMiniWorld(new MiniWorld(level != null ? level : ww, tag));
		else if(tag!=null)
		{
			this.storedMiniWorld = tag;
		}
		ticks = nbt.getInt("ticks");
		maxticks = nbt.getInt("maxticks");

		super.load(state, nbt);
	}
	

	
	@Override
	public CompoundNBT getUpdateTag()
	{
		CompoundNBT nbt = super.getUpdateTag();
		if(w!=null)
		{
			CompoundNBT tag = w.serializeNBT();
			tag.remove("red");
			nbt.putString("mini", TileEntityFallingTree.getSaveString(tag));
		}
		nbt.putInt("ticks", ticks);
		nbt.putInt("maxticks", maxticks);
		return nbt;
	}
	
	@Override
	public double getViewDistance() 
	{
		return Math.max(super.getViewDistance(), w!=null ? Math.max(w.height, Math.max(w.depth, w.width)) * 1.5D : 0);
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() 
	{
		return INFINITE_EXTENT_AABB;
	}
	
	@Override
	public void setLevelAndPosition(World worldIn, BlockPos pos)
	{
//		System.out.println("Setting level and pos at " + pos + " state:" + worldIn.getBlockState(pos));
		ww = worldIn;
		
		if(storedMiniWorld!=null)
		{
			setMiniWorld(new MiniWorld(worldIn, storedMiniWorld));
		}
		super.setLevelAndPosition(worldIn, pos);
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, getUpdateTag());
		return pack;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		if(level.isClientSide)
		{
			load(getBlockState(), pkt.getTag());
		}
	}
	
	public MiniWorld getMiniWorld()
	{
		return w;
	}
	
	public void setMiniWorld(MiniWorld w)
	{
		this.w = w;
	}
	
}
