package futurepack.common.block.logistic;

import futurepack.common.block.BlockHologram;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IBucketPickupHandler;
import net.minecraft.block.ILiquidContainer;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;

public abstract class BlockWaterLoggedHologram extends BlockHologram implements IBucketPickupHandler, ILiquidContainer, IWaterLoggable{

	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
	  
	public BlockWaterLoggedHologram(Block.Properties builder) 
	{
		super(builder);
	}
	
	public BlockWaterLoggedHologram(Block.Properties builder, boolean hasNBTCustomDrops) 
	{
		super(builder, hasNBTCustomDrops);
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(WATERLOGGED);
	}

	@Override
	public Fluid takeLiquid(IWorld worldIn, BlockPos pos, BlockState state)
	{
		if (state.getValue(WATERLOGGED)) 
		{
			worldIn.setBlock(pos, state.setValue(WATERLOGGED, Boolean.valueOf(false)), 3);
			return Fluids.WATER;
		}
		else
		{
			return Fluids.EMPTY;
		}
	}
	
	
	
	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}
	
	@Override
	public boolean canPlaceLiquid(IBlockReader worldIn, BlockPos pos, BlockState state, Fluid fluidIn)
	{
	      return fluidIn == Fluids.WATER;
	}
	
	@Override
	public boolean placeLiquid(IWorld worldIn, BlockPos pos, BlockState state, FluidState fluidStateIn)
	{
		if (!state.getValue(WATERLOGGED) && fluidStateIn.getType() == Fluids.WATER) 
		{
			if (!worldIn.isClientSide()) 
			{
				worldIn.setBlock(pos, state.setValue(WATERLOGGED, true), 3);
				worldIn.getLiquidTicks().scheduleTick(pos, fluidStateIn.getType(), fluidStateIn.getType().getTickDelay(worldIn));
			}
			return true;
		}
		else 
		{
			return false;
		}
	}
}
