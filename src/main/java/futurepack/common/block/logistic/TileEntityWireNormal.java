package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;

public class TileEntityWireNormal extends TileEntityWireBase 
{

	public TileEntityWireNormal() 
	{
		super(FPTileEntitys.WIRE_NORMAL);
	}

	@Override
	public int getMaxEnergy() 
	{
		return 500;
	}

}
