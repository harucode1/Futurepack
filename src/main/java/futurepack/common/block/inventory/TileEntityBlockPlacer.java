package futurepack.common.block.inventory;

import java.util.ArrayList;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.item.CraftingItems;
import futurepack.common.player.FakePlayerFactory;
import futurepack.common.player.FuturepackPlayer;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

public class TileEntityBlockPlacer extends TileEntityInventoryBase implements ITickableTileEntity
{
	private boolean last;
	private IItemHandler wrapper;
	private LazyOptional<IItemHandler> handler;
	
	private int offset;
	
	public static final int SLOT_RANGE = 9;
	
	public TileEntityBlockPlacer() 
	{
		super(FPTileEntitys.BLOCK_PLACER);
		
		wrapper = new InvWrapper(this);
	}
	
	@Override
	public void tick()
	{
		boolean now = level.getBestNeighborSignal(worldPosition)>0;
		if(now && !last)
		{
			ArrayList<Integer> list = new ArrayList<Integer>(9);
			for(int i=0;i<9;i++)
			{
				if(!items.get(i).isEmpty())
				{
					list.add(i);
				}
			}
			if(list.size()>0)
			{
				int i = level.random.nextInt(list.size());
				int slot = list.get(i);
				items.set(slot, useItem(items.get(slot)));
				level.updateNeighbourForOutputSignal(worldPosition, getBlockState().getBlock());
				if(!items.get(slot).isEmpty() && items.get(slot).getCount()<=0)
				{
					items.set(slot, ItemStack.EMPTY);
				}
			}
					
		}
		last = now;
	}

	@Override
	protected int getInventorySize()
	{
		return 10;
	}

	
	
	private ItemStack useItem(ItemStack it)
	{
		if(level instanceof ServerWorld)
		{
			BlockState state = getBlockState();
			Direction face = state.getValue(BlockRotateableTile.FACING);
			BlockPos pos = this.getBlockPos().relative(face, 1+offset);
			FuturepackPlayer pl = FakePlayerFactory.INSTANCE.getPlayer((ServerWorld) level);
			pl.setItemInHand(Hand.MAIN_HAND, it); //this is important
			
			FakePlayerFactory.setupPlayer(pl, pos, face.getOpposite());
			ActionResult<ItemStack> ar = FakePlayerFactory.itemClick(level, pos, face, it, pl, Hand.MAIN_HAND);
			
			if(ar.getResult() == ActionResultType.PASS || ar.getResult() == ActionResultType.FAIL)
			{
				offset++;
				offset %= getItem(SLOT_RANGE).isEmpty() ? 1 : (1 + getItem(SLOT_RANGE).getCount());
			}
			return ar.getObject();
			
		}
		return it;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return LazyOptional.empty();
		
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(handler!=null)
				return (LazyOptional<T>) handler;
			else
			{
				handler = LazyOptional.of(() -> wrapper);
				handler.addListener(h -> handler = null);
				return (LazyOptional<T>) handler;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public boolean canPlaceItem(int slot, ItemStack it)
	{
		if(slot == SLOT_RANGE)
		{
			return it.getItem() == CraftingItems.upgrade_range;
		}
		
		return super.canPlaceItem(slot, it);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(handler);
		super.setRemoved();
	}
	
	@Override
	public String getGUITitle() 
	{
		return "block.futurepack.block_placer";
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		nbt.putInt("block_offset", offset);
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		offset = nbt.getInt("block_offset");
		super.load(state, nbt);
	}
}
