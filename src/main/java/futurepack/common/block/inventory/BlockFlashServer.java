package futurepack.common.block.inventory;

import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockFlashServer extends BlockRotateable
{
	public static final BooleanProperty slot1 = BooleanProperty.create("slot1");
	public static final BooleanProperty slot2 = BooleanProperty.create("slot2");
	public static final BooleanProperty slot3 = BooleanProperty.create("slot3");
	public static final BooleanProperty slot4 = BooleanProperty.create("slot4");
	
	public static VoxelShape shapeNorth = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,1,16,16,16}}, 0, 0);
	public static VoxelShape shapeEast = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,1,16,16,16}}, 0, 270);
	public static VoxelShape shapeSouth = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,1,16,16,16}}, 0, 180);
	public static VoxelShape shapeWest = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,1,16,16,16}}, 0, 90);
	
	public BlockFlashServer(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
		registerDefaultState(this.stateDefinition.any().setValue(slot1, false).setValue(slot2, false).setValue(slot3, false).setValue(slot4, false));
	}
	
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityFlashServer();
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		switch(state.getValue(HORIZONTAL_FACING))
		{
		case NORTH:
			return shapeNorth;
		case EAST:
			return shapeEast;
		case SOUTH:
			return shapeSouth;
		case WEST:
			return shapeWest;
		default:
			return super.getShape(state, worldIn, pos, context);
		}
	}

	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.FLASH_SERVER.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(slot1, slot2, slot3, slot4);
	}	
}
