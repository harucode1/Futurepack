package futurepack.common.block.inventory;

import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockFermentationBarrel extends BlockRotateable
{
	public static final IntegerProperty FILL = IntegerProperty.create("fill", 0, 7);

	public BlockFermentationBarrel(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity pl, Hand handIn, BlockRayTraceResult hit) 
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.FERMENTATION_BARREL.openGui(pl, pos);;
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder) 
	{
		super.createBlockStateDefinition(builder);
		builder.add(FILL);
	}
	
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityFermentationBarrel();
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		Direction face = context.getHorizontalDirection();
		return super.getStateForPlacement(context).setValue(HORIZONTAL_FACING, face);
	}
	
}
