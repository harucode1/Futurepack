package futurepack.common.block.inventory;

import futurepack.common.block.BlockRotateable;
import futurepack.common.block.modification.TileEntityModulT1Calculation;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockModul extends BlockRotateable
{
	private final int mode;

	public BlockModul(Block.Properties props, int mode)
	{	
		super(props);
		this.mode = mode;
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			switch (mode)
			{
			case 1:
				FPGuiHandler.MODUL_1.openGui(pl, pos);
				break;
			case 2:
				FPGuiHandler.MODUL_2.openGui(pl, pos);
				break;
			case 3:
				FPGuiHandler.MODUL_3.openGui(pl, pos);
				break;
			case 4:
				FPGuiHandler.MODUL_1_CALC.openGui(pl, pos);
			default:
				break;
			}
		}
		return ActionResultType.SUCCESS;
	}
		
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		switch(mode)
		{
		case 1: return new TileEntityModulT1();
		case 2: return new TileEntityModulT2();
		case 3: return new TileEntityModulT3();
		case 4: return new TileEntityModulT1Calculation();
		default: return null;
		}
	}

}
