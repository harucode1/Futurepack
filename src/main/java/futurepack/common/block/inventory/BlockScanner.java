package futurepack.common.block.inventory;

import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class BlockScanner extends BlockRotateable
{
	public static final BooleanProperty ON = BooleanProperty.create("on");
	
	public BlockScanner(Block.Properties props)
	{
		super(props);
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(ON);
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, IWorld worldIn, BlockPos pos, BlockPos facingPos)
	{
		TileEntityScannerBlock scanner = (TileEntityScannerBlock) worldIn.getBlockEntity(pos);
		return state.setValue(ON, scanner.isOn());
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
//		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.BLOCK_SCANNER.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		 return new TileEntityScannerBlock();
	}
}
