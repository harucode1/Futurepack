package futurepack.common.block.inventory;

import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockSharedResearcher extends BlockForscher 
{
	public BlockSharedResearcher(Properties props) 
	{
		super(props);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntitySharedResearcher();
	}
}
