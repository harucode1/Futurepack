package futurepack.common.block.inventory;

import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockAdvancedBoardcomputer extends BlockBoardComputer
{
	
	
	public BlockAdvancedBoardcomputer(Properties props) 
	{
		super(props);
	}

//	@Override
//	public String getMetaName(int meta)
//	{
//		if(meta>=8)
//			meta=2;
//		else if(meta>=4)
//			meta=1;
//		else
//			meta=0;		
//		return "advanced_boardcomputer_"+meta;
//	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(w.isClientSide)
		{
			return ActionResultType.SUCCESS;
		}
			
		TileEntityBoardComputer comp = (TileEntityBoardComputer) w.getBlockEntity(pos);
		comp.searchForShip();
		
		if(HelperResearch.canOpen(pl, state))
		{
			if(comp.isOwner(pl))
			{
				FPGuiHandler gui = FPGuiHandler.BOARD_COMPUTER;
				if(this == InventoryBlocks.advanced_board_computer_s)
					gui = FPGuiHandler.BOARD_COMPUTER_BLACK;
				else if(this == InventoryBlocks.advanced_board_computer_g)
					gui = FPGuiHandler.BOARD_COMPUTER_LIGHT_GRAY;
				else if(this == InventoryBlocks.advanced_board_computer_w)
					gui = FPGuiHandler.BOARD_COMPUTER_WHITE;		
				gui.openGui(pl, pos);
			}
		}
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityAdvancedBoardComputer();
	};
}
