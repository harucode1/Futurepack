package futurepack.common.block.inventory;

import java.util.Random;

import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FurnaceBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockBrennstoffGenerator extends BlockRotateable
{
	private static final BooleanProperty LIT = FurnaceBlock.LIT;
	
	private static final VoxelShape[] shape = new VoxelShape[4];
	
	public  BlockBrennstoffGenerator(Block.Properties props)
	{
		super(props);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		return getShape(state.getValue(HORIZONTAL_FACING));
	}

	private VoxelShape getShape(Direction face)
	{
		if(shape[face.get2DDataValue()]!=null)
			return shape[face.get2DDataValue()]; 
		else
		{
			VoxelShape s = Block.box(0, 10, 0, 16, 13, 16);
			VoxelShape side, generator;
			switch (face) 
			{
			case NORTH:
				side = Block.box(13, 0, 0, 16, 11, 16);
				generator = Block.box(0, 0, 3, 13, 11, 13);
				break;
			case SOUTH:
				side = Block.box(0, 0, 0, 3, 11, 16);
				generator = Block.box(3, 0, 3, 16, 11, 13);
				break;
			case EAST:
				side = Block.box(0, 0, 13, 16, 11, 16);
				generator = Block.box(3, 0, 0, 13, 11, 13);
				break;
			case WEST:
				side = Block.box(0, 0, 0, 16, 11, 3);
				generator = Block.box(3, 0, 3, 13, 11, 16);
				break;
			default:
				side = null;
				generator = null;
			}
			
			return shape[face.get2DDataValue()] = VoxelShapes.or(s, VoxelShapes.or(side, generator));
		}
	}
	
	@Override
	public void animateTick(BlockState state, World w, BlockPos pos, Random rand)
	{
		if (state.getValue(LIT))
		{
			Direction i = state.getValue(HORIZONTAL_FACING);
			double x = pos.getX();
			double y = pos.getY() + 1.1D;
			double z = pos.getZ();
			double da = 0.6875D;
			double db = 0.15625D;

			switch (i)
			{
			case WEST:
				w.addParticle(ParticleTypes.SMOKE, x + 1D - db ,y , z + da, 0.0D, 0.0D, 0.0D);
				break;
			case NORTH:
				w.addParticle(ParticleTypes.SMOKE, x + 1D - da ,y , z + 1D - db, 0.0D, 0.0D, 0.0D);
				break;
			case EAST:
				w.addParticle(ParticleTypes.SMOKE, x + db ,y , z + 1D - da, 0.0D, 0.0D, 0.0D);
				break;
			case SOUTH:
				w.addParticle(ParticleTypes.SMOKE, x + da ,y , z + db, 0.0D, 0.0D, 0.0D);
				break;
			default:
				break;
			}
		}
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(LIT);
	}
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.T0_GENERATOR.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityBrennstoffGenerator();
	}
	

}
