package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ChestBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockCompositeChest extends ChestBlock
{
	protected BlockCompositeChest(Block.Properties props)
	{
		super(props, () -> {
		      return FPTileEntitys.COMPOSITE_CHEST;
		});
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if (w.isClientSide)
		{
			return ActionResultType.SUCCESS;
		}
		else
		{
			if(((ITileInventoryProvider)w.getBlockEntity(pos)).getInventory()!=null)
			{
				FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
				return ActionResultType.SUCCESS;
			}
			else
			{
				return ActionResultType.FAIL;
			}
		}
	}
	
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityCompositeChest();
	}
}
