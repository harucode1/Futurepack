package futurepack.common.block.inventory;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;


import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;

public class TileEntityDroneStation extends TileEntity  
{
	ItemContainer handler = new ItemContainer();

	public TileEntityDroneStation() 
	{
		super(FPTileEntitys.DRONE_STATION);
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("itemContainer", handler.serializeNBT());
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		handler.deserializeNBT(nbt.getCompound("itemContainer"));
	}
	
	private LazyOptional<IItemHandler> item_opt;
	private LazyOptional<INeonEnergyStorage> neon_opt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(item_opt!=null)
				return (LazyOptional<T>) item_opt;
			else
			{
				item_opt = LazyOptional.of(() -> handler);
				item_opt.addListener(p -> item_opt = null);
				return (LazyOptional<T>) item_opt;
			}
		}
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neon_opt!=null)
				return (LazyOptional<T>) neon_opt;
			else
			{
				neon_opt = LazyOptional.of(NeonWrapper::new);
				neon_opt.addListener(p -> neon_opt = null);
				return (LazyOptional<T>) neon_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(item_opt, neon_opt);
		super.setRemoved();
	}
	
	class ItemContainer extends ItemStackHandlerGuis
	{
		public ItemContainer()
		{
			super(36);
		}
			
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			if(slot>26 && !gui)
				return stack;
			else
			{
				return super.insertItem(slot, stack, simulate, gui);
			}
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			if(slot >26 && !gui)
				return ItemStack.EMPTY;
			else
				return super.extractItem(slot, amount, simulate, gui);
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			if(slot <= (gui?36:26))
			{
				if(slot>26)
				{
					return stack.getItem() instanceof IItemNeon;
				}
				return true;
			}
			return false;
		}
	}

	public IItemHandler getGui()
	{
		return new ItemHandlerGuiOverride(this.handler);
	}
	
	public class NeonWrapper implements INeonEnergyStorage
	{

		@Override
		public int get()
		{
			int p = 0;
			for(int i=27;i<handler.getSlots();i++)
			{
				ItemStack it  = handler.getStackInSlot(i);
				if(it != null && it.getItem() instanceof IItemNeon)
				{
					IItemNeon ch = (IItemNeon) it.getItem();
					p += ch.getNeon(it);
				}
			}
			return p;
		}

		@Override
		public int getMax()
		{
			int m = 0;
			for(int i=27;i<handler.getSlots();i++)
			{
				ItemStack it  = handler.getStackInSlot(i);
				if(it != null && it.getItem() instanceof IItemNeon)
				{
					IItemNeon ch = (IItemNeon) it.getItem();
					m += ch.getMaxNeon(it);
				}
			}
			return m;
		}

		@Override
		public int use(int e)
		{
			int used = 0;

			for(int i=27;i<handler.getSlots();i++)
			{
				ItemStack it  = handler.getStackInSlot(i);;
				if(it != null && it.getItem() instanceof IItemNeon)
				{
					IItemNeon ch = (IItemNeon) it.getItem();
					int red = Math.min(e, ch.getNeon(it));
					e -= red;
					used += red;
					ch.addNeon(it, -red);
				}
				if(e <= 0)
					break;
			}
			return used;
		}

		@Override
		public int add(int e)
		{
			int added = 0;
			for(int i=27;i<handler.getSlots();i++)
			{
				ItemStack it  = handler.getStackInSlot(i);;
				if(it != null && it.getItem() instanceof IItemNeon)
				{
					IItemNeon ch = (IItemNeon) it.getItem();
					int red = Math.min(e, ch.getMaxNeon(it) - ch.getNeon(it));
					e -= red;
					added += red;
					ch.addNeon(it, red);
				}
				if(e <= 0)
					break;
			}
			return added;
		}

		@Override
		public boolean canAcceptFrom(INeonEnergyStorage other)
		{
			return get() < getMax();
		}

		@Override
		public boolean canTransferTo(INeonEnergyStorage other)
		{
			return false;
		}

		@Override
		public EnumEnergyMode getType()
		{
			return EnumEnergyMode.USE;
		}
		
	}
}
