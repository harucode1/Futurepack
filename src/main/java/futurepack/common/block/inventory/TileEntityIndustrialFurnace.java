package futurepack.common.block.inventory;

import java.util.UUID;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileWithOwner;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.logistic.LogisticFluidWrapper;
import futurepack.common.fluids.FPFluids;
import futurepack.common.recipes.industrialfurnace.FPIndustrialFurnaceManager;
import futurepack.common.recipes.industrialfurnace.IndRecipe;
import futurepack.common.research.CustomPlayerData;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperFluid;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;

public class TileEntityIndustrialFurnace extends TileEntityInventoryLogistics implements ITickableTileEntity, ITilePropertyStorage, ITileWithOwner
{
	public boolean[] uses = new boolean[3];
	public int burnTime = 0;
	public int progress = 0;
	private int wait=0;
	int obsiProgress=0;
	private UUID owner = new UUID(0, 0);
	//int time=0;
	private IFluidHandler tank = new Tank();
	private final LazyOptional<IFluidHandler>[] fluidTankOpt;
	
	@SuppressWarnings("unchecked")
	public TileEntityIndustrialFurnace()
	{
		super(FPTileEntitys.INDUSTRIAL_FURNACE);
		fluidTankOpt = new LazyOptional[6];
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.FLUIDS);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.FLUIDS);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ITEMS, EnumLogisticType.FLUIDS};
	}
	
	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.FLUIDS)
		{
			if(fluidTankOpt[face.get3DDataValue()]!=null)
			{
				fluidTankOpt[face.get3DDataValue()].invalidate();
				fluidTankOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	@Override
	public void tick() 
	{
		if(level.isClientSide)
		{
			return;
		}
		
		if(getBlockState().getBlock().isAir(getBlockState(), level, worldPosition))
		{
			return; //face palm
		}

		if(getBlockState().getValue(BlockIndFurnace.LIT) != progress>0)
		{
			level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockIndFurnace.LIT, progress>0));
			setChanged();
		}
		
		if(burnTime<=2000 && !level.isClientSide && !items.get(0).isEmpty())
		{
			LazyOptional<IFluidHandlerItem> opt = items.get(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY, null);
			opt.ifPresent(handler -> {
				FluidStack stack = handler.drain(3000-burnTime, FluidAction.SIMULATE);
				if(stack!=null)
				{
					if(stack.getFluid()==FPFluids.OLD_LAVA && stack.getAmount()>=1)
					{
						stack = handler.drain(3000-burnTime, FluidAction.EXECUTE);
						burnTime += stack.getAmount();
						items.set(0, handler.getContainer());
					}
				}
			});
		}
		
		if(wait>0)
		{
			wait--;
		}
		else
		{	
			uses = new boolean[3];
			if(burnTime>=10 && (items.get(7).isEmpty() || (items.get(7).getCount()<64 && items.get(7).getItem()==Blocks.OBSIDIAN.asItem())))
			{
				IndRecipe[] recipes = FPIndustrialFurnaceManager.instance.getMatchingRecipes(new ItemStack[]{items.get(1),items.get(2),items.get(3)}, level);
				uses = FPIndustrialFurnaceManager.instance.getAndClearUses();
				boolean up = false;
				boolean reset = false;
				int usedSlots = 0;
				for(IndRecipe recipe : recipes)
				{
					if(recipe==null)
					{
						continue;
					}
					ItemStack out = recipe.getOutput();
					
					ServerWorld ws = (ServerWorld) level;
					CustomPlayerData data = CustomPlayerData.getDataFromUUID(owner, ws.getServer());
					if(data!=null && !data.canProduce(out))
					{
						continue;
					}					
					
					int[] inputs = new int[]{1,2,3};
					ItemStack[] allSlots = new ItemStack[3];
					for(int i=0;i<3;i++)
					{
						allSlots[i] = items.get(inputs[(usedSlots +i)%3]);
					}
					
					if(recipe.match( allSlots, new boolean[3]) && !out.isEmpty())
					{
						int slot = -1;
						for(int i=4;i<7;i++)
						{
							if(items.get(i).isEmpty() || (out.sameItem(items.get(i)) && items.get(i).getCount()+out.getCount() <=64))
							{
								slot = i;
								break;
							}				
						}
						if(slot>0)
						{
							if(!up)	
							{
								progress++;
								up=true;
							}
							if(progress>=400 && !level.isClientSide)
							{
								reset=true;
								burnTime-=10;
								obsiProgress++;
								
								if(items.get(slot).isEmpty())
								{										
									if(recipe.use(allSlots))
									{
										items.set(slot, out);
										usedSlots++;
									}
								}
								else if(items.get(slot).getCount()+out.getCount() <=64)
								{								
									if(recipe.use(allSlots))
									{	
										items.get(slot).grow(out.getCount());
										usedSlots++;
									}
								}
								else
								{
									if(recipe.use(allSlots))
									{
										int n = 64 - items.get(slot).getCount();
										items.get(slot).setCount(64);	
										out.setCount(n);
										
										for(int i=4;i<7;i++)
										{
											if(items.get(i).isEmpty())
											{
												items.set(i, out);
												break;
											}
											else if (out.sameItem(items.get(i)) && items.get(i).getCount()+n <=64)
											{
												items.get(i).grow(out.getCount());
												break;
											}				
										}
										usedSlots++;
									}
								}
							}
							for(int i=0;i<items.size();i++)
							{
								if(!items.get(i).isEmpty() && items.get(i).getCount()<= 0)
									items.set(i, ItemStack.EMPTY);
							}						
						}	
					}
				}	
				if(reset)
					progress=0;
				
				if(recipes==null || recipes.length==0)
				{
					progress=0;
					wait=0;
				}
			}
		}
		if(obsiProgress>=100)
		{
			obsiProgress=0;
			if(items.get(7).isEmpty())
				items.set(7, new ItemStack(Blocks.OBSIDIAN));
			else if(items.get(7).getItem()==Blocks.OBSIDIAN.asItem())
				items.get(7).grow(1);
			
		}
	}
	
//	@Override
//	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) 
//	{
//		if(world.isRemote)
//		{
//			NBTTagCompound nbt = pkt.getNbtCompound();
//			if(nbt.getBoolean("data"))
//				readData(nbt);
//			else
//				read(nbt);
//		}
//	}
//	
//	@Override
//	public Packet getUpdatePacket() 
//	{
//		NBTTagCompound nbt = new NBTTagCompound();
//		write(nbt);
//		final SPacketUpdateTileEntity pack = new SPacketUpdateTileEntity(pos, getBlockMetadata(), nbt);
//		return pack;
//	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		writeData(nbt);
		nbt.putLong("uuid1", owner.getMostSignificantBits());
		nbt.putLong("uuid2", owner.getLeastSignificantBits());
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		readData(nbt);
		owner = new UUID(nbt.getLong("uuid1"), nbt.getLong("uuid2"));
	}
	
	public void writeData(CompoundNBT nbt)
	{
		nbt.putInt("burnTime", burnTime);
		nbt.putInt("progress", progress);
		nbt.putInt("obsiProgress", obsiProgress);
	}
	
	public void readData(CompoundNBT nbt)
	{
		burnTime = nbt.getInt("burnTime");
		progress = nbt.getInt("progress");
		obsiProgress = nbt.getInt("obsiProgress");
	}


	@Override
	public boolean stillValid(PlayerEntity var1)
	{
		return isOwner(var1);
	}

	@Override
	public boolean canPlaceItem(int var1, ItemStack it) 
	{
		if(var1==0)
		{
			return HelperFluid.isFluidInside(FPFluids.OLD_LAVA, it);
		}
		if(var1==7)
			return false;
		return true;
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1,2,3,4,5,6,7};
	}

	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack item, Direction side)
	{
		if(!storage.canInsert(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		
		if(slot>=0 && slot <= 3)
		{
			return canPlaceItem(slot, item);
		}
			
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack it, Direction side)
	{
		if(!storage.canExtract(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		
		if(slot>=4 && slot <= 7)
		{
			return true;
		}
		if(slot==0)
		{
			return !HelperFluid.isFluidInside(FPFluids.OLD_LAVA, it);
		}
		return false;
	}

	public int getBurn() 
	{
		return this.burnTime;
	}
	
	public float getProgress()
	{
		return this.progress / 400F;
	}
	
	
	public FluidStack getFluid() 
	{
		if(burnTime<=0)
			return FluidStack.EMPTY;
		
		return new FluidStack(FPFluids.OLD_LAVA, this.burnTime);
	}

	public int getFluidAmount() 
	{
		return this.burnTime;
	}

	public int getCapacity()
	{
		return 3000;
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return progress;
		case 1:
			return burnTime;
		case 2:
			return TileEntityScannerBlock.booleanToInt(uses);
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			progress = value;
			break;
		case 1:
			burnTime = value;
			break;
		case 2:
			uses = TileEntityScannerBlock.intToBool(uses, value);
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 3;
	}
	
	@Override
	public void clearContent() { }

	@Override
	protected int getInventorySize()
	{
		return 8;
	}

	@Override
	public void setOwner(PlayerEntity pl)
	{
		owner = pl.getGameProfile().getId();
	}

	@Override
	public boolean isOwner(PlayerEntity pl)
	{
		if(owner==null)
			return true;
		return isOwner(pl.getGameProfile().getId());
	}
	
	@Override
	public boolean isOwner(UUID pl)
	{
		if(owner==null)
			return true;
		return this.owner.equals(pl);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return LazyOptional.empty();
		
		if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			if(fluidTankOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) fluidTankOpt[facing.get3DDataValue()];
			}
			else
			{
				if(getLogisticStorage().getModeForFace(facing, EnumLogisticType.FLUIDS) == EnumLogisticIO.NONE)
				{
					return LazyOptional.empty();
				}
				else
				{
					fluidTankOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new LogisticFluidWrapper(getLogisticStorage().getInterfaceforSide(facing), tank));
					fluidTankOpt[facing.get3DDataValue()].addListener(p -> fluidTankOpt[facing.get3DDataValue()]=null);
					return (LazyOptional<T>) fluidTankOpt[facing.get3DDataValue()];
				}
			}
		}

		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidTankOpt);
		super.setRemoved();
	}
	
	private class Tank implements IFluidHandler
	{
		@Override
		public int fill(FluidStack resource, FluidAction doFill)
		{
			if (resource == null || resource.getFluid() != FPFluids.OLD_LAVA)
	        {
	            return 0;
	        }

	        if (doFill == FluidAction.SIMULATE)
	        {
	            if (getFluid() == null)
	            {
	                return Math.min(getCapacity(), resource.getAmount());
	            }

	            return Math.min(getCapacity() - getFluidAmount(), resource.getAmount());
	        }
	        
	        int filled = getCapacity() - getFluidAmount();
	        int amount =  Math.min(filled, resource.getAmount());
	        
	        burnTime+=amount;
	        return amount;
		}

		@Override
		public int getTanks() 
		{
			return 1;
		}

		@Override
		public FluidStack getFluidInTank(int tank) 
		{
			return new FluidStack(FPFluids.OLD_LAVA, burnTime);
		}

		@Override
		public int getTankCapacity(int tank) 
		{
			return getCapacity();
		}

		@Override
		public boolean isFluidValid(int tank, FluidStack stack) 
		{
			return tank == 0 && stack.getFluid() == FPFluids.OLD_LAVA;
		}
		
		@Override
		public FluidStack drain(FluidStack resource, FluidAction action) 
		{
			return FluidStack.EMPTY;
		}

		@Override
		public FluidStack drain(int maxDrain, FluidAction action) 
		{
			return FluidStack.EMPTY;
		}
	}
	
	@Override
	public String getGUITitle() {
		return "gui.futurepack.industrial_furnace.title";
	}
}
