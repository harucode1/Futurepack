package futurepack.common.block.inventory;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

public class BlockOptibenchCraftingModule extends BlockRotateableTile
{

	public BlockOptibenchCraftingModule(Block.Properties props)
	{
		super(props);
	}

	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityOptiBenchCraftingModule();
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		((TileEntityOptiBenchCraftingModule)w.getBlockEntity(pos)).onNeighborChange();
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.OPTI_BENCH_CRAFTING_MODULE.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public void onNeighborChange(BlockState state, IWorldReader w, BlockPos pos, BlockPos neighbor) 
	{
		((TileEntityOptiBenchCraftingModule)w.getBlockEntity(pos)).onNeighborChange();
		super.onNeighborChange(state, w, pos, neighbor);
		
	}
	
	@Override
	public void setPlacedBy(World w, BlockPos pos, BlockState state, LivingEntity liv, ItemStack it) 
	{
		super.setPlacedBy(w, pos, state, liv, it);
		((TileEntityOptiBenchCraftingModule)w.getBlockEntity(pos)).onNeighborChange();
	}
	
}
