package futurepack.common.block.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.filter.OrGateFilter;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperInventory.SlotContent;
import futurepack.depend.api.helper.HelperItemFilter;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class TileEntityTickingPusher extends TileEntityPusher implements ITickableTileEntity, ITileInventoryProvider
{
    private int cooldown;

    public TileEntityTickingPusher(TileEntityType<? extends TileEntityTickingPusher> type) 
	{
		super(type);
	}

    public TileEntityTickingPusher() 
	{
		super(FPTileEntitys.PUSHER_TICKING);
	}

    @Override
	public void tick() 
	{
    	
//    	hier sind bugs
		if(cooldown++ > 20)
		{
            cooldown = 0;
			neighborChanged(level, worldPosition, null, true);
		}
		
		pushOutItems();
	}
}
