package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;

public class TileEntityAdvancedBoardComputer extends TileEntityBoardComputer
{
	public TileEntityAdvancedBoardComputer() 
	{
		super(FPTileEntitys.ADVANCED_BOARD_COMPUTER);
	}
	
	@Override
	public boolean isAdvancedBoardComputer()
	{
		return true;
	}
	
	@Override
	public int getMaxNE()
	{
		return 1500;
	}
}
