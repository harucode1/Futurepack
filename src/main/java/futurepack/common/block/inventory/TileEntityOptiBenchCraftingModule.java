package futurepack.common.block.inventory;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.modification.machines.OptiBenchCraftingBase;
import futurepack.common.block.modification.machines.TileEntityOptiBench;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityOptiBenchCraftingModule extends TileEntityInventoryLogistics implements ITilePropertyStorage
{
    protected int progress = 0;//max 10

    private LazyOptional<OptiBenchCraftingBase> optCrafting;
    private BlockPos optOptiBench;
    private int cachedDistance = -1;

	private ArrayList<ItemStack> overfilling = new ArrayList<>();

	public TileEntityOptiBenchCraftingModule(TileEntityType<? extends TileEntityOptiBenchCraftingModule> t) 
	{
		super(t);
	}

	public TileEntityOptiBenchCraftingModule() 
	{
		super(FPTileEntitys.OPTIBENCH_CRAFTING_MODULE);
	}


	
	@Override
	protected int getInventorySize()
	{
		return 11;
	}


    @Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(optCrafting);
		super.setRemoved();
	}

    private LazyOptional<OptiBenchCraftingBase> getCrafting()
    {
        if(optCrafting == null)
        {
            optCrafting = LazyOptional.of(() -> new Crafting());
		    optCrafting.addListener(p -> optCrafting=null);
			return optCrafting;
        }
        else
        {
            return optCrafting;
        }
    }

	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
	}
	
	@Override
	public String getGUITitle() 
    {
		return "block.futurepack.optibench_crafting_module";
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		ListNBT l = new ListNBT();
		for(int i=0;i<overfilling.size();i++)
		{
			if(overfilling.get(i)!=null && !overfilling.get(i).isEmpty())
			{
				CompoundNBT tag = new CompoundNBT();
				overfilling.get(i).save(tag);
				l.add(tag);
			}
		}
		nbt.put("overfilling", l);
		 nbt.putInt("progress", progress);
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		overfilling.clear();
		ListNBT l = nbt.getList("overfilling", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundNBT tag = l.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			overfilling.add(is);
		}
		progress = nbt.getInt("progress");
		super.load(state, nbt);
	}
	
	
    public class Crafting extends OptiBenchCraftingBase
	{
		@Override
		protected ItemStack getBlueprintItem()
		{
			return items.get(10);
		}

		@Override
		protected NonNullList<ItemStack> getAllCraftingItems()
		{
			return items;
		}
		
		@Override
		protected int getCraftingSlots()
		{
			return 9;
		}
		
		@Override
		protected int getListOffset()
		{
			return 0;
		}
		
		@Override
		protected ItemStack getOutputSlot()
		{
			return items.get(9);
		}
		
		@Override
		protected void setOutputSlot(ItemStack stack)
		{
			setItem(9, stack);
		}

		@Override
		protected List<ItemStack> getOverfillingItems()
		{
			return overfilling;
		}

		@Override
		protected void setProgress(int progress)
		{
			TileEntityOptiBenchCraftingModule.this.progress = progress;
		}

		@Override
		protected int getProgress()
		{
			return TileEntityOptiBenchCraftingModule.this.progress;
		}

		@Override
		protected int getMaxProgress()
		{
			return 10;
		}
		
		@Override
		protected void markDirty()
		{
			TileEntityOptiBenchCraftingModule.this.setChanged();
		}
	}

    @Override
	public boolean canPlaceItem(int slot, ItemStack var2) 
	{
		if(slot>=0 && slot<=8 && !var2.isEmpty())
		{
			return getCrafting().orElse(null).isItemValidForCraftingSlot(slot, var2, level);
			
		}
		return slot != 9;
	}

    private Direction getInput()
	{
		BlockState state = getBlockState();
		if(!state.hasProperty(BlockRotateableTile.FACING))
		{
			return Direction.UP;
		}
		Direction face = state.getValue(BlockRotateableTile.FACING);
		return face;
	}

	public void onNeighborChange()
	{
        TileEntityOptiBench b = getOptiBench(0);
        if(b!=null)
        {
            b.registerCrafter(getCrafting());
        }
    }

    protected TileEntityOptiBench getOptiBench(int distance)
    {
        if(optOptiBench!=null)
        {
            TileEntity t = level.getBlockEntity(optOptiBench);
            if(t!=null && t instanceof TileEntityOptiBench)
            {
                return distance + cachedDistance <=6 ? (TileEntityOptiBench)t : null;
            }
            else
            {
                optOptiBench = null;
                cachedDistance = -1;
            }
        }

        if(distance <= 6)
        {
            TileEntity t = level.getBlockEntity(worldPosition.relative(getInput(), -1));
            if(t!=null)
            {
                if(t instanceof TileEntityOptiBench)
                {
                    optOptiBench = worldPosition.relative(getInput(), -1);
                    cachedDistance = 1;
                    return (TileEntityOptiBench)t;
                } 
                else if(t instanceof TileEntityOptiBenchCraftingModule)
                {
                    TileEntityOptiBench bench = ((TileEntityOptiBenchCraftingModule)t).getOptiBench(distance+1);
                    if(bench!=null)
                    {
                        optOptiBench = bench.getBlockPos();
                        cachedDistance = ((TileEntityOptiBenchCraftingModule)t).cachedDistance +1;
                        return bench;
                    }
                }
            }
        }

        return null;
    }

		@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:		
			return this.progress;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:		
			this.progress = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 1;
	}

	@Override
	public void setItem(int slot, ItemStack item) 
	{
		super.setItem(slot, item);
		if(optCrafting!=null)
		{
			optCrafting.ifPresent(p -> p.updateRecipe(TileEntityOptiBenchCraftingModule.this.level));
		}
	}

}
