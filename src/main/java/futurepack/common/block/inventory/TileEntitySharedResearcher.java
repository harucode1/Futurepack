package futurepack.common.block.inventory;

import futurepack.api.PacketBase;
import futurepack.common.FPTileEntitys;
import futurepack.common.network.FunkPacketResearchFinished;
import futurepack.common.network.NetworkManager;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import net.minecraft.tileentity.TileEntityType;

public class TileEntitySharedResearcher extends TileEntityForscher 
{
	public TileEntitySharedResearcher(TileEntityType<TileEntitySharedResearcher> type) 
	{
		super(type);
		//IDEE: neue variante die pakete sendet & empfängt wenn forschung fertig ist -> spieler können zusammen forschen;
	}
	
	public TileEntitySharedResearcher() 
	{
		this(FPTileEntitys.SHARED_RESEARCHER);
	}
	
	@Override
	protected void finishResearch() 
	{
		Research r = getResearch();
		if(r!=null)
		{
			NetworkManager.sendPacketThreaded(this, new FunkPacketResearchFinished(getSenderPosition(), this, r, getOwnerID()));
		}
		super.finishResearch();
	}
	
	@Override
	public void onFunkPacket(PacketBase pkt) 
	{
		super.onFunkPacket(pkt);
		if(pkt instanceof FunkPacketResearchFinished)
		{
			FunkPacketResearchFinished finished = (FunkPacketResearchFinished) pkt;
			CustomPlayerData cp = CustomPlayerData.getDataFromUUID(getOwnerID(), this.level.getServer());
			cp.addResearch(finished.getResearch());
		}
	}
}
