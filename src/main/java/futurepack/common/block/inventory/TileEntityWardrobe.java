package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.logistic.IInvWrapper;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.block.BlockState;
import net.minecraft.inventory.IInventory;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntityWardrobe extends TileEntity implements ITileInventoryProvider
{
	private ItemStackHandler itemHandler;
	private LazyOptional<IItemHandler> itemOpt;
	
	public TileEntityWardrobe(TileEntityType<? extends TileEntityWardrobe> type, int size)
	{
		super(type);
		itemHandler = new ItemStackHandler(size);
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		nbt.put("container", itemHandler.serializeNBT());
		
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		if(nbt.contains("container"))
		{
			itemHandler.deserializeNBT(nbt.getCompound("container"));
		}
			
		super.load(state, nbt);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemOpt!=null)
			{
				return (LazyOptional<T>) itemOpt;
			}
			else
			{
				itemOpt = LazyOptional.of( ()-> itemHandler);
				itemOpt.addListener(p -> itemOpt = null);
				return (LazyOptional<T>) itemOpt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public IInventory getInventory() 
	{
		return getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).map(IInvWrapper::new).orElse(null);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(itemOpt);
		super.setRemoved();
	}
	
	public static class Normal extends TileEntityWardrobe
	{
		public Normal() 
		{
			super(FPTileEntitys.WARDROBE_N, 27);
		}
	}
	
	public static class Large extends TileEntityWardrobe
	{
		public Large() 
		{
			super(FPTileEntitys.WARDROBE_L, 36);
		}
	}

	@Override
	public String getGUITitle() {
		return "gui.futurepack.wardrobe.title";
	}
}
