package futurepack.common.block.inventory;

import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockBoardComputer extends BlockRotateable
{

	public BlockBoardComputer(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		TileEntityBoardComputer comp = (TileEntityBoardComputer) w.getBlockEntity(pos);
		comp.searchForShip();
		
		if(HelperResearch.canOpen(pl, state))
		{
			if(comp.isOwner(pl))
			{
				FPGuiHandler.BOARD_COMPUTER.openGui(pl, pos);
			}
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityBoardComputer();
	};
	
}
