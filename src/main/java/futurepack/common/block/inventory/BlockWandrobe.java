package futurepack.common.block.inventory;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockWandrobe extends BlockHoldingTile
{
	public final boolean big;

	public BlockWandrobe(Block.Properties props, boolean big) 
	{
		super(props);
		this.big = big;
	}

	//big = meta==2||meta==3||meta==6||meta==7||meta==10||meta==11;
	
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(!w.isClientSide)
		{
			FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}
	
	

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return big ? new TileEntityWardrobe.Large() : new TileEntityWardrobe.Normal();
	}
	
	
}
