package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.LogisticStorage;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.items.CapabilityItemHandler;

public class TileEntityLogisticChest extends TileEntityInventoryLogistics implements ITickableTileEntity
{

	private int buffer;
	
	public TileEntityLogisticChest(TileEntityType<? extends TileEntityLogisticChest> t) 
	{
		super(t);
	}

	public TileEntityLogisticChest() 
	{
		this(FPTileEntitys.LOGISTIC_CHEST);
	}
	
	@Override
	protected int getInventorySize()
	{
		return 4 * 9;
	}


	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
	}

	@Override
	public void tick() 
	{
		if(level.isClientSide)
			return;
		
		buffer++;
		if( (buffer + worldPosition.getX() + worldPosition.getY() + worldPosition.getZ()) % 6 == 0)
		{
			for(Direction face : FacingUtil.VALUES)
			{	
				this.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face).ifPresent(inv -> {
					HelperInventory.tryExtractIntoInv(getLevel(), getBlockPos(), face, inv, 0F, false, 2);
				});
			}
			if((buffer + worldPosition.getX() + worldPosition.getY() + worldPosition.getZ()) % 24 == 0)
			{
				checkEmpty();
			}
		}
	}
	
	@Override
	public void stopOpen(PlayerEntity pl) 
	{
		super.stopOpen(pl);
		checkEmpty();
	}
	
	protected void checkEmpty()
	{
		boolean notEmpty = items.stream().anyMatch(p -> !p.isEmpty());
		if(getBlockState().getValue(BlockLogisticChest.FILLED) != notEmpty)
		{
			level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockLogisticChest.FILLED, notEmpty));
		}
	}
	
	@Override
	public String getGUITitle() {
		return "block.futurepack.logistic_chest";
	}
}
