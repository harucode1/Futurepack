package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.common.block.logistic.LogisticItemHandlerWrapper;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

public abstract class TileEntityInventoryLogistics extends TileEntityInventoryBase implements ISidedInventory
{

	private final LazyOptional<ILogisticInterface>[] logisticOpt;
	private final LazyOptional<IItemHandler>[] itemHandlerOpt;
	protected final LogisticStorage storage;
	
	@SuppressWarnings("unchecked")
	public TileEntityInventoryLogistics(TileEntityType<? extends TileEntityInventoryLogistics> tileEntityTypeIn) 
	{
		super(tileEntityTypeIn);
		
		logisticOpt = new LazyOptional[6];
		itemHandlerOpt = new LazyOptional[6];
		
		storage = new LogisticStorage(this::onLogisticChange, getLogisticTypes());
		configureLogisticStorage(storage);
	}
	
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.ITEMS)
		{
			if(itemHandlerOpt[face.get3DDataValue()]!=null)
			{
				itemHandlerOpt[face.get3DDataValue()].invalidate();
				itemHandlerOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ITEMS};
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		if(side==null)
			return LazyOptional.empty();
		
		if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemHandlerOpt[side.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) itemHandlerOpt[side.get3DDataValue()];
			}
			else
			{
				if(getLogisticStorage().getModeForFace(side, EnumLogisticType.ITEMS) == EnumLogisticIO.NONE)
				{
					return LazyOptional.empty();
				}
				else
				{
					itemHandlerOpt[side.get3DDataValue()] = LazyOptional.of(() -> new LogisticItemHandlerWrapper(getLogisticStorage().getInterfaceforSide(side), getItemHandler(side)));
					itemHandlerOpt[side.get3DDataValue()].addListener(p -> itemHandlerOpt[side.get3DDataValue()]=null);
					return (LazyOptional<T>) itemHandlerOpt[side.get3DDataValue()];
				}
			}
		}
		else if(cap == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logisticOpt[side.get3DDataValue()] != null)
			{
				return (LazyOptional<T>) logisticOpt[side.get3DDataValue()];
			}
			else
			{
				logisticOpt[side.get3DDataValue()] = LazyOptional.of(() -> getLogisticStorage().getInterfaceforSide(side));
				logisticOpt[side.get3DDataValue()].addListener(p -> logisticOpt[side.get3DDataValue()]=null);
				return (LazyOptional<T>) logisticOpt[side.get3DDataValue()];
			}
		}
		
		return super.getCapability(cap, side);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(itemHandlerOpt);
		HelperEnergyTransfer.invalidateCaps(logisticOpt);
		super.setRemoved();
	}


	@Override
	protected abstract int getInventorySize();
	
	public IItemHandler getItemHandler(Direction side)
	{
		return new SidedInvWrapper(this, side);
	}

	public LogisticStorage getLogisticStorage() 
	{
		return storage;
	}
	
	
	public abstract void configureLogisticStorage(LogisticStorage storage);
	

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		ListNBT l = new ListNBT();
		for(int i=0;i<items.size();i++)
		{
			if(!items.get(i).isEmpty())
			{
				CompoundNBT tag = new CompoundNBT();
				items.get(i).save(tag);
				tag.putInt("slot", i);
				l.add(tag);
			}
		}
		nbt.put("Items", l);
		storage.write(nbt);
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		items.clear();
		ListNBT l = nbt.getList("Items", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundNBT tag = l.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			setItem(tag.getInt("slot"), is);
		}
		storage.read(nbt);
	}

	private int[] allSlots;
	
	@Override
	public int[] getSlotsForFace(Direction side)
	{
		if(allSlots==null)
		{
			allSlots = new int[getContainerSize()];
			for(int i=0;i<allSlots.length;i++)
				allSlots[i] = i;
		}
		return allSlots;
	}
	
	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack it, Direction side)
	{
		return storage.canInsert(side, EnumLogisticType.ITEMS) && canPlaceItem(slot, it);
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{		
		return storage.canExtract(side, EnumLogisticType.ITEMS);
	}
}
