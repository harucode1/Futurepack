package futurepack.common.block.inventory;

import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.NonNullList;

public abstract class TileEntityInventoryBase extends TileEntity implements IInventory, ITileInventoryProvider
{
	protected NonNullList<ItemStack> items;
	
	public TileEntityInventoryBase(TileEntityType<?> tileEntityTypeIn)
	{
		super(tileEntityTypeIn);
		items = NonNullList.withSize(getInventorySize(), ItemStack.EMPTY);
	}

	protected abstract int getInventorySize();
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pack;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		if(level.isClientSide)
		{
			load(getBlockState(), pkt.getTag());
		}
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		ListNBT l = new ListNBT();
		for(int i=0;i<items.size();i++)
		{
			if(!items.get(i).isEmpty())
			{
				CompoundNBT tag = new CompoundNBT();
				items.get(i).save(tag);
				tag.putInt("slot", i);
				l.add(tag);
			}
		}
		nbt.put("Items", l);
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		items.clear();
		ListNBT l = nbt.getList("Items", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundNBT tag = l.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			setItem(tag.getInt("slot"), is);
		}
	}
	
	@Override
	public int getContainerSize() 
	{
		return items.size();
	}

	@Override
	public ItemStack getItem(int var1) 
	{
		if(var1<items.size())
		{
			return items.get(var1);
		}
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack removeItem(int slot, int amount)
    {
        if (this.items.get(slot) != null)
        {
            ItemStack itemstack;

            if (this.items.get(slot).getCount() <= amount)
            {
                itemstack = this.items.get(slot);
                this.items.set(slot, ItemStack.EMPTY);
                this.setChanged();
                return itemstack;
            }
            else
            {
                itemstack = this.items.get(slot).split(amount);

                if (this.items.get(slot).getCount() == 0)
                {
                	this.items.set(slot, ItemStack.EMPTY);
                }

                this.setChanged();
                return itemstack;
            }
        }
        else
        {
            return ItemStack.EMPTY;
        }
    }

	@Override
	public ItemStack removeItemNoUpdate(int index)
	{
		if(getItem(index)!=null)
		{
			ItemStack stack = items.get(index);
			items.remove(index);
			return stack;
		}
		return null;
	}
	
	@Override
	public void setItem(int var1, ItemStack var2) 
	{
		items.set(var1, var2);
		this.setChanged();
	}

	@Override
	public int getMaxStackSize() 
	{
		return 64;
	}


	@Override
	public void startOpen(PlayerEntity pl) {}

	@Override
	public void stopOpen(PlayerEntity pl) {}

	@Override
	public boolean canPlaceItem(int var1, ItemStack var2) 
	{
		return true;
	}

	@Override
	public void clearContent() { }
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	//TODO: this must to ALL TileEntitys
	@Override
	public boolean stillValid(PlayerEntity player)
	{
		return HelperResearch.isUseable(player, this);
	}
	
	@Override
	public boolean isEmpty()
	{
		return false;
	}
	
	@Override
	public IInventory getInventory() 
	{
		return this;
	}
}
