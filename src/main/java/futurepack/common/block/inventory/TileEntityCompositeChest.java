package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import futurepack.common.gui.inventory.GuiCompositeChest.GenericContainer;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.block.ChestBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.DoubleSidedInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.properties.ChestType;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.LockableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

public class TileEntityCompositeChest extends ChestTileEntity implements ITileInventoryProvider
{
	private int ticksSinceSync;
	
	public TileEntityCompositeChest()
	{
		super(FPTileEntitys.COMPOSITE_CHEST);
		load(null, new CompoundNBT()); //this is to update the NonNullist
	}
	
	@Override
	public int getContainerSize()
    {
        return 4 * 9;
    }
	
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		AxisAlignedBB bb = new net.minecraft.util.math.AxisAlignedBB(worldPosition.offset(-1, 0, -1), worldPosition.offset(2, 2, 2));
		return bb;
	}
	
	@Override
	public IInventory getInventory() 
	{
		return ChestBlock.getContainer((ChestBlock) InventoryBlocks.composite_chest, getBlockState(), getLevel(), getBlockPos(), false);
	}
	
	public void tick() 
	{
		int i = this.worldPosition.getX();
		int j = this.worldPosition.getY();
		int k = this.worldPosition.getZ();
		++this.ticksSinceSync;
		this.openCount = calculatePlayersSc(this.level, this, this.ticksSinceSync, i, j, k, this.openCount);
		this.oOpenness = this.openness;
		float f = 0.1F;
		if (this.openCount > 0 && this.openness == 0.0F) {
			this.playSound(SoundEvents.CHEST_OPEN);
		}

		if (this.openCount == 0 && this.openness > 0.0F || this.openCount > 0 && this.openness < 1.0F) {
			float f1 = this.openness;
			if (this.openCount > 0) {
				this.openness += 0.1F;
			} else {
				this.openness -= 0.1F;
			}

			if (this.openness > 1.0F) {
				this.openness = 1.0F;
			}

			float f2 = 0.5F;
			if (this.openness < 0.5F && f1 >= 0.5F) {
				this.playSound(SoundEvents.CHEST_CLOSE);
			}

			if (this.openness < 0.0F) {
				this.openness = 0.0F;
			}
		}

	}

	public int calculatePlayersSc(World p_213977_0_, LockableTileEntity p_213977_1_, int p_213977_2_, int p_213977_3_, int p_213977_4_, int p_213977_5_, int p_213977_6_) 
	{
		if (!p_213977_0_.isClientSide && p_213977_6_ != 0 && (p_213977_2_ + p_213977_3_ + p_213977_4_ + p_213977_5_) % 200 == 0) 
		{
			p_213977_6_ = calculatePlayer(p_213977_0_, p_213977_1_, p_213977_3_, p_213977_4_, p_213977_5_);
		}

		return p_213977_6_;
	}

	public int calculatePlayer(World p_213976_0_, LockableTileEntity p_213976_1_, int p_213976_2_, int p_213976_3_, int p_213976_4_) 
	{
		int i = 0;
		float f = 5.0F;

		for(PlayerEntity playerentity : p_213976_0_.getEntitiesOfClass(PlayerEntity.class, new AxisAlignedBB((double)((float)p_213976_2_ - 5.0F), (double)((float)p_213976_3_ - 5.0F), (double)((float)p_213976_4_ - 5.0F), (double)((float)(p_213976_2_ + 1) + 5.0F), (double)((float)(p_213976_3_ + 1) + 5.0F), (double)((float)(p_213976_4_ + 1) + 5.0F)))) 
		{
			if (playerentity.containerMenu instanceof GenericContainer) 
			{
				IInventory iinventory = ((GenericContainer)playerentity.containerMenu).getBase();
				if (iinventory == p_213976_1_ || iinventory instanceof DoubleSidedInventory && ((DoubleSidedInventory)iinventory).contains(p_213976_1_)) 
				{
					++i;
				}
			}
		}

		return i;
	}
	
	protected void playSound(SoundEvent soundIn) 
	{
		ChestType chesttype = this.getBlockState().getValue(ChestBlock.TYPE);
		if (chesttype != ChestType.LEFT) 
		{
			double d0 = (double)this.worldPosition.getX() + 0.5D;
			double d1 = (double)this.worldPosition.getY() + 0.5D;
			double d2 = (double)this.worldPosition.getZ() + 0.5D;
			if (chesttype == ChestType.RIGHT) 
			{
				Direction direction = ChestBlock.getConnectedDirection(this.getBlockState());
				d0 += (double)direction.getStepX() * 0.5D;
				d2 += (double)direction.getStepZ() * 0.5D;
			}

			this.level.playSound((PlayerEntity)null, d0, d1, d2, soundIn, SoundCategory.BLOCKS, 0.5F, this.level.random.nextFloat() * 0.1F + 0.9F);
		}
	}

	@Override
	public String getGUITitle() {
		return "block.futurepack.composite_chest";
	}
}
