package futurepack.common.block.inventory;

import java.util.function.Supplier;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockPusher extends BlockRotateableTile
{
	private final Supplier<? extends TileEntityPusher> constructor;
	
	protected BlockPusher(Block.Properties props) 
	{
		this(props, TileEntityPusher::new);
	}

	protected BlockPusher(Block.Properties props, Supplier<? extends TileEntityPusher> supplier) 
	{
		super(props);
		this.constructor = supplier;
	}
	
	@Override
	public void onRemove(BlockState state, World w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		TileEntityPusher p = (TileEntityPusher) w.getBlockEntity(pos);
		if(p!=null)
			p.onBlockBreak();
		
		super.onRemove(state, w, pos, newState, isMoving);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return constructor.get();
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand handIn, BlockRayTraceResult hit) 
	{
		if(!w.isClientSide)
		{
			FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
			return ActionResultType.SUCCESS;
		}
		return ActionResultType.SUCCESS;
	}
}
