package futurepack.common.block.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.item.CraftingItems;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperInventory.SlotContent;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropsBlock;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootParameters;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tags.BlockTags;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;


//TODO: Upgrade fuer neuplanzen der Samen/pflanzen
//TODO: Upgrade fuer mehr Ticks (braucht nur redstone signal und nutz dan die BlockUpdates)
public class TileEntityBlockBreaker extends TileEntityInventoryBase implements ISidedInventory, ITickableTileEntity
{
	
	public static final int SLOT_RANGE = 1, SLOT_LUCK = 2, SLOT_SILCKTOUCH = 3;
	
	
	boolean last = true;
	public int on = 0;
//	ItemStack[] items = new ItemStack[4];
	List<ItemStack> storedItems = new ArrayList<ItemStack>();
	
	private IItemHandler handler = new ExtractSide();	
	private LazyOptional<IItemHandler> itemOpt;
	
	public TileEntityBlockBreaker() 
	{
		super(FPTileEntitys.BLOCK_BREAKER);
	}
	
	@Override
	public void tick() 
	{
		if(on>0)
		{
			on--;
			setChanged();
		}
		boolean now = level.getBestNeighborSignal(worldPosition)>0;
		if(now && !last)
		{
			if(items.get(0).isEmpty())
			{
				Direction face = getInputSide();
				
				int max = 1 + getEnchLevel(Enchantments.BLOCK_EFFICIENCY);
				BlockPos xyz = worldPosition;
				for(int j=0;j<max;j++)
				{
					xyz = xyz.relative(face);
					BlockState state = level.getBlockState(xyz);
					if(state.getBlock() instanceof CropsBlock)
					{
						CropsBlock cr = (CropsBlock) state.getBlock();
						boolean b = cr.isMaxAge(state);
						if(!b)
							continue;
					}
						
					if(!state.getBlock().isAir(state, level, xyz))
					{
						break;
					}	
				}
				if(level.isEmptyBlock(xyz))
				{			
					return;
				}
				
				BlockState state = level.getBlockState(xyz);
				
				if( state.getDestroySpeed(level, xyz) < 0)
					return;
				
				boolean croop = state.getBlock() instanceof CropsBlock;
				if(croop)
				{
					CropsBlock cr = (CropsBlock) state.getBlock();
					if(!cr.isMaxAge(state))
						return;
				}
					
				
				level.levelEvent(2001, xyz, Block.getId(level.getBlockState(xyz)));//from world.destroyBlock - this is the particles
				
				on=10;
				if(!level.isClientSide)
				{
					boolean wood = state.is(BlockTags.LOGS);
					level.removeBlock(xyz, false);
					if(croop && level.isEmptyBlock(xyz))
					{
						CropsBlock cr = (CropsBlock) state.getBlock();
						level.setBlockAndUpdate(xyz, cr.getStateForAge(0));
					}
					if(wood)
					{
						for(int j=-5;j<5;j++)
						{
							for(int k=-5;k<5;k++)
							{
								for(int l=-5;l<5;l++)
								{
									BlockPos jkl2 = xyz.offset(j,k,l);
									BlockState leave = level.getBlockState(jkl2);
									if(leave.is(BlockTags.LEAVES))
									{
										level.getBlockTicks().scheduleTick(jkl2, leave.getBlock(), 1);
									}
								}
							}
						}
					}
						
					ItemStack tool = getToolWithEnchantments();
					
					LootContext.Builder lootcontext$builder = (new LootContext.Builder((ServerWorld)this.level))
							.withRandom(this.level.random)
							.withParameter(LootParameters.ORIGIN, Vector3d.atLowerCornerOf(xyz))
							.withParameter(LootParameters.TOOL, tool)
							.withOptionalParameter(LootParameters.BLOCK_ENTITY, getLevel().getBlockEntity(xyz));
					
					for(ItemStack s : state.getBlock().getDrops(state, lootcontext$builder)) {
						Block.popResource(getLevel(), xyz, s);
					}
					
					//Block.spawnDrops(state, lootcontext$builder);
					
//					if(getEnchLevel(Enchantments.SILK_TOUCH)>0)
//					{
//						try
//						{
//							if(state.getBlock().canSilkHarvest(state, world, xyz, null))
//							{
//								ItemEntity ei = new ItemEntity(world, xyz.getX()+0.5,xyz.getY()+0.5,xyz.getZ()+0.5, new ItemStack(state.getBlock()));
//								world.addEntity(ei);
//							}
//							else
//							{
//								state.dropBlockAsItem(world, xyz, Math.min(5, getEnchLevel(Enchantments.FORTUNE)));
//							}
//						}
//						catch(NullPointerException e)
//						{
//							state.dropBlockAsItem(world, xyz, Math.min(5, getEnchLevel(Enchantments.FORTUNE)));
//						}
//					}
//					else
//					{
//
////						state.dropBlockAsItem(world, xyz, Math.min(5, getEnchLevel(Enchantments.FORTUNE)));
//					}
//					
					
					AxisAlignedBB bb = new AxisAlignedBB(xyz.getX(),xyz.getY(),xyz.getZ(),xyz.getX()+1,xyz.getY()+1,xyz.getZ()+1);		
					List<ItemEntity> list = level.getEntitiesOfClass(ItemEntity.class, bb);
					for(ItemEntity item : list)
					{
						if(item.isAlive())
						{
							storedItems.add(item.getItem());
							item.remove();
						}
					}

					
				}
				//if(b instanceof BlockCrops && world.getBlock(x, y, z)==Blocks.AIR)
				//{
					//world.setBlock(x, y, z, b);
				//}		
			}			
		}
		last = now;
		
		if(items.get(0).isEmpty() && !storedItems.isEmpty())
		{
			items.set(0, storedItems.remove(0));
		}
		level.getProfiler().push("eject_items");
		if(!items.get(0).isEmpty())
		{
			ArrayList<SlotContent> items = new ArrayList<SlotContent>(Arrays.asList(new SlotContent(handler, 0, this.items.get(0), null)));
			BlockPos jkl = worldPosition.relative(getOutputSide());
			
			ArrayList<SlotContent> done = (ArrayList<SlotContent>) HelperInventory.insertItems(level, jkl, getInputSide(), items);
			for(SlotContent slot : done)
			{
				slot.remove();
			}
			done = (ArrayList<SlotContent>) HelperInventory.ejectItemsIntoWorld(level, jkl, items);
			for(SlotContent slot : done)
			{
				slot.remove();
			}
		}
		level.getProfiler().pop();
	}
	
	private ItemStack getToolWithEnchantments() 
	{		
		ItemStack it = new ItemStack(Items.DIAMOND_PICKAXE);
		ListNBT list = it.getEnchantmentTags();
		
		if(!this.getItem(SLOT_LUCK).isEmpty())
		{
			CompoundNBT nbt = new CompoundNBT();
			nbt.putString("id", Enchantments.BLOCK_FORTUNE.getRegistryName().toString());
			nbt.putInt("lvl", this.getItem(SLOT_LUCK).getCount());
			list.add(nbt);
		}
		
		if(!this.getItem(SLOT_SILCKTOUCH).isEmpty())
		{
			CompoundNBT nbt = new CompoundNBT();
			nbt.putString("id", Enchantments.SILK_TOUCH.getRegistryName().toString());
			nbt.putInt("lvl", this.getItem(SLOT_SILCKTOUCH).getCount());
			list.add(nbt);
		}

		it.getOrCreateTag().put("Enchantments", list);
		return it;
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.putBoolean("last", last);

		ListNBT stored = new ListNBT();
		for(int i=0;i<storedItems.size();i++)
		{
			CompoundNBT tag = new CompoundNBT();
			storedItems.get(i).save(tag);
			stored.add(tag);
		}
		nbt.put("storedItems", stored);
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		last = nbt.getBoolean("last");

		storedItems.clear();
		ListNBT stored = nbt.getList("storedItems", 10);
		for(int i=0;i<stored.size();i++)
		{
			CompoundNBT tag = stored.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			storedItems.add(is);
		}
	}
	
	@Override
	protected int getInventorySize()
	{
		return 4;
	}
	
	@Override
	public boolean canPlaceItem(int slot, ItemStack it)
	{
		if(slot == 0)
			return super.canPlaceItem(slot, it);
		else if(slot == SLOT_RANGE)
		{
			return it.getItem() == CraftingItems.upgrade_range;
		}
		else if(slot == SLOT_LUCK)
		{
			return it.getItem() == CraftingItems.upgrade_luck;
		}
		else if(slot == SLOT_SILCKTOUCH)
		{
			return it.getItem() == CraftingItems.upgrade_silktouch;
		}
		else
		{
			return super.canPlaceItem(slot, it);
		}
	}
	
	@Override
	public int[] getSlotsForFace(Direction p_94128_1_)
	{
		return new int[]{};
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack ietm, Direction side)
	{
		return false;
	}
	
	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack item, Direction side)
	{
		return false;
	}
	
	public Direction getOutputSide()
	{
		return getInputSide().getOpposite();
	}
	
	public Direction getInputSide()
	{
		BlockState state = getBlockState();
		if(state.getBlock() != InventoryBlocks.block_breaker)
		{
			return Direction.UP;
		}
		Direction face = state.getValue(BlockRotateableTile.FACING);
		return face;
	}
	
	private int getEnchLevel(Enchantment ench)
	{
		return getItem(SLOT_RANGE).isEmpty() ? 0 : getItem(SLOT_RANGE).getCount();
	}

	@Override
	public void clearContent() { }
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY && facing == getOutputSide())
		{
			if(itemOpt!=null)
			{
				return (LazyOptional<T>) itemOpt;
			}
			else
			{
				itemOpt = LazyOptional.of(() -> handler);
				itemOpt.addListener(p-> itemOpt = null);
				return (LazyOptional<T>) itemOpt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(itemOpt);
		super.setRemoved();
	}
	
	public class ExtractSide implements IItemHandler
	{

		@Override
		public int getSlots()
		{
			return 1;
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return slot==0? items.get(0) : ItemStack.EMPTY;
		}

		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) 
		{
			return stack;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			if (amount == 0 && slot!=0)
				return ItemStack.EMPTY;
			if (items.get(0).isEmpty())
				return ItemStack.EMPTY;

			int out = Math.min(amount, items.get(0).getMaxStackSize());
			
			if (items.get(0).getCount() <= out)
			{
				if (!simulate)
				{
					items.set(0,  ItemStack.EMPTY);
				}
				return items.get(0);
			}
			else
			{
				if (!simulate)
				{
					items.set(0, ItemHandlerHelper.copyStackWithSize(items.get(0), items.get(0).getCount() - out));
				}			
				return ItemHandlerHelper.copyStackWithSize(items.get(0), out);
			}
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return 0;
		}

		@Override
		public boolean isItemValid(int slot, ItemStack stack) 
		{
			return false;
		}		
	}

	@Override
	public String getGUITitle() {
		return "block.futurepack.block_breaker";
	}
}
