package futurepack.common.block.inventory;

import javax.annotation.Nonnull;

import net.minecraft.item.ItemStack;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.ItemStackHandler;

public abstract class ItemStackHandlerGuis extends ItemStackHandler 
{
	private Boolean gui;
	
	public ItemStackHandlerGuis(int size)
	{
		super(size);
	}
	
	@Override
	public final ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		return insertItem(slot, stack, simulate, gui==null?false:gui);
	}
	
	@Override
	public final boolean isItemValid(int slot, ItemStack stack)
	{
		
		return isItemValid(slot, stack, gui==null?false:gui);
	}
	
	@Override
	public final ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		return extractItem(slot, amount, simulate, false);
	}
	
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
	{
		this.gui = gui;
		ItemStack it = super.insertItem(slot, stack, simulate);
		this.gui = null;
		return it;
	}	
	
	public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
	{
		this.gui = gui;
		ItemStack it = super.extractItem(slot, amount, simulate);
		this.gui = null;
		return it;
	}
	
	public boolean isItemValid(int slot, ItemStack stack, boolean gui)
	{
		return super.isItemValid(slot, stack);
	}
	
    @Nonnull
    public ItemStack insertItemInternal(int slot, @Nonnull ItemStack stack, boolean simulate)
    {
        if (stack.isEmpty())
            return ItemStack.EMPTY;
            
        //if (!isItemValid(slot, stack))
        //   return stack;

        validateSlotIndex(slot);

        ItemStack existing = this.stacks.get(slot);

        int limit = getStackLimit(slot, stack);

        if (!existing.isEmpty())
        {
            if (!ItemHandlerHelper.canItemStacksStack(stack, existing))
                return stack;

            limit -= existing.getCount();
        }

        if (limit <= 0)
            return stack;

        boolean reachedLimit = stack.getCount() > limit;

        if (!simulate)
        {
            if (existing.isEmpty())
            {
                this.stacks.set(slot, reachedLimit ? ItemHandlerHelper.copyStackWithSize(stack, limit) : stack);
            }
            else
            {
                existing.grow(reachedLimit ? limit : stack.getCount());
            }
            onContentsChanged(slot);
        }

        return reachedLimit ? ItemHandlerHelper.copyStackWithSize(stack, stack.getCount()- limit) : ItemStack.EMPTY;
    }
}
