package futurepack.common.block.inventory;

import futurepack.api.PacketBase;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.TileEntityNetworkMaschine;
import futurepack.common.network.FunkPacketNeon;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class TileEntityModulT1 extends TileEntityNetworkMaschine implements ITickableTileEntity, ITilePropertyStorage
{
	public CapabilityNeon power = new CapabilityNeon(10000, EnumEnergyMode.USE);

//	/**
//	 * 0: input baterie laden
//	 * 1: input baterie entladen
//	 * 2: output baterie voll
//	 * 3: output baterie leer
//	*/
//	private ItemStack[] items = new ItemStack[4];
	private ItemContainer handler = new ItemContainer();
	
	public TileEntityModulT1() 
	{
		super(FPTileEntitys.MODUL_T1);
	}
	
	@Override
	public void tick()
	{
		if(handler.getStackInSlot(0)!=null && handler.getStackInSlot(0).getItem() instanceof IItemNeon)
		{
			IItemNeon neon = (IItemNeon) handler.getStackInSlot(0).getItem();
			if(neon.isNeonable(handler.getStackInSlot(0)))
			{
				int c = Math.min(20, Math.min(neon.getMaxNeon(handler.getStackInSlot(0)) - neon.getNeon(handler.getStackInSlot(0)), power.get()));
				if(c > 0)
				{
					neon.addNeon(handler.getStackInSlot(0), power.use(c));
				}	
			}
			if(neon.getNeon(handler.getStackInSlot(0)) == neon.getMaxNeon(handler.getStackInSlot(0)))
			{
				if(handler.getStackInSlot(2).isEmpty())
				{
					handler.setStackInSlot(2, handler.getStackInSlot(0));
					handler.setStackInSlot(0, ItemStack.EMPTY);
				}
			}
		}
		if(handler.getStackInSlot(1)!=null && handler.getStackInSlot(1).getItem() instanceof IItemNeon)
		{
			IItemNeon neon = (IItemNeon) handler.getStackInSlot(1).getItem();
			if(neon.getNeon(handler.getStackInSlot(1))>0)
			{
				int c = Math.min(20, Math.min(neon.getNeon(handler.getStackInSlot(1)), power.getMax() - power.get()));
				if(c < 0)
				{
					neon.addNeon(handler.getStackInSlot(1), -power.add(c));
				}
			}
			if(neon.getNeon(handler.getStackInSlot(1))<=0)
			{
				if(handler.getStackInSlot(3).isEmpty())
				{
					handler.setStackInSlot(3, handler.getStackInSlot(1));
					handler.setStackInSlot(1, ItemStack.EMPTY);
				}
			}
		}
	}
			
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("energy", power.serializeNBT());
		nbt.put("itemContainer", handler.serializeNBT());
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
		if(nbt.contains("Items"))
		{
			ListNBT l = nbt.getList("Items", 10);
			for(int i=0;i<l.size();i++)
			{
				CompoundNBT tag = l.getCompound(i);
				ItemStack is = ItemStack.of(tag);
				handler.setStackInSlot(tag.getInt("slot"), is);
			}
		}
		else
		{
			handler.deserializeNBT(nbt.getCompound("itemContainer"));
		}
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		super.onFunkPacket(pkt);
		
		if(pkt instanceof FunkPacketNeon)
		{
			FunkPacketNeon ch = (FunkPacketNeon) pkt;
			int c = (ch.needed-ch.collected);
			if(c > 0 && power.get() > 0)
			{
				ch.collected += power.use(c);
			}
//			ch.speedmodifier += getSpeedUpgrades();
		}
	}

	

//	private int getSpeedUpgrades()
//	{
//		int upgrades = 0;
//		for(EnumFacing face : FacingUtil.VALUES)
//		{
//			BlockPos pos = this.pos.offset(face);
//			IBlockState state = world.getBlockState(pos);
//			if(state.getBlock() == FPBlocks.moduleSpeedUpgrade)
//			{
//				if(((BlockModulSpeedUpgrade)state.getBlock()).isConnectedToThis(world, pos, state, this))
//				{
//					upgrades++;		
//				}
//			}
//		}
//		return upgrades;
//	}
	
	public boolean isBatterie(ItemStack it, boolean chargeRequired)
	{
		if(it==null || it.isEmpty())
			return false;
		Item item = it.getItem();
		if(item instanceof IItemNeon)
		{
			IItemNeon neon = (IItemNeon) item;
			if(chargeRequired)
			{
				return neon.getNeon(it) > 0;
			}
			else
			{
				return neon.isNeonable(it);
			}
		}
		return false;
	}
	
	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return power.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			int d = value - power.get();
			if(d > 0)
				power.add(d);
			else
				power.use(-d);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 1;
	}


	private class ItemContainer extends ItemStackHandlerGuis
	{
		public ItemContainer()
		{
			super(4);
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			if(slot==2 || slot==3 || gui)
				return super.extractItem(slot, amount, simulate, gui);
			else 
				return null;
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			if( isItemValid(slot, stack))
				return super.insertItem(slot, stack, simulate, gui);
			else
				return stack;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			return (slot==0 || slot==1) && isBatterie(stack, slot==1);
		}
	}

	
	private LazyOptional<INeonEnergyStorage> neon_opt;
	private LazyOptional<IItemHandler> item_opt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return LazyOptional.empty();
		
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(item_opt!=null)
				return (LazyOptional<T>) item_opt;
			else
			{
				item_opt = LazyOptional.of(() -> handler);
				item_opt.addListener(p -> item_opt = null);
				return (LazyOptional<T>) item_opt;
			}
		}
		else if(capability == CapabilityNeon.cap_NEON)
		{
			if(neon_opt!=null)
				return (LazyOptional<T>) neon_opt;
			else
			{
				neon_opt = LazyOptional.of(() -> power);
				neon_opt.addListener(p -> neon_opt = null);
				return (LazyOptional<T>) neon_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neon_opt, item_opt);
		super.setRemoved();
	}
	
	public IItemHandler getGui()
	{
		return new ItemHandlerGuiOverride(handler);
	}
	
}
