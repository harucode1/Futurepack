package futurepack.common.block.inventory;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockFuelCell extends BlockHoldingTile
{
	public static final EnumProperty<Direction.Axis> AXIS = BlockStateProperties.AXIS;
	
	protected BlockFuelCell(Block.Properties props)
	{
		super(props);
		this.registerDefaultState(this.defaultBlockState().setValue(AXIS, Direction.Axis.Y));
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityFuelCell();
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.FUEL_CELL.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		switch(rot) 
		{
		case COUNTERCLOCKWISE_90:
		case CLOCKWISE_90:
			switch(state.getValue(AXIS)) {
			case X:
				return state.setValue(AXIS, Direction.Axis.Z);
			case Z:
				return state.setValue(AXIS, Direction.Axis.X);
			default:
				return state;
			}
		default:
			return state;
		}
	}

	@Override
	protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder)
	{
		builder.add(AXIS);
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		return this.defaultBlockState().setValue(AXIS, context.getClickedFace().getAxis());
	}
}
