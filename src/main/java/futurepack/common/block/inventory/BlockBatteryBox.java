package futurepack.common.block.inventory;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockBatteryBox extends BlockHoldingTile
{	
	public static final IntegerProperty CHARGE = IntegerProperty.create("charge", 0, 4);
	
	protected BlockBatteryBox(Block.Properties props)
	{
		super(props);
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.BATTERY_BOX.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(CHARGE);
	}
	
	@Override
	public void setPlacedBy(World w, BlockPos pos, BlockState state, LivingEntity liv, ItemStack it)
	{
		super.setPlacedBy(w, pos, state, liv, it);
		
		TileEntity tile = w.getBlockEntity(pos);
		if(tile instanceof TileEntityBatteryBox)
		{
			((TileEntityBatteryBox) tile).energy.use(0);
		}
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityBatteryBox();
	}
}
