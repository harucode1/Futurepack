package futurepack.common.block.inventory;

import java.util.List;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileXpStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.TileEntityNetworkMaschine;
import futurepack.common.network.FunkPacketExperience;
import futurepack.common.network.FunkPacketExperienceDistribution;
import net.minecraft.block.BlockState;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.math.AxisAlignedBB;

public class TileEntityModulT3 extends TileEntityNetworkMaschine implements ITickableTileEntity, ITileXpStorage, ITilePropertyStorage
{
	
	int storedXP = 0;
	
	public TileEntityModulT3() 
	{
		super(FPTileEntitys.MODUL_T3);
	}

	
	
	@Override
	public void tick()
	{
		if(storedXP < 1500)
		{
			List<ExperienceOrbEntity> orbs = level.getEntitiesOfClass(ExperienceOrbEntity.class, new AxisAlignedBB(worldPosition.offset(-1, -1, -1), worldPosition.offset(2,2,2)));
			for(ExperienceOrbEntity orb : orbs)
			{
				if(orb.isAlive()==false)
					continue;
				storedXP += orb.value;
				orb.remove();
			}
		}
	}
	
	@Override
	public CompoundNBT save(CompoundNBT compound)
	{
		super.save(compound);
		compound.putInt("storedXp", storedXP);
		return compound;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT compound)
	{
		super.load(state, compound);
		
		storedXP = compound.getInt("storedXp");
	}
	
	public void suck(PlayerEntity pl)
	{
		pl.giveExperienceLevels(-1);
		this.storedXP += pl.getXpNeededForNextLevel();
	}
	
	public static int xpBarCap(int level)
    {
        return level >= 30 ? 112 + (level - 30) * 9 : (level >= 15 ? 37 + (level - 15) * 5 : 7 + level * 2);
    }

	@Override
	public int getXp()
	{
		return storedXP;
	}

	/**
	 * 1500 = 30lvl
	 */
	@Override
	public int getMaxXp()
	{
		return 150000;
	}

	@Override
	public void setXp(int lvl)
	{
		storedXP = lvl;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		super.onFunkPacket(pkt);
		
		if(pkt instanceof FunkPacketExperience)
		{
			FunkPacketExperience xp = (FunkPacketExperience) pkt;
			int c = Math.min(storedXP, xp.needed - xp.collected);
			storedXP -= c;
			xp.collected += c;
		}
		else if(pkt instanceof FunkPacketExperienceDistribution)
		{
			FunkPacketExperienceDistribution xp = (FunkPacketExperienceDistribution) pkt;
			int c = Math.min(xp.XP, getMaxXp()-storedXP);
			xp.XP -= c;
			storedXP += c;
		}
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return storedXP & 0x7FF; //short max value
		case 1:
			return (storedXP >> 15) & 0x7FF;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			storedXP = (storedXP & (0x7FF<<15)) | value;
			break;
		case 1:
			storedXP = (storedXP & 0x7FF) | (value << 15);
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 2;
	}
}
