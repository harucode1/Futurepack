package futurepack.common.block.modification;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.WeakHashMap;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileWithOwner;
import futurepack.common.FuturepackMain;
import futurepack.common.block.ItemMoveTicker;
import futurepack.common.modification.EnumChipType;
import futurepack.common.player.FakePlayerFactory;
import futurepack.common.player.FuturepackPlayer;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageEntityForEater;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.Effects;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.PacketDistributor.PacketTarget;

/**
 * @param <T> is the Entity for the predicate
 */
public abstract class  TileEntityLaserBase<T extends LivingEntity> extends TileEntityModificationBase implements ITileWithOwner, ITilePropertyStorage
{
	private WeakHashMap<PlayerEntity, Long> warnedPlayers = new WeakHashMap<PlayerEntity, Long>();
	
	private UUID owner;
	
	public T src;
	public Vector3d entityPos;
	public Vector3d blockPos;
	
	private Class<T> clazz;
	
	public int pass;
	protected int entityTries = 0;
	
	private HashMap<String, Boolean> config = new HashMap<String, Boolean>();
	
	//Only Client
	public long lastMessageTime = 0;
	
	public TileEntityLaserBase(TileEntityType<? extends TileEntityLaserBase<T>> type, Class<T> cls)
	{
		super(type);
		clazz = cls;
		
		config.put("attack.player", false);
		config.put("attack.neutral", false);
		config.put("attack.mobs", true);
		config.put("kill.not", false);//dont work anyway
		
		config.put("player.warn", true);
		
		config.put("player.distance", false);
		
		config.put("wait.10", false);
		config.put("wait.15", false);
		config.put("wait.30", false);
		config.put("wait.60", false);

		config.put("has.ai", false);
	}

	public void resetConfig() {
		setConfig("attack.player", false);
		setConfig("attack.neutral", false);
		setConfig("attack.mobs", true);
		setConfig("kill.not", false);//dont work anyway

		setConfig("player.warn", true);

		setConfig("player.distance", false);

		setConfig("wait.10", false);
		setConfig("wait.15", false);
		setConfig("wait.30", false);
		setConfig("wait.60", false);
	}
	
	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public void updateTile(int ticks)
	{
		if(!level.isClientSide)
		{
			if(getChipPower(EnumChipType.AI) == 0) {
				resetConfig();
				setConfig("has.ai", false);
			}
			else {
				setConfig("has.ai", true);
			}

			if(shouldWork())
			{
				for(int i=0;i<ticks;i++)
				{
					if(src!=null)
					{
						if(!isEntityValid(src) || !src.isAlive())
						{
							entityPos = null;
							src = null;
							entityTries=0;
						}
					}
					
					if(src==null)
					{
						src = getNextEntity(Predicates.alwaysTrue());
						entityTries=0;
						if(src!=null)
						{
							MessageEntityForEater mes = new MessageEntityForEater(src, this.worldPosition);
							List<ServerPlayerEntity> list = level.getEntitiesOfClass(ServerPlayerEntity.class, new AxisAlignedBB(worldPosition.getX()-20, worldPosition.getY()-20, worldPosition.getZ()-20, worldPosition.getX()+20, worldPosition.getY()+20, worldPosition.getZ()+20), new Predicate<ServerPlayerEntity>() //getEntitiesWithinAABBExcludingEntity
							{			
								@Override
								public boolean apply(ServerPlayerEntity mp)
								{			
									boolean[] b =  FPPacketHandler.map.get(mp.getGameProfile().getId());
									if(b==null || b[1])
									{
										return true;
									}
									return false;
								}
							});
							for(ServerPlayerEntity pl : list)
							{
								PacketTarget p = PacketDistributor.PLAYER.with(() -> pl);
								FPPacketHandler.CHANNEL_FUTUREPACK.send(p, mes);
							}
						}
					}
					if(src!=null && isEntityValid(src) && canEntityBeSeen(src))
					{
						entityTries=0;
						progressEntity(src);			
					}
					else
					{
						entityTries++;
						if(entityTries > 5)
						{
							T newTarget = getNextEntity(e -> e != src);
							if(newTarget!=null)
							{
								src = newTarget;
								entityTries = 0;
							}
						}
					}
				}
			}
			else
			{
				src = null;
				entityTries=0;
			}
			
			
			Iterator<Entry<PlayerEntity, Long>> iter = warnedPlayers.entrySet().iterator();
			while(iter.hasNext())
			{
				Entry<PlayerEntity, Long> e = iter.next();
				if(System.currentTimeMillis() - e.getValue() > 1000*60*2)
				{
					iter.remove();
				}
			}
		}
	}
	
	@Override
	public void updateNaturally() 
	{
		if(!level.isClientSide)
		{
			if(src==null)
			{
				entityPos = null;			
			}
			else
			{
				if(!src.isAlive())
				{
					src = null;
					entityPos = null;
				}
			}
		}
		super.updateNaturally();
	}

	@Override
	public boolean isWorking()
	{
		return true;
	}
//	
//	public Predicate<T> getPredicate()
//	{
//		return new Predicate<T>()
//		{
//			@Override
//			public boolean apply(T input)
//			{
//				return true;
//			}
//		};
//	}

	public float getRange()
	{
		float pro = getChipPower(EnumChipType.TACTIC) * 2;
		return (2.5F + pro) * 2F;
	}
	
	protected boolean canEntityBeSeen(Entity p_70685_1_)
    {
		Vector3d entity = new Vector3d(p_70685_1_.getX(), p_70685_1_.getY() + p_70685_1_.getEyeHeight(), p_70685_1_.getZ());
		blockPos = getVecPos();
		blockPos = blockPos.add(blockPos.vectorTo(entity).normalize());
		
		RayTraceResult pos =  this.level.clip(new RayTraceContext(blockPos, entity, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.ANY, p_70685_1_));
		if(pos.getType() == RayTraceResult.Type.MISS)
		{
			entityPos = entity;
			return true;
		}
		else
		{
			return false;
		}
    }
	
	public Vector3d getVecPos()
	{
		if(getBlockState().getValue(BlockEntityLaserBase.ROTATION_VERTICAL) == Direction.DOWN)
			return new Vector3d(worldPosition.getX()+0.5,worldPosition.getY()-0.0625,worldPosition.getZ()+0.5);
		else
			return new Vector3d(worldPosition.getX()+0.5,worldPosition.getY()+1.0625,worldPosition.getZ()+0.5);
	}
	
	protected T getNextEntity(java.util.function.Predicate<T> isValid)
	{
		float r = getRange();
		List<T> list = level.getEntitiesOfClass(clazz, new AxisAlignedBB(-r*2, -r*2, -r*2, r*2, r*2, r*2).move(worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5), new Predicate<LivingEntity>()
		{

			@Override
			public boolean apply(LivingEntity input)
			{
				if(input.hasEffect(Effects.GLOWING))
					return true;
				return  worldPosition.distSqr(input.position(), true) < r*r;
			}
			
		});
		if(list.isEmpty())
			return null;
		
		list.sort(new Comparator<LivingEntity>()
		{
			@Override
			public int compare(LivingEntity o1, LivingEntity o2)
			{
				boolean b1 = o1.hasEffect(Effects.GLOWING);
				boolean b2 = o2.hasEffect(Effects.GLOWING);
				
				//^XOR
				if(b1^b2)
				{
					return b1 ? -1 : 1;
				}
				
				double dis = (worldPosition.distSqr(o1.position(), true) - worldPosition.distSqr(o2.position(), true));
				return dis < 0 ? -1 : (dis > 0 ? 1 : 0);
			}
		});
		
		Iterator<T> iter = list.iterator();
		T e;
		isValid = isValid.and(this::isEntityValid);
		while(iter.hasNext())
		{
			e = iter.next();
			if(!isValid.test(e))
			{
				continue;
			}
			if(canEntityBeSeen(e))
			{
				return e;
			}
		}
		return null;
	}
	
	public abstract boolean isEntityValid(T entity);
	
	public boolean matchConfig(Entity e)
	{
		if(e instanceof PlayerEntity)
		{	
			if(isOwner((PlayerEntity) e) && !canAttackOwner())
				return false;
		}
		
		if(getChipPower(EnumChipType.AI) > 0)
		{
			if(e instanceof PlayerEntity)
			{	
				if(getConfiguration("attack.player"))
				{
					if(isOwner((PlayerEntity) e) && !canAttackOwner())
						return false;
					
					if(getConfiguration("player.distance") && worldPosition.distSqr(e.position(), true) > 25)
						return false;
						
					Long l = warnedPlayers.get(e);
					if(l == null)
					{
						l = System.currentTimeMillis() + getMaxPlayerWaitTime();
						warnedPlayers.put((PlayerEntity) e, l);
						
						warnPlayer((PlayerEntity) e);
						return false;
					}
					else if(l < System.currentTimeMillis())
					{			
						return true;
					}
					else
					{
						warnPlayer((PlayerEntity) e);
						return false;
					}
				}
				return false;
			}
			else if(e instanceof IMob)
				return getConfiguration("attack.mobs");
			else if(e instanceof LivingEntity)
				return getConfiguration("attack.neutral");
			else
				return getConfiguration("attack.all");
		}
		return true;
	}
	
	/**
	 * @return true if the Entity is finished, else it will stay at this entity
	 */
	public abstract void progressEntity(T entity);
	
	public abstract boolean shouldWork();
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public abstract ResourceLocation getTexture();

	//@ TODO: OnlyIn(Dist.CLIENT)
	public abstract int getLaserColor();
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public abstract ResourceLocation getLaser();
	
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return INFINITE_EXTENT_AABB;
	}
	
//	@Override
//	public boolean shouldRenderInPass(int pass)
//	{
//		this.pass = pass;
//		return pass == 0 || pass == 1;
//	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public void setEntityLiv(T s)
	{
		this.src = s;
		Vector3d local = entityPos = new Vector3d(s.getX(), s.getY() + s.getEyeHeight(), s.getZ());
		blockPos = getVecPos();
		blockPos = blockPos.add(blockPos.vectorTo(local).normalize());
		
		lastMessageTime = System.currentTimeMillis();
	}
	
	public boolean attackEntity(LivingEntity base, float amount)
	{
		if(!base.level.isClientSide)
		{
			float xp = this.getChipPower(EnumChipType.ULTIMATE);
			boolean transport = this.getChipPower(EnumChipType.TRANSPORT)>0;
			if(xp>0)
			{
				ServerWorld world = (ServerWorld) this.level;
				FuturepackPlayer player = FakePlayerFactory.INSTANCE.getPlayer(world);
				player.setPos(this.blockPos.x+0.5, this.blockPos.y, this.blockPos.z+0.5);
				boolean b = base.hurt(DamageSource.playerAttack(player), amount);
				if(transport && base.getHealth()<=0)
				{
					new ItemMoveTicker(world, Vector3d.atLowerCornerOf(worldPosition), base.position());
				}
				return b;
			}
			else
			{
				boolean b = base.hurt(FuturepackMain.NEON_DAMAGE, amount);
				if(transport && base.getHealth()<=0)
				{
					new ItemMoveTicker(level, Vector3d.atLowerCornerOf(worldPosition), base.position());
				}
				return b;
			}
		}
		return false;
	}
	
	private short[] cashe;
	
	@Override
	public int getProperty(int id)
	{
		if(id<getShortSize())
		{
			return getConfigAsShorts()[id];
		}
		return energy.get();
	}

	@Override
	public void setProperty(int id, int value)
	{
		if(id<getShortSize())
		{
			if(cashe==null || cashe.length !=getShortSize())
				cashe = new short[getShortSize()];
			cashe[id] = (short) value;
			setConfigFromShort(cashe);
		}
		else
			setEnergy(value);
	}

	@Override
	public int getPropertyCount() 
	{
		return getShortSize() + 1;
	}
	
	
	public Iterable<String> getConfigs()
	{
		return config.keySet();
	}
	
	public boolean getConfiguration(String s)
	{
//		Boolean
		
		return config.get(s);
	}
	
	public void setConfig(String s, boolean b)
	{
		config.put(s, b);
	}
	
	private int getShortSize()
	{
		int size = config.size() / Short.SIZE;
		
		if(size * Short.SIZE < config.size())
			size++;
		
		return size;
	}
	
	public short[] getConfigAsShorts()
	{
		short[] data = new short[getShortSize()];
		
		int bit = 0;
		int index = 0;
		for(Entry<String, Boolean> e : config.entrySet())
		{
			data[index] |= (e.getValue()==true ? 1 : 0) << bit;
			bit++;
			
			if(bit>=Short.SIZE)
			{
				bit=0;
				index++;
			}
		}
		
		return data;
	}
	
	public void setConfigFromShort(short[] data)
	{
		int bit = 0;
		int index = 0;
		for(String s : config.keySet())
		{
			boolean b = 1 == ((data[index] >> bit) & 1);
			config.put(s, b);
			
			bit++;
			if(bit>=Short.SIZE)
			{
				bit=0;
				index++;
			}
		}
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		byte[] data = nbt.getByteArray("config");
		short[] shorts = new short[data.length/2];
		if(shorts.length>0) //this crashed because the length was 0 ....
		{
			for(int i=0;i<shorts.length;i++)
			{
				shorts[i] = (short) (data[i*2]&0xFF | ((data[i*2+1]<<8)&0xFF00));
			}
			setConfigFromShort(shorts);
		}	
		if(nbt.hasUUID("owner"))
			owner = nbt.getUUID("owner");
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		short[] shorts = getConfigAsShorts();
		byte[] data = new byte[shorts.length*2];
		for(int i=0;i<shorts.length;i++)
		{
			data[i*2] = (byte) (shorts[i] & 0xFF);
			data[i*2+1] = (byte) ((shorts[i] >> 8) & 0xFF);
		}
		nbt.putByteArray("config", data);
		if(owner!=null)
			nbt.putUUID("owner", owner);
		
		return nbt;
	}
	
	@Override
	public void setOwner(PlayerEntity pl)
	{
		owner = pl.getGameProfile().getId();
	}
	
	@Override
	public boolean isOwner(PlayerEntity pl)
	{
		return isOwner(pl.getGameProfile().getId());
	}
	
	@Override
	public boolean isOwner(UUID pl)
	{
		if(owner==null)
			return false;
		
		return owner.equals(pl);
	}
	
	public boolean canAttackOwner()
	{
		return false;
	}
	
	private WeakHashMap<PlayerEntity, Long> notice = new WeakHashMap<PlayerEntity, Long>();
	
	public void warnPlayer(PlayerEntity pl)
	{
		if(level.isClientSide)
			return;
		if(!getConfiguration("player.warn"))
			return;
		
		Long last = notice.get(pl);
		if(last != null && System.currentTimeMillis() - last < 1000)
		{
			return;
		}
		
		int i = 0;
		int l = (int) ((warnedPlayers.get(pl)-System.currentTimeMillis()) / 500);
		switch (l)
		{
		case 120:
			i=60;
			break;
		case 60:
			i=30;
			break;		
		case 30:
			i=15;
			break;
		case 20:
			i=10;
			break;
		case 10:
			i=5;
			break;
		case 2:
			i=1;
			break;
		default:
			i=-1;
			break;
		}
		if(i!=-1)
		{
			TranslationTextComponent trans = new TranslationTextComponent("laser.attack.warn.time", i);
			pl.sendMessage(trans, Util.NIL_UUID);
			notice.put(pl, System.currentTimeMillis());
		}
	}
	
	/**
	 * @return in milliseconds
	 */
	public long getMaxPlayerWaitTime()
	{
		int wait = 1;
		if(getConfiguration("wait.10"))
			wait += 10;
		if(getConfiguration("wait.15"))
			wait += 15;
		if(getConfiguration("wait.30"))
			wait += 30;
		if(getConfiguration("wait.60"))
			wait += 60;
		
		return wait * 1000;
	}
}
