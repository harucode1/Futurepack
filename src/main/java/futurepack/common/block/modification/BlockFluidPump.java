package futurepack.common.block.modification;

import java.util.function.Supplier;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.DirectionalBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockFluidPump extends BlockRotateableTile
{
	private static final Supplier<double[][]> shape = () -> new double[][]{
		{6,6,0,10,10,16},
		{0,0,1, 5,16,10},
		{5,2,2, 16,14,14}
	};
	public static final VoxelShape SHAPE_UP = HelperBoundingBoxes.createBlockShape(shape.get(), 90, 0);
	private final VoxelShape SHAPE_DOWN = HelperBoundingBoxes.createBlockShape(shape.get(), 270F, 0F);
	private final VoxelShape SHAPE_NORTH = HelperBoundingBoxes.createBlockShape(shape.get(), 0F, 180F);
	private final VoxelShape SHAPE_SOUTH = HelperBoundingBoxes.createBlockShape(shape.get(), 0F, 0F);
	private final VoxelShape SHAPE_WEST = HelperBoundingBoxes.createBlockShape(shape.get(), 0F, 270F);
	private final VoxelShape SHAPE_EAST = HelperBoundingBoxes.createBlockShape(shape.get(), 0F, 90F);
	
	
	public BlockFluidPump(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		switch(state.getValue(FACING))
		{
		case DOWN:
			return SHAPE_DOWN;
		case NORTH:
			return SHAPE_NORTH;
		case SOUTH:
			return SHAPE_SOUTH;
		case WEST:
			return SHAPE_WEST;
		case EAST:
			return SHAPE_EAST;
		case UP:
		default:
			return SHAPE_UP;
		}
	}
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity pl, Hand handIn, BlockRayTraceResult hit) 
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.FLUID_PUMP.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		Direction f = context.getNearestLookingDirection();
		if(f != Direction.UP && f != Direction.DOWN)
		{
			f = f.getCounterClockWise();
		}
		return defaultBlockState().setValue(DirectionalBlock.FACING, f);
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
    {
        return BlockRenderType.MODEL;
    }

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityFluidPump();
	}
}
