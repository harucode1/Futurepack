package futurepack.common.block.modification;

import java.util.function.Supplier;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.modification.EnumChipType;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockEntityLaserBase extends BlockHoldingTile
{
	public static final EnumProperty<Direction> ROTATION_VERTICAL = EnumProperty.create("rotation_vertical", Direction.class, Direction.Plane.VERTICAL);
			
	private final  Supplier<TileEntityType<? extends TileEntityLaserBase<?>>> type;

	private static final VoxelShape shape_bottom = VoxelShapes.or(Block.box(0, 0, 0, 16, 6, 16), Block.box(4, 0, 4, 12, 20, 12));
	private static final VoxelShape shape_ceiling   = VoxelShapes.or(Block.box(0, 10, 0, 16, 16, 16), Block.box(4, -4, 4, 12, 16, 12));
	
//	static final String names[] = {"eater", "killer", "healer", "rocket"};
	
	protected BlockEntityLaserBase(Block.Properties props, Supplier<TileEntityType<? extends TileEntityLaserBase<?>>> laser)
	{
		super(props);
		this.type = laser;
//		super(Material.IRON, 3);
//		setCreativeTab(FPMain.tab_maschiens);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		if(state.getValue(ROTATION_VERTICAL) == Direction.DOWN)
			return shape_ceiling;
		else if(state.getValue(ROTATION_VERTICAL) == Direction.UP)
			return shape_bottom;
		
		return super.getShape(state, worldIn, pos, sel);
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
    {
        return BlockRenderType.INVISIBLE;
    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(ROTATION_VERTICAL);
	}
	

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		return super.getStateForPlacement(context).setValue(ROTATION_VERTICAL, context.getClickedFace()!= Direction.DOWN ? Direction.UP : Direction.DOWN);
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		TileEntityLaserBase<?> base = (TileEntityLaserBase<?>) w.getBlockEntity(pos);
		if(base.getChipPower(EnumChipType.AI) > 0 && !w.isClientSide)
		{
			if(HelperResearch.canOpen(pl, state) && !w.isClientSide)
			{
				FPGuiHandler.LASER_EDIT.openGui(pl, pos);
				return ActionResultType.SUCCESS;
			}		
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return type.get().create();
	}
}
