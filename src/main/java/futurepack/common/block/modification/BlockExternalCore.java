package futurepack.common.block.modification;

import java.util.function.Supplier;

import futurepack.common.block.BlockRotateableTile;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;

public class BlockExternalCore extends BlockRotateableTile
{
	private static final Supplier<double[][]> shape = () -> new double[][]{
		{0,0,0,16,3,16},
		{2,3,1,14,16,15},
	};
	public static final VoxelShape SHAPE_UP = HelperBoundingBoxes.createBlockShape(shape.get(), 0, 0);
	private final VoxelShape SHAPE_DOWN = HelperBoundingBoxes.createBlockShape(shape.get(), 180F, 0F);
	private final VoxelShape SHAPE_NORTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 0F);
	private final VoxelShape SHAPE_SOUTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 180F);
	private final VoxelShape SHAPE_WEST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 90F);
	private final VoxelShape SHAPE_EAST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 270F);
	
	
	
	public BlockExternalCore(Block.Properties props)
	{
		super(props);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		switch(state.getValue(FACING))
		{
		case DOWN:
			return SHAPE_DOWN;
		case NORTH:
			return SHAPE_NORTH;
		case SOUTH:
			return SHAPE_SOUTH;
		case WEST:
			return SHAPE_WEST;
		case EAST:
			return SHAPE_EAST;
		case UP:
		default:
			return SHAPE_UP;
		}
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityExternalCore();
	}
}
