package futurepack.common.block.modification;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;

public class TileEntityExternalCore extends TileEntityModificationBase
{

	public TileEntityExternalCore() 
	{
		super(FPTileEntitys.EXTERNAL_CORE);
	}

	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public void updateTile(int ticks)
	{
		TileEntity tile = level.getBlockEntity(worldPosition.relative(getDirection()));
		if(tile instanceof ITickableTileEntity)
		{
			if(tile.hasLevel())
			{
				for(int i=0;i<ticks;i++)
					((ITickableTileEntity)tile).tick();
			}
				
		}
	}

	@Override
	public boolean isWorking()
	{
		return level.getBlockEntity(worldPosition.relative(getDirection())) instanceof ITickableTileEntity;
	}

	private Direction getDirection()
	{
		BlockState state = level.getBlockState(worldPosition);
		if(state.getBlock().isAir(state, level, worldPosition))
		{
			return Direction.UP;
		}
		return state.getValue(BlockRotateableTile.FACING).getOpposite();
	}
}
