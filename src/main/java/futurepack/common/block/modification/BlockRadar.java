package futurepack.common.block.modification;

import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;

public class BlockRadar extends BlockRotateableTile
{
	protected static final VoxelShape BOTTOM = VoxelShapes.join(
			VoxelShapes.join(
				Block.box(0, 0, 0, 16, 6, 16),
				Block.box(4, 6, 4, 12, 7, 12),
				IBooleanFunction.OR),
			Block.box(5, 7, 5, 11, 10, 11),
			IBooleanFunction.OR);

	protected static final VoxelShape TOP = VoxelShapes.join(
			VoxelShapes.join(
					Block.box(0, 10, 0, 16, 16, 16),
					Block.box(4, 9, 4, 12, 10, 12),
					IBooleanFunction.OR),
			Block.box(5, 6, 5, 11, 9, 11),
			IBooleanFunction.OR);

	protected static final VoxelShape NORTH = VoxelShapes.join(
			VoxelShapes.join(
					Block.box(0,0,10, 16, 16,16),
					Block.box(4, 4, 9, 12, 12, 10),
					IBooleanFunction.OR),
			Block.box(5, 5, 6, 11, 11, 9),
			IBooleanFunction.OR);

	protected static final VoxelShape EAST = VoxelShapes.join(
			VoxelShapes.join(
					Block.box(0,0,0, 6, 16,16),
					Block.box(6, 4, 4, 7, 12, 12),
					IBooleanFunction.OR),
			Block.box(7, 5, 5, 10, 11, 11),
			IBooleanFunction.OR);

	protected static final VoxelShape SOUTH = VoxelShapes.join(
			VoxelShapes.join(
					Block.box(0,0,0, 16, 16,6),
					Block.box(4, 4, 6, 12, 12, 7),
					IBooleanFunction.OR),
			Block.box(5, 5, 7, 11, 11, 10),
			IBooleanFunction.OR);

	protected static final VoxelShape WEST = VoxelShapes.join(
			VoxelShapes.join(
					Block.box(10,0,0, 16, 16,16),
					Block.box(9, 4, 4, 10, 12, 12),
					IBooleanFunction.OR),
			Block.box(6, 5, 5, 9, 11, 11),
			IBooleanFunction.OR);

	public BlockRadar(Properties props)
	{
		super(props);
	}

	@Override
	public VoxelShape getShape(BlockState blockState, IBlockReader blockReader, BlockPos pos, ISelectionContext selectionContext) {
		switch (blockState.getValue(BlockRotateableTile.FACING)) {
			case UP: return BOTTOM;
			case DOWN: return TOP;
			case NORTH: return NORTH;
			case EAST: return EAST;
			case SOUTH: return SOUTH;
			case WEST: return WEST;
			default: return BOTTOM;
		}
	}

	@Override
	public BlockRenderType getRenderShape(BlockState p_149645_1_) {
		return BlockRenderType.ENTITYBLOCK_ANIMATED;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityRadar();
	}
	

}
