package futurepack.common.block.modification;

import futurepack.common.block.BlockHoldingTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public class BlockWaterTurbine extends BlockHoldingTile
{

	protected BlockWaterTurbine(Block.Properties props)
	{
		super(props);
//		super(Material.IRON,2);
//		setCreativeTab(FPMain.tab_maschiens);
	}
//
//	@Override
//	public void getSubBlocks(ItemGroup tab, NonNullList<ItemStack> l)
//	{
//		l.add(new ItemStack(this,1,0));
//		l.add(new ItemStack(this,1,1));
//		l.add(new ItemStack(this,1,2));
//	}
//	
//	@Override
//	public int getMaxMetas() 
//	{
//		return 3;
//	}
//
//	@Override
//	public String getMetaName(int meta)
//	{
//		return "wasser_turbine_"+meta;
//	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityWaterTurbine();
	}
}
