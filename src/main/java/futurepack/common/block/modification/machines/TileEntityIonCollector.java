package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.EnumChipType;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.block.BlockState;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.world.LightType;

public class TileEntityIonCollector extends TileEntityMachineBase implements ISidedInventory, ITilePropertyStorage
{
	//int tick = 0;
	public float power;
	
	public TileEntityIonCollector()
	{
		super(FPTileEntitys.ION_COLLECTOR);
	}

	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
	}
	
	private float getDimensionalFactor()
	{
		if(level.dimension().location().equals(Dimensions.MENELAUS_ID))
		{
			return 1.6F;
		}
		
		return 1.0f;
	}
	
	@Override
	public void updateTile(int ticks) 
	{
		if(level.isClientSide)
			return;
		
		{
			//tick = 50;

			int light = level.getBrightness(LightType.SKY, worldPosition.above()) - level.getSkyDarken();
			light = (light < 0 ? 0 : light);
			
			power = (5.0F + light) * getDimensionalFactor();

			if(level.isThundering())
				power *= 2.0F;
			
			power *= (2.0F + getChipPower(EnumChipType.INDUSTRIE));
			
			this.energy.add((int)(power*ticks));
		}
		if(!items.get(0).isEmpty())
		{		
			if(items.get(0).getItem() instanceof IItemNeon)
			{
				IItemNeon ch = (IItemNeon) items.get(0).getItem();
				ItemStack it = items.get(0);
				if(ch.isNeonable(it))
				{
					int r = 1;
					r += getChipPower(EnumChipType.INDUSTRIE);
					r *= ticks;
					r = Math.min(r, ch.getMaxNeon(it) - ch.getNeon(it));
					r = Math.min(r, energy.get());
					
					if(r>0)
					{
						ch.addNeon(it, r);
						energy.use(r);
					}
				}					
			}		
		}
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
	}
	
	@Override
	public boolean isWorking() 
	{
		return true;
	}
	
	@Override
	public int getDefaultPowerUsage() 
	{
		return (int)power;
	}

	@Override
	public int getMaxStackSize() 
	{
		return 1;
	}

	@Override
	public boolean canPlaceItem(int var1, ItemStack var2) 
	{
		if(var1==0) 
		{
			if(var2 == null || var2.isEmpty())
			{
				return true;
			}
			if(var2.getItem() instanceof IItemNeon)
			{
				return ((IItemNeon)var2.getItem()).isNeonable(var2);
			}
		}
		return false;
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0};
	}

	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.PRODUCE;
	}
	
	@Override
	public int getProperty(int id) 
	{
		switch(id)
		{
		case 0:
			return (int) power;
		case 1:
			return energy.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:
			power = value;
			break;
		case 1:
			setEnergy(value);
			break;
		default:
			return;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 2;
	}

	@Override
	protected int getInventorySize()
	{
		return 1;
	}


}
