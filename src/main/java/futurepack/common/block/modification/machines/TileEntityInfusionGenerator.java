package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.item.DustItems;
import futurepack.common.modification.EnumChipType;
import net.minecraft.block.Blocks;
import net.minecraft.block.FlowerBlock;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.IItemProvider;
import net.minecraftforge.common.ForgeHooks;

public class TileEntityInfusionGenerator extends TileEntityMachineBase implements ISidedInventory, ITilePropertyStorage
{
//	private ItemStack[] items = new ItemStack[3];
	int burntime = 0;
	int maxbruntime = 0;

	public static final IItemProvider NEON_DUST = () -> DustItems.dust_neon;
	
	public TileEntityInfusionGenerator()
	{
		super(FPTileEntitys.PLASMA_GENERATOR);
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
	}
	

	@Override
	public void updateTile(int ticks)
	{
		if(level.isClientSide)
			return;
			
		if(!items.get(0).isEmpty() && !items.get(1).isEmpty() && burntime<=0 && (items.get(2).isEmpty() || items.get(2).getCount() < 64))
		{
			int i = getItemFuel(items.get(0));
			int j = getCristalPower(items.get(1));
			if(i>0 && j>0)
			{
				maxbruntime = burntime = i * j;
				items.get(0).shrink(1);
				items.get(1).shrink(1);
			}
			if(burntime >0)
			{
				level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockInfusionGenerator.LIT, true));
				setChanged();
			}
		}
		if(burntime > 0)
		{
			int add = Math.min(energy.getMax() - energy.get(), burntime);
			add = Math.min(add, getDefaultPowerUsage() * ticks);
			
			if(energy.add(add) > 0)
			{
				burntime-=add;
				if(burntime<=0)
				{
					if(System.currentTimeMillis()%10==0)
					{
						burntime=maxbruntime;
						maxbruntime=0;
					}
					else if(items.get(2).isEmpty())
					{
						items.set(2, new ItemStack(NEON_DUST, 1));
					}
					else
					{
						if(items.get(2).getItem() == NEON_DUST.asItem())
						{
							items.get(2).grow(1);
						}
					}
					setChanged();
				}
			}
		}
		else
		{
			if(getBlockState().getValue(BlockInfusionGenerator.LIT))
			{
				level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockInfusionGenerator.LIT, false));
				setChanged();
			}
		}
			
		if(items.get(0).isEmpty())
		{
			items.set(0, ItemStack.EMPTY);
		}
		if(items.get(1).isEmpty())
		{
			items.set(1, ItemStack.EMPTY);
		}	
	}
	
	private int getCristalPower(ItemStack it) 
	{
		if(it.getItem()==TerrainBlocks.crystal_neon.asItem())
		{
			return 2;
		}
		else if(it.getItem()==TerrainBlocks.crystal_alutin.asItem())
		{
			return 1;
		}
		else if(it.getItem()==TerrainBlocks.crystal_retium.asItem())
		{
			return 3;
		}
		else if(it.getItem()==TerrainBlocks.crystal_glowtite.asItem())
		{
			return 4;
		}
		else if(it.getItem()==TerrainBlocks.crystal_bioterium.asItem())
		{
			return 5;
		}
		
		
		return 0;
	}

	public int getItemFuel(ItemStack it)
	{
		Item item = it.getItem();
		float factor = (int) (1 + (getChipPower(EnumChipType.INDUSTRIE)/10));
		
		if(item.isEdible())
		{
			Food food = item.getFoodProperties();
			return (int) ((food.getNutrition() * 100.0f * food.getSaturationModifier()) * factor);
		}
		else if(item instanceof BlockItem && ((BlockItem)item).getBlock() instanceof FlowerBlock)
		{
			return (int) (200F * factor);
		}
		else if(item != Items.LAVA_BUCKET && item != Items.COAL && item != Blocks.COAL_BLOCK.asItem() && ForgeHooks.getBurnTime(it)>0)
		{
			return (int) (ForgeHooks.getBurnTime(it) * 2F * factor);
		}
		return 0;
	}

	
	@Override
	public void writeData(CompoundNBT nbt)
	{
		super.writeData(nbt);
		nbt.putInt("burntime", burntime);
	}
	
	@Override
	public void readData(CompoundNBT nbt)
	{
		super.readData(nbt);
		burntime = nbt.getInt("burntime");
	}
	
	@Override
	public boolean canPlaceItem(int var1, ItemStack it) 
	{
		if(var1==0)
		{
			return it.getItem().isEdible() || ForgeHooks.getBurnTime(it)>0;
		}
		else if(var1==1)
		{
			return getCristalPower(it) > 0;
		}
		else
		{	
			return var1 != 2;
		}
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1,2};
	}

	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		if(!storage.canInsert(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
			
		if(slot==0||slot==1)
			return canPlaceItem(slot, var2);
		
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		if(!storage.canExtract(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
			
		if(slot==2)
			return true;
		
		return false;
	}

	
	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.PRODUCE;
	}

	@Override
	public boolean isWorking() 
	{
		return true;
	}
	
	

	@Override
	public int getDefaultPowerUsage() 
	{
		return (int) (20 * (2+getChipPower(EnumChipType.INDUSTRIE)));
	}

	public int getProgress() 
	{
		return burntime;
	}

	public void setProgress(int val) 
	{
		this.burntime = val;
	}

	public boolean isBurning() 
	{
		return burntime > 0;
	}
	
	
	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:
			return this.energy.get();
		case 1:
			return this.burntime;
		default:
			return 0;
		}	
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			this.burntime = value;
			break;
		default:
			break;
		}	
	}

	@Override
	public int getPropertyCount() 
	{
		return 2;
	}

	
	@Override
	public void clearContent() { }

	@Override
	protected int getInventorySize()
	{
		return 3;
	}
}
