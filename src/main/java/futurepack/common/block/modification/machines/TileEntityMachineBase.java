package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

public abstract class TileEntityMachineBase extends TileEntityAbstractMachine implements ISidedInventory
{
	protected NonNullList<ItemStack> items;
	protected final LogisticStorage storage;
	
	public TileEntityMachineBase(TileEntityType<? extends TileEntityMachineBase> type)
	{
		super(type);
		items = NonNullList.withSize(getInventorySize(), ItemStack.EMPTY);
		
		storage = new LogisticStorage(this::onLogisticChange, getLogisticTypes());
		configureLogisticStorage(storage);
	}

	protected abstract int getInventorySize();
	
	@Override
	public IItemHandler getItemHandler(Direction side)
	{
		return new SidedInvWrapper(this, side);
	}
	
	@Override
	public LogisticStorage getLogisticStorage() 
	{
		return storage;
	}
	
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS};
	}
	
	public abstract void configureLogisticStorage(LogisticStorage storage);
	

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		ListNBT l = new ListNBT();
		for(int i=0;i<items.size();i++)
		{
			if(!items.get(i).isEmpty())
			{
				CompoundNBT tag = new CompoundNBT();
				items.get(i).save(tag);
				tag.putInt("slot", i);
				l.add(tag);
			}
		}
		nbt.put("Items", l);
		storage.write(nbt);
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		items.clear();
		ListNBT l = nbt.getList("Items", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundNBT tag = l.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			setItem(tag.getInt("slot"), is);
		}
		storage.read(nbt);
	}
	
	@Override
	public int getContainerSize() 
	{
		return items.size();
	}

	@Override
	public ItemStack getItem(int var1) 
	{
		if(var1<items.size())
		{
			return items.get(var1);
		}
		return null;
	}

	@Override
	public ItemStack removeItem(int slot, int amount)
    {
        if (this.items.get(slot) != null)
        {
            ItemStack itemstack;

            if (this.items.get(slot).getCount() <= amount)
            {
                itemstack = this.items.get(slot);
                setItem(slot, ItemStack.EMPTY);
                this.setChanged();
                return itemstack;
            }
            else
            {
                itemstack = this.items.get(slot).split(amount);

                if (this.items.get(slot).getCount() == 0)
                {
                	setItem(slot, ItemStack.EMPTY);
                }

                this.setChanged();
                return itemstack;
            }
        }
        else
        {
            return ItemStack.EMPTY;
        }
    }

	@Override
	public ItemStack removeItemNoUpdate(int index)
	{
		if(getItem(index)!=null)
		{
			ItemStack stack = items.get(index);
			setItem(index, ItemStack.EMPTY);
			return stack;
		}
		return null;
	}
	
	@Override
	public void setItem(int var1, ItemStack var2) 
	{
		if(var1 < items.size())
		{
			items.set(var1, var2);
			this.setChanged();
		}
	}

	@Override
	public int getMaxStackSize() 
	{
		return 64;
	}

	//TODO: this must to ALL TileEntitys
	@Override
	public boolean stillValid(PlayerEntity var1)
	{
		return HelperResearch.isUseable(var1, this);
	}

	@Override
	public void startOpen(PlayerEntity pl) {}

	@Override
	public void stopOpen(PlayerEntity pl) {}

	@Override
	public boolean canPlaceItem(int slot, ItemStack stack) 
	{
		return true;
	}

	@Override
	public void clearContent() { }
	
	@Override
	public boolean isEmpty()
	{
		return false;
	}
	
	private int[] allSlots;
	
	@Override
	public int[] getSlotsForFace(Direction side)
	{
		if(allSlots==null)
		{
			allSlots = new int[getContainerSize()];
			for(int i=0;i<allSlots.length;i++)
				allSlots[i] = i;
		}
		return allSlots;
	}
	
	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack it, Direction side)
	{
		return storage.canInsert(side, EnumLogisticType.ITEMS) && canPlaceItem(slot, it);
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{		
		return storage.canExtract(side, EnumLogisticType.ITEMS);
	}
}
