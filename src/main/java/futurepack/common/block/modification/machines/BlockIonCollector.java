package futurepack.common.block.modification.machines;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.block.BlockHoldingTile;
import futurepack.common.item.misc.ItemBatterie;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockIonCollector extends BlockHoldingTile
{

	public BlockIonCollector(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityIonCollector();
	}
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.ION_COLLECTOR.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public boolean hasAnalogOutputSignal(BlockState state)
    {
        return true;
    }
	
	@Override
	public int getAnalogOutputSignal(BlockState state, World w, BlockPos pos)
    {
		TileEntityIonCollector tile = (TileEntityIonCollector)w.getBlockEntity(pos);
		if(tile.getItem(0)!=null&&tile.getItem(0).getItem() instanceof ItemBatterie)
		{
			IItemNeon neon = (IItemNeon) tile.getItem(0).getItem();
			float d = neon.getNeon(tile.getItem(0));
			if(d<=0)
				return 0;
			float m = neon.getMaxNeon(tile.getItem(0));
			
			return (int) ((d/m)*15);
		}
		
		return 0;
    }
}
