package futurepack.common.block.modification.machines;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockSolarPanel extends BlockHoldingTile
{
	
	public BlockSolarPanel(Block.Properties props)
	{
		super(props);
	}

	
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.SOLAR_PANEL.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}


	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntitySolarPanel();
	}


}
