package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPLog;
import futurepack.common.FPTileEntitys;
import futurepack.common.recipes.centrifuge.FPZentrifugeManager;
import futurepack.common.recipes.centrifuge.ZentrifugeRecipe;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.block.BlockState;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;

public class TileEntityZentrifuge extends TileEntityMachineSupport implements ISidedInventory, ITilePropertyStorage
{

	private int progress=0, maxprogress=1;
	private ZentrifugeRecipe last;
	
	public TileEntityZentrifuge()
	{
		super(FPTileEntitys.ZENTRIFUGE, 1024, EnumEnergyMode.USE);
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.SUPPORT);
		
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
		
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS, EnumLogisticType.SUPPORT};
	}
	
	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public void updateTile(int ticks)
	{
		if(!level.isClientSide)
		{
			if(!items.get(0).isEmpty())
			{
			 	ZentrifugeRecipe rec = FPZentrifugeManager.instance.getMatchingRecipe(items.get(0), false);
				if(rec!=null)
				{
					if(rec!=last && last!=null)
					{
						progress = 0;
					}
					else if(last==null)
					{
						setChanged();
					}
					
					boolean continueRecipe;
					
					do
					{
						continueRecipe = false;

						if(areItemsMatching(rec) && support.get()>= rec.getNeededSupport())
						{			
							progress+= ticks;
							ticks = 0;
							int maxTime = rec.getTime();
							if(progress>=maxTime)
							{
								progress -= maxTime;
								continueRecipe = progress>=maxTime;
								support.use(rec.getNeededSupport());
								
								items.get(0).shrink(rec.getInput().getMinimalItemCount(items.get(0)));
								if(items.get(0).getCount()<=0)
								{
									items.set(0, ItemStack.EMPTY);
								}
								
								Mian:
								for(ItemStack it : rec.getOutput())
								{
									if(it==null || it.isEmpty())
										continue;
									
									for(int i=1;i<items.size();i++)
									{
										if(items.get(i).isEmpty())
										{
											items.set(i,  it);
											continue Mian;
										}
										else if(HelperInventory.canStack(items.get(i), it))
										{
											if(items.get(i).getCount() + it.getCount() <= it.getMaxStackSize())
											{
												items.get(i).grow(it.getCount());
												continue Mian;
											}
											else
											{
												int used = it.getMaxStackSize() - items.get(i).getCount();
												items.get(i).grow(used);
												it.shrink(used);
											}
										}
									}
									if(it.getCount()>0)
									{
										FPLog.logger.error("Something went wrong. Cant add All Items to Zentrufuge!! (" + it +")");
									}
								}
							}
						}
						else
						{
							progress = 0;
						}
					}
					while(continueRecipe);
					
					last = rec;
				}
				else
				{
					last = null;
					progress = 0;
					setChanged();
				}
				maxprogress = last!=null ? last.getTime() : 1;
			}
			else
			{
				last = null;
				progress = 0;
				maxprogress = 1;
				setChanged();
			}
			
			if(isWorking() != getBlockState().getValue(BlockZentrifuge.ACTIVATED))
			{
				level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockZentrifuge.ACTIVATED, isWorking()));
			}
		}
	}
	
	private boolean areItemsMatching(ZentrifugeRecipe rec)
	{
		if(rec.getOutput().length>3)
		{
			return false;
		}
			
		if(items.get(1).isEmpty() && items.get(2).isEmpty() && items.get(3).isEmpty())
		{
			return true;
		}
		boolean[] used = new boolean[3];
		for(ItemStack it : rec.getOutput())
		{
			boolean stacked = false;
			for(int i=1;i<items.size();i++)
			{
				if(items.get(i).isEmpty() && !used[i-1])
				{
					used[i-1] = true;
					stacked = true;
					break;
				}
				else if(!items.get(i).isEmpty())
				{
					if(HelperInventory.canStack(items.get(i), it))
					{
						if(items.get(i).getCount() + it.getCount() <= it.getMaxStackSize())
						{
							stacked = true;
							break;
						}
					}
				}
			}	
			if(!stacked)
			{
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public boolean isWorking()
	{
		return progress>0;
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		storage.write(nbt);
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		storage.read(nbt);
	}
	
	@Override
	public void writeData(CompoundNBT nbt)
	{
		super.writeData(nbt);
		nbt.putInt("progress", progress);
	}
	
	@Override
	public void readData(CompoundNBT nbt)
	{
		super.readData(nbt);
		progress = nbt.getInt("progress");
	}
	
	@Override
	public void clearContent() { }

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return energy.get();
		case 1:
			return progress;
		case 2:
			return support.get();
		case 3:
			return maxprogress;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			progress = value;
			break;
		case 2:
			support.set(value);
			break;
		case 3:
			maxprogress = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 4;
	}


	@Override
	public int[] getSlotsForFace(Direction side)
	{
		return new int[]{0,1,2,3};
	}


	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack item, Direction side)
	{
		if(!storage.canInsert(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		
		if(slot==0)
		{
			return canPlaceItem(slot, item);
		}
			
		return false;
	}
	
	@Override
	public boolean canPlaceItem(int var1, ItemStack var2) 
	{
		if(var1==0)
		{
			ZentrifugeRecipe rec = FPZentrifugeManager.instance.getMatchingRecipe(var2, true);
			return rec != null;
		}
		return true;
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack it, Direction side)
	{
		if(!storage.canExtract(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		
		if(slot>=1 && slot <= 3)
		{
			return true;
		}
		return false;
	}


	public float getProgress()
	{
		return progress / (float)maxprogress;
	}


	@Override
	protected int getInventorySize()
	{
		return 4;
	}

}
