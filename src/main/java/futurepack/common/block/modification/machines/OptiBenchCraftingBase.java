package futurepack.common.block.modification.machines;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.annotation.Nonnull;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

public abstract class OptiBenchCraftingBase
{
	//vars
    private BlueprintRecipe blueprint;
	private ItemStack last = ItemStack.EMPTY;

	//abstract methods
	protected abstract ItemStack getBlueprintItem();

	protected abstract NonNullList<ItemStack> getAllCraftingItems();

	protected abstract int getCraftingSlots();

	protected abstract int getListOffset();

	protected abstract ItemStack getOutputSlot();

	protected abstract void setOutputSlot(ItemStack stack);

	protected abstract List<ItemStack> getOverfillingItems();

	protected abstract void setProgress(int progress);

	protected abstract int getProgress();

	protected abstract int getMaxProgress();
	
	protected abstract void markDirty();

	//implemented methods

	protected void resetProgress()
	{
		setProgress(0);
	}
	
	protected ItemStack getCraftingSlot(int i)
	{
		if(i < getCraftingSlots())
			return getAllCraftingItems().get(getListOffset() +i);
		else
			return ItemStack.EMPTY;
	}

	protected void setCraftingSlot(int slot, ItemStack stack)
	{
		getAllCraftingItems().set(getListOffset() +slot, stack);
		markDirty();
	}

    protected BlueprintRecipe getBlueprint(World world)
	{
		if(blueprint!=null)
		{
			return blueprint;
		}
		else if(!getBlueprintItem().isEmpty() && getBlueprintItem().hasTag()&& getBlueprintItem().getTag().contains("recipe") && world!=null)
		{
			blueprint = BlueprintRecipe.createFromItem(getBlueprintItem(), world);
			return blueprint;
		}
		else
		{
			blueprint = null;
			return null;
		}
	}

	public boolean canInsertToOutputSlot(ItemStack toInsert)
	{
		return getOutputSlot().isEmpty() || (ItemStack.isSame(getOutputSlot(), toInsert) && ItemStack.tagMatches(getOutputSlot(), toInsert) && getOutputSlot().getCount()+toInsert.getCount()<=toInsert.getMaxStackSize());
	}

	public void shrinkCraftItems()
	{
		for(int i=0;i<getCraftingSlots();i++)
		{
			ItemStack it2 = getCraftingSlot(i);
			if(!it2.isEmpty())
			{
				it2.shrink(1);
				if(it2.getCount()<=0)
				{
					it2 = ItemStack.EMPTY;
				}
				setCraftingSlot(i, it2);
			}
		}
	}

	NonNullList<ItemStack> lastRemainingItems = null;

	@Nonnull
	public ItemStack getCurrentCrafting(World world)
	{		
		if(getBlueprint(world)!=null)
		{
			IRecipe rec = getBlueprint(world).getRecipe();
			CraftingInventory inv = getBlueprint(world).getCraftingTable(getAllCraftingItems(), getListOffset());
			
			if(rec!=null && rec.matches(inv, world))
			{
				ItemStack original = rec.assemble(inv);
				lastRemainingItems = rec.getRemainingItems(inv);
				return original.copy();
			}
		}
		return ItemStack.EMPTY;
	}

	public void addOverfillingItem(ItemStack tooMuch)
	{
		getOverfillingItems().add(tooMuch);
		markDirty();
	}

	public boolean isItemValidForCraftingSlot(int slot, ItemStack it, World world)
	{
		if(slot < getCraftingSlots())
		{
			if(getBlueprint(world)!=null)
			{
				return getBlueprint(world).isItemValid(it, slot);			
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public void sortCrafting(World world)
	{
		ArrayList<ItemStack> wrongItems = new ArrayList<>(getCraftingSlots());

		ArrayList<Integer> validSlots = new ArrayList<>(getCraftingSlots());
		ArrayList<Integer> emptySlots = new ArrayList<>(getCraftingSlots());
		// check slots if they are empty, correcctly filled or have wrong items (and remove the wrong items)
		for(int i=0;i<getCraftingSlots();i++)
		{
			ItemStack it = getCraftingSlot(i);
			if(it.isEmpty())
			{
				emptySlots.add(i);
			}
			else if(isItemValidForCraftingSlot(i, it, world))
			{
				if(it.getCount() > 1)
					validSlots.add(i);
			}
			else
			{
				wrongItems.add(it);
				setCraftingSlot(i, ItemStack.EMPTY);
				emptySlots.add(i);
			}
		}
		
		if(!validSlots.isEmpty() || !wrongItems.isEmpty())
		{
			Outer:
			for(int slot : emptySlots)
			{
				//first try to readd removed items
				for(ItemStack it : wrongItems)
				{
					if(isItemValidForCraftingSlot(slot, it, world))
					{
						setCraftingSlot(slot, it);
						wrongItems.remove(it);
						continue Outer;
					}
				}
				//try to balance the correct items with the empty slots
				for(int slotFilled : validSlots)
				{
					ItemStack it = getCraftingSlot(slotFilled);
					if(isItemValidForCraftingSlot(slot, it, world))
					{
						ItemStack sub = it.split(1);
						setCraftingSlot(slot, sub);
						if(it.getCount() <= 1)
						{
							validSlots.remove((Integer)slotFilled);
						}
						continue Outer;
					}	
				}
				
			}
		}
		getOverfillingItems().addAll(wrongItems);
	}

	public boolean craft(World world)
	{
		ItemStack currentOutput = getCurrentCrafting(world);
		if(!currentOutput.isEmpty() && canInsertToOutputSlot(currentOutput))
		{
			if(getOutputSlot().isEmpty())
			{
				setOutputSlot(currentOutput);
			}
			else
			{
				getOutputSlot().grow(currentOutput.getCount());
			}

			if(lastRemainingItems!=null)
			{
				getOverfillingItems().addAll(lastRemainingItems);
				lastRemainingItems = null;
			}
			shrinkCraftItems();

			return true;
		}
		else
		{
			sortCrafting(world);
			return false;
		}
	}

	public void updateRecipe(World world)
	{
		if(world!=null && world.isClientSide)		
			return;
		
		
		ItemStack it = getCurrentCrafting(world);
		
		if(!ItemStack.matches(it, last) )
		{
			resetProgress();
			last = it;
		}
	
		blueprint = null;
	}
	
	public void ejectOverfillingItems()
	{
		while(!getOverfillingItems().isEmpty())
		{
			ItemStack stack = getOverfillingItems().get(0);
			if(stack==null || stack.isEmpty())
			{
				getOverfillingItems().remove(0);
			}
			else if(canInsertToOutputSlot(stack))
			{
				if(getOutputSlot().isEmpty())
				{
					setOutputSlot(getOverfillingItems().remove(0));
				}
				else
				{
					getOutputSlot().grow(getOverfillingItems().remove(0).getCount());
				}
			}
			else
			{
				return;
			}
		}
	}

	public boolean tryCrafting(int ticks, World world, Consumer<OptiBenchCraftingBase> craftFinishedCallback)
	{
		if(getOverfillingItems().isEmpty())
		{
			boolean success = false;
			do
			{
				ItemStack it = getCurrentCrafting(world);
				if(it.isEmpty())
				{
					resetProgress();
				}
				else if(canInsertToOutputSlot(it))
				{		
					setProgress(getProgress() + ticks);
					ticks = 0;	
					if(getProgress() >=  getMaxProgress())
					{
						if(craft(world))
						{
							setProgress(getProgress() - getMaxProgress());
							craftFinishedCallback.accept(this);
							success = true;
						}
					}
						
				}
				else
				{
					resetProgress();
				}
			}
			while(getProgress() >= getMaxProgress());

			return success;
		}
		else
		{
			ejectOverfillingItems();
			return false;
		}
	}

}
