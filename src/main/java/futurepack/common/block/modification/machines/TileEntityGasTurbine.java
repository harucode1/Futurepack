package futurepack.common.block.modification.machines;

import java.util.function.Supplier;

import futurepack.api.Constants;
import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IFluidTankInfo;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.inventory.ItemHandlerGuiOverride;
import futurepack.common.block.inventory.ItemStackHandlerGuis;
import futurepack.common.block.logistic.LogisticFluidWrapper;
import futurepack.common.fluids.FPFluids;
import futurepack.common.modification.EnumChipType;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidActionResult;
import net.minecraftforge.fluids.FluidAttributes;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.IItemHandler;

import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;

public class TileEntityGasTurbine extends TileEntityAbstractMachine implements ITilePropertyStorage 
{
	private static final Supplier<Fluid> getBiogas = () -> FPFluids.biogasFluidStill;
	
	private LogisticStorage storage;
	private BioGasTank gas;
	private ItemContainer handler;
	
	private final LazyOptional<IFluidHandler>[] fluidHandlerOpt;
	
	private int speed;
	private boolean working; //For GUI
	
	@SuppressWarnings("unchecked")
	public TileEntityGasTurbine()
	{
		super(FPTileEntitys.GAS_TURBINE);
		storage = new LogisticStorage(this::onLogisticChange, EnumLogisticType.ITEMS, EnumLogisticType.FLUIDS, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.FLUIDS);
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.FLUIDS);
		
		gas = new BioGasTank(FluidAttributes.BUCKET_VOLUME*8);
		
		handler = new ItemContainer();
		
		speed = 1;
		
		fluidHandlerOpt = new LazyOptional[6];
	}
	
	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		super.onLogisticChange(face, type);
		if(type == EnumLogisticType.FLUIDS)
		{
			if(fluidHandlerOpt[face.get3DDataValue()]!=null)
			{
				fluidHandlerOpt[face.get3DDataValue()].invalidate();
				fluidHandlerOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	@Override
	public IItemHandler getItemHandler(Direction face)
	{
		return handler;
	}
	
	@Override
	public LogisticStorage getLogisticStorage() 
	{
		return storage;
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.PRODUCE;
	}
	
	
	
	@Override
	public void updateTile(int ticks) 
	{
		if(!level.isClientSide)
		{
			working = false;
			
			if(this.speed <= 0)
			{
				this.speed = 1;
			}
			
			int speed = this.speed * ticks;
			//TODO reimplement on/off textures by updating LIT blocksatte bof gas turbine
			if(gas.getFluidAmount() >= speed)
			{
				float e = (getDefaultPowerUsage() / 10.0f) * speed;
				if(this.energy.add((int) e) > 0)
				{
					gas.drainInternal(speed, true);
					working = true;
					if(level.random.nextInt(100) < 10)
					{
						this.speed = this.speed < 10 ? this.speed + 1 : this.speed;
						updateData();
					}
				}
				else
				{
					if(level.random.nextInt(100) < 20)
					{
						this.speed = this.speed > 1 ? this.speed - 1 : this.speed;
						updateData();
					}
				}
			}
			else
			{
				if(level.random.nextInt(100) < 20)
				{
					this.speed = this.speed > 1 ? this.speed - 1 : this.speed;
					updateData();
				}
			}
		}
	}
	
	@Override
	public void updateNaturally()
	{
		if(!level.isClientSide)
		{
			if(!handler.getStackInSlot(0).isEmpty())
			{
				FluidActionResult res = FluidUtil.tryEmptyContainer(handler.getStackInSlot(0), gas, 8000, null, true);
				if(res.isSuccess())
				{
					handler.setStackInSlot(0, res.getResult());
				}
			}
		}
	}
	
	
	@Override
	public int getDefaultPowerUsage() 
	{
		return (int) (20 * (2+getChipPower(EnumChipType.INDUSTRIE)));
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		gas.readFromNBT(nbt.getCompound("gas"));
		storage.read(nbt);
		handler.deserializeNBT(nbt.getCompound("items"));
		speed = nbt.getInt("speed");
		working = nbt.getBoolean("working");
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		CompoundNBT tag = new CompoundNBT();
		gas.writeToNBT(tag);
		nbt.put("gas", tag);
	
		storage.write(nbt);
		
		CompoundNBT items = handler.serializeNBT();
		nbt.put("items", items);
		
		nbt.putInt("speed", speed);
		
		nbt.putBoolean("working", working);

		return nbt;
	}
	
	
	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:
			if(gas.getFluid()!=null)
				return 1;
			return -1;
		case 1:
			return gas.getFluidAmount();
		case 2:
			return energy.get();
		case 3:
			return working ? speed : 0;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			if(value==-1)
				gas.setFluid(null);
			else
			{
				gas.setFluid(new FluidStack(getBiogas.get(), 1));			
			}
			break;
		case 1:
			if(gas.getFluid()!=null)
				gas.getFluid().setAmount(value);
			break;
		case 2:
			energy.set(value);
			break;
		case 3:
			speed = value;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 4;
	}

	@Override
	public boolean isWorking() 
	{
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return LazyOptional.empty();
		
		if(capability==CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			if(fluidHandlerOpt[facing.get3DDataValue()] != null)
			{
				return (LazyOptional<T>) fluidHandlerOpt[facing.get3DDataValue()];
			}
			else
			{
				if(getLogisticStorage().getModeForFace(facing, EnumLogisticType.FLUIDS) == EnumLogisticIO.NONE)
				{
					LazyOptional.empty();
				}
				else
				{
					fluidHandlerOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new LogisticFluidWrapper(getLogisticStorage().getInterfaceforSide(facing), gas));
					fluidHandlerOpt[facing.get3DDataValue()].addListener(p -> fluidHandlerOpt[facing.get3DDataValue()]=null);
					return (LazyOptional<T>) fluidHandlerOpt[facing.get3DDataValue()];
				}
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidHandlerOpt);
		super.setRemoved();
	}

	private class ItemContainer extends ItemStackHandlerGuis
	{

		public ItemContainer()
		{
			super(1);
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(isItemValid(slot, stack, gui))		
				return super.insertItem(slot, stack, simulate, gui);
			
			return stack;
		}
		
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			if(slot == 0)
	    	{
				return FluidUtil.tryEmptyContainer(stack, new BioGasTank(8000), 8000, null, false).isSuccess();
	    	}
			
			return false;
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(slot==0)
			{
				if(gui || !isItemValid(slot, getStackInSlot(0), gui))
					return super.extractItem(slot, amount, simulate, gui);
			}
			return ItemStack.EMPTY;
		}
		
		@Override
		public int getSlotLimit(int slot)
		{
			return 1;
		}
		
	}

	public IItemHandler getGui()
	{
		return new ItemHandlerGuiOverride(handler);
	}

	
	class BioGasTank extends FluidTank
	{

		public BioGasTank(int capacity) 
		{
			super(capacity);
		}
		
		public void drainInternal(int amountDrained, boolean execute) 
		{
			super.drain(amountDrained, execute ? FluidAction.EXECUTE : FluidAction.SIMULATE);
		}

		@Override
	    public boolean isFluidValid(FluidStack fluid)
	    {
	        return fluid.getFluid() == getBiogas.get();
	    }
		
		@Override
		public FluidStack drain(FluidStack resource, FluidAction action) 
		{
			return FluidStack.EMPTY;
		}
		
		@Override
		public FluidStack drain(int maxDrain, FluidAction action) 
		{
			return FluidStack.EMPTY;
		}
	}

	public IFluidTankInfo getGas() 
	{
		return new IFluidTankInfo.FluidTankInfo(gas, 0);
	}
	
	public int getSpeed()
	{
		return speed;
	}

	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		load(getBlockState(), pkt.getTag());
//		world.HelperChunks.renderUpdate(w, pos);(pos, pos);
		level.markAndNotifyBlock(worldPosition, level.getChunkAt(getBlockPos()), getBlockState(), getBlockState(), 3, Constants.RECURSIVE_BLOCKUPDATES);
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pack;
	}
	
	private boolean last_state = false;
	
	public void updateData()
	{
		boolean now = working;
		if(now != last_state)
		{
			last_state = now;
		
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
		}
		
		//markDirty();
	}
	
	public boolean isWorkingClient()
	{
		return working;
	}
	
}
