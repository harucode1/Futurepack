package futurepack.common.block.modification.machines;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import futurepack.api.interfaces.IItemSupport;
import futurepack.common.FPConfig;
import futurepack.common.FPSounds;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityOptiBench extends TileEntityOptiBenchBase
{
	public TileEntityOptiBench()
	{
		super(FPTileEntitys.OPTI_BENCH);

		registerCrafter(LazyOptional.of(() -> selfCrafter));
	}

	public static final int FIELD_PROGRESS = 0;

//	private ItemStack last = ItemStack.EMPTY;
//	private ItemStack[] left;
	private long lasttime = 0;
	private int crafterPos = 0;

	private ArrayList<ItemStack> overfilling = new ArrayList<>();

	private ArrayList<LazyOptional<OptiBenchCraftingBase>> connectedCrafters = new ArrayList<>();
	private Crafting selfCrafter = new Crafting();
//	private BlueprintRecipe blueprint;
	
	private int tickdiv = 0;
	@Override
	public void updateNaturally()
	{
		tickdiv++;
		if(!level.isClientSide && tickdiv == 4 )	
		{	
			tickdiv = 0;
			if(getCrafter().getBlueprint(level)!=null)
			{
				getCrafter().sortCrafting(level);
			}
		}
		
		if(System.currentTimeMillis() - lasttime >= 1228 && progress>0)
		{
			level.playLocalSound(worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5, FPSounds.OPTIBENCH, SoundCategory.BLOCKS, (float) (0.2F * FPConfig.CLIENT.volume_optibench.get()), 1.0F, false);
			lasttime = System.currentTimeMillis();
		}
	}
	
	@Override
	public void updateTile(int ticks) 
	{
		for(int i=0;i<items.size();i++)
		{
			ItemStack it = items.get(i);
			if(!it.isEmpty() && it.getCount()<= 0)
			{
				items.set(i, ItemStack.EMPTY);
			}
		}
		
		
		if(!level.isClientSide)	
		{
			if(this.support.get() > 0 && !items.get(11).isEmpty() && items.get(11).getItem() instanceof IItemSupport)
			{
				IItemSupport ai = (IItemSupport) items.get(11).getItem();
				int space = ai.getMaxSupport(items.get(11)) - ai.getSupport(items.get(11));
				if(space > 0)
				{
					ai.addSupport(items.get(11), support.use(Math.min(space, ticks)));
				}
			}
			
			Consumer<OptiBenchCraftingBase> craftCallback = c -> 
			{
				if(support.get()>=support.getMax() && !c.getOutputSlot().isEmpty())
				{
					support.use(support.getMax());
					c.getOutputSlot().grow(1);
				}
							
				support.add(1);
			};

			if(!getCrafter().tryCrafting(ticks, level, craftCallback))
			{
				nextCrafter();
			}

		}
		if(support.get()>0 && this.getChipPower(EnumChipType.SUPPORT)>0)
		{
			HelperEnergyTransfer.sendSupportPoints(this);
		}
	}
	
/*	private void sortCrafting()
	{
		if(true)
		{
			crafting.sortCrafting(world);
			return;
		}

		/*
		//get Crafting Recep.
		
		//list all items
		ArrayList<Integer> lis = new ArrayList<Integer>(9);
		for(int i=0;i<9;i++)
		{
			ItemStack it = items.get(i);
			if(!it.isEmpty())
			{
				boolean add = true;
				for(int j=0;j<lis.size();j++)
				{
					ItemStack is = items.get(lis.get(j));
					
					if(HelperInventory.areItemsEqualNoSize(it,is))
					{
						is.grow(it.getCount());
						items.set(i, ItemStack.EMPTY);
						add = false;
						break;
					}
				}
				if(add)
				{
					lis.add(i);
				}
			}
		}
		
		if(lis.isEmpty())
			return;
		
		//Sort items in
		for(int i=0; i < lis.size(); i++ )
		{
			int slot = lis.get(i);
			ItemStack it = items.get(slot);
			int smax = it.getMaxStackSize();
			//suche platz f�r Item
			ArrayList<Integer> plist = new ArrayList<Integer>(9);
			ArrayList<Integer> uber = new ArrayList<Integer>(9);
			for(int j=0; j<9; j++)
			{
				if(blueprint.isItemValid(it, j))
				{
					if(items.get(j).isEmpty())
					{
						plist.add(j);
					}
					else if(HelperInventory.areItemsEqualNoSize(items.get(j), it))
					{
						plist.add(j);
						items.set(j, ItemStack.EMPTY);
					}
				}
				else if(!items.get(j).isEmpty() && HelperInventory.areItemsEqualNoSize(items.get(j), it))
				{
					uber.add(j);
				}
			}
			
			if(plist.size() == 0)
			{
				//whenn das item nicht ins Rezept geh�rt
				if(items.get(9).isEmpty())
				{
					items.set(9,  it);
					items.set(slot, ItemStack.EMPTY);
				}
				else if(HelperInventory.areItemsEqualNoSize(items.get(9), it) && items.get(9).getCount() < smax)
				{
					if(items.get(9).getCount() + it.getCount() > smax)
					{
						int diff = items.get(9).getCount() + it.getCount() -smax;
						items.get(9).setCount(smax);
						it.setCount(diff);
					
					}
					else
					{
						items.get(9).grow(it.getCount());
						items.set(slot, ItemStack.EMPTY);
					}
				}
				continue;
			}
			
			int size = it.getCount() / plist.size();
			int rest = it.getCount() % plist.size();
			
			
			if((size+rest) > smax)
			{
				int urest = (size - smax)*plist.size() + rest;
				size = smax; 
				rest = 0;
				
				//Rest zur�cklegen
				for(int j = 0; j < uber.size(); j++) {
					if(urest > smax)
					{
						ItemStack uit = it.copy();
						uit.setCount(smax);
						items.set(uber.get(j), uit);
						urest -= smax;
					}
					else if(urest == 0)
					{
						items.set(uber.get(j), ItemStack.EMPTY);
					}
					else
					{
						ItemStack uit = it.copy();
						uit.setCount(urest);
						items.set(uber.get(j), uit);
						urest = 0;
					}
				}
			}
			else
			{
				for(int j = 0; j < uber.size(); j++)
					items.set(uber.get(j), ItemStack.EMPTY);
			}
			
			int placed = 0;
			
			//items einsortieren
			for(int j = 0; j < plist.size(); j++)
			{
					ItemStack tit = it.copy();
					tit.setCount(size);
					placed += size;
					if(j == 0) {
						tit.grow(rest);
						placed += rest;
					}
					items.set(plist.get(j), tit);
					if(size == 0 || placed >= it.getCount())
						break;
			}
			
			//Wait we might loose items
			if(placed < it.getCount()) {
				
				int realPlaced = 0;
				
				for(int k = 0; k < 9; k++) {
					if(HelperInventory.areItemsEqualNoSize(items.get(k), it)) {
						realPlaced += items.get(k).getCount();
					}
				}
				if(realPlaced <= placed) {
					if(items.get(9).isEmpty()) {
						ItemStack restStack = it.copy();
						
						int restCount = restStack.getCount() - placed;
						if(restCount <= restStack.getMaxStackSize())
							restStack.setCount(restCount);
						else
							restStack.setCount(restStack.getMaxStackSize());
						
						items.set(9, restStack);
					}
					else { //Okay what to do now... .-. Panic 
						
						ItemStack restStack = it.copy();
						
						int restCount = restStack.getCount() - placed;
						if(restCount <= restStack.getMaxStackSize())
							restStack.setCount(restCount);
						else
							restStack.setCount(restStack.getMaxStackSize());
						
						this.world.addEntity(new ItemEntity(world, this.getPos().getX(), this.getPos().getY() + 1, this.getPos().getZ(), restStack));
					}
				}
			}
		}
		
		
	}
*/


	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		ListNBT l = new ListNBT();
		for(int i=0;i<overfilling.size();i++)
		{
			if(overfilling.get(i)!=null && !overfilling.get(i).isEmpty())
			{
				CompoundNBT tag = new CompoundNBT();
				overfilling.get(i).save(tag);
				l.add(tag);
			}
		}
		nbt.put("overfilling", l);
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		overfilling.clear();
		ListNBT l = nbt.getList("overfilling", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundNBT tag = l.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			overfilling.add(is);
		}
		super.load(state, nbt);
	}
	
	@Override
	public void updateRecipe()
	{
		selfCrafter.updateRecipe(level);
	}

//	@Nonnull
//	public ItemStack getCurentCrafting()
//	{		
//		return crafting.getCurrentCrafting(world);
//	}
 	
	@Override
	public boolean canPlaceItem(int slot, ItemStack var2) 
	{
		if(slot>=0 && slot<=8 && !var2.isEmpty())
		{
			return selfCrafter.isItemValidForCraftingSlot(slot, var2, level);
			
		}
		if(slot == 11)
		{
			return var2.getItem() instanceof IItemSupport;
		}
		return slot != 9;
	}

	
//	private BlueprintRecipe getBlueprint()
//	{
//		return crafting.getBlueprint(world);
//	}

	public class Crafting extends OptiBenchCraftingBase
	{
		@Override
		protected ItemStack getBlueprintItem()
		{
			return items.get(10);
		}

		@Override
		protected NonNullList<ItemStack> getAllCraftingItems()
		{
			return items;
		}
		
		@Override
		protected int getCraftingSlots()
		{
			return 9;
		}
		
		@Override
		protected int getListOffset()
		{
			return 0;
		}
		
		@Override
		protected ItemStack getOutputSlot()
		{
			return items.get(9);
		}
		
		@Override
		protected void setOutputSlot(ItemStack stack)
		{
			setItem(9, stack);
		}

		@Override
		protected List<ItemStack> getOverfillingItems()
		{
			return overfilling;
		}

		@Override
		protected void setProgress(int progress)
		{
			TileEntityOptiBench.this.progress = progress;
		}

		@Override
		protected int getProgress()
		{
			return TileEntityOptiBench.this.progress;
		}

		@Override
		protected int getMaxProgress()
		{
			return 10;
		}
		
		@Override
		protected void markDirty()
		{
			TileEntityOptiBench.this.setChanged();
		}
	}

	public void registerCrafter(LazyOptional<OptiBenchCraftingBase> externCrafter)
	{
		if(!connectedCrafters.contains(externCrafter))
		{
			connectedCrafters.add(externCrafter);
			externCrafter.addListener(p -> connectedCrafters.remove(externCrafter));
		}
	}

	public OptiBenchCraftingBase getCrafter()
	{
		for(int i=0;i<connectedCrafters.size();i++)
		{
			int j = (i+crafterPos) % connectedCrafters.size();
			if(connectedCrafters.get(j).isPresent())
			{
				return connectedCrafters.get(j).orElseThrow(NullPointerException::new);
			}
			else
			{
				connectedCrafters.remove(j);
				i--;
			}
		}

		return selfCrafter;
	}

	public void nextCrafter()
	{
		crafterPos++;
	}


}
