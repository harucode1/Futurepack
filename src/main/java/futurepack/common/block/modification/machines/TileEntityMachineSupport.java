package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticType;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public abstract class TileEntityMachineSupport extends TileEntityMachineBase 
{

	protected final CapabilitySupport support;

	private LazyOptional<ISupportStorage>[] supportOpt;
	
	public TileEntityMachineSupport(TileEntityType<? extends TileEntityMachineSupport> type, int max, EnumEnergyMode supportMode)
	{
		this(type, new CapabilitySupport(max, supportMode));
	}
	
	@SuppressWarnings("unchecked")
	protected TileEntityMachineSupport(TileEntityType<? extends TileEntityMachineSupport> type, CapabilitySupport support)
	{
		super(type);
		this.support = support;
		supportOpt = new LazyOptional[6];
	}

	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		super.onLogisticChange(face, type);
		if(type == EnumLogisticType.SUPPORT)
		{
			if(supportOpt[face.get3DDataValue()]!=null)
			{
				supportOpt[face.get3DDataValue()].invalidate();
				supportOpt[face.get3DDataValue()] = null;
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		if(side==null)
			return LazyOptional.empty();
		
		if(cap == CapabilitySupport.cap_SUPPORT)
		{
			return HelperEnergyTransfer.getSupportCap(supportOpt, side, this::getLogisticStorage, ()->support);
		}
		
		return super.getCapability(cap, side);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(supportOpt);
		super.setRemoved();
	}
	
	@Override
	public void writeData(CompoundNBT nbt)
	{
		super.writeData(nbt);
		nbt.put("support", support.serializeNBT());
	}
	
	@Override
	public void readData(CompoundNBT nbt)
	{
		super.readData(nbt);
		support.deserializeNBT(nbt.getCompound("support"));
	}
	
	public CapabilitySupport getSupport() 
	{
		return support;
	}
}
