package futurepack.common.block.modification.machines;

import java.util.UUID;

import futurepack.common.item.misc.MiscItems;
import futurepack.common.recipes.crafting.InventoryCraftingForResearch;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

public class BlueprintRecipe
{
	private NonNullList<ItemStack> items = NonNullList.withSize(9, ItemStack.EMPTY);
	private String name;
	private IRecipe rec;
	private UUID owner;
	private World w;
	
	public BlueprintRecipe(NonNullList<ItemStack> items, String name, UUID owner, World w)
	{
		super();
		this.items = items;
		this.name = name;
		this.owner = owner;
		this.w = w;
	}

	public IRecipe getRecipe()
	{
		if(rec!=null)
			return rec;
		
		CraftingInventory inv = getCrafting();
		rec = w.getRecipeManager().getRecipeFor(IRecipeType.CRAFTING, inv, w).orElse(null);
		return rec; //cashing the recipe
	}
	
	private CraftingInventory getCrafting()
	{
		return getCraftingTable(items, 0);
	}
	
	public boolean isItemValid(ItemStack it, int slot)
	{
		if(getRecipe()!=null)
		{
			CraftingInventory orginal = getCrafting();
			if(rec.matches(orginal, w))
			{
				ItemStack valid = rec.assemble(orginal);
				
				CraftingInventory test = getCrafting();
				test.setItem(slot, it);
				if(rec.matches(test, w))
				{
					ItemStack result = rec.assemble(test);
					return HelperInventory.areItemsEqualNoSize(valid, result);
				}
			}			
		}
		return false;
	}
	
	public CraftingInventory getCraftingTable(NonNullList<ItemStack> items, int start)
	{
		CraftingInventory inv = new InventoryCraftingForResearch(new Container(ContainerType.CRAFTING, start)
		{	
			@Override
			public boolean stillValid(PlayerEntity playerIn)
			{
				return true;
			}
		}, 3, 3, owner);
		for(int i=0;i<inv.getContainerSize();i++)
		{
			inv.setItem(i, items.get(i+start));
		}
		return inv;
	}
	
	public void addRecipeToItem(ItemStack item)
	{
		if(!item.hasTag())
			item.setTag(new CompoundNBT());
		
		CompoundNBT nbt = new CompoundNBT();
		ListNBT list = new ListNBT();
		for(int i=0;i<items.size();i++)
		{
			if(!items.get(i).isEmpty())
			{
				CompoundNBT tag = new CompoundNBT();
				items.get(i).save(tag);
				tag.putInt("Slot", i);
				list.add(tag);
			}
		}
		nbt.put("items", list);
		nbt.putString("output", name);
		nbt.putLong("uu", owner.getMostSignificantBits());
		nbt.putLong("id", owner.getLeastSignificantBits());
		
		item.getTag().put("recipe", nbt);
	}
	
	public static BlueprintRecipe createFromItem(ItemStack it, World w)
	{
		if(it.getItem() == MiscItems.crafting_recipe)
		{
			if(it.getTagElement("recipe") != null)
			{
				CompoundNBT nbt = it.getTagElement("recipe");
				NonNullList<ItemStack> items = NonNullList.withSize(9, ItemStack.EMPTY);
				ListNBT list = nbt.getList("items", 10);
				for(int i=0;i<list.size();i++)
				{
					CompoundNBT tag = list.getCompound(i);
					ItemStack is = ItemStack.of(tag);
					items.set(tag.getInt("Slot"), is);
				}
				String name = nbt.getString("output");
				UUID owner = new UUID(nbt.getLong("uu"), nbt.getLong("id"));
				BlueprintRecipe rec = new BlueprintRecipe(items, name, owner, w);
				return rec;
			}
		}
		return null;
	}
}
