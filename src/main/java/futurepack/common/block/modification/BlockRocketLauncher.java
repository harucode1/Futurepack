package futurepack.common.block.modification;

import futurepack.common.FPTileEntitys;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockRocketLauncher extends BlockEntityLaserBase 
{

	protected BlockRocketLauncher(Properties props) 
	{
		super(props, () -> FPTileEntitys.ROCKET_LAUNCHER);
	}

	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state) && !w.isClientSide)
		{
			FPGuiHandler.ROCKET_LAUNCHER_EDIT.openGui(pl, pos);
			return ActionResultType.SUCCESS;
		}
		
		return super.use(state, w, pos, pl, hand, hit);
	}
}
