package futurepack.common.block.modification;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.Constants;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.modification.EnumChipType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class TileEntityEntityEater extends TileEntityLaserBase<LivingEntity>
{
	float progress = 0;
	
	public TileEntityEntityEater()
	{
		super(FPTileEntitys.ENTITY_EATER, LivingEntity.class);
	}

	@Override
	public boolean isEntityValid(LivingEntity entity)
	{
		if(!matchConfig(entity))
			return false;
		
		if(getConfiguration("kill.not"))
		{
			return entity.getHealth() > 1F;
		}
		return entity.isAlive();
	}

	@Override
	public void progressEntity(LivingEntity entity)
	{	
		float attack = entity.getHealth();
			
		if(getConfiguration("kill.not"))
			attack--;
		else
			attack++;
		
		if(!entity.canChangeDimensions() && attack > 5)
		{
			attack = 5;
		}
			
		if(attackEntity(entity, attack))
		{
			killEverythingInLine(entity);
			
			progress = attack * (getChipPower(EnumChipType.INDUSTRIE)+1);
			this.energy.add((int) progress);
			setChanged();
//			if(world.rand.nextInt(20)==0)
//			{
//				world.playSoundEffect(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, "random.eat", 3.0F, 0.5F);
//			}
			double x =  worldPosition.getX()+0.5 - entity.getX();
			double y =  worldPosition.getY()+0.5 - entity.getY();
			double z =  worldPosition.getZ()+0.5 - entity.getZ();
			//double dis = Math.sqrt(x*x + y*y + z*z);
			
			level.addParticle(ParticleTypes.PORTAL, worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5,x/20D,y/20D,z/20D);
			
//			src = null;
		}
	}

	@Override
	public boolean shouldWork()
	{
		return energy.get() < energy.getMax();
	}

	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.PRODUCE;
	}
	
	@Override
	public ResourceLocation getTexture()
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/eater_1.png");
	}

	@Override
	public int getLaserColor()
	{
		return 0x8800FF;
	}
	
	@Override
	public ResourceLocation getLaser()
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/laser_eater.png");
	}

	public void killEverythingInLine(Entity e)
	{
		List<LivingEntity> en = getAllEntitties(level, new Vector3d(worldPosition.getX()+0.5,worldPosition.getY()+1.0625,worldPosition.getZ()+0.5), new Vector3d(e.getX(), e.getY() + e.getEyeHeight(), e.getZ()));
		for(LivingEntity base : en)
		{
			if(base.isAlive())
			{
				float attack = base.getHealth();
				if(getConfiguration("kill.not"))
					attack--;
				else
					attack++;
				if(base.hurt(FuturepackMain.NEON_DAMAGE, attack))
				{
					progress = attack * (getChipPower(EnumChipType.INDUSTRIE)+1);
					energy.add((int) progress);
				}
			}
		}
		
	}
	
	public static List<LivingEntity> getAllEntitties(World w, Vector3d start, Vector3d end)
	{
		List<LivingEntity> base = w.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(start.x, start.y, start.z, end.x, end.y, end.z));
		
		Vector3d vec = end.subtract(start);
		double dis = vec.length();
		
		double dx = vec.x / dis;
		double dy = vec.y / dis;
		double dz = vec.z / dis;
		
		List<LivingEntity> all = new ArrayList<LivingEntity>(base.size());
		
		for(double x=0;x<dis;x+=0.0625)
		{
			Vector3d point = start.add(dx*x, dy*x, dz*x);
			List<LivingEntity> l = getWithin(base, point);
			all.addAll(l);
			base.removeAll(l);
			
			if(base.isEmpty())
			{
				break;
			}
		}
		
		return all;
	}
	
	public static List<LivingEntity> getWithin(List<LivingEntity> list, Vector3d p)
	{
		List<LivingEntity> base = new ArrayList<LivingEntity>(list.size());
		for(LivingEntity b : list)
		{
			if(b.getBoundingBox().contains(p))
			{
				base.add(b);
			}
		}
		return base;
	}
//	
//	@Override
//	public boolean isWorking()
//	{
//		return true;
//	}
//	
//	
	@Override
	public int getDefaultPowerUsage() 
	{
		return (int) progress;
	}
//	
//	@Override
//	public AxisAlignedBB getRenderBoundingBox()
//	{
//		return INFINITE_EXTENT_AABB;
//	}
//	
//	@Override
//	public boolean shouldRenderInPass(int pass)
//	{
//		return pass == 0;
//	}
	
	
}
