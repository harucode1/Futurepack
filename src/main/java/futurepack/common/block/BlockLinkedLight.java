package futurepack.common.block;

import net.minecraft.block.AirBlock;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockLinkedLight extends AirBlock
{

	public BlockLinkedLight(Properties properties) 
	{
		super(properties);
	}

	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityLinkedLight();
	}
}
