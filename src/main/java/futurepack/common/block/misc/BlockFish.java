package futurepack.common.block.misc;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.RotatedPillarBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockFish extends RotatedPillarBlock
{
	private final VoxelShape[][] boxes;
	public static final IntegerProperty AGE_0_3 = BlockStateProperties.AGE_3;

	public BlockFish(Block.Properties props) 
	{
		super(props);
		boxes = new VoxelShape[3][4];
		registerDefaultState(super.stateDefinition.any().setValue(AGE_0_3, 0));
		for(Direction.Axis a: Direction.Axis.values())
		{
			for(int i=0;i<boxes[a.ordinal()].length;i++)
			{
				float h = 1 - (i*0.25F);
				float x=1,y=1,z=1;
				switch(a)
				{
				case X:x=h;break;
				case Y:y=h;break;
				case Z:z=h;break;
				}
				boxes[a.ordinal()][i] = VoxelShapes.box(0, 0, 0, x,y,z);
			}
		}
	}
	
//	@Override
//	public void setBlockBoundsForItemRender()
//	{
//		this.setBlockBounds(0,0,0,1,1,1);
//	}
//	 
//	@Override
//	public AxisAlignedBB getCollisionBoundingBox(World w, BlockPos pos, IBlockState state) 
//	{
//		float m = getMetaFromState(state);
//		m =1-( m / 4F);
//		return new AxisAlignedBB(0, 0, 0, 1, m, 1).offset(pos.getX(),pos.getY(),pos.getZ());
//	}
//	
//	@Override
//	public void setBlockBoundsBasedOnState(IWorldReader w, BlockPos pos) 
//	{
//		float m = getMetaFromState(w.getBlockState(pos));
//		m =1-( m / 4F);
//		setBlockBounds(0, 0, 0, 1, m, 1);
//	}
//	
//	@Override
//	public void addCollisionBoxesToList(World worldIn, BlockPos pos, IBlockState state, AxisAlignedBB mask, List list, Entity collidingEntity)
//    {
//        this.setBlockBoundsBasedOnState(worldIn, pos);
//        super.addCollisionBoxesToList(worldIn, pos, state, mask, list, collidingEntity);
//    }
//	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		int m = state.getValue(AGE_0_3);
		Direction.Axis axis = state.getValue(AXIS);
		return boxes[axis.ordinal()][m];
	}

	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if (pl.canEat(false))
        {
			int meta = state.getValue(AGE_0_3);
			if(meta+1>=4)
			{
				w.removeBlock(pos, false);
			}
			else
			{
				w.setBlockAndUpdate(pos, state.setValue(AGE_0_3, meta+1));
			}
			pl.getFoodData().eat(5, 0.2F);
			
			return ActionResultType.SUCCESS;
        }
		return ActionResultType.PASS;
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//        return this.getDefaultState().withProperty(AGE_0_3, meta%4).withProperty(AXIS, EnumFacing.Axis.values()[meta/4]);
//    }
//	
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//		EnumFacing.Axis axis = state.getValue(AXIS);
//		int m = state.getValue(AGE_0_3);
//        return m + axis.ordinal()*4;
//    }

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(AGE_0_3);
	}
}
