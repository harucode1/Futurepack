package futurepack.common.block.misc;

import futurepack.common.block.BlockHoldingTile;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tags.ItemTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

public class BlockSaplingHolder extends BlockHoldingTile
{
	public static final BooleanProperty FILLED = BooleanProperty.create("filled");

	private final PlantType planttype;
	
	protected BlockSaplingHolder(Block.Properties props, PlantType planttype)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
		
		this.planttype = planttype;
		this.registerDefaultState(this.stateDefinition.any().setValue(FILLED, false));
	}
	
	@Override
	public boolean canSustainPlant(BlockState state, IBlockReader world, BlockPos pos, Direction facing, IPlantable plantable)
	{
		if(world.getBlockState(pos.relative(facing)).isAir(world, pos))//for prechecks
		{
			return true;
		}
		PlantType plant = plantable.getPlantType(world, pos.relative(facing));
		if(plant == planttype)
			return true;
		return super.canSustainPlant(state, world, pos, facing, plantable);
	}
	
//	@Override
//	public IBlockState getActualState(IBlockState state, IWorldReader w, BlockPos pos)
//	{
//		TileEntitySaplingHolder tile = (TileEntitySaplingHolder) w.getTileEntity(pos);
//		if(tile!=null)
//		{
//			state = state.withProperty(filled, !tile.getFilter().isEmpty());
//			
//		}
//		return state;
//	}
	
//	@Override
//	public void getSubBlocks(ItemGroup itemIn, NonNullList<ItemStack> items)
//	{
//		 items.add(new ItemStack(this,1,0));
//		 items.add(new ItemStack(this,1,1));
//		 items.add(new ItemStack(this,1,2));
//	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		ItemStack it = pl.getItemInHand(hand);
		TileEntitySaplingHolder tile = (TileEntitySaplingHolder) w.getBlockEntity(pos);
		if(it==null || it.isEmpty())
		{
			tile.setFilter(ItemStack.EMPTY);
			w.setBlockAndUpdate(pos, state.setValue(FILLED, false));
			return ActionResultType.SUCCESS;
		}
		else
		{
			if(isSapling(it))
			{
				ItemStack stack = tile.getFilter();
				if(stack!=null)
				{
					if(HelperInventory.areItemsEqualNoSize(stack, it))
					{
						tile.tryPlace(it);
						return ActionResultType.SUCCESS;
					}
				}
				tile.setFilter(it.copy());
				w.setBlockAndUpdate(pos, state.setValue(FILLED, true));
				return ActionResultType.SUCCESS;
			}
		}
		
		return super.use(state, w, pos, pl, hand, hit);
	}
	
	public static boolean isSapling(ItemStack it)
	{
		return ItemTags.SAPLINGS.contains(it.getItem());
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntitySaplingHolder();
	}
	
    @Override
    protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
    {
    	super.createBlockStateDefinition(builder);
    	builder.add(FILLED);
    }
}
