package futurepack.common.block.misc;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockWirelessRedstoneReceiver extends Block
{
	public static final IntegerProperty POWER = BlockStateProperties.POWER;

	public BlockWirelessRedstoneReceiver(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_deco);
		registerDefaultState(this.stateDefinition.any().setValue(POWER, 0));
	}

	
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(POWER);
	}
	
	@Override
	public boolean isSignalSource(BlockState state)
    {
        return true;
    }
    
    @Override
    public int getSignal(BlockState state, IBlockReader blockAccess, BlockPos pos, Direction side)
    {
    	return state.getValue(POWER);
    }
    
    public static void updateReceiversAround(World w, BlockPos pos, int oldPower, int newPower)
    {
    	if(oldPower!=newPower)
    	{
    		int range = Math.max(oldPower, newPower);
    		for(int x=-range;x<range;x++)
    		{
    			for(int y=-range;y<range;y++)
    			{
    				for(int z=-range;z<range;z++)
    				{
    					int r = Math.abs(x) + Math.abs(y) + Math.abs(z);
    					
    					if(r > range)
    						continue;
    					
    					int power = Math.max(0, newPower-r);
    					BlockState state = w.getBlockState(pos.offset(x,y,z));
    					if(state.getBlock() == MiscBlocks.wirelessRedstoneReceiver)
    					{
    						state = state.setValue(POWER, power);
    						w.setBlock(pos.offset(x,y,z), state, 3);
    					}
    				}
    			}
    		}
    	}
    }
}
