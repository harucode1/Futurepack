package futurepack.common.block.misc;

import futurepack.common.block.BlockRotateableTile;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IBucketPickupHandler;
import net.minecraft.block.ILiquidContainer;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;

public class BlockExternCooler extends BlockRotateableTile implements IBucketPickupHandler, ILiquidContainer, IWaterLoggable
{
	public static final IntegerProperty TEMPERATUR = IntegerProperty.create("temp", 0, 2);
	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
	   
	private final VoxelShape SHAPE_UP = VoxelShapes.or(Block.box(0, 0, 0, 16, 4, 16), Block.box(1, 4, 1, 15, 15, 15));
	private final VoxelShape SHAPE_DOWN = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 180F, 0F);
	private final VoxelShape SHAPE_NORTH = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 90F, 0F);
	private final VoxelShape SHAPE_SOUTH = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 90F, 180F);
	private final VoxelShape SHAPE_WEST = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 90F, 90F);
	private final VoxelShape SHAPE_EAST = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 90F, 270F);
	
	public BlockExternCooler(Block.Properties props) 
	{
		super(props);
		this.registerDefaultState(this.defaultBlockState().setValue(TEMPERATUR, 1).setValue(WATERLOGGED, Boolean.valueOf(false)));
		
		HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 0F, 0F);
		
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(TEMPERATUR, WATERLOGGED);
	}
	
	@Override
	public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, IWorld w, BlockPos pos, BlockPos facingPos)
	{
		if (stateIn.getValue(WATERLOGGED)) 
		{
			w.getLiquidTicks().scheduleTick(pos, Fluids.WATER, Fluids.WATER.getTickDelay(w));
		}

		
		@SuppressWarnings("deprecation")
        BlockState state = super.updateShape(stateIn, facing, facingState, w, pos, facingPos);
		TileEntityExternCooler t = (TileEntityExternCooler) w.getBlockEntity(pos);
		if(t==null)
			return state;
		else
			return state.setValue(TEMPERATUR, Math.max((int)(t.f - 1), 0));
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityExternCooler();
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		switch(state.getValue(FACING))
		{
		case DOWN:
			return SHAPE_DOWN;
		case NORTH:
			return SHAPE_NORTH;
		case SOUTH:
			return SHAPE_SOUTH;
		case WEST:
			return SHAPE_WEST;
		case EAST:
			return SHAPE_EAST;
		case UP:
		default:
			return SHAPE_UP;
		}
	}



	@Override
	public Fluid takeLiquid(IWorld worldIn, BlockPos pos, BlockState state)
	{
		if (state.getValue(WATERLOGGED)) 
		{
			worldIn.setBlock(pos, state.setValue(WATERLOGGED, Boolean.valueOf(false)), 3);
			return Fluids.WATER;
		}
		else
		{
			return Fluids.EMPTY;
		}
	}
	
	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}
	
	@Override
	public boolean canPlaceLiquid(IBlockReader worldIn, BlockPos pos, BlockState state, Fluid fluidIn)
	{
	      return fluidIn == Fluids.WATER;
	}
	
	@Override
	public boolean placeLiquid(IWorld worldIn, BlockPos pos, BlockState state, FluidState fluidStateIn)
	{
		if (!state.getValue(WATERLOGGED) && fluidStateIn.getType() == Fluids.WATER) 
		{
			if (!worldIn.isClientSide()) 
			{
				worldIn.setBlock(pos, state.setValue(WATERLOGGED, true), 3);
				worldIn.getLiquidTicks().scheduleTick(pos, fluidStateIn.getType(), fluidStateIn.getType().getTickDelay(worldIn));
			}
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		return super.getStateForPlacement(context).setValue(FACING, context.getClickedFace());
	}
}
