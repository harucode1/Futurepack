package futurepack.common.block.misc;

import futurepack.common.block.BlockHoldingTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BlockForceField extends BlockHoldingTile 
{

	public BlockForceField(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_deco);
	}

//	@Override
//	public boolean isOpaqueCube(IBlockState state)
//	{
//		return false;
//	}
	
	@Override
	public void onRemove(BlockState state, World w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if (state.getBlock() != newState.getBlock()) 
		{
			w.removeBlockEntity(pos);
		}
	}

	@Override
	public void setPlacedBy(World w, BlockPos pos, BlockState state, LivingEntity liv, ItemStack it)
	{
		TileEntityForceField force = (TileEntityForceField) w.getBlockEntity(pos);
		force.letPlayerPass = true;
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
	{
		return BlockRenderType.ENTITYBLOCK_ANIMATED;
	}
	
	@Override
	@OnlyIn(Dist.CLIENT)
	public float getShadeBrightness(BlockState state, IBlockReader worldIn, BlockPos pos) 
	{
		return 1.0F;
	}
	
	@Override
	public boolean propagatesSkylightDown(BlockState state, IBlockReader reader, BlockPos pos) 
	{
		return true;
	}
	
	@Override
	public VoxelShape getCollisionShape(BlockState state, IBlockReader w, BlockPos pos, ISelectionContext context) 
	{
		if(context.getEntity()!=null)
		{
			TileEntityForceField field = (TileEntityForceField) w.getBlockEntity(pos);
			if(field !=null && field.canEntityPass(context.getEntity()))
			{			
				return VoxelShapes.empty();
			}
		}
		return super.getCollisionShape(state, w, pos, context);
	}
	
	@Override
	public boolean skipRendering(BlockState state, BlockState adjacentBlockState, Direction side)
	{
		if(adjacentBlockState.getBlock() == this)
			return true;
		
		return super.skipRendering(state, adjacentBlockState, side);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityForceField();
	}
}
