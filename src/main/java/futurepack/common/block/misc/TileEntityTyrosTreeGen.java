package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import futurepack.world.dimensions.biomes.FPBiomes;
import futurepack.world.gen.feature.TyrosTreeFeature;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.server.ServerWorld;

public class TileEntityTyrosTreeGen extends TileEntity implements ITickableTileEntity
{
	private boolean done = false;

	public TileEntityTyrosTreeGen() 
	{
		super(FPTileEntitys.TYROS_TREE_GEN);
	}

	@Override
	public void tick() 
	{
		if(!level.isClientSide && !done)
		{
			execute();
		}
		if(done)
		{
			level.removeBlock(worldPosition, false);
			level.removeBlockEntity(worldPosition);
		}
			
	}

	private void execute()
	{
		TyrosTreeFeature f = TyrosTreeFeature.LARGE_TYROS_TREE;
		if(!level.isClientSide) {
			if(!f.place((ServerWorld)level, ((ServerWorld)level).getChunkSource().getGenerator(), level.random, getBlockPos().offset(-2, 0, -2), FPBiomes.TYROS_TREE_CONFIG))
			{
				f.place((ServerWorld)level, ((ServerWorld)level).getChunkSource().getGenerator(), level.random, getBlockPos(), FPBiomes.TYROS_TREE_CONFIG);
			}
		}
		done = true;
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		nbt.putBoolean("done", done);
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT compound)
	{
		done = compound.getBoolean("done");
		super.load(state, compound);
	}
}
