package futurepack.common.block.misc;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockDungeonCore extends Block
{

	public BlockDungeonCore(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);	
//		setCreativeTab(FPMain.tab_deco);
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
    {
        return BlockRenderType.MODEL;
    }
	
	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving)
	{
		if(w.hasNeighborSignal(pos))
		{
			removeDungeonProtection(w, pos);
		}
	}
	
	public static void removeDungeonProtection(World w, BlockPos pos)
	{
		TileEntityDungeonCore tile = (TileEntityDungeonCore) w.getBlockEntity(pos);
		tile.removeDungeonProtection();
	}
	
	@Override
	public boolean hasTileEntity(BlockState state) 
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityDungeonCore();
	}
}
