package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.modification.thermodynamic.TemperatureManager;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;

public class TileEntityExternCooler extends TileEntity implements ITickableTileEntity
{
	public TileEntityExternCooler() 
	{
		super(FPTileEntitys.EXTERN_COOLER);
	}

	public float f ;
	
	@Override
	public void tick()
	{
		BlockState state = getBlockState();
		Direction face = state.getValue(BlockRotateableTile.FACING);
		BlockPos xyz = worldPosition.relative(face.getOpposite());
		
		if(level.hasChunkAt(xyz))
		{
			Biome gen = level.getBiome(worldPosition);
			float temp = TemperatureManager.getTempDegrees(gen, worldPosition);
			f = temp>25F ? 1 : temp<20F ? 3 : 2;
			
			TileEntity t = level.getBlockEntity(xyz);
			if(t instanceof TileEntityModificationBase)
			{
				TileEntityModificationBase m = (TileEntityModificationBase) t;

				m.setHeatCool(f, temp);
			}
		}
	}
	
}
