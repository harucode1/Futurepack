package futurepack.common.block.misc;

import futurepack.api.FacingUtil;
import futurepack.common.FuturepackTags;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldWriter;
import net.minecraft.world.World;

public class BlockQuantanium extends Block
{
	public static final BooleanProperty up = BooleanProperty.create("up");
	public static final BooleanProperty down = BooleanProperty.create("down");
	public static final BooleanProperty north = BooleanProperty.create("north");
	public static final BooleanProperty east = BooleanProperty.create("east");
	public static final BooleanProperty south = BooleanProperty.create("south");
	public static final BooleanProperty west = BooleanProperty.create("west");
	public static final BooleanProperty glowing = BooleanProperty.create("glowing");
	
	public static final BooleanProperty[] faces = new BooleanProperty[]{down,up,north,south,west,east};

	public BlockQuantanium(Block.Properties props)
	{
		super(props);
		this.registerDefaultState(this.stateDefinition.any().setValue(up, false).setValue(down, false).setValue(north, false).setValue(south, false).setValue(west, false).setValue(east, false).setValue(glowing, false));
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos)
	{
		state = state.setValue(faces[facing.get3DDataValue()], isAccepted(facingState) );
		return state;
	}

	public boolean isAccepted(BlockState state)
	{
		if(state.getBlock() == this)
			return true;
		return state.is(FuturepackTags.quantanium_connecting);
	}
	
	@Override
	public void entityInside(BlockState state, World world, BlockPos pos, Entity entityIn) {		
		if(!world.isClientSide) {
			if(!state.getValue(glowing) && entityIn instanceof PlayerEntity) {
//				world.setBlockState(pos, state.with(glowing, true), 3);
			}
		}
	}
	
	@Override
	public VoxelShape getCollisionShape(BlockState state, IBlockReader w, BlockPos pos, ISelectionContext context) 
	{
		if(!state.getValue(glowing) && w.getBlockState(pos) == state)
		{
			if(context.getEntity() instanceof PlayerEntity && w instanceof IWorldWriter)
			{
				((IWorldWriter)w).setBlock(pos, state.setValue(glowing, true), 3);
			}
		}
		return super.getCollisionShape(state, w, pos, context);
	}
	
	@Override
	public int getLightValue(BlockState state, IBlockReader world, BlockPos pos) 
	{
		if(state.getValue(glowing))
		{
			return super.getLightValue(state, world, pos);
		}
		return 0;
	}

	
	@Override
	public void onPlace(BlockState state, World w, BlockPos pos, BlockState oldState, boolean isMoving) 
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(!w.isClientSide) {
			if(oldState.getBlock()!=this)
			{
				for(Direction face : FacingUtil.VALUES)
				{
					state = state.setValue(faces[face.get3DDataValue()], isAccepted(w.getBlockState(pos.relative(face))));
				}
				w.setBlock(pos, state, 2);
			}
		}
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(faces);
		builder.add(glowing);
	}
}
