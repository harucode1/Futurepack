package futurepack.common.block.misc;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.SoundType;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class BlockBedrockRift extends Block
{
	public BlockBedrockRift() 
	{
		super(Properties.copy(Blocks.BEDROCK).sound(SoundType.STONE).lightLevel(state -> 8).noDrops().randomTicks());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onRemove(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving)
	{
		//Why?!?   Thermal flat bedrock destroy every Block, which is in the bedrock
		//Prevent replacing by bedrock is the easiest and cleanest solution from the pool of dirty hacks :P
        //(Note Bedrockores use generation in EndTick and ChunkLoad)
		 if(worldIn.getBlockState(pos).getBlock() == Blocks.BEDROCK)
			 worldIn.setBlockAndUpdate(pos, defaultBlockState());
		 else
			 super.onRemove(state, worldIn, pos, newState, isMoving); //dont remove TileEntity
	}
	
	@Override
	public void randomTick(BlockState state, ServerWorld w, BlockPos pos, Random r) 
	{
		super.randomTick(state, w, pos, r);
		double regen = (r.nextInt(4) - r.nextInt(10)) / 16384D;
		if(regen > 0)
		{
			TileEntityBedrockRift rift = (TileEntityBedrockRift) w.getBlockEntity(pos);
			if(rift!=null && rift.getFillrate() < 1.0)
			{
				rift.regenerate(regen);;
			}
		}
	}
	
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityBedrockRift();
	}
}
