package futurepack.common.block.misc;

import java.util.List;

import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.DirectionalBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockDungeonSpawner extends BlockRotateableTile
{
	public static final DirectionProperty FACING = DirectionalBlock.FACING;
	public static final IntegerProperty rotation = IntegerProperty.create("rot", 0, 3);
	
	
	public static final VoxelShape UP = VoxelShapes.or(Block.box(0, 0, 0, 16, 3, 16), Block.box(6, 0, 6, 10, 12, 10));
	public static final VoxelShape DOWN = VoxelShapes.or(Block.box(0, 13, 0, 16, 16, 16), Block.box(6, 4, 6, 10, 16, 10));
	public static final VoxelShape NORTH = Block.box(0, 0, 11, 16, 16, 16);
	public static final VoxelShape SOUTH = Block.box(0, 0, 0, 16, 16, 5);
	public static final VoxelShape WEST = Block.box(11, 0, 0, 16, 16, 16);
	public static final VoxelShape EAST = Block.box(0, 0, 0, 5, 16, 16);
	
	
	protected BlockDungeonSpawner(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_deco);
		
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		Direction face = state.getValue(FACING);
		switch(face)
		{
		case DOWN:
			return DOWN;
		case NORTH:
			return NORTH;
		case SOUTH:
			return SOUTH;
		case WEST:
			return WEST;
		case EAST:
			return EAST;
		case UP:
		default:
			return UP;
		}
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		BlockState state = super.getStateForPlacement(context);
		state = state.setValue(FACING, context.getClickedFace());
		if(context.getClickedFace().getAxis()==Axis.Y)
		{
			int rot = MathHelper.floor(context.getRotation() * 4.0F / 360.0F + 0.5D) & 3;							
			state = state.setValue(rotation, rot);
		}
		return state;
	}
	
	@Override
	public void onPlace(BlockState state, World w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(oldState.getBlock() == this)
		{
			Direction face = state.getValue(FACING);
			if(face.getAxis()!=Axis.Y)
			{
				int h = face.get2DDataValue();
				if(state.getValue(rotation) != h)
				{
					state = state.setValue(rotation, h);
					w.setBlockAndUpdate(pos, state);
				}
				
			}
		}
	}
	
	@Override
    public boolean isSignalSource(BlockState state)
    {
        return true;
    }
	
	@Override
	public int getSignal(BlockState state, IBlockReader w, BlockPos pos, Direction side)
	{
		if(side != state.getValue(FACING))
    		return 0;
    	
    	TileEntityDungeonSpawner tile = (TileEntityDungeonSpawner) w.getBlockEntity(pos);
        return tile.getRedstonePower();
	}
	

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(rotation);
	}


	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityDungeonSpawner();
	}
	
	@Override
	public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) 
	{
		tooltip.add(new TranslationTextComponent("block.futurepack.dungeon_spawner.tooltip"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
	
	public BlockState rotate(BlockState state, Rotation rot) {	
		
		int remove = 1;
		
		if(rot.equals(Rotation.CLOCKWISE_180)) {
			remove = 2;
		}
		else if(rot.equals(Rotation.COUNTERCLOCKWISE_90)) {
			remove = 3;
		}
		
		int newRotation = state.getValue(rotation)-remove;
		if(newRotation < 0) {
			newRotation += 4;
		}
		
		return state.setValue(FACING, rot.rotate(state.getValue(FACING))).setValue(rotation, newRotation);
	}
}
