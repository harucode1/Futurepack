package futurepack.common.block.misc;

import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public class BlockMagnet extends BlockRotateableTile
{
	public BlockMagnet(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityMagnet();
	}
}
