package futurepack.common.block.misc;

import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.world.IBlockReader;

public class BlockGravityPulser extends BlockRotateableTile
{
	public BlockGravityPulser(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityPulsit();
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		Direction face = context.getNearestLookingDirection().getOpposite();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}
}
