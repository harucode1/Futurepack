package futurepack.common.block.misc;


import futurepack.api.interfaces.tilentity.ITileHologramAble;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;

public class TileEntityForceField extends TileEntity implements ITileHologramAble
{
	public boolean letProjectilesPass = false;
	public boolean letMobsPass = false;
	public boolean letAnimalsPass = false;
	public boolean letBabysPass = false;
	public boolean letPlayerPass = false;
	/**
	 * The NBT tag of the item will not be checked correctly.
	 */
	public ItemStack neededItem = null;
		
	private BlockState hologram = null;
	
	public TileEntityForceField() 
	{
		super(FPTileEntitys.FORCE_FIELD);
	}
	
	public boolean canEntityPass(Entity e)
	{
		if(e instanceof ProjectileEntity && letProjectilesPass)
		{
			return true;
		}		
		
		if(e instanceof LivingEntity)
		{
			LivingEntity base = (LivingEntity) e;
			if(base.isBaby() && letBabysPass)
			{
				return true;
			}
		}
		
		if(e instanceof AnimalEntity && letAnimalsPass)
		{
			return true;
		}
		
		if(e instanceof IMob && letMobsPass)
		{
			return true;
		}
		
		if(e instanceof PlayerEntity && letPlayerPass)
		{
			return true;
		}
			
		if(neededItem!=null && !neededItem.isEmpty())
		{
			if(e instanceof PlayerEntity)
			{
				PlayerEntity pl = (PlayerEntity) e;
				return pl.inventory.contains(neededItem);	//meta bassed keys!!!
			}
			else if(e instanceof LivingEntity)
			{
				LivingEntity liv = (LivingEntity) e;
				return ItemStack.matches(neededItem, (liv.getItemInHand(Hand.MAIN_HAND))) || ItemStack.matches(neededItem, (liv.getItemInHand(Hand.OFF_HAND)));
			}
			else
			{
				return false;
			}
		}
		
		return false;
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.putBoolean("projectiles", letProjectilesPass);
		nbt.putBoolean("mobs", letMobsPass);
		nbt.putBoolean("animals", letAnimalsPass);
		nbt.putBoolean("babys", letBabysPass);
		nbt.putBoolean("player", letPlayerPass);
		
		if(neededItem!=null)
		{
			nbt.put("item", neededItem.serializeNBT());
		}
		
		if(hologram!=null)
			nbt.put("holo", HelperHologram.toNBT(hologram));
		
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		letProjectilesPass = nbt.getBoolean("projectiles");
		letMobsPass = nbt.getBoolean("mobs");
		letAnimalsPass = nbt.getBoolean("animals");
		letBabysPass = nbt.getBoolean("babys");
		letPlayerPass = nbt.getBoolean("player");
		
		if(nbt.contains("item"))
		{
			neededItem = ItemStack.of(nbt.getCompound("item"));
		}
		if(nbt.contains("holo"))
			hologram = HelperHologram.fromNBT(nbt.getCompound("holo"));
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		load(getBlockState(), pkt.getTag());
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		return new SUpdateTileEntityPacket(worldPosition, -1, getUpdateTag());
	}
	
//	@Override
//	public boolean hasFastRenderer()
//	{
//		return HelperHologram.hasFastRenderer(this);
//	}

//	@Override
//	public boolean shouldRenderInPass(int pass)
//	{
//		return hasHologram() ? pass==0 : pass == 1;
//	}
	
	@Override
	public BlockState getHologram()
	{
		return hologram;
	}

	@Override
	public boolean hasHologram()
	{
		return hologram!=null;
	}

	@Override
	public void setHologram(BlockState state)
	{
		hologram = state;
	}
}
