package futurepack.common.block.misc;

import futurepack.api.Constants;
import futurepack.common.block.BlockRotateable;
import futurepack.common.gui.escanner.GuiResearchMainOverview;
import net.minecraft.advancements.Advancement;
import net.minecraft.block.BlockState;
import net.minecraft.block.LecternBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.pathfinding.PathType;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockTectern extends BlockRotateable 
{

	public BlockTectern(Properties properties) 
	{
		super(properties);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityResearchExchange();
	}
	
	@Override
	public VoxelShape getOcclusionShape(BlockState state, IBlockReader worldIn, BlockPos pos) 
	{
		return LecternBlock.SHAPE_COMMON;
	}

	@Override
	public boolean useShapeForLightOcclusion(BlockState state) 
	{
		return true;
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) 
	{
		return this.defaultBlockState().setValue(HORIZONTAL_FACING, context.getHorizontalDirection().getOpposite());
	}

	@Override
	public VoxelShape getCollisionShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
	{
		return LecternBlock.SHAPE_COLLISION;
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
	{
		switch(state.getValue(BlockRotateable.HORIZONTAL_FACING)) 
		{
		case NORTH:
			return LecternBlock.SHAPE_NORTH;
		case SOUTH:
			return LecternBlock.SHAPE_SOUTH;
		case EAST:
			return LecternBlock.SHAPE_EAST;
		case WEST:
			return LecternBlock.SHAPE_WEST;
		default:
			return LecternBlock.SHAPE_COMMON;
		}
	}
	
	@Override
	public boolean isPathfindable(BlockState state, IBlockReader worldIn, BlockPos pos, PathType type) 
	{
		return false;
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand handIn, BlockRayTraceResult hit) 
	{
		if(!w.isClientSide && pl instanceof ServerPlayerEntity)
		{
			TileEntityResearchExchange tile = (TileEntityResearchExchange) w.getBlockEntity(pos);
			tile.onInteract(pl);
			ServerWorld sw = (ServerWorld) w;
			Advancement adv = sw.getServer().getAdvancements().getAdvancement(new ResourceLocation(Constants.MOD_ID, "brain_touch"));
			((ServerPlayerEntity)pl).getAdvancements().award(adv, "use_tectern");
		}
		else
		{
			DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
			{
				@Override
				public void run() 
				{
					Minecraft.getInstance().setScreen(new GuiResearchMainOverview());
				}
			});
		}
		return ActionResultType.SUCCESS;
	}

}
