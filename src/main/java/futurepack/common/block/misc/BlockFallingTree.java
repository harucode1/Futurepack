package futurepack.common.block.misc;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockFallingTree extends Block
{

	public BlockFallingTree(Block.Properties props)
	{
		super(props);
//		super(Material.WOOD);
	}

//	@Override
//	public boolean isFullBlock(IBlockState state)
//	{
//		return false;
//	}
//	
//	@Override
//	public boolean isOpaqueCube(IBlockState state)
//	{
//		return false;
//	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
	{
		return BlockRenderType.ENTITYBLOCK_ANIMATED;
	}

	@Override
	public boolean hasTileEntity(BlockState state) 
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityFallingTree();
	}
	
	@Override
	public void onRemove(BlockState state, World world, BlockPos pos, BlockState newState, boolean isMoving) 
	{
		super.onRemove(state, world, pos, newState, isMoving);
		if(newState.getBlock()!=this)
		{
			TileEntityFallingTree tree = (TileEntityFallingTree) world.getBlockEntity(pos);
			if(tree!=null)
			{
				tree.onBlockBreak();
			}
		}
	}
}
