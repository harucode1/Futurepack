package futurepack.common.block.misc;

import futurepack.common.FPConfig;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;

import net.minecraft.item.Item.Properties;

public class ItemWIP extends BlockItem 
{

	public ItemWIP(Block blockIn, Properties builder) 
	{
		super(blockIn, builder);
	}
	
	@Override
	protected boolean allowdedIn(ItemGroup group) 
	{
		if(FPConfig.IS_RESEARCH_DEBUG.getAsBoolean())
			return super.allowdedIn(group);
		return false;
	}
	
	@Override
	public ITextComponent getName(ItemStack it) 
	{
		return new StringTextComponent(TextFormatting.RED + "Only Dev " + TextFormatting.RESET).append(super.getName(it));
	}
}
