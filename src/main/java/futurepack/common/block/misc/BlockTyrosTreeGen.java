package futurepack.common.block.misc;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public class BlockTyrosTreeGen extends Block 
{

	public BlockTyrosTreeGen() 
	{
		super(Block.Properties.copy(Blocks.BEDROCK).noCollission().randomTicks().noDrops().lightLevel(state -> 15));
	}

	@Override
	public boolean hasTileEntity(BlockState state) 
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityTyrosTreeGen();
	}
}
