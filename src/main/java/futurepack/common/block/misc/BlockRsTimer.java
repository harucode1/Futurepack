package futurepack.common.block.misc;

import java.util.List;
import java.util.Random;

import com.google.common.base.Predicates;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.play.server.SPlaySoundPacket;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class BlockRsTimer extends BlockHoldingTile
{
	public static final IntegerProperty TIME = IntegerProperty.create("time", 0, 8);
	
	public BlockRsTimer(Block.Properties props)
	{
		super(props);
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(TIME);
	}
	
	private int getTime(BlockState state)
	{
		return state.getValue(TIME);
	}
	
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random random)
	{
		TileEntityRsTimer timer = (TileEntityRsTimer) w.getBlockEntity(pos);
		
		int next, current;
		current = next = getTime(state);	
		next++;
		next %= 9;
		int delay = timer.getDelay(next);

		state = state.setValue(TIME, next);
		w.setBlockAndUpdate(pos, state);
		 
		if(next == 8)
		{
			SPlaySoundPacket okt = new SPlaySoundPacket(SoundEvents.LEVER_CLICK.getRegistryName(), SoundCategory.BLOCKS, new Vector3d(pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F), 0.4F, 0.4F);
			
			List<ServerPlayerEntity> clients = w.getEntitiesOfClass(ServerPlayerEntity.class, new AxisAlignedBB(pos.offset(-15, -15, -15), pos.offset(15,15,15)));
			
			clients.stream().map( mp -> mp.connection).filter(Predicates.notNull()).forEach(c -> c.send(okt));
			
		}
		if(!w.getBlockTicks().hasScheduledTick(pos, this))
			w.getBlockTicks().scheduleTick(pos, this, delay);
	}
	
	@Override
	public void onPlace(BlockState state, World w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(oldState.getBlock()!=this)
			w.getBlockTicks().scheduleTick(pos, this, 0);
		
		if(w.isClientSide)
		{
			throw new RuntimeException();
		}
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.RS_TIMER.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}
	
	
	
	@Override
	public boolean canConnectRedstone(BlockState state, IBlockReader world, BlockPos pos, Direction side)
	{
		return true;
	}
	
	@Override
	public boolean isSignalSource(BlockState state)
    {
        return getTime(state) == 8;
    }
    
    @Override
    public int getSignal(BlockState state, IBlockReader blockAccess, BlockPos pos, Direction side)
    {
    	return getTime(state) == 8 ? 15 : 0;
    }

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityRsTimer();
	}
	
	
}
