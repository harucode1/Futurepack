package futurepack.common.block.misc;

import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockRFtoNEConverted extends BlockRotateable implements IItemMetaSubtypes
{
	
	public BlockRFtoNEConverted(Block.Properties props)
	{
		super(props);
	}

	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.RF2NE_CONVERTER.openGui(pl, pos);
		}
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public int getMaxMetas()
	{
		return 12;
	}

	@Override
	public String getMetaName(int meta) 
	{
		return "rf_ne_converter_"+meta/4;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityRFtoNEConverter();
	}


}
