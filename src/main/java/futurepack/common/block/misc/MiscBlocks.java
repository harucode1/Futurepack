package futurepack.common.block.misc;

import futurepack.api.Constants;
import futurepack.common.FuturepackMain;
import futurepack.common.block.BlockLinkedLight;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.inventory.InventoryBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItem;
import net.minecraft.item.DyeColor;
import net.minecraft.item.Item;
import net.minecraftforge.common.PlantType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class MiscBlocks 
{
	
	public static final Block quantanium = new BlockQuantanium(Block.Properties.of(Material.METAL, DyeColor.PURPLE).strength(3F, 5F).lightLevel(state -> 12)).setRegistryName(Constants.MOD_ID, "quantanium");
	public static final Block extern_cooler = new BlockExternCooler(Block.Properties.of(Material.METAL).strength(5F, 10F).noOcclusion()).setRegistryName(Constants.MOD_ID, "extern_cooler");
	public static final Block bedrock_rift = new BlockBedrockRift().setRegistryName(Constants.MOD_ID, "bedrock_rift");
	public static final Block falling_tree = new BlockFallingTree(Block.Properties.copy(Blocks.BEDROCK).sound(SoundType.WOOD).noOcclusion().noDrops()).setRegistryName(Constants.MOD_ID, "falling_tree");
	
	public static final Block antenna_white = new BlockAntenna(InventoryBlocks.maschine_white).setRegistryName(Constants.MOD_ID, "antenna_white");
	public static final Block antenna_gray = new BlockAntenna(InventoryBlocks.maschine_light_gray).setRegistryName(Constants.MOD_ID, "antenna_gray");
	public static final Block antenna_black = new BlockAntenna(InventoryBlocks.maschine_black).setRegistryName(Constants.MOD_ID, "antenna_black");
	
	public static final Block teleporter = new BlockTeleporter(InventoryBlocks.simple_white, false, false).setRegistryName(Constants.MOD_ID, "teleporter");
	public static final Block teleporter_up = new BlockTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F), true, false).setRegistryName(Constants.MOD_ID, "teleporter_up");
	public static final Block teleporter_down = new BlockTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F), false, true).setRegistryName(Constants.MOD_ID, "teleporter_down");
	public static final Block teleporter_both = new BlockTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F), true, true).setRegistryName(Constants.MOD_ID, "teleporter_both");
	
	public static final Block beam = new BlockDungeonTeleporter(Block.Properties.of(Material.METAL, MaterialColor.COLOR_BLACK).strength(3F, 5F).noDrops(), false, false).setRegistryName(Constants.MOD_ID, "beam");
	public static final Block beam_up = new BlockDungeonTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F).noDrops(), true, false).setRegistryName(Constants.MOD_ID, "beam_up");
	public static final Block beam_down = new BlockDungeonTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F).noDrops(), false, true).setRegistryName(Constants.MOD_ID, "beam_down");
	public static final Block beam_both = new BlockDungeonTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F).noDrops(), true, true).setRegistryName(Constants.MOD_ID, "beam_both");
	
	public static final Block sapling_holder_plains = new BlockSaplingHolder(Block.Properties.of(Material.METAL, MaterialColor.PLANT).strength(3F, 5F), PlantType.PLAINS).setRegistryName(Constants.MOD_ID, "sapling_holder_plains");
	public static final Block sapling_holder_desert = new BlockSaplingHolder(Block.Properties.of(Material.METAL, MaterialColor.SAND).strength(3F, 5F), PlantType.DESERT).setRegistryName(Constants.MOD_ID, "sapling_holder_desert");
	public static final Block sapling_holder_nether = new BlockSaplingHolder(Block.Properties.of(Material.METAL, MaterialColor.COLOR_BROWN).strength(3F, 5F), PlantType.NETHER).setRegistryName(Constants.MOD_ID, "sapling_holder_nether");
	
	public static final Block neon_engine = new BlockNeonEngine(Block.Properties.copy(DecoBlocks.color_iron_blue).noOcclusion()).setRegistryName(Constants.MOD_ID, "neon_engine"); 
	public static final Block gravity_pulser = new BlockGravityPulser(Block.Properties.of(Material.STONE, MaterialColor.TERRACOTTA_LIGHT_BLUE).strength(5F, 10F)).setRegistryName(Constants.MOD_ID, "gravity_pulser");
	public static final Block magnet = new BlockMagnet(Block.Properties.of(Material.METAL).strength(5F, 10F)).setRegistryName(Constants.MOD_ID, "magnet");
	public static final Block rs_timer = new BlockRsTimer(Block.Properties.of(Material.DECORATION, MaterialColor.COLOR_LIGHT_GRAY).sound(SoundType.METAL).strength(3F, 5F)).setRegistryName(Constants.MOD_ID, "rs_timer");
	public static final Block force_field = new BlockForceField(Block.Properties.copy(Blocks.BEDROCK).noOcclusion().isSuffocating((state,world,pos) -> false).isViewBlocking((state,world,pos) -> false)).setRegistryName(Constants.MOD_ID, "force_field");
	public static final Block dungeon_spawner = new BlockDungeonSpawner(Block.Properties.copy(Blocks.BEDROCK).noOcclusion()).setRegistryName(Constants.MOD_ID, "dungeon_spawner");
	
	public static final Block wirelessRedstoneReceiver = new BlockWirelessRedstoneReceiver(InventoryBlocks.simple_black).setRegistryName(Constants.MOD_ID, "wr_receiver");
	public static final Block wirelessRedstoneTransmitter = new BlockWirelessRedstoneTransmitter(InventoryBlocks.simple_black, false).setRegistryName(Constants.MOD_ID, "wr_transmitter");
	public static final Block wirelessRedstoneTransmitterInverted = new BlockWirelessRedstoneTransmitter(InventoryBlocks.simple_black, true).setRegistryName(Constants.MOD_ID, "wr_transmitter_i");
	public static final Block airlock_door = new BlockAirlockDoor(InventoryBlocks.maschine_light_gray).setRegistryName(Constants.MOD_ID, "airlock_door");
	public static final Block dungeon_core = new BlockDungeonCore(Block.Properties.copy(Blocks.BEDROCK).noDrops()).setRegistryName(Constants.MOD_ID, "dungeon_core");
	public static final Block fp_lever = new BlockFpLever(Block.Properties.copy(DecoBlocks.color_iron_fence_white)).setRegistryName(Constants.MOD_ID, "fp_lever");
	public static final Block fp_button = new BlockFpButton(Block.Properties.copy(DecoBlocks.color_iron_fence_white)).setRegistryName(Constants.MOD_ID, "button");
	
	public static final Block tyros_tree_gen = new BlockTyrosTreeGen().setRegistryName(Constants.MOD_ID, "tyros_tree_gen");
	
	public static final Block rf2ne_converter_white = new BlockRFtoNEConverted(InventoryBlocks.maschine_white).setRegistryName(Constants.MOD_ID, "rf2ne_converter_white");
	public static final Block rf2ne_converter_gray = new BlockRFtoNEConverted(InventoryBlocks.maschine_light_gray).setRegistryName(Constants.MOD_ID, "rf2ne_converter_gray");
	public static final Block rf2ne_converter_black = new BlockRFtoNEConverted(InventoryBlocks.maschine_black).setRegistryName(Constants.MOD_ID, "rf2ne_converter_black");
	
	public static final Block modular_door = new BlockModularDoor(Block.Properties.of(Material.METAL, MaterialColor.COLOR_LIGHT_GRAY).strength(3F, 5F).dynamicShape().noOcclusion()).setRegistryName(Constants.MOD_ID, "modular_door");
	public static final Block fish_block = new BlockFish(Block.Properties.of(Material.CAKE, MaterialColor.COLOR_CYAN).sound(SoundType.WOOL).strength(1.5F, 2.5F).noDrops().noOcclusion()).setRegistryName(Constants.MOD_ID, "fish_block");
	public static final Block claime = new BlockClaime(Block.Properties.of(Material.METAL, DyeColor.YELLOW).strength(5F, 2000F).noOcclusion()).setRegistryName(Constants.MOD_ID, "claime");
	public static final Block linked_light = new BlockLinkedLight(Block.Properties.of(Material.AIR).noDrops().strength(0.0F).lightLevel(state -> 15)).setRegistryName(Constants.MOD_ID, "linked_light");
	public static final Block structure_fix_helper = new BlockStructureFixHelper(Block.Properties.of(Material.HEAVY_METAL).strength(2000.0F).lightLevel(state -> 15)).setRegistryName(Constants.MOD_ID, "structure_fix_helper");
	public static final Block tectern = new BlockTectern(InventoryBlocks.maschine_black).setRegistryName(Constants.MOD_ID, "tectern");
	public static final Block dungeon_checkpoint = new BlockDungeonCheckpoint(InventoryBlocks.maschine_white_notsolid).setRegistryName(Constants.MOD_ID, "dungeon_checkpoint");
	
	public static final Block door_marker = new BlockDoorMarker().setRegistryName(Constants.MOD_ID, "door_marker");
	
	
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> r = event.getRegistry();
		
		r.registerAll(quantanium, extern_cooler);
		r.registerAll(bedrock_rift, falling_tree);
		r.registerAll(antenna_white, antenna_gray, antenna_black);
		r.registerAll(teleporter, teleporter_up, teleporter_down, teleporter_both);
		r.registerAll(beam, beam_up, beam_down, beam_both);
		r.registerAll(sapling_holder_plains, sapling_holder_desert, sapling_holder_nether);
		r.registerAll(neon_engine, gravity_pulser, magnet, rs_timer, force_field, dungeon_spawner);
		r.registerAll(wirelessRedstoneReceiver, wirelessRedstoneTransmitter, wirelessRedstoneTransmitterInverted, airlock_door, dungeon_core, fp_lever, fp_button);
		r.registerAll(tyros_tree_gen);
		r.registerAll(rf2ne_converter_white, rf2ne_converter_gray, rf2ne_converter_black);
		r.registerAll(modular_door, fish_block, claime, linked_light, structure_fix_helper, tectern, dungeon_checkpoint);
		
		r.registerAll(door_marker);
	}
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.register(item(quantanium));
		r.register(item(extern_cooler));
		r.register(item(bedrock_rift));
		r.registerAll(item(antenna_white), item(antenna_gray), item(antenna_black));
		r.registerAll(item(teleporter), item(teleporter_up), item(teleporter_down), item(teleporter_both));
		r.registerAll(item(beam), item(beam_up), item(beam_down), item(beam_both));
		r.registerAll(item(sapling_holder_plains), item(sapling_holder_desert), item(sapling_holder_nether));
		r.registerAll(item(neon_engine), item(gravity_pulser), item(magnet), item(rs_timer), item(force_field), item(dungeon_spawner));
		r.registerAll(item(wirelessRedstoneReceiver), item(wirelessRedstoneTransmitter), item(wirelessRedstoneTransmitterInverted), item(airlock_door), item(dungeon_core), item(fp_lever), item(fp_button));
		r.registerAll(item(rf2ne_converter_white), item(rf2ne_converter_gray), item(rf2ne_converter_black));
		r.registerAll(item(modular_door), item(fish_block), item(claime), itemWIP(structure_fix_helper), item(tectern), item(dungeon_checkpoint));
		
		r.registerAll(itemWIP(door_marker));
	}
	
	private static final Item item(Block bl)
	{
		return new BlockItem(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(bl.getRegistryName());
	}
	
	private static final Item itemWIP(Block bl)
	{
		return new ItemWIP(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(bl.getRegistryName());
	}
}
