package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import futurepack.common.player.FakePlayerFactory;
import futurepack.common.player.FuturepackPlayer;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;


public class TileEntitySaplingHolder extends TileEntity implements ITickableTileEntity
{
	//@Nullable NO NO NO!!!!!
	private ItemStack filter = ItemStack.EMPTY;
	
	private SaplingInserter itemHandler = new SaplingInserter();
	
	public TileEntitySaplingHolder() 
	{
		super(FPTileEntitys.SAPLING_HOLDER);
	}
	
	public ItemStack getFilter()
	{
		return filter;
	}
	
	@Override
	public void tick()
	{
		if(level.isClientSide)
			return;
		
		if(itemHandler.contained!=null)
		{
			ItemStack it = itemHandler.contained;
			itemHandler.contained = tryPlace(it);
			if(itemHandler.contained.isEmpty())
			{
				itemHandler.contained=ItemStack.EMPTY;
			}
		}
	}
	
	
	public void setFilter(ItemStack it)
	{
		filter = it;
		filter.setCount(1);
		setChanged();
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pack;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		if(level.isClientSide)
		{
			load(getBlockState(), pkt.getTag());
		}
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	public ItemStack tryPlace(ItemStack it)
	{
		if(!level.isClientSide)
		{
			if(!level.isEmptyBlock(worldPosition.above()))
				return it;
			
			if(filter.isEmpty() || HelperInventory.areItemsEqualNoSize(it, filter))
			{
				Direction face = Direction.DOWN;
				
				FuturepackPlayer pl = FakePlayerFactory.INSTANCE.getPlayer((ServerWorld) level);
				FakePlayerFactory.setupPlayer(pl, worldPosition.above(1), face);
				pl.startUsingItem(Hand.MAIN_HAND);
				pl.setItemInHand(Hand.MAIN_HAND, it);
				return FakePlayerFactory.itemClick(level, worldPosition.above(1), face.getOpposite(), it, pl, Hand.MAIN_HAND).getObject();
			}
		}
		return it;
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		if(!filter.isEmpty())
		{
			CompoundNBT tagt = new CompoundNBT();
			filter.save(tagt);
			nbt.put("filter", tagt);
		}
		
		nbt.put("items", itemHandler.serializeNBT());
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		if(nbt.contains("filter"))
		{
			filter = ItemStack.of(nbt.getCompound("filter"));
		}
		itemHandler.deserializeNBT(nbt.getCompound("items"));
		super.load(state, nbt);
	}
	
	private LazyOptional<IItemHandler> item_opt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(item_opt != null)
			{
				return (LazyOptional<T>) item_opt;
			}
			else
			{
				item_opt = LazyOptional.of(() -> itemHandler);
				item_opt.addListener(p -> item_opt = null);
				return (LazyOptional<T>) item_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(item_opt);
		super.setRemoved();
	}
	
	public class SaplingInserter implements IItemHandler, INBTSerializable<CompoundNBT>
	{
		private ItemStack contained = ItemStack.EMPTY;
		
		@Override
		public int getSlots() 
		{
			
			return 1;
		}
		
		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return contained;
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			if(stack.isEmpty())
				return ItemStack.EMPTY;
				
			if(isItemValid(slot, stack) && contained.isEmpty())
			{
				if(simulate)
				{
					ItemStack st = stack.copy();
					st.split(1);
					return st;
				}
				else
				{
					ItemStack st = stack.copy();
					contained = st.split(1);
					return st;
				}			
			}
			return stack;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate) 
		{
			return ItemStack.EMPTY;
		}

		@Override
		public CompoundNBT serializeNBT()
		{
			CompoundNBT nbt = new CompoundNBT();
			if(!contained.isEmpty())
			{
				contained.save(nbt);
			}
			return nbt;
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt)
		{
			contained = ItemStack.of(nbt);
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return 1;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack) 
		{
			return HelperInventory.areItemsEqualNoSize(stack, filter);
		}
		
	}
}
