package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import futurepack.common.FPTileEntitys;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public class TileEntityResearchExchange extends TileEntity 
{
	private Set<Research> stored = new TreeSet<>();
	
	public TileEntityResearchExchange(TileEntityType<? extends TileEntityResearchExchange> type) 
	{
		super(type);
	}
	
	public TileEntityResearchExchange() 
	{
		this(FPTileEntitys.RESEARCH_EXCHANGE);
	}

	public void onInteract(PlayerEntity pl)
	{
		CustomPlayerData cd = CustomPlayerData.getDataFromPlayer(pl);	
		
		stored.forEach(cd::addResearchUnsafe);
		
		for(String rid : cd.getAllResearches())
		{
			Research r = ResearchManager.getResearchOrElse(rid, null);
			if(r!=null)
			{
				stored.add(r);
			}
		}
	}
	
	@Override
	public CompoundNBT save(CompoundNBT compound) 
	{
		ListNBT list = new ListNBT();
		for(Research r : stored)
		{
			list.add(StringNBT.valueOf(r.getName()));
		}
		compound.put("researches", list);
		return super.save(compound);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT compound) 
	{
		ListNBT list = compound.getList("researches", StringNBT.valueOf("").getId());
		ArrayList<Research> arraylist = new ArrayList<Research>(list.size());
		for(int i=0;i<list.size();i++)
		{
			arraylist.add(ResearchManager.getResearch(list.getString(i)));
		}
		stored.clear();
		stored.addAll(arraylist);
		super.load(state, compound);
	}
}
