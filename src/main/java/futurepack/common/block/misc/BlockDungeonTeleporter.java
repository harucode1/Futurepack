package futurepack.common.block.misc;

import futurepack.world.WorldGenTecDungeon;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockDungeonTeleporter extends Block
{
	private final boolean up, down;

	protected BlockDungeonTeleporter(Properties props, boolean up, boolean down) 
	{
		super(props);
		this.up = up;
		this.down = down;
		
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(w.isClientSide)
			return ActionResultType.SUCCESS;
			
		MinecraftServer serv = w.getServer();
		if(serv!=null)
		{
			WorldGenTecDungeon.handleTeleporterClick(w, serv, pos, pl, state);
		}
		return ActionResultType.SUCCESS;
	}

	public boolean isUp()
	{
		return up;
	}
	
	public boolean isDown()
	{
		return down;
	}
	
	@Override
	public boolean hasTileEntity(BlockState state) 
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityDungeonTeleporter();
	}
}
