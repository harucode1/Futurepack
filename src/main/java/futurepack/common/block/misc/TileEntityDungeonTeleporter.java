package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public class TileEntityDungeonTeleporter extends TileEntity 
{

	public TileEntityDungeonTeleporter(TileEntityType<TileEntityDungeonTeleporter> tileEntityTypeIn) 
	{
		super(tileEntityTypeIn);
	}

	public TileEntityDungeonTeleporter() 
	{
		super(FPTileEntitys.TELEPORTER_DUNGEON);
	}
	
	private BlockPos otherTeleporter = null;
	private ResourceLocation targetDimension = null;
	
	@Override
	public CompoundNBT save(CompoundNBT nbt) 
	{
		if(otherTeleporter!=null)
		{
			nbt.putIntArray("teleport", new int[]{otherTeleporter.getX(), otherTeleporter.getY(), otherTeleporter.getZ()});
		}
		if(targetDimension!=null)
		{
			nbt.putString("dimension", targetDimension.toString());
		}
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt) 
	{
		super.load(state, nbt);
		if(nbt.contains("dimension"))
		{
			targetDimension = new ResourceLocation(nbt.getString("dimension"));
		}
		if(nbt.contains("teleport"))
		{
			int[] t = nbt.getIntArray("teleport");
			otherTeleporter = new BlockPos(t[0], t[1], t[2]);
		}
	}
	
	public void setTargetTeleporter(BlockPos pos, World w)
	{
		otherTeleporter = pos;
		targetDimension = w.dimension().location();
		this.setChanged();
	}

	public BlockPos getOtherTeleporter() {
		return otherTeleporter;
	}

	public RegistryKey<World> getTargetDimension() 
	{
		RegistryKey<World> registrykey = RegistryKey.create(Registry.DIMENSION_REGISTRY, targetDimension);
		return registrykey;
	}
	
	
}
