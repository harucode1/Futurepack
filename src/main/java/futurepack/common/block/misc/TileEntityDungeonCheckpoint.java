package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITileRenameable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.dim.structures.NameGenerator;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.server.ServerChunkProvider;
import net.minecraft.world.server.TicketType;

public class TileEntityDungeonCheckpoint extends TileEntity implements ITileRenameable
{
	private ITextComponent name;

	public TileEntityDungeonCheckpoint(TileEntityType<TileEntityDungeonCheckpoint> tileEntityTypeIn) 
	{
		super(tileEntityTypeIn);
	}

	public TileEntityDungeonCheckpoint() 
	{
		this(FPTileEntitys.DUNGEON_CHECKPOINT);
	}

	@Override
	public ITextComponent getName() 
	{
		if(name==null)
		{
			name = new StringTextComponent(NameGenerator.DUNGEON.getRandomName(level.random));
			setChanged();
		}
		return name;
	}

	@Override
	public void setName(String s) 
	{
		this.name = new StringTextComponent(s);
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt) 
	{
		if(name!=null)
			nbt.putString("name", ITextComponent.Serializer.toJson(name));
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt) 
	{
		super.load(state, nbt);
		if(nbt.contains("name"))
			name = ITextComponent.Serializer.fromJsonLenient(nbt.getString("name"));
	}
	
	public boolean teleportTo(Entity e, BlockPos pos)
	{
		if(level.getChunkSource() instanceof ServerChunkProvider)
		{
			ServerChunkProvider prov = (ServerChunkProvider) level.getChunkSource();
			ChunkPos destination = new ChunkPos(pos);
			prov.addRegionTicket(TicketType.POST_TELEPORT, destination, 0, e.getId());
			
			BlockState state = level.getBlockState(pos);
			if(state.hasProperty(BlockRotateableTile.FACING))
			{
				Direction dir = state.getValue(BlockRotateableTile.FACING);
				BlockPos set = pos.relative(dir);
				if(level.isEmptyBlock(set))
				{
					if(!level.isEmptyBlock(set.above()))
					{
						set = set.below();
					}
					e.teleportToWithTicket(set.getX()+0.5, set.getY()+0.125, set.getZ()+0.5);
					return true;
				}
			}
		}
		return false;
	}
}
