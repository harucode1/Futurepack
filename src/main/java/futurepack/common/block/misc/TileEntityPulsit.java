package futurepack.common.block.misc;

import java.util.List;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.PushReaction;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.entity.item.HangingEntity;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityPulsit extends TileEntity implements ITickableTileEntity
{
	boolean last = false;
	boolean deferedTick = false;
	
	
	public TileEntityPulsit() 
	{
		super(FPTileEntitys.GRAVITY_PULSER);
	}
	@Override
	public void tick() 
	{		
		boolean now =  level.getBestNeighborSignal(worldPosition)>0;
		
		if(!last&&now || deferedTick)
		{
			deferedTick = false;
			Direction face = getBlockState().getValue(BlockRotateableTile.FACING);
			
			if(createFallingBlock(level, worldPosition.relative(face)) && !deferedTick)
			{
				deferedTick = true;
				return;
			}
			
			int range = 20;
			List<Entity> list = level.getEntities(null, new AxisAlignedBB(-range, -range, -range, range, range, range).move(worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5));//, null);//getEntitiesWithinAABBExcludingEntity
			
			for(Entity item : list)
			{
				if(item instanceof HangingEntity)
				{
					continue;
				}
				
//				double[] d = invertDirektion(d3, d4, d5, dir);
//				d3 = d[0];
//				d4 = d[1];
//				d5 = d[2];
//				
//				d3 = mincheck(d3);
//				d4 = mincheck(d4);
//				d5 = mincheck(d5);
//				
//				double dis = Math.sqrt(getDistanceSq(item.posX, item.posY, item.posZ));
//				
//				d3 = d3/dis *2;
//				d4 = d4/dis *2;
//				d5 = d5/dis *2;
				
				double dis = Math.sqrt(worldPosition.distSqr(item.getX(), item.getY(), item.getZ(), false));
				double mX = face.getStepX()*2;
				double mY = face.getStepY()*2;
				double mZ = face.getStepZ()*2;
				mX *= (20/dis/20);
				mY *= (20/dis/20);
				mZ *= (20/dis/20);
				item.push(mX, mY, mZ);
				item.fallDistance = 0F;
			}
		}
		last = now;
	}
	
	public static boolean createFallingBlock(World w, BlockPos pos)
	{
//		if(!w.isRemote)
		{
			BlockState state = w.getBlockState(pos);
			if(state.isAir(w, pos))
				return false;
			
			PushReaction reaction = state.getPistonPushReaction();
			if(reaction == PushReaction.NORMAL || reaction == PushReaction.PUSH_ONLY)
			{
				FallingBlockEntity falling = new FallingBlockEntity(w, pos.getX()+0.5, pos.getY()+0.1, pos.getZ()+0.5, state);
				return w.addFreshEntity(falling);
				
			}
		}
		
		return false;
	}
}
