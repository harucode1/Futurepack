package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;

public class TileEntityAirlockDoor extends TileEntity implements ITickableTileEntity
{
	public long switched = 0;
	private int deadTime = 2;
	public boolean extended = false;
	
	public TileEntityAirlockDoor()
	{
		super(FPTileEntitys.AIRLOCK_DDOR);
		switched = System.currentTimeMillis();
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return new AxisAlignedBB(-1,-1,-1,2,2,2).move(worldPosition.getX(), worldPosition.getY(), worldPosition.getZ());
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		if(level.isClientSide)
			extended = getBlockState().getValue(BlockAirlockDoor.EXTENDED);
	}

	@Override
	public void tick() 
	{
		if(level.isClientSide)
		{
			if(--deadTime <= 0)
			{
				deadTime = 2;
				if(extended != getBlockState().getValue(BlockAirlockDoor.EXTENDED))
				{
					switched = System.currentTimeMillis();
					extended = !extended;
					deadTime = 15;
				}
			}
		}
	}
}
