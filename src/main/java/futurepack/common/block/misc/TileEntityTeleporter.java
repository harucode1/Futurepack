package futurepack.common.block.misc;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityTeleporter extends TileEntity 
{
	

	public TileEntityTeleporter() 
	{
		super(FPTileEntitys.TELEPORTER);
	}

	public CapabilityNeon energy = new CapabilityNeon(200, EnumEnergyMode.USE);
	public LazyOptional<CapabilityNeon> opt;
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("energy", energy.serializeNBT());
		return nbt;
	}
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		energy.deserializeNBT(nbt.getCompound("energy"));
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		SUpdateTileEntityPacket pkt = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pkt;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		load(getBlockState(), pkt.getTag());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(opt==null)
			{
				opt = LazyOptional.of(()->energy);
				opt.addListener(p -> opt = null);
			}
			return (LazyOptional<T>) opt;
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(opt);
		super.setRemoved();
	}

}
