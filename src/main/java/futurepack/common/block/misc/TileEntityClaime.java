package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;

public class TileEntityClaime extends TileEntity implements ITickableTileEntity, ITilePropertyStorage
{
	private static int ID = 0;
	
	public int x=2,y=2,z=2;
	public int mx=0,my=0,mz=0;
	public boolean renderAll=true;
	public String name = "Claime";
//	private Ticket chunckLoader;
	private String playerName = "Minecraft";
	
	public TileEntityClaime()
	{
		super(FPTileEntitys.CLAIME);
		name+=ID;
		ID++;
	}
	
	@Override
	public void tick() 
	{
//		if(!world.isRemote && !FPConfig.disableClaimeChunkloading)
//		{
//			if(chunckLoader==null)
//				chunckLoader = FPChunckManager.getClaimeTicketForPlayer(world, playerName);
//			
//			ForgeChunkManager.forceChunk(chunckLoader, new ChunkPos(pos.getX()>>4, pos.getZ()>>4));
//			
//		}
		
		if(x<2)
			x=2;
		if(y<2)
			y=2;
		if(z<2)
			z=2;
		
	}
//	
//	@Override
//	public void setWorldObj(World w) 
//	{
//		super.setWorldObj(w);
//		ArrayList<TileEntityClaime> claimes = new ArrayList<TileEntityClaime>();
//		for(Object o : w.loadedTileEntityList)
//		{
//			if(o instanceof TileEntityClaime)
//			{
//				if(o==this)
//					continue;
//				
//				claimes.add((TileEntityClaime) o);
//			}
//		}
//		
//		for(int i=0;i<claimes.size();i++)
//		{
//			TileEntityClaime c = claimes.get(i);
//			if(name.equals(c.name))
//			{
//				ID++;
//				name  = "Claime" + ID;
//				i=0;
//			}
//		}
//	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pack;
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		return nbt;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		super.load(getBlockState(), pkt.getTag());
		super.onDataPacket(net, pkt);
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.putInt("sx", x);
		nbt.putInt("sy", y);
		nbt.putInt("sz", z);
		nbt.putInt("mx", mx);
		nbt.putInt("my", my);
		nbt.putInt("mz", mz);
		nbt.putBoolean("render", renderAll);
		nbt.putString("name", name);
		nbt.putString("player", playerName);
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		x = nbt.getInt("sx");
		y = nbt.getInt("sy");
		z = nbt.getInt("sz");
		mx = nbt.getInt("mx");
		my = nbt.getInt("my");
		mz = nbt.getInt("mz");
		renderAll = nbt.getBoolean("render");
		name = nbt.getString("name");
		playerName = nbt.getString("player");
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() 
	{
		return new AxisAlignedBB(worldPosition.getX()-x, worldPosition.getY()-y, worldPosition.getZ()-z, worldPosition.getX()+x+1, worldPosition.getY()+y, worldPosition.getZ()+z+1).move(mx, my, mz);
	}

	public void BroudcastData() 
	{
		setChanged();
	}
	
	@Override
	public void onChunkUnloaded()
	{
//		if(chunckLoader!=null)
//		{
//			ForgeChunkManager.unforceChunk(chunckLoader, new ChunkPos(pos.getX()>>4, pos.getZ()>>4));
//		}
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		case 3:
			return mx;
		case 4:
			return my;
		case 5:
			return mz;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			x = value;
			break;
		case 1:
			y = value;
			break;
		case 2:
			z = value;
			break;
		case 3:
			mx = value;
			break;
		case 4:
			my = value;
			break;
		case 5:
			mz = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 6;
	}

	public void onPlaced(LivingEntity placer)
	{
		playerName = placer.getName().getString();
	}
	
}
