package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.stream.Stream;

import futurepack.common.FPTileEntitys;
import futurepack.common.dim.structures.enemys.EnemyEntry;
import futurepack.common.dim.structures.enemys.EnemyWave;
import futurepack.common.dim.structures.enemys.WaveLoader;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.play.server.STitlePacket;
import net.minecraft.network.play.server.STitlePacket.Type;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class TileEntityDungeonSpawner extends TileEntity implements ITickableTileEntity
{
	public static final int range = 3;
	
	static ITextComponent start = new TranslationTextComponent("dungeon.message.start");
	static ITextComponent cleared = new TranslationTextComponent("dungeon.message.cleared");
	static ITextComponent finished = new TranslationTextComponent("dungeon.message.finished");
	
	private ArrayList<EnemyWave> waves = new ArrayList<EnemyWave>();
	
	private boolean solved = false;
	private boolean aktiv = false;
	private int waveNum = 0;
	private int coolDown = -1;
	private int redstonePower = 5;
	
	private boolean firstTick = true;
	
	public TileEntityDungeonSpawner()
	{
		super(FPTileEntitys.DUNGEON_SPAWNER);
		waves.add(WaveLoader.createWaveFromId("default"));
	}
	
	@Override
	public void onChunkUnloaded()
	{
		reset();
		super.onChunkUnloaded();
	}

	@Override
	public void tick()
	{
		if(!level.isClientSide)
		{
			if(firstTick)
			{
				level.updateNeighborsAt(worldPosition, getBlockState().getBlock());
				firstTick=false;
			}
			if(!solved)
			{
				PlayerEntity pl = level.getNearestPlayer(worldPosition.getX() +0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5, range, false);
				if(pl!=null && !aktiv)
				{
					start();
				}
				else if(aktiv)
				{
					nextWave();
				}
			}
		}
	}	
	
	public void reset()
	{
		if(aktiv)
		{
			solved = false;
			waveNum = 0;
			coolDown = -1;
			aktiv = false;
			
			waves.forEach(EnemyWave::clear);
		}		
	}
	
	public void start()
	{
		if(!aktiv && !waves.isEmpty())
		{
			aktiv = true;
			waveNum = 0;
			coolDown = -1;
			Direction face = level.getBlockState(worldPosition).getValue(BlockDungeonSpawner.FACING);
			
			level.updateNeighborsAt(worldPosition, getBlockState().getBlock());
			
			waves.get(waveNum).startWave(level, worldPosition.relative(face));
			broadcastMessage(start, waves.get(waveNum).subtitle);
		}
	}
	
	public void broadcastMessage(ITextComponent title, ITextComponent subtitle)
	{
		STitlePacket subt = new STitlePacket(Type.SUBTITLE, subtitle);
		STitlePacket titl = new STitlePacket(Type.TITLE, title);
		
		int r = 128;
		Stream<ServerPlayerEntity> list = level.players().stream().filter(p -> p instanceof ServerPlayerEntity).filter(pl -> worldPosition.distSqr(pl.blockPosition()) <= r*r).map(p -> (ServerPlayerEntity)p);
		list.forEach(mp -> {
			if(subtitle!=null)
				mp.connection.send(subt);
			mp.connection.send(titl);
		});
	}
	
	public void nextWave()
	{
		if(aktiv)
		{
			if(waves.isEmpty())
			{
				reset();
				return;
			}
			
			if(waveNum >= waves.size())
			{
				aktiv = false;
				solved = true;
				setChanged();
				level.updateNeighborsAt(worldPosition, getBlockState().getBlock());
			}
			else if(waves.get(waveNum).isComplete(level))
			{
				if(coolDown<0)
				{
					broadcastMessage(cleared, null);
				}
					
				if(coolDown>= waves.get(waveNum).getCoolDown())
				{
					waveNum++;
					if(waveNum < waves.size())
					{
						waves.get(waveNum).startWave(level, worldPosition);
						broadcastMessage(start, waves.get(waveNum).subtitle);
						coolDown = -1;
					}
					else
					{
						solved = true;
						aktiv = false;
						setChanged();
						level.updateNeighborsAt(worldPosition, getBlockState().getBlock());
						broadcastMessage(finished, null);
					}
				}	
				else
				{
					coolDown++;
				}
			}
		}
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.putBoolean("solved", solved);
		nbt.putBoolean("aktiv", aktiv);
		nbt.putInt("waveNum", waveNum);
		nbt.putInt("coolDown", coolDown);
		nbt.putInt("redpower", redstonePower);
		
		ListNBT list = new ListNBT();
		for(EnemyWave w : waves)
		{
			list.add(w.serializeNBT());
		}
		nbt.put("waves", list);
		
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		solved = nbt.getBoolean("solved");
		aktiv = nbt.getBoolean("aktiv");
		waveNum = nbt.getInt("waveNum");
		coolDown = nbt.getInt("coolDown");
		redstonePower = nbt.getInt("redpower");
		
		waves = new ArrayList<EnemyWave>();
		ListNBT list = nbt.getList("waves", 10);
		for(int i=0;i<list.size();i++)
		{
			waves.add(WaveLoader.createWaveFromNBT(list.getCompound(i)));
		}
		super.load(state, nbt);
	}

	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	public int getRedstonePower()
	{
		return solved ? redstonePower : 0;
	}
	
	public int getWaveCount()
	{
		return waves.size();
	}
	
	
	private int[][] waveColors = null;
	
	public int[] getWaveColors(int wave)
	{
		if(waveColors==null)
		{
			waveColors = new int[getWaveCount()][];
			for(int i=0;i<waveColors.length;i++)
			{
				EnemyWave wav = waves.get(i);
				float total = wav.getEnemyTotal();
				waveColors[i] = new int[wav.enemys.size()];
				for(int j=0;j<waveColors[i].length;j++)
				{
					EnemyEntry ent = wav.enemys.get(j);
					waveColors[i][j] = ent.getDangerColor(ent.getEnemyCount() / total);
				}
			}
		}		
		return waveColors[wave];		
	}
	
	public void addWaves(String[] waves )
	{
		this.waves.clear();
		for(String wave : waves)
		{
			this.waves.add(WaveLoader.createWaveFromId(wave));
		}
		
	}
}
