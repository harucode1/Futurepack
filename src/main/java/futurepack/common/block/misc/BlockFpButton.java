package futurepack.common.block.misc;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.StoneButtonBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;

public class BlockFpButton extends StoneButtonBlock
{
    protected static final VoxelShape FPAABB_DOWN_OFF_Z = VoxelShapes.box(0.25D, 0.75D, 0.28125D, 0.75D, 1.0D, 0.71875D);
    protected static final VoxelShape FPAABB_UP_OFF_Z = VoxelShapes.box(0.25D, 0.0D, 0.28125D, 0.75D, 0.25D, 0.71875D);
    
    protected static final VoxelShape FPAABB_DOWN_OFF_X = VoxelShapes.box(0.28125D, 0.75D, 0.25D, 0.71875D, 1.0D, 0.75D);
    protected static final VoxelShape FPAABB_UP_OFF_X = VoxelShapes.box(0.28125D, 0.0D, 0.25D, 0.71875D, 0.25D, 0.75D);
    
    protected static final VoxelShape FPAABB_NORTH_OFF = VoxelShapes.box(0.25D, 0.28125D, 0.75D, 0.75D, 0.71875D, 1.0D);
    protected static final VoxelShape FPAABB_SOUTH_OFF = VoxelShapes.box(0.25D, 0.28125D, 0.0D, 0.75D, 0.71875D, 0.25D);
    protected static final VoxelShape FPAABB_WEST_OFF = VoxelShapes.box(0.75D, 0.28125D, 0.25D, 1.0D, 0.71875D, 0.75D);
    protected static final VoxelShape FPAABB_EAST_OFF = VoxelShapes.box(0.0D, 0.28125D, 0.25D, 0.25D, 0.71875D, 0.75D);
    protected static final VoxelShape FPAABB_DOWN_ON_Z = VoxelShapes.box(0.25D, 0.84375D, 0.28125D, 0.75D, 1.0D, 0.71875D);
    protected static final VoxelShape FPAABB_UP_ON_Z = VoxelShapes.box(0.25D, 0.0D, 0.28125D, 0.75D, 0.15625D, 0.71875D);
    
    protected static final VoxelShape FPAABB_DOWN_ON_X = VoxelShapes.box(0.28125D, 0.84375D, 0.25D, 0.71875D, 1.0D, 0.75D);
    protected static final VoxelShape FPAABB_UP_ON_X = VoxelShapes.box(0.28125D, 0.0D, 0.25D, 0.71875D, 0.15625D, 0.75D);
    
    protected static final VoxelShape FPAABB_NORTH_ON = VoxelShapes.box(0.25D, 0.28125D, 0.84375D, 0.75D, 0.71875D, 1.0D);
    protected static final VoxelShape FPAABB_SOUTH_ON = VoxelShapes.box(0.25D, 0.28125D, 0.0D, 0.75D, 0.71875D, 0.15625D);
    protected static final VoxelShape FPAABB_WEST_ON = VoxelShapes.box(0.84375D, 0.28125D, 0.25D, 1.0D, 0.71875D, 0.75D);
    protected static final VoxelShape FPAABB_EAST_ON = VoxelShapes.box(0.0D, 0.28125D, 0.25D, 0.15625D, 0.71875D, 0.75D);
	
	protected BlockFpButton(Block.Properties props) 
	{
		super(props);
//		setCreativeTab(ItemGroup.REDSTONE);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		Direction enumfacing = state.getValue(FACING);
		boolean flag = state.getValue(POWERED);
		switch(state.getValue(FACE)) 
		{
		case FLOOR:
			if (enumfacing.getAxis() == Direction.Axis.X) {
				return flag ? FPAABB_UP_ON_X : FPAABB_UP_OFF_X;
			}

			return flag ? FPAABB_UP_ON_Z : FPAABB_UP_OFF_Z;
		case WALL:
			switch(enumfacing) 
			{
			case EAST:
				return flag ? FPAABB_EAST_ON : FPAABB_EAST_OFF;
			case WEST:
				return flag ? FPAABB_WEST_ON : FPAABB_WEST_OFF;
			case SOUTH:
				return flag ? FPAABB_SOUTH_ON : FPAABB_SOUTH_OFF;
			case NORTH:
			default:
				return flag ? FPAABB_NORTH_ON : FPAABB_NORTH_OFF;
			}
		case CEILING:
		default:
			if (enumfacing.getAxis() == Direction.Axis.X)
			{
				return flag ? FPAABB_DOWN_ON_X : FPAABB_DOWN_OFF_X;
			}
			else 
			{
				return flag ? FPAABB_DOWN_ON_Z : FPAABB_DOWN_OFF_Z;
			}
		}
	}

    @Override
    protected void playSound(@Nullable PlayerEntity pl, IWorld w, BlockPos pos, boolean clickOn)
    {
        w.playSound(clickOn ? pl : null, pos, this.getSound(clickOn), SoundCategory.BLOCKS, 0.3F, clickOn ? 1.0F : 0.9F);
     }

}
