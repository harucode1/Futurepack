package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import futurepack.world.scanning.ChunkData;
import futurepack.world.scanning.FPChunkScanner;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;

public class TileEntityBedrockRift extends TileEntity
{
	public TileEntityBedrockRift() 
	{
		super(FPTileEntitys.BEDROCK_RIFT);
	}

	private double fillrate = 1.0;
	private boolean scanned = false;
	
	private ChunkData data = null;
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		nbt.putDouble("fillrate", fillrate);
		nbt.putBoolean("scanned", scanned);
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		fillrate = nbt.getDouble("fillrate");
		scanned = nbt.getBoolean("scanned");
		super.load(state, nbt);
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return super.save(new CompoundNBT());
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		if(level.isClientSide)
		{
			load(getBlockState(), pkt.getTag());
		}
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = save(new CompoundNBT());
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pack;
	}
	
	public ChunkData getData()
	{
		if(!scanned)
			return null;
		if(data!=null)
			return data;
		
		data = FPChunkScanner.INSTANCE.getData(level, worldPosition);
		return data;
	}
	
	public double getFillrate()
	{
		return fillrate;
	}

	public void removeOres(double removed)
	{
		fillrate -= removed;
		if(fillrate <= 0.0)
			fillrate = 0.0;
	}

	public void regenerate(double d)
	{
		fillrate += d;
	}
	
	public boolean isScanned()
	{
		return scanned;
	}

	public void setScanned(boolean b)
	{
		scanned = b;
	}

	public int getComparatorOutput()
	{
		int redstone = (int) (fillrate * 15);
		if(fillrate>=1)
			redstone = 15;
		else if(redstone==0 && fillrate>0)
			redstone=1;
		
		return redstone;
	}
}
