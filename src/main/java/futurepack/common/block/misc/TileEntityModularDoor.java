package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;

public class TileEntityModularDoor extends TileEntity implements ITickableTileEntity
{
	private int size = 32, prevSize=32;
	
	//client only
	private BlockState buffer = null;
	
	public TileEntityModularDoor() 
	{
		super(FPTileEntitys.MODULAR_DOOR);
	}
	
	public void open()
	{
		BlockState state = level.getBlockState(worldPosition);
		state = state.setValue(BlockModularDoor.OPEN, true);
		state = state.setValue(BlockModularDoor.COMPLETE, false);
		
		level.setBlock(worldPosition, state, 3);
	}
	
	public void close()
	{
		BlockState state = level.getBlockState(worldPosition);
		state = state.setValue(BlockModularDoor.OPEN, false);
		state = state.setValue(BlockModularDoor.COMPLETE, false);
		
		level.setBlock(worldPosition, state, 3);
	}
	
	public void invisible()
	{
		size = 0;
		setChanged();
	}
	
	public float getSize()
	{
		return size *0.5F;
	}
	
	public float getPrevSize()
	{
		return prevSize *0.5F;
	}

	@Override
	public void tick()
	{
		if(level.isClientSide)
		{
			BlockState state = level.getBlockState(worldPosition);
			if(buffer!=null)
			{
				state = buffer;
				level.setBlockAndUpdate(worldPosition, buffer);
				buffer = null;
			}
			if(!state.getValue(BlockModularDoor.COMPLETE))
			{
				update(state);
			}
		}
	}
	
	public void update(BlockState state)
	{
		prevSize=size;
		if(state.getValue(BlockModularDoor.OPEN))
		{
			if(size>1)
			{
				size--;
				level.getBlockTicks().scheduleTick(worldPosition, state.getBlock(), 1);
				return;
			}
		}
		else
		{
			if(size<32)
			{
				size++;
				level.getBlockTicks().scheduleTick(worldPosition, state.getBlock(), 1);
				return;
			}
		}

		onMovingFinished();
	}
	
	private void onMovingFinished()
	{
		BlockState state = level.getBlockState(worldPosition);
		state = state.setValue(BlockModularDoor.COMPLETE, true);
		level.setBlock(worldPosition, state, 3);
		
		((BlockModularDoor)MiscBlocks.modular_door).onMovingFinished(level, worldPosition, state);
	}
	
	@Override
	public CompoundNBT save(CompoundNBT compound)
	{
		super.save(compound);
		compound.putInt("size", size);
		return compound;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT compound)
	{
		super.load(state, compound);
		prevSize = size = compound.getInt("size");
	}
	
//	@Override
//	public boolean shouldRefresh(World w, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{
//		if(oldState.getBlock()==newSate.getBlock())
//		{
//			if(w.isRemote)
//			{
//				if(oldState.get(BlockModularDoor.OPEN) != newSate.get(BlockModularDoor.OPEN))
//				{
//					if(newSate.get(BlockModularDoor.COMPLETE))
//					{
//						buffer = newSate.with(BlockModularDoor.COMPLETE, false);
//					}
//				}
//			}
//			return false;
//		}
//		
//		return true;
//	}

	public boolean canRender()
	{
		BlockState state = level.getBlockState(worldPosition);
		if(state.getBlock()==Blocks.AIR)
			return false;
		return size>0 && (!state.getValue(BlockModularDoor.COMPLETE) || state.getValue(BlockModularDoor.OPEN));
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		SUpdateTileEntityPacket packet = new SUpdateTileEntityPacket(worldPosition, -1, save(new CompoundNBT()));
		return packet;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		load(getBlockState(), pkt.getTag());
	}
}
