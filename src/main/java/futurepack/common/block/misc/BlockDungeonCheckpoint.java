package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.dim.structures.TeleporterMap;
import futurepack.common.dim.structures.TeleporterMap.TeleporterEntry;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageChekpointSelect;
import futurepack.common.sync.MessageEScanner;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.ClickEvent.Action;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.PacketDistributor;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockDungeonCheckpoint extends BlockRotateableTile 
{
	private static final Supplier<double[][]> shape = () -> new double[][]{
		{1,0,1,15,4,15},
		{0,4,0,16,7,16}
	};
	public static final VoxelShape UP = HelperBoundingBoxes.createBlockShape(shape.get(), 0, 0);
	private final VoxelShape DOWN = HelperBoundingBoxes.createBlockShape(shape.get(), 180F, 0F);
	private final VoxelShape NORTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 0F);
	private final VoxelShape SOUTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 180F);
	private final VoxelShape WEST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 90F);
	private final VoxelShape EAST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 270F);
	
	
	public BlockDungeonCheckpoint(Properties props) 
	{
		super(props);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityDungeonCheckpoint();
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		Direction face = state.getValue(BlockRotateableTile.FACING);
		switch (face)
		{
		case NORTH:
			return NORTH;
		case SOUTH:
			return SOUTH;
		case WEST:	
			return WEST;
		case EAST:
			return EAST;
		case DOWN:
			return DOWN;
		case UP:
		default:
			return UP;
		}
	}
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit)
	{
		if(!worldIn.isClientSide)
		{
			onInteraction(worldIn, pos, player);
			
		}
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public void entityInside(BlockState state, World worldIn, BlockPos pos, Entity entityIn)
	{
		super.entityInside(state, worldIn, pos, entityIn);
		
		if(!worldIn.isClientSide && entityIn instanceof PlayerEntity)
		{
			//onInteraction(worldIn, pos,  (PlayerEntity) entityIn);
		}
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		Direction face = context.getClickedFace();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}
	
	public void onInteraction(World w, BlockPos pos, PlayerEntity pl)
	{
		if(!w.isClientSide)
		{
			TeleporterMap tmap = TeleporterMap.getTeleporterMap((ServerWorld) w);
			tmap.addTeleporter(pl, pos);
			
			List<TeleporterEntry> checkpoints = tmap.getTeleporter(pl);
			List<IFormattableTextComponent> list = new ArrayList<IFormattableTextComponent>(checkpoints.size() + 1);
			
			list.add(new StringTextComponent("You are at: "));
			list.add(new StringTextComponent("Teleport to: "));
			
			Style here = Style.EMPTY.withBold(true).withColor(TextFormatting.DARK_GRAY);

			//Non-destructive on-the-fly creation of a sorting key for compatibility with already existing checkpoints
			checkpoints.sort(Comparator.comparing(a ->
			a.name.substring(a.name.indexOf(167) + 2, a.name.length() - 3) +//Dungeon name
			String.format("%03d", Integer.parseInt(a.name.substring(6, a.name.indexOf(' ', 6)))) +//Floor
			a.name.substring(a.name.length() - 3) +//Tech level
			a.name.substring(8, 10)));//Dungeon name color
			for(TeleporterEntry e : checkpoints)
			{
				StringTextComponent name = new StringTextComponent(e.name);
				
				if(pos.equals(e.pos))
				{
					name.setStyle(here);
					list.get(0).append(name);
				}
				else
				{
					
					ClickEvent event = new ClickEvent(Action.CHANGE_PAGE, "dungeon_teleport=" + Long.toHexString(e.pos.asLong()));
					Style click = Style.EMPTY.setUnderlined(true).withColor(TextFormatting.DARK_BLUE).withClickEvent(event);
					name.setStyle(click);
					list.add(name);
				}
			}
			
			MessageEScanner msg = new MessageEScanner(true, list.toArray(new ITextComponent[list.size()]));
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)pl), msg);
		}
		
	}

	/**
	 * this gets called from ComponentInteractiveText
	 * @param value is the long of the teleporter pos as hex
	 */
	public static void onClientSelectTeleporter(String value, PlayerEntity player)
	{
		long l = Long.parseUnsignedLong(value, 16);
		MessageChekpointSelect sel = new MessageChekpointSelect(l);
		FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(sel);
		Minecraft.getInstance().setScreen(null);
	}
}
