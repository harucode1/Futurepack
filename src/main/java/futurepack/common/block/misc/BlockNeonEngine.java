package futurepack.common.block.misc;

import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;

public class BlockNeonEngine extends BlockRotateableTile
{
	private static final VoxelShape shape_up = VoxelShapes.or(Block.box(0, 0+1, 0, 16, 16-5, 16), Block.box(4, 0, 4, 12, 16, 12));
	private static final VoxelShape shape_down = VoxelShapes.or(Block.box(0, 5, 0, 16, 15, 16), Block.box(4, 0, 4, 12, 16, 12));
	private static final VoxelShape shape_east = VoxelShapes.or(Block.box(1, 0, 0, 11, 16, 16), Block.box(0, 4, 4, 16, 12, 12));
	private static final VoxelShape shape_west = VoxelShapes.or(Block.box(5, 0, 0, 15, 16, 16), Block.box(0, 4, 4, 16, 12, 12));
	private static final VoxelShape shape_north = VoxelShapes.or(Block.box(0, 0, 5, 16, 16, 15), Block.box(4, 4, 0, 12, 12, 16));
	private static final VoxelShape shape_south = VoxelShapes.or(Block.box(0, 0, 1, 16, 16, 11), Block.box(4, 4, 0, 12, 12, 16));
	
	protected BlockNeonEngine(Block.Properties props) 
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);		
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		switch (state.getValue(BlockRotateableTile.FACING)) 
		{
		case UP:
			return shape_up;
		case DOWN:
			return shape_down;
		case EAST:
			return shape_east;
		case WEST:
			return shape_west;
		case NORTH:
			return shape_north;
		case SOUTH:
			return shape_south;
		default:
			return super.getShape(state, worldIn, pos, sel);
		}
		
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
	{
		return BlockRenderType.ENTITYBLOCK_ANIMATED;
	}

//	@Override
//	public void onBlockPlacedBy(World w, BlockPos pos, IBlockState state, EntityLivingBase liv, ItemStack it) 
//	{
//		int o = EnumFacing.getDirectionFromEntityLiving(pos, liv).getIndex();
//		w.setBlockState(pos, state.withProperty(FPBlocks.META(mm), o), 2);
//		super.onBlockPlacedBy(w, pos, state, liv, it);
//	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityNeonEngine();
	}
}
