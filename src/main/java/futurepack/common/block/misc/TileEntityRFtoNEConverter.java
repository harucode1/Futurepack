package futurepack.common.block.misc;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPPotions;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.block.BlockRotateable;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperEntities;
import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.LightningBoltEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.Path;
import net.minecraft.potion.EffectInstance;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class TileEntityRFtoNEConverter extends TileEntity implements ITickableTileEntity, ITilePropertyStorage
{
	public static final int PROPERTY_CONV = 1;
	public static final int PROPERTY_WORKING = 2;
	private CapabilityNeon power = new CapabilityNeon(10000, EnumEnergyMode.PRODUCE);
	private EnumConversation conv = EnumConversation.S5;
	private boolean working = false;
	
	private List<WeakReference<CreeperEntity>> creeper = null;
	
	public TileEntityRFtoNEConverter() 
	{
		super(FPTileEntitys.RF2NE_CONVERTER);
	}
	
	// 4RF = 1NE
	
	@Override
	public void tick()
	{
		if(working && conv.rate>=100 && !level.isClientSide)
		{
			if(level.random.nextInt(10) == 0)
			{
				List<CreeperEntity> l = getActiveCreeper();
				for(CreeperEntity c : l)
				{
					if(HelperEntities.navigateEntity(c, getBlockPos().above(), false))
					{
						break;
					}
				}
			}
		}
		if(power.get() > 0)
		{
			HelperEnergyTransfer.powerLowestBlock(this);
		}
	}
	
	@Nullable
	private List<CreeperEntity> getActiveCreeper()
	{
		if(creeper == null || creeper.isEmpty())
		{
			AxisAlignedBB bb = new AxisAlignedBB(worldPosition.offset(-50, -50, -50), worldPosition.offset(50, 50, 50));
			Iterator<CreeperEntity> iter =level.getEntitiesOfClass(CreeperEntity.class, bb, new Predicate<CreeperEntity>()
			{
				@Override
				public boolean test(CreeperEntity c) 
				{
					if(c.isAlive() && !c.isPowered()) //isCharged = isPowered
					{
						return c.getNavigation().isDone();
					}
					return false;
				}
			}).iterator();
			creeper = new ArrayList<>(16);
			ArrayList<CreeperEntity> list = new ArrayList<>(16);
			while(iter.hasNext())
			{
				CreeperEntity c = iter.next();
				if(c!=null)
				{
					creeper.add(new WeakReference<CreeperEntity>(c));
					list.add(c);
				}
			}
			return list;
		}
		else
		{
			ArrayList<CreeperEntity> list = new ArrayList<>(creeper.size());
			Iterator<WeakReference<CreeperEntity>> iter = creeper.iterator();
			while(iter.hasNext())
			{
				WeakReference<CreeperEntity> ref = iter.next();
				CreeperEntity c = ref.get();
				if(ref.get()==null || ref.get().isAlive()==false || ref.get().isPowered())
					iter.remove();
				else
					list.add(c);
					
			}
			return list;
		}
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("energy", power.serializeNBT());
		nbt.putByte("speed", (byte)conv.ordinal());
		nbt.putBoolean("working", working);
		
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
		conv = EnumConversation.values()[nbt.getByte("speed")];
		working = nbt.getBoolean("working");
	}
	
	private LazyOptional<IEnergyStorage> optRF;
	private LazyOptional<INeonEnergyStorage> optNeon; 
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityEnergy.ENERGY && facing == getRFInput())
		{
			if(optRF!=null)
			{
				return (LazyOptional<T>) optRF;
			}
			else
			{
				optRF = LazyOptional.of(SaveableStorage::new);
				optRF.addListener(p -> optRF = null);
				return (LazyOptional<T>) optRF;
			}
		}			
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(optNeon!=null)
			{
				return (LazyOptional<T>) optNeon;
			}
			else
			{
				optNeon = LazyOptional.of(() -> power);
				optNeon.addListener(p -> optNeon = null);
				return (LazyOptional<T>) optNeon;
			}
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(optNeon, optRF);
		super.setRemoved();
	}
	
	
	private void doDamage(int transfer)
	{
		EnumConversation.getCoversationFromNeon(transfer).applyEffect(this);
	}
	
	public  EnumConversation getConv()
	{
		return conv;
	}
	
	public Direction getRFInput()
	{
		BlockState state = level.getBlockState(worldPosition);
		if(state.getBlock().isAir(state, level, worldPosition))
			return Direction.UP;
		
		return state.getValue(BlockRotateable.HORIZONTAL_FACING);
	}
	
	private class SaveableStorage implements IEnergyStorage
	{
		@Override
		public int receiveEnergy(int maxReceive, boolean simulate)
		{
			if(!working)
				return 0;
			
			maxReceive = Math.min(maxReceive, conv.rate*4);
			int ne = maxReceive /4;
			int space = power.getMax() - power.get();
			int transfer = Math.min(ne, space);
			
			if(transfer >= 100 && !simulate)
			{
				doDamage(transfer);
			}
			
			if(!simulate)
			{
				power.add(transfer);
			}
			
			return transfer *4;
		}

		@Override
		public int extractEnergy(int maxExtract, boolean simulate)
		{
			return 0;
		}

		@Override
		public int getEnergyStored()
		{
			return power.get() * 2;
		}

		@Override
		public int getMaxEnergyStored()
		{
			return power.getMax() * 2;
		}

		@Override
		public boolean canExtract()
		{
			return false;
		}

		@Override
		public boolean canReceive()
		{
			return working;
		}

		
	}

	@Override
	public int getProperty(int id)
	{
		switch(id)
		{
		case 0:
			return power.get();
		case PROPERTY_CONV: 
			return conv.ordinal();
		case PROPERTY_WORKING:
			return working ? 1 : 0;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:
			int d = value - power.get();
			if(d>0)
				power.add(d);
			else
				power.use(-d);
			break;
		case PROPERTY_CONV: 
			conv = EnumConversation.values()[value];
			break;
		case PROPERTY_WORKING:
			working = value == 1;
			break;
		default:
			break;
		}
	}


	@Override
	public int getPropertyCount()
	{
		return 3;
	}
	
	public static enum EnumConversation
	{
		S1(10),
		S2(25),
		S3(50),
		S4(75),
		S5(100),//Charged Creeper r=8
		S6(250),//Paralized, r=16
		S7(500),//Instand Damage, r=16
		S8(750),//Blitze, r=24
		S9(1000);//explode, mit block damage, 
		
		private final int rate;
		public final int xPos, yPos;
		
		EnumConversation(int rate)
		{
			this.rate = rate;
			xPos = 122 + (ordinal()/3) * 41;
			yPos = 0 + (ordinal()%3) * 21;
		}
		
		public static EnumConversation getCoversationFromNeon(int transfer)
		{
			EnumConversation[] ec = EnumConversation.values();
			int i=0;
			while(ec[i].rate < transfer)
				i++;
			
			return ec[i];
		}
		
		public void applyEffect(TileEntityRFtoNEConverter tile)
		{
			List<LivingEntity> list2 = null;
			List<CreeperEntity> list1;
			switch(this)
			{
			case S9:
				if(tile.level.random.nextInt(10 * 60 * 20) == 0)
				{
					tile.level.explode(null, tile.worldPosition.getX(), tile.worldPosition.getY(), tile.worldPosition.getZ(), 16F, true, Mode.BREAK);
					return;
				}
			case S8:
				list2 = tile.level.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(tile.worldPosition.offset(-24, -24, -24), tile.worldPosition.offset(24, 24, 24)));
				list2.forEach(e -> {
					if(e.level.random.nextInt(40)==0)
					{
						LightningBoltEntity bolt = EntityType.LIGHTNING_BOLT.create(e.level);
						bolt.setPos(e.getX(), e.getY(), e.getZ());
						bolt.setVisualOnly(false);
						e.level.addFreshEntity(bolt);
					}
						
				});
			case S7:
				if(list2==null)
					list2 = tile.level.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(tile.worldPosition.offset(-16, -16, -16), tile.worldPosition.offset(16, 16, 16)));
				list2.forEach(e -> {
					if(e.level.random.nextInt(20)==0)
						e.hurt(FuturepackMain.NEON_DAMAGE, 1);
				});
			case S6:
				if(list2==null)
					list2 = tile.level.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(tile.worldPosition.offset(-16, -16, -16), tile.worldPosition.offset(16, 16, 16)));
				list2.forEach(e -> {
					if(e.level.random.nextInt(10)==0)
						e.addEffect(new EffectInstance(FPPotions.paralyze, 20*2));
				});	
			case S5:
				list1 = tile.level.getEntitiesOfClass(CreeperEntity.class, new AxisAlignedBB(tile.worldPosition.offset(-8, -8, -8), tile.worldPosition.offset(8, 8, 8)), e -> {return !e.isPowered();});
				list1.forEach(e -> {
					CompoundNBT nbt = new CompoundNBT();
					e.addAdditionalSaveData(nbt);
					nbt.putBoolean("powered", true);
					e.readAdditionalSaveData(nbt);
				});
			default:
				break;
			}
		}
		
		public int getLimit()
		{
			return rate;
		}
	}

	public boolean isWorking()
	{
		return working;
	}
}
