package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperMagnetism;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;

public class TileEntityMagnet extends TileEntity implements ITickableTileEntity
{
	int pause = 0;
	
	
	public TileEntityMagnet() 
	{
		super(FPTileEntitys.MAGNET);
	}

	
	@Override
	public void tick() 
	{
		
		pause--;
		
		if(pause>0)
			return;
		HelperMagnetism.doMagnetism(level, worldPosition);
		pause=1;
	}

}
