package futurepack.common.block.misc;

import futurepack.common.block.BlockHoldingTile;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockStructureFixHelper extends BlockHoldingTile 
{

	public BlockStructureFixHelper(Properties builder) 
	{
		super(builder);
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) 
	{
		return new TileEntityStructureFixHelper();
	}

}
