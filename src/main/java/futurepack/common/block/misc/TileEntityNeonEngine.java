package futurepack.common.block.misc;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class TileEntityNeonEngine extends TileEntity implements ITickableTileEntity
{

	public boolean working = false;
	private CapabilityNeon power = new CapabilityNeon(10000, EnumEnergyMode.USE);
	
	public EnumHeat heatState = EnumHeat.OFF;
	public EnumHeat lastState = null;
	private int heat;
	
	
	public TileEntityNeonEngine() 
	{
		super(FPTileEntitys.NEON_ENGINE);
	}
	
	private LazyOptional<INeonEnergyStorage> neon_opt;
	private LazyOptional<IEnergyStorage> rf_opt;
	
	@Override
	public void tick()
	{
		if(heat>0)
			heat--;
		
		working = level.getBestNeighborSignal(worldPosition) > 0;
		if((working||heat>0) && power.get()>=40)
		{
			if(rf_opt!=null)
			{
				rf_opt.ifPresent(storage -> {
					int used = storage.receiveEnergy(power.get()*2, false);
					power.use(used/2);
					if(heat<2000 && working)
						heat+=2;
				});
			}
			else
			{
				BlockState state = level.getBlockState(getBlockPos());
				Direction dir = state.getValue(BlockRotateableTile.FACING);
				BlockPos xyz = worldPosition.relative(dir);
				TileEntity tile = level.getBlockEntity(xyz);
				if(tile!=null)
				{
					rf_opt = tile.getCapability(CapabilityEnergy.ENERGY, dir.getOpposite());
					rf_opt.addListener(p -> rf_opt = null);
				}
			}
		}
		if(heat>1900)
			heatState =EnumHeat.HOT;
		else if(heat>1200)
			heatState = EnumHeat.WARM;
		else if(heat>500)
			heatState = EnumHeat.MEDIUM;
		else if(heat>0)
			heatState = EnumHeat.COLD;
		else
			heatState = EnumHeat.OFF;
		
		if(lastState != heatState)
		{
			lastState = heatState;
			
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neon_opt!=null)
				return (LazyOptional<T>) neon_opt;
			else
			{
				neon_opt = LazyOptional.of(() -> power);
				neon_opt.addListener(p -> neon_opt = null);
				return (LazyOptional<T>) neon_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neon_opt);
		super.setRemoved();
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		CompoundNBT nbt = new CompoundNBT();
		save(nbt);
		SUpdateTileEntityPacket pack = new SUpdateTileEntityPacket(worldPosition, -1, nbt);
		return pack;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt)
	{
		if(level.isClientSide)
		{
			load(getBlockState(), pkt.getTag());
		}
	}
	
	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}

	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("energy", power.serializeNBT());
		nbt.putInt("heat", heat);
		return nbt;
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
		heat = nbt.getInt("heat");
	}

//	@Optional.Method(modid = "CoFHCore")
//	@Override
//	public int receiveEnergy(EnumFacing paramForgeDirection, int paramInt, boolean paramBoolean)
//	{
//		return 0;
//	}
	
	public enum EnumHeat
	{
		OFF(0F, 0, "futurepack:textures/model/engine_2_schwarz.png"),
		COLD(0.001F, 100, "futurepack:textures/model/engine_2.png"),
		MEDIUM(0.002F, 250,"futurepack:textures/model/engine_2_grun.png"),
		WARM(0.003F, 500, "futurepack:textures/model/engine_2_orange.png"),
		HOT(0.004F, 1000, "futurepack:textures/model/engine_2_rot.png");
		
		private float speed;
		private int maxRF;
		private ResourceLocation tex;
		
		EnumHeat(float speed, int maxRF, String root)
		{
			this.speed = speed;
			this.maxRF = maxRF;
			tex = new ResourceLocation(root);
		}
		
		public ResourceLocation getTexture()
		{
			return tex;
		}
		
		public float getSpeed()
		{
			return speed;
		}
		
		public int getMaxRF()
		{
			return maxRF;
		}
	}
}
