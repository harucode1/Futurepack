package futurepack.common.block.misc;

import java.util.function.Consumer;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileAntenne;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import futurepack.common.FPTileEntitys;
import futurepack.common.network.EventWirelessFunk;
import futurepack.common.network.NetworkManager;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityAntenna extends TileEntity implements ITileAntenne, INetworkUser, ITilePropertyStorage
{
	private int frequenz = 0;
	private int lastRange = -1;
	
	private final Consumer<EventWirelessFunk> method; 
	
	public TileEntityAntenna()
	{
		super(FPTileEntitys.ANTENNA);
		method = this::onFunkEvent;
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		if(hasLevel())
		{
			NetworkManager.registerWirelessTile(method, getLevel());
		}
	}
	
	@Override
	public void onChunkUnloaded()
	{
		NetworkManager.unregisterWirelessTile(method, getLevel());
		super.onChunkUnloaded();
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		nbt.putInt("frequenz", frequenz);
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		frequenz = nbt.getInt("frequenz");
		super.load(state, nbt);
	}
	
	public void onFunkEvent(EventWirelessFunk event)
	{
		if(!level.isClientSide)
		{	
			if(this.isRemoved())
			{
				NetworkManager.unregisterWirelessTile(method, getLevel());
				return;
			}
			if(!level.hasChunkAt(worldPosition))
			{
				NetworkManager.unregisterWirelessTile(method, getLevel());
				return;
			}
			
			if(event.canRecive(this))
			{
				if(event.frequenz == this.frequenz)
				{
					float dis = (float) Math.sqrt(event.source.distSqr(getSenderPosition()));
					if(dis < event.range + this.getRange())
					{
						NetworkManager.sendPacketUnthreaded(this, event.objPassed);
					}
				}		
			}
		}
		else
		{
			NetworkManager.unregisterWirelessTile(method, getLevel());
		}
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public int getRange()
	{
		if(level.isClientSide)
			return lastRange;
		else
		{
			FPBlockSelector sp = FPSelectorHelper.getSelector(level, getSenderPosition(), FPBlockSelector.base);//no TileEntites, so can be off thread
			return lastRange = sp.getBlocks(getSenderPosition(), Material.METAL);
		}
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		NetworkManager.sendEvent(new EventWirelessFunk(getSenderPosition(), frequenz, getRange(), pkt), level);
	}

	@Override
	public void postPacketSend(PacketBase pkt)
	{
		
	}

	@Override
	public World getSenderWorld()
	{
		return this.level;
	}
	
	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}

	@Override
	public int getProperty(int id)
	{
		switch (id) 
		{
		case 0:
			return frequenz;
		case 1:
			if(lastRange==0)
				return getRange();
			return lastRange;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			frequenz = value;
			break;
		case 1:
			lastRange = value;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 2;
	}

	public int getFrequenz()
	{
		return frequenz;
	}

	public void setFrequenz(int integer)
	{
		frequenz = integer;
	}
}
