package futurepack.common.block.misc;

import java.util.Random;

import futurepack.api.FacingUtil;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Direction.AxisDirection;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class BlockModularDoor extends BlockRotateableTile
{
	public static final BooleanProperty OPEN = BooleanProperty.create("open");
	public static final BooleanProperty COMPLETE = BooleanProperty.create("complete");
	
	public BlockModularDoor(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		this.setDefaultState(this.blockState.getBaseState().withProperty(OPEN, false).withProperty(COMPLETE, true));
//		setCreativeTab(FPMain.tab_deco);
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//	{
//		return this.getDefaultState().with(FACING, EnumFacing.byIndex(meta%6)).withProperty(OPEN, meta>=6).withProperty(COMPLETE, false);
//	}
//	
//	@Override
//	public int getMetaFromState(IBlockState state)
//	{
//		return (state.get(OPEN)? 6 : 0) + state.get(FACING).getIndex();
//	}
//	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(OPEN, COMPLETE);
	}
	
	VoxelShape[][] cache = new VoxelShape[6][32];
		
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader w, BlockPos pos, ISelectionContext sel)
	{
		if(state.getValue(COMPLETE) && !state.getValue(OPEN))
		{
			return VoxelShapes.block();
		}
		TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
		if(door!=null)
		{
			if(door.getSize()==0)
			{
				return VoxelShapes.empty();
			}
			else
			{
				int i1 = state.getValue(FACING).get3DDataValue();
				int i2 = (int) (door.getSize() * 2F)-1;
				if(cache[i1][i2]==null)
				{
					float x1=0F,y1=0F,z1=0F,x2=1F,y2=1F,z2=1F;
					Direction face = state.getValue(FACING);
					float h = Math.max(0.01F, 1F - door.getSize()/16F);
					
					if(face.getAxisDirection()==AxisDirection.POSITIVE)
					{
						x2 -= h * face.getStepX();
						y2 -= h * face.getStepY();
						z2 -= h * face.getStepZ();
					}
					else
					{
						x1 -= h * face.getStepX();
						y1 -= h * face.getStepY();
						z1 -= h * face.getStepZ();
					}
					cache[i1][i2] = VoxelShapes.box(x1,y1,z1,x2,y2,z2);
				}
//				List<EntityLivingBase> list = source.getEntitiesWithinAABB(EntityLivingBase.class, cache[i1][i2].offset(pos));
//				if(list.size()>0)
//				{
//					EnumFacing face = state.get(FACING);
//					float x = 0.01F * face.getXOffset();
//					float y = 0.015F * face.getYOffset();
//					float z = 0.01F * face.getZOffset();
//					
//					for(EntityLivingBase base : list)
//					{
//						base.getMotion().x += x;
//						base.getMotion().y += y;
//						base.getMotion().z += z;
//					}
//				}
				return cache[i1][i2];
			}
		}
		
		return VoxelShapes.block();
	}
	
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity playerIn, Hand hand, BlockRayTraceResult hit)
	{
		if(w.isClientSide)
			return ActionResultType.SUCCESS;
			
		BlockPos pointer = searchForMovingBlock(w, pos, state);
		BlockState pointerVal = w.getBlockState(pointer);
		 	
		if(pointerVal.getValue(OPEN))
		{
			startClosing(w, pointer, pointerVal);
			pointerVal = w.getBlockState(pointer);
			
			BlockPos other = getOpposite(w, pointer, pointerVal);
			if(other!=null)
			{
				BlockState otherVal = w.getBlockState(other);
				startClosing(w, other, otherVal);
			}
		}
		else
		{
			startOpening(w, pointer, pointerVal);
			pointerVal = w.getBlockState(pointer);
			
			BlockPos other = getOpposite(w, pointer, pointerVal);
			if(other!=null)
			{
				BlockState otherVal = w.getBlockState(other);
				startOpening(w, other, otherVal);
			}
		}
		return ActionResultType.SUCCESS;
	}
	
	/**
	 * This searches for the block that is actualy moving. (to stop,start it)
	 */
	private BlockPos searchForMovingBlock(World w, BlockPos pos, BlockState state)
	{
		Direction face = state.getValue(FACING);
		if(state.getValue(OPEN))
		{
			face = face.getOpposite();
		}
		
		BlockPos pointer = pos;
		BlockState pointerVal = state;
		
		while(pointerVal.getValue(COMPLETE))
		{	
			pointerVal = w.getBlockState(pointer.relative(face));
			if(pointerVal.getBlock()!=this || pointerVal.getValue(FACING)!=state.getValue(FACING))
			{
				pointerVal = w.getBlockState(pointer);
				break;
			}
			pointer = pointer.relative(face);
		}
		return pointer;
	}
	
	private BlockPos getOpposite(World w, BlockPos pos, BlockState state)
	{
		Direction face = state.getValue(FACING);
		BlockPos pointer = pos;
		BlockState pointerVal = state;
		
		while(pointerVal.getBlock() == this && pointerVal.getValue(FACING)==face)
		{
			pointer = pointer.relative(face);
			pointerVal = w.getBlockState(pointer);
		}
		
		if(pointerVal.getBlock()==this && pointerVal.getValue(FACING)==face.getOpposite())
		{
			return searchForMovingBlock(w, pointer, pointerVal);
		}
		
		return null;
	}
	
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random random)
	{
		TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
		door.update(state);
	}
	
	public void onMovingFinished(World w, BlockPos pos, BlockState state)
	{
		if(state.getValue(OPEN))
		{
			BlockPos face = pos.relative(state.getValue(FACING), -1);
			BlockState holder = w.getBlockState(face);
			if(holder.getBlock()==this)
			{
				startOpening(w, face, holder);
				makeInvisible(w, pos, state);
			}
		}
		else
		{
			BlockPos face = pos.relative(state.getValue(FACING), 1);
			BlockState holder = w.getBlockState(face);
			if(holder.getBlock()==this && holder.getValue(FACING)==state.getValue(FACING))
			{
				startClosing(w, face, holder);
			}
		}
	}
	
	public void startOpening(World w, BlockPos pos, BlockState state)
	{
		if(state.getValue(OPEN)==false)
		{
			TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
			door.open();
			w.getBlockTicks().scheduleTick(pos, this, 1);
			
			tryTriggerNeighbors(w, pos, state, true);		
		}
	}
	
	public void startClosing(World w, BlockPos pos, BlockState state)
	{
		if(state.getValue(OPEN)==true)
		{
			TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
			door.close();
			w.getBlockTicks().scheduleTick(pos, this, 1);
			
			tryTriggerNeighbors(w, pos, state, false);
		}
	}
	
	public void tryTriggerNeighbors(World w, BlockPos pos, BlockState state, boolean open)
	{
		Direction.Axis axis = state.getValue(FACING).getAxis();
		for(Direction faces : FacingUtil.VALUES)
		{
			if(faces.getAxis()!=axis)
			{
				BlockPos nextPos = pos.relative(faces);
				BlockState other = w.getBlockState(nextPos);
				if(other.getBlock() == this)
				{
					if(other.getValue(OPEN)!=open)
					{
						if(open)
						{
							startOpening(w, nextPos, other);
						}
						else
						{
							startClosing(w, nextPos, other);
						}
					}
				}
			}
		}
		
	}
	
	public void makeInvisible(World w, BlockPos pos, BlockState state)
	{
		TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
		door.invisible();
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
	{
		return state.getValue(COMPLETE)&&!state.getValue(OPEN) ? BlockRenderType.MODEL : BlockRenderType.INVISIBLE;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityModularDoor();
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		Direction face = context.getNearestLookingDirection().getOpposite();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}
}
