package futurepack.common.block.misc;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.LeverBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockFpLever extends LeverBlock
{
    protected static final VoxelShape FPLEVER_NORTH_AABB = VoxelShapes.box(0.0625D, 0.0625D, 0.75D, 0.9375D, 0.9375D, 1.0D);
    protected static final VoxelShape FPLEVER_SOUTH_AABB = VoxelShapes.box(0.0625D, 0.0625D, 0.0D, 0.9375D, 0.9375D, 0.25D);
    protected static final VoxelShape FPLEVER_WEST_AABB = VoxelShapes.box(0.75D, 0.0625D, 0.0625D, 1.0D, 0.9375D, 0.9375D);
    protected static final VoxelShape FPLEVER_EAST_AABB = VoxelShapes.box(0.0D, 0.0625D, 0.0625D, 0.25D, 0.9375D, 0.9375D);
    
    protected static final VoxelShape FPLEVER_UP_AABB = VoxelShapes.box(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.25D, 0.9375D);
    protected static final VoxelShape FPLEVER_DOWN_AABB = VoxelShapes.box(0.0625D, 0.75D, 0.0625D, 1.0D, 0.9375D, 0.9375D);

	
	public BlockFpLever(Block.Properties props)
	{
		super(props);
//		setCreativeTab(ItemGroup.REDSTONE);
//		setSoundType(SoundType.WOOD);
	}
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit)
	{
	      state = state.cycle(POWERED);
	      boolean flag = state.getValue(POWERED);
	      if (worldIn.isClientSide) 
	      {
	         return ActionResultType.SUCCESS;
	      } 
	      else 
	      {
	         worldIn.setBlock(pos, state, 3);
	         
	         if(flag)
	        	 worldIn.playSound((PlayerEntity)null, pos, SoundEvents.FENCE_GATE_OPEN, SoundCategory.BLOCKS, 0.3F, 1.5F);
	         else
	        	 worldIn.playSound((PlayerEntity)null, pos, SoundEvents.FENCE_GATE_CLOSE, SoundCategory.BLOCKS, 0.3F, 1.5F);
	         worldIn.updateNeighborsAt(pos, this);
	         worldIn.updateNeighborsAt(pos.relative(getConnectedDirection(state).getOpposite()), this);
	         return ActionResultType.SUCCESS;
	      }
	   }
	
//    public AxisAlignedBB getBoundingBox(IBlockState state, IWorldReader source, BlockPos pos)
//    {
//        switch ((BlockLever.EnumOrientation)state.getValue(FACING))
//        {
//            case EAST:
//            default:
//                return FPLEVER_EAST_AABB;
//            case WEST:
//                return FPLEVER_WEST_AABB;
//            case SOUTH:
//                return FPLEVER_SOUTH_AABB;
//            case NORTH:
//                return FPLEVER_NORTH_AABB;
//            case UP_Z:
//            case UP_X:
//                return FPLEVER_UP_AABB;
//            case DOWN_X:
//            case DOWN_Z:
//                return FPLEVER_DOWN_AABB;
//        }
//    }
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
	      switch(state.getValue(FACE)) {
	      case FLOOR:
	         switch(state.getValue(FACING).getAxis()) {
	         case X:
	         case Z:
	         default:
	            return FPLEVER_UP_AABB;
	         }
	      case WALL:
	         switch(state.getValue(FACING)) {
	         case EAST:
	            return FPLEVER_EAST_AABB;
	         case WEST:
	            return FPLEVER_WEST_AABB;
	         case SOUTH:
	            return FPLEVER_SOUTH_AABB;
	         case NORTH:
	         default:
	            return FPLEVER_NORTH_AABB;
	         }
	      case CEILING:
	      default:
	         switch(state.getValue(FACING).getAxis()) {
	         case X:
	         case Z:
	         default:
	            return FPLEVER_DOWN_AABB;
	         }
	      }
	   }
    
}
