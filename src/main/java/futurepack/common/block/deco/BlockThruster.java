package futurepack.common.block.deco;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.DirectionalBlock;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class BlockThruster extends DirectionalBlock
{
//	IIcon[][] cols = new IIcon[3][5];	
//	IIcon[] off = new IIcon[3];
//	private static final IntegerProperty backcol = IntegerProperty.create("bg", 0, 2);
//	private static final IntegerProperty forecol = IntegerProperty.create("fg", 0, 4);
	
	public static final BooleanProperty powered = BlockStateProperties.POWERED;
	public static final DirectionProperty FACING = DirectionalBlock.FACING;
	
	int[] cols = new int[]{0x47fafa, 0xf9d647, 0x31fc22, 0xf28b06, 0x8943d7};
	
	public BlockThruster(Block.Properties props)
	{
		super(props);
		registerDefaultState(this.stateDefinition.any().setValue(powered, false));
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(FACING, powered);
		super.createBlockStateDefinition(builder);
	}
	
	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) 
	{
		boolean bb = w.getBestNeighborSignal(pos) > 0;
		w.setBlock(pos, state.setValue(powered, bb), 3);
	}
	
	@Override
	public void animateTick(BlockState state, World w, BlockPos pos, Random rand)
	{
		if(state.getValue(powered))
		{
			Direction f = state.getValue(FACING);
			IParticleData red = ParticleTypes.SMOKE;
			w.addParticle(red, pos.getX()+w.random.nextFloat(), pos.getY()+w.random.nextFloat(), pos.getZ()+w.random.nextFloat(), f.getStepX()/2D*w.random.nextFloat(),f.getStepY()/2D*w.random.nextFloat(),f.getStepZ()/2D*w.random.nextFloat());
	        w.addParticle(red, pos.getX()+w.random.nextFloat(), pos.getY()+w.random.nextFloat(), pos.getZ()+w.random.nextFloat(), f.getStepX()/2D*w.random.nextFloat(),f.getStepY()/2D*w.random.nextFloat(),f.getStepZ()/2D*w.random.nextFloat());
		
		}
		super.animateTick(state, w, pos, rand);
	}
	
	@Override
	public BlockState rotate(BlockState state, IWorld world, BlockPos pos, Rotation direction)
	{
		return rotate(state, direction);
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn)
	{
		return state.setValue(FACING, mirrorIn.mirror(state.getValue(FACING)));
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		Direction face = context.getNearestLookingDirection().getOpposite();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}
}
