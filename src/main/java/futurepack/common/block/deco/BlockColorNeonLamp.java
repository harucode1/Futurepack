package futurepack.common.block.deco;

import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockColorNeonLamp extends RedstoneLampBlock
{

	public BlockColorNeonLamp(Properties properties)
	{
		super(properties);
	}

	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityNeonLamp();
	}
	
	@Override
	@Nullable
	public BlockState getStateForPlacement(BlockItemUseContext context)
	{
		return this.defaultBlockState().setValue(LIT, false);
	}

	public static boolean isPowered(World w, BlockPos pos)
	{
		if(w.hasNeighborSignal(pos))
		{
			TileEntityNeonLamp tile = (TileEntityNeonLamp) w.getBlockEntity(pos);
			return tile.isPowered();
		}
		return false;
	}
	
	@Override
	public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving)
	{
		if (!worldIn.isClientSide) 
		{
			boolean flag = state.getValue(LIT);
			if (flag != isPowered(worldIn, pos)) 
			{
				if (flag) 
				{
					worldIn.getBlockTicks().scheduleTick(pos, this, 4);
				}
				else 
				{
					worldIn.setBlock(pos, state.cycle(LIT), 2);
				}
			}

		}
	}
	
	@Override
	public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random)
	{
		if (!worldIn.isClientSide) 
		{
			if (state.getValue(LIT) && !isPowered(worldIn, pos)) 
			{
				worldIn.setBlock(pos, state.cycle(LIT), 2);
			}
		}
	}
}
