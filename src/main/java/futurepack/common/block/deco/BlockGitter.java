package futurepack.common.block.deco;

import net.minecraft.block.BlockState;
import net.minecraft.block.PaneBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockGitter extends PaneBlock {

	public BlockGitter(Properties builder) 
	{
		super(builder);
	}

	@Override
	public boolean useShapeForLightOcclusion(BlockState state) {
		return true;
	}
	
	@Override
	public boolean propagatesSkylightDown(BlockState state, IBlockReader reader, BlockPos pos) {
		return true;
	}
	
}
