package futurepack.common.block.deco;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.DoorBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;

public class BlockCompositeDoor extends DoorBlock
{

	public BlockCompositeDoor(Block.Properties props)
	{
		super(props);
	}
	
	private int getCloseSound()
    {
        return 1011;//metal door
    }

    private int getOpenSound()
    {
        return 1005;//metal door
    }
	
	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit)
    {
		state = state.cycle(OPEN);
        worldIn.setBlock(pos, state, 10);
        worldIn.levelEvent(player, state.getValue(OPEN) ? this.getOpenSound() : this.getCloseSound(), pos, 0);
        return ActionResultType.SUCCESS;
    }
	
}
