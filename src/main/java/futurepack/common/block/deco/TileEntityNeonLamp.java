package futurepack.common.block.deco;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.TileEntityLinkedLight;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.HuskEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityNeonLamp extends TileEntity implements ITickableTileEntity
{

	public TileEntityNeonLamp() 
	{
		super(FPTileEntitys.NEON_LAMP);
	}

	public CapabilityNeon power = new CapabilityNeon(100, EnumEnergyMode.USE);
	public LazyOptional<CapabilityNeon> opt;
	private int cooldown = 0;
	private Set<BlockPos> blockSet;
	private Iterator<BlockPos> blockLips;
	private boolean allLightsPlaced = false;
	private int checkCooldown = 0;
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{
		super.save(nbt);
		nbt.put("energy", power.serializeNBT());
		return nbt;
	} 
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		super.load(state, nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(opt==null)
			{
				opt = LazyOptional.of(()->power);
				opt.addListener(p -> opt = null);
			}
			return (LazyOptional<T>) opt;
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(opt);
		
		super.setRemoved();
	}
	
	@Override
	public void tick() 
	{
		if(!level.isClientSide)
		{
			if(cooldown>0)
				cooldown--;
			else
			{
				cooldown = allLightsPlaced ? 20 : 3;
			
				BlockState state = getBlockState();
				if(state.hasProperty(RedstoneLampBlock.LIT))
				{
					state.tick((ServerWorld)level, worldPosition, level.random);
					
					if(state.getValue(RedstoneLampBlock.LIT))
					{	
						burnZombies(level, worldPosition, 10);
						placeAdditionalLights();
					}
					else
					{
						blockLips = null;
						allLightsPlaced = false;
						
						if(power.get() >= power.getMax())
						{
							if(level.hasNeighborSignal(worldPosition))
							{
								getBlockState().neighborChanged(level, worldPosition, Blocks.AIR, worldPosition.above(), false);
							}
						}
					}
				}
			}
		}
	}
	
	public static void burnZombies(World w, BlockPos start, int range)
	{
		List<LivingEntity> list = w.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(start.offset(-range, -range, -range), start.offset(1+range,1+range,1+range)), new Predicate<LivingEntity>()
		{

			@Override
			public boolean test(LivingEntity t)
			{
				if(t.isInvertedHealAndHarm())
				{
					if(t instanceof HuskEntity)
					{
						return false; // ideal check if the should brun in day, but method is private so for now I will use this...
					}
					return true;	
				}
				return false;
			}
		});
		list.forEach(e -> e.setSecondsOnFire(20));
	}

	public boolean isPowered() 
	{		
		return cooldown==0 ? power.use(1)>0 : power.get() > 0;
	}
	
	final IBlockValidator validatorAllAir = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(World w, ParentCoords pos) 
		{
			return w.getBlockState(pos).getMaterial() == Material.AIR;
		}
	};
	
	final IBlockValidator validatorBlockAir = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(World w, ParentCoords pos) 
		{
			return w.getBlockState(pos).getBlock() == Blocks.AIR;
		}
	};
	
	
	
	public void placeAdditionalLights()
	{
		if(blockLips == null)
		{
			LightSelector selector = new LightSelector(getBlockPos());
			FPBlockSelector bs = new FPBlockSelector(level, selector);
			bs.selectBlocks(worldPosition);
			blockSet = new HashSet<>(bs.getValidBlocks(validatorAllAir));
			ArrayList<BlockPos> list = new ArrayList<>(bs.getValidBlocks(validatorBlockAir));
			Collections.shuffle(list);
			blockLips = list.iterator();
			bs.clear();
			if(list.isEmpty())
			{
				checkCooldown = 120;
			}
		}
		
		if(blockLips.hasNext())
		{
			BlockPos pos = blockLips.next();
			if(level.isEmptyBlock(pos))
			{
				if(level.setBlockAndUpdate(pos, MiscBlocks.linked_light.defaultBlockState()))
				{
					TileEntityLinkedLight light = (TileEntityLinkedLight) level.getBlockEntity(pos);
					light.setLinkedBlock(getBlockPos(), getBlockState());
					light.stillValid = this::isStillValid;
				}
				
			}
		}
		else if(checkCooldown > 0)
		{
			checkCooldown--;
		}
		else
		{
			allLightsPlaced = true;
			blockLips = null;
		}
	}
	
	public boolean isStillValid(TileEntityLinkedLight tile)
	{
		if(isPowered())
		{
			if(blockSet!=null)
			{
				return  blockSet.contains(tile.getBlockPos());
			}
		}
		return false;
	}
}
