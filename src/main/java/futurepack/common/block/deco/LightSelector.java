package futurepack.common.block.deco;

import java.util.HashMap;
import java.util.Map;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class LightSelector implements IBlockSelector
{
	private final BlockPos start;
	private Map<Vector3d, BlockPos> raytraceMap;
	private Map<BlockPos, Boolean> isValid;
	
	public LightSelector(BlockPos pos) 
	{
		start = pos;
		raytraceMap = new HashMap<>();
		isValid = new HashMap<>();
	}

	@Override
	public boolean isValidBlock(World w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent) 
	{
		if(diagonal)
			return false;
		if(isValidBase(w, pos))
		{
			return hasLineOfSightToStart(w, pos, diagonal);
		}
		return false;
	}
	
	@Override
	public boolean canContinue(World w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent) 
	{
		return start.distSqr(pos) < (15*15);
	}
	
	private boolean isValidBase(World w, BlockPos pos)
	{
		return isValid.computeIfAbsent(pos, p ->
		{
			BlockState state = w.getBlockState(p);
			boolean glassy = state.getLightBlock(w, p) < 10;
			return glassy;
		});
	}
	
	public boolean hasLineOfSightToStart(World w, BlockPos pos, boolean dia)
	{
		Vector3d vec = Vector3d.atLowerCornerOf(pos.subtract(start));
		BlockPos hit = getRayHit(vec, w);
		
		double disHit = start.distSqr(hit);
		double disBlock = start.distSqr(pos);
		
		return disBlock < disHit;
	}
	
	@SuppressWarnings("unused")
	private BlockPos getBlockHitBase(Vector3d vec, World w)
	{
		final boolean debug = false;
		
		final Vector3d start = new Vector3d(this.start.getX()+0.5, this.start.getY()+0.5, this.start.getZ()+0.5);
		Vector3d test = start;
		double dis = 0;
		BlockPos lastP = this.start, pos = this.start;
		while(dis <= 15*15)
		{
			test = test.add(vec);
			dis = start.distanceToSqr(test);
			lastP = pos;
			pos = new BlockPos(test);
			if(pos.equals(lastP))
				continue;
			
			if(!isValidBase(w, pos))
			{
				break;
			}
		}
		
		if(debug && !w.isClientSide)
		{
			((ServerWorld)w).sendParticles(ParticleTypes.BARRIER, test.x, test.y, test.z, 1, 0D, 0D, 0D, 0D);
		}
		return pos;
	}
	
	public BlockPos getRayHit(Vector3d vec, World w)
	{
		vec = vec.normalize().scale(0.5);
		BlockPos d = raytraceMap.get(vec);
		if(d == null)
		{
			d = getBlockHitBase(vec, w);
			raytraceMap.put(vec, d);
			return d;
		}
		else
		{
			return d;
		}
	}
	
	
}
