package futurepack.common.block.deco;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.StairsBlock;

public class BlockTreppe extends StairsBlock
{
	public BlockTreppe(BlockState state, Block.Properties build)
	{
		super(state, build);
	}
	
	public BlockTreppe(BlockState state)
	{
		this(state, Block.Properties.copy(state.getBlock()).noOcclusion());
	}
	
	public BlockTreppe(Block b)
	{
		this(b.defaultBlockState(), Block.Properties.copy(b).noOcclusion());
	}
}
