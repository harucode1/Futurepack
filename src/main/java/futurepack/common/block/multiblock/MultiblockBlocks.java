package futurepack.common.block.multiblock;

import futurepack.api.Constants;
import futurepack.common.FuturepackMain;
import futurepack.common.block.deco.DecoBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.DyeColor;
import net.minecraft.item.Item;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class MultiblockBlocks 
{
    public static final Block.Properties notSolid_white = Block.Properties.of(Material.METAL, DyeColor.byName("white", null)).strength(5F, 10F).sound(SoundType.METAL).harvestTool(ToolType.PICKAXE).noOcclusion();
    
	public static final Block ftl_drive = new BlockFTLMulti(DecoBlocks.default_light_gray).setRegistryName(Constants.MOD_ID, "ftl_drive");
	public static final Block deepcore_miner = new BlockDeepCoreMiner(notSolid_white).setRegistryName(Constants.MOD_ID, "deepcore_miner");
	
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> r = event.getRegistry();
		
		r.register(ftl_drive);
		r.register(deepcore_miner);
	}
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		r.register(item(ftl_drive));
		r.register(item(deepcore_miner));
	}
	
	private static final Item item(Block bl)
	{
		return new BlockItem(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(bl.getRegistryName());
	}
}
