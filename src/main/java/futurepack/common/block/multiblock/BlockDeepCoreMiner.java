package futurepack.common.block.multiblock;

import javax.annotation.Nullable;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.block.misc.TileEntityBedrockRift;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.recipes.multiblock.MultiblockPatterns;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperEntities;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.pattern.BlockPattern;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.CachedBlockInfo;
import net.minecraft.util.Direction;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.Hand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class BlockDeepCoreMiner extends BlockHoldingTile
{
	public static final EnumProperty<EnumDeepCoreMiner> variants = EnumProperty.create("variant", EnumDeepCoreMiner.class);

	public BlockDeepCoreMiner(Block.Properties props)
	{
		super(props);
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
    {
		EnumDeepCoreMiner v = state.getValue(variants);
        return v == EnumDeepCoreMiner.maschine ? BlockRenderType.MODEL : BlockRenderType.INVISIBLE;
    }

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		EnumDeepCoreMiner type = state.getValue(variants);
		if(type.isMain())
			return new TileEntityDeepCoreMinerMain();
		if(type.isInventory())
			return new TileEntityDeepCoreMinerInventory();
		return null;
	}

	@Override
	public boolean hasTileEntity(BlockState state) 
	{
		EnumDeepCoreMiner type = state.getValue(variants);
		return type.isMain() || type.isInventory();
	}
	
	@Override
	public ActionResultType use(BlockState state, World w, BlockPos pos, PlayerEntity pl, Hand hand, BlockRayTraceResult hit)
	{
		if(state.getValue(variants)==EnumDeepCoreMiner.maschine && !w.isClientSide)
		{
			ItemStack it = pl.getItemInHand(hand);
			if(it.getItem() == ToolItems.scrench)
			{
				BlockPattern pattern = MultiblockPatterns.getDeepMinerPattern();
				BlockPattern.PatternHelper helper = pattern.find(w, pos);
				if(helper!=null)
				{
					if(helper.getUp()== Direction.UP)
					{
						TileEntityDeepCoreMinerMain main = (TileEntityDeepCoreMinerMain) w.getBlockEntity(pos);
						main.storeUsedBlocks(helper);
						
						EnumDeepCoreMiner[][] multiblock = new EnumDeepCoreMiner[][]
								{
									{EnumDeepCoreMiner.Main, EnumDeepCoreMiner.MainUp1, EnumDeepCoreMiner.MainUp2},
									{EnumDeepCoreMiner.Middle1, EnumDeepCoreMiner.Middle2, EnumDeepCoreMiner.Middle3},
									{null, null, null}
								};
						
						if(helper.getForwards().getAxis()==Axis.Z)
						{
							multiblock[2][0] = EnumDeepCoreMiner.Inv1X;
							multiblock[2][1] = EnumDeepCoreMiner.Inv2X;
							multiblock[2][2] = EnumDeepCoreMiner.Inv3X;
						}
						else if(helper.getForwards().getAxis()==Axis.X) //yes this is the exat opposite
						{
							multiblock[2][0] = EnumDeepCoreMiner.Inv1Z;
							multiblock[2][1] = EnumDeepCoreMiner.Inv2Z;
							multiblock[2][2] = EnumDeepCoreMiner.Inv3Z;
						}
						
						for(int x=0;x < pattern.getWidth();x++)
						{
							for(int y=0;y < pattern.getHeight();y++)
							{
								CachedBlockInfo bws = helper.getBlock(x, y, 0);
								HelperEntities.disableItemSpawn();
								w.setBlockAndUpdate(bws.getPos(), this.defaultBlockState().setValue(variants, multiblock[x][2-y]));
								HelperEntities.enableItemSpawn();
							}
						}
					}
					else
					{
						pl.sendMessage(new TranslationTextComponent("fp.chat.error.activate.wrong.orientation"), Util.NIL_UUID);
					}
				}
				else
				{
					pl.sendMessage(new TranslationTextComponent("fp.chat.error.activate.wrong.pattern"), Util.NIL_UUID);
				}
			}
			else
			{
				pl.sendMessage(new TranslationTextComponent("fp.chat.error.activate.scrench.needed"), Util.NIL_UUID);
			}
			return ActionResultType.SUCCESS;
		}
		else if(state.getValue(variants).isInventory() && !w.isClientSide)
		{
			FPGuiHandler.SCROLLABLE_INVENTORY.openGui(pl, pos);
			return ActionResultType.SUCCESS;
		}
		else
		{
			BlockPos p = getMainBlock(w, pos, state);
			if(p!=null && !w.isClientSide)
			{
				if(HelperResearch.canOpen(pl, state))
				{
					FPGuiHandler.DEEPCORE_MINER.openGui(pl, p);
				}
				return ActionResultType.SUCCESS;
			}
			else if(w.isClientSide)
			{
				return ActionResultType.SUCCESS;
			}
		}
		
		return super.use(state, w, pos, pl, hand, hit);
	}
	
	private boolean alreadyRestoring = false;
	
	@Override
	public void onRemove(BlockState state, World w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.getBlock()!=this)
		{
			if(alreadyRestoring)
			{
				w.removeBlockEntity(pos);
				return;
			}
			TileEntity start = w.getBlockEntity(pos);
			if(state.getValue(variants) != EnumDeepCoreMiner.Main)
			{
				if(state.getValue(variants) == EnumDeepCoreMiner.maschine)
					super.onRemove(state, w, pos, newState, isMoving);
				else
					w.removeBlockEntity(pos);
			}
			alreadyRestoring = true;
			if(!isMultiBlockWorking(w, pos, state, true))
			{		
				System.out.println("BlockDeepCoreMiner.breakBlock()");
				//after isMultiBlockWorking blocks are already restored
				final TileEntity tile = w.getBlockEntity(pos);
				if(tile!=null && state.getValue(variants) == EnumDeepCoreMiner.Main)//only keep data of main block
				{ 
					if(w instanceof ServerWorld)
					{
						final BlockPos p = pos.immutable();
						Thread t = new Thread(new Runnable()
						{	
							@Override
							public void run()
							{
								try {
									Thread.sleep(50);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								((ServerWorld) w).getServer().submitAsync(new Runnable()
								{					
									@Override
									public void run()
									{
										TileEntity o = start;
										if(o==null)
											o = tile;
										
										o.clearRemoved();
										w.setBlockEntity(p, o);
										BlockState state = w.getBlockState(p);
										//w.notifyBlockUpdate(pos, Blocks.AIR.getDefaultState(), state, 3);
									}
								});
							}
						});
						t.setDaemon(true);
						t.start();					
					}
				}
			}
			else
			{
				super.onRemove(state, w, pos, newState, isMoving);
			}
			alreadyRestoring = false;
		}
	}
	
	@Nullable
	public static BlockPos getMainBlock(IWorldReader w, BlockPos pos, BlockState state)
	{
		EnumDeepCoreMiner type = state.getValue(variants);
		switch(type)
		{
		case Inv1X:
			if(isMainBlock(w, pos.offset(2, 0, 0)))
				return pos.offset(2, 0, 0);
			else if(isMainBlock(w, pos.offset(-2, 0, 0)))
				return pos.offset(-2, 0, 0);
			else
				return null;
		case Inv1Z:
			if(isMainBlock(w, pos.offset(0, 0, 2)))
				return pos.offset(0, 0, 2);
			else if(isMainBlock(w, pos.offset(0, 0, -2)))
				return pos.offset(0, 0, -2);
			else
				return null;
		case Inv2X:
			if(isMainBlock(w, pos.offset(2, -1, 0)))
				return pos.offset(2, -1, 0);
			else if(isMainBlock(w, pos.offset(-2, -1, 0)))
				return pos.offset(-2, -1, 0);
			else
				return null;
		case Inv2Z:
			if(isMainBlock(w, pos.offset(0, -1, 2)))
				return pos.offset(0, -1, 2);
			else if(isMainBlock(w, pos.offset(0, -1, -2)))
				return pos.offset(0, -1, -2);
			else
				return null;
		case Inv3X:
			if(isMainBlock(w, pos.offset(2, -2, 0)))
				return pos.offset(2, -2, 0);
			else if(isMainBlock(w, pos.offset(-2, -2, 0)))
				return pos.offset(-2, -2, 0);
			else
				return null;
		case Inv3Z:
			if(isMainBlock(w, pos.offset(0, -2, 2)))
				return pos.offset(0, -2, 2);
			else if(isMainBlock(w, pos.offset(0, -2, -2)))
				return pos.offset(0, -2, -2);
			else
				return null;
		case Main:
			return pos;
		case MainUp1:
			if(isMainBlock(w, pos.below()))
				return pos.below();
			return null;
		case MainUp2:
			if(isMainBlock(w, pos.below(2)))
				return pos.below(2);
			return null;
		case Middle1:
			if(isMainBlock(w, pos.offset(0, 0, 1)))
				return pos.offset(0, 0, 1);
			else if(isMainBlock(w, pos.offset(0, 0, -1)))
				return pos.offset(0, 0, -1);
			else if(isMainBlock(w, pos.offset(1, 0, 0)))
				return pos.offset(1, 0, 0);
			else if(isMainBlock(w, pos.offset(-1, 0, 0)))
				return pos.offset(-1, 0, 0);
			else
				return null;
		case Middle2:
			pos = pos.below();
			if(isMainBlock(w, pos.offset(0, 0, 1)))
				return pos.offset(0, 0, 1);
			else if(isMainBlock(w, pos.offset(0, 0, -1)))
				return pos.offset(0, 0, -1);
			else if(isMainBlock(w, pos.offset(1, 0, 0)))
				return pos.offset(1, 0, 0);
			else if(isMainBlock(w, pos.offset(-1, 0, 0)))
				return pos.offset(-1, 0, 0);
			else
				return null;
		case Middle3:
			pos = pos.below(2);
			if(isMainBlock(w, pos.offset(0, 0, 1)))
				return pos.offset(0, 0, 1);
			else if(isMainBlock(w, pos.offset(0, 0, -1)))
				return pos.offset(0, 0, -1);
			else if(isMainBlock(w, pos.offset(1, 0, 0)))
				return pos.offset(1, 0, 0);
			else if(isMainBlock(w, pos.offset(-1, 0, 0)))
				return pos.offset(-1, 0, 0);
			else
				return null;
		default:
			return null;		
		}
	}
	
	public static boolean isMultiBlockWorking(IWorldReader w, BlockPos pos, BlockState state, boolean restoreBlocks)
	{
		EnumDeepCoreMiner type = state.getValue(variants);
		if(type!=EnumDeepCoreMiner.maschine)
		{
			BlockPos p = getMainBlock(w, pos, state);
			if(p==null)
				return false;
			return checkFromMain(w, p, restoreBlocks);
		}
		else
		{
			return true;
		}
	}
	
	private static boolean isMainBlock(IWorldReader w, BlockPos main)
	{
		BlockState state = w.getBlockState(main);
		if(state.getBlock() == MultiblockBlocks.deepcore_miner)
		{
			if(state.getValue(variants) == EnumDeepCoreMiner.Main)
				return true;
			else if(state.getValue(variants) == EnumDeepCoreMiner.maschine)
			{
				TileEntityDeepCoreMinerMain tile = (TileEntityDeepCoreMinerMain) w.getBlockEntity(main);
				if(tile.getLogic()!=null)
				{
					if(tile.hasLevel())
					{
						tile.getLevel().setBlockAndUpdate(main, state.setValue(variants, EnumDeepCoreMiner.Main));
					}
					return true;
				}
			}
		}
		return false;
	}
	
	private static boolean checkFromMain(IWorldReader w, BlockPos pos, boolean restoreBlocks)
	{	
		TileEntityDeepCoreMinerMain tile = (TileEntityDeepCoreMinerMain) w.getBlockEntity(pos);
		if(tile==null)
			 return false;
		
		if(!isMainBlock(w, pos))
		{
			tile.restoreUsedBlocks();
			return false;
		}
		BlockState state;
		
		state = w.getBlockState(pos.above());
		if(state.getBlock()!=MultiblockBlocks.deepcore_miner || state.getValue(variants)!=EnumDeepCoreMiner.MainUp1)
		{
			tile.restoreUsedBlocks();
			return false;
		}		
		state = w.getBlockState(pos.above(2));
		if(state.getBlock()!=MultiblockBlocks.deepcore_miner || state.getValue(variants)!=EnumDeepCoreMiner.MainUp2)
		{	
			tile.restoreUsedBlocks();
			return false;
		}
		Direction face = tile.getFacing();
		if(face==null)
		{
			tile.restoreUsedBlocks();
			return false;
		}
		
		state = w.getBlockState(pos.relative(face));
		if(state.getBlock()!=MultiblockBlocks.deepcore_miner || state.getValue(variants)!=EnumDeepCoreMiner.Middle1)
		{
			tile.restoreUsedBlocks();
			return false;
		}
		state = w.getBlockState(pos.relative(face).above());
		if(state.getBlock()!=MultiblockBlocks.deepcore_miner || state.getValue(variants)!=EnumDeepCoreMiner.Middle2)
		{
			tile.restoreUsedBlocks();
			return false;
		}
		state = w.getBlockState(pos.relative(face).above(2));
		if(state.getBlock()!=MultiblockBlocks.deepcore_miner || state.getValue(variants)!=EnumDeepCoreMiner.Middle3)
		{
			tile.restoreUsedBlocks();
			return false;
		}
		
		state = w.getBlockState(pos.relative(face, 2));
		if(state.getBlock()!=MultiblockBlocks.deepcore_miner || state.getValue(variants)!= (face.getAxis()==Axis.X ? EnumDeepCoreMiner.Inv1X : EnumDeepCoreMiner.Inv1Z))
		{
			tile.restoreUsedBlocks();
			return false;
		}
		state = w.getBlockState(pos.relative(face, 2).above());
		if(state.getBlock()!=MultiblockBlocks.deepcore_miner || state.getValue(variants)!= (face.getAxis()==Axis.X ? EnumDeepCoreMiner.Inv2X : EnumDeepCoreMiner.Inv2Z))
		{
			tile.restoreUsedBlocks();
			return false;
		}
		state = w.getBlockState(pos.relative(face, 2).above(2));
		if(state.getBlock()!=MultiblockBlocks.deepcore_miner || state.getValue(variants)!= (face.getAxis()==Axis.X ? EnumDeepCoreMiner.Inv3X : EnumDeepCoreMiner.Inv3Z))
		{
			tile.restoreUsedBlocks();
			return false;
		}
		
		return true;
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//		return this.getDefaultState().with(variants, EnumDeepCoreMiner.fromMeta(meta));
//    }
//
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//        return state.get(variants).getMetadata();
//    }
	
//	@Override
//    protected BlockStateContainer createBlockState()
//    {
//        return new BlockStateContainer(this, variants);
//    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(variants);
		super.createBlockStateDefinition(builder);
	}
	
	@Override
	public boolean hasAnalogOutputSignal(BlockState state)
	{
		return state.getValue(variants) != EnumDeepCoreMiner.maschine;
	}
	
	@Override
	public int getAnalogOutputSignal(BlockState state, World w, BlockPos pos)
	{
		EnumDeepCoreMiner var = state.getValue(variants);
		if(var.isInventory())
		{
			if(w.getBlockEntity(pos) instanceof TileEntityDeepCoreMinerInventory)
			{
				TileEntityDeepCoreMinerInventory inv = (TileEntityDeepCoreMinerInventory) w.getBlockEntity(pos);
				return inv.getComparatorOutput();
			}
			else
			{
				return 0;
			}
		}
		else if(var == EnumDeepCoreMiner.Main || var == EnumDeepCoreMiner.MainUp1 || var == EnumDeepCoreMiner.MainUp2)
		{
			BlockPos main = getMainBlock(w, pos, state);
			if (main==null)
				return 0;
			TileEntityDeepCoreMinerMain tile = (TileEntityDeepCoreMinerMain) w.getBlockEntity(main);
			return tile.getComparatorOutput();
		}
		else
		{
			BlockPos main = getMainBlock(w, pos, state);
			if (main==null)
				return 0;
			
			TileEntityDeepCoreMinerMain tile = (TileEntityDeepCoreMinerMain) w.getBlockEntity(main);
			TileEntityBedrockRift rift = tile.getLogic().getRift();
			if(rift==null)
				return 0;
			
			return rift.getComparatorOutput();
		}
	}
	
	@Override
	public boolean propagatesSkylightDown(BlockState state, IBlockReader reader, BlockPos pos) 
	{
		EnumDeepCoreMiner type = state.getValue(variants);
		return type != EnumDeepCoreMiner.maschine;
	}
	
	@Override
	public int getLightBlock(BlockState state, IBlockReader worldIn, BlockPos pos) 
	{
		EnumDeepCoreMiner type = state.getValue(variants);
		if(type != EnumDeepCoreMiner.maschine)
		{
			return 0;
		}
		return super.getLightBlock(state, worldIn, pos);
	}
	
	/*
	 * FIXME: Find solution, because this is now final...
	@Override
	public boolean isSolid(BlockState state) 
	{
		EnumDeepCoreMiner type = state.get(variants);
		return type == EnumDeepCoreMiner.maschine;
	}*/
	
	
	public static enum EnumDeepCoreMiner implements IStringSerializable
	{
		maschine,
		
		Main,
		MainUp1(new BlockPos(0, -1, 0)),
		MainUp2(new BlockPos(0, -2, 0)),
		Middle1,Middle2,Middle3,
		Inv1X(new BlockPos(2, 0, 0)),Inv2X(new BlockPos(2, -1, 0)),Inv3X(new BlockPos(2, -2, 0)),
		Inv1Z(new BlockPos(0, 0, 2)),Inv2Z(new BlockPos(0, -1, 2)),Inv3Z(new BlockPos(0, -2, 2));

		private EnumDeepCoreMiner(BlockPos pos)
		{
			main = pos;
			inv = name().startsWith("Inv");
		}
		
		private EnumDeepCoreMiner()
		{
			this(new BlockPos(0,0,0));
		}
		
		private boolean inv;
		private BlockPos main;
		
		@Override
		public String getSerializedName()
		{
			return name().toLowerCase();
		}
		
		public int getMetadata()
		{
			return ordinal();
		}
		
		public static EnumDeepCoreMiner fromMeta(int i)
		{
			return EnumDeepCoreMiner.values()[i];
		}
		
		public boolean isInventory()
		{
			return inv;
		}
		
		//This can return the wrong direction so you need to add/subtract to the mian one
		public BlockPos getMain(boolean inverted)
		{
			if(inverted)
			{
				return new BlockPos(-main.getX(), main.getY(), -main.getZ());
			}
			else
				return main;
		}

		public boolean isMain()
		{
			return this == Main || this == maschine;
		}
	}
}
