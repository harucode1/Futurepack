package futurepack.common.block;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.network.FunkPacketPing;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public class TileEntityNetworkMaschine extends TileEntity implements ITileNetwork
{
	public TileEntityNetworkMaschine(TileEntityType<? extends TileEntityNetworkMaschine> tileEntityTypeIn) 
	{
		super(tileEntityTypeIn);
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(pkt instanceof FunkPacketPing)
		{
			((FunkPacketPing) pkt).pong(this);
		}
	}
}
