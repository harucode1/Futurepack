package futurepack.common.block.terrain;

import java.util.Random;

import futurepack.api.FacingUtil;
import futurepack.common.entity.living.EntityCrawler;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockSpiderHive extends Block 
{
	private final VoxelShape shape = VoxelShapes.or(Block.box(2, 0, 2, 14, 11, 14), VoxelShapes.or(Block.box(4, 14, 4, 12, 16, 12), Block.box(3, 11, 3, 13, 14, 13)));
	
	public BlockSpiderHive(Properties properties) 
	{
		super(properties.randomTicks().noOcclusion().noDrops());
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		return shape;
	}
	
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random rand)
	{
		if(w.random.nextInt(2) == 0)
		{
			EntityCrawler crawler = new EntityCrawler(w);
			crawler.setBaby(true);
			Direction face = FacingUtil.HORIZONTAL[w.random.nextInt(4)];
			pos = pos.relative(face);
			crawler.setPos(pos.getX()+0.5, pos.getY(), pos.getZ()+0.5);
			w.addFreshEntity(crawler);
		}
	}
    
    @Override
    public void onRemove(BlockState state, World w, BlockPos pos, BlockState newState, boolean isMoving)
    {
    	int c = 10 + w.random.nextInt(30);
		for(int i=0;i<c;i++)
		{
			EntityCrawler crawler = new EntityCrawler(w);
			crawler.setBaby(true);
			crawler.setPos(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5);
			crawler.setDeltaMovement(0, 0.5 * w.random.nextFloat(), 0); ;
			w.addFreshEntity(crawler);
		}
    	super.onRemove(state, w, pos, newState, isMoving);
    }
    
    @Override
    public boolean canSurvive(BlockState state, IWorldReader w, BlockPos pos)
    {
    	return HelperBoundingBoxes.isSideSolid(w, pos.below(), Direction.UP);
    }
}
