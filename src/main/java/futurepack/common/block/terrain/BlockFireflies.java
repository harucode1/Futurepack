package futurepack.common.block.terrain;

import java.util.Random;

import futurepack.client.particle.ParticleFireflyMoving;
import futurepack.client.particle.ParticleFireflyStill;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockFireflies extends Block
{

	public BlockFireflies(Properties properties) 
	{
		super(properties.noCollission().randomTicks().noOcclusion());
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, World w, BlockPos pos, Random rand)
	{
		float x = pos.getX() + rand.nextFloat();
		float y = pos.getY() + rand.nextFloat();
		float z = pos.getZ() + rand.nextFloat();
		
		ParticleFireflyStill fly = new ParticleFireflyMoving((ClientWorld) w,x,y,z, rand.nextFloat()-0.5, rand.nextFloat()-0.5, rand.nextFloat()-0.5);
		if(Dimensions.TYROS_ID.equals(w.dimension().location())) // has to be id comparison because DimensionID is ot inited if on server
			fly.initRandomColor(0x9900ff, 0x00FFff);
		Minecraft.getInstance().particleEngine.add(fly); 
		
		fly = new ParticleFireflyStill((ClientWorld) w, x, y, z);
		if(Dimensions.TYROS_ID.equals(w.dimension().location()))
			fly.initRandomColor(0x9900ff, 0x00FFff);
		Minecraft.getInstance().particleEngine.add(fly);
		
		super.animateTick(stateIn, w, pos, rand);
	}
	
	@Override
	public BlockRenderType getRenderShape(BlockState state)
	{
		return BlockRenderType.INVISIBLE;
	}
	

	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random rand)
	{
		if(w.isDay())
		{
			w.removeBlock(pos, false);
			return;
		}
			
		int h = w.getHeightmapPos(Type.MOTION_BLOCKING, pos).getY();
		
		Direction dir = Direction.UP;
		if(pos.getY()-h > 4)
		{
			dir = Direction.DOWN;
		}
		else
		{
			dir = Direction.from3DDataValue(1+rand.nextInt(5));
		}
			
		BlockPos pos2 = pos.relative(dir);
		if(w.isEmptyBlock(pos2))
		{
			w.setBlockAndUpdate(pos2, state);
			w.removeBlock(pos, false);
			w.getBlockTicks().scheduleTick(pos2, this, 20 * rand.nextInt(30));
		}
	}

}
