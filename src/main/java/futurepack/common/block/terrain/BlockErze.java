package futurepack.common.block.terrain;

import net.minecraft.block.BlockState;
import net.minecraft.block.OreBlock;
import net.minecraftforge.common.ToolType;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockErze extends OreBlock
{
	public final int harvestLevel;

	public BlockErze(Properties builder, int harvestLevel)
	{
		super(builder);
		this.harvestLevel = harvestLevel;
	}

	@Override
	public ToolType getHarvestTool(BlockState state)
	{
		return ToolType.PICKAXE;
	}
	
	@Override
	public int getHarvestLevel(BlockState state)
	{
		return harvestLevel;
	}
}
