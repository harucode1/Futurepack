package futurepack.common.block.terrain;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.GravelBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;

public class BlockFpGravel extends GravelBlock
{
	public BlockFpGravel(Block.Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public int getDustColor(BlockState state, IBlockReader reader, BlockPos pos) 
	{
		return 0x904400;
	}
}
