package futurepack.common.block.terrain;

import java.util.Random;

import futurepack.common.block.plants.PlantBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.block.RotatedPillarBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockTyrosLog extends RotatedPillarBlock
{

	public BlockTyrosLog(Properties props) 
	{
		super(props.randomTicks());
	}
	
	@Override
	public void randomTick(BlockState state, ServerWorld w, BlockPos pos, Random random)
	{
		if(random.nextInt(20)==0)
		{
			if(w.isEmptyBlock(pos.below()))
			{
			w.setBlockAndUpdate(pos.below(), PlantBlocks.glowmelo.defaultBlockState());
			}
		}
		super.tick(state, w, pos, random);
	}

}
