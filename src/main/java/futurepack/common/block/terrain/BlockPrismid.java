package futurepack.common.block.terrain;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.entity.LivingEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BlockPrismid extends Block 
{
	private final VoxelShape box = Block.box(1, 0, 1, 15, 15, 15);
	
	public BlockPrismid(Block.Properties props) 
	{
		super(props.randomTicks().noOcclusion());
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		return box;
	}
	
	@Override
	public boolean canSurvive(BlockState state, IWorldReader w, BlockPos pos)
	{
		BlockState stateDown = w.getBlockState(pos.below());
		return (stateDown.getBlock() == TerrainBlocks.prismide_stone);
	}
	
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random r)
	{
		super.tick(state, w, pos, r);

		neighborChanged(state, w, pos, Blocks.AIR, null, false);
		if(r.nextInt(1)==0 && !w.isClientSide)
		{
			List<LivingEntity> targets = w.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(pos.offset(-4, -4, -4), pos.offset(4, 4, 4)));
			for (LivingEntity entity : targets)
			{
				entity.addEffect(new EffectInstance(Effects.WITHER, 100, 2));
			}	
		}
		
		if(w.isClientSide)
		{
			w.addParticle(ParticleTypes.FIREWORK, pos.getX(), pos.getY()+0.4, pos.getZ()+0.5, r.nextGaussian() * 0.05D, 0.5D, r.nextGaussian() * 0.05D);
		}
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, World w, BlockPos pos, Random rand)
	{
		if(rand.nextFloat() <= 0.001)
	{
		
		ParticleManager aman = Minecraft.getInstance().particleEngine;
	
		for(int i = 0; i < 20; i++) 
		{
			float x = pos.getX() + rand.nextFloat();
			float y = pos.getY() + rand.nextFloat();
			float z = pos.getZ() + rand.nextFloat();
			
//			SimpleAnimatedParticle sp = new FireworkParticle.Spark(w, x, y, z, rand.nextFloat() - 0.5, -0.2 + rand.nextFloat() / 2.0  , rand.nextFloat() - 0.5, aman);
//			sp.setAlphaF(0.7F);
//
//			sp.setColorFade(0x000000);
//			
//			sp.setColor(0x114400 * rand.nextInt(4));
//
//			sp.multipleParticleScaleBy(0.8F);
//			
//			aman.addEffect(sp);
			
			w.addParticle(ParticleTypes.FIREWORK, x, y, z, rand.nextFloat() - 0.5, -0.2 + rand.nextFloat() / 2.0  , rand.nextFloat() - 0.5);
		
		}
	}
	}
	
	@SuppressWarnings(value = { "deprecation" })
	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block bl, BlockPos nBlock, boolean isMoving)
	{
		if(!canSurvive(state, w, pos) && !w.isClientSide)
		{
			w.destroyBlock(pos, true);
		}
		super.neighborChanged(state, w, pos, bl, nBlock, isMoving);
	}
	
	@Override
	public void onPlace(BlockState state, World w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		neighborChanged(state, w, pos, Blocks.AIR, null, isMoving);
		super.onPlace(state, w, pos, oldState, isMoving);
	}
	
	@Override
	public void onRemove(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.getBlock()!=this)
		{
			if(!worldIn.isClientSide)
			{
				List<LivingEntity> targets = worldIn.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(pos.offset(-4, -4, -4), pos.offset(4, 4, 4)));
				for (LivingEntity entity : targets)
				{
					entity.addEffect(new EffectInstance(Effects.WITHER, 100, 2));
				}
			}
		}
		super.onRemove(state, worldIn, pos, newState, isMoving);
	}

}
