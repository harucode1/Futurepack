package futurepack.common.block.terrain;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.server.ServerWorld;

public class BlockPrismidStone extends Block 
{
	
	public BlockPrismidStone(Block.Properties props) 
	{
		super(props.randomTicks());
//		super(Material.ROCK);
//		setCreativeTab(FPMain.tab_deco);
//		setSoundType(SoundType.GLASS);
//		setTickRandomly(true);
	}
	
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random r)
	{
		super.tick(state, w, pos, r);
		
		if(r.nextInt(100)==0 && !w.isClientSide)
		{
			 Biome b = w.getBiome(pos);
			
			if(w.isEmptyBlock(pos.above())) // always grow prsmide for now
			{
				w.setBlockAndUpdate(pos.above(), TerrainBlocks.prismide.defaultBlockState());
			}
		}
	}
	
	private int getAmountDropped(Random rand, int min, int max, int fortune)
	{
		int std = MathHelper.nextInt(rand, min, max);
		
		if(fortune > 0)
		{
	        int i = rand.nextInt(fortune + 2) - 1;
	
	        if (i < 0)
	        {
	            i = 0;
	        }

	        return std * (i + 1);
		}
		return std;
	}
	

}
