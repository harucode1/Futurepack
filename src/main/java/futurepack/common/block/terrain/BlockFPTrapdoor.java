package futurepack.common.block.terrain;

import net.minecraft.block.BlockState;
import net.minecraft.block.TrapDoorBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockFPTrapdoor extends TrapDoorBlock 
{

	public BlockFPTrapdoor(Properties properties) 
	{
		super(properties);
	}

	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult rayTraceResult) 
	{
		state = state.cycle(OPEN);
		worldIn.setBlock(pos, state, 2);
		if (state.getValue(WATERLOGGED)) {
			worldIn.getLiquidTicks().scheduleTick(pos, Fluids.WATER, Fluids.WATER.getTickDelay(worldIn));
		}
		
		this.playSound(player, worldIn, pos, state.getValue(OPEN));
		return ActionResultType.SUCCESS;
	}
}
