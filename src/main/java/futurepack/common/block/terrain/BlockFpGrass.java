package futurepack.common.block.terrain;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.GrassBlock;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

public class BlockFpGrass extends GrassBlock
{
	public BlockFpGrass(Block.Properties properties)
	{
		super(properties);
	}
	
	@Override
	public boolean canSustainPlant(BlockState state, IBlockReader world, BlockPos pos, Direction facing, IPlantable plantable)
	{
		PlantType plantType = plantable.getPlantType(world, pos.relative(facing));
		//fast accept
		if(plantType == PlantType.PLAINS)
		{
			return true;
		}
		return super.canSustainPlant(state, world, pos, facing, plantable);
	}
	
}
