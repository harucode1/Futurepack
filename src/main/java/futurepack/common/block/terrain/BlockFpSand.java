package futurepack.common.block.terrain;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.SandBlock;
import net.minecraft.block.material.Material;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

public class BlockFpSand extends SandBlock
{

	public BlockFpSand(int p_i48338_1_, Block.Properties properties) 
	{
		super(p_i48338_1_, properties);
	}


	@Override
	public boolean canSustainPlant(BlockState state, IBlockReader world, BlockPos pos, Direction facing, IPlantable plantable)
	{
		BlockState plant = plantable.getPlant(world, pos.relative(facing));
		net.minecraftforge.common.PlantType type = plantable.getPlantType(world, pos.relative(facing));
	    		   
		if (plant.getBlock() == Blocks.CACTUS)
		{
			return true;
		}
		
		if(type == PlantType.DESERT)
			return true;
		else if(type == PlantType.BEACH)
		{
            boolean isBeach = this.getBlock() == Blocks.GRASS_BLOCK || net.minecraftforge.common.Tags.Blocks.DIRT.contains(this) || this.getBlock() == Blocks.SAND || this.getBlock() == TerrainBlocks.sand_m;
            boolean hasWater = (world.getBlockState(pos.east()).getMaterial() == Material.WATER ||
                    world.getBlockState(pos.west()).getMaterial() == Material.WATER ||
                    world.getBlockState(pos.north()).getMaterial() == Material.WATER ||
                    world.getBlockState(pos.south()).getMaterial() == Material.WATER);
            return isBeach && hasWater;
		}
		else
		{
			return super.canSustainPlant(state, world, pos, facing, plantable);
		}
	}
}
