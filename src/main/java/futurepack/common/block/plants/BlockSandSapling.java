package futurepack.common.block.plants;

import net.minecraft.block.BlockState;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.trees.Tree;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.PlantType;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockSandSapling extends SaplingBlock
{
	private boolean asking = false;
	
	public BlockSandSapling(Tree p_i48337_1_, Properties properties)
	{
		super(p_i48337_1_, properties);
		
	}

    @Override
    public PlantType getPlantType(IBlockReader world, BlockPos pos) 
    {
    	return PlantType.DESERT;
    }

    @Override
    protected boolean mayPlaceOn(BlockState state, IBlockReader world, BlockPos pos)
    {
    	if(asking)
    		return false;
    	
    	if(state.getMaterial() == Material.SAND)
    	{
    		return true;
    	}
    	else
    	{
    		asking = true;
    		state.canSustainPlant(world, pos, Direction.UP, this);
    		asking = false;
    	}
    	
    	return super.mayPlaceOn(state, world, pos);
    }
   
//	/*Grow*/
//	public void grow(World worldIn, BlockPos pos, IBlockState state, Random rand)
//	 {
//	        if (state.getValue(STAGE).intValue() == 0)
//	        {
//	            worldIn.setBlockState(pos, state.func_235896_a_Property(STAGE), 4);
//	        }
//	        else
//	        {
//	            this.generateTree(worldIn, pos, state, rand);
//	        }
//	 }
	
//	public void generateTree(World worldIn, BlockPos pos, IBlockState state, Random rand)
//    {
//        if (!net.minecraftforge.event.terraingen.TerrainGen.saplingGrowTree(worldIn, rand, pos)) return;
//        
//        int meta = state.getValue(FPBlocks.META(maxmeta));
//        
//        if(meta == 0)
//        {
//        	//check for air
//        	for(int h=1; h<11; h++){
//        		if(!worldIn.isAirBlock(pos.up(h)))
//        			return;
//        	}
//        	
//        	WorldGenBigMushrooms wgbm = new WorldGenBigMushrooms();
//        	wgbm.generate(worldIn, rand, pos);
//        }
//        
//        if(meta == 1)
//        {
//        	int i = 0;
//        	int j = 0;
//        	int k = 0;
//        	int l = 0;
//        	
//            for (i = 0; i >= -4; --i)
//            {
//                for (j = 0; j >= -4; --j)
//                {
//                    if(this.canGrowHere(worldIn, pos, meta, i, j))
//                    {    
//                    	pos=pos.add(i, 0, j);
//                    	//check for Blocks im Stamm
//                		for (int x = 0; x <= 4; ++x)
//                        {
//                            for (int y = 1; y <= 36; ++y)
//                            {
//                            	for (int z = 0; z <= 4; z++)
//                            	{
//                            		if(!worldIn.isAirBlock(pos.add(x, y,z)))
//                            			return;
//                            	}
//                            }
//                        }
//                
//                    	WorldGenTyrosTree wgtt = new WorldGenTyrosTree();
//                		wgtt.addGenerationCoords(worldIn, pos.add(2, 0, 2), true);
//                		wgtt.progressTrees();
//                		wgtt.plantCachedTrees(worldIn);
//                		return;
//                    }
//                }
//            }
//        }
//   }
//	
//	private boolean canGrowHere(World worldIn, BlockPos pos, int meta, int i, int j){
//        int k = 0;
//        int l = 0;
//			
//		for (k = 0; k <= 4; ++k)
//        {
//            for (l = 0; l <= 4; ++l)
//            {
//                if(!this.isTypeAt(worldIn, pos.add(i+k, 0, j+l), 1))
//                	return false;
//            }  
//        }
//		return true;
//	}
//    
//    public boolean isTypeAt(World worldIn, BlockPos pos, int type)
//    {
//        IBlockState IBlockState = worldIn.getBlockState(pos);
//        return IBlockState.getBlock() == this && IBlockState.getValue(FPBlocks.META(maxmeta)) == type;
//    }
//	
    
    
    	
//    /*Meta Name and Inverntory*/
//	@Override
//	public String getMetaNameSub(ItemStack is) 
//	{
//		int i = is.getItemDamage();
//		switch(i)
//		{
//			case 0: return "menelaus";
//			case 1: return "tyros";
//		}
//		return getTranslationKey();
//	}
//
//	@Override
//	public int getMaxMetas()
//	{
//		return 2;
//	}
//
//	@Override
//	public String getMetaName(int i)
//	{
//		switch(i)
//		{
//			case 0: return "sapling_menelaus";
//			case 1: return "sapling_tyros";
//		}
//		return null;
//	}
//	
}
