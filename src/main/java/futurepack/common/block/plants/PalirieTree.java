package futurepack.common.block.plants;

import java.util.Random;

import futurepack.world.dimensions.biomes.FPBiomes;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.server.ServerWorld;

public class PalirieTree extends BasicNotTree<NoFeatureConfig> 
{
	private ConfiguredFeature<NoFeatureConfig, ?> tree = FPBiomes.PALIRIE_TREE.configured(NoFeatureConfig.INSTANCE);
	
	@Override
	protected ConfiguredFeature<NoFeatureConfig, ?> getTree(Random randomIn, boolean largeHive) 
	{
		return tree;
	}
	
	
	@Override
	public boolean growTree(ServerWorld world, ChunkGenerator chunkGenerator, BlockPos pos, BlockState state, Random rand)
	{
		world.setBlock(pos, Blocks.AIR.defaultBlockState(), 4);
//		tree.config.forcePlacement();
		if (tree.place(world, chunkGenerator, rand, pos)) {
			return true;
		} else {
			world.setBlock(pos, state, 4);
			return false;
		}
	}
}
