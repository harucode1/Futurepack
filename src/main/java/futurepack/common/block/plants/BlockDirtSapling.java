package futurepack.common.block.plants;

import net.minecraft.block.SaplingBlock;
import net.minecraft.block.trees.Tree;


import net.minecraft.block.AbstractBlock.Properties;

public class BlockDirtSapling extends SaplingBlock
{

	public BlockDirtSapling(Tree p_i48337_1_, Properties properties)
	{
		super(p_i48337_1_, properties);
	}

}
