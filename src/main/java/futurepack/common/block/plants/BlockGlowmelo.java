package futurepack.common.block.plants;

import java.util.Random;

import futurepack.common.FPConfig;
import futurepack.common.item.FoodItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FallingBlock;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class BlockGlowmelo extends FallingBlock
{
	static final int maxAge = 6;
	public static IntegerProperty AGE_0_6 = IntegerProperty.create("age", 0, maxAge);
	
	private final VoxelShape[] boxes;
	
	public BlockGlowmelo(Block.Properties props)
	{
		super(props);
		boxes=new VoxelShape[7];
		for(int i=0;i<boxes.length;i++)
		{
			boxes[i] = VoxelShapes.box( 0.4375F-i*0.0625F, 0.75F-i*0.125F, 0.4375F-i*0.0625F, 0.5625F+i*0.0625F, 1F, 0.5625F+i*0.0625F);
		}
	}
	
	@Override
	public ItemStack getCloneItemStack(IBlockReader worldIn, BlockPos pos, BlockState state)
	{
		return new ItemStack(FoodItems.glowmelo);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		int age = state.getValue(AGE_0_6);
		return boxes[age];
	}
	
	
	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerWorld w, BlockPos pos, Random random)
	{
		super.randomTick(state, w, pos, random);
		if(random.nextInt(5)==0)
		{	
			if(state.getValue(AGE_0_6)< maxAge)
			{
				state = state.setValue(AGE_0_6, state.getValue(AGE_0_6)+1);
				w.setBlockAndUpdate(pos, state);
			}
			else if(w.isEmptyBlock(pos.below()) && random.nextInt(5)==0 && FPConfig.SERVER.glowmelow_drop.get())
			{
				FallingBlockEntity entityfallingblock = new FallingBlockEntity(w, pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D, w.getBlockState(pos));
				entityfallingblock.setHurtsEntities(true);
				w.addFreshEntity(entityfallingblock);
			}
			else if(random.nextInt(10)==0)
			{
				w.removeBlock(pos, false);
			}
		}
	}

	
	@Override
	public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random)
	{
//		super.tick(state, worldIn, pos, random);
		if (!worldIn.isClientSide)
		{
			checkFall(worldIn, pos, state);
		}
	}
	
	private void checkFall(World w, BlockPos pos, BlockState state)
	{
		if(w.getBlockState(pos.above()).getMaterial() != Material.WOOD)
		{
			if (isFree(w.getBlockState(pos.below())) && pos.getY() >= 0) 
			{
				FallingBlockEntity entityfallingblock = new FallingBlockEntity(w, pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D, w.getBlockState(pos));
				entityfallingblock.setHurtsEntities(true);
				this.falling(entityfallingblock);
				w.addFreshEntity(entityfallingblock);
			}
		}
	}
	
//	@Override
//	public boolean isOpaqueCube(IBlockState state)
//	{
//		return false;
//	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(AGE_0_6);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) 
	{
		if(context.getItemInHand().getItem() == FoodItems.glowmelo)
		{
			return this.defaultBlockState().setValue(BlockGlowmelo.AGE_0_6, 6);
		}
		return super.getStateForPlacement(context);
	}
}
