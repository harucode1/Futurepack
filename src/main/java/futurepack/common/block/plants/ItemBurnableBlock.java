package futurepack.common.block.plants;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;


import net.minecraft.item.Item.Properties;

public class ItemBurnableBlock extends BlockItem
{
	private final int burntime;
	
	public ItemBurnableBlock(Block blockIn, Properties builder, int burntime) 
	{
		super(blockIn, builder);
		this.burntime = burntime;
	}
	
	@Override
	public int getBurnTime(ItemStack itemStack) 
	{
		return burntime;
	}

}
