package futurepack.common.block.plants;

import java.util.Random;

import futurepack.common.item.FoodItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropsBlock;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.Direction;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.PlantType;

public class BlockTopinambur extends CropsBlock
{
	 public static final BooleanProperty TOP = BooleanProperty.create("top");
	 private static final VoxelShape[] CROPS_AABB_1 = new VoxelShape[] {VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.5625D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};
	 private static final VoxelShape[] CROPS_AABB_2 = new VoxelShape[] {VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.1875D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.5625D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.9375D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), VoxelShapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};
	 
	public BlockTopinambur(Block.Properties	props) 
	{
		super(props);	
		this.registerDefaultState(this.stateDefinition.any().setValue(this.getAgeProperty(), Integer.valueOf(0)).setValue(TOP, false));
	}
	
	@Override
    public VoxelShape getShape(BlockState state, IBlockReader source, BlockPos pos, ISelectionContext sel)
    {
		int age = getAge(state);
		
		if(state.getValue(TOP))
			return CROPS_AABB_2[age];
		else
			return CROPS_AABB_1[age];
    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(TOP);
	}
    
    private boolean growBothBlocks(World worldIn, BlockPos pos, BlockState state, int age)
    {
    	if(age > getMaxAge())
    		return false;
    	
    	if(age > 2)
    	{
    		BlockState tbs = worldIn.getBlockState(pos.above());
    		
    		if(tbs.getBlock() == this)
    		{
    			worldIn.setBlock(pos.above(), tbs.setValue(AGE, age), 2);
    		}
    		else if(worldIn.isEmptyBlock(pos.above()))
    		{
    			worldIn.setBlock(pos.above(), this.getStateForAge(age).setValue(TOP, true), 3);
    		}
    		else
    		{
    			return false;
    		}
    		
    	}
    	
    	worldIn.setBlock(pos, state.setValue(AGE, age), 2);
    	
    	return true;
    }
    
    @Override
    public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand)
    {
        if(state.getValue(TOP))
        	return;

        if (!worldIn.isAreaLoaded(pos, 1)) return; // Forge: prevent loading unloaded chunks when checking neighbor's light
        if (worldIn.getRawBrightness(pos.above(), 0) >= 9)
        {
            int i = this.getAge(state);

            if (i < this.getMaxAge())
            {
                float f = getGrowthSpeed(this, worldIn, pos);

                if(net.minecraftforge.common.ForgeHooks.onCropsGrowPre(worldIn, pos, state, rand.nextInt((int)(25.0F / f) + 1) == 0))
                {
                	if(growBothBlocks(worldIn, pos, state, i + 1))
                	{         	
                		net.minecraftforge.common.ForgeHooks.onCropsGrowPost(worldIn, pos, state);
                	}
                }
            }
        }
    }
    
    @Override
    public void growCrops(World worldIn, BlockPos pos, BlockState state)
    {
        if(state.getValue(TOP))
        {
        	BlockState ibs = worldIn.getBlockState(pos.below());
        	if(ibs.getBlock() == this)
        	{
        		growCrops(worldIn, pos.below(), worldIn.getBlockState(pos.below()));
        	}
        	
        	return;
        }
        	
        int i = this.getAge(state) + this.getBonemealAgeIncrease(worldIn);
        int j = this.getMaxAge();

        if (i > j)
        {
            i = j;
        }

        growBothBlocks(worldIn, pos, state, i);
    }
    
    @Override
    protected int getBonemealAgeIncrease(World worldIn)
    {
        return MathHelper.nextInt(worldIn.random, 1, 3);
    }
    
    @Override
    public boolean canSurvive(BlockState state, IWorldReader worldIn, BlockPos pos)
    {
        if (state.getBlock() == this) //Forge: This function is called during world gen and placement, before this block is set, so if we are not 'here' then assume it's the pre-check.
        {
        	if(state.getValue(TOP))
        	{
        		BlockState ibs = worldIn.getBlockState(pos.below());
        		return ibs.getBlock() == this  && !ibs.getValue(TOP);
        	}
        	       	
            BlockState soil = worldIn.getBlockState(pos.below());
            return soil.getBlock().canSustainPlant(soil, worldIn, pos.below(), Direction.UP, this);
            
        }
        
        return super.canSurvive(state, worldIn, pos);
    }
        
    @Override
    public PlantType getPlantType(IBlockReader world, BlockPos pos) 
    {
    	return PlantType.PLAINS;
    }
    
    @Override
	protected IItemProvider getBaseSeedId() 
    {
        return FoodItems.topinambur_potato;
     }


    
}
