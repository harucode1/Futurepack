package futurepack.common.block.plants;

import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.trees.Tree;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.BaseTreeFeatureConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.server.ServerWorld;

public abstract class BasicNotTree<F extends IFeatureConfig> extends Tree 
{

	@Override
	protected ConfiguredFeature<BaseTreeFeatureConfig, ?> getConfiguredFeature(Random randomIn, boolean largeHive) 
	{
		return null;
	}
	
	protected abstract ConfiguredFeature<F, ?> getTree(Random randomIn, boolean largeHive);
	
	public boolean growTree(ServerWorld world, ChunkGenerator chunkGenerator, BlockPos pos, BlockState state, Random rand)
	{
		ConfiguredFeature<F, ?> tree = getTree(rand, false);
		
		world.setBlock(pos, Blocks.AIR.defaultBlockState(), 4);
//		tree.config.forcePlacement();
		if (tree.place(world, chunkGenerator, rand, pos)) {
			return true;
		} else {
			world.setBlock(pos, state, 4);
			return false;
		}
	}

}
