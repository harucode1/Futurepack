package futurepack.common.block.plants;

import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.common.FPTileEntitys;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityOxades extends TileEntity implements ITickableTileEntity
{
	private int ticks = 0;
	private int puff = 0;
	
	
	public TileEntityOxades() 
	{
		super(FPTileEntitys.OXADES);
	}
	
	public static final SoundEvent[] sound = new SoundEvent[]{SoundEvents.GRAVEL_HIT, SoundEvents.WOOL_HIT, SoundEvents.THORNS_HIT};
	
	private LazyOptional<IChunkAtmosphere> opt;
	
	@Override
	public void tick()
	{
		ticks++;
		if(ticks>200)
		{
			int age = level.getBlockState(worldPosition).getValue(BlockOxades.AGE);
			age -= 6;
			if(age > 0)
			{
				int oxy = age * ticks;
				Chunk c = level.getChunkAt(worldPosition);
				if(level.isClientSide)
				{
					puff = 3+age;
					float pitch = 1.5F + level.random.nextFloat();
					level.playLocalSound(worldPosition.getX()+0.5, worldPosition.getY()+0.7, worldPosition.getZ()+0.5, sound[level.random.nextInt(sound.length)], SoundCategory.BLOCKS, 0.2F, pitch, false);
				}
				else if(opt == null)
				{
					opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
					opt.addListener( p -> opt = null);
				}
				if(opt!=null)
				{
					opt.ifPresent(atm -> {
						int o = oxy - atm.addAir(worldPosition.getX()&15, worldPosition.getY(), worldPosition.getZ()&15, oxy);
						if(o > 0)
						{
							atm.addAir(worldPosition.getX()&15, worldPosition.getY(), worldPosition.getZ()&15, o);
						}
					});
				}
			}
			ticks = 0;
		}
		if(puff>0)
		{
			for(int i=0;i<puff;i++)
			{
				float x = (level.random.nextFloat() - 0.5F) * 0.1F;
				float z = (level.random.nextFloat() - 0.5F) * 0.1F;
				level.addParticle(ParticleTypes.CLOUD, worldPosition.getX()+0.5, worldPosition.getY()+0.7, worldPosition.getZ()+0.5, x, 0.1, z);
			}
			puff--;
			
		}
	}
	
	@Override
	public CompoundNBT save(CompoundNBT nbt)
	{	
		nbt.putInt("ticks", ticks);
		return super.save(nbt);
	}
	
	@Override
	public void load(BlockState state, CompoundNBT nbt)
	{
		ticks = nbt.getInt("ticks");
		super.load(state, nbt);
	}

	@Override
	public CompoundNBT getUpdateTag()
	{
		return save(new CompoundNBT());
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket()
	{
		return new SUpdateTileEntityPacket(worldPosition, 0, getUpdateTag());
	}
}
