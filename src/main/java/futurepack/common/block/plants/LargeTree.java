package futurepack.common.block.plants;

import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.trees.Tree;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.BaseTreeFeatureConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.server.ServerWorld;

public abstract class LargeTree extends Tree 
{
	final int size;

	public LargeTree(int width)
	{
		size = width;
	}

	@Override
	public boolean growTree(ServerWorld worldIn, ChunkGenerator chunkGenerator, BlockPos pos, BlockState blockUnder, Random rand) 
	{
		for (int x = 0; x > -size; --x) 
		{
			for (int z = 0; z > -size; --z) 
			{
				if (canLargeTreeSpawnAt(blockUnder, worldIn, pos, x, z)) 
				{
					return this.spawnLargeTree(worldIn, pos, blockUnder, rand, x, z);
				}
			}
		}

		return super.growTree(worldIn, chunkGenerator, pos, blockUnder, rand);
	}

	@Nullable
	protected abstract ConfiguredFeature<BaseTreeFeatureConfig, ?> getLargeTreeFeature(Random random);

	public boolean spawnLargeTree(ISeedReader worldIn, BlockPos pos, BlockState blockUnder, Random random, int xOffset, int zOffset) 
	{
		ConfiguredFeature<BaseTreeFeatureConfig, ?> abstracttreefeature = this.getLargeTreeFeature(random);
		if (abstracttreefeature == null) 
		{
			return false;
		} else 
		{
			BlockState air = Blocks.AIR.defaultBlockState();
			for (int x = 0; x < size; ++x) 
			{
				for (int z = 0; z < size; ++z) 
				{
					worldIn.setBlock(pos.offset(xOffset+x, 0, zOffset+z), air, 4);
				}
			}
			if (abstracttreefeature.place(worldIn, ((ServerWorld)worldIn).getChunkSource().getGenerator(), random, pos.offset(xOffset , 0, zOffset))) 
			{
				return true;
			}
			else
			{
				for (int x = 0; x < size; ++x) 
				{
					for (int z = 0; z < size; ++z) 
					{
						worldIn.setBlock(pos.offset(xOffset+x, 0, zOffset+z), blockUnder, 4);
					}
				}
				return false;
			}
		}
	}

	public boolean canLargeTreeSpawnAt(BlockState blockUnder, IBlockReader worldIn, BlockPos pos, int xOffset, int zOffset) 
	{
		Block block = blockUnder.getBlock();
		for (int x = 0; x < size; ++x) 
		{
			for (int z = 0; z < size; ++z) 
			{
				if(block != worldIn.getBlockState(pos.offset(xOffset+x, 0, zOffset+z)).getBlock())
				{
					return false;
				}
			}
		}
		return true;
	}

}
