package futurepack.common.block.plants;

import futurepack.common.item.FoodItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropsBlock;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockErse extends CropsBlock
{
	private static final VoxelShape box = VoxelShapes.box(0.25F, 0, 0.25F, 0.75F, 0.4375F, 0.75F);
	
	public BlockErse(Block.Properties b) 
	{
		super(b);		
	}
	
	@Override
	public void onRemove(BlockState state, World w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.isAir(w, pos))
		{
			if(isMaxAge(state))
			{
				w.setBlock(pos, getStateForAge(3), 2);
				Block.dropResources(state, w, pos);
			}
		}
		super.onRemove(state, w, pos, newState, isMoving);
	}
	
	@Override
	protected Item getBaseSeedId()
    {
        return FoodItems.erse;
    }

//    @Override
//	protected Item getCropsItem()
//    {
//        return FoodItems.erse;
//    }
//    
//    @Override
//    public int quantityDropped(BlockState state, Random random)
//    {
//    	return 1;
//    }
    
//    @Override
//    public boolean doesSideBlockRendering(IBlockState state, IWorldReader world, BlockPos pos, EnumFacing face)
//    {
//    	return false;
//    }
  
    
    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) 
    {
    	return box;
    }
}

