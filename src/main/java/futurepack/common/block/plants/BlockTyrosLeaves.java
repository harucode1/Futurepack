package futurepack.common.block.plants;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.LeavesBlock;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.LightType;
import net.minecraft.world.TickPriority;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.IForgeShearable;

import net.minecraft.block.AbstractBlock.Properties;

public class BlockTyrosLeaves extends LeavesBlock implements IForgeShearable
{
	public static IntegerProperty LIGHT = IntegerProperty.create("light", 0, 15);
	
	public BlockTyrosLeaves(Properties properties) 
	{
		super(properties.randomTicks());
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(LIGHT);
	}
	
	@Override
	public void tick(BlockState state, ServerWorld w, BlockPos pos, Random random)
	{
		int lightB = w.getBrightness(LightType.BLOCK, pos);
		if(state.getValue(LIGHT) != lightB)
		{
			state = state.setValue(LIGHT, lightB);
			w.setBlock(pos, state, 2);
		}
		super.tick(state, w, pos, random);
	}
	
	@Override
	public void neighborChanged(BlockState state, World w, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving)
	{
		if(!w.getBlockTicks().willTickThisTick(pos, this))
			w.getBlockTicks().scheduleTick(pos, this, 1, TickPriority.LOW);
		super.neighborChanged(state, w, pos, blockIn, fromPos, isMoving);
	}
	
//	public BlockFpLeaves()
//	{
//		this.leavesFancy = true;
//        this.setTickRandomly(true);
//        this.setCreativeTab(FPMain.tab_deco);
//        setDefaultState(this.blockState.getBaseState().withProperty(subtype, 0).withProperty(BlockLeaves.CHECK_DECAY, true).withProperty(BlockLeaves.DECAYABLE, true));
//        Blocks.FIRE.setFireInfo(this, 30, 60);
//        this.leavesFancy = true;
//	}
	
//	public static void main(String[] args) throws IOException
//	{
//		File img1 = new File("C:\\tmp\\t1.png");
//		File img2 = new File("C:\\tmp\\t2.png");
//		
//		BufferedImage bimg1 = ImageIO.read(img1);
//		BufferedImage bimg2 = ImageIO.read(img2);
//		
//		int w = Math.min(bimg1.getWidth(), bimg2.getWidth());
//		int h = Math.min(bimg1.getHeight(), bimg2.getHeight());
//		
//		for(int i=0;i<16;i++)
//		{
//			BufferedImage imgOut = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
//			WritableRaster raster = imgOut.getRaster();
//			for(int x=0;x<w;x++)
//			{
//				for(int y=0;y<h;y++)
//				{
//					int rgb1 = bimg1.getRGB(x, y);
//					int rgb2 = bimg2.getRGB(x, y);
//					
//					System.out.println("I1 "+Integer.toHexString(rgb1));
//					System.out.println("I2 "+Integer.toHexString(rgb2));
//					
//					float p1 = i/15F;
//					float p2 = 1 - i/15F;
//					
//					int r = (int) ( ((rgb1>>16) & 0xFF)*p1 + ((rgb2>>16) & 0xFF)*p2 );
//					int g = (int) ( ((rgb1>>8) & 0xFF)*p1 + ((rgb2>>8) & 0xFF)*p2 );
//					int b = (int) ( ((rgb1>>0) & 0xFF)*p1 + ((rgb2>>0) & 0xFF)*p2 );
//					
//					System.out.println("O "+Integer.toHexString(r) + Integer.toHexString(g) +Integer.toHexString(b));
//					
//					raster.setPixel(x, y, new int[]{r,g,b, 255});
//				}
//			}
//			ImageIO.write(imgOut, "png", new File("C:\\tmp\\tyros_frame"+i+".png"));
//		}
//		
//	}
}
