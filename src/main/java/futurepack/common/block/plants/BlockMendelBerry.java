package futurepack.common.block.plants;

import java.util.Random;

import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.item.FoodItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CropsBlock;
import net.minecraft.item.Item;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.PlantType;

public class BlockMendelBerry extends CropsBlock
{
	
	//todo
	private static final VoxelShape[] box = new VoxelShape[] {
			Block.box(6, 0, 6, 10, 3, 10), 
			Block.box(4, 0, 4, 12, 5, 12),
			Block.box(4, 0, 4, 12, 6, 12),
			Block.box(3, 0, 3, 13, 10, 13),
			Block.box(3, 0, 3, 13, 15, 13),
			Block.box(3, 0, 3, 13, 16, 13),
			Block.box(3, 0, 3, 13, 16, 13),
			Block.box(3, 0, 3, 13, 16, 13)
			};
	
	public BlockMendelBerry(Block.Properties props) 
	{
		super(props);		
	}
	
	@Override
	public void onRemove(BlockState state, World w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.isAir(w, pos))
		{
			Block.dropResources(state, w, pos);
			
			boolean canStay = canSurvive(state, w, pos);
					
			int age = getAge(state);
			if(age == 5)
			{
				w.setBlock(pos, state.setValue(AGE, 2), 2);
				//if(canStay)
				//	dropBlockAsItem(w, pos, state, 0);
			}
			else if (age >= getMaxAge())
			{
				w.setBlock(pos, state.setValue(AGE, 2), 2);
				//if(canStay)
				//	dropBlockAsItem(w, pos, state, 0);
			}		
			//ground is gone - full destroy
			if(!canStay)
			{
				//add an Unraw drop
				//if(age == 5 || age >= 7)
				//	dropBlockAsItem(w, pos, state.withProperty(AGE, 2), 0);
				
				w.removeBlock(pos, false);
			}
		}
		else
		{
			super.onRemove(state, w, pos, newState, isMoving);
		}
	}
	
	@Override
	public PlantType getPlantType(IBlockReader world, BlockPos pos)
	{
		return PlantType.DESERT;
	}
	
	@Override
	protected boolean mayPlaceOn(BlockState state, IBlockReader worldIn, BlockPos pos)
	{
		return super.mayPlaceOn(state, worldIn, pos) || state.getBlock() == TerrainBlocks.dirt_m;
	}
	
	@Override
	protected IItemProvider getBaseSeedId() 
	{
		return FoodItems.mendel_seed;
	}

	
	@Override
	public Item asItem() 
	{
		return FoodItems.mendel_seed;
	}
	

	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
	{
		return box[getAge(state)];
	}
	
//	@Override
//	public boolean shouldSideBeRendered(IBlockState blockState, IWorldReader blockAccess, BlockPos pos, EnumFacing side)
//	{
//		return true;
//	}
	
	private boolean willSeedHarvest(BlockState state)
	{
		 int age = getAge(state);
		 return (age == 5 || age >= getMaxAge());
	}
	
	@Override
	public void growCrops(World worldIn, BlockPos pos, BlockState state)
	{
		super.growCrops(worldIn, pos, state);
		state = worldIn.getBlockState(pos);
		purifyNeonSand(worldIn, pos, state);
	}
	
	@Override
	public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand)
	{
		super.tick(state, worldIn, pos, rand);
		state = worldIn.getBlockState(pos);
		purifyNeonSand(worldIn, pos, state);
	}
	
	public void purifyNeonSand(World w, BlockPos pos, BlockState state)
	{
		if(state.getBlock()!=this)
			return;
		
		if(getAge(state) < getMaxAge())
		{
			return;
		}
		
		BlockState down = w.getBlockState(pos.below());
		Block sand = down.getBlock();
		if(sand == TerrainBlocks.sand_alutin || sand == TerrainBlocks.sand_bioterium || sand == TerrainBlocks.sand_glowtite || sand == TerrainBlocks.sand_retium)
		{
			w.setBlockAndUpdate(pos.below(), TerrainBlocks.sand_neon.defaultBlockState());
			w.removeBlock(pos, false); //this is needed because of the break block mechanic
			w.setBlockAndUpdate(pos, Blocks.DEAD_BUSH.defaultBlockState());
		}
	}
}
