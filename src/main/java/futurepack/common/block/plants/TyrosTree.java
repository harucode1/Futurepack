package futurepack.common.block.plants;

import java.util.Random;

import futurepack.world.dimensions.biomes.FPBiomes;
import futurepack.world.gen.feature.TyrosTreeFeature;
import net.minecraft.world.gen.feature.BaseTreeFeatureConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;

public class TyrosTree extends LargeTree
{
	public TyrosTree() 
	{
		super(5);
	}

	@Override
	protected ConfiguredFeature<BaseTreeFeatureConfig, ?> getConfiguredFeature(Random random, boolean par2) 
	{
		return Feature.TREE.configured(FPBiomes.TYROS_TREE_CONFIG);
	}

	@Override
	protected ConfiguredFeature<BaseTreeFeatureConfig, ?> getLargeTreeFeature(Random random) 
	{
		return TyrosTreeFeature.LARGE_TYROS_TREE.configured(FPBiomes.TYROS_TREE_CONFIG);
	}

}
