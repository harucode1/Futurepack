package futurepack.common.block.plants;

import futurepack.common.item.FoodItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropsBlock;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;

public class BlockOxades extends CropsBlock
{
	public static final IntegerProperty AGE = IntegerProperty.create("age", 0, 11);
	private static VoxelShape[] boxes = new VoxelShape[16];
	
	public BlockOxades(Block.Properties props) 
	{
		super(props);		
	}
	
	@Override
	public IntegerProperty getAgeProperty()
    {
        return AGE;
    }
	
	@Override
	protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder)
	{
		builder.add(getAgeProperty());
	}

	@Override
    public int getMaxAge()
    {
        return 11;
    }
	
	
	
	@Override
	protected IItemProvider getBaseSeedId()
    {
        return FoodItems.oxades_seeds;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext sel)
    {
    	return getSelctionBox(getAge(state));
    }
    
    private static VoxelShape getSelctionBox(int growth)
    {
    	int[] height = new int[]{2,4,5,6,7,9,9,7,9,11,14,15};
    	if(boxes[height[growth]]!=null)
    		return boxes[height[growth]];
    	else
    	{
    		return boxes[height[growth]] = Block.box(1D, 0D, 1D, 15D, height[growth], 15D);
    	}
    }
    
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world)
	{
		return new TileEntityOxades();
	}
	
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
}
