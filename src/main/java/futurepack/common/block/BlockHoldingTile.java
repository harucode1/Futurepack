package futurepack.common.block;

import futurepack.api.interfaces.tilentity.ITileWithOwner;
import futurepack.common.DirtyHacks;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public abstract class BlockHoldingTile extends Block
{
	protected final boolean hasNBTCustomDrops;

	public BlockHoldingTile(Block.Properties builder, boolean hasNBTCustomDrops) 
	{
		super(hasNBTCustomDrops ? noDrops(builder) : builder);
		this.hasNBTCustomDrops = hasNBTCustomDrops;
	}
	
	public BlockHoldingTile(Block.Properties builder) 
	{
		this(builder, true);
	}
	@Override
	public boolean hasTileEntity(BlockState state)
	{
		return true;
	}
	
	private static Block.Properties noDrops(Block.Properties builder)
	{
		Block.Properties copy = Block.Properties.of(Material.CAKE);
		DirtyHacks.replaceData(copy, builder, Block.Properties.class);
		copy.noDrops();
		return copy;
	}
	
	@Override
	public abstract TileEntity createTileEntity(BlockState state, IBlockReader world);
	
	@SuppressWarnings("deprecation")
	@Override
	public void onRemove(BlockState state, World w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if (state.getBlock() != newState.getBlock()) 
		{
			if(hasNBTCustomDrops)
				HelperInventory.dropNBT(w, pos, state);
			
			super.onRemove(state, w, pos, newState, isMoving);
			w.removeBlockEntity(pos);
		}
	}

	@Override
	public BlockRenderType getRenderShape(BlockState state)
    {
        return BlockRenderType.MODEL;
    }

	@Override
	public void setPlacedBy(World w, BlockPos pos, BlockState state, LivingEntity liv, ItemStack it)
	{
		HelperInventory.placeNBT(w, pos, liv, it);
		
		if(liv instanceof PlayerEntity)
		{
			TileEntity tile = w.getBlockEntity(pos);
			if(tile!=null && tile instanceof ITileWithOwner)
			{
				((ITileWithOwner)tile).setOwner((PlayerEntity) liv);
			}
		}
	}
	
//	@Override
//	public void harvestBlock(World worldIn, PlayerEntity player, BlockPos pos, BlockState state, @Nullable TileEntity te, ItemStack stack) //COPY from BlockContainer
//	{
//		if (te instanceof INameable && ((INameable)te).hasCustomName()) 
//		{
//			player.addStat(Stats.BLOCK_MINED.get(this));
//			player.addExhaustion(0.005F);
//			if (worldIn.isRemote) 
//			{
//				return;
//			}
//
//			int i = EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, stack);
//			Item item = this.getItemDropped(state, worldIn, pos, i).asItem();
//			if (item == Items.AIR)
//			{
//				return;
//	         }
//
//			ItemStack itemstack = new ItemStack(item, this.quantityDropped(state, worldIn.rand));
//			itemstack.setDisplayName(((INameable)te).getCustomName());
//			spawnAsEntity(worldIn, pos, itemstack);
//		}
//		else
//		{
//			super.harvestBlock(worldIn, player, pos, state, (TileEntity)null, stack);
//		}
//	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean triggerEvent(BlockState state, World worldIn, BlockPos pos, int id, int param) //COPY from BlockContainer
	{
		super.triggerEvent(state, worldIn, pos, id, param);
		TileEntity tileentity = worldIn.getBlockEntity(pos);
		return tileentity == null ? false : tileentity.triggerEvent(id, param);
	}
}
