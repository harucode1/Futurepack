package futurepack.common.sync;

import java.util.Collection;
import java.util.function.Supplier;

import futurepack.common.recipes.RecipeManagerSyncer;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

public class MessageSendRecipes 
{
	private String managerName;
	private Collection<?> recipes;
	
	public MessageSendRecipes(String managerName, Collection<?> recipes)
	{
		this.managerName = managerName;
		this.recipes = recipes;
	}

	@SuppressWarnings("deprecation")
	public static void consume(MessageSendRecipes message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				RecipeManagerSyncer.INSTANCE.overrideClientRecipes(message.managerName, message.recipes);
			}
		});
		ctx.get().setPacketHandled(true);
	}
	
	public static MessageSendRecipes decode(PacketBuffer buf) 
	{
		String name = buf.readUtf();
		Collection<?> recipes = RecipeManagerSyncer.INSTANCE.readRecipes(name, buf);
		return new MessageSendRecipes(name, recipes);
	}
		
	public static void encode(MessageSendRecipes msg, PacketBuffer buf) 
	{
		buf.writeUtf(msg.managerName);
		RecipeManagerSyncer.INSTANCE.writeRecipes(msg.managerName, msg.recipes, buf);
	}
}
