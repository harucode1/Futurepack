package futurepack.common.sync;

import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

/**RenderPipeItems sync to Server*/
public class MessageRendering
{

	boolean doRender;
	boolean syncEater;
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public MessageRendering(boolean render, boolean eater) 
	{
		doRender = render;
		syncEater = eater;
	}
	
	public static MessageRendering decode(PacketBuffer buf) 
	{
		return new MessageRendering(buf.readBoolean(), buf.readBoolean());
	}
	
	public static void encode(MessageRendering msg, PacketBuffer buf) 
	{
		buf.writeBoolean(msg.doRender);
		buf.writeBoolean(msg.syncEater);
	}
	
	public static void consume(MessageRendering message, Supplier<NetworkEvent.Context> ctx) 
	{
		ServerPlayerEntity mp =  ctx.get().getSender();
		FPPacketHandler.map.put(mp.getGameProfile().getId(), new boolean[]{message.doRender, message.syncEater});
		ctx.get().setPacketHandled(true);
	}
}
