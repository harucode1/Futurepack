package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.common.recipes.RecipeManagerSyncer;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

public class MessageSendAllRecipesCorrect 
{
	private String managerName;
	
	public MessageSendAllRecipesCorrect(String managerName)
	{
		this.managerName = managerName;
	}

	public static void consume(MessageSendAllRecipesCorrect message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				RecipeManagerSyncer.INSTANCE.allRecipesOk(message.managerName);
			}
		});
		ctx.get().setPacketHandled(true);
	}
	
	public static MessageSendAllRecipesCorrect decode(PacketBuffer buf) 
	{
		String name = buf.readUtf();
		return new MessageSendAllRecipesCorrect(name);
	}
		
	public static void encode(MessageSendAllRecipesCorrect msg, PacketBuffer buf) 
	{
		buf.writeUtf(msg.managerName);
	}
}
