package futurepack.common.sync;

import java.util.List;
import java.util.function.Supplier;

import futurepack.common.block.misc.TileEntityDungeonCheckpoint;
import futurepack.common.dim.structures.TeleporterMap;
import futurepack.common.dim.structures.TeleporterMap.TeleporterEntry;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;

public class MessageChekpointSelect 
{

	private long teleporterPos;
	
	public MessageChekpointSelect(long l) 
	{
		teleporterPos = l;
	}
	
	public static MessageChekpointSelect decode(PacketBuffer buf) 
	{
		return new MessageChekpointSelect(buf.readLong());
	}
		
	public static void encode(MessageChekpointSelect msg, PacketBuffer buf) 
	{
		buf.writeLong(msg.teleporterPos);
	}
			
	public static void consume(MessageChekpointSelect message, Supplier<NetworkEvent.Context> ctx) 
	{
		if(ctx.get().getDirection() == NetworkDirection.PLAY_TO_SERVER)
		{
			ctx.get().enqueueWork(new Runnable()
			{	
				@Override
				public void run() 
				{
					ServerPlayerEntity pl = ctx.get().getSender();
					TeleporterMap tmap = TeleporterMap.getTeleporterMap((ServerWorld) pl.getLevel());
					List<TeleporterEntry> l = tmap.getTeleporter(pl);
					for(TeleporterEntry e : l)
					{
						if(e.pos.asLong() == message.teleporterPos)
						{
							TileEntityDungeonCheckpoint tile = (TileEntityDungeonCheckpoint) pl.getLevel().getBlockEntity(e.pos);
							tile.teleportTo(pl, e.pos);
						}
					}
						
				}
			});
			ctx.get().setPacketHandled(true);
		}
		
	}
}
