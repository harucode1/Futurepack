package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.api.interfaces.IAirSupply;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

/** 
 * Send the modified Air to the Player (for Athmosphereless planets)
 */
public class MessagePlayerAir
{
	private int playerAir;
	
	
	public MessagePlayerAir(int playerAir)
	{
		this.playerAir = playerAir;
	}
	
	public MessagePlayerAir(PlayerEntity pl)
	{
		this(pl.getCapability(AtmosphereManager.cap_AIR, null).orElseThrow(IllegalArgumentException::new).getAir());
	}
	
	public static MessagePlayerAir decode(PacketBuffer buf) 
	{
		return new MessagePlayerAir(buf.readVarInt());
	}
	
	public static void encode(MessagePlayerAir msg, PacketBuffer buf) 
	{
		buf.writeVarInt(msg.playerAir);
	}
	
	public static void consume(MessagePlayerAir message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () ->  
											() -> 
		{
			LazyOptional<IAirSupply> l = Minecraft.getInstance().player.getCapability(AtmosphereManager.cap_AIR, null);
			l.ifPresent(air -> {
				air.addAir(message.playerAir - air.getAir());
			});
			ctx.get().setPacketHandled(true);
		}); 
	}
}
