package futurepack.common.sync;

import futurepack.common.FPLog;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraftforge.fml.network.NetworkDirection;

public class NetworkHandler {

	/**
	 * Send the Server AirCap value to the Client
	 */
	public static void sendPlayerAir(ServerPlayerEntity mp)
	{
		FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(new MessagePlayerAir(mp), mp.connection.connection, NetworkDirection.PLAY_TO_CLIENT);
	}

	/**
	 * Sends empty Packet to server to get all Researches from this player
	 */
	public static void requestResearchDataFromServer()
	{
		FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageResearchCall());
	}

	/**
	 * Send the pressed key to the server
	 */
	public static void sendKeyPressedToServer(EnumKeyTypes type)
	{
		FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageKeyPressed(type));
	}

	public static void sendResearchesToPlayer(ServerPlayerEntity sender)
	{
		FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(new MessageResearchResponse(sender.server, CustomPlayerData.getDataFromPlayer(sender)), sender.connection.connection, NetworkDirection.PLAY_TO_CLIENT);
		FPLog.logger.debug("Resived Research request from " + sender.getName());
	}

}
