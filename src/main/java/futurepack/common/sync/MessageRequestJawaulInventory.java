package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.common.entity.living.EntityAlphaJawaul;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;


public class MessageRequestJawaulInventory
{
	public MessageRequestJawaulInventory()
	{

	}

	public static MessageRequestJawaulInventory decode(PacketBuffer buf) 
	{
		return new MessageRequestJawaulInventory();
	}
		
	public static void encode(MessageRequestJawaulInventory msg, PacketBuffer buf) 
	{
	}
			
	public static void consume(MessageRequestJawaulInventory message, Supplier<NetworkEvent.Context> ctx) 
	{
		ServerPlayerEntity pl = ctx.get().getSender();
		
		if(pl.isPassenger() && pl.getVehicle() instanceof EntityAlphaJawaul)
		{
			pl.openMenu((EntityAlphaJawaul)pl.getVehicle());
		}
		
		ctx.get().setPacketHandled(true);
	}
	
}
