package futurepack.common.sync;

import java.util.Arrays;
import java.util.function.Supplier;

import futurepack.api.Constants;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.Event;

public class KeyManager
{
	public static void onPlayerKeyInput(ServerPlayerEntity pl, EnumKeyTypes type)
	{
		EventFuturepackKey event = new EventFuturepackKey(pl, type);
		MinecraftForge.EVENT_BUS.post(event);
	}
	
	public static enum EnumKeyTypes
	{
		ALL_BUTTONS,
		GLEITER,
		COMPOSITEARMOR,
		MODUL_MAGNET;
	
		public final String translationKey;
		
		private EnumKeyTypes()
		{
			translationKey = "key." + Constants.MOD_ID + "." + this.name().toLowerCase();
		}
		
		public Supplier<ItemStack> getDisplayItemStack(PlayerEntity pl)
		{
			switch(this)
			{
			case GLEITER:
				return () -> new ItemStack(ToolItems.gleiter);
			case COMPOSITEARMOR:
				return () -> new ItemStack(ToolItems.composite_chestplate);
			case MODUL_MAGNET:
				return () -> new ItemStack(ToolItems.modul_magnet);
			default:
				return () -> ItemStack.EMPTY;
			}
		}
		
		public boolean shouldShow()
		{
			return this != ALL_BUTTONS;
		}

		public static EnumKeyTypes[] getKeys()
		{
			return Arrays.stream(values()).filter(EnumKeyTypes::shouldShow).toArray(EnumKeyTypes[]::new);
		}
	}
	
	/**Posted at {@link MinecraftForge#EVENT_BUS}
	 */
	public static class EventFuturepackKey extends Event
	{
		public final ServerPlayerEntity player;
		public final EnumKeyTypes type;
		
		public EventFuturepackKey(ServerPlayerEntity presser, EnumKeyTypes type)
		{
			super();
			this.player = presser;
			this.type = type;
		}
	}
}
