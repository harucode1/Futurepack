package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import io.netty.buffer.Unpooled;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;

/**
 * Used in some Conatainers for Extra Data (from CLient to Server)
*/
public class MessageContainer 
{
	private PacketBuffer filled;
	
	public MessageContainer(IGuiSyncronisedContainer tile)
	{
		PacketBuffer pb = new PacketBuffer(Unpooled.buffer());
		tile.writeToBuffer(pb);
		filled = pb;
	}
		
	public MessageContainer(PacketBuffer filled) 
	{
		this.filled = filled;
	}
	
	public static MessageContainer decode(PacketBuffer buf) 
	{
		byte[] data = buf.readByteArray();
		return new MessageContainer(new PacketBuffer(Unpooled.wrappedBuffer(data)));
	}

	public static void encode(MessageContainer msg, PacketBuffer buf) 
	{
		byte[] data = new byte[msg.filled.readableBytes()];
		msg.filled.readBytes(data);
		buf.writeByteArray(data);
	}
		
		
	public static void consume(MessageContainer message, Supplier<NetworkEvent.Context> ctx) 
	{
		if(ctx.get().getDirection() == NetworkDirection.PLAY_TO_SERVER)
		{
			ServerPlayerEntity player = ctx.get().getSender();
				
			if(player!=null)
			{
				//System.out.println(player);
				if(player.containerMenu instanceof IGuiSyncronisedContainer)
				{
					IGuiSyncronisedContainer sync = (IGuiSyncronisedContainer) player.containerMenu;
					sync.readFromBuffer(message.filled);
					ctx.get().setPacketHandled(true);
				}
			}
		}
	}
	
}
