package futurepack.common.sync;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.function.Supplier;

import futurepack.common.entity.CapabilityPlayerData;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

/**Sync NBT data from Entity with client*/
public class MessageFPData
{
	private CompoundNBT nbt;
	private int id;

	public MessageFPData(CompoundNBT data, int id)
	{
		nbt = data;
		this.id = id;
	}
	
	public static MessageFPData decode(PacketBuffer buf) 
	{
		int id = buf.readVarInt();
		int i = buf.readVarInt();
		if(i>0)
		{
			byte[] bytes = new byte[i];
			buf.readBytes(bytes);
			try
			{
				CompoundNBT nbt = CompressedStreamTools.readCompressed(new ByteArrayInputStream(bytes));
				return new MessageFPData(nbt, id);
			}
			catch (IOException e) 
			{
				throw new RuntimeException(e);
			}
		}
		return null;
	}

	public static void encode(MessageFPData msg, PacketBuffer buf) 
	{
		buf.writeVarInt(msg.id);
		try
		{
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			CompressedStreamTools.writeCompressed(msg.nbt, out);
			byte[] bytes = out.toByteArray();
			buf.writeVarInt(bytes.length);
			buf.writeBytes(bytes);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
		
		
	public static void consume(MessageFPData message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				World w = Minecraft.getInstance().level;
				if(w!=null)
				{
					Entity e = w.getEntity(message.id);
					if(e!=null)
					{
						e.getCapability(CapabilityPlayerData.cap_PLAYERDATA).ifPresent(d -> {
							d.addAll(message.nbt);
						});
					}		
				}
			}
		});
		ctx.get().setPacketHandled(true);
	}

}
