package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.common.block.modification.TileEntityLaserBase;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

/**Packet for Entity Eater Sync*/
public class MessageEntityForEater
{
	private int id;
	private BlockPos pos;
	
	public MessageEntityForEater(LivingEntity e, BlockPos pos)
	{
		this.id = e.getId();
		this.pos = pos;
	}
	
	public MessageEntityForEater()
	{
	}

	
	@SuppressWarnings("unchecked")
	private void consume()
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				World w = Minecraft.getInstance().level;

				LivingEntity liv = (LivingEntity) w.getEntity(id);
				if(liv!=null)
				{
					@SuppressWarnings("rawtypes")
					TileEntityLaserBase eater = (TileEntityLaserBase) w.getBlockEntity(pos);
					if(eater!=null)
						eater.setEntityLiv(liv);			
				}
			}
		});
		
	}
	
	public static void consume(MessageEntityForEater message, Supplier<NetworkEvent.Context> ctx) 
	{
		message.consume();
		ctx.get().setPacketHandled(true);
	}
	
	public static MessageEntityForEater decode(PacketBuffer buf) 
	{
		MessageEntityForEater msg = new MessageEntityForEater();
		msg.pos = buf.readBlockPos();
		msg.id =  buf.readVarInt();
		return msg;
	}
		
	public static void encode(MessageEntityForEater msg, PacketBuffer buf) 
	{
		buf.writeBlockPos(msg.pos);
		buf.writeVarInt(msg.id);
	}
	
}
