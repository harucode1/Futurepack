package futurepack.common.sync;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

/**
 * Sended to Server when need a Players Research Data
*/
public class MessageResearchCall
{
	public static MessageResearchCall decode(PacketBuffer buf) 
	{
		return new MessageResearchCall();
	}
	
	public static void encode(MessageResearchCall msg, PacketBuffer buf) 
	{
		
	}
	
	public static void consume(MessageResearchCall message, Supplier<NetworkEvent.Context> ctx) 
	{
		NetworkHandler.sendResearchesToPlayer(ctx.get().getSender());
		ctx.get().setPacketHandled(true);
	}
	
}
