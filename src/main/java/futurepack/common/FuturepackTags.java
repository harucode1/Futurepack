package futurepack.common;

import futurepack.api.Constants;
import net.minecraft.block.Block;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.FluidTags;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ITag.INamedTag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;

public class FuturepackTags
{
	
	public static final INamedTag<Block> force_field = bockWrapper(new ResourceLocation(Constants.MOD_ID, "dungeon/force_field"));
	public static final INamedTag<Block> entity_laser = bockWrapper(new ResourceLocation(Constants.MOD_ID, "dungeon/entity_laser"));
	public static final INamedTag<Block> wardrobe = bockWrapper(new ResourceLocation(Constants.MOD_ID, "wardrobe"));
	public static final INamedTag<Block> METAL_FENCE = bockWrapper(new ResourceLocation(Constants.MOD_ID, "metal_fence"));
	public static final INamedTag<Block> METAL_BLOCK = bockWrapper(new ResourceLocation(Constants.MOD_ID, "metal_block"));
	public static final INamedTag<Block> quantanium_connecting = bockWrapper(new ResourceLocation(Constants.MOD_ID, "quantanium_connecting"));
	public static final INamedTag<Block> tag_crystal_ground_neon = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/neon"));//done
	public static final INamedTag<Block> tag_crystal_ground_alutin = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/alutin"));//done
	public static final INamedTag<Block> tag_crystal_ground_retium = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/retium"));//done
	public static final INamedTag<Block> tag_crystal_ground_glowtite = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/glowtite"));//done
	public static final INamedTag<Block> tag_crystal_ground_bioterium = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/bioterium"));//Done
	public static final INamedTag<Block> SAPLING_HOLDER = bockWrapper(new ResourceLocation(Constants.MOD_ID, "sapling_holder"));
	public static final INamedTag<Block> not_miner_breakable = bockWrapper(new ResourceLocation(Constants.MOD_ID, "not_miner_breakable"));
	public static final INamedTag<Block> MYCEL = bockWrapper(new ResourceLocation(Constants.MOD_ID, "mycel"));
	public static final INamedTag<Block> thruster = bockWrapper(new ResourceLocation(Constants.MOD_ID, "spaceship/thrusters"));
	public static final INamedTag<Block> teleporter = bockWrapper(new ResourceLocation(Constants.MOD_ID, "spaceship/teleporter"));
	public static final INamedTag<Block> neon_producer = bockWrapper(new ResourceLocation(Constants.MOD_ID, "spaceship/neon_producer"));
	public static final INamedTag<Block> ORE_IRON = bockWrapper(new ResourceLocation("forge:ores/iron"));
	public static final INamedTag<Block> ORE_MAGNETITE = bockWrapper(new ResourceLocation("forge:ores/magnetite"));
	public static final INamedTag<Block> block_crystals = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystals"));
	public static final INamedTag<Block> neon_sand = bockWrapper(new ResourceLocation(Constants.MOD_ID, "neonsand"));
	public static final INamedTag<Block> plasmatank_wall = bockWrapper(new ResourceLocation(Constants.MOD_ID, "plasmatank_wall")); //TODO
	public static final INamedTag<Block> stone_menelaus = bockWrapper(new ResourceLocation(Constants.MOD_ID, "stone_menelaus"));
	public static final INamedTag<Block> erse_spawn_able = bockWrapper(new ResourceLocation(Constants.MOD_ID, "erse_spawn_able"));
	public static final INamedTag<Block> radioactive_light = bockWrapper(new ResourceLocation(Constants.MOD_ID, "radioactive_light"));
	public static final INamedTag<Block> BLOCK_ORES = bockWrapper(new ResourceLocation("forge:ores"));
	
	public static final INamedTag<Item> MAGNETIC = itemWrapper(new ResourceLocation(Constants.MOD_ID, "magnetic"));
	public static final INamedTag<Item> MAGNET = itemWrapper(new ResourceLocation(Constants.MOD_ID, "magnet"));
	public static final INamedTag<Item> item_crystals = itemWrapper(new ResourceLocation(Constants.MOD_ID, "crystals"));
	public static final INamedTag<Item> gemDiamond = itemWrapper(new ResourceLocation("forge:gems/diamond"));
	public static final INamedTag<Item> gemQuartz = itemWrapper(new ResourceLocation("forge:gems/quartz"));
	public static final INamedTag<Item> ingotIron = itemWrapper(new ResourceLocation("forge:ingots/iron"));
	public static final INamedTag<Item> ingotNeon = itemWrapper(new ResourceLocation("forge:ingots/neon"));
	public static final INamedTag<Item> ingotCopper = itemWrapper(new ResourceLocation("forge:ingots/copper"));
	public static final INamedTag<Item> ingotGold = itemWrapper(new ResourceLocation("forge:ingots/gold"));
	public static final INamedTag<Item> COMPOST = itemWrapper(new ResourceLocation(Constants.MOD_ID, "compost"));//done
	public static final INamedTag<Item> INGOTS = itemWrapper(new ResourceLocation("forge:ingots"));
	public static final INamedTag<Item> GEMS = itemWrapper(new ResourceLocation("forge:gems"));
	public static final INamedTag<Item> ORES = itemWrapper(new ResourceLocation("forge:ores"));
	public static final INamedTag<Item> UNCOLOR = itemWrapper(new ResourceLocation(Constants.MOD_ID, "uncolor"));
	public static final ITag<Item> MINING_MINDCONTROL = itemWrapper(new ResourceLocation(Constants.MOD_ID, "mining_mindcontrol"));
	
	public static final INamedTag<Fluid> PARALYZING = fluidWrapper(new ResourceLocation(Constants.MOD_ID, "paralyzing"));
	public static final INamedTag<Fluid> OXYGEN = fluidWrapper(new ResourceLocation(Constants.MOD_ID, "oxygen"));
	
	
	public static ITag.INamedTag<Block> bockWrapper(ResourceLocation res)
	{
		return BlockTags.bind(res.toString());
	}
	
	public static ITag.INamedTag<Item> itemWrapper(ResourceLocation res)
	{
		return ItemTags.bind(res.toString());
	}
	
	public static ITag.INamedTag<Fluid> fluidWrapper(ResourceLocation res)
	{
		return FluidTags.bind(res.toString());
	}
}
