package futurepack.common.player;

import com.mojang.authlib.GameProfile;

import net.minecraft.network.NetworkManager;
import net.minecraft.network.PacketDirection;
import net.minecraft.network.play.ServerPlayNetHandler;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.FakePlayer;

public class FuturepackPlayer extends FakePlayer
{

	public FuturepackPlayer(ServerWorld world, GameProfile name)
	{
		super(world, name);
		NetworkManager manager = new NetworkManager(PacketDirection.SERVERBOUND);
		this.connection = new ServerPlayNetHandler(world.getServer(), manager, this);
	}
	
	@Override
	public Vector3d position()
    {
        return new Vector3d(this.getX(), this.getY(), this.getZ());
    }
}
