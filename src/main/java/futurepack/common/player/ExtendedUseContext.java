package futurepack.common.player;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;

public class ExtendedUseContext extends ItemUseContext 
{
	//this is so I can access the constructor 
	protected ExtendedUseContext(World worldIn, PlayerEntity player, Hand handIn, ItemStack heldItem, BlockRayTraceResult rayTraceResultIn) 
	{
		super(worldIn, player, handIn, heldItem, rayTraceResultIn);
	}

}
