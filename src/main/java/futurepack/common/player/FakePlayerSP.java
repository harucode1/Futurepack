package futurepack.common.player;

import futurepack.api.interfaces.IAirSupply;
import futurepack.common.DirtyHacks;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.network.play.ClientPlayNetHandler;
import net.minecraft.client.util.ClientRecipeBook;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.stats.StatisticsManager;
import net.minecraft.tags.FluidTags;
import net.minecraft.tags.ITag;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class FakePlayerSP extends ClientPlayerEntity
{
	private ClientPlayerEntity wrapped;

	public FakePlayerSP(Minecraft mcIn, ClientWorld worldIn, ClientPlayNetHandler netHandler, StatisticsManager statFile, ClientRecipeBook book, boolean clientSneakState, boolean clientSprintState)
	{
		super(mcIn, worldIn, netHandler, statFile, book, clientSneakState, clientSneakState);
		wrapped = null;
	}
	
	public FakePlayerSP(ClientPlayerEntity sp)
	{
		this(Minecraft.getInstance(), (ClientWorld) sp.getCommandSenderWorld(), sp.connection, sp.getStats(), sp.getRecipeBook(), sp.isShiftKeyDown(), sp.isSprinting());
		DirtyHacks.replaceData(this, sp, ClientPlayerEntity.class);
		DirtyHacks.replaceData(this, sp, PlayerEntity.class);
		DirtyHacks.replaceData(this, sp, LivingEntity.class);
		DirtyHacks.replaceData(this, sp, Entity.class);
		wrapped = sp;
	}
	
	@Override
	public boolean isEyeInFluid(ITag<Fluid> tagIn)
	{
		if(FluidTags.WATER == tagIn)
		{
			if(!AtmosphereManager.hasEntityOxygen(this))
			{
				return true;
			}
		}
		return super.isEyeInFluid(tagIn);
	}
	
	@Override
	public int getAirSupply()
	{
		IAirSupply supp = getCapability(AtmosphereManager.cap_AIR, null).orElseGet(DefaultAir::new);
		return supp.getAir();
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(wrapped!=null)
		{
			return wrapped.getCapability(capability, facing);
		}
		else
			return super.getCapability(capability, facing);
	}
	
	private class DefaultAir implements IAirSupply
	{

		@Override
		public int getAir()
		{
			return FakePlayerSP.super.getAirSupply();
		}

		@Override
		public void addAir(int amount)
		{
			FakePlayerSP.super.setAirSupply(getAir() + amount);
		}

		@Override
		public void reduceAir(int amount)
		{
			addAir(-amount);
		}

		@Override
		public int getMaxAir()
		{
			return FakePlayerSP.super.getMaxAirSupply();
		}
		
	}
}
