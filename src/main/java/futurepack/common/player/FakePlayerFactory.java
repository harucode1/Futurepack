package futurepack.common.player;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

import com.mojang.authlib.GameProfile;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FakePlayerFactory
{
	public static GameProfile FUTUREPACK = new GameProfile(UUID.fromString("32A91F07-7AdB-4125-BA67-23E2B78DAF76"), "[Futurepack]");
	public static final FakePlayerFactory INSTANCE = new FakePlayerFactory();
	
	private Map<ServerWorld,FuturepackPlayer> players = new WeakHashMap<ServerWorld, FuturepackPlayer>();
	
	public FakePlayerFactory()
	{
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	public FuturepackPlayer getPlayer(ServerWorld server)
	{
		FuturepackPlayer pl = players.get(server);
		if(pl==null)
		{
			pl = new FuturepackPlayer(server, FUTUREPACK);
			players.put(server, pl);
		}
		return pl;
	}
	
	@SubscribeEvent
	public void onWorldUnload(WorldEvent.Unload event)
	{
//		ArrayList<FakePlayer> pls = players.get(event.world);
		players.remove(event.getWorld());
	}
	
	
	public static ActionResultType itemUseBase(World w, BlockPos pos, Direction face, ItemStack it, ServerPlayerEntity pl, Hand hand)
	{
		BlockRayTraceResult ray = new BlockRayTraceResult(new Vector3d(1, 1, 1), face, pos, false);
		ItemUseContext context = new ExtendedUseContext(w, pl, hand, it, ray);
		
		Item item = it.getItem();
		if(item instanceof BlockItem)
		{
			BlockState state = w.getBlockState(pos);
			if(!state.canBeReplaced(new BlockItemUseContext(context)))
			{
				return ActionResultType.FAIL;
			}
		}
		return item.useOn(context);
	}
	
	public static ActionResult<ItemStack> itemRightClickBase(World w, ItemStack it, ServerPlayerEntity pl, Hand hand)
	{
		Item item = it.getItem();
		return item.use(w, pl, hand);
	}
	
	public static ActionResultType itemEntityClick(World w, BlockPos pos, ItemStack it, ServerPlayerEntity pl, Hand hand)
	{
		List<LivingEntity> list =  w.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(pos, pos.offset(1, 1, 1)));
		if(!list.isEmpty())
		{
			return it.getItem().interactLivingEntity(it, pl, list.get(0), hand);
		}
		return ActionResultType.PASS;
	}
	
	public static ActionResult<ItemStack> itemClick(World w, BlockPos pos, Direction face, ItemStack it, ServerPlayerEntity pl, Hand hand)
	{
		ActionResultType result = itemEntityClick(w, pos, it, pl, hand);
		if(result.consumesAction())
		{
			return new ActionResult<ItemStack>(result, it);
		}
		else
		{
			result = itemUseBase(w, pos, face, it, pl, hand);
			if(result.consumesAction())
			{
				return new ActionResult<ItemStack>(result, it);
			}
			else
			{
				return itemRightClickBase(w, it, pl, hand);
			}
		}
	}
	
	public static void setupPlayer(ServerPlayerEntity pl, BlockPos blockPos, Direction face)
	{
		double x = blockPos.getX()+0.5 + 0.5*face.getStepX();
		double y = blockPos.getY()-1 - 0.5*face.getStepY();
		double z = blockPos.getZ()+0.5 + 0.5*face.getStepZ();
		
		pl.setPos(x, y, z);
		
		//pl.rotationYaw - N-O-S-W;
		//pl.rotationPitch - Oben-Unten;
		switch(face)
		{
		case DOWN:
			pl.xRot=90;
			pl.yRot = 0;
			break;
		case UP:
			pl.xRot=-90;
			pl.yRot = 0;
			break;
		case SOUTH:
			pl.xRot = 45;
			pl.yRot = 0;
			break;
		case NORTH:
			pl.xRot = 45;
			pl.yRot = -180;
			break;
		case WEST:
			pl.xRot = 45;
			pl.yRot = 90;
			break;
		case EAST:
			pl.xRot = 45;
			pl.yRot = -90;
			break;
		default:
			break;
		}
	}
}
