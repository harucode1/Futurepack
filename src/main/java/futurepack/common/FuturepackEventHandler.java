package futurepack.common;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Predicates;

import futurepack.api.Constants;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.CapabilityPlayerData;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ItemMindControllMiningHelmet;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import futurepack.common.sync.KeyManager.EventFuturepackKey;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.world.dimensions.Dimensions;
import futurepack.world.dimensions.atmosphere.FullBlockCache;
import futurepack.world.scanning.FPChunkScanner;
import net.minecraft.block.BlockState;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkPrimerWrapper;
import net.minecraft.world.chunk.ChunkStatus;
import net.minecraft.world.chunk.EmptyChunk;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.server.ChunkHolder;
import net.minecraft.world.server.ChunkManager;
import net.minecraft.world.server.ServerChunkProvider;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.CriticalHitEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerWakeUpEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FuturepackEventHandler 
{
	public static final FuturepackEventHandler INSTANCE = new FuturepackEventHandler();
	private static final UUID MICRO_GRAVITY_ID = UUID.fromString("A1B69F2A-2F7C-31EF-9022-7C4E7D5E2ABA");
	private static final AttributeModifier MICRO_GRAVITY = new AttributeModifier(MICRO_GRAVITY_ID, "MICRO GRAVITY", -0.075, AttributeModifier.Operation.ADDITION); // Add -0.07 to 0.08 so we get the vanilla default of 0.01
	   
	private byte cooldown = 120; // 120 ticks / 20 = 60 seconds / 60 = 1 minutes
	private ArrayList<WeakReference<MobEntity>> mindControlled = new ArrayList<WeakReference<MobEntity>>();
	
	
	@SubscribeEvent
	public void onCriticalHit(CriticalHitEvent event)
	{
		if(event.getPlayer().getMainHandItem().getItem() != ToolItems.sword_neon)
		{
			return;
		}
		event.setDamageModifier(2F);
		
		boolean critical = event.isVanillaCritical();
		if(!critical)
		{
			critical = !event.getPlayer().isOnGround() && !event.getPlayer().isInWater() && !event.getPlayer().isPassenger();
			if(!critical)
			{
				critical = event.getPlayer().level.random.nextInt(20) == 0;
			}
			if(critical)
			{
				event.setResult(Result.ALLOW);
			}
		}
		
		if(critical)
		{
			if(event.getTarget() instanceof LivingEntity)
			{
				LivingEntity liv = (LivingEntity) event.getTarget();
				liv.addEffect(new EffectInstance(FPPotions.paralyze, 20*5, 0));
			}
		}
	}
	
	public static boolean isModdedDimension(World type)
	{
		if(type== null)
			return false;
		else if(type.dimension().location() == null)
			return false;
		
		return Constants.MOD_ID.equalsIgnoreCase(type.dimension().location().getNamespace());
	}
	
	private List<ServerPlayerEntity> players = new ArrayList<ServerPlayerEntity>();
	
	@SubscribeEvent
	public void onDeath(LivingDeathEvent event)
	{
		if(event.getEntity() instanceof ServerPlayerEntity && !event.getEntity().level.isClientSide && (isModdedDimension(event.getEntity().getCommandSenderWorld())))
		{
			players.add((ServerPlayerEntity) event.getEntityLiving());
		}
	}
	
	
	@SubscribeEvent
	public void onJoin(final EntityJoinWorldEvent event)
	{
		if(event.getEntity() instanceof ServerPlayerEntity && !event.getEntity().level.isClientSide)
		{
			final ServerPlayerEntity pl = (ServerPlayerEntity) event.getEntity();
			
			if(players.contains(pl))
			{
				if(isModdedDimension(pl.getCommandSenderWorld()))
				{
					players.remove(pl);
					//TODO: player does not respawn in spaceship
					
//					final BlockPos bedLocation = pl.getBedLocation();
//					final boolean foced = pl.isSpawnForced();	
//					if(foced)
//					{
//						Thread t = new Thread(new Runnable()
//						{				
//							@Override
//							public void run()
//							{
//								try {
//									Thread.sleep(20);
//								} catch (InterruptedException e) {
//									e.printStackTrace();
//								}
//								BlockPos c = bedLocation;
//								BlockPos h = pl.world.getHeight(Type.MOTION_BLOCKING, c);
//									
//								if(c.getY() - h.getY() > 10)
//								{
//									c = h;
//								}
//										
//								if(c!=null)
//									pl.setPositionAndUpdate(c.getX()+0.5,c.getY()+0.5,c.getZ()+0.5);
//								FPLog.logger.debug("Respawn Player %s at custom position",pl);
//							}
//						}, "Player Respawn Helper " + pl.getDisplayName().getString());
//						t.start();
//					}
				}
			}
			
			
			
		}
		else if(event.getEntity() instanceof PlayerEntity && event.getEntity().level.isClientSide)
		{
			if(isModdedDimension(event.getWorld()))
			{
				Thread t = new Thread(new Runnable()
				{	
					@Override
					public void run()
					{
						if(event.getEntity() instanceof ClientPlayerEntity)
						{
	//						World w = event.getEntity().world;
							
							int logReducer = 20;
							int waiting = 0;
							
							while(((ClientPlayerEntity)event.getEntity()).xOld == 8.5 && ((ClientPlayerEntity)event.getEntity()).xOld == 8.5)
							{
								if(logReducer++ >= 20)
								{
									FPLog.logger.debug("Waiting for sync ...");
									logReducer = 0 - waiting*20;
									waiting++;
								}
								try {
									Thread.sleep(200);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
	
							Chunk c = event.getEntity().level.getChunkAt(event.getEntity().blockPosition());
							boolean output = false;
							while(c instanceof EmptyChunk)
							{
								double lx=event.getEntity().getX();
								double ly=event.getEntity().getY();
								double lz=event.getEntity().getZ();
								
								event.getEntity().lerpMotion(0, 0, 0);
								
								if(!output)
								{
									FPLog.logger.debug("Try to help Entity %s becasue the chunk is empty (was at %s,%s,%s)",event.getEntity(),lx,ly,lz);
									output = true;
								}
								
								c = event.getEntity().level.getChunkAt(event.getEntity().blockPosition());
								try {
									Thread.sleep(50);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}
					}
				});
				t.start();
			}
		}
	}
	
	
	
	@SubscribeEvent
	public void onPlayerWakeUp(PlayerWakeUpEvent event)
	{
		World w = event.getPlayer().level;
		if(isModdedDimension(w) && !w.isClientSide)
		{
			System.out.println(w.getDayTime());
			long time =  w.getDayTime() + 24000L;
			((ServerWorld)w).setDayTime(time);
			System.out.println(w.getDayTime());
		}
	}
	
//	@SubscribeEvent
//	public void onFuturepackKeyPressed(EventFuturepackKey key)
//	{
//		CompositeArmorInventory.onCompositeArmorOpened(key);
//	}
	
	@SubscribeEvent
	public void onChunkLoad(ChunkEvent.Load event)
	{		
		if(event.getChunk().getStatus().getChunkType() == ChunkStatus.Type.LEVELCHUNK)
		{
			if(event.getWorld()!=null)
			{
				if(event.getWorld().isClientSide())
					return;
				
				FPChunkScanner.INSTANCE.scanChunk(event.getChunk());
				return;
			}
			else
			{
				IChunk c = event.getChunk();
				if(c instanceof ChunkPrimerWrapper)
				{
					ChunkPrimerWrapper wr = (ChunkPrimerWrapper)c;
					Chunk base = wr.getWrapped();
					
					if(base.getLevel()!=null && !base.getLevel().isClientSide)
					{
						FPChunkScanner.INSTANCE.scanChunk(base);
						return;
					}
				}
				else
				{
					throw new IllegalArgumentException("Unknown chunk class with null world " + c.getClass());
				}
			}
			
		}
		else
		{
			return;
		}
			
	}
	
	public void spawnFireflies(World w, BlockPos pos)
	{
		int range = 12;
		for(int x = pos.getX()-range;x<pos.getX()+range;x++)
		{
			for(int z = pos.getZ()-range;z<pos.getZ()+range;z++)
			{
				if(w.random.nextInt(256)==0)
				{
					BlockPos xyz = w.getHeightmapPos(Type.WORLD_SURFACE, new BlockPos(x,0,z));
					xyz = xyz.above();
					if(w.isEmptyBlock(xyz))
					{
						BlockState state = TerrainBlocks.fireflies.defaultBlockState();
						w.setBlockAndUpdate(xyz, state);
					}					
				}				
			}
		}
	}
	
	
	
	@SubscribeEvent
	public void onWorldTick(TickEvent.WorldTickEvent event)
	{
		World w = event.world;
		if(!w.isClientSide && (w.dimension().location().equals(Dimensions.MENELAUS_ID) || w.dimension().location().equals(Dimensions.TYROS_ID)) && event.phase==Phase.START)
		{
			long time = w.getDayTime();
			if(time>13000 && w.random.nextInt(120)==0)
			{
				List<ServerPlayerEntity> pls = ((ServerWorld)w).players();
				for(PlayerEntity pl : pls)
				{
					BlockPos pos = pl.blockPosition();
					Biome b = w.getBiome(pos);
					
					if(!b.getRegistryName().getPath().contains("rockdesert"))
					{
						pos = pos.relative(w.random.nextBoolean() ? Direction.NORTH : Direction.SOUTH, w.random.nextInt(20)+ 20);
						pos = pos.relative(w.random.nextBoolean() ? Direction.EAST : Direction.WEST, w.random.nextInt(20)+ 20);
						spawnFireflies(w, pos);
					}
				}
			}
		}
		if(!w.isClientSide && (w.dimension().location().equals(Dimensions.ASTEROID_BELT_ID)))
		{
			if(event.world.getChunkSource() instanceof ServerChunkProvider)
			{
				ServerChunkProvider prov = (ServerChunkProvider) event.world.getChunkSource();
				ChunkManager cm = prov.chunkMap;
				Stream<ChunkHolder> status = cm.visibleChunkMap.values().stream();//AT are great
				List<Chunk> list = status.map(ChunkHolder::getTickingChunk).filter(Predicates.notNull()).collect(Collectors.toList());
				list.stream()
					.map(c -> c.getEntitySections()[0].getAllInstances())
					.filter(l -> !l.isEmpty())
					.flatMap(List::stream)
					.filter(e -> e.getY() < 0)
					.forEach(e -> e.teleportTo(e.getX(), 350, e.getZ()));
			}
		}
		if(!w.isClientSide)
		{
			FullBlockCache.onWorldTick(w);
		}
		
	}
	
	@SubscribeEvent
	public void onServerTick(TickEvent.ServerTickEvent event)
	{
		if(event.phase == Phase.END)
		{
			if(--cooldown < 0 )
			{
				cooldown = 120;
			}
			
			for(int i=0;i<mindControlled.size();i++)
			{
				WeakReference<MobEntity> ref = mindControlled.get(i);
				if(ref == null)
				{
					mindControlled.remove(i--);
				}
				else
				{	
					MobEntity mob = ref.get();
					if(mob == null || !mob.isAlive())
					{
						mindControlled.remove(i--);
					}
					else if(!ItemMindControllMiningHelmet.onMobArmorTick(mob))
					{
							mindControlled.remove(i--);
					}
				}
			}
		}
	}

	
	@SubscribeEvent
	public void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event)
	{
		if(event.getPlayer() instanceof ServerPlayerEntity)
		{
//			FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(new MessageMagnetisemUpdate(FPConfig.helmets, FPConfig.chestplates, FPConfig.leggings, FPConfig.boots), (EntityPlayerMP) event.getPlayer());
		
		      PlayerEntity player = event.getPlayer();
//		      player.getBrain().hasMemory(p_218191_1_)
		      CompoundNBT entityData = CapabilityPlayerData.getPlayerdata(player);
		      if(FPConfig.SERVER.isSpawnNoteEnabled.get() && !entityData.getBoolean("knownPlayer"))
		      {
		    	  entityData.putBoolean("knownPlayer", true);
		    	  player.inventory.add(new ItemStack(MiscItems.spawn_note));
		      }
//		      player.getEntityData().put(Constants.MOD_ID, entityData);
		}
	}
	
	@SubscribeEvent
	public void onBlockUpdate(BlockEvent.NeighborNotifyEvent event)
	{
		if(event.getWorld() instanceof World)
		{
			FPSelectorHelper.onBlockUpdate((World)event.getWorld(), event.getPos());
			FullBlockCache.notifyBlockChange(event.getPos(), (World)event.getWorld());
		}
			
	}
	
	@SubscribeEvent
	public void onBlockBreak(BlockEvent.BreakEvent event)
	{
		if(event.getWorld() instanceof World)
		{
			FullBlockCache.notifyBlockChange(event.getPos(), (World)event.getWorld());
			FPSelectorHelper.onBlockUpdate((World)event.getWorld(), event.getPos());
		}
	}
	
	@SubscribeEvent
	public void onBlockPlace(BlockEvent.EntityPlaceEvent event)
	{
		if(event.getWorld() instanceof World)
		{
			FPSelectorHelper.onBlockUpdate((World)event.getWorld(), event.getPos());
			FullBlockCache.notifyBlockChange(event.getPos(), (World)event.getWorld());
		}
	}
	
	@SubscribeEvent
	public void onBlockPlaceMulti(BlockEvent.EntityMultiPlaceEvent event)
	{
		if(event.getWorld() instanceof World)
		{
			event.getReplacedBlockSnapshots().forEach(s -> FullBlockCache.notifyBlockChange(s.getPos(), (World)event.getWorld()));
			FPSelectorHelper.onBlockUpdate((World)event.getWorld(), event.getReplacedBlockSnapshots());
		}
	}
	
	@SubscribeEvent
	public void onWorldLoad(final WorldEvent.Load event)
	{
		if(event.getWorld() instanceof ServerWorld)
		{
			HelperChunks.getTicketKeeper((ServerWorld) event.getWorld());
		}
	}
	
	@SubscribeEvent
	public void onLivingUpdate(LivingEvent.LivingUpdateEvent event)
	{
		final LivingEntity liv = event.getEntityLiving();
	
		if(liv.isInWater())
		{
			BlockPos pos = liv.blockPosition();
			FluidState state = liv.getCommandSenderWorld().getFluidState(pos);
			if(state.is(FuturepackTags.PARALYZING))
			{
				liv.addEffect(new EffectInstance(FPPotions.paralyze, 20));
			}
		}
		
		if(!event.getEntity().getCommandSenderWorld().isClientSide)
		{
			boolean slowFalling = (liv.getCommandSenderWorld().dimension().location().equals(Dimensions.ASTEROID_BELT_ID));
				
			// 0.01 = 0.08 * (1+x)
			// 0.01/0.08 = 1+x
			// (0.01/0.08)-1 = x
			
			AttributeModifier GRAVITY = new AttributeModifier(MICRO_GRAVITY_ID, "MICRO GRAVITY", (0.01/0.08)-1, AttributeModifier.Operation.MULTIPLY_TOTAL); 
				
			if(liv.getAttribute(ForgeMod.ENTITY_GRAVITY.get()).hasModifier(GRAVITY))
			{
				liv.getAttribute(ForgeMod.ENTITY_GRAVITY.get()).removeModifier(GRAVITY);	
			}
			if(slowFalling)
			{
				liv.getAttribute(ForgeMod.ENTITY_GRAVITY.get()).addTransientModifier(GRAVITY);
				liv.fallDistance = 0F;
			}	
		}
		
		if(cooldown == 0)
		{
			if(liv instanceof MobEntity)
			{
				for(ItemStack it : liv.getArmorSlots())
				{
					if(!it.isEmpty() && it.getItem().is(FuturepackTags.MINING_MINDCONTROL))
					{
						mindControlled.add(new WeakReference<MobEntity>((MobEntity) liv));
						break;
					}
				}
					
					
			}
			
		}
	}
	
	
	@SubscribeEvent
	public void onBiomeLoad(BiomeLoadingEvent event)
	{
		WorldGenRegistry.post(event);
	}
	
	@SubscribeEvent
	public void onFpKeyPress(EventFuturepackKey event)
	{
		if(event.type == EnumKeyTypes.ALL_BUTTONS)
		{
			//this is handled client side
//			FPGuiHandler.GENERIC_CHEST.openGui(event., entity);
		}
	}
}
