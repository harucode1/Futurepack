package futurepack.common.entity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


import futurepack.common.spaceships.AABBWorldCash;
import futurepack.common.spaceships.MovingShip;
import futurepack.common.spaceships.SpaceshipCashClient;
import futurepack.common.spaceships.SpaceshipCashServer;
import futurepack.depend.api.MiniWorld;
import net.minecraft.block.*;
import net.minecraft.block.Block;
import net.minecraft.block.FenceBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.BlockState;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.crash.ReportedException;
import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;
import net.minecraft.init.ParticleTypes;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.ServerWorld;

public class EntityMovingShip extends Entity
{
	private static final DataParameter<Optional<UUID>> SHIP_ID = EntityDataManager.createKey(EntityMovingShip.class, DataSerializers.OPTIONAL_UNIQUE_ID);
	
	private BlockPos destination;
	
	AABBWorldCash cash = new AABBWorldCash(this);
	
	public EntityMovingShip(World worldIn)
	{
		super(worldIn);
		setNoGravity(true);
		setSize(0.1F, 0.1F);
	}

	@Override
	protected void registerData()
	{
		this.dataManager.register(SHIP_ID, Optional.empty());
	}

	@Override
	protected void readAdditional(CompoundNBT nbt)
	{
		if(nbt.contains("shipIDM") && nbt.contains("shipIDL"))
		{
			setShipID(new UUID(nbt.getLong("shipIDM"), nbt.getLong("shipIDL")));
			MovingShip ship = new MovingShip(nbt.getCompound("ship"), this);
		}
		if(nbt.contains("dest"))
		{
			int[] dest = nbt.getIntArray("dest");
			destination = new BlockPos(dest[0], dest[1], dest[2]);
		}
	}

	@Override
	protected void writeAdditional(CompoundNBT nbt)
	{
		UUID shipID = getShipID();
		if(shipID!=null)
		{
			nbt.putLong("shipIDM", shipID.getMostSignificantBits());
			nbt.putLong("shipIDL", shipID.getLeastSignificantBits());
			
			MovingShip ship = SpaceshipCashServer.getShipByUUID(shipID);
			if(ship!=null)
				nbt.put("ship", ship.write());
		}
		if(destination!=null)
		{
			int[] dest = new int[]{destination.getX(), destination.getY() , destination.getZ()};
			nbt.putIntArray("dest", dest);
		}
	}
	
	@Override
	public void baseTick()
	{
		super.baseTick();
//		
//		if(!world.isRemote)
//		{
//			if(destination!=null)
//			{
//				MiniWorld mini = SpaceshipCashClient.getShipFromID(world, getShipID());
//				
//				if(mini==null)
//					return;
//				
//				if(this.getBoundingBox() !=null && this.getBoundingBox().contains(new Vector3d(destination).addVector(mini.width/2.0,0, mini.depth/2.0).addVector(0.0, 0.1, 0.0)))
//				{
//					if(ticksExisted == 40)
//					{
//						MovingShip ship = getMovingShip();
//						ship.placeBlocks(world, destination);
//						ship.placeObjects(world, destination);
//						
//						this.remove();
//						SpaceshipCashServer.removeInvalidEntrys();
//					}
//					else
//					{
//						rotationPitch = 0;
//						rotationYaw = 0;
//						this.getMotion().x = 0;
//						this.getMotion().y = 0;
//						this.getMotion().z = 0;
//						setPositionAndUpdate(destination.getX()+mini.width/2.0, destination.getY(), destination.getZ()+mini.depth/2.0);
//					}
//				}
//				else
//				{
//					Vector3d mot = new Vector3d(destination).addVector(mini.width/2.0, 0, mini.depth/2.0).subtract(this.getPositionVector()).normalize().scale(0.1D);
//					this.getMotion().x = mot.x;
//					this.getMotion().y = mot.y;
//					this.getMotion().z = mot.z;
//					ticksExisted = 0;
//				}
//			}
//			else
//			{
//				MovingShip ship = getMovingShip();
//				ship.placeBlocks(world, new BlockPos(this));
//				ship.placeObjects(world, new BlockPos(this));
//				
//				this.remove();
//				SpaceshipCashServer.removeInvalidEntrys();
//			}
//		}
		if(world.isRemote)
		{
			UUID uuid = getShipID();
			if(uuid!=null && height == 0.1F && width == 0.1F)
			{
				MiniWorld mini = SpaceshipCashClient.getShipFromID(world, uuid);
				if(mini!=null)
				{
					double x = posX, y = posY, z = posZ;
					setSize(Math.max(mini.depth, mini.width), mini.height);
					posX = x;
					posY = y;
					posZ = z;
				}
			}
		}
//		move(MoverType.SELF, getMotion().x, getMotion().y, getMotion().z);
//		//rotationYaw=30;
//		//rotationPitch=-30;
		
		
		
		MovingShip ship = getMovingShip();
		
		int wait1 = 20 * 10;
		int move = 20 * 30;
		int rotate = 20 * 9;
		
		if(ticksExisted < wait1)
		{
			
		}
		else if(ticksExisted-wait1 < move)
		{
			move(MoverType.SELF, 0, 0.1, 0);
		}
		else if(ticksExisted -move -wait1 < rotate)
		{
			rotationYaw = (1 + ticksExisted -move -wait1) / (float)rotate * 360F;
		}
		else if(ticksExisted -move -wait1 -rotate < move)
		{
			move(MoverType.SELF, 0, -0.1, 0);
		}
		else if(ticksExisted -(2*move) -wait1 -rotate > wait1)
		{
			if(ship!=null && !world.isRemote)
			{
				BlockPos pos = new BlockPos(this).add(-ship.getWidth()/2, 0, -ship.getDepth()/2);
				
				ship.placeBlocks(world, pos);
				ship.placeObjects(world, pos);
				
				this.remove();
				SpaceshipCashServer.removeInvalidEntrys();
			}
			
		}
		
		world.addParticle(ParticleTypes.CRIT, posX, posY, posZ, 0F, 0F, 0F);
		
		if(!world.isRemote)
		{
			((ServerWorld)world).spawnParticle(ParticleTypes.ANGRY_VILLAGER, posX, posY, posZ, 5, 0F, 0F, 0F, 0F);
			if(destination!=null)
			{
				((ServerWorld)world).spawnParticle(ParticleTypes.BARRIER, destination.getX()+0.5F, destination.getX()+0.5F, destination.getX()+0.5F, 5, 0F, 0F, 0F, 0F);
			}
		}
	}
	
	
	public UUID getShipID()
	{
		return this.dataManager.get(SHIP_ID).orNull();
	}
	
	public void setShipID(UUID shipID)
	{
		this.dataManager.set(SHIP_ID, Optional.fromNullable(shipID));
		
		MovingShip ship = getMovingShip();
		if(ship!=null)
		{
			setSize(Math.max(ship.getWidth(), ship.getDepth()), ship.getHeight());
			
			//
			
			List<AxisAlignedBB> vanilla = world.getCollisionBoxes(this, getBoundingBox());
			List<AxisAlignedBB> cashed = cash.getCollisionBoxes(this, getBoundingBox());
			
			int v = vanilla.size();
			int c = cashed.size();
			
			if(v != c)
			{
				cashed.isEmpty();
			}
		}
	}
	
	private MovingShip getMovingShip()
	{
		UUID uid = getShipID();
		if(uid!=null)
		{
			return SpaceshipCashServer.getShipByUUID(uid);
		}
		return null;
	}
	
	@Override
	protected boolean canFitPassenger(Entity passenger)
    {
		return passenger instanceof EntityMovingShipChair;
    }
	
	@Override
	public void updatePassenger(Entity passenger)
	{
		if (this.isPassenger(passenger))
		{
			EntityMovingShipChair chair = (EntityMovingShipChair) passenger;
			chair.alignToShip(posX, posY, posZ, rotationYaw, rotationPitch);
		}
	}

	public void setDestination(BlockPos endCoords)
	{
		destination = endCoords;
	}
	
	@Override
	public boolean isInLava()
    {
        return false;
    }
	
	@Override
	public boolean handleWaterMovement()
    {
		return false;
    }
	
	@Override
	protected boolean canTriggerWalking()
	{
		return false;
	}

	public double getSizeSq()
	{
		return 100;
	}
	
	public void move(MoverType type, double x, double y, double z)
    {
//		if(true)
//		{
//			super.move(type, x, y, z);
//			return;
//		}
		
        if (this.noClip)
        {
            this.setBoundingBox(this.getBoundingBox().offset(x, y, z));
            this.resetPositionToBB();
        }
        else
        {
            if (type == MoverType.PISTON)
            {
//                long i = this.world.getTotalWorldTime();
//
//                if (i != this.pistonDeltasGameTime)
//                {
//                    Arrays.fill(this.pistonDeltas, 0.0D);
//                    this.pistonDeltasGameTime = i;
//                }
//
//                if (x != 0.0D)
//                {
//                    int j = EnumFacing.Axis.X.ordinal();
//                    double d0 = MathHelper.clamp(x + this.pistonDeltas[j], -0.51D, 0.51D);
//                    x = d0 - this.pistonDeltas[j];
//                    this.pistonDeltas[j] = d0;
//
//                    if (Math.abs(x) <= 9.999999747378752E-6D)
//                    {
//                        return;
//                    }
//                }
//                else if (y != 0.0D)
//                {
//                    int l4 = EnumFacing.Axis.Y.ordinal();
//                    double d12 = MathHelper.clamp(y + this.pistonDeltas[l4], -0.51D, 0.51D);
//                    y = d12 - this.pistonDeltas[l4];
//                    this.pistonDeltas[l4] = d12;
//
//                    if (Math.abs(y) <= 9.999999747378752E-6D)
//                    {
//                        return;
//                    }
//                }
//                else
//                {
//                    if (z == 0.0D)
//                    {
//                        return;
//                    }
//
//                    int i5 = EnumFacing.Axis.Z.ordinal();
//                    double d13 = MathHelper.clamp(z + this.pistonDeltas[i5], -0.51D, 0.51D);
//                    z = d13 - this.pistonDeltas[i5];
//                    this.pistonDeltas[i5] = d13;
//
//                    if (Math.abs(z) <= 9.999999747378752E-6D)
//                    {
//                        return;
//                    }
//                }
            	return;
            }

            this.world.getProfiler().startSection("move");
            double d10 = this.posX;
            double d11 = this.posY;
            double d1 = this.posZ;

            if (this.isInWeb)
            {
                this.isInWeb = false;
                x *= 0.25D;
                y *= 0.05000000074505806D;
                z *= 0.25D;
                this.getMotion().x = 0.0D;
                this.getMotion().y = 0.0D;
                this.getMotion().z = 0.0D;
            }

            double d2 = x;
            double d3 = y;
            double d4 = z;

//            if ((type == MoverType.SELF || type == MoverType.PLAYER) && this.onGround && this.isSneaking() && this instanceof EntityPlayer)
//            {
//                for (double d5 = 0.05D; x != 0.0D && this.world.getCollisionBoxes(this, this.getBoundingBox().offset(x, (double)(-this.stepHeight), 0.0D)).isEmpty(); d2 = x)
//                {
//                    if (x < 0.05D && x >= -0.05D)
//                    {
//                        x = 0.0D;
//                    }
//                    else if (x > 0.0D)
//                    {
//                        x -= 0.05D;
//                    }
//                    else
//                    {
//                        x += 0.05D;
//                    }
//                }
//
//                for (; z != 0.0D && this.world.getCollisionBoxes(this, this.getBoundingBox().offset(0.0D, (double)(-this.stepHeight), z)).isEmpty(); d4 = z)
//                {
//                    if (z < 0.05D && z >= -0.05D)
//                    {
//                        z = 0.0D;
//                    }
//                    else if (z > 0.0D)
//                    {
//                        z -= 0.05D;
//                    }
//                    else
//                    {
//                        z += 0.05D;
//                    }
//                }
//
//                for (; x != 0.0D && z != 0.0D && this.world.getCollisionBoxes(this, this.getBoundingBox().offset(x, (double)(-this.stepHeight), z)).isEmpty(); d4 = z)
//                {
//                    if (x < 0.05D && x >= -0.05D)
//                    {
//                        x = 0.0D;
//                    }
//                    else if (x > 0.0D)
//                    {
//                        x -= 0.05D;
//                    }
//                    else
//                    {
//                        x += 0.05D;
//                    }
//
//                    d2 = x;
//
//                    if (z < 0.05D && z >= -0.05D)
//                    {
//                        z = 0.0D;
//                    }
//                    else if (z > 0.0D)
//                    {
//                        z -= 0.05D;
//                    }
//                    else
//                    {
//                        z += 0.05D;
//                    }
//                }
//            }

            List<AxisAlignedBB> list1 = cash.getCollisionBoxes(this, this.getBoundingBox().expand(x, y, z));
            AxisAlignedBB axisalignedbb = this.getBoundingBox();

            if (y != 0.0D)
            {
                int k = 0;

                for (int l = list1.size(); k < l; ++k)
                {
                    y = ((AxisAlignedBB)list1.get(k)).calculateYOffset(this.getBoundingBox(), y);
                }

                this.setBoundingBox(this.getBoundingBox().offset(0.0D, y, 0.0D));
            }

            if (x != 0.0D)
            {
                int j5 = 0;

                for (int l5 = list1.size(); j5 < l5; ++j5)
                {
                    x = ((AxisAlignedBB)list1.get(j5)).calculateXOffset(this.getBoundingBox(), x);
                }

                if (x != 0.0D)
                {
                    this.setBoundingBox(this.getBoundingBox().offset(x, 0.0D, 0.0D));
                }
            }

            if (z != 0.0D)
            {
                int k5 = 0;

                for (int i6 = list1.size(); k5 < i6; ++k5)
                {
                    z = ((AxisAlignedBB)list1.get(k5)).calculateZOffset(this.getBoundingBox(), z);
                }

                if (z != 0.0D)
                {
                    this.setBoundingBox(this.getBoundingBox().offset(0.0D, 0.0D, z));
                }
            }

            boolean flag = this.onGround || d3 != y && d3 < 0.0D;

            if (this.stepHeight > 0.0F && flag && (d2 != x || d4 != z))
            {
                double d14 = x;
                double d6 = y;
                double d7 = z;
                AxisAlignedBB axisalignedbb1 = this.getBoundingBox();
                this.setBoundingBox(axisalignedbb);
                y = (double)this.stepHeight;
                List<AxisAlignedBB> list = cash.getCollisionBoxes(this, this.getBoundingBox().expand(d2, y, d4));
                AxisAlignedBB axisalignedbb2 = this.getBoundingBox();
                AxisAlignedBB axisalignedbb3 = axisalignedbb2.expand(d2, 0.0D, d4);
                double d8 = y;
                int j1 = 0;

                for (int k1 = list.size(); j1 < k1; ++j1)
                {
                    d8 = ((AxisAlignedBB)list.get(j1)).calculateYOffset(axisalignedbb3, d8);
                }

                axisalignedbb2 = axisalignedbb2.offset(0.0D, d8, 0.0D);
                double d18 = d2;
                int l1 = 0;

                for (int i2 = list.size(); l1 < i2; ++l1)
                {
                    d18 = ((AxisAlignedBB)list.get(l1)).calculateXOffset(axisalignedbb2, d18);
                }

                axisalignedbb2 = axisalignedbb2.offset(d18, 0.0D, 0.0D);
                double d19 = d4;
                int j2 = 0;

                for (int k2 = list.size(); j2 < k2; ++j2)
                {
                    d19 = ((AxisAlignedBB)list.get(j2)).calculateZOffset(axisalignedbb2, d19);
                }

                axisalignedbb2 = axisalignedbb2.offset(0.0D, 0.0D, d19);
                AxisAlignedBB axisalignedbb4 = this.getBoundingBox();
                double d20 = y;
                int l2 = 0;

                for (int i3 = list.size(); l2 < i3; ++l2)
                {
                    d20 = ((AxisAlignedBB)list.get(l2)).calculateYOffset(axisalignedbb4, d20);
                }

                axisalignedbb4 = axisalignedbb4.offset(0.0D, d20, 0.0D);
                double d21 = d2;
                int j3 = 0;

                for (int k3 = list.size(); j3 < k3; ++j3)
                {
                    d21 = ((AxisAlignedBB)list.get(j3)).calculateXOffset(axisalignedbb4, d21);
                }

                axisalignedbb4 = axisalignedbb4.offset(d21, 0.0D, 0.0D);
                double d22 = d4;
                int l3 = 0;

                for (int i4 = list.size(); l3 < i4; ++l3)
                {
                    d22 = ((AxisAlignedBB)list.get(l3)).calculateZOffset(axisalignedbb4, d22);
                }

                axisalignedbb4 = axisalignedbb4.offset(0.0D, 0.0D, d22);
                double d23 = d18 * d18 + d19 * d19;
                double d9 = d21 * d21 + d22 * d22;

                if (d23 > d9)
                {
                    x = d18;
                    z = d19;
                    y = -d8;
                    this.setBoundingBox(axisalignedbb2);
                }
                else
                {
                    x = d21;
                    z = d22;
                    y = -d20;
                    this.setBoundingBox(axisalignedbb4);
                }

                int j4 = 0;

                for (int k4 = list.size(); j4 < k4; ++j4)
                {
                    y = ((AxisAlignedBB)list.get(j4)).calculateYOffset(this.getBoundingBox(), y);
                }

                this.setBoundingBox(this.getBoundingBox().offset(0.0D, y, 0.0D));

                if (d14 * d14 + d7 * d7 >= x * x + z * z)
                {
                    x = d14;
                    y = d6;
                    z = d7;
                    this.setBoundingBox(axisalignedbb1);
                }
            }

            this.world.getProfiler().endSection();
            this.world.getProfiler().startSection("rest");
            this.resetPositionToBB();
            this.collidedHorizontally = d2 != x || d4 != z;
            this.collidedVertically = d3 != y;
            this.onGround = this.collidedVertically && d3 < 0.0D;
            this.collided = this.collidedHorizontally || this.collidedVertically;
            int j6 = MathHelper.floor(this.posX);
            int i1 = MathHelper.floor(this.posY - 0.20000000298023224D);
            int k6 = MathHelper.floor(this.posZ);
            BlockPos blockpos = new BlockPos(j6, i1, k6);
            BlockState iblockstate = this.world.getBlockState(blockpos);

            if (iblockstate.getMaterial() == Material.AIR)
            {
                BlockPos blockpos1 = blockpos.down();
                BlockState iblockstate1 = this.world.getBlockState(blockpos1);
                Block block1 = iblockstate1.getBlock();

                if (block1 instanceof FenceBlock || block1 instanceof WallBlock || block1 instanceof FenceGateBlock)
                {
                    iblockstate = iblockstate1;
                    blockpos = blockpos1;
                }
            }

            this.updateFallState(y, this.onGround, iblockstate, blockpos);

            if (d2 != x)
            {
                this.getMotion().x = 0.0D;
            }

            if (d4 != z)
            {
                this.getMotion().z = 0.0D;
            }

            Block block = iblockstate.getBlock();

            if (d3 != y)
            {
                block.onLanded(this.world, this);
            }


            try
            {
                this.doBlockCollisions();
            }
            catch (Throwable throwable)
            {
                CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Checking entity block collision");
                CrashReportCategory crashreportcategory = crashreport.makeCategory("Entity being checked for collision");
                this.addEntityCrashInfo(crashreportcategory);
                throw new ReportedException(crashreport);
            }

            this.world.getProfiler().endSection();
        }
    }
}
